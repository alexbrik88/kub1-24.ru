<?php
namespace frontend\models;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\employee\Employee;
use common\models\rent\RentAgreement;
use common\modules\acquiring\models\Acquiring;
use common\modules\cards\models\CardAccount;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\AnalyticsMultiCompanyManager;
use frontend\modules\cash\models\cashSearch\ChartTrait;
use frontend\modules\cash\models\cashSearch\FilterItemsTrait;
use frontend\modules\cash\models\cashSearch\FilterNamesTrait;
use frontend\modules\cash\models\cashSearch\HelperTrait;
use frontend\modules\cash\models\cashSearch\QueryForeignTrait;
use frontend\modules\cash\models\cashSearch\QueryTrait;
use frontend\modules\cash\models\cashSearch\StatisticsTrait;
use frontend\modules\cash\models\cashSearch\WalletNamesTrait;
use yii\base\Exception;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\db\Query;
use Yii;

class RentAgreementCashSearch extends \yii\base\Model {

    use ChartTrait;
    use FilterItemsTrait;
    use FilterNamesTrait;
    use HelperTrait;
    use StatisticsTrait;
    use WalletNamesTrait;

    const WALLET_BANK = 1;
    const WALLET_ORDER = 2;
    const WALLET_EMONEY = 3;
    const WALLET_ACQUIRING = 4;
    const WALLET_CARD = 5;
    const WALLET_FOREIGN_BANK = 11;
    const WALLET_FOREIGN_ORDER = 12;
    const WALLET_FOREIGN_EMONEY = 13;

    const OPERATION_TYPE_FACT = 1;
    const OPERATION_TYPE_PLAN = 2;

    const FILTER_AMOUNT_TYPE_EQUAL = 0;
    const FILTER_AMOUNT_TYPE_MORE_THAN = 1;
    const FILTER_AMOUNT_TYPE_LESS_THAN = 2;
    const FILTER_FLOW_TYPE_ALL = -1;
    const FILTER_FLOW_TYPE_INCOME = 1;
    const FILTER_FLOW_TYPE_EXPENSE = 0;

    // widget
    public $widget_wallet_id;
    public $widget_currency_name;

    // filters;
    public $company_id;
    public $contractor_name;
    public $income_item_id;
    public $expenditure_item_id;
    public $flow_type;
    public $project_id;
    public $payment_priority;
    public $operation_type;

    // multiple filters
    public $contractor_ids;
    public $reason_ids;
    public $wallet_ids;

    // stats
    public $periodStartDate;
    public $periodEndDate;

    // private
    protected $_employee;
    protected $_companiesIds;
    protected $_filterQuery;
    protected $_filterQueryFullPeriod;

    // modal #many-item
    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;

    // chart
    private $_chartDataFact = [];
    private $_chartDataPlan = [];

    // plan table
    public static $paymentPriorityNames = [
        Contractor::PAYMENT_PRIORITY_HIGH => '1 - Большой',
        Contractor::PAYMENT_PRIORITY_MEDIUM => '2 - Средний',
        Contractor::PAYMENT_PRIORITY_LOW => '3 - Наименьший',
    ];

    // show plan
    protected $_userConfigShowPlan = true;

    private RentAgreement $rentAgreement;

    public function __construct(RentAgreement $rentAgreement, $config = [])
    {
        $this->rentAgreement = $rentAgreement;
        $this->_companiesIds = [$rentAgreement->company_id];
        $this->contractor_ids = [$rentAgreement->contractor_id];

        parent::__construct($config);
    }

    public function getRentAgreement()
    {
        return $this->rentAgreement;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [[
                'company_id',
                'wallet_ids',
                'flow_type',
                'contractor_ids',
                'reason_ids',
                'project_id',
                'contractor_name',
                'payment_priority',
                'operation_type',
                'sale_point_id',
                'industry_id',
            ], 'safe'],
            [[
                'contractor_name_2',
                'description_2',
                'amount_type_2',
                'amount_2',
                'flow_type_2',
            ], 'safe']
        ]);
    }

    /**
     *
     */
    public function init()
    {
        $this->_employee = Yii::$app->user->identity;
        $this->widget_wallet_id = Yii::$app->session->get('cash_widget_wallet');
        $this->widget_currency_name = Yii::$app->session->get('cash_widget_currency');
        $this->initWalletNames($this->_companiesIds);
    }

    /**
     *
     */
    public function load($params, $formName = null)
    {
        parent::load($params);
        $this->contractor_ids = array_filter((array)$this->contractor_ids);
        $this->reason_ids = array_filter((array)$this->reason_ids);
        $this->wallet_ids = array_filter((array)$this->wallet_ids);
        $this->project_id = strlen($this->project_id) ? $this->project_id : null;
        $this->sale_point_id = strlen($this->sale_point_id) ? $this->sale_point_id : null;
        $this->industry_id = strlen($this->industry_id) ? $this->industry_id : null;
    }

    /**
     * @param array $params
     * @return SqlDataProvider
     * @throws \Exception
     */
    public function search($params = [])
    {
        if (!$this->_companiesIds)
            throw new Exception('CashSearch params not initialized');

        $this->load($params);

        $dateRange = StatisticPeriod::getSessionPeriod();
        $this->periodStartDate = DateHelper::format($dateRange['from'], 'd.m.Y', 'Y-m-d');
        $this->periodEndDate = DateHelper::format($dateRange['to'], 'd.m.Y', 'Y-m-d');

        $query = $this->getWalletsQuery([]);

        $query->addSelect([
            't.id',
            't.company_id',
            't.tb',
            't.amountExpense',
            't.amountIncome',
            't.rs',
            't.cashbox_id',
            't.emoney_id',
            't.acquiring_id',
            't.card_id',
            't.created_at',
            't.date',
            't.flow_type',
            't.amount',
            't.contractor_id',
            't.description',
            't.expenditure_item_id',
            't.income_item_id',
            't.wallet_id',
            't.project_id',
            't.sale_point_id',
            't.industry_id',
            't.is_internal_transfer',
            't.transfer_key',
            't.has_tin_children'
        ]);

        $query->leftJoin('contractor', 'contractor.id = t.contractor_id')->addSelect('contractor.payment_priority');

        $this->setFilters($query);

        $this->_filterQueryFullPeriod = clone $query;

        $query->andWhere(['between', 't.date', $dateRange['from'], $dateRange['to']]);

        $query->groupBy('t.id');

        $this->_filterQuery = clone $query;

        $dataProvider = new SqlDataProvider([
            'sql' => $query->groupBy('t.transfer_key')->createCommand()->rawSql,
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncomeExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'description',
                    'created_at',
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    public function getIsForeign()
    {
        return (bool) $this->widget_currency_name == Currency::DEFAULT_NAME;
    }

    public function getCurrencySymbol()
    {
        return (string) ArrayHelper::getValue(Currency::$currencySymbols, $this->widget_currency_name);
    }

    public function getCurrency_name()
    {
        return $this->widget_currency_name;
    }
    private function getWalletsQuery($walletsList = [])
    {
        if (empty($walletsList)) {
            $cbf = $this->_getFlowQuery(self::WALLET_BANK);
            $cef = $this->_getFlowQuery(self::WALLET_EMONEY);
            $cof = $this->_getFlowQuery(self::WALLET_ORDER);

            return (new Query)->from([
                't' => $cbf
                    ->union($cef, true)
                    ->union($cof, true)
            ]);
        }

        $selectedWallets = [];
        foreach ($walletsList AS $walletId) {
            if (strpos($walletId, '_') !== false) {
                @list($cashBlock, $currentWalletId) = explode('_', $walletId);
            } else {
                $cashBlock = $walletId;
                $currentWalletId = null;
            }

            $selectedWallets[$cashBlock][] = $currentWalletId ? "'{$currentWalletId}'" : null;
        }

        $_searchByWalletsTypes = [];
        foreach ($selectedWallets AS $walletId => $wallets) {
            switch ($walletId) {
                case self::WALLET_BANK:
                    $_searchByWalletsTypes[] = $this->_getFlowQuery(self::WALLET_BANK, implode(',', $wallets));
                    break;
                case self::WALLET_ORDER:
                    $_searchByWalletsTypes[] = $this->_getFlowQuery(self::WALLET_ORDER, implode(',', $wallets));
                    break;
                case self::WALLET_EMONEY:
                    $_searchByWalletsTypes[] = $this->_getFlowQuery(self::WALLET_EMONEY, implode(',', $wallets));
                    break;
            }
        }

        $subQuery = null;
        foreach ($_searchByWalletsTypes as $q) {
            if (!empty($subQuery))
                $subQuery->union($q, true);
            else
                $subQuery = $q;
        }

        return (new Query)->from(['t' => $subQuery]);
    }

    private function _getFlowQuery($cashBlock, $walletIDs = null)
    {
        $currentWalletCondition = $parentIdNullCondition = new Expression('1 = 1');

        if ($cashBlock == self::WALLET_BANK) {
            $className = CashBankFlows::class;
            if ($walletIDs) {
                $currentWalletCondition = "rs IN ($walletIDs)";
            }
            $parentIdNullCondition = 'parent_id IS NULL';
        }
        elseif ($cashBlock == self::WALLET_ORDER) {
            $className = CashOrderFlows::class;
            if ($walletIDs) {
                $currentWalletCondition = "cashbox_id IN ($walletIDs)";
            }
        }
        elseif ($cashBlock == self::WALLET_EMONEY) {
            $className = CashEmoneyFlows::class;
            if ($walletIDs) {
                $currentWalletCondition = "emoney_id IN ($walletIDs)";
            }
        } else {
            throw new \Exception('Invalid wallet.');
        }

        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        $tableName = $className::tableName();

        return $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" AS tb'),
                "IF({{t}}.[[flow_type]] = 0 OR {{t}}.[[is_internal_transfer]], {{t}}.[[amount]], 0) AS amountExpense",
                "IF({{t}}.[[flow_type]] = 1 OR {{t}}.[[is_internal_transfer]], {{t}}.[[amount]], 0) AS amountIncome",
                $cashBlock == self::WALLET_BANK ? 't.rs' : 'null AS [[rs]]',
                $cashBlock == self::WALLET_ORDER ? 't.cashbox_id' : 'null AS [[cashbox_id]]',
                $cashBlock == self::WALLET_EMONEY ? 't.emoney_id' : 'null AS [[emoney_id]]',
                'null AS [[acquiring_id]]',
                'null AS [[card_id]]',
                't.created_at',
                't.date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                't.project_id',
                't.sale_point_id',
                't.industry_id',
                new Expression(
                    ($cashBlock == self::WALLET_BANK ? 'CONCAT("1_", t.rs) AS wallet_id' : (
                        $cashBlock == self::WALLET_ORDER ? 'CONCAT("2_", t.cashbox_id) AS wallet_id' : (
                            $cashBlock == self::WALLET_EMONEY ? 'CONCAT("3_", t.emoney_id) AS wallet_id' :
                                'null AS wallet_id'
                            )
                        )
                    )
                ),
                't.is_internal_transfer',
                new Expression('IF (flow_type = 0, CONCAT(id, "_", IFNULL(internal_transfer_flow_id, "C")), CONCAT(IFNULL(internal_transfer_flow_id, "C"), "_", id)) AS transfer_key'),
                new Expression(
                    ($cashBlock == self::WALLET_BANK ? 't.has_tin_children' : (
                        $cashBlock == self::WALLET_ORDER ? '0 AS has_tin_children' : (
                            $cashBlock == self::WALLET_EMONEY ? '0 AS has_tin_children' :
                                '0 AS has_tin_children'
                            )
                        )
                    )
                ),
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $this->_companiesIds])
            ->andWhere($currentWalletCondition)
            ->andWhere($parentIdNullCondition);  // parent_id_null_condition
    }
}
