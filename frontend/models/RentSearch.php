<?php

namespace frontend\models;

use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\rent\Category;
use common\models\rent\Entity;
use common\models\rent\RentAgreement;
use frontend\components\PageSize;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Фильтр для "аренды" (вкладка "календарь" и "аренда")
 *
 * @property bool $hasFilters
 */
class RentSearch extends Model
{

    const SCENARIO_CALENDAR = 'calendar';
    const SCENARIO_LIST = 'list';

    const SORT_ATTRIBUTES = [
        self::SCENARIO_CALENDAR => ['title'],
        self::SCENARIO_LIST => [
            'document_number' => [
                'asc' => ['actual' => SORT_DESC, 'document_number' => SORT_ASC],
                'desc' => ['actual' => SORT_DESC, 'document_number' => SORT_DESC],
            ],
            'created_at' => [
                'asc' => ['actual' => SORT_DESC, 'created_at' => SORT_ASC],
                'desc' => ['actual' => SORT_DESC, 'created_at' => SORT_DESC],
            ],
            'date_begin' => [
                'asc' => ['actual' => SORT_DESC, 'date_begin' => SORT_ASC],
                'desc' => ['actual' => SORT_DESC, 'date_begin' => SORT_DESC],
            ],
            'date_end' => [
                'asc' => ['actual' => SORT_DESC, 'date_end' => SORT_ASC],
                'desc' => ['actual' => SORT_DESC, 'date_end' => SORT_DESC],
            ],
            'days_left' => [
                'asc' => ['actual' => SORT_DESC, 'days_left' => SORT_ASC],
                'desc' => ['actual' => SORT_DESC, 'days_left' => SORT_DESC],
            ],
            'amount' => [
                'asc' => ['actual' => SORT_DESC, 'created_at' => SORT_ASC],
                'desc' => ['actual' => SORT_DESC, 'created_at' => SORT_DESC],
            ],
        ]
    ];

    const DEFAULT_ORDER = [
        self::SCENARIO_CALENDAR => ['title' => SORT_ASC],
        self::SCENARIO_LIST => ['days_left' => SORT_ASC]
    ];

    const CALENDAR_MODE_DAY = 'day';
    const CALENDAR_MODE_MONTH = 'month';
    const CALENDAR_MODES = [self::CALENDAR_MODE_DAY, self::CALENDAR_MODE_MONTH];
    const CALENDAR_MODE_LABELS = [
        self::CALENDAR_MODE_DAY => 'День',
        self::CALENDAR_MODE_MONTH => 'Месяц',
    ];

    /** @var int */
    public $company_id;
    /** @var string */
    public $keyword;
    /** @var int */
    public $category;
    /** @var int */
    public $entity;
    /** @var int */
    public $entity_id;
    /** @var int */
    public $contractor;
    /** @var int */
    public $employee;
    /** @var string */
    public $calendarMode;
    /** @var string[] */
    public $calendarRange;
    /** @var string */
    public $status;

    public $_query;

    public static function applyDefaultSort()
    {
        if (Yii::$app->request->get('sort') === null && Yii::$app->request->get('defaultSorting') !== null) {
            Yii::$app->request->setQueryParams(['sort' => Yii::$app->request->get('defaultSorting')]);
        }
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CALENDAR => ['keyword', 'category'],
            self::SCENARIO_LIST => ['entity', 'entity_id', 'status', 'contractor', 'keyword', 'category', 'employee']
        ];
    }

    public function rules()
    {
        return [
            [['category', 'entity', 'contractor', 'employee'], 'integer'],
            [['entity_id'], 'safe'],
            [['status'], 'in', 'range' => RentAgreement::STATUS],
            [
                ['category', 'entity', 'contractor', 'employee'],
                'filter',
                'filter' => function($value) {
                    return empty($value) ? null : $value;
                }
            ],
            ['keyword', 'string', 'max' => 50],

            [
                ['category', 'entity', 'contractor', 'employee'],
                function($attribute) {
                    if ($this->hasErrors($attribute)) {
                        $this->clearErrors($attribute);
                        $this->$attribute = null;
                    }

                    return $this->$attribute;
                },
                'skipOnError' => false,
            ],
            [
                'keyword',
                function($attribute) {
                    if ($this->hasErrors($attribute)) {
                        $this->clearErrors($attribute);
                        return substr($this->$attribute, 0, 50);
                    }

                    return $this->$attribute;
                },
                'skipOnError' => false
            ],
        ];
    }

    public function search(array $params): ActiveDataProvider
    {
        $this->load($params);
        $this->validate();

        /** @var ActiveQuery $query */
        $query = $this->{'_query' . ucfirst($this->scenario)}();

        $query->andFilterWhere([
            Entity::tableName() . '.id' => $this->entity,
            Entity::tableName() . '.category_id' => $this->category,
            RentAgreement::tableName() . '.contractor_id' => $this->contractor,
            RentAgreement::tableName() . '.document_author_id' => $this->employee
        ]);

        if ($this->keyword) {
            $query->andWhere(['OR',
                ['like', Entity::tableName() . '.title', $this->keyword]
            ]);
        }

        if ($this->entity_id) {
            $query->andWhere([Entity::tableName() . '.id' => $this->entity_id]);
        }

        if ($this->status) {
            $date = date('Y-m-d');

            $currentRentsQuery = RentAgreement::find()
                ->where('entity_id = ' . RentEntity::tableName() . '.id')
                ->andWhere(['and',
                    ['<=', RentAgreement::tableName() . '.date_begin', $date],
                    ['>=', RentAgreement::tableName() . '.date_end', $date],
                ]);

            switch ($this->status) {
                case RentAgreement::STATUS_PROCEED :
                    $query->andWhere(['and',
                        ['exists', $currentRentsQuery],
                    ]);
                    break;
                case RentAgreement::STATUS_COMPLETED :
                    $query->andWhere(['and',
                        ['not', ['exists', $currentRentsQuery]],
                    ]);
                    break;
            }
        }

        $this->_query = $query;

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => self::SORT_ATTRIBUTES[$this->scenario],
                'defaultOrder' => self::DEFAULT_ORDER[$this->scenario]
            ],
            'pagination' => [
                'pageSize' => PageSize::get()
            ]
        ]);
    }

    public function getQuery():ActiveQuery
    {
        return $this->_query;
    }


    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Базовый запрос для вкладки "календарь"
     * @return ActiveQuery
     */
    private function _queryCalendar(): ActiveQuery
    {
        $query = RentEntity::findActual($this->company_id)->joinWith(['rents' => function($query) {
            /** @var ActiveQuery $query */
            $query->orderBy([
                RentAgreement::tableName() . '.date_begin' => SORT_ASC,
                RentAgreement::tableName() . '.date_end' => SORT_ASC
            ]);
            $query->andOnCondition([RentAgreement::tableName() . '.deleted' => false]);

            if ($this->calendarMode == self::CALENDAR_MODE_DAY) {
                
            } else {
                $query->andOnCondition(['or',
                    ['between', RentAgreement::tableName() . '.date_begin', $this->calendarRange[0], $this->calendarRange[1]],
                    ['between', RentAgreement::tableName() . '.date_end', $this->calendarRange[0], $this->calendarRange[1]],
                    ['and', ['<', RentAgreement::tableName() . '.date_begin', $this->calendarRange[0]], ['>', RentAgreement::tableName() . '.date_end', $this->calendarRange[1]]]
                ]);
            }
        }]);

        return $query->groupBy(RentEntity::tableName() . '.id');
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Базовый запрос для вкладки "аренда"
     * @return ActiveQuery
     */
    private function _queryList(): ActiveQuery
    {
        $query = RentAgreement::findByFilter($this->company_id, $this->entity, $this->contractor, $this->employee);

        $query->joinWith(['employee', 'contractor'], true);

        $query->addSelect([
            RentAgreement::tableName() . '.*',
            'days_left' => new Expression('datediff(' . RentAgreement::tableName() . '.date_end, now())'),
            'actual' => new Expression('if(' . RentAgreement::tableName() . '.date_end >= now(), 1, 0)'),
            'IF (date_begin <= CURRENT_DATE() and CURRENT_DATE() <= date_end, 1, 2) as status',
        ]);

        return $query;
    }

    public function getHasFilters() {
        return $this->category || $this->entity || $this->contractor || $this->employee;
    }

    public function entityList()
    {
        if (!$this->_query) {
            $this->search([]);
        }

        return (clone $this->getQuery())
            ->select([Entity::tableName() . '.title'])
            ->indexBy(Entity::tableName() . '.id')
            ->column();
    }

    public function categoryList()
    {
        return Category::flatList();
    }

    public function entityActualList()
    {
        return ArrayHelper::map(Entity::findActual($this->company_id)->select(['id', 'title'])->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    public function contractorList()
    {
        $query = Contractor::find()
            ->byCompany($this->company_id)
            ->byDeleted(false)
            ->byStatus(Contractor::ACTIVE)
            ->byContractor(Contractor::TYPE_CUSTOMER);

        $contractors = [];

        /** @var Contractor $contractor */
        foreach ($query->all() as $contractor) {
            $contractors[$contractor->id] = $contractor->getNameWithType();
        }

        return $contractors;
    }

    public function employeeList()
    {
        $query = EmployeeCompany::find()->where([
            'company_id' => $this->company_id,
            'is_working' => true,
        ])->orderBy([
            'lastname' => SORT_ASC,
            'firstname' => SORT_ASC,
            'patronymic' => SORT_ASC,
        ]);

        return ArrayHelper::map($query->all(), 'employee_id', 'shortFio');
    }
}
