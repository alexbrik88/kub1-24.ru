<?php
namespace frontend\models;

use common\components\validators\PasswordValidator;
use yii\base\Model;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Password reset form
 */
class ChangePasswordForm extends Model
{
    public $oldPassword;
    public $newPassword;
    public $newPasswordRepeat;

    /**
     * @var \common\models\employee\Employee
     */
    private $_user;

    /**
     * @param array $config
     * @param null $user
     * @throws NotFoundHttpException
     */
    public function __construct($config = [], $user = null)
    {
        $this->_user = $user ? $user : Yii::$app->user->identity;
        if (!$this->_user) {
            throw new NotFoundHttpException;
        }

        parent::__construct($config = []);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oldPassword', 'newPassword', 'newPasswordRepeat'], 'required'],
            [['oldPassword'], function ($attribute) {
                if (!$this->_user->validatePassword($this->$attribute)) {
                    $this->addError($attribute, 'Неверный пароль');
                }
            }],
            [['newPassword'], PasswordValidator::className(),],
            [['newPasswordRepeat'], 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oldPassword' => 'Старый пароль',
            'newPassword' => 'Новый пароль',
            'newPasswordRepeat' => 'Повтор нового пароля',
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function save()
    {
        $user = $this->_user;
        $user->setPassword($this->newPassword);

        if ($user->save(true, ['password'])) {
            $user->closeOpenSessions(Yii::$app->session->id);

            return true;
        }

        return false;
    }
}
