<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.10.2019
 * Time: 0:00
 */

namespace frontend\models\jobs;

use common\models\employee\Employee;
use common\modules\import\models\ImportJobData;
use frontend\components\StatisticPeriod;
use Yii;
use yii\data\ActiveDataProvider;

class JobsSearch extends ImportJobData
{
    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'finished_at' => 'Дата завершения',
            'type' => 'Тип задачи',
            'params' => 'Параметры',
            'result' => 'Статус',
            'error' => 'Описание ошибки',
        ];
    }

    public function search($params = [])
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $period = StatisticPeriod::getSessionPeriod();
        $query = ImportJobData::find()
            ->andWhere([
                'AND',
                // ['employee_id' => $user->id],
                ['company_id' => $user->currentEmployeeCompany->company_id],
                ['between', 'created_at', strtotime($period['from'] . ' 00:00:00'), strtotime($period['to'] . ' 23:59:59')]
            ])
            ->orderBy(['created_at' => SORT_DESC]);

        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
