<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.01.2018
 * Time: 9:45
 */

namespace frontend\models;


use yii\base\Model;
use common\models\employee\Employee;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class ChangeDuplicateNotifyForm
 * @package frontend\models
 */
class ChangeDuplicateNotifyForm extends Model
{
    /**
     * @var
     */
    public $duplicateNotifyToSms;
    /**
     * @var
     */
    public $duplicateNotifyToEmail;

    /**
     * @var Employee
     */
    private $_user;

    /**
     * @param array $config
     * @param null $user
     * @throws NotFoundHttpException
     */
    public function __construct($config = [], $user = null)
    {
        $this->_user = $user ? $user : Yii::$app->user->identity;
        if (!$this->_user) {
            throw new NotFoundHttpException;
        }

        parent::__construct($config = []);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['duplicateNotifyToSms', 'duplicateNotifyToEmail'], 'safe'],
            [['duplicateNotifyToSms', 'duplicateNotifyToEmail'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'duplicateNotifyToSms' => 'Дублировать уведомления по SMS',
            'duplicateNotifyToEmail' => 'Дублировать уведомления по электронной почте',
        ];
    }

    /**
     * @return bool
     */
    public function saveNotify()
    {
        $user = $this->_user;
        $user->duplicate_notification_to_sms = $this->duplicateNotifyToSms;
        $user->duplicate_notification_to_email = $this->duplicateNotifyToEmail;

        return $user->save(false, [
            'duplicate_notification_to_sms', 'duplicate_notification_to_email',
        ]);
    }
}