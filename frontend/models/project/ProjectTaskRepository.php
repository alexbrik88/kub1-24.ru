<?php

namespace frontend\models\project;

use common\db\SortBuilder;
use common\components\helpers\ArrayHelper;
use common\models\project\Project;
use common\models\project\ProjectEmployee;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @deprecated
 */
final class ProjectTaskRepository extends Model
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int|null
     */
    public $contractor_id;

    /**
     * @var int|null
     */
    public $project_id;

    /**
     * @var int|null
     */
    public $employee_id;

    /**
     * @var int|null
     */
    public $owner_id;

    /**
     * @var int|null
     */
    public $status;

    /**
     * @var string|null
     */
    public $search;

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'date_begin' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'employee_id' => 'Ответственный',
            'project_id' => 'Проект',
            'description' => 'Краткое описание',
            'time' => 'Время',
            'status' => 'Статус',
        ];
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['employee_id'], 'integer'],
            [['project_id'], 'integer'],
            [['status'], 'integer'],
            [['search'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getById(int $id): ?ActiveRecord
    {
        return $this->getQuery()->andWhere(['task_id' => $id])->one();
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery()->with([
                'project',
                'event',
                'employeeCompany',
            ]),
            'sort' => $this->getSort(),
        ]);
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        $builder = new SortBuilder();
        $builder->addOrder('task_number', ProjectTask::tableName());
        $builder->addOrder('date_begin', ProjectTask::tableName());
        $builder->addOrder('date_end', ProjectTask::tableName());
        $builder->addOrders(['task_number', 'date_begin', 'date_end'], ProjectTask::tableName(), 'task_id');

        return $builder->buildSort();
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        $query = ProjectTask::find()->andFilterWhere([
            'company_id' => $this->company_id,
            'employee_id' => $this->employee_id,
            'project_id' => $this->project_id,
            'status' => $this->status,
        ]);

        if (!empty($this->search)) {
            $query->andWhere(['like', 'description', $this->search]);
        }

        if ($this->owner_id) {
            $query->andWhere(['employee_id' => $this->owner_id]);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getResponsible() // TODO: не имеет отношения к данной сущности (ProjectTask)!
    {
        $projectResponsible = Project::find()
            ->select([
                Project::tableName() . '.responsible',
                Project::tableName() . '.responsible as employee_id',
                'CONCAT(lastname, " ", firstname_initial, ".", patronymic_initial, ".") as fio'
            ])
            ->joinWith('responsibleEmployee')
            ->andWhere(['and',
                [Project::tableName() . '.id' => $this->project_id],
                [Project::tableName() . '.company_id' => $this->company_id],
            ])
            ->asArray()
            ->all();

        $responsible = ProjectEmployee::find()
            ->select([
                ProjectEmployee::tableName() . '.employee_id',
                'CONCAT(lastname, " ", firstname_initial, ".", patronymic_initial, ".") as fio'
            ])
            ->joinWith('employee')
            ->andWhere(['and',
                ['project_id' => $this->project_id],
                [ProjectEmployee::tableName() . '.company_id' => $this->company_id],
            ])
            ->asArray()
            ->all();

        return ArrayHelper::map(array_merge($responsible, $projectResponsible),
            'employee_id',
            'fio');
    }
}
