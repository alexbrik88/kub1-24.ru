<?php

namespace frontend\models\project;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\EmployeeCompany;
use common\models\project\Project;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property-read int $task_id Идентификатор задачи
 * @property int $task_number Номер задачи
 * @property string $task_name Дополнительный номер
 * @property int $company_id Идентификатор компании
 * @property int $employee_id Идентификатор ответственного сотрудника
 * @property int $project_id Идентефикатор проекта
 * @property int $status Статус задачи
 * @property string $description Описание задачи
 * @property string $date_begin Дата начала
 * @property string $date_end Дата окончания
 * @property string $time Время
 * @property-read string $created_at
 * @property-read string $updated_at
 * @property-read string $statusName Имя название статуса
 * @property-read Project $project Проект
 * @property-read EmployeeCompany $employeeCompany
 */
class ProjectTask extends ActiveRecord // TODO: final
{
    /** @var int */
    public const STATUS_IN_PROGRESS = 1;

    /** @var int */
    public const STATUS_EXPIRED = 2;

    /** @var int */
    public const STATUS_COMPLETED = 3;

    /** @var string[] */
    public const STATUS_LIST = [
        self::STATUS_IN_PROGRESS => 'В работе',
        self::STATUS_EXPIRED => 'Просрочена',
        self::STATUS_COMPLETED => 'Завершена',
    ];

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param Company $company
     * @param int|mixed $taskId
     * @return static|null
     */
    public static function findById(Company $company, $taskId): ?self
    {
        if (!is_scalar($taskId)) {
            return null;
        }

        return self::findOne(['company_id' => $company->id, 'task_id' => $taskId]);
    }

    /**
     * @param Company $company
     * @return int
     */
    public static function getNextNumber2(Company $company): int // TODO: оригинальное имя занято
    {
        $number = self::find()->andWhere(['company_id' => $company->id])->max('task_number') ?: 0;
        $number++;

        return $number;
    }

    /**
     * @return ActiveQuery
     */
    public function getProject(): ActiveQuery
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployeeCompany(): ActiveQuery
    {
        return $this->hasOne(EmployeeCompany::class, ['company_id' => 'company_id', 'employee_id' => 'employee_id']);
    }

    /**
     * Получить название статуса
     *
     * @return string
     */
    public function getStatusName(): string
    {
        return self::STATUS_LIST[$this->status];
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%project_task}}';
    }

    /**
     * @param int $company_id
     * @return void
     */
    public static function updateStatuses(int $company_id): void // TODO: Company
    {
        self::updateAll(
            ['date_end' => date(DateHelper::FORMAT_DATE)],
            ['company_id' => $company_id, 'status' => self::STATUS_COMPLETED, 'date_end' => null],
        );

        self::updateAll(
            ['date_end' => null],
            ['company_id' => $company_id, 'status' => self::STATUS_IN_PROGRESS],
        );
    }
}
