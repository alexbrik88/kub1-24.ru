<?php

namespace frontend\models\project;

use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\company\CompanyIndustry;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use yii\base\Model;

/**
 * Class ProjectForm
 */
class ProjectForm extends Model
{
    private EmployeeCompany $employeeCompany;
    private Company $company;
    private Project $project;
    private array $projectCustomers = [];

    /**
     * form attributes
     */
    public $name;
    public $start_date;
    public $end_date;
    public $status;
    public $responsible;
    public $number;
    public $addition_number;
    public $industry_id;
    public $description;
    public $customers = 1;

    public function __construct(EmployeeCompany $employeeCompany, ?Project $project = null, $config = [])
    {
        $this->employeeCompany = $employeeCompany;
        $this->company = $employeeCompany->company;
        $this->project = $project ?? new Project([
            'responsible' => $employeeCompany->employee_id,
            'status' => Project::STATUS_INPROGRESS,
            'number' => Project::getNextProjectNumber($this->company),
            'start_date' => date('d.m.Y')
        ]);

        $this->setAttributes($this->project->getAttributes([
            'name',
            'status',
            'responsible',
            'number',
            'addition_number',
            'industry_id',
            'description',
        ]), false);

        $this->start_date = $this->project->start_date && ($d = date_create($this->project->start_date)) ? $d->format('d.m.Y') : null;
        $this->end_date = $this->project->end_date && ($d = date_create($this->project->end_date)) ? $d->format('d.m.Y') : null;

        $this->project->company_id = $this->company->id;
        $this->project->populateRelation('company', $this->company);

        foreach ($this->project->customers as $projectCustomer) {
            $this->projectCustomers[] = new ProjectCustomerForm($this->project, $projectCustomer);
        }

        parent::__construct($config);
    }

    public function copy(Project $project)
    {
        $this->setAttributes($project->getAttributes([
            'name',
            'status',
            'responsible',
            'addition_number',
            'industry_id',
            'description',
        ]), false);
        $this->start_date = $project->start_date && ($d = date_create($project->start_date)) ? $d->format('d.m.Y') : null;
        $this->end_date = $project->end_date && ($d = date_create($project->end_date)) ? $d->format('d.m.Y') : null;

        $this->projectCustomers = [];
        foreach ($project->customers as $projectCustomer) {
            $projectCustomerCopy = clone $projectCustomer;
            $projectCustomerCopy->id = null;
            $projectCustomerCopy->project_id = $this->project->id;
            $projectCustomerCopy->isNewRecord = true;
            $this->projectCustomers[] = new ProjectCustomerForm($this->project, $projectCustomerCopy);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'description',
                ],
                'trim',
            ],
            [
                [
                    'name',
                    'number',
                    'start_date',
                    'end_date',
                    'status',
                    'responsible',
                    'customers',
                ],
                'required',
            ],
            [['description'], 'string', 'max' => 1000],
            [['number', 'name'], 'string', 'max' => 255],
            [['addition_number'], 'string', 'max' => 32],
            [['start_date', 'end_date'], 'date', 'format' => 'php:d.m.Y'],
            ['status', 'in', 'range' => array_keys(Project::$statuses)],
            [
                'responsible',
                'exist',
                'skipOnError' => true,
                'targetClass' => EmployeeCompany::class,
                'targetAttribute' => [
                    'responsible' => 'employee_id',
                ],
                'filter' => function ($query) {
                    $query->andWHere([
                        'company_id' => $this->company->id,
                    ]);
                }
            ],
            [
                'name',
                'unique',
                'skipOnError' => true,
                'targetClass' => Project::class,
                'filter' => function ($query) {
                    $query->andWHere([
                        'company_id' => $this->company->id,
                    ]);
                    if ($this->project->id) {
                        $query->andWhere(['not', ['id' => $this->project->id]]);
                    }
                }
            ],
            [
                'industry_id',
                'exist',
                'skipOnError' => true,
                'skipOnEmpty' => true,
                'targetClass' => CompanyIndustry::class,
                'targetAttribute' => [
                    'industry_id' => 'id',
                ],
                'filter' => function ($query) {
                    $query->andWHere([
                        'company_id' => $this->company->id,
                    ]);
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => 'Номер проекта',
            'responsible' => 'Ответственный',
            'projectCustomers' => 'Заказчик проекта',
            'name' => 'Название проекта',
            'status' => 'Статус',
            'description' => 'Описание',
            'start_date' => 'Дата начала проекта',
            'end_date' => 'Дата завершения проекта',
            'industry_id' => 'Направление',
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->projectCustomers = [];
            $customersData = $data['ProjectCustomerForm'] ?? [];
            foreach ((array) $customersData as $itemData) {
                $projectCustomer = new ProjectCustomerForm($this->project);
                $projectCustomer->load($itemData, '');
                $this->projectCustomers[] = $projectCustomer;
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        if (empty($this->projectCustomers)) {
            $this->addError('customers', 'Необходимо добавить заказчика.');
        } else {
            foreach ($this->projectCustomers as $customer) {
                if (!$customer->validate()) {
                    foreach ($customer->getFirstErrors() as $key => $value) {
                        $this->addError('customers', $value);
                    }
                }
            }
        }

        parent::afterValidate();
    }

    public function setProjectCustomers()
    {
        //
    }

    /**
     * @return array
     */
    public function getProjectCustomers()
    {
        return $this->projectCustomers;
    }

    /**
     * @return bool
     */
    public function getIsNewRecord()
    {
        return $this->project->getIsNewRecord();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->project->id;
    }

    /**
     * @return
     */
    public function addCustomer(Contractor $contractor)
    {
        $projectCustomer = new ProjectCustomerForm($this->project);
        $projectCustomer->customer_id = $contractor->id;
        $this->projectCustomers[] = $projectCustomer;

        return $projectCustomer;
    }

    /**
     * @return array
     */
    public function getResponsibleData()
    {
        return $this->company->getEmployeeCompanies()->andFilterWhere([
            'or',
            ['is_working' => true],
            ['employee_id' => $this->project->responsible],
        ])->orderBy([
            'lastname' => SORT_ASC,
            'firstname' => SORT_ASC,
            'patronymic' => SORT_ASC,
        ])->all();
    }

    /**
     * @return array
     */
    public function getIndustryData()
    {
        return ['' => 'Без направления'] + CompanyIndustry::find()
                ->where(['company_id' => $this->company->id])
                ->select(['name'])
                ->indexBy('id')
                ->column();
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        return Yii::$app->db->transaction(function ($db) {
            if ($this->_save()) {
                return true;
            }

            if (($t = $db->getTransaction()) && $t->isActive) {
                $t->rollBack();
            }

            return false;
        });
    }

    /**
     * @return bool
     */
    private function _save()
    {
        $this->project->name = $this->name;
        $this->project->status = $this->status;
        $this->project->responsible = $this->responsible;
        $this->project->number = $this->number;
        $this->project->addition_number = $this->addition_number;
        $this->project->industry_id = $this->industry_id;
        $this->project->description = $this->description;
        $this->project->start_date = $this->start_date && ($d = date_create($this->start_date)) ? $d->format('Y-m-d') : null;
        $this->project->end_date = $this->end_date && ($d = date_create($this->end_date)) ? $d->format('Y-m-d') : null;

        if ($this->project->save(false)) {
            $ids = [];
            foreach ($this->projectCustomers as $customer) {
                if ($customer->save()) {
                    $ids[] = $customer->id;
                } else {
                    return false;
                }
            }

            ProjectCustomer::deleteAll([
                'and',
                ['project_id' => $this->project->id],
                ['not', ['id' => $ids]],
            ]);

            return true;
        }

        return false;
    }
}
