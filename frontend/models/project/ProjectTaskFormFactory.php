<?php

namespace frontend\models\project;

use common\components\date\DateHelper;
use common\models\employee\Employee;
use Yii;

/**
 * @deprecated
 */
final class ProjectTaskFormFactory
{
    /**
     * @return ProjectTaskForm
     */
    public function modelCreate(): ProjectTaskForm
    {
        return $this->createForm(ProjectTaskForm::SCENARIO_CREATE);
    }

    /**
     * @param ProjectTask $task
     * @return ProjectTaskForm
     */
    public function modelUpdate(ProjectTask $task): ProjectTaskForm
    {
        $form = $this->createForm(ProjectTaskForm::SCENARIO_UPDATE);
        $form->setAttributes($task->getAttributes(), false);
        $form->task = $task;
        $form->date = DateHelper::format($task->date_begin, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        return $form;
    }

    /**
     * @param string $scenario
     * @return ProjectTaskForm
     */
    private function createForm(string $scenario): ProjectTaskForm
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        return new ProjectTaskForm([
            'scenario' => $scenario,
            'company_id' => $employee->company->id,
            'employee_id' => $employee->id,
            'status' => ProjectTask::STATUS_IN_PROGRESS,
            'date' => date(DateHelper::FORMAT_USER_DATE),
            'time' => date('H:i'),
            'task' => new ProjectTask(),
        ]);
    }
}
