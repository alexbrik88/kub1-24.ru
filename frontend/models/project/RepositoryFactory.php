<?php

namespace frontend\models\project;

use frontend\rbac\permissions\crm\TaskAccess;
use Yii;

/**
 * @deprecated
 */
final class RepositoryFactory
{
    /**
     * @param string|null $project_id
     * @return ProjectTaskRepository
     */
    public function createProjectTaskRepository(?string $project_id = null): ProjectTaskRepository
    {
        return new ProjectTaskRepository([
            'company_id' => Yii::$app->user->identity->company->id,
            'project_id' => $project_id,
            'owner_id' => Yii::$app->user->can(TaskAccess::VIEW_ALL) ? null : Yii::$app->user->identity->id,
        ]);
    }
}
