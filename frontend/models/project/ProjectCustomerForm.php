<?php

namespace frontend\models\project;

use Yii;
use common\models\Agreement;
use common\models\Company;
use common\models\Contractor;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class ProjectCustomerForm
 */
class ProjectCustomerForm extends Model
{
    private Company $company;
    private Project $project;
    private ProjectCustomer $projectCustomer;

    public $customer_id;
    public $agreement_id;
    public $amount;

    public function __construct(Project $project, ?ProjectCustomer $projectCustomer = null, $config = [])
    {
        $this->project = $project;
        $this->company = $project->company;
        $this->projectCustomer = $projectCustomer ?? new ProjectCustomer();

        $this->id = $this->projectCustomer->id;
        $this->customer_id = $this->projectCustomer->customer_id;
        $this->agreement_id = $this->projectCustomer->agreement_id;
        $this->amount = intval($this->projectCustomer->amount) / 100;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount'], 'filter', 'filter' => function ($value) {
                return is_numeric($value) ? round($value, 2) : str_replace(',', '.', $value);
            }],
            [['customer_id'], 'required'],
            [['id', 'customer_id', 'agreement_id'], 'integer'],
            [['amount'], 'number', 'min' => 0, 'max' => 999999999999.99],
            [
                'id',
                'exist',
                'skipOnError' => true,
                'targetClass' => ProjectCustomer::class,
                'filter' => function ($query) {
                    $query->andWHere([
                        'project_id' => $this->project->id,
                    ]);
                }
            ],
            [
                'customer_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Contractor::class,
                'targetAttribute' => ['customer_id' => 'id'],
                'filter' => function ($query) {
                    $query->andWHere([
                        'or',
                        ['company_id' => null],
                        ['company_id' => $this->project->company->id],
                    ]);
                }
            ],
            [
                'agreement_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Agreement::class,
                'targetAttribute' => ['agreement_id' => 'id'],
                'filter' => function ($query) {
                    $query->andWHere([
                        'company_id' => $this->project->company->id,
                    ]);
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Заказчик',
            'agreement_id' => 'Основание',
            'amount' => 'Сумма по договору',
        ];
    }

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->projectCustomer->id = $value ?: null;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->projectCustomer->id;
    }

    /**
     * @inheritdoc
     */
    public function getCustomer()
    {
        return $this->customer_id ? Contractor::find()->andWhere([
            'id' => $this->customer_id,
        ])->andWhere([
            'or',
            ['company_id' => null],
            ['company_id' => $this->company->id],
        ])->one() : null;
    }

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        if ($customer = $this->getCustomer()) {
            return $customer->getShortTitle();
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getAgreementData()
    {
        $agreementData = ['' => ''];
        if ($this->customer_id) {
            $agreementData += ArrayHelper::map($this->company->getAgreements()->andWhere([
                'contractor_id' => $this->customer_id,
            ])->all(), 'id', 'ListItemName');

        }

        return $agreementData;
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->projectCustomer->setIsNewRecord($this->projectCustomer->id ? false : true);
        $this->projectCustomer->project_id = $this->project->id;
        $this->projectCustomer->customer_id = $this->customer_id;
        $this->projectCustomer->agreement_id = $this->agreement_id;
        $this->projectCustomer->amount = round($this->amount * 100);

        return $this->projectCustomer->save(false);
    }
}
