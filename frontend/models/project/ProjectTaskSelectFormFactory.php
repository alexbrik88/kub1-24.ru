<?php

namespace frontend\models\project;

use common\models\employee\Employee;
use Yii;

/**
 * @deprecated
 */
final class ProjectTaskSelectFormFactory
{
    /**
     * @return ProjectTaskSelectForm
     */
    public function createForm(): ProjectTaskSelectForm
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        return new ProjectTaskSelectForm(['company_id' => $employee->company->id]);
    }
}
