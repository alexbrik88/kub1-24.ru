<?php

namespace frontend\models\project;

use common\models\EmployeeCompany;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * @property-read ProjectTask[] $tasks
 */
final class ProjectTaskSelectForm extends Model
{
    /**
     * @var int[]
     */
    public $task_id;

    /**
     * @var int|null
     */
    public $employee_id;

    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int|null
     */
    public $status;

    /**
     * @var ProjectTask[]
     */
    private $_tasks;

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Ответственный',
            'status' => 'Статус',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['task_id'], 'required'],
            [['task_id'], 'each', 'rule' => ['integer']],
            [['employee_id'], 'integer'],
            [
                ['employee_id'],
                'exist',
                'targetClass' => EmployeeCompany::class,
                'targetAttribute' => 'employee_id',
                'filter' => [
                    'company_id' => $this->company_id,
                    'is_working' => true,
                ],
            ],
            [['status'], 'integer'],
            [['status'], 'in', 'range' => array_keys(ProjectTask::STATUS_LIST)],
        ];
    }

    /**
     * @return ProjectTask[]
     */
    public function getTasks(): array
    {
        if ($this->_tasks == null) {
            $this->_tasks = (new ProjectTaskRepository(['company_id' => $this->company_id]))
                ->getQuery()
                ->andWhere(['task_id' => $this->task_id])
                ->all();
        }

        return $this->_tasks;
    }

    /**
     * @return bool
     */
    public function updateModels(): bool
    {
        if (!$this->tasks || !$this->company_id) {
            return false;
        }

        $task_id = ArrayHelper::getColumn($this->tasks, 'task_id');

        if ($this->status) {
            ProjectTask::updateAll(['status' => $this->status], ['task_id' => $task_id]);
            ProjectTask::updateStatuses($this->company_id);
        }

        if ($this->employee_id) {
            ProjectTask::updateAll(['employee_id' => $this->employee_id], ['task_id' => $task_id]);
        }

        return true;
    }
}
