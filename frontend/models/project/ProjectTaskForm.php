<?php

namespace frontend\models\project;

use common\components\date\DateHelper;
use Yii;

final class ProjectTaskForm extends ProjectTask
{
    /** @var string */
    public const SCENARIO_CREATE = 'create';

    /** @var string */
    public const SCENARIO_UPDATE = 'update';

    /**
     * @var string|null
     */
    public $date;

    /**
     * @var ProjectTask
     */
    public $task;

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'task_number' => 'Номер',
            'task_name' => 'Доп. номер',
            'project_id' => 'Проект',
            'employee_id' => 'Ответственный',
            'status' => 'Статус',
            'description' => 'Краткое описание',
            'date' => 'Дата',
            'time' => 'Время',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['task_id'], 'required'],
            [['task_number'], 'required'],
            [['project_id'], 'required'],
            [['date'], 'required'],

            [['employee_id'], 'required'],
            [['status'], 'required'],
            [['description'], 'required'],
            [['time'], 'required'],

            [['task_id'], 'integer'],
            [['task_number'], 'integer', 'min' => 1],
            [['project_id'], 'integer'],

            [['task_name'], 'trim'],
            [['task_name'], 'string', 'max' => 64],
            [['task_name'], 'match', 'pattern' => '#^([a-zA-ZА-ЯЁ0-9_/-]+)$#ui'],

            [['employee_id'], 'integer'],
            [['status'], 'integer'],
            [['status'], 'in', 'range' => array_keys(self::STATUS_LIST)],
            [['date'], 'date', 'format' => 'php:' . DateHelper::FORMAT_USER_DATE],
            [['time'], 'time', 'format' => 'php:H:i'],
            [['description'], 'string'],
        ];
    }

    /**
     * @return bool
     */
    public function saveModel(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $attributes['date_begin'] = DateHelper::format($this->date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
        $this->task->setAttributes($attributes, false);

        if ($this->task->save(false)) {
            Yii::$app->session->setFlash('success', 'Задача успешно сохранена');
            ProjectTask::updateStatuses($this->company_id);

            return true;
        }

        Yii::$app->session->setFlash('success', 'Ошибка сохранения задачи');

        return false;
    }

    /**
     * @inheritDoc
     */
    public function scenarios()
    {
        $attributes = [
            'task_name',
            'task_number',
            '!company_id',
            '!employee_id',
            'project_id',
            'status',
            'description',
            'date',
            'time',
        ];

        return [
            self::SCENARIO_CREATE => $attributes,
            self::SCENARIO_UPDATE => array_merge(['!task_id'], $attributes),
        ];
    }

    /**
     * @param int $project_id
     * @return int
     */
    public static function getNextNumber(int $project_id): int
    {
        $repository = (new RepositoryFactory)->createProjectTaskRepository($project_id);
        $taskNumber = $repository->getQuery()->max('task_number') ?: 0;
        $taskNumber++;

        return $taskNumber;
    }
}
