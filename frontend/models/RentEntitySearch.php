<?php
namespace frontend\models;

use common\models\rent\RentAgreement;
use frontend\components\PageSize;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class RentEntitySearch extends RentEntity
{
    public $status;

    public function rules()
    {
        return [
            [['status'], 'in', 'range' => RentEntity::STATUS],
            [['ownership'], 'in', 'range' => RentEntity::OWNERSHIP_TYPE],
            [['category_id', 'created_by'], 'integer'],
            [['title'], 'safe']
        ];
    }

    public function search($params)
    {
        $this->load($params);
        if (!$this->validate()) {
            foreach ($this->errors as $attribute => $errors) {
                $this->$attribute = null;
            }
        }

        $query = RentEntity::findActual($this->company_id)->with(['category', 'employee'])
            ->filterWhere($this->getAttributes(['ownership', 'category_id', 'created_by']));

        if ($this->title) {
            $query->andWhere(['like', 'title', $this->title]);
        }

        if ($this->status) {
            $date = date('Y-m-d');

            $currentRentsQuery = RentAgreement::find()
                ->where('entity_id = ' . RentEntity::tableName() . '.id')
                ->andWhere(['and',
                    ['<=', RentAgreement::tableName() . '.date_begin', $date],
                    ['>=', RentAgreement::tableName() . '.date_end', $date],
                ]);

            $featureRentsQuery = RentAgreement::find()
                ->where('entity_id = ' . RentEntity::tableName() . '.id')
                ->andWhere(['>=', RentAgreement::tableName() . '.date_begin', $date]);

            switch ($this->status) {
                case RentEntity::STATUS_RETIRED :
                    $query->andWhere([RentEntity::tableName() . '.retired' => new Expression(1)]);
                    break;
                case RentEntity::STATUS_RENT :
                    $query->andWhere(['and',
                        ['exists', $currentRentsQuery],
                        [RentEntity::tableName() . '.retired' => new Expression(0)]
                    ]);
                    break;
                case RentEntity::STATUS_RESERVED :
                    $query->andWhere(['and',
                        ['not', ['exists', $currentRentsQuery]],
                        ['exists', $featureRentsQuery]
                    ]);
                    break;
                case RentEntity::STATUS_FREE :
                    $query->andWhere(['and',
                        ['not', ['exists', $currentRentsQuery]],
                        ['not', ['exists', $featureRentsQuery]]
                    ]);
                    break;
            }
        }

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['id', 'created_at', 'title'],
                'defaultOrder' => ['id' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => PageSize::get()
            ]
        ]);
    }
}
