<?php

namespace frontend\models;

use common\models\Contractor;
use frontend\modules\integration\models\evotor\Employee;
use Yii;
use common\models\product\PriceList;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/**
 * KudirSearch represents the model behind the search form about `common\models\Kudir`.
 */
class PriceListSearch extends PriceList
{
    protected $_searchQuery;

    public $find_by;
    public $status_id;
    public $author_id;

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['find_by', 'status_id', 'author_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function setSearchQuery(ActiveQuery $query)
    {
        $this->_searchQuery = clone $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery()
    {
        return $this->_searchQuery;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $A = static::tableName();

        $query = PriceList::find()->where([
            "$A.company_id" => Yii::$app->user->identity->company->id,
            "$A.is_deleted" => false
        ]);
        $query->andFilterWhere([
            "$A.author_id" => $this->author_id,
            "$A.status_id" => $this->status_id
        ]);
        if ($this->find_by) {
            $query->andFilterWhere(['like', 'name', $this->find_by]);
        }

        $countQuery = clone $query;
        $count = (int) $countQuery->limit(-1)->offset(-1)->orderBy([])->count('*');

        $this->searchQuery = $query;

        return new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'name',
                    'created_at',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getCreators()
    {
        return \common\components\helpers\ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getSearchQuery()
            ->joinWith('author')
            ->groupBy('author_id')
            ->all(), 'author_id', 'author.shortFio'));
    }
}
