<?php
namespace frontend\models\yandexMetrika;

class YandexMetrika {

    const DISABLED_URLS = [
        '/retail/default/index',
        '/analytics/selling-report/index',
        '/analytics/finance-plan',
        '/analytics/finance/profit-and-loss',
        '/cash/order/index' // many-create bug
    ];

    /**
     * @return bool
     */
    public static function disabledByUrl()
    {
        foreach (self::DISABLED_URLS as $disabledUrl) {
            if (0 === strpos(\Yii::$app->request->url, $disabledUrl))
                return true;
        }

        return false;
    }
}