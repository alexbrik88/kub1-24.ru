<?php

namespace frontend\models;

use common\models\employee\Employee;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\EmployeeCompany;
use common\models\employee\EmployeeRole;

/**
 * EmployeeCompanySearch represents the model behind the search form about `common\models\EmployeeCompany`.
 */
class EmployeeCompanySearch extends EmployeeCompany
{
    public $is_owner;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_working', 'employee_role_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmployeeCompany::find()
            ->addSelect(['employee_company.*', 'is_owner' => 'IF({{employee}}.[[id]] = {{company}}.[[owner_employee_id]], 1, 0)'])
            ->alias('employee_company')
            ->joinWith(['employee', 'company', 'employeeRole'])
            ->andWhere(['employee_company.company_id' => $this->company_id])
            ->andWhere(['employee.is_deleted' => false]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'fio' => SORT_ASC,
                ],
                'attributes' => [
                    'fio' => [
                        'asc' => [
                            'employee_company.lastname' => SORT_ASC,
                            'employee_company.firstname' => SORT_ASC,
                            'employee_company.patronymic' => SORT_ASC,
                        ],
                        'desc' => [
                            'employee_company.lastname' => SORT_DESC,
                            'employee_company.firstname' => SORT_DESC,
                            'employee_company.patronymic' => SORT_DESC,
                        ],
                    ],
                    'position',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'employee_company.is_working' => $this->is_working,
            'employee_company.employee_role_id' => $this->employee_role_id,
        ])->andFilterHaving([
            'is_owner' => $this->is_owner,
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getEmployeeRoleArray($companyId = null)
    {
        $companyId = $companyId !== null ? $companyId : Yii::$app->user->identity->company->id;
        $rolesIds = EmployeeCompany::find()->where(['company_id' => $companyId])->select('employee_role_id')->distinct()->column();
        $query = EmployeeRole::find()->actual()->orderBy(['sort' => SORT_ASC]);

        if ($rolesIds)
            $query->andWhere(['id' => $rolesIds]);

        return ArrayHelper::map($query->all(), 'id', 'name');
    }

    /**
     * @return array
     */
    public function getEmployeeStatusArray()
    {
        return [
            '' => 'Все',
            1 => 'Работает',
            0 => 'Уволен',
        ];
    }

    /**
     * @return array
     */
    public function getOwnerArray()
    {
        return [
            '' => 'Все',
            1 => 'Да',
            0 => 'Нет',
        ];
    }
}
