<?php

namespace frontend\models;

use common\models\product\PriceListOrder;
use Yii;
use common\models\product\PriceList;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/**
 * KudirSearch represents the model behind the search form about `common\models\Kudir`.
 */
class PriceListOrderSearch extends PriceListOrder
{
    protected $_searchQuery;

    public $find_by;

    /**
     * @var PriceList
     */
    public $ownerPriceList;

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['find_by', 'product_group_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function setSearchQuery(ActiveQuery $query)
    {
        $this->_searchQuery = clone $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery()
    {
        return $this->_searchQuery;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $A = static::tableName();

        $query = PriceListOrder::find()
            ->joinWith('productGroup')
            ->where([
            "$A.price_list_id" => $this->ownerPriceList->id,
        ]);
        if ($this->find_by) {
            if ($this->ownerPriceList->include_article_column) {
                $query->andFilterWhere(['or' ,
                    ['like', 'name', $this->find_by],
                    ['like', 'article', $this->find_by]
                ]);
            } else {
                $query->andFilterWhere(['like', 'name', $this->find_by]);
            }
        }

        $query->andFilterWhere(['product_group_id' => $this->product_group_id]);

        $countQuery = clone $query;
        $count = (int) $countQuery->limit(-1)->offset(-1)->orderBy([])->count('*');

        $this->searchQuery = $query;

        $sortAttr = $this->ownerPriceList->getSortAttributeName();

        return new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'sort',
                    'production_type',
                    'name',
                    'product_group_id',
                    'product_group.title',
                    'article',
                    'price_for_sell' => [
                        'asc' => [
                            'abs(price_for_sell)' => SORT_ASC,
                        ],
                        'desc' => [
                            'abs(price_for_sell)' => SORT_DESC,
                        ],
                    ],
                    'quantity',
                ],
                'defaultOrder' => [
                    'production_type' => SORT_DESC,
                    $sortAttr => SORT_ASC
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getFilterGroups()
    {
        return \common\components\helpers\ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getSearchQuery()
            ->joinWith('productGroup')
            ->groupBy('product_group.title')
            ->all(), 'productGroup.id', 'productGroup.title'));
    }
}
