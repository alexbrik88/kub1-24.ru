<?php

namespace frontend\models;

use common\components\image\EasyThumbnailImage;
use common\models\Contractor;
use common\models\DynamicModelLabels;
use common\models\product\Product;
use common\models\rent\Attribute;
use common\models\rent\AttributeValue;
use common\models\rent\Entity;
use common\models\rent\RentAgreement;
use yii\base\Exception as ExceptionBase;
use yii\base\InvalidCallException;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * @property RentAgreement[] $rentsActual  Актуальные аренды (с self::DAYS_FROM по self::DAYS_TO)
 * @property RentAgreement   $attachedRent Аренда, связанная с объектом. Связь устанавливается в классе RentSearch
 */
class RentEntity extends Entity
{

    public $attachedRentId;

    const DAYS_FROM = -5;
    const DAYS_TO = 25;

    const SCENARIO_ATTR_IMAGE = 'attr-image';

    const THUMBNAIL_WIDTH = 193; //ширина превью (px)
    const THUMBNAIL_HEIGHT = 136; //высота превью (px)

    /** @var string[] Дополнительные атрибуты */
    public $attr = [];

    public $delete_pic;

    /** @var int[] */
    private $_imageDeleted = [];
    private $_id;

    public static function currentTimestampFrom(): int
    {
        return strtotime(self::currentDateFrom());
    }

    public static function currentTimestampTo(): int
    {
        return strtotime(self::currentDateTo());
    }

    public static function currentDateFrom(): string
    {
        return date('Y-m-d', time() + self::DAYS_FROM * 86400);
    }

    public static function currentDateTo(): string
    {
        return date('Y-m-d', time() + self::DAYS_TO * 86400);
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_ATTR_IMAGE => [
                'id', 'company_id', 'category_id', 'product_id', 'owner_id',
                'title', 'description', 'comment', 'attr', 'delete_pic',
                'count', 'useful_life_in_month', 'archive', 'retired',
                'purchased_at', 'purchased_price', 'created_at', 'created_by'
            ]
        ]);
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [
                'product_id', 'default',
                'when' => function () { return $this->isNewRecord; },
                'value' => function() { return $this->createProduct(); }
            ],

            ['product_id', 'required'],
            ['attr', 'attrValidator']
        ]);
    }

    private function createProduct()
    {
        $product = new Product([
            'company_id' => $this->company_id,
            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            'title' => 'Аренда ' . $this->title,
            'country_origin_id' => 1
        ]);

        $product->save();
        $this->populateRelation('product', $product);

        return $product->id;
    }

    public function markImageDelete(array $indexes)
    {
        foreach ($indexes as $id => $value) {
            if ($value === true || $value === 1 || $value === '1') {
                $this->_imageDeleted[] = (int)$id;
            }
        }
        $this->_imageDeleted = array_unique($this->_imageDeleted);
    }

    public function attrValidator(string $attribute, /** @noinspection PhpUnusedParameterInspection */ $params)
    {
        $data = $rule = [];
        $attributeList = Attribute::find()->where(['category_id' => $this->category_id, 'show' => 1])->all();
        $validator = new DynamicModelLabels(array_map(function ($item) {
            return 'attr-' . $item->id;
        }, $attributeList));
        foreach ($attributeList as $item) {
            /** @var Attribute $item */
            $name = 'attr-' . $item->id;
            $validator->addLabel($name, $item->title);
            $data[$name] = $this->$attribute[$item->id] ?? null;
            if ($item->required === true) {
                $rule[] = [$name, 'required'];
                $validator->addRule($name, 'required');
                switch ($item->type) {
                    case Attribute::TYPE_INTEGER:
                        $rule[] = [$name, 'integer'];
                        break;
                    case Attribute::TYPE_FLOAT:
                        $rule[] = [$name, 'double'];
                        break;
                    case Attribute::TYPE_STRING:
                        $rule[] = [$name, 'string', 'max' => 500];
                        break;
                    case Attribute::TYPE_ENUM:
                        $rule[] = [$name, 'in', 'range' => $item->data];
                        break;
                    case Attribute::TYPE_SET:
                        $rule[] = [$name, 'each', 'rule' => ['in', $item->data]];
                }
            }
        }
        $this->attr = $data;
        $validator->setAttributes($data);
        if ($validator->validate() === true) {
            return true;
        }
        $this->addErrors($validator->errors);
        return false;
    }

    /**
     * Сохраняет дополнительные атрибуты в таблицу rent_attribute_value
     * @inheritDoc
     * @throws Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = $this->getDb()->beginTransaction();
        $this->_id = (int)$this->id;

        if (parent::save($runValidation, $attributeNames) === false) {
            return false;
        }

        $transaction->commit();
        return true;
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     * @throws ExceptionBase
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->scenario !== self::SCENARIO_ATTR_IMAGE) {
            return;
        }

        if (!$this->isNewRecord) {
            AttributeValue::deleteAll(['entity_id' => $this->id]);
        }

        $attributes = [];
        foreach ($this->attr as $id => $item) {
            $attr = new AttributeValue();
            $attr->entity_id = $this->id;
            $attr->attribute_id = substr($id, 5);
            $attr->value = $item;
            $attr->save();
            $attributes[] = $attr;
        }
        $this->populateRelation('attributeValues', $attributes);

        //Удалить помеченные изображения
        foreach ($this->_imageDeleted as $item) {
            $this->deletePicture($item);
        }

        $path = $this->uploadPath($this->_id, true);
        $files = file_exists($path) ? FileHelper::findFiles($path) : [];
        $path = $this->uploadPath() . DIRECTORY_SEPARATOR;
        if (file_exists($path) === false) {
            FileHelper::createDirectory($path);
        }
        if ($files) {
            foreach ($files as $item) {
                rename($item, $path . basename($item));
            }
        }
    }

    /**
     * Загружает изображение во временный файл
     * @param string $fileName Имя файла (pic1/pic2/pic3/pic4)
     * @return string
     * @throws ExceptionBase
     * @throws InvalidCallException
     */
    public function uploadImage(string $fileName)
    {
        $uploadPath = $this->uploadPath((int)$this->id, true);
        if (in_array($fileName, ['pic1', 'pic2', 'pic3', 'pic4']) === false) {
            throw new ExceptionBase('Illegal file name');
        }
        $file = UploadedFile::getInstanceByName($fileName);
        if ($file === null) {
            throw new ExceptionBase('Ошибка загрузки файла');
        }
        if (in_array($file->type, ['image/jpeg', 'image/png']) === false) {
            throw new InvalidCallException('Допустимый формат файла: jpg, jpeg, png');
        }
        $filename = $uploadPath . DIRECTORY_SEPARATOR . $fileName . '.' . $file->extension;
        if (file_exists($uploadPath) === false) {
            FileHelper::createDirectory($uploadPath);
        }
        $file->saveAs($filename);
        return $filename;
    }

    /**
     * Возвращает URL превью
     * @param int $index  Порядковый номер изображения
     * @param int $width  Ширина (px)
     * @param int $height Высота (px)
     * @param int $mode   Режим масштабирования
     * @return string|null
     */
    public function pictureThumb(int $index, int $width = self::THUMBNAIL_WIDTH, int $height = self::THUMBNAIL_HEIGHT, $mode = EasyThumbnailImage::THUMBNAIL_INSET)
    {
        if (!$this->id) {
            return null;
        }
        $path = $this->uploadPath() . DIRECTORY_SEPARATOR;
        foreach (file_exists($path) ? FileHelper::findFiles($path) : [] as $file) {
            $ext = explode('.', basename($file));
            $name = reset($ext);
            if ($name === "pic{$index}") {
                return EasyThumbnailImage::thumbnailSrc($file, $width, $height, $mode);
            }
        }
        return null;
    }

    /**
     * Удаляет изображение объекта
     * @param int $index Порядковый номер изображения
     */
    public function deletePicture(int $index)
    {
        $path = $this->uploadPath() . DIRECTORY_SEPARATOR;
        $files = file_exists($path) ? FileHelper::findFiles($path, ['recursive' => false]) : [];
        if ($files) {
            foreach ($files as $file) {
                $name_ext = explode('.', basename($file));
                $name = reset($name_ext);
                if ($name === "pic{$index}") {
                    @unlink($file);
                }
            }
        }
    }

    /**
     * Проверяет находится ли в аренде объект
     * @param string $date Дата в формате Y-m-d
     * @return RentAgreement|null Объект если в аренде, NULl, если в этот день не в аренде
     */
    public function isRented(string $date)
    {
        if (!$this->attachedRentId) {
            return null;
        }
        $rent = $this->attachedRent;
        $date = strtotime($date);
        if (strtotime($rent->date_end) >= $date && strtotime($rent->date_begin) <= $date) {
            return $rent;
        }
        return null;
    }

    /**
     * @return ActiveQuery
     */
    public function getRentsActual(): ActiveQuery
    {
        $t = time();
        return $this->getRents()
            ->andOnCondition(['and',
                ['<=', RentAgreement::tableName() . '.date_begin', date('Y-m-d', $t + 86400 * self::DAYS_TO)],
                ['>=', RentAgreement::tableName() . '.date_end', date('Y-m-d', $t + 86400 * self::DAYS_FROM)]
            ]);
    }

    /**
     * @return \common\models\ContractorQuery
     */
    public function getContractorsActual()
    {
        $rangeExpr = new Expression('now() between date_begin abd date_end');
        $selectExpr = new Expression('distinct contractor_id');
        return Contractor::find()->where(['id' => $this->getRents()->where($rangeExpr)->select($selectExpr)->column()]);
    }

    /**
     * @return ActiveQuery
     */
    public function getAttachedRent(): ActiveQuery
    {
        return $this->hasOne(RentAgreement::class, ['id' => 'attachedRentId']);
    }
}