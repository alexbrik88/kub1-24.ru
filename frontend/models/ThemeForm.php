<?php
namespace frontend\models;

use common\models\employee\Employee;
use yii\base\Model;
use Yii;

/**
 * ThemeForm
 */
class ThemeForm extends Model
{
    public $wide;

    /**
     * @var \common\models\employee\Employee
     */
    private $_user;

    /**
     * @param $user Employee
     * @param $config array
     */
    public function __construct(Employee $user, $config = [])
    {
        $this->_user = $user;
        $this->wide = $user->config->theme_wide ?? 0;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wide'], 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function getWideItems()
    {
        return [
            '0' => 'Узкая тема',
            '1' => 'Широкая тема',
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function save()
    {
        if ($this->validate()) {
            $this->_user->config->updateAttributes([
                'theme_wide' => $this->wide,
            ]);

            return true;
        }

        return false;
    }
}
