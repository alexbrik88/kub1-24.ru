<?php

namespace frontend\models;

use common\models\address\AddressClassifier;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\FsspItem;
use common\models\FsspTask as FsspDataCommon;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\Exception;

class FsspTask extends FsspDataCommon
{

    const URL = 'https://api-ip.fssprus.ru/api/v1.0/';

    /**
     * Ставит задачу загрузки данных по контрагенту
     * @param string $entityType
     * @param int    $entityId
     * @return self
     * @throws InvalidArgumentException
     */
    public static function createOrGet(string $entityType, int $entityId): self
    {
        $self = self::findByEntity($entityType, $entityId);
        if ($self === null) {
            $self = new self($entityType, $entityId);
        }
        return $self;
    }

    /**
     * @param string $entityType
     * @param int    $entityId
     * @throws InvalidArgumentException
     */
    public function __construct(string $entityType = null, int $entityId = null)
    {
        parent::__construct();
        if ($entityType !== null) {
            $this->setEntity($entityType, $entityId);
        }
    }

    /**
     * Перезапускает процесс загрузки данных
     * @param CompanyType $type
     * @param string      $name
     * @param string[]    $address
     * @return bool
     * @throws InvalidConfigException
     */
    public function restart(CompanyType $type, string $name, array $address)
    {
        if ($type->id == CompanyType::TYPE_IP) {
            $result = $this->_restartNP($name, $address);
        } else {
            $result = $this->_restartLE($name, $address, $type);
        }
        if ($result === false) {
            $this->status = self::STATUS_ERROR;
            return false;
        }
        $this->status = self::STATUS_REQUEST;
        $this->date = date('Y-m-d H:i:s');
        return $this->save();
    }

    /**
     * @param string      $name
     * @param CompanyType $type
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function loadTaskData(string $name, CompanyType $type)
    {
        $result = self::_request('result', ['task' => $this->task]);
        if ($result['task_end'] === null || $result['result'][0]['status'] == 1 || $result['result'][0]['status'] == 2) {
            $this->status = self::STATUS_REQUEST;
            return;
        }
        if ($this->id > 0) {
            FsspItem::deleteAll(['task_id' => $this->id]);
        }
        $fsspList = [];
        $length = mb_strlen($name);
        foreach ($result['result'] as $item) {
            if ($result['status'] == 3) {
                $this->status = self::STATUS_ERROR;
                continue;
            }
            foreach ($item['result'] as $subitem) {
                if ($type->id != CompanyType::TYPE_IP && mb_substr($subitem['name'], 0, $length) !== $name) {
                    continue;
                }
                $exe = explode(' от ', $subitem['exe_production']);
                $endDate = mb_strpos($subitem['ip_end'], ',');
                $endBase = trim(mb_substr($subitem['ip_end'], $endDate + 1));
                $endDate = trim(mb_substr($subitem['ip_end'], 0, $endDate));
                $fsspItem = new FsspItem();
                $fsspItem->load([
                    'task_id' => $this->id,
                    'name' => $subitem['name'],
                    'production_number' => $exe[0],
                    'production_date' => date('Y-m-d', strtotime($exe[1])),
                    'subject' => $subitem['subject'],
                    'detail' => $subitem['details'],
                    'department' => $subitem['department'],
                    'bailiff' => $subitem['bailiff'],
                    'end_date' => $endDate,
                    'end_base' => $endBase
                ], '');
                $fsspItem->save();
                $fsspList[] = $fsspItem;
            }
        }
        $this->populateRelation('items', $fsspList);
        $this->status = self::STATUS_SUCCESS;
        $this->date = date('Y-m-d H:i:s');
        $this->save(false, ['status', 'date']);
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    private static function _token(): string
    {
        $token = Yii::$app->params['fsspToken'] ?? null;
        if (!$token) {
            throw new InvalidConfigException('You have to set Token FSSP API before execute API requests');
        }
        return $token;
    }

    /**
     * @param string     $method
     * @param array      $get
     * @param array|null $post
     * @return mixed
     * @throws InvalidConfigException
     * @throws Exception
     */
    private static function _request(string $method, array $get, array $post = null)
    {
        $client = new Client();
        $request = $client->createRequest();
        $request->setHeaders([
            'Accept' => 'application/json',
        ]);
        $request->setMethod($post === null ? 'GET' : 'POST');
        if ($post === null) {
            $get['token'] = self::_token();
        } else {
            $post = [
                'token' => self::_token(),
                'request' => $post
            ];
        }
        $request->setFullUrl(self::URL . $method . '?' . http_build_query($get));
        $request->setFormat(Client::FORMAT_JSON);
        $request->setData($post);
        $response = $request->send();
        if ($response->getStatusCode() !== '200') {
            throw new Exception('Cannot get answer from FSSP API', $response->getStatusCode());
        }
        $response = json_decode($response->content, true);
        if (is_array($response) === false) {
            throw new Exception('Invalid FSSP API response');
        }
        if ($response['status'] !== 'success') {
            throw  new Exception($response['exception'], $response['code']);
        }
        return $response['response'];
    }

    /**
     * Перезапуск задачи для юридических лиц
     * @param string      $name    Название компании
     * @param string[]    $address Адреса
     * @param CompanyType $type    Тип юридического лица
     * @return bool
     * @throws InvalidConfigException
     */
    private function _restartLE(string $name, array $address, CompanyType $type)
    {
        $postalCode = self::_postalCode($address);
        if ($postalCode === null) {
            return false;
        }
        $region = AddressClassifier::regionByPostal($postalCode);
        if ($region === null) {
            return false;
        }
        $request = [
            [
                'type' => 2,
                'params' => [
                    'name' => $type->name . ' ' . $name,
                    'address' => $postalCode,
                    'region' => $region
                ]
            ]
        ];
        $request[] = [
            'type' => 2,
            'params' => [
                'name' => mb_strtoupper($type->name_full) . ' ' . $name,
                'address' => $postalCode,
                'region' => $region
            ]
        ];
        try {
            $this->task = self::_request('search/group', [], $request)['task'];
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Перезапуск задачи для физических лиц
     * @param string   $fio     Ф.И.О
     * @param string[] $address Список адресов
     * @return bool
     * @throws InvalidConfigException
     */
    private function _restartNP(string $fio, array $address)
    {
        $postalCode = self::_postalCode($address);
        if ($postalCode === null) {
            return false;
        }
        $region = AddressClassifier::regionByPostal($postalCode);
        if ($region === null) {
            return false;
        }
        $name = explode(' ', $fio);
        if (count($name) !== 3) {
            return false;
        }
        try {
            $this->task = self::_request('search/physical', [
                'region' => $region,
                'firstname' => $name[1],
                'secondname' => $name[2],
                'lastname' => $name[0],
                //'birthdate' => '' //жаль, но даты рождения у нас нет
            ])['task'];
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param string[] $address
     * @return int|null
     */
    private static function _postalCode(array $address)
    {
        foreach ($address as $item) {
            if ($item && preg_match('~\b[\d]{6}\b~', $item, $postalCode) === 1) {
                return (int)$postalCode[0];
            }
        }
        return null;
    }

}
