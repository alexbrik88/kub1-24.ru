<?php

namespace frontend\models;

use common\components\validators\PhoneValidator;
use common\models\company\RegistrationPageType;
use common\models\employee\Employee;
use Yii;

/**
 * Class AuthSignupForm
 */
class AuthSignupForm extends RegistrationForm
{
    const SCENARIO_EMAIL = 'email';
    const SCENARIO_LOGIN = 'login';
    const SCENARIO_CREATE = 'create';

    public $rememberMe = true;

    private $_user = false;

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_EMAIL => [
                'email',
            ],
            self::SCENARIO_LOGIN => [
                'email',
                'password',
                'rememberMe',
            ],
            self::SCENARIO_CREATE => [
                'email',
                'companyType',
                'promoCode',
                'phone',
                'checkrules',
                'taxationTypeOsno',
                'taxationTypeUsn',
                'taxationTypeEnvd',
                'taxationTypePsn',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'companyType', 'password'], 'required'],
            [['email'], 'email'],
            [
                ['email'], 'unique',
                'targetClass' => Employee::className(),
                'targetAttribute' => 'email',
                'filter' => ['is_deleted' => false],
                'on' => [self::SCENARIO_CREATE],
            ],
            [['checkrules'], 'required', 'message' => 'Для продолжения необходимо принять условия'],
            [['taxationTypeOsno', 'taxationTypeUsn', 'taxationTypeEnvd', 'taxationTypePsn'], 'boolean'],
            [['taxationTypeOsno'], 'taxationTypeValidation'],
            [['phone'], PhoneValidator::className()],
            [
                ['companyType'], 'in', 'range' => self::$typeIds,
                'message' => 'Форма компании не выбрана.',
            ],
            [['promoCode'], 'string'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный пользователь или пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate() && ($user = $this->getUser())) {
            return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return Employee|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Employee::findIdentityByLogin($this->email);
        }

        return $this->_user;
    }
}
