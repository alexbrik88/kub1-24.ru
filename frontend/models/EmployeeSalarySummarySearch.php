<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\employee\EmployeeSalarySummary;

/**
 * EmployeeSalarySummarySearch represents the model behind the search form about `common\models\employee\EmployeeSalarySummary`.
 */
class EmployeeSalarySummarySearch extends EmployeeSalarySummary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmployeeSalarySummary::find();

        // add conditions that should always apply here
        $query->andWhere([
            'company_id' => $this->company_id,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['date' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        return $dataProvider;
    }
}
