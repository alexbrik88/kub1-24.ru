<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.06.2018
 * Time: 15:55
 */

namespace frontend\models;


use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\document\Invoice;
use common\models\service\InvoicePaymentHelper;
use common\models\service\Payment;
use common\models\service\PaymentOrder;
use common\models\service\PaymentType;
use common\models\service\ServiceInvoiceTariff;
use frontend\models\log\LogHelper;
use frontend\modules\documents\components\InvoiceHelper;
use yii\base\Exception;
use yii\base\Model;
use Yii;
use yii\db\Connection;

/**
 * Class PaymentInvoiceForm
 * @package frontend\models
 */
class PaymentInvoiceForm extends Model
{
    /**
     * @var
     */
    public $tariffId;
    /**
     * @var
     */
    public $paymentTypeId;
    /**
     * @var
     */
    public $companyId;

    /**
     * @var Company
     */
    protected $_company;
    /**
     * @var ServiceInvoiceTariff
     */
    protected $_tariff;
    /**
     * @var Payment
     */
    protected $_payment;
    /**
     * @var Invoice
     */
    protected $_invoice;
    /**
     * @var Invoice
     */
    protected $_out_invoice;
    /**
     * @var string
     */
    protected $_pdfContent;

    /**
     * PaymentInvoiceForm constructor.
     * @param Company|null $company
     * @param array $config
     */
    public function __construct(Company $company, $config = [])
    {
        $this->_company = $company;
        $this->companyId = $company->id;

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['tariffId', 'paymentTypeId'], 'required'],
            [
                ['tariffId'], 'in',
                'range' => [
                    ServiceInvoiceTariff::ONE_INVOICE,
                    ServiceInvoiceTariff::THREE_INVOICES,
                ],
                'message' => 'Тарифный план не указан.',
            ],
            [
                ['paymentTypeId'], 'in',
                'range' => [
                    PaymentType::TYPE_ONLINE,
                ],
                'message' => 'Способ оплаты не указан.',
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'tariffId' => 'Тариф',
            'paymentTypeId' => 'Способ оплаты',
            'companyId' => 'На какую компанию выставить счет',
        ];
    }

    /**
     * @return Company
     */
    public function afterValidate()
    {
        $this->_tariff = ServiceInvoiceTariff::findOne($this->tariffId);

        parent::afterValidate();
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return ServiceInvoiceTariff
     */
    public function getTariff()
    {
        return $this->_tariff;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->_payment;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * @return Invoice
     */
    public function getOutInvoice()
    {
        return $this->_out_invoice;
    }

    /**
     * @return string|null
     */
    public function getPdfContent()
    {
        return $this->_pdfContent;
    }

    /**
     * @return Payment
     */
    public function getNewPayment()
    {
        $orderArray = [];
        $payment = new Payment([
            'company_id' => $this->company->id,
            'sum' => 0,
            'type_id' => $this->paymentTypeId,
            'invoice_tariff_id' => $this->tariff->id,
            'is_confirmed' => false,
            'payment_for' => Payment::FOR_ADD_INVOICE,
        ]);
        $paymentOrder = new PaymentOrder([
            'company_id' => $this->company->id,
            'invoice_tariff_id' => $this->tariff->id,
            'price' => $this->tariff->total_amount,
            'discount' => 0,
            'sum' => $this->tariff->total_amount,
        ]);
        $paymentOrder->populateRelation('company', $this->company);
        $paymentOrder->populateRelation('payment', $payment);
        $orderArray[] = $paymentOrder;

        $payment->sum += $paymentOrder->sum;
        $payment->sum .= '';
        $payment->populateRelation('orders', $orderArray);

        return $payment;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function createPayment()
    {
        if (!$this->company || !$this->tariff) {
            throw new Exception("The form configure error", 1);
        }
        $payment = $this->getNewPayment();
        if (!$payment->save()) {
            Yii::$app->session->setFlash('error', 'Ошибка при создании платежа. Попробуйте ещё раз');

            return false;
        }
        foreach ($payment->orders as $order) {
            $order->payment_id = $payment->id;
            if (!$order->save()) {
                Yii::$app->session->setFlash('error', 'Ошибка при создании платежа. Попробуйте ещё раз.');

                return false;
            }
        }
        $this->_payment = $payment;

        $invoice = InvoicePaymentHelper::getInvoice($this->payment, $this->tariff, $this->company);
        if (!$invoice) {
            Yii::$app->session->setFlash('error', 'Ошибка при создании счёта. Попробуйте ещё раз.');

            return false;
        }
        InvoiceHelper::afterSave($invoice->id);
        $this->_invoice = $invoice;

        return true;
    }

    /**
     * @return PdfRenderer
     */
    public function getInvoiceRenderer()
    {
        return Invoice::getRenderer(null, $this->invoice, PdfRenderer::DESTINATION_STRING);
    }

    /**
     * @return bool|mixed
     * @throws \Throwable
     */
    public function makePayment()
    {
        if (!$this->validate()) {
            return false;
        }
        $useTransaction = LogHelper::$useTransaction;
        LogHelper::$useTransaction = false;
        $created = Yii::$app->db->transaction(function (Connection $db) {
            if ($this->createPayment()) {
                return true;
            }
            $db->transaction->rollBack();

            return false;
        });
        LogHelper::$useTransaction = $useTransaction;

        return $created;
    }
}
