<?php

namespace frontend\models;

use common\models\document\Invoice;
use common\models\document\Order;
use common\models\product\Product;
use Yii;
use yii\base\Model;
use yii\data\SqlDataProvider;

class SellingSearch extends Product
{
    public $ioType;
    public $contractorId;
    public $sellingMonthNumber;
    public $sellingYearNumber;
    public $productTitle;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'sellingMonthNumber',
                    'sellingYearNumber',
                    'productTitle'
                ],
                'safe',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');

        $dateStr = "{$this->sellingYearNumber}.{$this->sellingMonthNumber}.01";
        $date1 = date_create_from_format('Y.m.d', $dateStr) ? : date_create('first day of this month');
        $date2 = clone($date1);
        $from = $date1->modify('-11 month')->format('Y-m-01');
        $till = $date2->modify('last day of this month')->format('Y-m-d');

        $query = self::find()->joinWith([
            'orders.invoice',
            'productUnit',
        ], false)->select([
            'product_id' => 'product.id',
            'product_title' => 'product.title',
            'productUnitTitle' => 'product_unit.name',
        ])->distinct()->andWhere([
            'product.company_id' => $this->company_id,
            'invoice.type' => $this->ioType,
            'invoice.contractor_id' => $this->contractorId,
            'invoice.is_deleted' => false,
        ])->andWhere([
            'between',
            'invoice.document_date',
            $from,
            $till,
        ])->orderBy('product.title');

        if ($this->productTitle) {
            $query->andWhere(['like', 'product.title', '%' . $this->productTitle . '%', false]);
        }

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $dataProvider;
    }
}
