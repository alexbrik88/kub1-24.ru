<?php

namespace frontend\models;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\rent\Category;
use common\models\rent\Entity;
use common\models\rent\RentAgreement;
use frontend\components\PageSize;
use frontend\components\StatisticPeriod;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class RentAgreementSearch
 */
class RentAgreementSearch extends RentAgreement
{
    public $category_id;
    public $entity_id;
    public $keyword;
    public $status;

    private Company $_company;
    private ?Entity $_entity;
    private $_query;

    /**
     * @inheritdoc
     */
    public function __construct(Company $company, ?Entity $entity = null, $conf = [])
    {
        $this->_company = $company;
        $this->_entity = $entity;

        parent::__construct($conf);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function rules()
    {
        return [
            [['entity_id', 'category', 'contractor_id', 'document_author_id', 'category_id', 'keyword', 'status'], 'safe'],
        ];
    }

    public function search(array $params): ActiveDataProvider
    {
        $period = StatisticPeriod::getSessionPeriod();

        $query = RentAgreement::find()->joinWith([
            'contractor',
            'entity',
            'contractor',
        ])->andWhere([
            RentAgreement::tableName() . '.company_id' => $this->_company->id,
            RentAgreement::tableName() . '.deleted' => false,
        ])->andWhere([
            'between',
            RentAgreement::tableName() . '.document_date',
            $period['from'],
            $period['to'],
        ]);

        if ($this->_entity) {
            $query->andWhere([
                Entity::tableName() . '.id' => $this->_entity->id,
            ]);
        }

        $query->addSelect([
            RentAgreement::tableName() . '.*',
            'days_left' => new Expression('datediff(' . RentAgreement::tableName() . '.date_end, now())'),
            'actual' => new Expression('if(' . RentAgreement::tableName() . '.date_end >= now(), 1, 0)'),
            'IF (date_begin <= CURRENT_DATE() and CURRENT_DATE() <= date_end, 1, 2) as status',
        ]);

        $this->load($params);

        $query->andFilterWhere([
            Entity::tableName() . '.id' => $this->entity_id,
            Entity::tableName() . '.category_id' => $this->category_id,
            RentAgreement::tableName() . '.contractor_id' => $this->contractor_id,
            RentAgreement::tableName() . '.document_author_id' => $this->document_author_id,
        ]);

        $query->andFilterWhere([
            'like', Entity::tableName() . '.title', $this->keyword,
        ]);

        if ($this->status) {
            $date = date('Y-m-d');

            switch ($this->status) {
                case RentAgreement::STATUS_PROCEED :
                    $query->andWhere([
                        'and',
                        ['<=', RentAgreement::tableName() . '.date_begin', $date],
                        ['>=', RentAgreement::tableName() . '.date_end', $date],
                    ]);
                    break;
                case RentAgreement::STATUS_COMPLETED :
                    $query->andWhere([
                        '>=', RentAgreement::tableName() . '.date_end', $date
                    ]);
                    break;
            }
        }

        $this->_query = $query;

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_number' => [
                        'asc' => ['[[document_number]] * 1' => SORT_ASC],
                        'desc' => ['[[document_number]] * 1' => SORT_DESC],
                    ],
                    'created_at',
                    'date_begin',
                    'date_end',
                    'days_left',
                    'amount',
                ],
                'defaultOrder' => ['document_number' => SORT_DESC],
            ],
            'pagination' => [
                'pageSize' => PageSize::get()
            ]
        ]);
    }

    public function getQuery():ActiveQuery
    {
        return $this->_query;
    }


    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Базовый запрос для вкладки "календарь"
     * @return ActiveQuery
     */
    private function _queryCalendar(): ActiveQuery
    {
        $query = RentEntity::findActual($this->_company->id)->joinWith(['rents' => function($query) {
            /** @var ActiveQuery $query */
            $query->orderBy([
                RentAgreement::tableName() . '.date_begin' => SORT_ASC,
                RentAgreement::tableName() . '.date_end' => SORT_ASC
            ]);
            $query->andOnCondition([RentAgreement::tableName() . '.deleted' => false]);

            if ($this->calendarMode == self::CALENDAR_MODE_DAY) {
                
            } else {
                $query->andOnCondition(['or',
                    ['between', RentAgreement::tableName() . '.date_begin', $this->calendarRange[0], $this->calendarRange[1]],
                    ['between', RentAgreement::tableName() . '.date_end', $this->calendarRange[0], $this->calendarRange[1]],
                    ['and', ['<', RentAgreement::tableName() . '.date_begin', $this->calendarRange[0]], ['>', RentAgreement::tableName() . '.date_end', $this->calendarRange[1]]]
                ]);
            }
        }]);

        return $query->groupBy(RentEntity::tableName() . '.id');
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Базовый запрос для вкладки "аренда"
     * @return ActiveQuery
     */
    private function _queryList(): ActiveQuery
    {
        $query = RentAgreement::findByFilter($this->_company->id, $this->entity, $this->contractor, $this->employee);

        $query->joinWith(['employee', 'contractor'], true);

        $query->addSelect([
            RentAgreement::tableName() . '.*',
            'days_left' => new Expression('datediff(' . RentAgreement::tableName() . '.date_end, now())'),
            'actual' => new Expression('if(' . RentAgreement::tableName() . '.date_end >= now(), 1, 0)'),
            'IF (date_begin <= CURRENT_DATE() and CURRENT_DATE() <= date_end, 1, 2) as status',
        ]);

        return $query;
    }

    public function getHasFilters() {
        return $this->category_id || $this->entity_id || $this->contractor_id || $this->document_author_id;
    }

    public function entityList()
    {
        if (!$this->_query) {
            $this->search([]);
        }

        return (clone $this->getQuery())
            ->select([Entity::tableName() . '.title'])
            ->indexBy(Entity::tableName() . '.id')
            ->column();
    }

    public function categoryList()
    {
        return Category::flatList();
    }

    public function entityActualList()
    {
        return ArrayHelper::map(Entity::findActual($this->_company->id)->select(['id', 'title'])->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    public function contractorFilterList()
    {
        $query = Contractor::find()
            ->byCompany($this->_company->id)
            ->byDeleted(false)
            ->byStatus(Contractor::ACTIVE)
            ->byContractor(Contractor::TYPE_CUSTOMER);

        $contractors = [];

        /** @var Contractor $contractor */
        foreach ($query->all() as $contractor) {
            $contractors[$contractor->id] = $contractor->getNameWithType();
        }

        return $contractors;
    }

    public function employeeList()
    {
        $query = EmployeeCompany::find()->where([
            'company_id' => $this->_company->id,
            'is_working' => true,
        ])->orderBy([
            'lastname' => SORT_ASC,
            'firstname' => SORT_ASC,
            'patronymic' => SORT_ASC,
        ]);

        return ArrayHelper::map($query->all(), 'employee_id', 'shortFio');
    }
}
