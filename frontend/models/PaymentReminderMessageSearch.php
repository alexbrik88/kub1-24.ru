<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.05.2018
 * Time: 6:40
 */

namespace frontend\models;


use common\components\helpers\ArrayHelper;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageCategory;
use Yii;
use yii\data\ActiveDataProvider;
use common\models\employee\Employee;

/**
 * Class PaymentReminderMessageSearch
 * @package frontend\models
 */
class PaymentReminderMessageSearch extends PaymentReminderMessage
{
    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $query = PaymentReminderMessage::find()->andWhere(['company_id' => $user->company->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'number',
                ],
                'defaultOrder' => [
                    'number' => SORT_ASC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere(['category_id' => $this->category_id]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getCategoryFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(PaymentReminderMessageCategory::find()->all(), 'id', 'name'));
    }
}