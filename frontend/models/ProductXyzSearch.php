<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.12.2017
 * Time: 18:44
 */

namespace frontend\models;


use common\models\document\Invoice;
use common\models\document\Order;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\product\ProductStore;
use DateTime;
use php_rutils\RUtils;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ProductXyzSearch
 * @package frontend\models
 *
 * Getters
 * @property DateTime $dateTo
 * @property DateTime $dateFrom
 * @property integer $period
 * @property string $periodName
 * @property array $periodItems
 * @property array $monthArray
 * @property array $monthColumns
 * @property array $monthAttributes
 * @property array $headerTableData
 */
class ProductXyzSearch extends Product
{
    const THIS_MONTH = 1;
    const THIS_QUARTER = 2;
    const THIS_YEAR = 3;
    const PAST_MONTH = 4;
    const PAST_QUARTER = 5;
    const PAST_YEAR = 6;

    const DEFAULT_PERIOD = self::THIS_MONTH;

    const GROUP_X = 'X';
    const GROUP_Y = 'Y';
    const GROUP_Z = 'Z';

    public $groupArray = [
        self::GROUP_X => self::GROUP_X,
        self::GROUP_Y => self::GROUP_Y,
        self::GROUP_Z => self::GROUP_Z,
    ];

    /**
     * @var
     */
    public $group;

    /**
     * @var
     */
    public $product_title;

    /**
     * @var null
     */
    protected $_dateFrom = null;
    /**
     * @var null
     */
    protected $_dateTo = null;
    /**
     * @var array
     */
    protected $_period = [];
    /**
     * @var array
     */
    protected $_monthArray = [];
    /**
     * @var array
     */
    protected $_headerTableData = [];

    /**
     * @var array
     */
    public static $periodArray = [
        self::THIS_MONTH => 'Этот месяц',
        self::THIS_QUARTER => 'Этот квартал',
        self::THIS_YEAR => 'Этот год',
        self::PAST_MONTH => 'Прошлый месяц',
        self::PAST_QUARTER => 'Прошлый квартал',
        self::PAST_YEAR => 'Прошлый год',
    ];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['group', 'product_title'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function search($params = [])
    {
        $result = [];

        $this->load($params);

        $products = $this->getBaseQuery()->asArray()->all();

        foreach ($products as $productData) {
            $productID = $productData['product_id'];
            if (!isset($result[$productID])) {
                $result[$productID] = [
                    'product_id' => $productData['product_id'],
                    'product_title' => $productData['product_title'],
                    'product_count' => $productData['product_count'],
                ];
                foreach ($this->monthArray as $oneMonth) {
                    $result[$productID]['month-' . $oneMonth] = 0;
                }
                $result[$productID]['month-' . $productData['monthNumber']] += $productData['count'];
                $result[$productID]['totalCount'] = 0;
            } else {
                $result[$productID]['month-' . $productData['monthNumber']] += $productData['count'];
            }
            $result[$productID]['totalCount'] += $productData['count'];
        }
        $monthCount = count($this->monthArray);
        $this->_headerTableData = [
            self::GROUP_X => [
                'countProductTypes' => 0,
                'countProductInStock' => 0,
            ],
            self::GROUP_Y => [
                'countProductTypes' => 0,
                'countProductInStock' => 0,
            ],
            self::GROUP_Z => [
                'countProductTypes' => 0,
                'countProductInStock' => 0,
            ],
        ];
        foreach ($result as $data) {
            $productID = $data['product_id'];
            $result[$productID]['averageCount'] = $result[$productID]['totalCount'] / $monthCount;
            $result[$productID]['squareSum'] = 0;
            foreach ($data as $key => $value) {
                if (in_array($key, ['product_count', 'product_id', 'product_title', 'totalCount', 'squareSum', 'averageCount'])) {
                    continue;
                }
                $result[$productID]['squareSum'] += pow(($value - $result[$productID]['averageCount']), 2);
            }
            $standardDeviation = $result[$productID]['squareSum'] / (($monthCount - 1) ? ($monthCount - 1) : 1);
            $coefficientVariation = round($standardDeviation / ($result[$productID]['averageCount'] ? $result[$productID]['averageCount'] : 1), 2);
            $result[$productID]['coefficientVariation'] = $coefficientVariation;
            if ($coefficientVariation > 25) {
                $group = self::GROUP_Z;
            } elseif ($coefficientVariation < 10) {
                $group = self::GROUP_X;
            } else {
                $group = self::GROUP_Y;
            }
            $result[$productID]['group'] = $group;

            $this->_headerTableData[$group]['countProductTypes'] += 1;
            $this->_headerTableData[$group]['countProductInStock'] += $result[$productID]['product_count'];

            if ($this->group) {
                if ($this->group !== $group) {
                    unset($result[$productID]);
                }
            }
            if ($this->product_title) {
                if ((int)$this->product_title != (int)$productID) {
                    unset($result[$productID]);
                }
            }
        }

        $sortAttributes = $this->monthAttributes;
        array_push($sortAttributes, 'coefficientVariation');

        return new ArrayDataProvider([
            'allModels' => $result,
            'sort' => [
                'attributes' => $sortAttributes,
            ],
        ]);
    }

    /**
     * @param integer $value
     */
    public function setPeriod($value)
    {
        $this->_period = in_array($value *= 1, array_keys(self::$periodArray)) ? $value : self::DEFAULT_PERIOD;

        switch ($this->_period) {
            case self::THIS_QUARTER:
                $quarter = ceil(date('n') / 3);
                $this->_dateFrom = DateTime::createFromFormat('n', $quarter * 3 - 2)->modify('first day of this month');
                $this->_dateTo = DateTime::createFromFormat('n', $quarter * 3)->modify('last day of this month');
                break;

            case self::THIS_YEAR:
                $this->_dateFrom = new DateTime('first day of january');
                $this->_dateTo = new DateTime('last day of december');
                break;

            case self::PAST_MONTH:
                $this->_dateFrom = new DateTime('first day of -1 month');
                $this->_dateTo = new DateTime('last day of -1 month');
                break;

            case self::PAST_QUARTER:
                $quarter = ceil(date('n') / 3);
                $this->_dateFrom = DateTime::createFromFormat('n', $quarter * 3 - 2)->modify('first day of -3 month');
                $this->_dateTo = DateTime::createFromFormat('n', $quarter * 3)->modify('last day of -3 month');
                break;

            case self::PAST_YEAR:
                $this->_dateFrom = new DateTime('-1 year first day of january');
                $this->_dateTo = new DateTime('-1 year last day of december');
                break;

            case self::THIS_MONTH:
            default:
                $this->_dateFrom = new DateTime('first day of this month');
                $this->_dateTo = new DateTime('last day of this month');
                break;
        }
        $this->_monthArray = [];
        $month = (int)$this->_dateFrom->format('n');
        $monthTo = (int)$this->_dateTo->format('n');
        while ($month <= $monthTo) {
            $this->_monthArray[] = $month++;
        }
    }

    /**
     * @return integer
     */
    public function getPeriod()
    {
        return $this->_period;
    }

    /**
     * @return array
     */
    public function getMonthArray()
    {
        return $this->_monthArray;
    }

    /**
     * @return array
     */
    public function getDateRange()
    {
        return [
            'from' => $this->dateFrom->format('Y-m-d'),
            'to' => $this->dateTo->format('Y-m-d'),
        ];
    }

    /**
     * @return DateTime
     */
    public function getDateFrom()
    {
        return $this->_dateFrom;
    }

    /**
     * @return DateTime
     */
    public function getDateTo()
    {
        return $this->_dateTo;
    }

    /**
     * @return array
     */
    public function getHeaderTableData()
    {
        return $this->_headerTableData;
    }

    /**
     * @return string
     */
    public function getPeriodName()
    {
        return self::$periodArray[$this->period];
    }

    /**
     * @return array
     */
    public function getPeriodItems()
    {
        $periodItems = [];
        foreach (self::$periodArray as $id => $name) {
            $periodItems[] = [
                'label' => $name,
                'url' => ['/product/xyz', 'period' => $id],
                'options' => ($this->period == $id) ? ['class' => 'active'] : [],
                'linkOptions' => ['class' => 'xyz-pjax-link']
            ];
        }

        return $periodItems;
    }

    /**
     * @return array
     */
    public function getMonthColumns()
    {
        $monthColumns = [];
        foreach ($this->monthArray as $month) {
            $monthColumns[] = [
                'attribute' => "month-{$month}",
                'label' => RUtils::dt()->ruStrFTime([
                    'date' => "01.{$month}." . date('Y'),
                    'format' => 'F',
                ]),
                'headerOptions' => [
                    'class' => 'sorting text-center',
                    'rowspan' => 1,
                    'colspan' => 1,
                    'style' => 'border-right: 1px solid #ddd;',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($data) use ($month) {
                    return $data["month-{$month}"];
                },
            ];
        }

        return $monthColumns;
    }

    /**
     * @return array
     */
    public function getMonthAttributes()
    {
        $monthAttributes = [];
        foreach ($this->monthArray as $month) {
            $monthAttributes[] = "month-{$month}";
        }

        return $monthAttributes;
    }

    /**
     * Build header table data
     */
    public function buildHeaderTableData()
    {
        $totalProductTypes = 0;
        $totalProductInStock = 0;

        foreach ($this->_headerTableData as $oneGroupData) {
            $totalProductTypes += $oneGroupData['countProductTypes'];
            $totalProductInStock += $oneGroupData['countProductInStock'];
        }
        $this->_headerTableData['totalProductTypes'] = $totalProductTypes;
        $this->_headerTableData['totalProductInStock'] = $totalProductInStock;
        foreach ($this->_headerTableData as $key => $oneGroupData) {
            if (in_array($key, ['X', 'Y', 'Z'])) {
                $this->_headerTableData[$key]['percentProductTypes'] = $oneGroupData['countProductTypes'] / ($this->_headerTableData['totalProductTypes'] ? $this->_headerTableData['totalProductTypes'] : 1) * 100;
                $this->_headerTableData[$key]['percentProductInStock'] = $oneGroupData['countProductInStock'] / ($this->_headerTableData['totalProductInStock'] ? $this->_headerTableData['totalProductInStock'] : 1) * 100;
            }
        }
    }

    /**
     * @return array
     */
    public function getGroupFilter()
    {
        return array_merge([null => 'Все'], [
            self::GROUP_X => self::GROUP_X,
            self::GROUP_Y => self::GROUP_Y,
            self::GROUP_Z => self::GROUP_Z,
        ]);
    }

    /**
     * @return array
     */
    public function getProductTitleFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()->groupBy('product_id')->asArray()->all(), 'product_id', 'product_title'));
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $productTableName = Product::tableName();
        $orderTableName = Order::tableName();
        $invoiceTableName = Invoice::tableName();
        $dateRange = $this->getDateRange();

        $storeQuery = ProductStore::find()->alias('ps')->joinWith('product pr', false)
            ->select(['ps.product_id', 'total' => 'SUM({{ps}}.[[quantity]])'])
            ->andWhere(['pr.company_id' => $user->company->id])
            ->groupBy('ps.product_id');

        return Product::find()
            ->select([
                "CAST({{s}}.[[total]] AS SIGNED) as product_count",
                "$orderTableName.product_id",
                "$orderTableName.product_title",
                "SUM(CAST($orderTableName.quantity AS UNSIGNED)) as count",
                "MONTH($invoiceTableName.document_date) as monthNumber",
            ])
            ->leftJoin($orderTableName, "$orderTableName.product_id = $productTableName.id")
            ->leftJoin($invoiceTableName, "$invoiceTableName.id = $orderTableName.invoice_id")
            ->leftJoin(['s' => $storeQuery], '{{product}}.[[id]] = {{s}}.[[product_id]]')
            ->byCompany($user->company->id)
            ->byUser()
            ->byDeleted()
            ->byProductionType(Product::PRODUCTION_TYPE_GOODS)
            ->andWhere(['and',
                ["$invoiceTableName.type" => Documents::IO_TYPE_OUT],
                ["$invoiceTableName.is_deleted" => Invoice::NOT_IS_DELETED],
                ['between', "$invoiceTableName.document_date", $dateRange['from'], $dateRange['to']]
            ])
            //->andFilterWhere(["$productTableName.id" => $this->product_title])
            ->groupBy(["monthNumber", "product_id"]);
    }
}