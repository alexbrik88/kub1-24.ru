<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.04.2018
 * Time: 10:23
 */

namespace frontend\models;


use common\components\helpers\ArrayHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\OrderDocument;
use common\models\store\StoreCompanyContractor;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use Yii;
use common\models\employee\Employee;
use yii\db\ActiveQuery;

/**
 * Class StoreCompanyContractorSearch
 * @package frontend\models
 */
class StoreCompanyContractorSearch extends Contractor
{
    /**
     * @var
     */
    public $countOrderDocuments;

    /**
     * @var
     */
    public $totalOrderDocumentAmount;

    /**
     * @var
     */
    public $averageTotalOrderDocumentAmount;

    /**
     * @var
     */
    public $responsibleEmployeeID;

    /**
     * @var ActiveQuery|null
     */
    protected $query = null;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['countOrderDocuments', 'totalOrderDocumentAmount', 'averageTotalOrderDocumentAmount'], 'safe'],
            [['contractor_id', 'responsibleEmployeeID'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $dateRange = StatisticPeriod::getSessionPeriod();
        $tContractor = static::tableName();
        $tType = CompanyType::tableName();

        $query = $this->query = static::find()
            ->select([
                $tContractor . '.*',
                'COUNT(orderDocuments.id) as countOrderDocuments',
                'SUM(orderDocuments.total_amount) as totalOrderDocumentAmount',
                'SUM(orderDocuments.total_amount) / IFNULL(COUNT(orderDocuments.id), 1) as averageTotalOrderDocumentAmount',
                'IFNULL(' . StoreCompanyContractor::tableName() . '.status, 0) as storeStatus',
                "$tType.name_short as contractorType",
                "ISNULL({{{$tType}}}.[[name_short]]) as isNullContractorName",

            ])
            ->joinWith('storeCompanyContractors')
            ->leftJoin($tType, "{{{$tContractor}}}.[[company_type_id]] = {{{$tType}}}.[[id]]")
            ->leftJoin(['orderDocuments' => OrderDocument::find()
                ->andWhere(['is_deleted' => OrderDocument::NOT_IS_DELETED])
                ->andWhere(['from_store' => OrderDocument::CREATED_FROM_STORE])
                ->andWhere(['between', OrderDocument::tableName() . '.document_date', $dateRange['from'], $dateRange['to']])
            ], $tContractor . '.id = orderDocuments.contractor_id')
            ->andWhere([$tContractor . '.is_deleted' => false])
            ->andWhere([$tContractor . '.company_id' => $user->company->id])
            ->andWhere([$tContractor . '.status' => Contractor::ACTIVE])
            ->byContractor(Contractor::TYPE_CUSTOMER);

        $query->groupBy(static::tableName() . '.id');

        $a = new \yii\db\Expression("ISNULL({{{$tType}}}.[[name_short]])");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'countOrderDocuments',
                    'totalOrderDocumentAmount',
                    'averageTotalOrderDocumentAmount',
                    'storeStatus',
                    'contractorType',
                    "$tContractor.name",
                    'isNullContractorName',
                ],
                'defaultOrder' => [
                    'storeStatus' => SORT_DESC,
                    'isNullContractorName' => SORT_ASC,
                    'contractorType' => SORT_ASC,
                    "$tContractor.name" => SORT_ASC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([static::tableName() . '.contractor_id' => $this->id]);

        $query->andFilterWhere([Contractor::tableName() . '.responsible_employee_id' => $this->responsibleEmployeeID]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getResponsibleEmployeeFilter()
    {
        $query = clone $this->query;
        $employeeTableName = Employee::tableName();

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($query
            ->select([
                "*",
                "CONCAT($employeeTableName.lastname, ' ', $employeeTableName.firstname_initial, '. ', $employeeTableName.patronymic_initial, '.') as fio",
                "contractor.responsible_employee_id as responsibleEmployeeId"
            ])
            ->leftJoin(Employee::tableName(),
                Contractor::tableName() . '.responsible_employee_id = ' . Employee::tableName() . '.id')
            ->andWhere(['not', [Employee::tableName() . '.id' => null]])
            ->groupBy(Contractor::tableName() . '.responsible_employee_id')
            ->asArray()
            ->all(), 'responsibleEmployeeId', 'fio'));
    }
}