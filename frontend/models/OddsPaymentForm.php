<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.06.2018
 * Time: 9:32
 */

namespace frontend\models;


use common\models\service\OddsPaymentHelper;
use frontend\modules\documents\components\InvoiceHelper;
use yii\base\Model;
use common\components\pdf\PdfRenderer;
use common\models\document\Invoice;
use common\models\service\Payment;
use common\models\service\PaymentOrder;
use common\models\service\PaymentType;
use common\models\Company;
use yii\base\Exception;
use Yii;
use yii\db\Connection;

/**
 * Class OddsPaymentForm
 * @package frontend\models
 *
 *
 * @property Company $company
 * @property Payment $payment
 * @property Invoice $invoice
 * @property Invoice $outInvoice
 */
class OddsPaymentForm extends Model
{
    /**
     * @var
     */
    public $paymentTypeId;
    /**
     * @var
     */
    public $companyId;

    /**
     * @var Company
     */
    protected $_company;
    /**
     * @var Payment
     */
    protected $_payment;
    /**
     * @var Invoice
     */
    protected $_invoice;
    /**
     * @var Invoice
     */
    protected $_out_invoice;
    /**
     * @var string
     */
    protected $_pdfContent;

    /**
     * @param Company $company
     * @param array $config
     * @throws Exception
     */
    public function __construct(Company $company = null, $config = [])
    {
        if ($company) {
            $this->companyId = $company->id;
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentTypeId', 'companyId'], 'required'],
            [
                ['paymentTypeId'], 'in',
                'range' => [
                    PaymentType::TYPE_ONLINE,
                    PaymentType::TYPE_INVOICE,
                ],
                'message' => 'Способ оплаты не указан.',
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'paymentTypeId' => 'Способ оплаты',
            'companyId' => 'На какую компанию выставить счет',
        ];
    }

    /**
     * @return Company
     */
    public function afterValidate()
    {
        $this->_company = Company::findOne($this->companyId);

        parent::afterValidate();
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->_payment;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * @return Invoice
     */
    public function getOutInvoice()
    {
        return $this->_out_invoice;
    }

    /**
     * @return string|null
     */
    public function getPdfContent()
    {
        return $this->_pdfContent;
    }

    /**
     * @return Payment
     */
    public function getNewPayment()
    {
        $orderArray = [];
        $payment = new Payment([
            'company_id' => $this->company->id,
            'sum' => 0,
            'type_id' => $this->paymentTypeId,
            'is_confirmed' => false,
            'payment_for' => Payment::FOR_ODDS,
        ]);
        $paymentOrder = new PaymentOrder([
            'company_id' => $this->company->id,
            'price' => 7500,
            'discount' => 0,
            'sum' => 7500,
        ]);
        $paymentOrder->populateRelation('company', $this->company);
        $paymentOrder->populateRelation('payment', $payment);
        $orderArray[] = $paymentOrder;

        $payment->sum += $paymentOrder->sum;
        $payment->sum .= '';
        $payment->populateRelation('orders', $orderArray);

        return $payment;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function createPayment()
    {
        if (!$this->company) {
            throw new Exception("The form configure error", 1);
        }
        $payment = $this->getNewPayment();
        if (!$payment->save()) {
            Yii::$app->session->setFlash('error', 'Ошибка при создании платежа. Попробуйте ещё раз');

            return false;
        }
        foreach ($payment->orders as $order) {
            $order->payment_id = $payment->id;
            if (!$order->save()) {
                Yii::$app->session->setFlash('error', 'Ошибка при создании платежа. Попробуйте ещё раз.');

                return false;
            }
        }
        $this->_payment = $payment;

        $invoice = OddsPaymentHelper::getInvoice($this->payment, $this->company);
        if (!$invoice) {
            Yii::$app->session->setFlash('error', 'Ошибка при создании счёта. Попробуйте ещё раз.');

            return false;
        }
        $this->_invoice = $invoice;

        return true;
    }

    /**
     * @return bool
     */
    public function invoicePayment()
    {
        $this->_out_invoice = OddsPaymentHelper::getInInvoiceByOut($this->invoice, $this->company);
        InvoiceHelper::afterSave($this->_out_invoice->id);
        if (!$this->outInvoice) {
            Yii::$app->session->setFlash('error', 'Ошибка при создании входящего счёта. Попробуйте ещё раз.');

            return false;
        }

        $this->_pdfContent = $this->invoiceRenderer->output(false);
        $subject = 'Счет на оплату тарифа на предоставление настройки финансовых отчетов';

        $mailer = Yii::$app->mailer
            ->compose([
                'html' => 'system/odds-payment/html',
                'text' => 'system/odds-payment/text',
            ], [
                'company' => $this->company,
                'invoice' => $this->invoice,
                'subject' => $subject,
            ])
            ->setFrom([Yii::$app->params['emailList']['info'] => Yii::$app->params['emailFromName']])
            ->setTo($this->company->email)
            ->setSubject($subject)
            ->attachContent($this->_pdfContent, [
                'fileName' => 'invoice.pdf',
                'contentType' => 'application/pdf',
            ]);

        if ($this->invoice->contractor_bik && $this->invoice->contractor_rs) {
            $content = Yii::$app->view->render('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
                'model' => $this->invoice,
            ]);
            $name = ($this->invoice->is_invoice_contract && $this->invoice->type == Documents::IO_TYPE_OUT) ? 'счет-договора' : 'счета';
            $mailer->attachContent($content, [
                'fileName' => 'Платежное_поручение_для_' . $name . '_№' .
                    mb_ereg_replace("([^\w\s\d\-_])", '', $this->invoice->fullNumber) . '.txt',
                'contentType' => 'text/plain',
            ]);
        }

        if (!$mailer->send()) {
            Yii::$app->session->setFlash('error', 'Ошибка при отправке счета. Попробуйте ещё раз.');

            return false;
        }

        return true;
    }

    /**
     * @return PdfRenderer
     */
    public function getInvoiceRenderer()
    {
        return Invoice::getRenderer(null, $this->invoice, PdfRenderer::DESTINATION_STRING);
    }

    /**
     * @return boolean
     */
    public function makePayment()
    {
        if (!$this->validate()) {
            return false;
        }
        $created = Yii::$app->db->transaction(function (Connection $db) {
            if ($this->createPayment()) {
                switch ($this->paymentTypeId) {
                    case PaymentType::TYPE_INVOICE:
                        if ($this->invoicePayment()) {
                            Yii::$app->session->setFlash('success', "Счет на оплату отправлен на ваш e-mail.");

                            return true;
                        }
                        break;
                }
                return true;
            }
            $db->transaction->rollBack();

            return false;
        });

        return $created;
    }
}