<?php

namespace frontend\models;

use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceStatistic;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * PaymentReminderContractorSearch represents the model behind the search form about `common\models\Contractor`.
 */
class PaymentReminderContractorSearch extends Contractor
{
    public $debt_count;
    public $debt_sum;
    public $is_type;
    public $search;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'search'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $dateRange
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->select([
            'contractor.id',
            'contractor.name',
            'contractor.company_type_id',
            'contractor.responsible_employee_id',
            'debt_count' => 'IFNULL(COUNT({{invoice}}.[[contractor_id]]), 0)',
            'debt_sum' => 'IFNULL(SUM({{invoice}}.[[remaining_amount]]), 0)',
            'is_type' => 'ISNULL({{company_type}}.[[name_short]])',
        ])->joinWith([
            'companyType',
            'responsibleEmployeeCompany',
        ])->leftJoin(Invoice::tableName(), [
            'and',
            ['invoice.contractor_id' => new \yii\db\Expression('{{contractor}}.[[id]]')],
            ['invoice.is_deleted' => 0],
            ['invoice.type' => Documents::IO_TYPE_OUT],
            ['invoice.invoice_status_id' => InvoiceStatus::$payAllowed],
            ['<', 'invoice.payment_limit_date', date('Y-m-d')],
        ])->leftJoin(PaymentReminderMessageContractor::tableName(), [
            'and',
            ['payment_reminder_message_contractor.contractor_id' => new \yii\db\Expression('{{contractor}}.[[id]]')],
            ['payment_reminder_message_contractor.company_id' => new \yii\db\Expression('{{contractor}}.[[company_id]]')],
        ])->andWhere([
            'contractor.company_id' => $this->company_id,
            'contractor.is_deleted' => false,
            'payment_reminder_message_contractor.id' => null,
        ])->groupBy('contractor.id');

        $this->load($params);
        $this->getSorted($params);

        $query->andFilterWhere([
            'contractor.id' => $this->id,
            'contractor.responsible_employee_id' => $this->responsible_employee_id,
        ])->andFilterWhere([
            'or',
            ['like', 'contractor.ITN', $this->search],
            ['like', 'contractor.name', $this->search],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'route' => 'payment-reminder/customers',
                'attributes' => [
                    'debt_count' => [
                        'default' => SORT_DESC,
                    ],
                    'debt_sum' => [
                        'default' => SORT_DESC,
                    ],

                    'is_type',
                    'company_type.name_short',
                    'contractor.name',
                ],
                'defaultOrder' => [
                    'is_type' => SORT_ASC,
                    'company_type.name_short' => SORT_ASC,
                    'contractor.name' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
