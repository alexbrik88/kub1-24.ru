<?php

namespace frontend\models;

use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use Yii;
use yii\base\Model;

class Qiwi extends Model
{
    public $uid;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid'], 'string'],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Уникальный идентификатор счета',
            'email' => 'E-mail пользователя',
        ];
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return is_string($this->uid) ? Invoice::findOne([
            'uid' => $this->uid,
            'type' => Documents::IO_TYPE_OUT,
            'is_deleted' => false,
        ]) : null;
    }

    /**
     * @return array
     */
    public function formData()
    {
        $invoice = $this->getInvoice();

        if ($invoice === null) {
            Yii::$app->session->setFlash('error', 'Счет не найден.');

            return false;
        } elseif (!in_array($invoice->invoice_status_id, InvoiceStatus::$payAllowed) || empty($invoice->company->qiwi_public_key)) {
            Yii::$app->session->setFlash('error', 'Счет не может быть оплачен.');

            return false;
        }

        $email = $this->validate(['email']) ? $this->email : null;

        return [
            'publicKey' => $invoice->company->qiwi_public_key,
            'billId' => (string) $invoice->id,
            'amount' => bcdiv($invoice->total_amount_with_nds, '100'),
            'phone' => $invoice->contractor->director_phone,
            'email' => $email ? : $invoice->contractor->director_email,
            'account' => $invoice->company_id,
            'lifetime' => $invoice->payment_limit_date . 'T2359',
            'successUrl' => Yii::$app->urlManager->createAbsoluteUrl([
                'qiwi/success',
                'uid' => $this->uid,
            ]),
        ];
    }
}
