<?php
namespace frontend\models;

use common\components\validators\EmployeeEmailValidator;
use common\models\employee\Employee;
use yii\base\Model;
use Yii;
use yii\web\IdentityInterface;
use yii\web\NotFoundHttpException;

/**
 * Password reset form
 */
class ChangeEmailForm extends Model
{
    public $password;
    public $email;

    /**
     * @var \common\models\employee\Employee
     */
    private $_user;

    /**
     * @param array $config
     * @param null $user
     * @throws NotFoundHttpException
     */
    public function __construct(IdentityInterface $user, $config = [])
    {
        $this->_user = $user;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $user = $this->_user;

        return [
            [['password', 'email',], 'required'],
            [['password'], function ($attribute) {
                if (!$this->_user->validatePassword($this->$attribute)) {
                    $this->addError($attribute, 'Неверный пароль');
                }
            }],
            [['email'], 'email'],
            [
                ['email'], 'unique',
                'targetClass' => $this->_user->className(),
                'targetAttribute' => 'email',
                'filter' => $this->_user->hasAttribute('is_deleted') ? [
                    'is_deleted' => false,
                ] : null,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Текущий пароль',
            'email' => 'Новая электронная почта',
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function save()
    {
        if ($this->validate()) {
            $this->_user->updateAttributes(['email' => $this->email]);

            return true;
        }

        return false;
    }
}
