<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 31.8.15
 * Time: 10.13
 * Email: t.kanstantsin@gmail.com
 */

namespace frontend\models;

use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\components\validators\EmployeeEmailValidator;
use common\components\validators\PasswordValidator;
use common\components\validators\PhoneValidator;
use common\models\Company;
use common\models\company\CompanyNotification;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use common\models\CompanyProductType;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\notification\Notification;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\service\ServiceModule;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\TaxationType;
use common\models\TimeZone;
use frontend\components\StatisticPeriod;
use frontend\modules\subscribe\forms\PromoCodeForm;
use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\helpers\Url;

/**
 * Class RegistrationForm
 * @package frontend\models
 */
class RegistrationForm extends Model
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_CAPTCHA = 'captcha';

    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;
    /**
     * @var int
     */
    public $companyType;
    /**
     * @var
     */
    public $taxationTypeOsno;
    /**
     * @var
     */
    public $taxationTypeUsn;
    /**
     * @var
     */
    public $taxationTypeEnvd;
    /**
     * @var
     */
    public $taxationTypePsn;
    /**
     * @var null
     */
    public $wpId = null;

    /**
     * @var null
     */
    public $promoCode = null;

    /**
     * @var bool
     */
    public $checkrules = null;

    /**
     * @var string
     */
    public $req = null;

    /**
     * @var null
     */
    public $phone = null;

    /**
     * @var null
     */
    public $registrationPageTypeId = null;

    /**
     * @var string
     */
    public $googleAnalyticsId = null;

    /**
     * @var string
     */
    public $utm = null;

    /**
     * @var string
     */
    public $source = null;

    /**
     * @var string
     */
    public $reCaptcha;

    /**
     * @var Company
     */
    private $_company;
    /**
     * @var Employee
     */
    private $_user;

    /**
     * @var array
     */
    public static $typeIds = [
        CompanyType::TYPE_IP,
        CompanyType::TYPE_OOO,
        CompanyType::TYPE_PAO,
        CompanyType::TYPE_EMPTY,
    ];

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'email',
                'companyType',
                'taxationTypeOsno',
                'taxationTypeUsn',
                'taxationTypeEnvd',
                'taxationTypePsn',
                'registrationPageTypeId',
                'promoCode',
                'googleAnalyticsId',
                'utm',
                'source',
                'checkrules',
                'password',
                'phone',
                'req',
            ],
            self::SCENARIO_CAPTCHA => [
                'email',
                'companyType',
                'taxationTypeOsno',
                'taxationTypeUsn',
                'taxationTypeEnvd',
                'taxationTypePsn',
                'registrationPageTypeId',
                'promoCode',
                'googleAnalyticsId',
                'utm',
                'source',
                'checkrules',
                'password',
                'phone',
                'req',
                'reCaptcha',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['email', 'companyType'], 'required'],
            [['email'], 'email'],
            [['email'], EmployeeEmailValidator::className(),],
            [
                ['email'], 'unique',
                'targetClass' => Employee::className(),
                'targetAttribute' => 'email',
            ],
            [['password'], 'required', 'when' => function ($model) {
                return $model->password !== null;
            }],
            [['password'], PasswordValidator::className(), 'when' => function ($model) {
                return $model->password !== null;
            }],
            [['checkrules'], 'required', 'when' => function ($model) {
                return !YII_ENV_DEV && !\Yii::$app->request->headers->has('origin');
            }, 'message' => 'Для продолжения необходимо принять условия'],
            [['taxationTypeOsno', 'taxationTypeUsn', 'taxationTypeEnvd', 'taxationTypePsn'], 'boolean'],
            [['taxationTypeOsno'], 'taxationTypeValidation'],
            [['phone'], PhoneValidator::className()],
            [
                ['companyType'], 'in', 'range' => self::$typeIds,
                'message' => 'Форма компании не выбрана.',
            ],
            [['registrationPageTypeId'], 'integer'],
            [['registrationPageTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => RegistrationPageType::className(), 'targetAttribute' => ['registrationPageTypeId' => 'id']],
            [['promoCode', 'googleAnalyticsId', 'utm', 'source'], 'string'],
            [['req'], 'string'],
        ];

        if (Yii::$app->params['useReCaptcha']) {
            $rules[] = [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator2::className()];
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function taxationTypeValidation($attribute, $params)
    {
        if ($this->companyType != CompanyType::TYPE_EMPTY &&
            empty($this->taxationTypeOsno) &&
            empty($this->taxationTypeUsn) &&
            empty($this->taxationTypeEnvd) &&
            empty($this->taxationTypePsn)
        ) {
            $this->addError($attribute, 'Необходимо заполнить систему налогообложения.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'companyType' => 'Форма бизнеса',
            'taxationType' => 'Система налогобложения',
            'taxationTypeOsno' => 'ОСНО (общая)',
            'taxationTypeUsn' => 'УСН (упращенка)',
            'taxationTypeEnvd' => 'ЕНВД (вмененка)',
            'taxationTypePsn' => 'ПСН (патент)',
            'reCaptcha' => '',
        ];
    }

    /**
     * @param  array  $params
     * @return Company
     */
    public static function getNewCompany($params = [])
    {
        return new Company(array_merge([
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'scenario' => Company::SCENARIO_REGISTRATION,
            'activation_type' => Company::ACTIVATION_EMPTY_PROFILE,
            'new_template_order' => 1,
        ], $params));
    }

    /**
     * @param  array  $params
     * @return Employee
     */
    public static function getNewEmployee($params = [])
    {
        return new Employee(array_merge([
            'employee_role_id' => EmployeeRole::ROLE_CHIEF,
            'is_registration_completed' => false,
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'statistic_range_date_from' => StatisticPeriod::getDefaultPeriod()['from'],
            'statistic_range_date_to' => StatisticPeriod::getDefaultPeriod()['to'],
            'statistic_range_name' => StatisticPeriod::DEFAULT_PERIOD,
            'is_old_kub_theme' => false,
            'auth_key' => '',
        ], $params));
    }

    /**
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function save($validate = true)
    {
        if ($validate && !$this->validate()) {
            return false;
        }

        return \Yii::$app->db->transaction(function (Connection $db) {
            $isSelfEmployed = $this->companyType == CompanyType::TYPE_EMPTY;
            if (empty($this->password)) {
                $this->password = Employee::generatePassword(Employee::PASS_LENGTH);
            }
            // create user
            $user = self::getNewEmployee([
                'email' => $this->email,
                'wp_id' => $this->wpId,
            ]);
            $user->setPassword($this->password);

            // save user
            if ($user->save(false)) {
                // create company
                $company = self::getNewCompany([
                    'self_employed' => $isSelfEmployed,
                    'owner_employee_id' => $user->id,
                    'created_by' => $user->id,
                    'company_type_id' => $this->companyType,
                    'email' => $this->email,
                    'phone' => $this->phone,
                    'registration_page_type_id' => $this->registrationPageTypeId,
                    'form_google_analytics_id' => $this->googleAnalyticsId,
                    'form_utm' => $this->utm,
                    'form_source' => $this->source,
                ]);

                $companyTaxationType = new CompanyTaxationType([
                    'osno' => $isSelfEmployed ? false : ($this->taxationTypeOsno ? : false),
                    'usn' => $isSelfEmployed ? true : ($this->taxationTypeUsn ? : false),
                    'envd' => $isSelfEmployed ? false : ($this->taxationTypeEnvd ? : false),
                    'psn' => $isSelfEmployed ? false : ($this->taxationTypePsn ? : false),
                ]);
                if ($this->registrationPageTypeId == RegistrationPageType::PAGE_TYPE_BANK_ROBOT &&
                $this->companyType == CompanyType::TYPE_IP && $companyTaxationType->usn) {
                    $companyTaxationType->usn_type = CompanyTaxationType::INCOME;
                    $companyTaxationType->usn_percent = 6;
                } elseif ($isSelfEmployed) {
                    $companyTaxationType->usn_type = CompanyTaxationType::INCOME;
                    $companyTaxationType->usn_percent = CompanyTaxationType::SELF_EMPLOYED_PERCENT;
                }
                $company->populateRelation('companyTaxationType', $companyTaxationType);

                $company->saveInvite($this->req);

                // save company
                if ($company->save() && $company->createTrialSubscribe()) {
                    $company->main_id = $company->id;
                    $company->updateAttributes(['main_id' => $company->id]);
                    $company->companyTaxationType->company_id = $company->id;
                    if ($company->companyTaxationType->save() && Contractor::createFounder($company)) {
                        $employeeCompany = new EmployeeCompany([
                            'company_id' => $company->id,
                        ]);
                        $employeeCompany->employee = $user;
                        if ($this->promoCode) {
                            $promoCode = new PromoCodeForm([
                                'company' => $company,
                                'code' => $this->promoCode,
                            ]);
                            \Yii::$app->db->transaction(function (Connection $db) use ($promoCode) {
                                if (!$promoCode->validate() || !$promoCode->save()) {
                                    \common\components\helpers\ModelHelper::logErrors($promoCode, __METHOD__);
                                    $db->transaction->rollBack();
                                }
                            });
                        }
                        if ($employeeCompany->save(false)) {
                            $user->updateAttributes([
                                'company_id' => $company->id,
                                'main_company_id' => $company->id,
                            ]);
                            if ($user->getCompany(true) !== null) {
                                $this->_user = $user;
                                $this->_company = $company;

                                return true;
                            }
                        }
                    } else {
                        \common\components\helpers\ModelHelper::logErrors($company->companyTaxationType, __METHOD__);
                    }
                } else {
                    \common\components\helpers\ModelHelper::logErrors($company, __METHOD__);
                }
            }

            if ($db->getTransaction()->isActive) {
                $db->getTransaction()->rollBack();
            }

            return false;
        });
    }

    /**
     * @return Employee
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        switch ($this->registrationPageTypeId) {
            case RegistrationPageType::PAGE_TYPE_INVOICE_CONTRACT:
                $url = Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT, 'isContract' => true]);
                break;
            case RegistrationPageType::PAGE_TYPE_ACT:
                $url = Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT, 'document' => Documents::SLUG_ACT]);
                break;
            case RegistrationPageType::PAGE_TYPE_PACKING_LIST:
                $url = Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT, 'document' => Documents::SLUG_PACKING_LIST]);
                break;
            case RegistrationPageType::PAGE_TYPE_INVOICE_FACTURE:
                $url = Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT, 'document' => Documents::SLUG_INVOICE_FACTURE]);
                break;
            case RegistrationPageType::PAGE_TYPE_UPD:
                $url = Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT, 'document' => Documents::SLUG_UPD]);
                break;
            case RegistrationPageType::PAGE_TYPE_PRICE_LIST:
            case RegistrationPageType::PAGE_TYPE_PRICE_LIST_LANDING:
                if ($this->_user) {
                    $this->_user->updateAttributes(['need_look_service_video' => false]);
                }
                $url = Url::to(['/price-list/step-instruction']);
                break;
            case RegistrationPageType::PAGE_TYPE_B2B_MODULE:
                $url = Url::to(['/b2b/company']);
                break;
            case RegistrationPageType::PAGE_TYPE_BANK_ROBOT:
            case RegistrationPageType::PAGE_TYPE_DECLARATION_TEMPLATE_IP:
            case RegistrationPageType::PAGE_TYPE_NULL_DECLARATION_TEMPLATE_IP:
            case RegistrationPageType::PAGE_TYPE_TAX_COM_OFD:
            case RegistrationPageType::PAGE_TYPE_NULL_LANDING:
                $url = Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]);
                if ($this->companyType == CompanyType::TYPE_IP && $this->taxationTypeUsn) {
                    if ($this->_user) {
                        $this->_user->updateAttributes(['need_look_service_video' => false]);
                    }
                    $url = Url::to(['/tax/robot/index']);
                    $this->company->updateAttributes([
                        'login_redirect_times' => 5,
                        'login_redirect_url' => $url,
                    ]);
                }
                break;
            case RegistrationPageType::PAGE_TYPE_AGREEMENT_SERVICE:
                $url = Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]);
                $agreementID = $this->_company ? $this->_company->createAgreementTemplateService() : null;
                if ($agreementID) {
                    Yii::$app->session->set('showAgreementCreatePopup', true);
                    $url = Url::to(['/documents/agreement-template/view', 'id' => $agreementID]);
                }
                break;
            case RegistrationPageType::PAGE_TYPE_PAYMENT:
                if ($this->_user) {
                    $this->_user->updateAttributes(['need_look_service_video' => false]);
                }
                $url = Url::to(['/documents/payment-order/create']);
                break;
            case RegistrationPageType::PAGE_TYPE_PAYMENT_REMINDER:
                if ($this->_user) {
                    $this->_user->updateAttributes(['need_look_service_video' => false]);
                }
                $url = Url::to(['/payment-reminder/index']);
                break;
            case RegistrationPageType::PAGE_TYPE_FOREIGN_INVOICE_LANDING:
                $url = Url::to(['/documents/foreign-currency-invoice/first-create', 'type' => Documents::IO_TYPE_OUT]);
                break;
            case RegistrationPageType::PAGE_TYPE_CRM:
                $url = Url::to(['/crm/task/index']);
                break;
            case RegistrationPageType::PAGE_TYPE_INVOICE:
            case RegistrationPageType::PAGE_TYPE_BLOG:
            case RegistrationPageType::PAGE_TYPE_LANDING_PRODUCTS:
            default:
                $url = Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]);
                break;
        }

        return $url;
    }
}
