<?php

namespace frontend\models;

use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\company\CompanyIndustry;
use common\models\company\CompanyType;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\rbac\permissions\Cash;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * ContractorSearch represents the model behind the search form about `common\models\Contractor`.
 */
class ContractorSearch extends Contractor
{
    const ACTIVITY_STATUS_ALL = 0;
    const ACTIVITY_STATUS_ACTIVE = 1;
    const ACTIVITY_STATUS_INACTIVE = 2;
    const TAXATION_ALL = 0;
    const TAXATION_NDS = 1;
    const TAXATION_NO_NDS = 2;
    const NO_ACCOUNTING_ALL = 0;
    const NO_ACCOUNTING_YES = 1;
    const NO_ACCOUNTING_NO = 2;

    public static $activityStatusToStatus = [
        self::ACTIVITY_STATUS_ALL => null,
        self::ACTIVITY_STATUS_ACTIVE => self::ACTIVE,
        self::ACTIVITY_STATUS_INACTIVE => self::INACTIVE,
    ];
    public static $taxationStatusToStatus = [
        self::TAXATION_ALL => null,
        self::TAXATION_NDS => 1,
        self::TAXATION_NO_NDS => 0
    ];
    public static $isAccountingStatusToStatus = [
        self::NO_ACCOUNTING_ALL => null,
        self::NO_ACCOUNTING_YES => 1,
        self::NO_ACCOUNTING_NO => 0
    ];

    const NOT_PAID_INVOICE_COUNT = 'notPaidInvoiceCount';
    const NOT_PAID_INVOICE_SUM = 'notPaidInvoiceSum';
    const PAID_INVOICE_COUNT = 'paidInvoiceCount';
    const PAID_INVOICE_SUM = 'paidInvoiceSum';
    
    const FILTER_KEY_WITHOUT = '-';

    public $activityStatus = self::ACTIVITY_STATUS_ACTIVE;
    public $filterTaxation;
    public $filterForm;
    public $group_id;
    public $filterIsAccounting;
    public $contact;
    public $phone;
    public $agree;
    public $notPaidInvoiceCount;
    public $notPaidInvoiceSum;
    public $paidInvoiceCount;
    public $paidInvoiceSum;
    public $not_paid_count;
    public $group_name;
    public $not_paid_sum;
    public $paid_count;
    public $paid_sum;
    public $prepayment_sum;
    public $balance_sum;
    public $balance_docs_sum;
    public $duplicate = false;
    public $autoinvoice;

    private $_dateRange;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['activityStatus'], 'in', 'range' => [
                self::ACTIVITY_STATUS_ALL,
                self::ACTIVITY_STATUS_ACTIVE,
                self::ACTIVITY_STATUS_INACTIVE,
            ],],
            [['filterTaxation'], 'in', 'range' => [
                self::TAXATION_ALL,
                self::TAXATION_NDS,
                self::TAXATION_NO_NDS
            ]],
            [['filterIsAccounting'], 'in', 'range' => [
                self::NO_ACCOUNTING_ALL,
                self::NO_ACCOUNTING_YES,
                self::NO_ACCOUNTING_NO
            ]],
            [
                [
                    'name',
                    'contact',
                    'group_name',
                    'phone',
                    'group_id',
                    'notPaidInvoiceCount',
                    'notPaidInvoiceSum',
                    'paidInvoiceCount',
                    'paidInvoiceSum',
                    'responsible_employee_id',
                    'duplicate',
                    'autoinvoice',
                    'filterForm',
                    'invoice_income_item_id',
                    'invoice_expenditure_item_id',
                    'customer_industry_id',
                    'seller_industry_id',
                    'customer_sale_point_id',
                    'seller_sale_point_id',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public static function canViewBalanceByDocuments()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        return $user->company->getActualSubscription(SubscribeTariffGroup::BI_FINANCE)
            || $user->company->getActualSubscription(SubscribeTariffGroup::BI_FINANCE_PLUS)
            || $user->company->getActualSubscription(SubscribeTariffGroup::BI_ALL_INCLUSIVE);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $dateRange
     *
     * @return ActiveDataProvider
     */
    public function search($params, $dateRange, $formName = null)
    {

        $this->load($params, $formName);
        $this->_dateRange = $dateRange;
        $cTable = $this->tableName();
        $subQueryNopaid = $this->_getNopaidQuery();
        $subQueryPaid = $this->_getPaidQuery();
        $subQueryPrepayment = $this->_getPrepaymentQuery();

        // only when payed Findirector
        if (self::canViewBalanceByDocuments()) {
            $subQueryNopaidByDocs = (new Query())
                ->select("contractor_id, SUM(amount) AS docs_sum")
                ->from(
                    $this->_getNopaidByDocsQuery(Documents::DOCUMENT_ACT)
                        ->union($this->_getNopaidByDocsQuery(Documents::DOCUMENT_UPD), true)
                        ->union($this->_getNopaidByDocsQuery(Documents::DOCUMENT_PACKING_LIST), true)
                )
                ->groupBy("contractor_id");
        } else {
            $subQueryNopaidByDocs = null;
        }

        $query = self::find()
            ->addSelect([
                'contractor.*',
                'group_name' => 'contractor_group.title',
                'contact' => 'IF({{contractor}}.[[contact_is_director]] = true, {{contractor}}.[[director_name]], {{contractor}}.[[contact_name]])',
                'phone' => 'IF({{contractor}}.[[contact_is_director]] = true, {{contractor}}.[[director_phone]], {{contractor}}.[[contact_phone]])',
                'not_paid_count' => 'IFNULL(`not_paid`.`invoice_count`, 0)',
                'not_paid_sum' => 'IFNULL(`not_paid`.`invoice_sum`, 0)',
                'paid_count' => 'IFNULL(`paid`.`invoice_count`, 0)',
                'paid_sum' => 'IFNULL(`paid`.`invoice_sum`, 0)',
                '{{autoinvoice}}.[[status]] [[autoinvoice]]',
                'prepayment_sum' => 'IFNULL(`prepayment`.`prepayment_sum`, 0)',
                'balance_sum' => 'IFNULL(`prepayment`.`prepayment_sum`, 0) - IFNULL(`not_paid`.`invoice_sum`, 0)'
            ])
            ->joinWith('companyType t');
            if($this->type == 2) {
                $query->leftJoin('contractor_group', 'contractor_group.id = contractor.customer_group_id');
            }
            if($this->type == 1){
                $query->leftJoin('contractor_group', 'contractor_group.id = contractor.seller_group_id');
            }
            $query->leftJoin(['not_paid' => $subQueryNopaid], 'not_paid.contractor_id = contractor.id')
            ->leftJoin(['paid' => $subQueryPaid], 'paid.contractor_id = contractor.id')
            ->leftJoin(['prepayment' => $subQueryPrepayment], 'prepayment.contractor_id = contractor.id')
            ->leftJoin('agreement', [
                '=',
                'agreement.id',
                (new Query)->select('id')
                    ->from(['a' => 'agreement'])
                    ->where('{{a}}.[[contractor_id]] = {{contractor}}.[[id]]')
                    ->orderBy(['a.document_date' => SORT_DESC])
                    ->limit(1),
            ])
            ->leftJoin(['autoinvoice' => Autoinvoice::tableName()], [
                '=',
                'autoinvoice.id',
                (new Query)->select('id')
                    ->from(['a2' => 'autoinvoice'])
                    ->where('{{a2}}.[[contractor_id]] = {{contractor}}.[[id]]')
                    ->andWhere([
                        'a2.status' => [Autoinvoice::ACTIVE, Autoinvoice::CANCELED],
                    ])
                    ->orderBy(['a2.status' => SORT_ASC])
                    ->limit(1),
            ])
            ->leftJoin('agreement_type', '{{agreement}}.[[document_type_id]] = {{agreement_type}}.[[id]]')
            ->byCompany($this->company_id)
            ->byIsDeleted(false)
            ->byType($this->type)
            ->groupBy('contractor.id');

        if ($subQueryNopaidByDocs) {
            $query->addSelect([
                'balance_docs_sum' => 'IFNULL(`prepayment`.`prepayment_sum`, 0) - IFNULL(`not_paid_docs`.`docs_sum`, 0)'
            ])->leftJoin([
                'not_paid_docs' => $subQueryNopaidByDocs
            ], 'not_paid_docs.contractor_id = contractor.id');

        } else {
            $query->addSelect('null AS [[balance_docs_sum]]');
        }

        self::searchByRoleFilter($query);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name' => [
                    'asc' => [
                        self::tableName() . '.face_type' => SORT_ASC,
                        't.name_short' => SORT_ASC,
                        self::tableName() . '.name' => SORT_ASC,
                    ],
                    'desc' => [
                        self::tableName() . '.face_type' => SORT_DESC,
                        't.name_short' => SORT_DESC,
                        self::tableName() . '.name' => SORT_DESC,
                    ],
                    'default' => SORT_ASC,
                ],
                'agree' => [
                    'asc' => [
                        'agreement_type.name' => SORT_ASC,
                        'agreement.document_number' => SORT_ASC,
                        'agreement.document_date' => SORT_ASC,
                    ],
                    'desc' => [
                        'agreement_type.name' => SORT_DESC,
                        'agreement.document_number' => SORT_DESC,
                        'agreement.document_date' => SORT_DESC,
                    ],
                ],
                'phone',
                'not_paid_count',
                'group_name',
                'not_paid_sum',
                'paid_count',
                'paid_sum',
                'prepayment_sum',
                'balance_sum',
                'balance_docs_sum'
            ],
            'defaultOrder' => $this->duplicate ?
                ['name' => SORT_ASC,] :
                ['not_paid_count' => SORT_DESC,],
        ]);

        if($this->group_name){
            $fieldGroup = [1=>"seller_group_id",2=>"customer_group_id"];
            $query->andWhere([$cTable . '.'.$fieldGroup[$this->type] => $this->group_name ? $this->group_name : null]);
        }

        if ($this->invoice_income_item_id === self::FILTER_KEY_WITHOUT) {
            $query->andWhere([$cTable . '.invoice_income_item_id' => null]);
        }
        elseif ($this->invoice_expenditure_item_id === self::FILTER_KEY_WITHOUT) {
            $query->andWhere([$cTable . '.invoice_expenditure_item_id' => null]);
        } else {
            $query->andFilterWhere([
                $cTable . '.status' => self::$activityStatusToStatus[(int)$this->activityStatus],
                $cTable . '.taxation_system' => self::$taxationStatusToStatus[(int)$this->filterTaxation],
                $cTable . '.responsible_employee_id' => $this->responsible_employee_id,
                $cTable . '.company_type_id' => $this->filterForm,
                $cTable . '.not_accounting' => self::$isAccountingStatusToStatus[(int)$this->filterIsAccounting],
                $cTable . '.invoice_income_item_id' => $this->invoice_income_item_id,
                $cTable . '.invoice_expenditure_item_id' => $this->invoice_expenditure_item_id,
            ]);
        }

        if (strlen($this->customer_industry_id)) {
            $query->andWhere([$cTable . '.customer_industry_id' => $this->customer_industry_id ?: null]);
        }
        if (strlen($this->seller_industry_id)) {
            $query->andWhere([$cTable . '.seller_industry_id' => $this->seller_industry_id ?: null]);
        }
        if (strlen($this->customer_sale_point_id)) {
            $query->andWhere([$cTable . '.customer_sale_point_id' => $this->customer_sale_point_id ?: null]);
        }
        if (strlen($this->seller_sale_point_id)) {
            $query->andWhere([$cTable . '.seller_sale_point_id' => $this->seller_sale_point_id ?: null]);
        }

        if ($this->name) {
            $query->byName($this->name);
        }

        $query->andFilterHaving(['contact' => $this->contact]);
        if ($this->autoinvoice) {
            $query->andWhere(['autoinvoice.status' => $this->autoinvoice == 'no' ? null : $this->autoinvoice]);
        }

        if ($this->duplicate) {
            $cloneQuery = clone $query;
            $contractorIDs = $cloneQuery
                ->select(Contractor::tableName() . '.id')
                ->groupBy(Contractor::tableName() . '.id')
                ->column();
            if ($contractorIDs) {
                $contractorIDsString = implode(',', $contractorIDs);
                $contractorIDs = Contractor::find()
                    ->andWhere(['id' => $contractorIDs])
                    ->andWhere(['or',
                        "(`ITN`) IN (SELECT `ITN`
                            FROM `contractor`
                            WHERE `id` IN ({$contractorIDsString})
                            AND `ITN` IS NOT NULL
                            AND `ITN` <> ''
                            GROUP BY `ITN`
                            HAVING COUNT(`id`) > 1
                        )",
                        "CONCAT(`physical_lastname`, ' ', `physical_firstname`, ' ', `physical_patronymic`) IN (
                            SELECT CONCAT(`physical_lastname`, ' ', `physical_firstname`, ' ', `physical_patronymic`) as fio
                            FROM `contractor`
                            WHERE `id` IN ({$contractorIDsString})
                            AND `face_type` = 1
                            GROUP BY `fio`
                            HAVING COUNT(`id`) > 1
                        )"
                    ])
                    ->column();
                $query->andWhere([Contractor::tableName() . '.id' => $contractorIDs,]);
            }
        }

        return $dataProvider;
    }

    /**
     * @return Query
     */
    private function _getNopaidQuery() 
    {
        return Invoice::find()
            ->select([
                'invoice_count' => 'COUNT({{invoice}}.[[contractor_id]])',
                'invoice_sum' => 'SUM(IFNULL({{invoice}}.[[remaining_amount]], 0))',
                'invoice.contractor_id'
            ])
            ->where([
                'and',
                ['invoice.is_deleted' => 0],
                ['invoice.type' => (int)$this->type],
                ['invoice.company_id' => (int)$this->company_id],
                ['invoice.invoice_status_id' => ArrayHelper::getValue(InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID], $this->type)],
                ['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']],
            ])
            ->groupBy('invoice.contractor_id');
    }

    /**
     * @return Query
     */
    private function _getPaidQuery()
    {
        return Invoice::find()
            ->select([
                'invoice_count' => 'COUNT({{invoice}}.[[contractor_id]])',
                'invoice_sum' => 'SUM(IFNULL({{invoice}}.[[payment_partial_amount]], 0))',
                'invoice.contractor_id'
            ])
            ->where([
                'and',
                ['invoice.is_deleted' => 0],
                ['invoice.type' => (int)$this->type],
                ['invoice.company_id' => (int)$this->company_id],
                ['invoice.invoice_status_id' => ArrayHelper::getValue(InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::PAID], $this->type)],
                ['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']],
            ])
            ->groupBy('invoice.contractor_id');
    }

    /**
     * @return Query
     */
    private function _getPrepaymentQuery()
    {
        $flowType = ($this->type == Documents::IO_TYPE_IN) ? CashFlowsBase::FLOW_TYPE_EXPENSE : CashFlowsBase::FLOW_TYPE_INCOME;

        $_subQueryPrepayment1 = (new \yii\db\Query)
            ->select([
                'a.id',
                'a.contractor_id',
                'a.amount AS flowsAmount',
                'IFNULL(SUM(b.amount), 0) AS invoicesAmount',
            ])
            ->from(['a' => CashBankFlows::tableName()])
            ->leftJoin(['b' => CashBankFlowToInvoice::tableName()], 'a.id = b.flow_id')
            ->where(['a.company_id' => $this->company_id, 'a.flow_type' => $flowType])
            ->andWhere(['between', 'a.recognition_date', $this->_dateRange['from'], $this->_dateRange['to']])
            ->groupBy('a.id');
        $_subQueryPrepayment2 = (new \yii\db\Query)
            ->select([
                'a.id',
                'a.contractor_id',
                'a.amount AS flowsAmount',
                'IFNULL(SUM(b.amount), 0) AS invoicesAmount',
            ])
            ->from(['a' => CashOrderFlows::tableName()])
            ->leftJoin(['b' => CashOrderFlowToInvoice::tableName()], 'a.id = b.flow_id')
            ->where(['a.company_id' => $this->company_id, 'a.flow_type' => $flowType])
            ->andWhere(['between', 'a.recognition_date', $this->_dateRange['from'], $this->_dateRange['to']])
            ->groupBy('a.id');
        $_subQueryPrepayment3 = (new \yii\db\Query)
            ->select([
                'a.id',
                'a.contractor_id',
                'a.amount AS flowsAmount',
                'IFNULL(SUM(b.amount), 0) AS invoicesAmount',
            ])
            ->from(['a' => CashEmoneyFlows::tableName()])
            ->leftJoin(['b' => CashEmoneyFlowToInvoice::tableName()], 'a.id = b.flow_id')
            ->where(['a.company_id' => $this->company_id, 'a.flow_type' => $flowType])
            ->andWhere(['between', 'a.recognition_date', $this->_dateRange['from'], $this->_dateRange['to']])
            ->groupBy('a.id');

        return (new \yii\db\Query)
            ->from(['t' => $_subQueryPrepayment1->union($_subQueryPrepayment2, true)->union($_subQueryPrepayment3, true)])
            ->select(new \yii\db\Expression('contractor_id, SUM(flowsAmount) - SUM(invoicesAmount) AS prepayment_sum'))
            ->groupBy('contractor_id');
    }

    /**
     * @param int $documentType
     * @return Query
     * @throws InvalidConfigException
     */
    private function _getNopaidByDocsQuery($documentType)
    {
        switch ($documentType) {
            case Documents::DOCUMENT_ACT:
                $docClass = Act::class;
                break;
            case Documents::DOCUMENT_UPD:
                $docClass = Upd::class;
                break;
            case Documents::DOCUMENT_PACKING_LIST:
                $docClass = PackingList::class;
                break;
            default:
                throw new InvalidConfigException('Unknown document type');
        }

        return $docClass::find()
            ->alias('document')
            ->select([
                'contractor_id' => '{{document}}.[[contractor_id]]',
                'amount' => "SUM(IFNULL({{document}}.[[remaining_amount]], 0))"
            ])
            ->where([
                'and',
                ['type' => (int)$this->type],
                ['company_id' => (int)$this->company_id],
                ['between', 'document.document_date', $this->_dateRange['from'], $this->_dateRange['to']],
            ])
            ->groupBy('{{document}}.[[contractor_id]]');
    }

    /**
     * using:
     * \frontend\models\ContractorSearch::searchByRoleFilter($query);
     *
     * @param $query
     */
    public static function searchByRoleFilter($query, $alias = 'contractor')
    {
        if (!Yii::$app->user->can(\frontend\rbac\permissions\Contractor::INDEX_STRANGERS)) {
            $query->andWhere([
                $alias.'.responsible_employee_id' => Yii::$app->user->id,
            ]);
        }
        if (!Yii::$app->user->can(\frontend\rbac\permissions\Contractor::INDEX_NOT_ACCOUNTING)) {
            $query->andWhere(['or',
                [$alias.'.not_accounting' => false],
                [$alias.'.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }
    }

    /**
     * @param ActiveDataProvider $dataProvider
     */
    private function _setSort(ActiveDataProvider $dataProvider)
    {
        $dataProvider->setSort([
            'attributes' => [
                'name' => [
                    'asc' => [
                        self::tableName() . '.face_type' => SORT_ASC,
                        't.name_short' => SORT_ASC,
                        self::tableName() . '.name' => SORT_ASC,
                    ],
                    'desc' => [
                        self::tableName() . '.face_type' => SORT_DESC,
                        't.name_short' => SORT_DESC,
                        self::tableName() . '.name' => SORT_DESC,
                    ],
                    'default' => SORT_ASC,
                ],
                'notPaidInvoiceCount' => [
                    'asc' => ['u.invoice_count' => SORT_ASC],
                    'desc' => ['u.invoice_count' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'notPaidInvoiceSum' => [
                    'asc' => ['u.invoice_sum' => SORT_ASC],
                    'desc' => ['u.invoice_sum' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'paidInvoiceCount' => [
                    'asc' => ['u.invoice_count' => SORT_ASC],
                    'desc' => ['u.invoice_count' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'paidInvoiceSum' => [
                    'asc' => ['u.invoice_sum' => SORT_ASC],
                    'desc' => ['u.invoice_sum' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => [
                'name' => SORT_ASC,
            ],
        ]);
    }

    /**
     * @param ActiveDataProvider $dataProvider
     */
    private function _setSortCondition(ActiveDataProvider $dataProvider, $subQuery)
    {
        $sortParam = Yii::$app->request->getQueryParam($dataProvider->getSort()->sortParam);
        $sortParam = trim($sortParam, "- \t\n\r\0\x0B");
        if (!in_array($sortParam, [
            self::NOT_PAID_INVOICE_COUNT,
            self::NOT_PAID_INVOICE_SUM,
            self::PAID_INVOICE_COUNT,
            self::PAID_INVOICE_SUM,
        ])
        ) {
            return;
        }

        $this->setStatus($subQuery, $sortParam);
    }

    /**
     * @param \yii\db\QueryInterface $query
     *
     * @return mixed
     */
    private function setDocumentDate($query)
    {
        $query->andWhere(['between', Invoice::tableName() . '.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
    }

    /**
     * @param \yii\db\QueryInterface $query
     * @param $sortParam
     *
     * @return mixed
     */
    private function setStatus($query, $sortParam)
    {
        $query->andWhere(['in', Invoice::tableName() . '.invoice_status_id', $this->getStatusArrayBySortParam($sortParam)]);
    }

    /**
     * @param $sortParam
     *
     * @return mixed
     */
    public function getStatusArrayBySortParam($sortParam)
    {
        return in_array($sortParam, [
            self::NOT_PAID_INVOICE_COUNT,
            self::NOT_PAID_INVOICE_SUM,
        ]) ? InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][$this->type]
            : InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::PAID][$this->type];
    }

    /**
     * @param ActiveQuery $query
     * @return array|false
     */
    public function getContactItemsByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $data = $searchQuery->select([
            'contact' => 'IF(contractor.contact_is_director = true, contractor.director_name, contractor.contact_name)',
        ])->column();

        $data = array_unique($data);
        natsort($data);

        return ['' => 'Все'] + array_combine($data, $data);
    }

    /**
     * @param ActiveQuery $query
     * @return array
     */
    public function getResponsibleItemsByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $dataArray = $this->company
            ->getEmployeeCompanies()
            ->andWhere([
                'employee_id' => $searchQuery->select([
                    'contractor.responsible_employee_id',
                    'contact' => 'IF(contractor.contact_is_director = true, contractor.director_name, contractor.contact_name)',
                ])->distinct()->column(),
            ])
            ->orderBy(['lastname' => SORT_ASC, 'firstname_initial' => SORT_ASC, 'patronymic_initial' => SORT_ASC])
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($dataArray, 'employee_id', function ($model) {
                return $model->getFio(true);
            });
    }

    /**
     * @param ActiveQuery $query
     * @return array|false
     */
    public function getArticleItemsByQuery($type, ActiveQuery $query)
    {
        $searchQuery = clone $query;

        $exceptContractorId = Yii::$app->params['service']['contractor_id'];
        $classArticle = ($type == Contractor::TYPE_CUSTOMER)
            ? InvoiceIncomeItem::class
            : InvoiceExpenditureItem::class;

        $dataIds = $searchQuery->select([
            'article' => ($type == Contractor::TYPE_CUSTOMER)
                ? 'contractor.invoice_income_item_id'
                : 'contractor.invoice_expenditure_item_id'
        ])->andFilterWhere(['not', ['contractor.id' => $exceptContractorId]])->distinct()->column();

        return ['' => 'Все'] + [self::FILTER_KEY_WITHOUT => '---'] + ArrayHelper::map(
            $classArticle::find()
                ->where(['id' => $dataIds])
                ->select(['id','name'])
                ->asArray()->all(), 'id', 'name');
    }

public function getIndustryItemsByQuery($type, ActiveQuery $query)
{
    $searchQuery = clone $query;
    $exceptContractorId = Yii::$app->params['service']['contractor_id'];
    $dataIds = $searchQuery->select([
        'article' => ($type == Contractor::TYPE_CUSTOMER)
            ? 'contractor.customer_industry_id'
            : 'contractor.seller_industry_id'
    ])->andFilterWhere(['not', ['contractor.id' => $exceptContractorId]])->distinct()->column();

    return ['' => 'Все'] + ['0' => 'Без направления'] + ArrayHelper::map(
            CompanyIndustry::find()
                ->where(['id' => $dataIds])
                ->select(['id','name'])
                ->asArray()->all(), 'id', 'name');
}
public function getSalePointItemsByQuery($type, ActiveQuery $query)
{
    $searchQuery = clone $query;
    $exceptContractorId = Yii::$app->params['service']['contractor_id'];
    $dataIds = $searchQuery->select([
        'article' => ($type == Contractor::TYPE_CUSTOMER)
            ? 'contractor.customer_sale_point_id'
            : 'contractor.seller_sale_point_id'
    ])->andFilterWhere(['not', ['contractor.id' => $exceptContractorId]])->distinct()->column();

    return ['' => 'Все'] + ['0' => 'Без точки продаж'] + ArrayHelper::map(
            SalePoint::find()
                ->where(['id' => $dataIds])
                ->select(['id','name'])
                ->asArray()->all(), 'id', 'name');
}

    /**
     * @param ActiveDataProvider $dataProvider
     * @param string $type
     * @return \PhpOffice\PhpSpreadsheet\Writer\IWriter
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function getExcelWriter(ActiveDataProvider $dataProvider, $type = 'Xlsx')
    {
        $modelArray = $dataProvider->models;
        $count = $dataProvider->count;
        $dataArray = [
            [
                'Название',
                'Контакт',
                'Телефон',
                'Договор',
                'Счетов не оплачено',
                'Не оплачено счетов на сумму',
                'Счетов оплачено',
                'Оплачено счетов на сумму',
                'Ответственный',
            ],
        ];
        foreach ($modelArray as $model) {
            $employee = EmployeeCompany::findOne([
                'employee_id' => $model->responsible_employee_id,
                'company_id' => $model->company_id,
            ]);
            $dataArray[] = [
                $model->getTitle(true),
                $model->realContactName ?: '',
                $model->realContactPhone ?: '',
                $model->getLastAgreementStr(false),
                TextHelper::moneyFormat($model->not_paid_count),
                TextHelper::invoiceMoneyFormat($model->not_paid_sum, 2),
                TextHelper::moneyFormat($model->paid_count),
                TextHelper::invoiceMoneyFormat($model->paid_sum, 2),
                $employee ? $employee->getFio(true) : '',
            ];
        }

        $i = $count + 1;
        $objPHPExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(18);
        $objPHPExcel->getActiveSheet()->fromArray($dataArray);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:I{$i}")->applyFromArray([
            'borders' => array(
                'allborders' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
            ),
        ]);

        return \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, $type);
    }
}
