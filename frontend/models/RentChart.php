<?php namespace frontend\models;

use common\models\rent\Category;
use common\models\rent\RentAgreement;
use common\models\rent\RentAgreementItem;
use Yii;
use common\models\Company;
use yii\helpers\ArrayHelper;

class RentChart {

    const OBJECT_TYPE_TOTAL = null;

    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public static function getSelect2Data()
    {
        return [
            RentChart::OBJECT_TYPE_TOTAL => 'Итого',
        ] + ArrayHelper::map(Category::find()->all(), 'id', 'title');
    }

    public function getSingleChartData($periods, $customPeriod, $customObjectType) {

        $dateFrom = $periods[0]['from'] ?? date('Y-m-d');
        $dateTo = $periods[count($periods)-1]['to'] ?? date('Y-m-d');
        $groupBy = ($customPeriod === 'days') ? 'ymd' : 'ym';
        $byDays = $customPeriod === 'days';
        $byMonths = !$byDays;
        $filterByObjectType = ($customObjectType > 0)
            ? ('o.category_id = ' . (int)$customObjectType)
            : '1=1';

        $tableAgreement = RentAgreement::tableName();
        $tableAgreementItem = RentAgreementItem::tableName();
        $tableObject = RentEntity::tableName();

        $dataLeased = Yii::$app->db->createCommand("
            SELECT
              DATE_FORMAT(a.date_begin, '%Y%m%d') AS ymd,        
              DATE_FORMAT(a.date_begin, '%Y%m') AS ym,
              COUNT(i.id) AS cnt
            FROM {$tableAgreement} a
            LEFT JOIN {$tableAgreementItem} i ON i.rent_agreement_id = a.id
            LEFT JOIN {$tableObject} o ON i.rent_entity_id = o.id
            WHERE a.company_id = '{$this->company->id}' 
              AND a.date_begin BETWEEN '{$dateFrom}' AND '{$dateTo}' 
              AND {$filterByObjectType}
            GROUP BY {$groupBy}
        ")->queryAll();

        $baseLineLeased = Yii::$app->db->createCommand("
            SELECT
              COUNT(i.id) AS cnt
            FROM {$tableAgreement} a
            LEFT JOIN {$tableAgreementItem} i ON i.rent_agreement_id = a.id
            LEFT JOIN {$tableObject} o ON i.rent_entity_id = o.id
            WHERE a.company_id = '{$this->company->id}' 
              AND a.date_begin < '{$dateFrom}'
              AND {$filterByObjectType}  
        ")->queryScalar();

        $dataReturn = Yii::$app->db->createCommand("
            SELECT
              DATE_FORMAT(a.date_end, '%Y%m%d') AS ymd,        
              DATE_FORMAT(a.date_end, '%Y%m') AS ym,
              COUNT(i.id) AS cnt
            FROM {$tableAgreement} a
            LEFT JOIN {$tableAgreementItem} i ON i.rent_agreement_id = a.id
            LEFT JOIN {$tableObject} o ON i.rent_entity_id = o.id            
            WHERE a.company_id = '{$this->company->id}' 
              AND a.date_end BETWEEN '{$dateFrom}' AND '{$dateTo}'
              AND {$filterByObjectType}            
            GROUP BY {$groupBy}
        ")->queryAll();

        $baseLineReturn = Yii::$app->db->createCommand("
            SELECT
              COUNT(i.id) AS cnt
            FROM {$tableAgreement} a
            LEFT JOIN {$tableAgreementItem} i ON i.rent_agreement_id = a.id
            LEFT JOIN {$tableObject} o ON i.rent_entity_id = o.id            
            WHERE a.company_id = '{$this->company->id}' 
              AND a.date_end < '{$dateFrom}'
              AND {$filterByObjectType}
        ")->queryScalar();

        $dataTotalCount = Yii::$app->db->createCommand("
            SELECT
              DATE_FORMAT(o.purchased_at, '%Y%m%d') AS ymd,        
              DATE_FORMAT(o.purchased_at, '%Y%m') AS ym,
              SUM(IFNULL(o.count, 1)) AS cnt
            FROM {$tableObject} o
            WHERE o.company_id = '{$this->company->id}' 
              AND o.purchased_at BETWEEN '{$dateFrom}' AND '{$dateTo}'
              AND {$filterByObjectType}
            GROUP BY {$groupBy}
        ")->queryAll();

        $baseLineTotal = Yii::$app->db->createCommand("
            SELECT
              SUM(IFNULL(o.count, 1)) AS cnt
            FROM {$tableObject} o
            WHERE o.company_id = '{$this->company->id}' 
              AND o.purchased_at < '{$dateFrom}'
              AND {$filterByObjectType}
        ")->queryScalar();

        $ret = [
            'leased' => [],
            'return' => [],
            'total_count' => [],
            'rent_count' => []
        ];

        $cntRentBalance = 0;
        $cntTotalPrev = 0;
        foreach ($periods as $num => $period) {

            @list($y,$m,$d) = explode('-', $period['from']);

            $cnt = $cntLeased = 0;
            foreach ($dataLeased as $leased)
                if ($byMonths && $leased['ym'] == $y.$m || $byDays && $leased['ymd'] === $y.$m.$d) {
                    $cnt += $leased['cnt'];
                    $cntLeased += $leased['cnt'];
                }
            $ret['leased'][] = $cnt;

            $cnt = $cntReturned = 0;
            foreach ($dataReturn as $return)
                if ($byMonths && $return['ym'] == $y.$m || $byDays && $return['ymd'] === $y.$m.$d) {
                    $cnt += $return['cnt'];
                    $cntReturned += $return['cnt'];
                }
            $ret['return'][] = $cnt;

            $cnt = 0;
            foreach ($dataTotalCount as $total)
                if ($byMonths && $total['ym'] == $y.$m || $byDays && $total['ymd'] === $y.$m.$d)
                    $cnt += $total['cnt'];
            $ret['total_count'][] = $cntTotalNext = $cnt + $cntTotalPrev
                + ($num == 0 ? $baseLineTotal : 0);
            $cntTotalPrev = $cntTotalNext;

            $cntRentBalance += ($cntLeased - $cntReturned);
            $ret['rent_count'][] = $cntRentBalance + ($baseLineLeased - $baseLineReturn);
        }

        return $ret;
    }

    public function getPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0")
    {
        $start = date_create_from_format('Y-m-d H:i:s', date('Y-m-d 00:00:00'));
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month")->modify("-2 month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = $curr->format('Ym');
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }
}