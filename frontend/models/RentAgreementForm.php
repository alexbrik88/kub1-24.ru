<?php

namespace frontend\models;

use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\rent\Entity;
use common\models\rent\RentAgreement;
use common\models\rent\RentAgreementItem;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class RentAgreementForm
 */
class RentAgreementForm extends Model
{
    public $document_number;
    public $document_additional_number;
    public $document_date;
    public $contractor_id;
    public $date_begin;
    public $date_end;
    public $payment_period;
    public $has_discount;
    public $discount_type;
    public $is_hidden_discount;
    public $comment;
    public $amount;

    private RentAgreement $rentAgreement;
    private EmployeeCompany $employeeCompany;
    private int $company_id;
    private array $oldItems = [];
    private array $newItems = [];
    private array $formItems = [];

    public function __construct(EmployeeCompany $employeeCompany, RentAgreement $rentAgreement = null, $config = [])
    {
        $this->company_id = $employeeCompany->company_id;
        if ($rentAgreement === null) {
            $rentAgreement = new RentAgreement([
                'document_date' => date('d.m.Y'),
                'date_begin' => date('d.m.Y'),
                'date_end' => date('31.12.Y'),
                'payment_period' => RentAgreement::PAYMENT_PERIOD_DEFAULT,
            ]);
        }
        if (empty($rentAgreement->company_id)) {
            $rentAgreement->company_id = $employeeCompany->company_id;
        }
        if (empty($rentAgreement->document_author_id)) {
            $rentAgreement->document_author_id = $employeeCompany->employee_id;
        }
        if (empty($rentAgreement->document_number)) {
            $rentAgreement->document_number = RentAgreement::getNextNumber($employeeCompany->company);
        }

        $this->employeeCompany = $employeeCompany;
        $this->rentAgreement = $rentAgreement;

        $this->setAttributes($rentAgreement->getAttributes([
            'document_number',
            'document_additional_number',
            'contractor_id',
            'payment_period',
            'has_discount',
            'discount_type',
            'is_hidden_discount',
            'comment',
        ]), false);

        $this->document_date = $rentAgreement->document_date && ($d = date_create($rentAgreement->document_date)) ? $d->format('d.m.Y') : null;
        $this->date_begin = $rentAgreement->date_begin && ($d = date_create($rentAgreement->date_begin)) ? $d->format('d.m.Y') : null;
        $this->date_end = $rentAgreement->date_end && ($d = date_create($rentAgreement->date_end)) ? $d->format('d.m.Y') : null;

        $this->amount = round($rentAgreement->amount / 100, 2);

        foreach ($rentAgreement->items as $item) {
            if ($item->id) {
                $this->oldItems[$item->id] = $item;
            }
            $this->formItems[] = new RentAgreementItemForm($rentAgreement, $item);
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'document_number',
                    'document_additional_number',
                    'comment',
                ],
                'trim',
            ],
            [
                [
                    'contractor_id',
                    'date_begin',
                    'date_end',
                    'document_number',
                    'document_date',
                ],
                'required',
            ],
            [
                [
                    'date_begin',
                    'date_end',
                    'document_date',
                ],
                'date',
                'format' => 'php:d.m.Y',
            ],
            [['date_end'], 'validatePeriod'],
            [
                ['document_number'], 'unique',
                'targetClass' => RentAgreement::class,
                'targetAttribute' => 'document_number',
                'filter' => function ($query) {
                    $query->andWhere(['company_id' => $this->rentAgreement->company_id]);
                    $query->andWhere(['deleted' => false]);
                    $query->andWhere(['IFNULL([[document_additional_number]], "")' => strval($this->document_additional_number)]);
                    if ($date = date_create($this->document_date)) {
                        $query->andWhere([
                            'between',
                            'document_date',
                            $date->format('Y-01-01'),
                            $date->format('Y-12-31'),
                        ]);
                    }
                    if ($this->rentAgreement->id) {
                        $query->andWhere([
                            'not',
                            ['id' => $this->rentAgreement->id],
                        ]);
                    }
                },
            ],
            [
                ['contractor_id'],
                'exist',
                'targetClass' => Contractor::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company_id],
            ],
            [['comment'], 'string', 'max' => 500],
            [['has_discount', 'is_hidden_discount', 'discount_type'], 'default', 'value' => '0'],
            [['has_discount', 'is_hidden_discount', 'discount_type'], 'boolean'],
            [['payment_period'], 'in', 'range' => array_keys(RentAgreement::$paymenPeriodList)],
            [['items'], 'safe'],
        ];
    }

    public function validatePeriod($attribute, $params)
    {
        $start = date_create($this->date_begin);
        $end = date_create($this->date_end);
        if ($start > $end) {
            $this->addError($attribute, 'Дата начала аренды не может быть болшьше даты её окончания.');

            return;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'document_number' => 'Номер документа',
            'document_additional_number' => 'Доп. номер документа',
            'document_date' => 'Дата документа',
            'contractor_id' => 'Арендатор',
            'date_begin' => 'Дата начала аренды',
            'date_end' => 'Дата окончания аренды',
            'payment_period' => 'Период оплаты',
            'has_discount' => 'Указать скидку',
            'comment' => 'Комментарий',
        ];
    }

    public function setItems()
    {
        // setter do nothing
    }

    public function getItems()
    {
        return $this->formItems;
    }

    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);
        $itemDataArray = ArrayHelper::getValue($data, 'RentAgreementItemForm');

        if ($load || $itemDataArray !== null) {
            $this->formItems = [];
            foreach ((array) $itemDataArray as $itemData) {
                $itemForm = new RentAgreementItemForm($this->rentAgreement);
                $load = $itemForm->load($itemData, '') || $load;
                $this->formItems[] = $itemForm;
            }

            return true;
        }

        return false;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        $validate = parent::validate($attributeNames, $clearErrors);
        if (count($this->formItems)) {
            foreach ($this->formItems as $item) {
                $validate = $item->validate() && $validate;
                if ($item->hasErrors()) {
                    foreach ($item->getFirstErrors() as $key => $value) {
                        $this->addError('items', $value);
                    }
                }
            }
        } else {
            $validate = false;
            $this->addError('items', 'Необходимо добавить хотя бы один объект аренды.');
        }

        return $validate;
    }

    public function afterValidate()
    {
        parent::afterValidate();

        // set total amount
        $this->amount = 0;
        foreach ($this->formItems as $item) {
            $this->amount += $item->amount;
        }
    }

    public function save() : bool
    {
        if (!$this->validate()) {
            return false;
        }

        // set attributes
        $this->rentAgreement->setAttributes($this->getAttributes([
            'document_number',
            'document_additional_number',
            'contractor_id',
            'payment_period',
            'has_discount',
            'discount_type',
            'is_hidden_discount',
            'comment',
        ]), false);

        // set dates
        $this->rentAgreement->document_date = date_create($this->document_date)->format('Y-m-d');
        $this->rentAgreement->date_begin = date_create($this->date_begin)->format('Y-m-d');
        $this->rentAgreement->date_end = date_create($this->date_end)->format('Y-m-d');

        // set amount
        $this->rentAgreement->amount = round($this->amount * 100);

        $isSaved = false;
        $transaction = \Yii::$app->db->beginTransaction();

        try {
            if ($this->saveInternal()) {
                $transaction->commit();
                $isSaved = true;
            } else {
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $isSaved;
    }

    /**
     * @return bool
     */
    private function saveInternal() : bool
    {
        if ($this->rentAgreement->save()) {
            $this->newItems = [];
            foreach ($this->formItems as $key => $itemForm) {
                if ($itemForm->save($key + 1)) {
                    $item = $itemForm->getModel();
                    $this->newItems[$item->id] = $item;
                } else {
                    \common\components\helpers\ModelHelper::logErrors($itemForm, __METHOD__);
                    return false;
                }
            }
            foreach ($this->oldItems as $item) {
                if (!isset($this->newItems[$item->id])) {
                    if (!$item->delete()) {
                        return false;
                    }
                }
            }
            $this->oldItems = $this->newItems;
            $this->newItems = [];

            return true;
        } else {
            \common\components\helpers\ModelHelper::logErrors($this->rentAgreement, __METHOD__);
        }

        return false;
    }

    /**
     * @return array
     */
    public function addNewItem(RentAgreementItem $item)
    {
        return $this->newItems[$item->id] = $item;
    }

    /**
     * @return array
     */
    public function getRentAgreement()
    {
        return $this->rentAgreement;
    }

    /**
     * @return array
     */
    public function getEntityIdData()
    {
        return ArrayHelper::map(Entity::findActual($this->company_id)->select([
            'id',
            'title',
        ])->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    /**
     * @return int
     */
    public function getPeriodDays()
    {
        $date_begin = date_create($this->date_begin);
        $date_end = date_create($this->date_end);
        if ($date_begin <= $date_end) {
            $date_diff = date_diff($date_begin, $date_end)->format('%a')+1;
        } else {
            $date_diff = 0;
        }

        return $date_diff;
    }

    /**
     * @return int
     */
    public function getPaymentCount()
    {
        switch ($this->payment_period) {
            case RentAgreement::PAYMENT_PERIOD_DAY:
                $count = $this->getPeriodDays();
                break;

            case RentAgreement::PAYMENT_PERIOD_MONTH:
                $count = round($this->getPeriodDays()/30, 1);
                break;

            case RentAgreement::PAYMENT_PERIOD_YEAR:
                $count = round($this->getPeriodDays()/365, 1);
                break;

            case RentAgreement::PAYMENT_PERIOD_ALL:
            default:
                $count = 1;
                break;
        }

        return max(1, $count);
    }
}
