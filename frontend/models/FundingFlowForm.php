<?php
/**
 * Created by konstantin.
 * Date: 14.7.15
 * Time: 15.32
 */

namespace frontend\models;


use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFactory;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashFundingType;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use yii\base\Model;

/**
 * Class FundingFlowForm
 * @package frontend\models
 *
 * @property CashFundingType $fundingType
 */
class FundingFlowForm extends Model
{

    /**
     * @var array
     */
    public static $flowArray = [
        CashFactory::TYPE_BANK => 'Банк',
        CashFactory::TYPE_EMONEY => 'E-money',
        CashFactory::TYPE_ORDER => 'Касса',
    ];

    /**
     * @var
     */
    public $flow;
    /**
     * @var
     */
    public $flowType;
    /**
     * @var
     */
    public $sum;
    /**
     * @var
     */
    public $fundingTypeId;

    /**
     * @var CashFundingType
     */
    private $_fundingType;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['flow', 'flowType', 'sum', 'fundingTypeId',], 'required'],
            ['flow', 'in', 'range' => array_keys(self::$flowArray),
                'message' => '{attribute} (' . join(', ', self::$flowArray) . ') указан неверно.',
            ],
            ['flowType', 'in', 'range' => [CashFlowsBase::FLOW_TYPE_EXPENSE, CashFlowsBase::FLOW_TYPE_INCOME],
                'message' => '{attribute} указано неверно.',
            ],
            [['sum'], 'number', 'max' => \Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(\Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['sum'], 'compare', 'operator' => '>', 'compareValue' => 0,
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            ['fundingTypeId', 'in', 'range' => CashFundingType::find()->select('id')->column(),
                'message' => '{attribute} не найден.',
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'flow' => 'Тип операции',
            'flowType' => 'Направление операции',
            'sum' => 'Сумма',
            'fundingTypeId' => 'Тип финансирования',
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->sum = round(TextHelper::parseMoneyInput($this->sum) * 100); // input in rubles

        return parent::beforeValidate();
    }

    /**
     *
     */
    public function save()
    {

        $flowModel = $this->createFlow();

        $result = LogHelper::save($flowModel, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE);

        return $result;
    }


    /**
     * @return CashFlowsBase
     * @throws NotFoundHttpException
     */
    private function createFlow()
    {
        $flowClass = CashFactory::getCashClass($this->flow);
        /* @var CashFlowsBase $flowModel */
        $flowModel = new $flowClass();

        $flowModel->company_id = \Yii::$app->user->identity->company->id;
        $flowModel->contractor_id = Contractor::getCompanysFounder($flowModel->company_id)->id;

        $flowModel->date = date(DateHelper::FORMAT_USER_DATE);
        $flowModel->description = $this->fundingType->name;

        $flowModel->has_invoice = 0;
        $flowModel->invoice_id = null;
        $flowModel->expenditure_item_id = InvoiceExpenditureItem::ITEM_OTHER;
        $flowModel->flow_type = $this->flowType;

        $flowModel->amount = $this->sum / 100; // cash flow takes float on input.

        if ($flowModel->hasAttribute('is_accounting')) {
            $flowModel->is_accounting = 0;
        }

        if ($flowModel instanceof CashOrderFlows) {
            /* @var CashOrderFlows $flowModel */
            $flowModel->author_id = \Yii::$app->user->identity->id;
            $flowModel->number = (string) CashOrderFlows::getNextNumber($flowModel->company_id, $flowModel->flow_type);
            $flowModel->reason_id = CashOrdersReasonsTypes::VALUE_OTHER;
            $flowModel->other = $flowModel->description;
        }

        $flowModel->is_funding_flow = 1;
        $flowModel->cash_funding_type_id = $this->fundingTypeId;

        if ($flowModel instanceof CashBankFlows && $flowModel->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
            $flowModel->income_item_id = InvoiceIncomeItem::ITEM_OTHER;
        }

        return $flowModel;
    }

    /**
     * @return CashFundingType
     */
    public function getFundingType()
    {
        if ($this->_fundingType === null) {
            $this->_fundingType = CashFundingType::findOne($this->fundingTypeId);
        }

        return $this->_fundingType;
    }

    /**
     * @param CashFundingType $fundingType
     */
    public function setFundingType($fundingType)
    {
        $this->_fundingType = $fundingType;
    }

}