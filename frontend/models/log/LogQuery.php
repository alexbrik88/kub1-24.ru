<?php

namespace frontend\models\log;

use yii\db\ActiveQuery;
use common\models\ICompanyStrictQuery;

/**
 * Class ActQuery
 *
 * @package common\models\document
 */
class LogQuery extends ActiveQuery implements ICompanyStrictQuery
{

    /**
     *
     * @param $companyId
     * @return InvoiceQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            Log::tableName() . '.company_id' => $companyId,
        ]);
    }

}
