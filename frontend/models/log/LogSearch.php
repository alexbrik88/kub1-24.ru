<?php

namespace frontend\models\log;

use common\models\employee\EmployeeRole;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ActSearch represents the model behind the search form about `common\models\Act`.
 */
class LogSearch extends Log
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log_event_id', 'log_entity_type_id', 'author_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $dateRange Array
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var LogQuery $query */
        $query = Log::find();
        $query->with('author');

        $query->byCompany($this->company_id)->with([
            "employeeCompany",
            "logEntityType",
            "logEvent",
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'model_name' => $this->model_name,
            'model_id' => $this->model_id,
            'author_id' => $this->author_id,
            'log_entity_type_id' => $this->log_entity_type_id,
            'log_event_id' => $this->log_event_id,
        ]);

        $user = Yii::$app->user->identity;
        if ($user->currentRole->id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere([
                'author_id' => $user->id,
            ]);
        }

        return $dataProvider;
    }

}
