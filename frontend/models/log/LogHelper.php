<?php

namespace frontend\models\log;

use common\models\Company;
use common\models\cash\CashOrderFlows;
use common\models\employee\Employee;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * Class LogHelper
 * @package frontend\models\log
 *
 * @todo create component.
 */
class LogHelper
{
    /**
     *
     */
    const DEFAULT_MESSAGE = 'Сообщение не задано';

    /**
     * Whether to use transactions or not. For many operations it is probably more useful.
     * @var bool
     */
    public static $useTransaction = true;

    /**
     * @param ActiveRecord $model
     * @param integer $logEntityType
     * @param integer $logEvent
     * @param callable $callback
     * @param string|null $isolationLevel
     * @return bool
     * @throws \Exception
     */
    public static function save(ActiveRecord $model, $logEntityType, $logEvent, callable $callback = null, $isolationLevel = null)
    {
        if (!$model instanceof ILogMessage) {
            throw new Exception('Model must be an example of `ILogMessage`');
        }

        $useTransaction = Yii::$app->db->getTransaction() === null && self::$useTransaction;

        $saveClosure = function (\yii\db\Connection $db) use ($model, $logEntityType, $logEvent, $callback, $useTransaction) {
            if (
                ($callback === null ? $model->save() : call_user_func($callback, $model))
                &&
                LogHelper::log($model, $logEntityType, $logEvent)
            ) {
                return true;
            }
            if ($useTransaction) {
                $db->getTransaction()->rollBack();
            }
            return false;
        };

        if ($useTransaction) {
            return Yii::$app->db->transaction($saveClosure, $isolationLevel);
        } else {
            return call_user_func($saveClosure, Yii::$app->db);
        }
    }

    /**
     * @param ActiveRecord $model
     * @param $logEntityType
     * @param callable $callback
     * @param null $isolationLevel
     * @return mixed
     * @throws \Exception
     */
    public static function delete(ActiveRecord $model, $logEntityType, callable $callback = null, $isolationLevel = null)
    {
        if (!$model instanceof ILogMessage) {
            throw new Exception('Model must be an example of `ILogMessage`');
        }

        $useTransaction = Yii::$app->db->getTransaction() === null && self::$useTransaction;

        $deleteClosure = function (\yii\db\Connection $db) use ($model, $logEntityType, $callback, $useTransaction) {
            if (
                ($callback === null ? $model->delete() : call_user_func($callback, $model))
                &&
                LogHelper::log($model, $logEntityType, LogEvent::LOG_EVENT_DELETE)
            ) {
                return true;
            }
            if ($useTransaction) {
                $db->getTransaction()->rollBack();
            }

            return false;
        };

        if ($useTransaction) {
            return Yii::$app->db->transaction($deleteClosure, $isolationLevel);
        } else {
            return call_user_func($deleteClosure, Yii::$app->db);
        }
    }


    /**
     * Creates and saves log
     * @param ActiveRecord|ILogMessage $model
     * @param int|LogEntityType $logEntityType LogEntityType's id
     * @param int|LogEvent $logEvent
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function log(ActiveRecord $model, $logEntityType, $logEvent)
    {
        if (!$model instanceof ILogMessage) {
            throw new Exception('Model must be an example of `ILogMessage`');
        }
        if (!is_numeric($logEntityType) && !($logEntityType instanceof LogEntityType)) {
            throw new Exception('LogEntityType указан неверно.');
        }
        if (!is_numeric($logEvent) && !($logEvent instanceof LogEvent)) {
            throw new Exception('LogEvent указан неверно.');
        }

        $log = new Log();
        $log->author_id = static::getAuthor($model)->id ?? null;
        $log->company_id = $model->company->id;

        $log->log_entity_type_id = is_numeric($logEntityType) ? $logEntityType : $logEntityType->id;
        $log->log_event_id = is_numeric($logEvent) ? $logEvent : $logEvent->id;

        $log->model_name = $model->className();
        $log->model_id = $model->id;

        $log->attributesNewArray = $model->attributes;
        $log->attributesOldArray = $model->oldAttributes;

        // sets to null if event not defined.
        $log->message = $model->getLogMessage($log);

        if (empty($log->message)) {
            $log->message = self::DEFAULT_MESSAGE;
        }

        if ($log->save()) {
            return true;
        } else {
            Yii::error("log save errors:\n" . var_export($log->errors, true));

            return false;
        }
    }

    /**
     * @return common\models\employee\Employee
     */
    public static function getAuthor(ILogMessage $model)
    {
        if ($model->isNewRecord) {
            return $model->author;
        } elseif (Yii::$app->id == 'app-frontend' && $model->company->getEmployeeCompanies()->andWhere(['employee_id' => Yii::$app->user->id])->exists()) {
            return Yii::$app->user->identity;
        } else {
            return $model->company->employeeChief;
        }
    }

    /**
     * Generates log message. If can't - uses default message attribute in `Log` model.
     * @param Log $log
     * @return string
     */
    public static function getLogMessage(Log $log)
    {
        $message = $log->message;

        if (($model = self::getLogModel($log->model_name, $log->model_id)) !== null) {
            $message = $model->getLogMessage($log);
        }

        return $message;
    }

    public static function getLogIcon(Log $log)
    {
        $icon = ['', ''];

        if (($model = self::getLogModel($log->model_name, $log->model_id)) !== null) {
            $icon = $model->getLogIcon($log);
        }

        return $icon;
    }

    /**
     * @param $modelName
     * @param $modelId
     * @return ILogMessage|null
     */
    private static function getLogModel($modelName, $modelId = null)
    {
        $id = $modelId ? : 0;
        if (!isset(self::$_logModelArray[$modelName])) {
            self::$_logModelArray[$modelName] = [];
        }
        if (!array_key_exists($id, self::$_logModelArray[$modelName])) {
            self::$_logModelArray[$modelName][$id] = null;
            if (class_exists($modelName)) {
                $model = $modelName::findOne($id) ? : new $modelName;
                if ($model instanceof ILogMessage) {
                    self::$_logModelArray[$modelName][$id] = $model;
                }
            }
        }

        return self::$_logModelArray[$modelName][$id];
    }

    /**
     * @param $modelName
     * @return ILogMessage|null
     */
    private static function _old_getLogModel($modelName)
    {
        if (!isset(self::$_logModelArray[$modelName])) {
            self::$_logModelArray[$modelName] = null;
            if (class_exists($modelName)) {
                $model = new $modelName;
                if ($model instanceof ILogMessage) {
                    self::$_logModelArray[$modelName] = $model;
                }
            }
        }

        return self::$_logModelArray[$modelName];
    }

    /**
     * @var ILogMessage[]
     */
    private static $_logModelArray = [];

}
