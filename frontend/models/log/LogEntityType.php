<?php

namespace frontend\models\log;

use Yii;

/**
 * This is the model class for table "log_entity_type".
 *
 * @property integer $id
 * @property string $name
 */
class LogEntityType extends \yii\db\ActiveRecord
{
    const TYPE_DOCUMENT = 1;
    const TYPE_CASH = 2;
    const TYPE_PROJECT = 3;
    const TYPE_COMPANY_INDUSTRY = 4;
    const TYPE_SALE_POINT = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_entity_type';
    }

}
