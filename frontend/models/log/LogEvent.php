<?php

namespace frontend\models\log;

use Yii;

/**
 * This is the model class for table "log_event".
 *
 * @property integer $id
 * @property string $name
 */
class LogEvent extends \yii\db\ActiveRecord
{
    const LOG_EVENT_CREATE = 1;
    const LOG_EVENT_DELETE = 2;
    const LOG_EVENT_UPDATE = 3;
    const LOG_EVENT_UPDATE_STATUS = 4;
    const LOG_EVENT_RESPONSIBLE = 5;
    const LOG_EVENT_INDUSTRY = 6;
    const LOG_EVENT_PROJECT = 7;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_event';
    }
}
