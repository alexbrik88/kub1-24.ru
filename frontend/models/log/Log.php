<?php

namespace frontend\models\log;

use common\models\Company;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $company_id
 * @property integer $log_entity_type_id
 * @property integer $log_event_id
 * @property string $model_name
 * @property integer $model_id
 * @property string $attributes_new
 * @property string $attributes_old
 * @property string $message
 *
 * @property Company $company
 * @property Employee $author
 * @property EmployeeCompany $employeeCompany
 * @property LogEntityType $logEntityType
 * @property LogEvent $logEvent
 * @property yii\db\ActiveRecord $model
 *
 * @property array $attributesNewArray
 * @property array $attributesOldArray
 */
class Log extends \yii\db\ActiveRecord
{

    protected $_attributesNewArray;
    protected $_attributesOldArray;

    /**
     * @inheritdoc
     * @return LogQuery the newly created [[LogQuery]] instance.
     */
    public static function find()
    {
        return Yii::createObject(LogQuery::className(), [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'company_id', 'log_entity_type_id', 'log_event_id', 'model_name', 'model_id'], 'required'],
            [['created_at', 'author_id', 'company_id', 'log_entity_type_id', 'log_event_id', 'model_id'], 'integer'],
            [['attributes_new', 'attributes_old'], 'string'],
            [['model_name'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 2048],
            [
                ['author_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Employee::className(),
                'targetAttribute' => ['author_id' => 'id'],
            ],
            [
                ['company_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id'],
            ],
            [
                ['log_entity_type_id'], 'exist', 'skipOnError' => true,
                'targetClass' => LogEntityType::className(),
                'targetAttribute' => ['log_entity_type_id' => 'id'],
            ],
            [
                ['log_event_id'], 'exist', 'skipOnError' => true,
                'targetClass' => LogEvent::className(),
                'targetAttribute' => ['log_event_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'author_id' => 'Автор',
            'company_id' => 'Компания',
            'log_entity_type_id' => 'Log entity type',
            'log_event_id' => 'Log event',
            'model_name' => 'Model Name',
            'model_id' => 'Model ID',
            'attributes_new' => 'Attributes New',
            'attributes_old' => 'Attributes Old',
            'message' => 'Сообщение',
        ];
    }

    /**
     * @inheritdoc
     *
     * The default implementation returns the names of the columns whose values have been populated into this record.
     */
    public function fields()
    {
        $fields = [
            "id",
            "created_at",
            "author_id",
            "company_id",
            "log_entity_type_id",
            "log_event_id",
            "model_name",
            "model_id",
            "message",
            "attributesNewArray",
            "attributesOldArray",
        ];

        return array_combine($fields, $fields);
    }

    /**
     * @inheritdoc
     *
     * The default implementation returns the names of the columns whose values have been populated into this record.
     */
    public function extraFields()
    {
        $fields = [
            "logEntityType",
            "logEvent",
        ];

        return array_combine($fields, $fields);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), [
            'employee_id' => 'author_id',
            'company_id' => 'company_id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogEntityType()
    {
        return $this->hasOne(LogEntityType::className(), ['id' => 'log_entity_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogEvent()
    {
        return $this->hasOne(LogEvent::className(), ['id' => 'log_event_id']);
    }

    /**
     * @inheritdoc
     */
    public function setAttributesNewArray(array $value)
    {
        $this->_attributesNewArray = $value;
        $this->attributes_new = Json::encode($value);
    }

    /**
     * @return array
     */
    public function getAttributesNewArray()
    {
        if ($this->_attributesNewArray === null) {
            $this->_attributesNewArray = (array) Json::decode($this->attributes_new);
        }
        return $this->_attributesNewArray;
    }

    /**
     * @inheritdoc
     */
    public function setAttributesOldArray(array $value)
    {
        $this->_attributesOldArray = $value;
        $this->attributes_old = Json::encode($value);
    }

    /**
     * @return array
     */
    public function getAttributesOldArray()
    {
        if ($this->_attributesOldArray === null) {
            $this->_attributesOldArray = (array) Json::decode($this->attributes_old);
        }
        return $this->_attributesOldArray;
    }

    /**
     * @param  string $attribute
     * @return mixed
     */
    public function getModelAttributeNew($attribute)
    {
        return ArrayHelper::getValue($this->getAttributesNewArray(), $attribute);
    }

    /**
     * @param  string $attribute
     * @return mixed
     */
    public function getModelAttributeOld($attribute)
    {
        return ArrayHelper::getValue($this->getAttributesOldArray(), $attribute);
    }

    /**
     * @return yii\db\ActiveRecord
     */
    public function getModel()
    {
        $class = $this->model_name;

        if (class_exists($class)) {
            return $class::findOne($this->model_id);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-check';
//        fa fa-rub
//        fa fa-file-text-o
    }
}
