<?php

namespace frontend\models\log;


/**
 * Interface ILogMessage
 *
 * Interface must be implemented if Log functionality is used.
 *
 * @package common\models\log
 */
interface ILogMessage
{

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log);

    /**
     * Returns array for set color and icon. Used on displaying log messages.
     * @example: ['label-danger', 'fa fa-file-text-o']
     * @param Log $log
     * @return array
     */
    public function getLogIcon(Log $log);

    /**
     * @return common\models\Company
     */
    public function getCompany();

    /**
     * @return common\models\employee\Employee
     */
    public function getAuthor();

}