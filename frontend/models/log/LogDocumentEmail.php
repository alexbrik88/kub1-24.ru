<?php

namespace frontend\models\log;

use Yii;
use common\models\Company;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "log_document_email".
 *
 * @property int $id
 * @property int $company_id
 * @property string $email
 * @property int $model_id
 * @property string $model_name
 * @property int $created_at
 *
 * @property Company $company
 */
class LogDocumentEmail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_document_email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'email', 'model_id', 'model_name', 'created_at'], 'required'],
            [['company_id', 'model_id', 'created_at'], 'integer'],
            [['email', 'model_name'], 'string', 'max' => 255],
            [
                ['company_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Company::class,
                'targetAttribute' => ['company_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'email' => 'Email',
            'model_id' => 'Model ID',
            'model_name' => 'Model Name',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @param $email
     * @param $companyId
     * @param $modelId
     * @param $modelName
     * @return bool
     */
    public static function saveEmail($email, $companyId, $modelId, $modelName)
    {
        $logEmail = new self([
            'company_id' => $companyId,
            'model_id' => $modelId,
            'model_name' => $modelName,
            'email' => $email,
            'created_at' => time()
        ]);

        return $logEmail->save();
    }
}
