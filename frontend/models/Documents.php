<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 18.04.15
 * Time: 10:50
 */

namespace frontend\models;

use common\models\Agreement;
use common\models\document\AbstractDocument;
use common\models\document\AgentReport;
use common\models\document\GoodsCancellation;
use common\models\document\OrderDocument;
use common\models\document\Proxy;
use common\models\document\query\SalesInvoiceQuery;
use common\models\document\SalesInvoice;
use common\models\document\Waybill;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateDocuments;
use common\models\project\ProjectEstimateItem;
use common\models\project\ProjectEstimateItemDocuments;
use common\models\rent\RentAgreement;
use frontend\modules\documents\models\AgentReportSearch;
use frontend\modules\documents\models\AutoinvoiceSearch;
use common\models\document\Act;
use common\models\document\Autoinvoice;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use common\models\document\Upd;
use common\models\document\query\ForeignCurrencyInvoiceQuery;
use common\models\document\query\IIOStrictQuery;
use common\models\document\query\InvoiceFactureQuery;
use common\models\document\query\InvoiceQuery;
use common\models\document\query\PackingListQuery;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\ICompanyStrictQuery;
use Exception;
use frontend\models\log\Log;
use frontend\modules\documents\models\ActSearch;
use frontend\modules\documents\models\ForeignCurrencyInvoiceSearch;
use frontend\modules\documents\models\GoodsCancellationSearch;
use frontend\modules\documents\models\InvoiceFactureSearch;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\documents\models\PackingListSearch;
use frontend\modules\documents\models\ProxySearch;
use frontend\modules\documents\models\SalesInvoiceSearch;
use frontend\modules\documents\models\SpecificDocument;
use frontend\modules\documents\models\SpecificDocumentSearch;
use frontend\modules\documents\models\UpdSearch;
use frontend\modules\documents\models\WaybillSearch;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class Documents - класс реализует фабрику документов.
 *
 * @package frontend\models\
 */
class Documents
{

    /**
     * Входящие документы
     */
    const IO_TYPE_IN = 1;
    /**
     * Исходящие документы
     */
    const IO_TYPE_OUT = 2;
    /**
     * Исходящие документы
     */
    const TYPE_SPECIFIC = 3;

    /**
     * Входящие документ (url)
     */
    const IO_TYPE_IN_URL = 'in';
    /**
     * Исходящие документ (url)
     */
    const IO_TYPE_OUT_URL = 'out';

    /**
     * Отдельный раздел "мои документы"
     */
    const TYPE_SPECIFIC_URL = 'specific';
    /**
     * Вид документа - акт.
     */
    const DOCUMENT_ACT = 1;
    /**
     * Вид документа - счет.
     */
    const DOCUMENT_INVOICE = 2;
    /**
     * Вид документа - счет-фактура.
     */
    const DOCUMENT_INVOICE_FACTURE = 3;
    /**
     * Вид документа - товарные накладные(документ).
     */
    const DOCUMENT_PACKING_LIST = 4;
    /**
     * Вид документа - платёжное поручение.
     */
    const DOCUMENT_PAYMENT_ORDER = 5;
    /**
     * Вид документа - отдельный документ (раздел "Мои документы")
     */
    const DOCUMENT_SPECIFIC = 6;
    /**
     * Вид документа - шаблон.
     */
    const DOCUMENT_AUTOINVOICE = 7;
    /**
     * Вид документа - Универсальный передаточный документ.
     */
    const DOCUMENT_UPD = 8;
    /**
     * Вид документа - заказы.
     */
    const DOCUMENT_ORDER = 9;
    /**
     * Вид документа - договор.
     */
    const DOCUMENT_AGREEMENT = 11;
    /**
     * Вид документа - товарные накладные(документ).
     */
    const DOCUMENT_WAYBILL = 12;
    /**
     * Вид документа - доверенность.
     */
    const DOCUMENT_PROXY = 13;
    /**
     * Вид документа - агетнский отчет.
     */
    const DOCUMENT_AGENT_REPORT = 14;
    /**
     * Вид документа - Invoice - счет в иностранной валюте.
     */
    const DOCUMENT_FOREIGN_CURRENCY_INVOICE = 15;
    /**
     * Вид документа - договор аренды.
     */
    const DOCUMENT_RENT = 17;
    /**
     * Вид документа - Invoice - счет в иностранной валюте.
     */
    const DOCUMENT_SALES_INVOICE = 16;
    /**
     * Вид документа - Смета.
     */
    const DOCUMENT_PROJECT_ESTIMATE = 18;
    /**
     * Вид документа - Продукты в смете.
     */
    const DOCUMENT_PROJECT_ESTIMATE_ITEM = 19;
    /**
     * Вид документа - Списание товара
     */
    const DOCUMENT_GOODS_CANCELLATION = 20;

    /**
     * В счёте товар
     */
    const PRODUCTION_TYPE_GOOD = 1;
    /**
     * В счёте услуга
     */
    const PRODUCTION_TYPE_SERVICE = 0;
    /**
     * Счёт
     */
    const FILENAME_INVOICE = 'invoice';
    /**
     * Акт
     */
    const FILENAME_ACT = 'act';
    /**
     * Товарная накладная
     */
    const FILENAME_PACKING_LIST = 'packing_list';
    /**
     * Расходная накладная
     */
    const FILENAME_SALES_INVOICE = 'sales_invoice';
    /**
     * Счет-фактура
     */
    const FILENAME_INVOICE_FACTURE = 'invoice_facture';
    /**
     * Упд
     */
    const FILENAME_UPD = 'upd';
    /**
     * Invoice
     */
    const FILENAME_FOREIGN_CURRENCY_INVOICE = 'foreign_currency_invoice';
    /**
     * Авто-Счёт
     */
    const FILENAME_AUTOINVOICE = 'autoinvoice';

    /**
     * Счет
     */
    const SLUG_INVOICE = 'invoice';
    /**
     * Акт
     */
    const SLUG_ACT = 'act';
    /**
     * Товарная накладная
     */
    const SLUG_PACKING_LIST = 'packing-list';
    /**
     * Товарная накладная
     */
    const SLUG_SALES_INVOICE = 'sales-invoice';
    /**
     * Счет-фактура
     */
    const SLUG_INVOICE_FACTURE = 'invoice-facture';
    /**
     * Упд
     */
    const SLUG_UPD = 'upd';
    /**
     * Invoice
     */
    const SLUG_FOREIGN_CURRENCY_INVOICE = 'foreign-currency-invoice';
    /**
     * ТТН
     */
    const SLUG_WAYBILL = 'waybill';
    /**
     * Доверенность
     */
    const SLUG_PROXY = 'proxy';
    /**
     * Договор
     */
    const SLUG_AGREEMENT = 'agreement';
    /**
     * Агентский отчет
     */
    const SLUG_AGENT_REPORT = 'agent-report';
    /**
     * Упд
     */
    const SLUG_ORDER = 'order-document';
    /**
     * Смета
     */
    const SLUG_PROJECT_ESTIMATE = 'project-estimate';
    /**
     * Продукты в смете
     */
    const SLUG_PROJECT_ESTIMATE_ITEM = 'project-estimate-item';

    /**
     * Списание товара
     */
    const SLUG_GOODS_CANCELLATION = 'goods-cancellation';

    /**
     * @var array
     */
    public static $docMap = [
        self::DOCUMENT_ACT => Act::class,
        self::DOCUMENT_INVOICE => Invoice::class,
        self::DOCUMENT_INVOICE_FACTURE => InvoiceFacture::class,
        self::DOCUMENT_PACKING_LIST => PackingList::class,
        self::DOCUMENT_PAYMENT_ORDER => PaymentOrder::class,
        self::DOCUMENT_SPECIFIC => SpecificDocument::class,
        self::DOCUMENT_AUTOINVOICE => Autoinvoice::class,
        self::DOCUMENT_UPD => Upd::class,
        self::DOCUMENT_ORDER => OrderDocument::class,
        self::DOCUMENT_AGREEMENT => Agreement::class,
        self::DOCUMENT_WAYBILL => Waybill::class,
        self::DOCUMENT_PROXY => Proxy::class,
        self::DOCUMENT_AGENT_REPORT => AgentReport::class,
        self::DOCUMENT_FOREIGN_CURRENCY_INVOICE => ForeignCurrencyInvoice::class,
        self::DOCUMENT_PROJECT_ESTIMATE => ProjectEstimate::class,
        self::DOCUMENT_PROJECT_ESTIMATE_ITEM => ProjectEstimateItem::class,
        self::DOCUMENT_GOODS_CANCELLATION => GoodsCancellation::class
    ];

    /**
     * @var array
     */
    public static $slugList = [
        self::SLUG_ACT,
        self::SLUG_PACKING_LIST,
        self::SLUG_SALES_INVOICE,
        self::SLUG_INVOICE_FACTURE,
        self::SLUG_UPD,
        self::SLUG_PROXY,
        self::SLUG_AGENT_REPORT,
        self::SLUG_PROJECT_ESTIMATE,
        self::SLUG_PROJECT_ESTIMATE_ITEM,
        self::SLUG_WAYBILL,
        self::SLUG_GOODS_CANCELLATION
    ];
    /**
     * @var array
     */
    public static $slugToType = [
        self::SLUG_ACT => self::DOCUMENT_ACT,
        self::SLUG_PACKING_LIST => self::DOCUMENT_PACKING_LIST,
        self::SLUG_INVOICE_FACTURE => self::DOCUMENT_INVOICE_FACTURE,
        self::SLUG_UPD => self::DOCUMENT_UPD,
        self::SLUG_WAYBILL => self::DOCUMENT_WAYBILL,
        self::SLUG_PROXY => self::DOCUMENT_PROXY,
        self::SLUG_AGENT_REPORT => self::DOCUMENT_AGENT_REPORT,
        self::SLUG_PROJECT_ESTIMATE => self::DOCUMENT_PROJECT_ESTIMATE,
        self::SLUG_PROJECT_ESTIMATE_ITEM => self::DOCUMENT_PROJECT_ESTIMATE_ITEM,
        self::SLUG_GOODS_CANCELLATION => self::DOCUMENT_GOODS_CANCELLATION
    ];
    /**
     * @var array
     */
    public static $typeToSlug = [
        self::DOCUMENT_INVOICE => self::SLUG_INVOICE,
        self::DOCUMENT_ACT => self::SLUG_ACT,
        self::DOCUMENT_PACKING_LIST => self::SLUG_PACKING_LIST,
        self::DOCUMENT_INVOICE_FACTURE => self::SLUG_INVOICE_FACTURE,
        self::DOCUMENT_SALES_INVOICE => self::SLUG_SALES_INVOICE,
        self::DOCUMENT_UPD => self::SLUG_UPD,
        self::DOCUMENT_WAYBILL => self::SLUG_WAYBILL,
        self::DOCUMENT_PROXY => self::SLUG_PROXY,
        self::DOCUMENT_AGENT_REPORT => self::SLUG_AGENT_REPORT,
        self::DOCUMENT_ORDER => self::SLUG_ORDER,
        self::DOCUMENT_PROJECT_ESTIMATE => self::SLUG_PROJECT_ESTIMATE,
        self::DOCUMENT_PROJECT_ESTIMATE_ITEM => self::SLUG_PROJECT_ESTIMATE_ITEM,
        self::DOCUMENT_GOODS_CANCELLATION => self::SLUG_GOODS_CANCELLATION
    ];

    /**
     * @var array
     */
    public static $slugToName = [
        self::SLUG_ACT => 'Акт',
        self::SLUG_PACKING_LIST => 'ТН',
        self::SLUG_WAYBILL => 'ТТН',
        self::SLUG_INVOICE_FACTURE => 'СФ',
        self::SLUG_UPD => 'УПД',
        self::SLUG_PROXY => 'Доверенность',
        self::SLUG_AGENT_REPORT => 'Агетнский отчет',
        self::SLUG_PROJECT_ESTIMATE => 'Смета',
        self::SLUG_PROJECT_ESTIMATE_ITEM => 'Продукты в смете',
        self::SLUG_GOODS_CANCELLATION => 'Списание товара'
    ];

    public static $docName = [
        self::DOCUMENT_ACT => 'Акт',
        self::DOCUMENT_INVOICE => 'Счет',
        self::DOCUMENT_INVOICE_FACTURE => 'Счет фактура',
        self::DOCUMENT_PACKING_LIST => 'ТН',
        self::DOCUMENT_UPD => 'УПД',
        self::DOCUMENT_AGREEMENT => 'Договор',
        self::DOCUMENT_WAYBILL => 'ТТН',
        self::DOCUMENT_PROXY => 'Доверенность',
        self::DOCUMENT_AGENT_REPORT => 'Агентский отчет',
        self::DOCUMENT_PROJECT_ESTIMATE => 'Смета',
        self::DOCUMENT_PROJECT_ESTIMATE_ITEM => 'Смета',
        self::DOCUMENT_GOODS_CANCELLATION => 'Списание товара'
    ];

    /**
     * @var
     */
    protected static $_mapIdName;

    /**
     * Массив соответствия типа к url
     *
     * @var array
     */
    public static $ioTypeToUrl = [
        self::IO_TYPE_IN => self::IO_TYPE_IN_URL,
        self::IO_TYPE_OUT => self::IO_TYPE_OUT_URL,
        self::TYPE_SPECIFIC => self::TYPE_SPECIFIC_URL,
    ];

    /**
     * Returns document type ID by Model Class name
     * @param  string $class
     * @return integer|null
     */
    public static function typeIdByClass($class)
    {
        return is_string($class) ? (array_search($class, self::$docMap) ? : null) : null;
    }

    /**
     * Returns document type Name by document type ID
     * @return array
     */
    public static function mapIdName()
    {
        if (!isset(self::$_mapIdName)) {
            self::$_mapIdName = [];
            foreach (self::$docMap as $id => $class) {
                if (($obj = new $class) && !empty($obj->printablePrefix)) {
                    self::$_mapIdName[$id] = $obj->printablePrefix;
                }
            }
            asort(self::$_mapIdName);
        }

        return self::$_mapIdName;
    }

    /**
     * Returns document type Name by document type ID
     * @param  string $class
     * @return string|null
     */
    public static function typeNameById($id, $default = null)
    {
        return ArrayHelper::getValue(self::mapIdName(), $id, $default);
    }

    /**
     * Certain search provider
     * @param $typeSearcher
     * @param $ioType
     *
     * @return InvoiceSearch
     * @throws NotFoundHttpException
     */
    public static function loadSearchProvider($typeSearcher, $ioType)
    {
        switch ($typeSearcher) {
            case self::DOCUMENT_INVOICE:
                $searchModel = new InvoiceSearch();
                break;
            case self::DOCUMENT_INVOICE_FACTURE:
                $searchModel = new InvoiceFactureSearch();
                break;
            case self::DOCUMENT_ACT:
                $searchModel = new ActSearch();
                break;
            case self::DOCUMENT_PACKING_LIST:
                $searchModel = new PackingListSearch();
                break;
            case self::DOCUMENT_WAYBILL:
                $searchModel = new WaybillSearch();
                break;
            case self::DOCUMENT_SALES_INVOICE:
                $searchModel = new SalesInvoiceSearch();
                break;
            case self::DOCUMENT_PROXY:
                $searchModel = new ProxySearch();
                break;
            case self::DOCUMENT_SPECIFIC:
                $searchModel = new SpecificDocumentSearch();
                break;
            case self::DOCUMENT_AUTOINVOICE:
                $searchModel = new AutoinvoiceSearch();
                break;
            case self::DOCUMENT_UPD:
                $searchModel = new UpdSearch();
                break;
            case self::DOCUMENT_FOREIGN_CURRENCY_INVOICE:
                $searchModel = new ForeignCurrencyInvoiceSearch();
                break;
            case self::DOCUMENT_AGREEMENT:
                $searchModel = new AgreementSearch();
                break;
            case self::DOCUMENT_AGENT_REPORT:
                $searchModel = new AgentReportSearch();
                break;
            case self::DOCUMENT_GOODS_CANCELLATION:
                $searchModel = new GoodsCancellationSearch();
                break;
            default:
                throw new NotFoundHttpException();
        }

        $searchModel->type = $ioType;

        return $searchModel;
    }

    /**
     * @param ActiveRecord $model
     * @return int
     * @throws NotFoundHttpException
     */
    public static function getDocumentTypeByInstance($model)
    {
        if ($model instanceof Invoice) {
            $type = Documents::DOCUMENT_INVOICE;
        } elseif ($model instanceof InvoiceFacture) {
            $type = Documents::DOCUMENT_INVOICE_FACTURE;
        } elseif ($model instanceof Act) {
            $type = Documents::DOCUMENT_ACT;
        } elseif ($model instanceof Autoinvoice) {
            $type = Documents::DOCUMENT_AUTOINVOICE;
        } elseif ($model instanceof PackingList) {
            $type = Documents::DOCUMENT_PACKING_LIST;
        } elseif ($model instanceof Upd) {
            $type = Documents::DOCUMENT_UPD;
        } elseif ($model instanceof ForeignCurrencyInvoice) {
            $type = Documents::DOCUMENT_FOREIGN_CURRENCY_INVOICE;
        } elseif ($model instanceof OrderDocument) {
            $type = Documents::DOCUMENT_ORDER;
        } elseif ($model instanceof Agreement) {
            $type = Documents::DOCUMENT_AGREEMENT;
        } elseif ($model instanceof Waybill) {
            $type = Documents::DOCUMENT_WAYBILL;
        } elseif ($model instanceof AgentReport) {
            $type = Documents::DOCUMENT_AGENT_REPORT;
        } elseif ($model instanceof GoodsCancellation) {
            $type = Documents::DOCUMENT_GOODS_CANCELLATION;
        } else {
            throw new NotFoundHttpException();
        }

        return $type;
    }

    /**
     * Returns document model INSTANCE depending on document and io type
     *
     * @param int $id
     * @param int $typeSearcher
     * @param int $ioType
     *
     * @param bool $strictByCompany
     *
     * @return AbstractDocument
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public static function loadModel($id, $typeSearcher, $ioType, $strictByCompany = true)
    {
        $modelClass = self::getModel($typeSearcher);

        /* @var ActiveQuery|InvoiceQuery|InvoiceFactureQuery|PackingListQuery|SalesInvoiceQuery|Agreement $query */
        $query = $modelClass::find();

        if ($strictByCompany && $query instanceof ICompanyStrictQuery) {
            if (!(Yii::$app->user->identity instanceof Employee)) {
                throw new ForbiddenHttpException();
            }

            $query->byCompany(Yii::$app->user->identity->company->id);
        }

        if ($query instanceof IIOStrictQuery) {
            $query->byIOType($ioType);
        }

        $query->andWhere([
            $modelClass::tableName() . '.id' => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            //throw new NotFoundHttpException('Document load failed. Model with id `' . $id . '` not found.');
            $email = \Yii::$app->params['emailList']['support'];
            throw new NotFoundHttpException(
                "Загрузка документа не удалась. Попробуйте повторить попытку или обратитесь в службу технической поддержки <a href=\"mailto:$email\">$email</a></p>"
            );
        }

        return $model;
    }

    /**
     * Certain document model CLASS depending on document and io type
     * @param $typeSearcher
     * @return Act|Invoice|InvoiceFacture|PackingList|ActiveRecord
     * @throws NotFoundHttpException
     *
     */
    public static function getModel($typeSearcher)
    {
        /* @var ActiveRecord $class */
        switch ($typeSearcher) {
            case self::DOCUMENT_AUTOINVOICE:
                $class = Autoinvoice::className();
                break;
            case self::DOCUMENT_INVOICE:
                $class = Invoice::className();
                break;
            case self::DOCUMENT_INVOICE_FACTURE:
                $class = InvoiceFacture::className();
                break;
            case self::DOCUMENT_ACT:
                $class = Act::className();
                break;
            case self::DOCUMENT_PACKING_LIST:
                $class = PackingList::className();
                break;
            case self::DOCUMENT_SALES_INVOICE:
                $class = SalesInvoice::className();
                break;
            case self::DOCUMENT_PAYMENT_ORDER:
                $class = PaymentOrder::className();
                break;
            case self::DOCUMENT_SPECIFIC:
                $class = SpecificDocument::className();
                break;
            case self::DOCUMENT_UPD:
                $class = Upd::className();
                break;
            case self::DOCUMENT_FOREIGN_CURRENCY_INVOICE:
                $class = ForeignCurrencyInvoice::className();
                break;
            case self::DOCUMENT_AGREEMENT:
                $class = Agreement::className();
                break;
            case self::DOCUMENT_ORDER:
                $class = OrderDocument::className();
                break;
            case self::DOCUMENT_WAYBILL:
                $class = Waybill::className();
                break;
            case self::DOCUMENT_WAYBILL:
                $class = Waybill::className();
                break;
            case self::DOCUMENT_PROXY:
                $class = Proxy::className();
                break;
            case self::DOCUMENT_AGENT_REPORT:
                $class = AgentReport::className();
                break;
            case self::DOCUMENT_RENT:
                $class = RentAgreement::class;
                break;
            case self::DOCUMENT_PROJECT_ESTIMATE:
                $class = ProjectEstimateDocuments::class;
                break;
            case self::DOCUMENT_PROJECT_ESTIMATE_ITEM:
                $class = ProjectEstimateItemDocuments::class;
                break;
            case self::DOCUMENT_GOODS_CANCELLATION:
                $class = GoodsCancellation::class;
                break;
            default:
                throw new NotFoundHttpException('Model class not found.'.$typeSearcher);
        }

        return $class;
    }

    /**
     * Certain document model CLASS depending on document and io type
     * @param $typeSearcher
     *
     * @return Invoice|Act|PackingList|InvoiceFacture|ActiveRecord
     * @throws NotFoundHttpException
     */
    public static function getBaseModel($typeSearcher)
    {
        /* @var ActiveRecord $class */
        switch ($typeSearcher) {
            case self::DOCUMENT_INVOICE:
                $class = Invoice::className();
                break;
            case self::DOCUMENT_AUTOINVOICE:
                $class = Invoice::className();
                break;
            case self::DOCUMENT_INVOICE_FACTURE:
                $class = InvoiceFacture::className();
                break;
            case self::DOCUMENT_ACT:
                $class = Act::className();
                break;
            case self::DOCUMENT_PACKING_LIST:
                $class = PackingList::className();
                break;
            case self::DOCUMENT_SALES_INVOICE:
                $class = SalesInvoice::className();
                break;
            case self::DOCUMENT_PAYMENT_ORDER:
                $class = PaymentOrder::className();
                break;
            case self::DOCUMENT_SPECIFIC:
                $class = SpecificDocument::className();
                break;
            case self::DOCUMENT_UPD:
                $class = Upd::className();
                break;
            case self::DOCUMENT_FOREIGN_CURRENCY_INVOICE:
                $class = ForeignCurrencyInvoice::className();
                break;
            case self::DOCUMENT_AGREEMENT:
                $class = Agreement::className();
                break;
            case self::DOCUMENT_ORDER:
                $class = OrderDocument::className();
                break;
            case self::DOCUMENT_WAYBILL:
                $class = Waybill::className();
                break;
            case self::DOCUMENT_WAYBILL:
                $class = Waybill::className();
                break;
            case self::DOCUMENT_PROXY:
                $class = Proxy::className();
                break;
            case self::DOCUMENT_AGENT_REPORT:
                $class = AgentReport::className();
                break;
            case self::DOCUMENT_RENT:
                $class = RentAgreement::class;
                break;
            case self::DOCUMENT_PROJECT_ESTIMATE:
                $class = ProjectEstimateDocuments::class;
                break;
            case self::DOCUMENT_PROJECT_ESTIMATE_ITEM:
                $class = ProjectEstimateItemDocuments::class;
                break;
            case self::DOCUMENT_GOODS_CANCELLATION:
                $class = GoodsCancellation::class;
                break;
            default:
                throw new NotFoundHttpException('Model class not found.'.$typeSearcher);
        }

        return $class;
    }

    /**
     * Returns documents' search query for certain document model
     * @param integer $typeSearcher
     * @param integer $ioType
     *
     * @return ActiveQuery
     * @throws NotFoundHttpException
     */
    public static function getSearchQuery($typeSearcher, $ioType)
    {
        /* @var Invoice|Act|PackingList|InvoiceFacture|ActiveRecord $class */
        $class = self::getModel($typeSearcher);
        $query = $class::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($ioType);

        return $query;
    }

    /**
     * @param ActiveRecord $model
     * @param bool $absolutePath
     * @param string $primaryKey
     * @return string
     * @throws Exception
     */
    public static function getDocumentFilePath(ActiveRecord $model, $absolutePath = true, $primaryKey = 'id')
    {
        return Documents::getDocumentUploadPath($model, $absolutePath) . DIRECTORY_SEPARATOR . $model->{$primaryKey};
    }

    /**
     * Returns folder path for documents upload
     * @param ActiveRecord $model
     * @param bool $absolutePath
     *
     * @return string
     * @throws Exception
     */
    public static function getDocumentUploadPath($model, $absolutePath = true)
    {
        $filePath = DIRECTORY_SEPARATOR . 'documents' . DIRECTORY_SEPARATOR . $model->tableName();
        $fullPath = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . $filePath;

        if (!file_exists($fullPath)) {
            $parentDir = substr($fullPath, 0, strrpos($fullPath, DIRECTORY_SEPARATOR));
            if (is_writable($parentDir)) {
                mkdir($fullPath);
            } else {
                throw new Exception('Директория для записи не доступна.' . $fullPath);
            }
        }

        return $absolutePath ? $fullPath : $filePath;
    }

    public static function getLogIcon(Log $log)
    {
        $icon = ['', 'fa fa-file-text-o'];

        if ($log->getModelAttributeNew('type') == self::IO_TYPE_OUT) {
            $icon[0] = 'label-info';
        } else {
            $icon[0] = 'label-warning';
        }

        return $icon;
    }

    /**
     * Send email with any documents
     * @param  EmployeeCompany $sender  the current EmployeeCompany instance
     * @param  array           $models  array of models of documents sent
     * @param  array|string    $toEmail
     * @return bool
     */
    public static function sendEmail(EmployeeCompany $sender, $models, $toEmail)
    {
        $models = (array) $models;
        $subject = "Документы от " . $sender->company->getTitle(true);
        $params = [
            'models' => $models,
            'sender' => $sender,
            'subject' => $subject,
            'toEmail' => $toEmail,
            'pixel' => [
                'company_id' => $sender->company_id,
                'email' => is_array($toEmail) ? implode(',', $toEmail) : $toEmail,
            ],
        ];
        $message = Yii::$app->mailer->compose([
            'html' => "system/documents/any/html",
            'text' => "system/documents/any/text",
        ], $params)
            ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
            ->setReplyTo([$sender->employee->email => $sender->getFio(true)])
            ->setSubject($subject)
            ->setTo($toEmail);


        foreach ($models as $model) {
            if ($content = $model->pdfContent) {
                $message->attachContent($content, [
                    'fileName' => $model->getPdfFileName(),
                    'contentType' => 'application/pdf',
                ]);
            }
        }

        if ($message->send()) {
            foreach ($models as $model) {
                if (method_exists($model, 'updateSendStatus')) {
                    $model->updateSendStatus($sender->employee_id);
                }
            }

            return true;
        }

        return false;
    }
}
