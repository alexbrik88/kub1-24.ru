<?php

namespace frontend\models;

use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentProduct;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductInitialBalance;
use common\models\product\ProductStore;
use common\models\product\ProductTurnover;
use common\models\product\Store;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Cookie;

class ProductSearch extends Product
{
    public $balanceStart;
    public $balanseEnd;

    public $balanceStartValue;
    public $balanceEndValue;
    public $periodBuyValue;
    public $periodSaleValue;

    public $documentType;
    public $exclude;

    public $amountForBuy;
    public $amountForSell;

    public $reserveCount;
    public $availableCount;

    public $store_id;
    public $quantity;
    public $reserve;
    public $available;

    public $filterStatus = self::ALL;
    public $filterImage = self::ALL;
    public $filterComment = self::ALL;
    public $filterDate = self::ALL;
    public $storeArchive = false;

    public $dateFrom;
    public $dateTo;

    public $turnoverType = self::TURNOVER_BY_COUNT;

    const ALL = 0;

    const IN_ARCHIVE = 1;
    const IN_WORK = 2;
    const IN_RESERVE = 3;

    const HAS_IMAGE = 1;
    const NO_IMAGE = 2;

    const HAS_COMMENT = 1;
    const NO_COMMENT = 2;

    const FILTER_YESTERDAY = 1;
    const FILTER_TODAY = 2;
    const FILTER_WEEK = 3;
    const FILTER_MONTH = 4;


    const DEFAULT_SORTING_TITLE = 'title';
    const DEFAULT_SORTING_TITLE_EN = 'title_en';
    const DEFAULT_SORTING_GROUP_ID = 'group_id';

    const TURNOVER_BY_COUNT = 1;
    const TURNOVER_BY_AMOUNT = 2;

    private $_summary;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'production_type',
                    'title',
                    'unit_type',
                    'article',
                    'group_id',
                    'store_id',
                    'product_unit_id',
                    'documentType',
                    'exclude',
                    'balanceStart',
                    'balanseEnd',
                    'amountForBuy',
                    'amountForSell',
                    'reserveCount',
                    'availableCount',
                    'filterStatus',
                    'defaultSorting',
                    'filterImage',
                    'filterComment',
                    'filterDate',
                    'dateFrom',
                    'dateTo',
                    'turnoverType',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'quantity' => 'Количество на складе',
        ]);
    }

    /**
     * @return ProductQuery
     */
    public function baseQuery()
    {
        $query = ProductSearch::find()->alias('product')
            ->byCompany($this->company_id)
            ->byUser()
            ->notForSale($this->documentType == Documents::IO_TYPE_OUT ? false : null)
            ->andWhere(['product.production_type' => $this->production_type])
            ->andWhere(['product.is_deleted' => false])
            ->andWhere(['not', ['product.status' => Product::DELETED]])
            ->leftJoin(ProductGroup::tableName(), '{{product}}.[[group_id]] = {{product_group}}.[[id]]')
            ->leftJoin(['r' => Product::reserveQuery($this->company_id)], '{{product}}.[[id]] = {{r}}.[[product_id]]')
            ->leftJoin(['b' => '__product_balance'], '{{product}}.[[id]] = {{b}}.[[product_id]]');

        if (!empty($this->exclude)) {
            $query->andWhere(['not', ['product.id' => $this->exclude]]);
        }

        $query->andFilterWhere([
            'group_id' => $this->group_id,
            'product_unit_id' => $this->product_unit_id,
            'article' => $this->article,
            'unit_type' => $this->unit_type,
        ]);

        if ($this->storeArchive) {
            $query->byStatus(Product::ARCHIVE);
        } else {
            switch ($this->filterStatus) {
                case self::IN_ARCHIVE:
                    $query->byStatus(Product::ARCHIVE);
                    break;
                case self::IN_RESERVE:
                    $query->andWhere(['>', 'r.total', 0])
                        ->byStatus(Product::ACTIVE);
                    break;
                case self::ALL:
                    break;
                case self::IN_WORK:
                default:
                    $query->byStatus(Product::ACTIVE);
                    break;
            }
        }

        if ((int)$this->filterDate !== self::ALL) {
            $query->andWhere(['between',
                'date(from_unixtime(' . Product::tableName() . '.created_at))',
                DateHelper::format($this->dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE),
                DateHelper::format($this->dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE),
            ]);
        }

        if ($this->production_type == Product::PRODUCTION_TYPE_GOODS) {
            if ($this->filterImage == self::HAS_IMAGE) {
                $query->andWhere(['has_photo' => true]);
            } elseif ($this->filterImage == self::NO_IMAGE) {
                $query->andWhere(['has_photo' => false]);
            }

            if ($this->filterComment == self::HAS_COMMENT) {
                $query->andWhere(['and',
                    ['not', ['comment_photo' => null]],
                    ['not', ['comment_photo' => '']],
                ]);
            } elseif ($this->filterComment == self::NO_COMMENT) {
                $query->andWhere(['or',
                    ['comment_photo' => null],
                    ['comment_photo' => ''],
                ]);
            }
        }

        if ($this->title) {
            $query->andWhere([
                'or',
                ['like', Product::tableName() . '.title', $this->title],
                ['like', Product::tableName() . '.article', $this->title],
            ]);
        }

        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param string $formName
     * @param bool $hideZeroesQuantityProducts
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null, $hideZeroesQuantityProducts = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $this->load($params, $formName);

        $this->createTmpTable();

        $query = $this->baseQuery()->select([
            'quantity' => 'IFNULL({{b}}.[[balance]], 0)',
            'reserve' => 'IFNULL({{r}}.[[total]], 0)',
            'amountForBuy' => '(IFNULL({{product}}.[[price_for_buy_with_nds]], 0) * IFNULL({{b}}.[[balance]], 0))',
            'amountForSell' => '(IFNULL({{product}}.[[price_for_sell_with_nds]], 0) * IFNULL({{b}}.[[balance]], 0))',
        ]);

        $query->groupBy(Product::tableName() . '.id');

        $defaultSortingAttr = isset($params['defaultSorting']) ? $params['defaultSorting'] : self::DEFAULT_SORTING_TITLE;

        $sortAttributes = [
            'title',
            'title_en',
            'article',
            'quantity',
            'reserve',
            'available',
            'amountForSell',
            'reserveCount',
            'availableCount',
            'comment_photo',
            'price_for_buy_nds_id',
            'price_for_sell_with_nds',
            'price_for_sell_nds_id',
            'group_id' => [
                'asc' => ['product_group.title' => SORT_ASC,],
                'desc' => ['product_group.title' => SORT_DESC,],
            ],
        ];

        if ($user->currentEmployeeCompany && $user->currentEmployeeCompany->can_view_price_for_buy) {
            $sortAttributes[] = 'price_for_buy_with_nds';
            $sortAttributes[] = 'amountForBuy';
        }

        if (Product::PRODUCTION_TYPE_GOODS == $this->production_type && $hideZeroesQuantityProducts) {
            $query->andWhere(['>', 'b.balance', 0]);
        }

        $searchQuery = (clone $query);

        $summaryQuery = (new \yii\db\Query())->select([
            'quantity' => 'SUM([[quantity]])',
            'reserve' => 'SUM([[reserve]])',
            'reserve_amount' => 'SUM([[amountForReserve]])',
            'buy_amount' => 'SUM([[amountForBuy]])',
            'sell_amount' => 'SUM([[amountForSell]])',
        ])->from([
            't' => $query->addSelect([
                'amountForReserve' => '(IFNULL({{product}}.[[price_for_sell_with_nds]], 0) * IFNULL({{r}}.[[total]], 0))',
            ])
        ]);

        $this->_summary = $summaryQuery->all();

        $totalQuery = $this->baseQuery()->limit(-1)->offset(-1)->orderBy([]);
        if (Product::PRODUCTION_TYPE_GOODS == $this->production_type && $hideZeroesQuantityProducts) {
            $totalQuery->andWhere(['>', 'b.balance', 0]);
        }

        $searchQuery->addSelect([
            'product.*',
            'available' => 'GREATEST(IFNULL({{b}}.[[balance]], 0) - IFNULL({{r}}.[[total]], 0), 0)',
        ])->with('group')->with('productUnit');

        $dataProvider = new ActiveDataProvider([
            'query' => $searchQuery,
            'totalCount' => $totalQuery->count('product.id'),
            'pagination' => [
                'pageSize' => Yii::$app->request->get('per-page'),
            ],
            'sort' => [
                'attributes' => $sortAttributes,
                'defaultOrder' => [
                    $defaultSortingAttr => SORT_ASC,
                ],
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @return float
     */
    public function getAllQuantity()
    {
        return $this->_summary[0]['quantity'] ?? 0;
    }

    /**
     * @return float
     */
    public function getAllReserve()
    {
        return $this->_summary[0]['reserve'] ?? 0;
    }

    /**
     * @return float
     */
    public function getAllReserveAmount()
    {
        return $this->_summary[0]['reserve_amount'] ?? 0;
    }

    /**
     * @return integer
     */
    public function getAllAmountBuy()
    {
        return $this->_summary[0]['buy_amount'] ?? 0;
    }

    /**
     * @return integer
     */
    public function getAllAmountSell()
    {
        return $this->_summary[0]['sell_amount'] ?? 0;
    }

    protected function createTmpTable()
    {
        $db = Yii::$app->db;

        // todo: uncomment after adding `store_id` into `product_turnover` table
        //$query1 = (new \yii\db\Query())
        //    ->select([
        //        'product_id',
        //        'quantity' => 'IF([[type]]=2,-[[quantity]],[[quantity]])',
        //    ])
        //    ->from([
        //        't' => ProductTurnover::tableName()
        //    ])
        //    ->leftJoin([
        //        'p' => Product::tableName()
        //    ], '{{p}}.[[id]]={{t}}.[[product_id]]')
        //    ->andWhere([
        //        't.company_id' => $this->company_id,
        //        't.production_type' => Product::PRODUCTION_TYPE_GOODS,
        //        't.is_invoice_actual' => 1,
        //        't.is_document_actual' => 1,
        //        'p.is_deleted' => false
        //    ])
        //    ->andWhere(['not', ['p.status' => Product::DELETED]])
        //    ->andWhere([
        //        '<=',
        //        't.date',
        //        date('Y-m-d'),
        //    ]);
        //$query = (new \yii\db\Query())
        //    ->select([
        //        'product_id',
        //        'balance' => 'SUM([[quantity]])',
        //    ])
        //    ->from(['t' => $query1])
        //    ->groupBy('product_id');
        //$sql = $query->createCommand()->rawSql;

        // todo: remove after adding `store_id` into `product_turnover` table
        $companyId = $this->company_id;
        $storeId = is_array($this->store_id) ? null : $this->store_id;
        $isMainStore = ($storeId && ($store = Store::findOne($storeId))) ? $store->is_main : null;

        $command1 = Yii::$app->db->createCommand('
        
            SELECT product_id, SUM(quantity) AS balance FROM (
            
                    # quantity initial
                    SELECT 
                        SUM({{product_store}}.[[initial_quantity]]) AS [[quantity]],
                        {{product_store}}.[[product_id]]
                    FROM {{product}}
                        INNER JOIN {{product_store}} ON {{product}}.[[id]] = {{product_store}}.[[product_id]]
                    WHERE {{product}}.[[company_id]] = :company_id' . ($storeId
                        ? ' AND {{product_store}}.[[store_id]] = :store_id' : '') . '
                    GROUP BY {{product_store}}.[[product_id]]                   
    
                    UNION ALL
    
                    # quantity by packing list
                    SELECT 
                        SUM(IF({{packing_list}}.[[type]] = 1, 1, -1) * {{order_packing_list}}.[[quantity]]) AS [[quantity]],
                        {{order_packing_list}}.[[product_id]] 
                    FROM {{order_packing_list}}
                        INNER JOIN {{packing_list}} ON {{packing_list}}.[[id]] = {{order_packing_list}}.[[packing_list_id]]
                        INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{packing_list}}.[[invoice_id]]
                    WHERE {{invoice}}.[[company_id]] = :company_id' . ($storeId
                            ? (' AND ({{invoice}}.[[store_id]] = :store_id' . ($isMainStore
                                ? ' OR {{invoice}}.[[store_id]] IS NULL' : '') . ')') : '') . '
                        AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9) 
                        AND {{invoice}}.[[is_deleted]] = false
                        AND ({{packing_list}}.[[status_out_id]] IS NULL OR {{packing_list}}.[[status_out_id]] <> 5)
                    GROUP BY {{order_packing_list}}.[[product_id]]
                    
                    UNION ALL
                    
                    # quantity by upd
                    SELECT
                        SUM(IF({{upd}}.[[type]] = 1, 1, -1) * {{order_upd}}.[[quantity]]) AS [[quantity]],
                        {{order_upd}}.[[product_id]]
                    FROM {{order_upd}}
                        INNER JOIN {{upd}} ON {{upd}}.[[id]] = {{order_upd}}.[[upd_id]]
                    WHERE {{upd}}.[[id]] IN (                    
                        SELECT u.id FROM {{upd}} u
                            INNER JOIN {{invoice_upd}} ON {{invoice_upd}}.[[upd_id]] = u.id
                            INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{invoice_upd}}.[[invoice_id]] 
                        WHERE {{invoice}}.[[company_id]] = :company_id' . ($storeId
                            ? (' AND ({{invoice}}.[[store_id]] = :store_id' . ($isMainStore
                                ? ' OR {{invoice}}.[[store_id]] IS NULL' : '') . ')') : '') . '
                            AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                            AND {{invoice}}.[[is_deleted]] = false
                        )                    
                    GROUP BY {{order_upd}}.[[product_id]] 
                    
                    UNION ALL
                    
                    # quantity by sales invoices
                    SELECT 
                        SUM(IF({{sales_invoice}}.[[type]] = 1, 1, -1) * {{order_sales_invoice}}.[[quantity]]) AS [[quantity]],
                        {{order_sales_invoice}}.[[product_id]] 
                    FROM {{order_sales_invoice}}
                        INNER JOIN {{sales_invoice}} ON {{sales_invoice}}.[[id]] = {{order_sales_invoice}}.[[sales_invoice_id]]
                        INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{sales_invoice}}.[[invoice_id]]
                    WHERE {{invoice}}.[[company_id]] = :company_id' . ($storeId
                            ? (' AND ({{invoice}}.[[store_id]] = :store_id' . ($isMainStore
                                ? ' OR {{invoice}}.[[store_id]] IS NULL' : '') . ')') : '') . '          
                        AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9) 
                        AND {{invoice}}.[[is_deleted]] = false
                        AND ({{sales_invoice}}.[[status_out_id]] IS NULL OR {{sales_invoice}}.[[status_out_id]] <> 5)
                    GROUP BY {{order_sales_invoice}}.[[product_id]]
                ) `0` GROUP BY product_id
        ', [
            ':company_id' => $companyId,
            ':store_id' => $storeId,
        ]);

        $sql = $command1->rawSql;

        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __product_balance");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE `__product_balance` (
            `product_id` INT(11) NOT NULL,
            `balance` DECIMAL(20,10) NOT NULL,
            PRIMARY KEY (`product_id`)
        )");
        $comand->execute();

        if (Product::PRODUCTION_TYPE_GOODS == $this->production_type) {
            $comand = $db->createCommand("INSERT INTO __product_balance (`product_id`, `balance`) {$sql}");
            $comand->execute();
        }
    }

}
