<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.05.2018
 * Time: 7:40
 */

namespace frontend\models;


use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\EmployeeRole;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use Yii;
use yii\data\ActiveDataProvider;
use common\models\employee\Employee;

/**
 * Class PaymentReminderMessageContractorSearch
 * @package frontend\models
 */
class PaymentReminderMessageContractorSearch extends PaymentReminderMessageContractor
{
    /**
     * @var
     */
    public $contractor;

    /**
     * @var array
     */
    public $messageStatus = [
        null => null,
        1 => PaymentReminderMessageContractor::MESSAGE_ACTIVE,
        2 => PaymentReminderMessageContractor::MESSAGE_INACTIVE,
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contractor', 'message_1', 'message_2', 'message_3', 'message_4', 'message_5', 'message_6', 'message_7',
                'message_8', 'message_9', 'message_10',], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $query = PaymentReminderMessageContractor::find()
            ->select([
                PaymentReminderMessageContractor::tableName() . '.*',
                'invoiceSum' => 'SUM(inv.sum)',
            ])
            ->joinWith('contractor')
            ->leftJoin([
                'contractorType' => CompanyType::tableName()
            ], 'contractorType.id = ' . Contractor::tableName() . '.company_type_id')
            ->leftJoin([
                'inv' => Invoice::find()
                    ->select([
                        'invoice.*',
                        'sum' => '(IFNULL([[total_amount_with_nds]], 0) - IFNULL([[payment_partial_amount]], 0))'
                    ])
                    ->byCompany($user->company->id)
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->byDeleted(Invoice::NOT_IS_DELETED)
                    ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_REJECTED]]])
                    ->andWhere(['<', Invoice::tableName() . '.payment_limit_date', date('Y-m-d')])
                    ->andWhere(['<=', 'invoice.payment_limit_date', (new \DateTime("- 1 days"))->format('Y-m-d')])
            ], 'inv.contractor_id = ' . Contractor::tableName() . '.id')
            ->andWhere([PaymentReminderMessageContractor::tableName() . '.company_id' => $this->company_id]);
        if ($user->employee_role_id == EmployeeRole::ROLE_MANAGER) {
            $query->andWhere([Contractor::tableName() . '.responsible_employee_id' => $user->id]);
        }

        $query->groupBy(PaymentReminderMessageContractor::tableName() . '.contractor_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'contractor' => [
                        'asc' => [
                            Contractor::tableName() . '.face_type' => SORT_ASC,
                            'contractorType.name_short' => SORT_ASC,
                            Contractor::tableName() . '.name' => SORT_ASC,
                        ],
                        'desc' => [
                            Contractor::tableName() . '.face_type' => SORT_DESC,
                            'contractorType.name_short' => SORT_DESC,
                            Contractor::tableName() . '.name' => SORT_DESC,
                        ],
                        'default' => SORT_ASC,
                    ],
                    'invoiceSum',
                ],
                'defaultOrder' => [
                    'contractor' => SORT_ASC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([PaymentReminderMessageContractor::tableName() . '.contractor_id' => $this->contractor]);

        foreach (range(1, 10) as $number) {
            $attribute = 'message_' . $number;
            $query->andFilterWhere([PaymentReminderMessageContractor::tableName() . '.' . $attribute => $this->messageStatus[$this->$attribute]]);
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getMessageFilter()
    {
        return [
            null => 'Все',
            1 => 'Включено',
            2 => 'Выключено',
        ];
    }

    /**
     * @return array
     */
    public function getContractorFilter()
    {
        return [
            '' => 'Все',
        ] + Contractor::getSorted()->select([
            'contractor_name' => 'CONCAT_WS(" ", {{company_type}}.[[name_short]], {{contractor}}.[[name]])',
            'contractor.id',
        ])->joinWith('paymentReminderMessageContractor')->andWhere([
            PaymentReminderMessageContractor::tableName() . '.company_id' => $this->company_id,
        ])->indexBy('id')->column();
    }
}