<?php

namespace frontend\models;

use common\components\date\DateHelper;
use common\components\sender\unisender\UniSender;
use common\components\validators\PhoneValidator;
use common\models\Company;
use common\models\company\CompanyNotification;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\CompanyProductType;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\notification\Notification;
use common\models\TimeZone;
use frontend\components\StatisticPeriod;
use Yii;
use yii\base\Model;
use yii\db\Connection;

/**
 * Class RobotForm
 * @package frontend\models
 */
class RobotForm extends Model
{
    public $tax;
    public $envd;
    public $patent;
    public $worker;
    public $cashbox;
    public $clientbank;
    public $username;
    public $phone;
    public $email;
    public $req;

    public static $taxArray = [
        CompanyTaxationType::INCOME => 'УСН «Доходы»',
        CompanyTaxationType::INCOME_EXPENSES => 'УСН «Доходы минус расходы»',
    ];

    private $_company;
    private $_user;
    private $_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tax', 'username', 'phone', 'email', 'worker', 'cashbox', 'clientbank'], 'required'],
            [['tax'], 'in', 'range' => array_keys(self::$taxArray)],
            [['envd', 'patent', 'worker', 'cashbox', 'clientbank'], 'boolean'],
            [['email'], 'email'],
            [
                ['email'], 'unique',
                'targetClass' => Employee::class,
                'targetAttribute' => 'email',
                'filter' => [
                    'is_active' => true,
                    'is_deleted' => false,
                ]
            ],
            [['phone'], PhoneValidator::className()],
            [['req'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'tax' => 'Ваша система налогообложения',
            'envd' => 'ЕНВД',
            'patent' => 'Патент',
            'worker' => 'У вас есть сотрудники?',
            'cashbox' => 'Вы работаете через кассу?',
            'clientbank' => 'У вас есть Клиент-Банк?',
            'username' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'E-mail',
        ];
    }

    /**
     * @param null $req
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function registration()
    {
        if (!empty(Yii::$app->params['uniSender']['robotListId'])) {
            $uniSender = new UniSender();

            $data = [
                'field_names' => [
                    'email',
                    'email_list_ids',
                    'Name',
                    'sno',
                    'sno-envd',
                    'sno-patent',
                    'employees',
                    'kassa',
                    'klient-bank',
                ],
                'data' => [
                    [
                        $this->email,
                        Yii::$app->params['uniSender']['robotListId'],
                        $this->username,
                        self::$taxArray[$this->tax],
                        $this->envd,
                        $this->patent,
                        $this->worker,
                        $this->cashbox,
                        $this->clientbank,
                    ],
                ],
            ];

            $uniSender->importContacts($data);
        }

        if ($this->tax != CompanyTaxationType::INCOME_EXPENSES) {
            Yii::$app->mailer->htmlLayout = 'layouts/document-html';
            Yii::$app->mailer->compose([
                    'html' => 'system/taxrobot-no-register/html',
                    'text' => 'system/taxrobot-no-register/text',
                ], [
                    'subject' => 'Робот-Бухгалтер для ООО и ИП',
                ])
                ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                ->setTo($this->email)
                ->setSubject('Робот-Бухгалтер для ООО и ИП')
                ->send();

            return false;
        }

        return \Yii::$app->db->transaction(function (Connection $db) {
            $password = substr(uniqid(), -6);
            // create user
            $this->_user = new Employee([
                'employee_role_id' => EmployeeRole::ROLE_CHIEF,
                'email' => $this->email,
                'is_registration_completed' => false,
                'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
                'statistic_range_date_from' => StatisticPeriod::getDefaultPeriod()['from'],
                'statistic_range_date_to' => StatisticPeriod::getDefaultPeriod()['to'],
                'statistic_range_name' => StatisticPeriod::DEFAULT_PERIOD,
            ]);
            $this->_user->setPassword($password);

            // save user
            if ($this->_user->save(false)) {
                // create company
                $this->_company = new Company([
                    'scenario' => Company::SCENARIO_REGISTRATION,
                    'owner_employee_id' => $this->_user->id,
                    'created_by' => $this->_user->id,
                    'company_type_id' => CompanyType::TYPE_IP,
                    'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
                    'email' => $this->email,
                    'phone' => $this->phone,
                    'test' => false,
                    'activation_type' => Company::ACTIVATION_EMPTY_PROFILE,
                    'new_template_order' => 1,
                ]);

                $this->_company->saveInvite($this->req);

                // save company
                if ($this->_company->save(false) && $this->_company->createTrialSubscribe()) {
                    $this->_company->main_id = $this->_company->id;
                    $this->_company->save(false, ['main_id']);
                    $companyTaxationType = new CompanyTaxationType([
                        'company_id' => $this->_company->id,
                        'osno' => false,
                        'usn' => true,
                        'envd' => $this->envd,
                        'psn' => $this->patent,
                        'usn_type' => CompanyTaxationType::INCOME_EXPENSES,
                        'usn_percent' => 6,
                    ]);
                    if ($companyTaxationType->save(false) && Contractor::createFounder($this->_company)) {
                        $employeeCompany = new EmployeeCompany([
                            'company_id' => $this->_company->id,
                        ]);
                        $employeeCompany->employee = $this->_user;
                        if ($employeeCompany->save(false)) {
                            $this->_user->updateAttributes([
                                'company_id' => $this->_company->id,
                                'main_company_id' => $this->_company->id,
                            ]);
                            $this->_password = $password;

                            return true;
                        }
                    }
                }
                if ($db->getTransaction()->isActive) {
                    $db->getTransaction()->rollBack();
                }
            }

            return false;
        });
    }

    /**
     * @return Employee
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->_password;
    }
}
