<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 30.08.2019
 * Time: 22:30
 */

namespace frontend\models\yandexDirect;

use common\models\employee\Employee;
use common\models\yandex\YandexDirectCampaignReport;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class YandexDirectCampaignReportSearch extends YandexDirectCampaignReport
{
    private $_query;
    public $minDate;
    public $maxDate;
    public $status = 'Идут показы';
    public $strategy;
    public $showingPlace;
    public $campaignName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_id', 'status', 'strategy', 'showingPlace', 'campaignName'], 'safe'],
        ];
    }

    public function search(Employee $user, $params = [], $period = null)
    {
        if ($period === null || !is_array($period)) {
            $period = StatisticPeriod::getSessionPeriod();
        }

        $query = YandexDirectCampaignReport::find()
            ->alias('cr')
            ->joinWith('campaign c')
            ->select([
                    'cr.id',
                    'campaign_id',
                    'cr.company_id',
                    'c.name as name',
                    'c.status as status',
                    'c.strategy as strategy',
                    'c.showing_place as showing_place',
                    'SUM(IFNULL([[c.daily_budget_amount]], 0)) as daily_budget_amount',
                    'cost' => 'SUM(IFNULL([[cost]], 0))',
                    'impressions_count' => 'SUM(IFNULL([[impressions_count]], 0))',
                    'clicks_count' => 'SUM(IFNULL([[clicks_count]], 0))',
                    'ctr' => 'AVG([[ctr]])', // %
                    'avg_cpc' => 'AVG([[avg_cpc]])', // %
                    'conversions_count' => 'SUM(IFNULL([[conversions_count]], 0))',
                    'conversion_rate' => 'AVG([[conversion_rate]])', // %
                    'goals_roi' => 'SUM(IFNULL([[goals_roi]], 0))',
                    'date',
                    'avg_page_views_count', // not used
                    'revenue' => 'SUM(IFNULL([[revenue]], 0))',
                ]
            )
            ->andWhere([
                'AND',
                //['employee_id' => $user->id],
                ['cr.company_id' => $user->currentEmployeeCompany->company_id],
                ['between', 'date', $period['from'], $period['to']]
            ])->groupBy('campaign_id');

        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'sort' => [
                'attributes' => [
                    'daily_budget_amount',
                    'cost',
                    'impressions_count',
                    'clicks_count',
                    'ctr',
                    'avg_cpc',
                    'conversions_count',
                    'conversion_rate',
                    'goals_roi',
                ],
                'defaultOrder' => [
                    'clicks_count' => SORT_DESC,
                ],
            ],
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'campaign_id' => $this->campaign_id,
            'c.status' => $this->status,
            'c.strategy' => $this->strategy,
            'c.showing_place' => $this->showingPlace,
        ]);

        if ($this->campaignName) {
            $query->andWhere(['like', 'c.name', $this->campaignName]);
        }

        $this->_query = clone($query);

        // min/max date
        $queryMinMax = YandexDirectCampaignReport::find()
            ->andWhere([
                'AND',
                //['employee_id' => $user->id],
                ['company_id' => $user->currentEmployeeCompany->company_id],
                ['between', 'date', $period['from'], $period['to']]
            ]);
        $this->minDate = (clone $queryMinMax)->min('date');
        $this->maxDate = (clone $queryMinMax)->max('date');

        return $dataProvider;
    }

    public function getStatisticPeriodName()
    {
        $name = StatisticPeriod::getSessionName();
        if ($this->minDate && $this->maxDate) {
            $statisticsPeriod = StatisticPeriod::getSessionPeriod();
            $searchPeriod = ['from' => strtotime($statisticsPeriod['from']), 'to' => strtotime($statisticsPeriod['to'])];
            $dataPeriod = ['from' => strtotime($this->minDate), 'to' => strtotime($this->maxDate)];

            if ($searchPeriod['from'] < $dataPeriod['from'] || $searchPeriod['to'] > $dataPeriod['to']) {
                $name = date('d.m.Y', $dataPeriod['from']) . ' - ' . date('d.m.Y', $dataPeriod['to']);
            }
        }

        return $name;
    }

    public function getCampaignFilterList()
    {
        $query = clone $this->_query;

        return ['' => 'Все'] + ArrayHelper::map($query->groupBy('campaign_id')->asArray()->all(), 'campaign_id', 'campaign.name');
    }

    public function getStatusFilterList()
    {
        $query = clone $this->_query;
        return ['' => 'Все'] + ArrayHelper::map($query->groupBy('c.status')->asArray()->all(), 'status', 'status');
    }

    public function getStrategyFilterList()
    {
        $query = clone $this->_query;
        return ['' => 'Все'] + ArrayHelper::map($query->groupBy('c.strategy')->asArray()->all(), 'strategy', 'strategy');
    }

    public function getPlacementFilterList()
    {
        $query = clone $this->_query;
        return ['' => 'Все'] + ArrayHelper::map($query->groupBy('c.showing_place')->asArray()->all(), 'showing_place', 'showing_place');
    }
}