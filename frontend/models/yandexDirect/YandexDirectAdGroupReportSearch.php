<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 30.08.2019
 * Time: 22:30
 */

namespace frontend\models\yandexDirect;

use common\models\employee\Employee;
use common\models\yandex\YandexDirectAdGroupReport;
use common\models\yandex\YandexDirectCampaignReport;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class YandexDirectAdGroupReportSearch extends YandexDirectAdGroupReport
{

    private $_query;
    public $minDate;
    public $maxDate;
    public $status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ad_group_id', 'status'], 'safe'],
        ];
    }

    public function search(Employee $user, YandexDirectCampaignReport $campaign, $params = [], $period = null)
    {
        if ($period === null || !is_array($period)) {
            $period = StatisticPeriod::getSessionPeriod();
        }

        $query = YandexDirectAdGroupReport::find()
            ->alias('adgr')
            ->joinWith('adGroup adg')
            ->select([
                'adgr.id',
                'adgr.campaign_id',
                'adgr.company_id',
                'ad_group_id',
                'adg.name as name',
                'adg.status as status',
                'cost' => 'SUM(IFNULL([[cost]], 0))',
                'impressions_count' => 'SUM(IFNULL([[impressions_count]], 0))',
                'clicks_count' => 'SUM(IFNULL([[clicks_count]], 0))',
                'ctr' => 'AVG([[ctr]])', // %
                'avg_cpc' => 'AVG([[avg_cpc]])', // %
                'conversions_count' => 'SUM(IFNULL([[conversions_count]], 0))',
                'conversion_rate' => 'AVG([[conversion_rate]])', // %
                'goals_roi' => 'SUM(IFNULL([[goals_roi]], 0))',
                'date',
            ])
            ->andWhere([
                'AND',
                ['employee_id' => $user->id],
                ['adgr.company_id' => $user->currentEmployeeCompany->company_id],
                ['adgr.campaign_id' => $campaign->campaign_id],
                ['between', 'date', $period['from'], $period['to']]
            ])->groupBy('ad_group_id');

        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'sort' => [
                'attributes' => [
                    'cost',
                    'impressions_count',
                    'clicks_count',
                    'ctr',
                    'avg_cpc',
                    'conversions_count',
                    'conversion_rate',
                    'goals_roi',
                ],
                'defaultOrder' => [
                    'clicks_count' => SORT_DESC,
                ],
            ],
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'ad_group_id' => $this->ad_group_id,
            'adg.status' => $this->status,
        ]);

        $this->_query = clone($query);
        $this->minDate = (clone $query)->min('date');
        $this->maxDate = (clone $query)->max('date');

        return $dataProvider;
    }

    public function getAdGroupFilterList()
    {
        $query = clone $this->_query;
        return ['' => 'Все'] + ArrayHelper::map($query->groupBy('ad_group_id')->asArray()->all(), 'ad_group_id', 'name');
    }
    public function getStatusFilterList()
    {
        $query = clone $this->_query;
        return ['' => 'Все'] + ArrayHelper::map($query->groupBy('adg.status')->asArray()->all(), 'status', 'status');
    }
}