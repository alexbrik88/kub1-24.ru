<?php

namespace frontend\models;

use common\models\Company;
use common\models\product\Product;
use common\models\TaxRate;
use Yii;
use yii\base\Model;

/**
 * ProductNdsForm.
 */
class ProductNdsForm extends Model
{
    const CHANGE_ALL = 0;
    const CHANGE_SELECTED = 1;

    const PRICE_BUY = 'buy';
    const PRICE_SELL = 'sell';

    public $change;
    public $type;
    public $ids;
    public $price;
    public $from;
    public $to;
    public $is_alco;

    protected $_company;
    protected $_execCount = 0;

    public static $typeItems = [
        Product::PRODUCTION_TYPE_GOODS => 'Во всех товарах',
        Product::PRODUCTION_TYPE_SERVICE => 'Во всех услугах',
    ];

    public static $priceItems = [
        self::PRICE_BUY => 'Цена покупки',
        self::PRICE_SELL => 'Цена продажи',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['change', 'boolean'],
            [
                ['price', 'to'],
                'required',
                'message' => 'Необходимо выбрать',
            ],
            [
                ['type', 'from'],
                'required',
                'message' => 'Необходимо выбрать',
                'when' => function ($model) {
                    return $model->change != self::CHANGE_SELECTED;
                }
            ],
            [
                ['from', 'to'],
                'exist',
                'targetClass' => 'common\models\TaxRate',
                'targetAttribute' => 'id',
            ],
            [
                ['type'],
                'each',
                'skipOnEmpty' => true,
                'rule' => [
                    'in',
                    'range' => [
                        Product::PRODUCTION_TYPE_GOODS,
                        Product::PRODUCTION_TYPE_SERVICE,
                    ],
                ]
            ],
            [
                ['price'],
                'each',
                'skipOnEmpty' => false,
                'rule' => [
                    'in',
                    'range' => [
                        self::PRICE_BUY,
                        self::PRICE_SELL,
                    ],
                ]
            ],
            ['is_alco', 'safe'],
            ['ids', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'change' => 'Только в выбранных',
            'from' => 'Старое значение',
            'to' => 'Новое значение',
            'type' => 'Тип cписка',
            'price' => 'Тип цены',
        ];
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        if ($this->_company === null || !$this->validate()) {
            return false;
        }

        if ($this->change == self::CHANGE_ALL) {
            foreach ($this->type as $type) {
                foreach ($this->price as $price) {
                    Product::updateAll([
                        "price_for_{$price}_nds_id" => $this->to,
                    ], [
                        'company_id' => $this->_company->id,
                        'production_type' => $type,
                        "price_for_{$price}_nds_id" => $this->from,
                    ]);
                }
            }
        } elseif ($this->change == self::CHANGE_SELECTED) {
            foreach ($this->price as $price) {
                Product::updateAll([
                    "price_for_{$price}_nds_id" => $this->to,
                ], [
                    'company_id' => $this->_company->id,
                    'id' => $this->ids,
                ]);
            }
        }

        return true;
    }
}
