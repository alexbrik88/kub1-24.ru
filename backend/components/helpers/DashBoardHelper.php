<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.07.2016
 * Time: 5:34
 */

namespace backend\components\helpers;


use common\models\Agreement;
use common\models\bank\BankingParams;
use common\models\bank\BankUser;
use common\models\cash\CashBankStatementUpload;
use common\models\cash\CashOrderStatementUpload;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyInfoIndustry;
use common\models\company\CompanyInfoPlace;
use common\models\company\CompanyInfoTariff;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use common\models\CompanyQuery;
use common\models\ContractorQuery;
use common\models\CreateType;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\InvoiceFacture;
use common\models\document\OrderDocument;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use common\models\document\Upd;
use common\models\document\query\ActQuery;
use common\models\document\query\InvoiceFactureQuery;
use common\models\document\query\InvoiceQuery;
use common\models\document\query\PackingListQuery;
use common\models\document\query\PaymentOrderQuery;
use common\models\document\status\InvoiceStatus;
use common\models\employee\EmployeeClick;
use common\models\file\File;
use common\models\log\Log;
use common\models\ofd\OfdStatementUpload;
use common\models\out\OutInvoice;
use common\models\product\PriceList;
use common\models\report\ReportFile;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\Contractor;
use common\models\store\StoreCompanyContractor;
use frontend\models\Documents;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\modules\documents\models\SpecificDocument;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\export\ExportFiles;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use frontend\modules\cash\modules\banking\modules\sberbank\models\BankModel as SberbankModel;

/**
 * Class CompanyHelper
 * @package backend\components\helpers
 */
class DashBoardHelper
{
    const API_PARTNER_ANDROID = 1;

    /**
     * @var
     */
    protected $_dateRange;
    /**
     * @var int
     */
    protected $unixFrom;
    /**
     * @var int
     */
    protected $unixTill;
    /**
     * @var null
     */
    protected $_firstPayCompanies = null;
    /**
     * @var null
     */
    protected $_leavePaidCompanies = null;
    /**
     * @var null
     */
    protected $_returningCompanies = null;
    /**
     * @var null
     */
    protected $_repeatPaidCompanies = null;

    /**
     * @var array
     */
    public $companies = [];

    /**
     * @var integer|array
     */
    public $registration_page_type_id;

    /**
     * @var array
     */
    protected $cache = [];

    /**
     * @param $dateRange
     * @param array $config
     * @throws Exception
     */
    public function __construct($dateRange, $config = [])
    {
        if (!empty($dateRange)) {
            $this->_dateRange = $dateRange;
            $this->unixFrom = (new \DateTime($this->_dateRange['from']))->setTime(0, 0, 0)->getTimestamp();
            $this->unixTill = (new \DateTime($this->_dateRange['to']))->setTime(23, 59, 59)->getTimestamp();
        } else {
            throw new Exception('Date range can not be null');
        }
    }

    /**
     *  Кол-во компаний, у которых было оплачен сервис на начало выбранного периода
     *
     * @return int|string
     */
    public function getPaidInitialCompanies()
    {
        if (!isset($this->cache['paidInitialCount'])) {
            $this->cache['paidInitialCount'] = Subscribe::find()
                ->joinWith('company')
                ->andWhere([
                    'service_subscribe.tariff_id' => SubscribeTariff::paidStandartIds(),
                    'service_subscribe.status_id' => SubscribeStatus::STATUS_ACTIVATED,
                    'company.test' => false,
                ])
                ->andWhere(['<', 'service_subscribe.activated_at', $this->unixFrom])
                ->andWhere(['>=', 'service_subscribe.expired_at', $this->unixFrom])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->groupBy(Subscribe::tableName() . '.company_id')
                ->count();
        }

        return (int)$this->cache['paidInitialCount'];
    }

    /**
     *  Кол-во новых регистраций в сервисе за выбранный период
     *
     * @return int|string
     */
    public function getNewCompanies()
    {
        if (!isset($this->cache['newCompaniesCount'])) {
            $this->cache['newCompaniesCount'] = Company::find()
                ->andWhere(['between', 'created_at', $this->unixFrom, $this->unixTill])
                ->andWhere(['test' => false])
                ->andWhere(['blocked' => Company::UNBLOCKED])
                ->andFilterWhere(['registration_page_type_id' => $this->registration_page_type_id])
                ->count();
        }

        return (int)$this->cache['newCompaniesCount'];
    }

    /**
     * @return int|string
     */
    public function getNewTestCompanies()
    {
        if (!isset($this->cache['newTestCompaniesCount'])) {
            $this->cache['newTestCompaniesCount'] = Company::find()
                ->andWhere(['between', 'created_at', $this->unixFrom, $this->unixTill])
                ->andWhere(['test' => true])
                ->andWhere(['blocked' => Company::UNBLOCKED])
                ->andFilterWhere(['registration_page_type_id' => $this->registration_page_type_id])
                ->count();
        }

        return (int)$this->cache['newTestCompaniesCount'];
    }

    /**
     * @return int|string
     */
    public function getNewMainCompanies()
    {
        if (!isset($this->cache['newMainCompaniesCount'])) {
            $this->cache['newMainCompaniesCount'] = Company::find()
                ->andWhere('`' . Company::tableName() . '`.`id` = `' . Company::tableName() . '`.`main_id`')
                ->andWhere(['between', 'date(from_unixtime(`' . Company::tableName() . '`.`created_at`))', $this->_dateRange['from'], $this->_dateRange['to']])
                ->andWhere(['!=', 'test', Company::TEST_COMPANY])
                ->andWhere(['blocked' => Company::UNBLOCKED])
                ->andFilterWhere(['registration_page_type_id' => $this->registration_page_type_id])
                ->count();
        }

        return (int)$this->cache['newMainCompaniesCount'];
    }

    /**
     * @return int|string
     */
    public function getNewTestMainCompanies()
    {
        if (!isset($this->cache['newTestMainCompaniesCount'])) {
            $this->cache['newTestMainCompaniesCount'] = Company::find()
                ->andWhere('`' . Company::tableName() . '`.`id` = `' . Company::tableName() . '`.`main_id`')
                ->andWhere(['between', 'date(from_unixtime(`' . Company::tableName() . '`.`created_at`))', $this->_dateRange['from'], $this->_dateRange['to']])
                ->andWhere(['test' => Company::TEST_COMPANY])
                ->andWhere(['blocked' => Company::UNBLOCKED])
                ->andFilterWhere(['registration_page_type_id' => $this->registration_page_type_id])
                ->count();
        }
        return (int)$this->cache['newTestMainCompaniesCount'];
    }

    /**
     *  Кол-во новых регистраций в сервисе за выбранный период
     *
     * @return int|string
     */
    public function getNewMainCompaniesFromAndroid()
    {
        if (!isset($this->cache['newMainCompaniesCountFromAndroid'])) {
            $this->cache['newMainCompaniesCountFromAndroid'] = Company::find()
                ->andWhere('`' . Company::tableName() . '`.`id` = `' . Company::tableName() . '`.`main_id`')
                ->andWhere(['between', 'date(from_unixtime(`' . Company::tableName() . '`.`created_at`))', $this->_dateRange['from'], $this->_dateRange['to']])
                ->andWhere(['!=', 'test', Company::TEST_COMPANY])
                ->andWhere(['blocked' => Company::UNBLOCKED])
                ->andWhere(['registration_page_type_id' => RegistrationPageType::PAGE_TYPE_ANDROID])
                ->andFilterWhere(['registration_page_type_id' => $this->registration_page_type_id])
                ->count();
        }

        return (int)$this->cache['newMainCompaniesCountFromAndroid'];
    }

    /**
     * Кол-во всего компаний у кого был туториал в выбранном периоде.
     *   Считаем всех у кого туториал:
     *   1)    начался в периоде ДО выбранного периода, а закончился в выбранном периоде.
     *   2)    начался в периоде в выбранном периоде, и закончился в выбранном периоде.
     *   3)    начался в выбранном периоде, а закончился в периоде ПОСЛЕ выбранного периода.
     *
     * @return mixed
     */
    public function getTutorialCompanies()
    {
        if (!isset($this->cache['tutorialCompaniesCount'])) {
            $this->cache['tutorialCompaniesCount'] = Subscribe::find()
                ->alias('s')
                ->joinWith('company c')
                ->andWhere(['s.tariff_id' => SubscribeTariff::TARIFF_TRIAL])
                ->andWhere(['test' => false])
                ->andWhere(['<=', 's.activated_at', $this->unixTill])
                ->andWhere(['>=', 's.expired_at', $this->unixFrom])
                ->andFilterWhere(['c.registration_page_type_id' => $this->registration_page_type_id])
                ->groupBy('company_id')
                ->count();
        }
        return (int)$this->cache['tutorialCompaniesCount'];
    }

    /**
     *  Кол-во тех, у кого в выбранном периоде туториал закончился, но оплату они не сделали
     *
     * @return int
     */
    public function getNotTutorialCompanies()
    {
        if (!isset($this->cache['notTutorialCompaniesCount'])) {
            $this->cache['notTutorialCompaniesCount'] = Company::find()
                ->innerJoin(['trial' => Subscribe::tableName()], [
                    'trial.company_id' => new Expression('{{company}}.[[id]]'),
                    'trial.tariff_id' => SubscribeTariff::TARIFF_TRIAL
                ])
                ->leftJoin(['paid' => Subscribe::tableName()], [
                    'paid.company_id' => new Expression('{{company}}.[[id]]'),
                    'paid.tariff_id' => SubscribeTariff::paidStandartIds(),
                ])
                ->andWhere([
                    'company.test' => false,
                ])
                ->andWhere([
                    'between', 'trial.expired_at', $this->unixFrom, $this->unixTill
                ])
                ->andWhere([
                    'or',
                    ['paid.activated_at' => null,],
                    ['>', 'paid.activated_at', $this->unixTill],
                ])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->groupBy('company.id')
                ->count();
        }

        return (int)$this->cache['notTutorialCompaniesCount'];
    }

    /**
     *  Кол-во компаний, у которых был туториал в выбранном периоде и они оплатили счет в этом же периоде.
     *
     * @return int|string
     */
    public function getFirstPayCompanies()
    {
        if (!isset($this->cache['firstPayCompaniesCount'])) {
            $this->cache['firstPayCompaniesCount'] = count($this->getFirstPayCompaniesArray());
        }

        return $this->cache['firstPayCompaniesCount'];
    }

    /**
     *  Компании, у которых был туториал в выбранном периоде и они оплатили счет в этом же периоде.
     *
     * @return Company[]
     */
    public function getFirstPayCompaniesArray()
    {
        if ($this->_firstPayCompanies === null) {
            $this->_firstPayCompanies = Company::getSorted()
                ->select([
                    'company.id',
                    'name' => "IF(
                        {{company}}.[[company_type_id]] = :typeIP,
                        CONCAT({{company_type}}.[[name_short]], ' ', {{company}}.[[name_short]]),
                        CONCAT({{company_type}}.[[name_short]], ' \"', {{company}}.[[name_short]], '\"')
                    )",
                    'pay_at' => 'MIN({{payment}}.[[payment_date]])',
                ])
                ->joinWith(['subscribes subscribe', 'subscribes.payment payment'], false)
                ->andWhere([
                    'company.test' => false,
                    'payment.is_confirmed' => true,
                    'subscribe.tariff_id' => SubscribeTariff::paidStandartIds(),
                ])
                ->andWhere(['between', 'company.created_at', $this->unixFrom - (86400 * 14), $this->unixTill])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->having(['between', 'pay_at', $this->unixFrom, $this->unixTill])
                ->groupBy('company.id')
                ->indexBy('id')
                ->asArray()
                ->params([':typeIP' => CompanyType::TYPE_IP])
                ->all();
        }

        return $this->_firstPayCompanies;
    }

    /**
     *  = Значение 5 / значение 3 * 100%
     *
     * @return string
     */
    public function getTutorialCoefficient()
    {
        return round($this->getFirstPayCompanies() / ($this->getTutorialCompanies() ?: 1) * 100, 2) . '%';
    }

    /**
     *  Кол-во компаний, у которых в выбранном периоде, закончился оплаченный период и они НЕ сделали повторную оплату.
     *
     * @return int|string
     */
    public function getLeavePaidCompanies()
    {
        if (!isset($this->cache['leavePayCompaniesCount'])) {
            $this->cache['leavePayCompaniesCount'] = count($this->getLeavePaidCompaniesArray());
        }

        return $this->cache['leavePayCompaniesCount'];
    }

    /**
     *  Компании, у которых в выбранном периоде, закончился оплаченный период и они НЕ сделали повторную оплату.
     *
     * @return int|string
     */
    public function getLeavePaidCompaniesArray()
    {
        if ($this->_leavePaidCompanies === null) {
            $this->_leavePaidCompanies = Company::getSorted()
                ->select([
                    'company.id',
                    'name' => "IF(
                        {{company}}.[[company_type_id]] = :typeIP,
                        CONCAT({{company_type}}.[[name_short]], ' ', {{company}}.[[name_short]]),
                        CONCAT({{company_type}}.[[name_short]], ' \"', {{company}}.[[name_short]], '\"')
                    )",
                ])
                ->innerJoin(['expired' => Subscribe::tableName()], [
                    'and',
                    '{{expired}}.[[company_id]] = {{company}}.[[id]]',
                    ['expired.tariff_id' => SubscribeTariff::paidStandartIds()],
                    ['between', 'expired.expired_at', $this->unixFrom, $this->unixTill],
                ])
                ->leftJoin(['activated' => Subscribe::tableName()], [
                    'and',
                    '{{activated}}.[[company_id]] = {{company}}.[[id]]',
                    ['activated.tariff_id' => SubscribeTariff::paidStandartIds()],
                    ['or',
                        ['and',
                            '{{activated}}.[[activated_at]] >= {{expired}}.[[expired_at]]',
                            ['<=', 'activated.activated_at', $this->unixTill],
                        ],
                        ['{{activated}}.[[activated_at]]' => null],
                    ],
                ])
                ->andWhere([
                    'company.test' => false,
                    'activated.id' => null
                ])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->groupBy('company.id')
                ->indexBy('id')
                ->asArray()
                ->params([':typeIP' => CompanyType::TYPE_IP])
                ->all();
        }

        return $this->_leavePaidCompanies;
    }

    /**
     *  Кол-во компаний, у которых в выбранном периоде, закончился оплаченный период и они сделали повторную оплату (оплачен счет).
     *
     * @return int|string
     */
    public function getRepeatPaidCompanies()
    {
        if (!isset($this->cache['repeatPayCompaniesCount'])) {
            $this->cache['repeatPayCompaniesCount'] = count($this->getRepeatPaidCompaniesArray());
        }

        return $this->cache['repeatPayCompaniesCount'];
    }

    /**
     *  Компании, у которых в выбранном периоде, закончился оплаченный период и они сделали повторную оплату (оплачен счет).
     *
     * @return int|string
     */
    public function getRepeatPaidCompaniesArray()
    {
        if ($this->_repeatPaidCompanies === null) {
            $lastPaySubquery = Subscribe::find()
                ->select([
                    '*',
                    'pay_at' => 'MAX([[created_at]])',
                ])
                ->where([
                    'and',
                    ['tariff_id' => SubscribeTariff::paidStandartIds()],
                    ['status_id' => SubscribeStatus::$realSubscribes],
                    ['between', 'created_at', $this->unixFrom, $this->unixTill],
                ])
                ->groupBy('company_id');
            $beforePaySubquery = Subscribe::find()
                ->select([
                    'company_id',
                    'pay_count' => 'COUNT(*)',
                ])
                ->where([
                    'and',
                    ['tariff_id' => SubscribeTariff::paidStandartIds()],
                    ['status_id' => SubscribeStatus::$realSubscribes],
                    ['<', 'created_at', $this->unixTill],
                ])
                ->groupBy('company_id')
                ->having(['>', 'pay_count', 1]);
            $this->_repeatPaidCompanies = Company::getSorted()
                ->select([
                    'company.id',
                    'name' => "IF(
                        {{company}}.[[company_type_id]] = :typeIP,
                        CONCAT({{company_type}}.[[name_short]], ' ', {{company}}.[[name_short]]),
                        CONCAT({{company_type}}.[[name_short]], ' \"', {{company}}.[[name_short]], '\"')
                    )",
                ])
                ->innerJoin(['last_pay' => $lastPaySubquery], '{{company}}.[[id]] = {{last_pay}}.[[company_id]]')
                ->innerJoin(['more_pay' => $beforePaySubquery], '{{company}}.[[id]] = {{more_pay}}.[[company_id]]')
                ->andWhere(['company.test' => false])
                ->andWhere(['not', ['company.id' => array_keys($this->getReturningCompaniesArray())]])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->indexBy('id')
                ->asArray()
                ->params([':typeIP' => CompanyType::TYPE_IP])
                ->all();
        }

        return $this->_repeatPaidCompanies;
    }

    /**
     * @return int
     */
    public function getReturningCompanies()
    {
        if (!isset($this->cache['returningCompaniesCount'])) {
            $this->cache['returningCompaniesCount'] = count($this->getReturningCompaniesArray());
        }

        return $this->cache['returningCompaniesCount'];
    }

    /**
     * @return int
     */
    public function getReturningCompaniesArray()
    {
        if ($this->_returningCompanies === null) {
            $this->_returningCompanies = Company::getSorted()
                ->select([
                    'company.id',
                    'name' => "IF(
                        {{company}}.[[company_type_id]] = :typeIP,
                        CONCAT({{company_type}}.[[name_short]], ' ', {{company}}.[[name_short]]),
                        CONCAT({{company_type}}.[[name_short]], ' \"', {{company}}.[[name_short]], '\"')
                    )",
                    'last_expired' => 'MAX({{expired}}.[[expired_at]])',
                    'last_activated' => '({{activated}}.[[activated_at]] - 86400)',
                ])
                ->addSelect([
                    'company.*',
                ])
                ->innerJoin(['activated' => Subscribe::tableName()], [
                    'and',
                    ['activated.company_id' => new Expression('{{company}}.[[id]]')],
                    ['activated.tariff_id' => SubscribeTariff::paidStandartIds()],
                    ['<=', 'activated.activated_at', $this->unixTill],
                    ['>', 'activated.expired_at', $this->unixTill],
                ])
                ->leftJoin(['expired' => Subscribe::tableName()], [
                    'and',
                    ['expired.tariff_id' => SubscribeTariff::paidStandartIds()],
                    ['not', ['expired.tariff_id' => null]],
                    ['<', 'expired.expired_at', $this->unixTill],
                ])
                ->andWhere(['company.test' => false])
                ->andWhere(['not', ['activated.id' => null]])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->having('[[last_expired]] < [[last_activated]]')
                ->groupBy('company.id')
                ->indexBy('id')
                ->asArray()
                ->params([':typeIP' => CompanyType::TYPE_IP])
                ->all();
        }

        return $this->_returningCompanies;
    }

    /**
     *  = Значение 8 / (Значение 7 + значение 8) * 100
     *
     * @return string
     */
    public function getRepeatedSaleCoefficient()
    {
        $a = $this->getLeavePaidCompanies() + $this->getRepeatPaidCompanies();
        $b = $a ?: 1;

        return round($this->getRepeatPaidCompanies() / $b * 100, 2) . '%';
    }

    /**
     *  Кол-во компаний, у которых было оплачен сервис на конец выбранного периода
     *
     * @return int|string
     */
    public function getPaidEndPeriodCompanies()
    {
        return $this->getPaidInitialCompanies() + $this->getFirstPayCompanies() - $this->getLeavePaidCompanies() + $this->getReturningCompanies();
    }

    /**
     *  Оплаты за период
     *
     * @return int|string
     */
    public function getPeriodPaymentQuery()
    {
        return Payment::find()
            ->alias('payment')
            ->joinWith('company')
            ->andWhere(['and',
                ['payment.is_confirmed' => 1],
                ['payment.tariff_id' => SubscribeTariff::paidStandartIds()],
                ['not', ['type_id' => PaymentType::TYPE_PROMO_CODE]],
                ['company.test' => false],
                ['between', 'payment.payment_date', $this->unixFrom, $this->unixTill]
            ])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id]);
    }

    /**
     *  Сумма оплат за период
     *
     * @return mixed
     */
    public function getSum()
    {
        if (!isset($this->cache['periodPaymentSum'])) {
            $this->cache['periodPaymentSum'] = $this->getPeriodPaymentQuery()->sum('payment.sum');
        }

        return $this->cache['periodPaymentSum'];
    }

    /**
     *  Количество компаний сделавших оплаты в данном периоде
     *
     * @return mixed
     */
    public function getPeriodPayerCount()
    {
        if (!isset($this->cache['periodPayerCount'])) {
            $this->cache['periodPayerCount'] = $this->getPeriodPaymentQuery()->groupBy('payment.company_id')->count();
        }

        return (int)$this->cache['periodPayerCount'];
    }

    /**
     *  Сумма оплат за период/кол-во компаний сделавших эти оплаты в данном периоде
     *
     * @return float
     */
    public function getAverageCheck()
    {
        return round($this->getSum() / ($this->getPeriodPayerCount() ?: 1), 2);
    }

    /**
     * @return ContractorQuery
     */
    public function getContractor()
    {
        $this->companies = array_merge($this->companies, Contractor::find()
            ->joinWith('company')
            ->select('company.id')
            ->andWhere(['!=', 'test', Company::TEST_COMPANY])
            ->byStatus(Contractor::ACTIVE)
            ->byDeleted()
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->andWhere(['between', Contractor::tableName() . '.created_at', strtotime($this->_dateRange['from']), strtotime($this->_dateRange['to'])])->groupBy('company_id')->column());
        $this->companies = array_unique($this->companies);

        return Contractor::find()
            ->joinWith('company')
            ->andWhere(['!=', 'test', Company::TEST_COMPANY])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->byStatus(Contractor::ACTIVE)
            ->byDeleted()
            ->andWhere(['between', Contractor::tableName() . '.created_at', strtotime($this->_dateRange['from']), strtotime($this->_dateRange['to'])]);
    }

    /**
     * @return integer
     */
    public function getContractorCount($type)
    {
        if (!isset($this->cache['contractorCount'])) {
            $this->cache['contractorCount'] = [];
            $query = (new Query)->from(['contractor' => Contractor::tableName()])
                ->innerJoin(['company' => Company::tableName()], '{{contractor}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['contractor.is_deleted' => false])
                ->andWhere(['contractor.status' => Contractor::ACTIVE])
                ->andWhere(['between', 'contractor.created_at', $this->unixFrom, $this->unixTill]);
            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('company.id')->distinct()->column()));
            $result = $query
                ->select(['contractor.type', 'count' => 'COUNT(*)'])
                ->groupBy('contractor.type')
                ->all();

            foreach ($result as $row) {
                $this->cache['contractorCount'][$row['type']] = $row['count'];
            }
        }

        return isset($this->cache['contractorCount'][$type]) ? (int)$this->cache['contractorCount'][$type] : 0;
    }

    /**
     * @param $className
     * @param $ioType
     * @return InvoiceQuery|ActQuery|PackingListQuery|InvoiceFactureQuery|PaymentOrderQuery
     */
    public function getDocuments($className, $ioType)
    {
        /* @var Invoice|Act|PackingList|InvoiceFacture|PaymentOrder $className */
        $query = $className::find();
        if ($className == Invoice::className() || $className == InvoiceAuto::className()) {
            $query
                ->joinWith('company')
                ->byDeleted();
        } else {
            $query
                ->joinWith('invoice')
                ->leftJoin('company', 'company.id = invoice.company_id');
        }
        if ($className !== PaymentOrder::className()) {
            $query->byIOType($ioType);
        }
        $query->andWhere(['company.test' => false])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->byDocumentDateRange($this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59');

        $cloneQuery = clone $query;
        $this->companies = array_merge($this->companies, $cloneQuery->select('company.id')->column());
        $this->companies = array_unique($this->companies);

        return $query;
    }

    /**
     * @param $className (Invoice|InvoiceAuto|Act|PackingList|InvoiceFacture|PaymentOrder)::className()
     * @param $ioType
     * @return integer
     */
    public function getDocumentsCount($className, $ioType)
    {
        $docIds = [
            InvoiceAuto::className() => 0,
            Invoice::className() => 1,
            Act::className() => 2,
            PackingList::className() => 3,
            InvoiceFacture::className() => 4,
            Upd::className() => 5,
            PaymentOrder::className() => 6,
            Agreement::className() => 7,
        ];
        if (!isset($this->cache['documentsCount'])) {
            $this->cache['documentsCount'] = [];
            $typeOut = Documents::IO_TYPE_OUT;

            $subquery0 = (new Query)->from(['invoice' => InvoiceAuto::tableName()])
                ->select(["{$docIds[InvoiceAuto::className()]} AS [[doc_id]]", 'invoice.type', 'invoice.id', 'invoice.company_id'])
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $subquery1 = (new Query)->from(['invoice' => Invoice::tableName()])
                ->select(["{$docIds[Invoice::className()]} AS [[doc_id]]", 'invoice.type', 'invoice.id', 'invoice.company_id'])
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.is_invoice_contract' => 1]])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $subquery2 = (new Query)->from(['document' => Act::tableName()])
                ->select(["{$docIds[Act::className()]} AS [[doc_id]]", 'document.type', 'document.id', 'invoice.company_id'])
                ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $subquery3 = (new Query)->from(['document' => PackingList::tableName()])
                ->select(["{$docIds[PackingList::className()]} AS [[doc_id]]", 'document.type', 'document.id', 'invoice.company_id'])
                ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $subquery4 = (new Query)->from(['document' => InvoiceFacture::tableName()])
                ->select(["{$docIds[InvoiceFacture::className()]} AS [[doc_id]]", 'document.type', 'document.id', 'invoice.company_id'])
                ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $subquery5 = (new Query)->from(['document' => Upd::tableName()])
                ->select(["{$docIds[Upd::className()]} AS [[doc_id]]", 'document.type', 'document.id', 'invoice.company_id'])
                ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $subquery6 = (new Query)->from(['document' => PaymentOrder::tableName()])
                ->select(["{$docIds[PaymentOrder::className()]} AS [[doc_id]]", "$typeOut AS [[type]]", 'document.id', 'invoice.company_id'])
                ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $subquery7 = (new Query)->from(['agreement' => Agreement::tableName()])
                ->select(["{$docIds[Agreement::className()]} AS [[doc_id]]", "$typeOut AS [[type]]", 'agreement.id', 'agreement.company_id'])
                ->innerJoin(['company' => Company::tableName()], '{{agreement}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['not', ['agreement.agreement_template_id' => null]])
                ->andWhere(['agreement.is_created' => true])
                ->andWhere(['between', 'agreement.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $subqueryOut = (new Query)->from(['invoice' => Invoice::tableName()])
                ->select([
                    "{$docIds[Invoice::className()]} AS [[doc_id]]",
                    'type' => new Expression('"byOutLink"'),
                    'invoice.id',
                    'invoice.company_id'])
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['invoice.is_by_out_link' => true])
                ->andWhere(['not', ['invoice.is_invoice_contract' => 1]])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $subqueryInvCon = (new Query)->from(['invoice' => Invoice::tableName()])
                ->select([
                    "{$docIds[Invoice::className()]} AS [[doc_id]]",
                    'type' => new Expression('"invoiceContract"'),
                    'invoice.id',
                    'invoice.company_id'])
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['invoice.is_invoice_contract' => 1])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);

            $subquery = $subquery0
                ->union($subquery1)
                ->union($subquery2)
                ->union($subquery3)
                ->union($subquery4)
                ->union($subquery5)
                ->union($subquery6)
                ->union($subquery7)
                ->union($subqueryOut)
                ->union($subqueryInvCon);

            $companies = (new Query)->select('company_id')->distinct()->from(['t' => $subquery])->column();
            $this->companies = array_unique(array_merge($this->companies, $companies));

            $result = (new Query)
                ->select(['doc_id', 'type', 'count' => 'COUNT(*)'])
                ->from(['t' => $subquery])
                ->groupBy(['doc_id', 'type'])
                ->all();

            foreach ($result as $row) {
                $this->cache['documentsCount'][$row['type']][$row['doc_id']] = $row['count'];
            }
        }

        return isset($docIds[$className], $this->cache['documentsCount'][$ioType][$docIds[$className]]) ?
            (int)$this->cache['documentsCount'][$ioType][$docIds[$className]] : 0;
    }

    /**
     * @return InvoiceQuery
     */
    public function getInvoiceEmailMessages()
    {
        $query = Invoice::find()
            ->joinWith('company')
            ->andWhere(['!=', 'test', Company::TEST_COMPANY])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->byDeleted()
            ->byDocumentDateRange($this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59');
        $cloneQuery = clone $query;
        $this->companies = array_merge($this->companies, $cloneQuery->select('company.id')->column());
        $this->companies = array_unique($this->companies);

        return $query;
    }

    /**
     * @return integer
     */
    public function getSentInvoicesCount()
    {
        if (!isset($this->cache['sentInvoicesCount'])) {
            $query = Invoice::find()
                ->joinWith('company')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['>', 'invoice.email_messages', 0])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('company.id')->distinct()->column()));
            $this->cache['sentInvoicesCount'] = (int)$query->sum('email_messages');
        }

        return $this->cache['sentInvoicesCount'];
    }

    /**
     * @return CompanyQuery
     */
    public function getVisitCardEmailMessages()
    {
        $query = $this->getCompanies()->andWhere(['!=', 'test', Company::TEST_COMPANY]);
        $cloneQuery = clone $query;
        $this->companies = array_merge($this->companies, $cloneQuery->select('company.id')->column());
        $this->companies = array_unique($this->companies);

        return $query;
    }

    /**
     * @return CompanyQuery
     */
    public function getSentVisitCardCount()
    {
        if (!isset($this->cache['sentVisitCardCount'])) {
            $query = $this->getCompanies()->andWhere(['test' => false]);
            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('company.id')->column()));
            $this->cache['sentVisitCardCount'] = (int)$query->sum('email_messages_send_visit_card');
        }

        return $this->cache['sentVisitCardCount'];
    }

    /**
     * @param $className
     * @param $ioType
     * @return int
     */
    public function getFilesCount($className, $ioType)
    {
        if (!isset($this->cache['filesCount'])) {
            $subquery1 = File::find()
                ->select(['file.id', 'file.company_id', 'file.owner_model', 'owner.type'])
                ->leftJoin(['owner' => Invoice::tableName()], 'file.owner_id = owner.id')
                ->leftJoin(['company' => Company::tableName()], 'file.company_id = company.id')
                ->andWhere(['file.owner_model' => Invoice::className()])
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'file.created_at', $this->unixFrom, $this->unixTill]);
            $subquery2 = File::find()
                ->select(['file.id', 'file.company_id', 'file.owner_model', 'owner.type'])
                ->leftJoin(['owner' => Act::tableName()], 'file.owner_id = owner.id')
                ->leftJoin(['company' => Company::tableName()], 'file.company_id = company.id')
                ->andWhere(['file.owner_model' => Act::className()])
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'file.created_at', $this->unixFrom, $this->unixTill]);
            $subquery3 = File::find()
                ->select(['file.id', 'file.company_id', 'file.owner_model', 'owner.type'])
                ->leftJoin(['owner' => PackingList::tableName()], 'file.owner_id = owner.id')
                ->leftJoin(['company' => Company::tableName()], 'file.company_id = company.id')
                ->andWhere(['file.owner_model' => PackingList::className()])
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'file.created_at', $this->unixFrom, $this->unixTill]);
            $subquery4 = File::find()
                ->select(['file.id', 'file.company_id', 'file.owner_model', 'owner.type'])
                ->leftJoin(['owner' => InvoiceFacture::tableName()], 'file.owner_id = owner.id')
                ->leftJoin(['company' => Company::tableName()], 'file.company_id = company.id')
                ->andWhere(['file.owner_model' => InvoiceFacture::className()])
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'file.created_at', $this->unixFrom, $this->unixTill]);
            $subquery5 = File::find()
                ->select(['file.id', 'file.company_id', 'file.owner_model', 'owner.type'])
                ->leftJoin(['owner' => Upd::tableName()], 'file.owner_id = owner.id')
                ->leftJoin(['company' => Company::tableName()], 'file.company_id = company.id')
                ->andWhere(['file.owner_model' => Upd::className()])
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'file.created_at', $this->unixFrom, $this->unixTill]);

            $subquery = $subquery1->union($subquery2)->union($subquery3)->union($subquery4)->union($subquery5);

            $companies = (new Query)->select('company_id')->distinct()->from(['t' => $subquery])->column();
            $this->companies = array_unique(array_merge($this->companies, $companies));

            $result = (new Query)
                ->select([
                    'type',
                    'owner' => new Expression('{{t}}.[[owner_model]]'),
                    'count' => 'COUNT(*)',
                ])
                ->from(['t' => $subquery])
                ->groupBy(['owner_model', 'type'])
                ->all();

            foreach ($result as $row) {
                $this->cache['filesCount'][$row['type']][$row['owner']] = $row['count'];
            }
        }

        return isset($this->cache['filesCount'][$ioType][$className]) ? (int)$this->cache['filesCount'][$ioType][$className] : 0;
    }

    /**
     * @return int|string
     */
    public function getImport1C()
    {
        if (!isset($this->cache['import1CCount'])) {
            $query = CashBankStatementUpload::find()
                ->alias('upload')
                ->joinWith('company company')
                ->andWhere([
                    'company.test' => false,
                    'upload.source' => CashBankStatementUpload::SOURCE_FILE,
                ])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'upload.created_at', $this->unixFrom, $this->unixTill]);

            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('company.id')->distinct()->column()));

            $this->cache['import1CCount'] = $query->count();
        }

        return (int)$this->cache['import1CCount'];
    }

    /**
     * @param null $class
     * @return array|float|int
     * @throws \yii\web\NotFoundHttpException
     */
    public function getStatementFromBank($class = null)
    {
        if (!isset($this->cache['bankStatementUpload'])) {
            $this->cache['bankStatementUpload'] = [];
            $this->cache['bankStatementUpload']['company'] = [];
            $query = (new Query)
                ->from(['upload' => CashBankStatementUpload::tableName()])
                ->innerJoin(['company' => Company::tableName()], '{{company}}.[[id]] = {{upload}}.[[company_id]]')
                ->andWhere([
                    'company.test' => false,
                    'upload.source' => [CashBankStatementUpload::SOURCE_BANK, CashBankStatementUpload::SOURCE_BANK_AUTO],
                ])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'upload.created_at', $this->unixFrom, $this->unixTill]);

            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('company.id')->distinct()->column()));

            $result = $query
                ->select(['company.id as companyID', 'upload.bik', 'upload.source', 'count' => 'COUNT(*)'])
                ->groupBy(['upload.bik', 'upload.source'])
                ->all();

            foreach ($result as $row) {
                $this->cache['bankStatementUpload'][$row['source']][$row['bik']] = $row['count'];
                /* @var $company Company */
                $company = Company::findOne($row['companyID']);
                if ($company) {
                    $this->cache['bankStatementUpload']['company'][$row['source']][$row['bik']][$row['companyID']] = $company->getTitle(true);
                }
            }
        }

        $manual = ArrayHelper::getValue($this->cache['bankStatementUpload'], CashBankStatementUpload::SOURCE_BANK, []);
        $auto = ArrayHelper::getValue($this->cache['bankStatementUpload'], CashBankStatementUpload::SOURCE_BANK_AUTO, []);
        $manualCompanies = ArrayHelper::getValue($this->cache['bankStatementUpload']['company'], CashBankStatementUpload::SOURCE_BANK, []);

        if ($class) {
            return [
                'manual' => array_sum(ArrayHelper::filter($manual, $class::$bikList)),
                'auto' => array_sum(ArrayHelper::filter($auto, $class::$bikList)),
                'companies' => ArrayHelper::filter($manualCompanies, $class::$bikList)
            ];
        }

        return array_sum($manual) + array_sum($auto);
    }

    public function getStatementFromBankCompanyArray()
    {

    }

    /**
     * @param null $class
     * @return array|float|int
     * @throws \yii\web\NotFoundHttpException
     */
    public function getStatementFromOfd($class = null)
    {
        if (!isset($this->cache['ofdStatementUpload'])) {
            $this->cache['ofdStatementUpload'] = [];
            $query = (new Query)
                ->from(['upload' => CashOrderStatementUpload::tableName()])
                ->innerJoin(['company' => Company::tableName()], '{{company}}.[[id]] = {{upload}}.[[company_id]]')
                ->andWhere([
                    'company.test' => false,
                    'upload.source' => [CashOrderStatementUpload::SOURCE_OFD, CashOrderStatementUpload::SOURCE_OFD_AUTO],
                ])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'upload.created_at', $this->unixFrom, $this->unixTill]);

            foreach (Ofd::$modelClassArray as $key => $modelClass) {
                $cloneQuery = clone $query;
                $companyIds = $cloneQuery->select('company.id')->distinct()->andWhere([
                    'upload.service' => $modelClass::$alias,
                ])->column();
                $companyQuery = Company::find()->select([
                    'name' => 'CONCAT_WS(" ", {{type}}.[[name_short]], IFNULL({{company}}.[[name_short]], {{company}}.[[name_full]]))',
                    'company.id',
                ])->leftJoin([
                    'type' => CompanyType::tableName(),
                ], '{{type}}.[[id]]={{company}}.[[company_type_id]]')->andWhere([
                    'company.id' => $companyIds,
                ]);

                $this->cache['ofdStatementUpload']['company'][$modelClass::$alias] = $companyQuery->indexBy('id')->column();
                $this->companies = array_unique(array_merge($this->companies, $companyIds));
            }

            $result = $query
                ->select(['upload.service', 'upload.source', 'count' => 'COUNT(*)'])
                ->groupBy(['upload.service', 'upload.source'])
                ->all();

            foreach ($result as $row) {
                $this->cache['ofdStatementUpload'][$row['source']][$row['service']] = $row['count'];
            }
        }

        $manual = ArrayHelper::getValue($this->cache['ofdStatementUpload'], CashOrderStatementUpload::SOURCE_OFD, []);
        $auto = ArrayHelper::getValue($this->cache['ofdStatementUpload'], CashOrderStatementUpload::SOURCE_OFD_AUTO, []);
        $companies = ArrayHelper::getValue($this->cache['ofdStatementUpload'], 'company', []);

        if ($class) {
            return [
                'manual' => ArrayHelper::getValue($manual, $class::$alias, 0),
                'auto' => ArrayHelper::getValue($auto, $class::$alias, 0),
                'companies' => ArrayHelper::getValue($companies, $class::$alias, []),
            ];
        }

        return array_sum($manual) + array_sum($auto);
    }

    /**
     * @param null $class
     * @return array|float|int
     * @throws \yii\web\NotFoundHttpException
     */
    public function getOfdUploads($ofdId = null)
    {
        if (!isset($this->cache['getOfdUploads'])) {
            $this->cache['getOfdUploads'] = [];
            $query = (new Query)
                ->from(['upload' => OfdStatementUpload::tableName()])
                ->innerJoin(['company' => Company::tableName()], '{{company}}.[[id]] = {{upload}}.[[company_id]]')
                ->andWhere([
                    'company.test' => false,
                    'upload.source' => [OfdStatementUpload::SOURCE_OFD, OfdStatementUpload::SOURCE_OFD_AUTO],
                ])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'upload.created_at', $this->unixFrom, $this->unixTill]);

            foreach (\common\models\ofd\Ofd::active() as $id => $name) {
                $cloneQuery = clone $query;
                $companyIds = $cloneQuery->select('company.id')->distinct()->andWhere([
                    'upload.ofd_id' => $id,
                ])->column();
                $companyQuery = Company::find()->select([
                    'name' => 'CONCAT_WS(" ", {{type}}.[[name_short]], IFNULL({{company}}.[[name_short]], {{company}}.[[name_full]]))',
                    'company.id',
                ])->leftJoin([
                    'type' => CompanyType::tableName(),
                ], '{{type}}.[[id]]={{company}}.[[company_type_id]]')->andWhere([
                    'company.id' => $companyIds,
                ]);

                $this->cache['getOfdUploads']['company'][$id] = $companyQuery->indexBy('id')->column();
                $this->companies = array_unique(array_merge($this->companies, $companyIds));
            }

            $result = $query
                ->select(['upload.ofd_id', 'upload.source', 'count' => 'COUNT(*)'])
                ->groupBy(['upload.ofd_id', 'upload.source'])
                ->all();

            foreach ($result as $row) {
                $this->cache['getOfdUploads'][$row['source']][$row['ofd_id']] = $row['count'];
            }
        }

        $manual = ArrayHelper::getValue($this->cache['getOfdUploads'], OfdStatementUpload::SOURCE_OFD, []);
        $auto = ArrayHelper::getValue($this->cache['getOfdUploads'], OfdStatementUpload::SOURCE_OFD_AUTO, []);
        $companies = ArrayHelper::getValue($this->cache['getOfdUploads'], 'company', []);

        if ($ofdId !== null) {
            return [
                'manual' => ArrayHelper::getValue($manual, $ofdId, 0),
                'auto' => ArrayHelper::getValue($auto, $ofdId, 0),
                'companies' => ArrayHelper::getValue($companies, $ofdId, []),
            ];
        }

        return array_sum($manual) + array_sum($auto);
    }

    /**
     * @return int|string
     */
    public function getReportFile()
    {
        if (!isset($this->cache['reportFileCount'])) {
            $query = ReportFile::find()
                ->joinWith(['report report', 'report.company company'])
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'report.created_at', $this->unixFrom, $this->unixTill]);
            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('report.company_id')->column()));
            $this->cache['reportFileCount'] = $query->count();
        }

        return (int)$this->cache['reportFileCount'];
    }

    /**
     * @return int|string
     */
    public function getSpecificDocument()
    {
        if (!isset($this->cache['specificDocumentCount'])) {
            $query = SpecificDocument::find()
                ->alias('document')
                ->joinWith('company company')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'document.created_at', $this->unixFrom, $this->unixTill]);
            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('document.company_id')->column()));
            $this->cache['specificDocumentCount'] = $query->count();
        }

        return (int)$this->cache['specificDocumentCount'];
    }

    /**
     * @return mixed
     */
    public function getImportXlsProductCount()
    {
        if (!isset($this->cache['importXlsProductCount'])) {
            $this->cache['importXlsProductCount'] = $this->getCompanies()->sum('import_xls_product_count');
        }

        return (int)$this->cache['importXlsProductCount'];
    }

    /**
     * @return mixed
     */
    public function getImportXlsServiceCount()
    {
        if (!isset($this->cache['importXlsServiceCount'])) {
            $this->cache['importXlsServiceCount'] = $this->getCompanies()->sum('import_xls_service_count');
        }

        return (int)$this->cache['importXlsServiceCount'];
    }

    /**
     * @return int|string
     */
    public function getExport1C()
    {
        if (!isset($this->cache['export1CCount'])) {
            $query = Export::find()->alias('export')
                ->innerJoin(['company' => Company::tableName()], '{{company}}.[[id]] = {{export}}.[[company_id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'export.created_at', $this->unixFrom, $this->unixTill]);
            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('company.id')->distinct()->column()));
            $this->cache['export1CCount'] = $query->count();
        }

        return (int)$this->cache['export1CCount'];
    }

    /**
     * @return int|string
     */
    public function getExportFiles()
    {
        if (!isset($this->cache['exportFilesCount'])) {
            $query = ExportFiles::find()->alias('export')
                ->innerJoin(['company' => Company::tableName()], '{{company}}.[[id]] = {{export}}.[[company_id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', 'export.created_at', $this->unixFrom, $this->unixTill]);
            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('company.id')->distinct()->column()));
            $this->cache['exportFilesCount'] = $query->count();
        }

        return (int)$this->cache['exportFilesCount'];
    }

    /**
     * @return int|string
     */
    public function getAllActivity()
    {
        if (!isset($this->cache['allActivityCount'])) {
            $this->cache['allActivityCount'] =
                $this->getSentInvoicesCount() +
                $this->getSentVisitCardCount() +
                $this->getContractorCount(Contractor::TYPE_CUSTOMER) +
                $this->getContractorCount(Contractor::TYPE_SELLER) +
                $this->getDocumentsCount(Invoice::className(), Documents::IO_TYPE_OUT) +
                $this->getDocumentsCount(InvoiceAuto::className(), Documents::IO_TYPE_OUT) +
                $this->getDocumentsCount(Act::className(), Documents::IO_TYPE_OUT) +
                $this->getDocumentsCount(PackingList::className(), Documents::IO_TYPE_OUT) +
                $this->getDocumentsCount(InvoiceFacture::className(), Documents::IO_TYPE_OUT) +
                $this->getDocumentsCount(Upd::className(), Documents::IO_TYPE_OUT) +
                $this->getDocumentsCount(PaymentOrder::className(), Documents::IO_TYPE_OUT) +
                $this->getDocumentsCount(Invoice::className(), Documents::IO_TYPE_IN) +
                $this->getDocumentsCount(Act::className(), Documents::IO_TYPE_IN) +
                $this->getDocumentsCount(PackingList::className(), Documents::IO_TYPE_IN) +
                $this->getDocumentsCount(InvoiceFacture::className(), Documents::IO_TYPE_IN) +
                $this->getFilesCount(Invoice::className(), Documents::IO_TYPE_OUT) +
                $this->getFilesCount(Act::className(), Documents::IO_TYPE_OUT) +
                $this->getFilesCount(PackingList::className(), Documents::IO_TYPE_OUT) +
                $this->getFilesCount(InvoiceFacture::className(), Documents::IO_TYPE_OUT) +
                $this->getFilesCount(Upd::className(), Documents::IO_TYPE_OUT) +
                $this->getFilesCount(Invoice::className(), Documents::IO_TYPE_IN) +
                $this->getFilesCount(Act::className(), Documents::IO_TYPE_IN) +
                $this->getFilesCount(PackingList::className(), Documents::IO_TYPE_IN) +
                $this->getFilesCount(InvoiceFacture::className(), Documents::IO_TYPE_IN) +
                $this->getImport1C() +
                $this->getStatementFromBank() +
                $this->getStatementFromOfd() +
                $this->getOfdUploads() +
                $this->getReportFile() +
                $this->getSpecificDocument() +
                $this->getImportXlsProductCount() +
                $this->getImportXlsServiceCount() +
                $this->getExport1C() +
                $this->getExportFiles();
        }

        return $this->cache['allActivityCount'];
    }

    /**
     * @return int|string
     */
    public function getTrialCompanies()
    {
        if (!isset($this->cache['trialCompaniesCount'])) {
            $this->cache['trialCompaniesCount'] = Company::find()->alias('company')
                ->innerJoin([
                    'free' => Subscribe::find()
                        ->select(['company_id'])
                        ->distinct()
                        ->andWhere([
                            'or',
                            ['tariff_id' => SubscribeTariff::TARIFF_TRIAL],
                            ['tariff_id' => null],
                        ])
                        ->andWhere(['<=', 'activated_at', $this->unixTill])
                        ->andWhere(['>=', 'expired_at', $this->unixFrom])
                ], "{{free}}.[[company_id]] = {{company}}.[[id]]")
                ->leftJoin([
                    'paid' => Subscribe::find()
                        ->select(['company_id'])
                        ->distinct()
                        ->andWhere(['tariff_id' => SubscribeTariff::paidStandartIds()])
                        ->andWhere(['<=', 'activated_at', $this->unixTill])
                        ->andWhere(['>=', 'expired_at', $this->unixFrom])
                ], "{{paid}}.[[company_id]] = {{company}}.[[id]]")
                ->andWhere([
                    'and',
                    ['company.id' => $this->companies],
                    ['paid.company_id' => null],
                    [
                        'or',
                        ['>', 'company.free_tariff_start_at', $this->unixTill],
                        ['company.free_tariff_start_at' => null],
                    ],
                ])
                ->count();
        }

        return (int)$this->cache['trialCompaniesCount'];
    }

    /**
     * @return int|string
     */
    public function getPayedCompanies()
    {
        if (!isset($this->cache['payedCompaniesCount'])) {
            $this->cache['payedCompaniesCount'] = Company::find()->alias('company')
                ->innerJoin([
                    'paid' => Subscribe::find()
                        ->select(['company_id'])
                        ->distinct()
                        ->andWhere(['tariff_id' => SubscribeTariff::paidStandartIds()])
                        ->andWhere(['<=', 'activated_at', $this->unixTill])
                        ->andWhere(['>=', 'expired_at', $this->unixFrom])
                ], "{{paid}}.[[company_id]] = {{company}}.[[id]]")
                ->andWhere(['company.id' => $this->companies])
                ->count();
        }

        return (int)$this->cache['payedCompaniesCount'];
    }

    /**
     * @return int
     */
    public function getFreeTariffCompanies()
    {
        if (!isset($this->cache['freeTariffCompaniesCount'])) {
            $this->cache['freeTariffCompaniesCount'] = Company::find()->alias('company')
                ->leftJoin([
                    'paid' => Subscribe::find()
                        ->select(['company_id'])
                        ->distinct()
                        ->andWhere(['tariff_id' => SubscribeTariff::paidStandartIds()])
                        ->andWhere(['<=', 'activated_at', $this->unixTill])
                        ->andWhere(['>=', 'expired_at', $this->unixFrom])
                ], '{{paid}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere([
                    'and',
                    ['company.id' => $this->companies],
                    ['<', 'company.free_tariff_start_at', $this->unixTill],
                    ['paid.company_id' => null]
                ])
                ->count();
        }

        return (int)$this->cache['freeTariffCompaniesCount'];
    }

    /**
     * @param $osno
     * @param $usn
     * @param $envd
     * @param $psn
     * @return int|string
     */
    public function getCompaniesByTaxationSystem($osno, $usn, $envd, $psn, $usnType)
    {
        $query = $this->getCompanies()
            ->joinWith('companyTaxationType')
            ->andWhere(['and',
                [CompanyTaxationType::tableName() . '.osno' => $osno],
                [CompanyTaxationType::tableName() . '.usn' => $usn],
                [CompanyTaxationType::tableName() . '.envd' => $envd],
                [CompanyTaxationType::tableName() . '.psn' => $psn],
            ]);
        if ($usnType !== null) {
            $query->andWhere([CompanyTaxationType::tableName() . '.usn_type' => $usnType]);
        }

        return (int)$query->count();
    }

    /**
     * @param $type integer
     * @param $tax array
     * @return int
     */
    public function getCompaniesByTypeAndTax($type, $tax)
    {
        $query = $this->getCompanies()->andWhere(['company_type_id' => $type])
            ->joinWith('companyTaxationType')
            ->andWhere(['and',
                [CompanyTaxationType::tableName() . '.osno' => $tax[0]],
                [CompanyTaxationType::tableName() . '.usn' => $tax[1]],
                [CompanyTaxationType::tableName() . '.envd' => $tax[2]],
                [CompanyTaxationType::tableName() . '.psn' => $tax[3]],
            ]);
        if ($tax[4] !== null) {
            $query->andWhere([CompanyTaxationType::tableName() . '.usn_type' => $tax[4]]);
        }

        return (int)$query->count();
    }

    /**
     * @param $type
     * @return int|string
     */
    public function getCompaniesByType($type)
    {
        if (!isset($this->cache['companiesByTypeCount'])) {
            $result = $this->getCompanies()
                ->select(['company_type_id', 'count' => 'COUNT(*)'])
                ->groupBy('company_type_id')
                ->asArray()
                ->all();

            foreach ($result as $row) {
                $this->cache['companiesByTypeCount'][$row['company_type_id']] = $row['count'];
            }
        }

        return isset($this->cache['companiesByTypeCount'][$type]) ? $this->cache['companiesByTypeCount'][$type] : 0;
    }

    /**
     * @return mixed
     */
    public function getStoreCabinetCount()
    {
        return count($this->getStoreCabinetCompanyArray()->all());
    }

    /**
     * @return mixed
     */
    public function getStoreCabinetCompanyArray()
    {
        if (!isset($this->cache['storeCabinetCount'])) {
            $this->cache['storeCabinetCount'] = StoreCompanyContractor::find()
                ->select([
                    'company.id',
                    StoreCompanyContractor::tableName() . '.contractor_id as contractor_id',
                    'name' => "IF(
                        {{company}}.[[company_type_id]] = :typeIP,
                        CONCAT({{company_type}}.[[name_short]], ' ', {{company}}.[[name_short]]),
                        CONCAT({{company_type}}.[[name_short]], ' \"', {{company}}.[[name_short]], '\"')
                    )",
                ])
                ->joinWith('contractor')
                ->joinWith('contractor.company')
                ->joinWith('contractor.company.companyType')
                ->andWhere(['company.test' => !Company::TEST_COMPANY])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere([StoreCompanyContractor::tableName() . '.status' => StoreCompanyContractor::STATUS_ACTIVE])
                ->andWhere([Contractor::tableName() . '.is_deleted' => false])
                ->andWhere([Contractor::tableName() . '.status' => Contractor::ACTIVE])
                ->andWhere(['between', StoreCompanyContractor::tableName() . '.created_at', $this->unixFrom, $this->unixTill])
                ->groupBy(StoreCompanyContractor::tableName() . '.contractor_id')
                ->asArray()
                ->params([':typeIP' => CompanyType::TYPE_IP]);
        }

        return $this->cache['storeCabinetCount'];
    }

    /**
     * @return mixed
     */
    public function getOrderDocumentCountFromStoreCabinet()
    {
        if (!isset($this->cache['orderDocumentCountFromStoreCabinet'])) {
            $this->cache['orderDocumentCountFromStoreCabinet'] = OrderDocument::find()
                ->joinWith('company')
                ->andWhere(['!=', Company::tableName() . '.test', Company::TEST_COMPANY])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['is_deleted' => OrderDocument::NOT_IS_DELETED])
                ->andWhere(['between', OrderDocument::tableName() . '.document_date', $this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59'])
                ->andWhere(['from_store' => OrderDocument::CREATED_FROM_STORE])
                ->count();
        }

        return $this->cache['orderDocumentCountFromStoreCabinet'];
    }

    /**
     * @return mixed
     */
    public function getInvoiceCountFromStoreCabinet()
    {
        if (!isset($this->cache['invoiceCountFromStoreCabinet'])) {
            $this->cache['invoiceCountFromStoreCabinet'] = Invoice::find()
                ->joinWith('company')
                ->andWhere(['!=', 'test', Company::TEST_COMPANY])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->byDeleted()
                ->byDocumentDateRange($this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59')
                ->andWhere(['from_store_cabinet' => true])
                ->count();
        }

        return $this->cache['invoiceCountFromStoreCabinet'];
    }

    /**
     * @return mixed
     */
    public function getOutInvoiceCount()
    {
        if (!isset($this->cache['outInvoiceCount'])) {
            $this->cache['outInvoiceCount'] = OutInvoice::find()
                ->joinWith('company')
                ->andWhere(['!=', 'test', Company::TEST_COMPANY])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere([OutInvoice::tableName() . '.status' => OutInvoice::ACTIVE])
                ->andWhere(['between', OutInvoice::tableName() . '.created_at', $this->unixFrom, $this->unixTill])
                ->count();
        }

        return $this->cache['outInvoiceCount'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutInvoiceCompanyArray()
    {
        return OutInvoice::find()
            ->select([
                'company.id',
                OutInvoice::tableName() . '.company_id as company_id',
                'name' => "IF(
                        {{company}}.[[company_type_id]] = :typeIP,
                        CONCAT({{company_type}}.[[name_short]], ' ', {{company}}.[[name_short]]),
                        CONCAT({{company_type}}.[[name_short]], ' \"', {{company}}.[[name_short]], '\"')
                    )",
            ])
            ->joinWith('company')
            ->joinWith('company.companyType')
            ->andWhere(['!=', 'test', Company::TEST_COMPANY])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->andWhere([OutInvoice::tableName() . '.status' => OutInvoice::ACTIVE])
            ->andWhere(['between', OutInvoice::tableName() . '.created_at', $this->unixFrom, $this->unixTill])
            ->asArray()
            ->params([':typeIP' => CompanyType::TYPE_IP]);
    }

    /**
     * @return mixed
     */
    public function getInvoiceCountFromOutInvoice()
    {
        if (!isset($this->cache['invoiceCountFromOutInvoice'])) {
            $this->cache['invoiceCountFromOutInvoice'] = Invoice::find()
                ->joinWith('company')
                ->andWhere(['!=', 'test', Company::TEST_COMPANY])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->byDeleted()
                ->byDocumentDateRange($this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59')
                ->andWhere(['from_out_invoice' => true])
                ->count();
        }

        return $this->cache['invoiceCountFromOutInvoice'];
    }

    /**
     * @return mixed
     */
    public function getInvoiceLinksView()
    {
        return (int)Invoice::find()
            ->joinWith('company')
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted()
            ->byDocumentDateRange($this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59')
            ->sum('out_view_count');
    }

    /**
     * @return mixed
     */
    public function getInvoiceLinksSave()
    {
        return (int)Invoice::find()
            ->joinWith('company')
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted()
            ->byDocumentDateRange($this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59')
            ->sum('out_save_count');
    }

    /**
     * @return mixed
     */
    public function getInvoiceLinksPay()
    {
        return (int)Invoice::find()
            ->joinWith('company')
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted()
            ->byDocumentDateRange($this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59')
            ->sum('out_pay_count');
    }

    /**
     * @return int
     */
    public function getInvoiceLinkDownload()
    {

        return (int)Invoice::find()
            ->joinWith('company')
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted()
            ->byDocumentDateRange($this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59')
            ->sum('out_download_count');
    }

    /**
     * @return CompanyQuery $this
     */
    public function getCompanies()
    {
        return Company::find()
            ->andWhere([
                'company.test' => false,
                'company.blocked' => false,
            ])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->andWhere(['between', 'company.created_at', $this->unixFrom, $this->unixTill]);
    }

    /**
     * @param $clickType
     * @param bool $employee
     * @return int|string
     */
    public function getEmployeeClickCount($clickType, $employee = true)
    {
        $query = EmployeeClick::find()
            ->andWhere(['between', 'click_date', $this->_dateRange['from'] . ' 00:00:00', $this->_dateRange['to'] . ' 23:59:59'])
            ->andWhere(['click_type' => $clickType]);

        if ($employee) {
            $query->groupBy('employee_id');
        }

        return $query->count();
    }

    /**
     * @param array $bikList
     * @return int|string
     */
    public function getBankIntegrationsCount($bank_alias)
    {
        return BankingParams::find()
            ->andWhere(['bank_alias' => $bank_alias])
            ->andWhere(['param_name' => 'access_token'])
            ->andWhere(['between', BankingParams::tableName() . '.created_at', strtotime($this->_dateRange['from']), strtotime($this->_dateRange['to'])])
            ->count();
    }

    /**
     * @return array
     */
    public function getIntegrationsData()
    {
        $helper = new \common\components\Integrations(date_create($this->_dateRange['from']), date_create($this->_dateRange['to']));

        return $helper->getIntegrationsCountAll();
    }

    // API statistics

    /**
     * @return integer
     */
    public function getContractorCountFromAPI($type, $apiPartnerId = null)
    {
        if (!isset($this->cache['contractorCountFromAPI'])) {
            $this->cache['contractorCountFromAPI'] = [];
            $query = (new Query)->from(['contractor' => Contractor::tableName()])
                ->innerJoin(['company' => Company::tableName()], '{{contractor}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['contractor.is_deleted' => false])
                ->andWhere(['contractor.status' => Contractor::ACTIVE])
                ->andWhere(['between', 'contractor.created_at', $this->unixFrom, $this->unixTill])
                ->andWhere(['contractor.create_type_id' => CreateType::TYPE_API]);

            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('company.id')->distinct()->column()));
            $result = $query
                ->select(['contractor.type', 'contractor.create_api_id', 'count' => 'COUNT(*)'])
                ->groupBy('contractor.type, contractor.create_api_id')
                ->all();

            foreach ($result as $row) {
                $this->cache['contractorCountFromAPI'][$row['type']][$row['create_api_id']] = $row['count'];
            }
        }

        if (isset($this->cache['contractorCountFromAPI'][$type])) {
            if (!$apiPartnerId)
                return array_sum($this->cache['contractorCountFromAPI'][$type]);
            elseif (isset($this->cache['contractorCountFromAPI'][$type][$apiPartnerId]))
                return $this->cache['contractorCountFromAPI'][$type][$apiPartnerId];
        }

        return 0;
    }

    /**
     * @param $className (Invoice|InvoiceAuto|Act|PackingList|InvoiceFacture|PaymentOrder)::className()
     * @param $ioType
     * @return integer
     */
    public function getDocumentsCountFromAPI($className, $ioType, $apiPartnerId)
    {
        $docIds = [
            Invoice::className() => 1,
            Act::className() => 2,
            PackingList::className() => 3,
            InvoiceFacture::className() => 4,
            Upd::className() => 5,
        ];
        if (!isset($this->cache['documentsCountFromAPI'])) {
            $this->cache['documentsCountFromAPI'] = [];

            $subquery1 = (new Query)->from(['invoice' => Invoice::tableName()])
                ->select(["{$docIds[Invoice::className()]} AS [[doc_id]]", 'invoice.type', 'invoice.id', 'invoice.company_id', 'invoice.create_api_id'])
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.is_invoice_contract' => 1]])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']])
                ->andWhere(['invoice.create_type_id' => CreateType::TYPE_API]);
            $subquery2 = (new Query)->from(['document' => Act::tableName()])
                ->select(["{$docIds[Act::className()]} AS [[doc_id]]", 'document.type', 'document.id', 'invoice.company_id', 'invoice.create_api_id'])
                ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']])
                ->andWhere(['invoice.create_type_id' => CreateType::TYPE_API]);
            $subquery3 = (new Query)->from(['document' => PackingList::tableName()])
                ->select(["{$docIds[PackingList::className()]} AS [[doc_id]]", 'document.type', 'document.id', 'invoice.company_id', 'invoice.create_api_id'])
                ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']])
                ->andWhere(['invoice.create_type_id' => CreateType::TYPE_API]);
            $subquery4 = (new Query)->from(['document' => InvoiceFacture::tableName()])
                ->select(["{$docIds[InvoiceFacture::className()]} AS [[doc_id]]", 'document.type', 'document.id', 'invoice.company_id', 'invoice.create_api_id'])
                ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']])
                ->andWhere(['invoice.create_type_id' => CreateType::TYPE_API]);
            $subquery5 = (new Query)->from(['document' => Upd::tableName()])
                ->select(["{$docIds[Upd::className()]} AS [[doc_id]]", 'document.type', 'document.id', 'invoice.company_id', 'invoice.create_api_id'])
                ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->innerJoin(['company' => Company::tableName()], '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']])
                ->andWhere(['invoice.create_type_id' => CreateType::TYPE_API]);

            $subquery = $subquery1
                ->union($subquery2)
                ->union($subquery3)
                ->union($subquery4)
                ->union($subquery5);

            $companies = (new Query)->select('company_id')->distinct()->from(['t' => $subquery])->column();
            $this->companies = array_unique(array_merge($this->companies, $companies));

            $result = (new Query)
                ->select(['doc_id', 'type', 'create_api_id', 'count' => 'COUNT(*)'])
                ->from(['t' => $subquery])
                ->groupBy(['doc_id', 'type'])
                ->all();

            foreach ($result as $row) {
                $this->cache['documentsCountFromAPI'][$row['type']][$row['doc_id']][$row['create_api_id']] = $row['count'];
            }
        }

        return isset($docIds[$className], $this->cache['documentsCountFromAPI'][$ioType][$docIds[$className]], $this->cache['documentsCountFromAPI'][$ioType][$docIds[$className]][$apiPartnerId]) ?
            (int)$this->cache['documentsCountFromAPI'][$ioType][$docIds[$className]][$apiPartnerId] : 0;
    }

    /**
     * @return integer
     */
    public function getSentInvoicesCountFromAPI($apiPartnerId)
    {
        if (!isset($this->cache['sentInvoicesCountFromAPI'])) {
            $this->cache['sentInvoicesCountFromAPI'] = [];
            $query = (new Query)->from(['invoice' => Invoice::tableName()])
                ->innerJoin('company', '{{invoice}}.[[company_id]] = {{company}}.[[id]]')
                ->andWhere(['company.test' => false])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['invoice.create_type_id' => CreateType::TYPE_API])
                ->andWhere(['>', 'invoice.email_messages', 0])
                ->andWhere(['between', 'invoice.document_date', $this->_dateRange['from'], $this->_dateRange['to']]);
            $cloneQuery = clone $query;
            $this->companies = array_unique(array_merge($this->companies, $cloneQuery->select('company.id')->distinct()->column()));

            $result = $query
                ->select(['invoice.create_api_id', 'count' => 'SUM(invoice.email_messages)'])
                ->groupBy(['invoice.create_api_id'])
                ->all();

            foreach ($result as $row) {
                $this->cache['sentInvoicesCountFromAPI'][$row['create_api_id']] = (int)$row['count'];
            }
        }

        return isset($this->cache['sentInvoicesCountFromAPI'][$apiPartnerId]) ? $this->cache['sentInvoicesCountFromAPI'][$apiPartnerId] : 0;
    }

    public function getSberbankCompaniesIds($onlyPaid = false)
    {
        $ids = CashBankStatementUpload::find()
            ->joinWith('company')
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->andWhere([
                'cash_bank_statement_upload.source' => [
                    CashBankStatementUpload::SOURCE_BANK,
                    CashBankStatementUpload::SOURCE_BANK_AUTO,
                ]
            ])
            //->andWhere(['between', 'created_at', strtotime($this->_dateRange['from'].' 00:00:00'), strtotime($this->_dateRange['to'].' 23:59:59')])
            ->andWhere(['cash_bank_statement_upload.bik' => SberbankModel::$bikList])
            ->select('cash_bank_statement_upload.company_id')->distinct()->asArray()->column();

        if (!$onlyPaid)
            return $ids;

        $paidIds = Subscribe::find()
            ->joinWith('company')
            ->andWhere([
                'service_subscribe.tariff_id' => SubscribeTariff::paidStandartIds(),
                'service_subscribe.status_id' => SubscribeStatus::STATUS_ACTIVATED,
                'company.test' => false,
            ])
            ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
            ->andWhere(['>=', 'service_subscribe.expired_at', strtotime($this->_dateRange['to'].' 00:00:00')])
            ->andWhere(['service_subscribe.company_id' => $ids])
            ->select('service_subscribe.company_id')
            ->distinct()
            ->asArray()
            ->column();

        return $paidIds;
    }

    public function getSberbankAutoloadModeCount($companiesIds)
    {
        return CheckingAccountant::find()
            ->where(['bik' => SberbankModel::$bikList])
            ->andWhere(['not', ['autoload_mode_id' => false]])
            ->andWhere(['company_id' => $companiesIds])
            ->count();
    }

    public function getSberbankUploadsCount($companiesIds, $source)
    {
        return CashBankStatementUpload::find()
            ->where(['bik' => SberbankModel::$bikList])
            ->andWhere(['source' => $source])
            ->andWhere(['company_id' => $companiesIds])
            ->andWhere(['between', 'created_at', strtotime($this->_dateRange['from'].' 00:00:00'), strtotime($this->_dateRange['to'].' 23:59:59')])
            ->count();
    }

    public function getSberbankRequestsCount($companiesIds)
    {
        return CashBankStatementUpload::find()
            ->where(['bik' => SberbankModel::$bikList])
            ->andWhere(['company_id' => $companiesIds])
            ->andWhere(['between', 'created_at', strtotime($this->_dateRange['from'].' 00:00:00'), strtotime($this->_dateRange['to'].' 23:59:59')])
            ->select('SUM(1 + DATEDIFF([[period_till]], [[period_from]]))')
            ->scalar();
    }

    /**
     * @return mixed
     */
    public function getPriceListCount()
    {
        if (!isset($this->cache['priceListCount'])) {
            $this->cache['priceListCount'] = PriceList::find()
                ->joinWith('company')
                ->andWhere(['!=', 'test', Company::TEST_COMPANY])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', PriceList::tableName() . '.created_at', $this->unixFrom, $this->unixTill])
                ->count();
        }

        return $this->cache['priceListCount'];
    }

    /**
     * @return mixed
     */
    public function getPriceListSendsCount()
    {
        if (!isset($this->cache['priceListSendsCount'])) {
            $logItems = Log::find()
                ->leftJoin('company', 'company.id = log.company_id')
                ->andWhere(['!=', Company::tableName() . '.test', Company::TEST_COMPANY])
                ->andFilterWhere(['company.registration_page_type_id' => $this->registration_page_type_id])
                ->andWhere(['between', Log::tableName() . '.created_at', $this->unixFrom, $this->unixTill])
                ->andWhere([Log::tableName() . '.log_event_id' => 4])
                ->andWhere([Log::tableName() . '.model_name' => PriceList::class])
                ->andWhere([Log::tableName() . '.id' => new Expression('(SELECT max(id) FROM log l where log.model_id = l.model_id)')])
                ->select(['attributes_new'])
                ->groupBy(['model_id'])
                ->asArray()
                ->column();

            $this->cache['priceListSendsCount'] = 0;
            foreach ($logItems as $item) {
                $attributes = json_decode($item, true);
                $priceSendsCount = ArrayHelper::getValue($attributes, 'send_count', 0);
                $this->cache['priceListSendsCount'] += $priceSendsCount;
            }
        }

        return $this->cache['priceListSendsCount'];
    }

    /**
     * @param integer $industry
     * @param integer $role
     * @return integer
     */
    public function getEmployeeCountByIndustryRole($industry, $role)
    {
        return (int) $this->getCompanies()->innerJoin(
            '{{%employee_company}}',
            '{{company}}.[[id]]={{employee_company}}.[[company_id]]'
        )->andWhere([
            'company.info_industry_id' => $industry,
            'employee_company.info_role_id' => $role,
        ])->count('*');
    }

    /**
     * @param integer $role
     * @param integer $tariffGroup
     * @return integer
     */
    public function getCompanyTariffSelected($industry, $tariffGroup)
    {
        return (int) $this->getCompanies()->innerJoin([
            't' => CompanyInfoTariff::tableName(),
        ], '{{company}}.[[id]]={{t}}.[[company_id]]')->andWhere([
            'company.info_industry_id' => $industry,
            't.tariff_group_id' => $tariffGroup,
        ])->count('*');
    }

    /**
     * @return array
     */
    public function getStoresStatisticsByIndustries()
    {
        $query = CompanyInfoIndustry::find()->alias('t')->select([
            'name',
            'count' => 'SUM(IF([[info_industry_id]] IS NULL, 0, 1))',
            'shops_count' => 'SUM(IFNULL([[info_shops_count]], 0))',
            'sites_count' => 'SUM(IFNULL([[info_sites_count]], 0))',
            'storage_count' => 'SUM(IFNULL([[info_storage_count]], 0))',
        ])->leftJoin([
            'c' => $this->getCompanies()->select([
                'info_industry_id',
                'info_shops_count',
                'info_sites_count',
                'info_storage_count',
            ])
        ], '{{c}}.[[info_industry_id]] = {{t}}.[[id]]')
        ->andWhere([
            'or',
            ['need_shop' => 1],
            ['need_site' => 1],
            ['need_storage' => 1],
        ])
        ->groupBy('t.id');

        return $query->asArray()->all();
    }

    /**
     * @return array
     */
    public function getBusinessPlaceCount()
    {
        $query = CompanyInfoPlace::find()->alias('t')->select([
            't.name',
            'count' => 'SUM(IF({{c}}.[[info_place_id]] IS NULL, 0, 1))',
        ])->leftJoin([
            'c' => $this->getCompanies()->select([
                'info_place_id',
            ])
        ], '{{c}}.[[info_place_id]] = {{t}}.[[id]]')
        ->groupBy('t.id');

        return $query->asArray()->all();
    }
}
