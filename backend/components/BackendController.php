<?php
namespace backend\components;

use common\components\CommonController;
use common\components\DbTimeZone;
use Yii;
use common\components\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * Class BackendController
 * @package backend\components
 */
abstract class BackendController extends CommonController
{
    /**
     * @var string
     */
    public $layoutWrapperCssClass = '';


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Init
     */
    public function init()
    {
        parent::init();
        date_default_timezone_set('Europe/Moscow');
        DbTimeZone::sync();

        if (Yii::$app->user->getIsGuest() && $this->id != 'site') {
            Yii::$app->user->loginRequired();
        }

        $request = Yii::$app->getRequest();
        $referrer = $request->getReferrer();
        if (!$request->getIsAjax() && $referrer && $referrer != $request->getAbsoluteUrl()) {
            Url::remember($referrer, 'lastPage');
        }
    }
}