<?php

namespace backend\controllers;

use common\models\file\actions\GetFileAction;
use common\models\template\TemplateFile;
use Yii;
use common\models\template\Template;
use common\models\template\TemplateSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TemplateController implements the CRUD actions for Template model.
 */
class TemplateController extends BackendController
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'get-file' => [
                'class' => GetFileAction::className(),
                'model' => TemplateFile::className(),
                'fileNameField' => 'file_name',
                'folderPath' => DIRECTORY_SEPARATOR . TemplateFile::tableName(),
                'fileNameCallback' => function (TemplateFile $model) {
                    return $model->template_id . DIRECTORY_SEPARATOR . $model->id;
                },
            ],
        ]);
    }

    /**
     * Lists all Template models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TemplateSearch();
        $fileModel = new TemplateFile();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post()) {
            $model = $this->findModel(Yii::$app->request->post('id'));

            $fileModel->load(Yii::$app->request->post());
            $file = UploadedFile::getInstance($fileModel, 'file_name');

            if ($file) {
                $fileModel->template_id = $model->id;
                $fileModel->file_name = $file->name;
                $fileModel->save();
                $fileName = $fileModel->id;
                $file->saveAs($fileModel->getUploadPath(true) . DIRECTORY_SEPARATOR . $fileName);
            }

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $this->refresh();
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'templates' => $dataProvider->models,
            'templateFile' => $fileModel,
        ]);
    }

    /**
     * Creates a new Template model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Template();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Template model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (TemplateFile::findAll(['template_id' => $id])) {
            $templateFile = new TemplateFile();

            if ($templateFile->removeDirectory($id)) {
                TemplateFile::deleteAll(['template_id' => $id]);
            }
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteFile($id)
    {
        $model = $this->findTemplateFileModel($id);
        $model->removeFile($model->getFilePath($model->file_name));
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Template model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Template the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Template::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     *
     * @return TemplateFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTemplateFileModel($id)
    {
        if (($model = TemplateFile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
