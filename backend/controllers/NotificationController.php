<?php

namespace backend\controllers;

use common\components\UploadedFile;
use frontend\components\XlsHelper;
use himiklab\thumbnail\FileNotFoundException;
use Yii;
use common\models\notification\Notification;
use common\models\notification\NotificationSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * NotificationController implements the CRUD actions for Notification model.
 */
class NotificationController extends BackendController
{
    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notification model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Notification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notification();

        ini_set('max_execution_time', 3600);
        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            if ($model->activation_date == date('Y-m-d')) {
                $model->activation_time = date('H:i:s');
            }
            $model->save(false, ['activation_time']);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Notification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post('activation_date') == date('Y-m-d')) {
            $model->activation_time = date('H:i:s');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->activation_date == date('Y-m-d')) {
                $model->activation_time = date('H:i:s');
            }
            $model->save(false, ['activation_time']);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Notification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->_delete();

        return $this->redirect(['index']);
    }

    /**
     * @return array|bool
     * @throws NotFoundHttpException
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionUploadXls()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $file = UploadedFile::getInstanceByName('uploadfile');
        if ($file !== null) {
            $xlsHelper = new XlsHelper($file);
            $result = $xlsHelper->readForNotification();

            return [
                'message' => 'Успешно загружено ' . $result['successCount'] . ' уведомлений <br>
                    Загружено с ошибкой ' . $result['errorCount'] . ' уведомлений',
                'result' => $result['errorCount'] == 0,
            ];
        }


        return false;
    }

    /**
     * @throws FileNotFoundException
     */
    public function actionDownloadTemplate()
    {
        $fileName = 'Notification.xlsx';
        $path = \Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'xls' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . $fileName;
        if (file_exists($path)) {
            return \Yii::$app->response->sendFile($path, $fileName, ['mimeType' => mime_content_type($path)])->send();
        } else {
            throw new FileNotFoundException('File not found');
        }
    }

    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
