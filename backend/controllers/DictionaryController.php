<?php
namespace backend\controllers;

use common\models\dictionary\address\AddressDictionary;
use common\models\dictionary\bik\BikDictionary;
use common\models\employee\Employee;
use backend\components\BackendController;
use Yii;

/**
 * Dictionary controller
 */
class DictionaryController extends BackendController
{
    public $layout = '//view/layouts/clear';

    /**
     * @return string
     */
    public function actionBik($q)
    {
        $bikArray = [];
        $q = trim($q);
        if ($q !== '') {
            $bikArray = BikDictionary::find()
                ->byActive()
                ->byBikFilter($q)
                ->limit(20)
                ->asArray()
                ->all();
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $bikArray;
    }

    public function actionAddress($type, $q)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $searcher = new AddressDictionary($type, $q, Yii::$app->request->get());

        return (array) $searcher->search();
    }

    /**
     * @param integer $id
     * @param string $q
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionEmployee($q)
    {
        $employeeArray = [];
        $q = trim($q);
        if ($q !== '') {
            $employeeArray = Employee::find()
                ->select(['*', 'DATE_FORMAT(date_hiring, "%d.%m.%Y") as date_hiring_format',
                    'DATE_FORMAT(birthday, "%d.%m.%Y") as birthday_format',
                    'DATE_FORMAT(date_dismissal, "%d.%m.%Y") as date_dismissal_format',])
                ->byEmailFilter($q)
                ->byIsDeleted(Employee::NOT_DELETED)
                ->limit(20)
                ->asArray()
                ->all();
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $employeeArray;
    }
}
