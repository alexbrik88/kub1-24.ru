<?php
namespace backend\controllers;

use backend\components\BackendController;
use backend\components\helpers\DashBoardHelper;
use backend\models\LoginForm;
use frontend\components\StatisticPeriod;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\document\Invoice;

/**
 * Site controller
 */
class SiteController extends BackendController
{
    /**
     * @var
     */
    public $date_from;
    /**
     * @var
     */
    public $date_to;
    /**
     * @var
     */
    public $countInInvoice;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'statistic-range', 'sidebar-status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();
        $inData = new DashBoardHelper($dateRange);

        return $this->render('index', [
            'inData' => $inData,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     *
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        $this->redirect(Yii::$app->urlManager->createUrl('site/index'));
    }

    /**
     * @return array
     */
    public function actionStatisticRange()
    {
        $post = Yii::$app->request->post();

        if (!empty($post)) {

            Yii::$app->user->identity->setStatisticRangeDates($post['date_from'],$post['date_to']);
            Yii::$app->user->identity->setStatisticRangeName($post['name'],$post['date_from'],$post['date_to']);

            echo true;

        } else {

            echo false;
        }
    }

    /**
     *
     */
    public function actionSidebarStatus()
    {
        $post = Yii::$app->request->post();
        if (!empty($post)) {
            $session = Yii::$app->session;
            $session['sidebar'] = [
                'status' => $post['status'],
            ];
            echo true;
        }
        echo false;
    }

    /**
     * @param $param
     * @return mixed
     */
    public function getQuery($param)
    {
        return $result = $param
            ->andWhere(['[[type]]' => "1"])
            ->andWhere(['between', '[[document_date]]', $this->date_from, $this->date_to])
            ->all();
    }

    /**
     *
     */
    public function getDownloading()
    {
        $model = Invoice::find();
        $type = '1';
        $inInvoice = $this->getQuery($model, $type);
        $this->countInInvoice = is_array($inInvoice) ? count($inInvoice) : 0;
        $model = InvoiceFacture::find();
        $inInvoiceFacture = $this->getQuery($model, $type);
        $this->countInInvoiceFacture = is_array($inInvoiceFacture) ? count($inInvoiceFacture) : 0;
//        $this->countInInvoice = is_array($inInvoice) ? count($inInvoice) : 0 ;
//        $inInvoice = $this->getQuery(Invoice::find());
//        $this->countInInvoice = is_array($inInvoice) ? count($inInvoice) : 0 ;
//        $inInvoice = $this->getQuery(Invoice::find());
//        $this->countInInvoice = is_array($inInvoice) ? count($inInvoice) : 0 ;
    }
}
