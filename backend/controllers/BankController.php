<?php

namespace backend\controllers;

use backend\models\Bank;
use backend\models\BankSearch;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\filters\VerbFilter;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class BankController
 * @package backend\controllers
 */
class BankController extends BackendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new Bank();
        $searchModel = new BankSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @return array|string
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $model = new Bank();
        $controller = $this;
        if (\Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(\Yii::$app->request->post())) {
            \Yii::$app->db->transaction(function (Connection $db) use ($model, $controller) {
                if (\Yii::$app->request->post('Bank')['has_logo']) {
                    $model->logoImage = Bank::LOGO_IMAGE;
                }
                if ($model->save() && $model->_saveLogo()) {
                    return $controller->redirect(['index']);
                } else {
                    $db->transaction->rollBack();

                    return false;
                }
            });
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|string
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $controller = $this;
        if (\Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(\Yii::$app->request->post())) {
            \Yii::$app->db->transaction(function (Connection $db) use ($model, $controller) {
                if ($model->save() && $model->_saveLogo()) {
                    return $controller->redirect(['index']);
                } else {
                    $db->transaction->rollBack();

                    return false;
                }
            });
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionBlock($id)
    {
        $model = $this->findModel($id);
        $model->is_blocked = 1;

        if (!$model->save(true, ['is_blocked'])) {
            \Yii::$app->session->setFlash('error', 'Произошла ошибка при блокировании банка партнера "' . $model->bank_name . '"');
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @param string $type
     * @throws NotFoundHttpException
     */
    public function actionUploadLogo($id, $type = Bank::LOGO_IMAGE)
    {
        $attribute = $type == Bank::LOGO_IMAGE ? 'logo_link' : 'little_logo_link';
        $model = $this->findModel($id);
        if ($model->$attribute) {
            $filePath = $model->getUploadDirectory() . $model->$attribute;
            if (file_exists($filePath)) {
                \Yii::$app->response->sendFile($filePath, $model->$attribute, ['mimeType' => 'image/*'])->send();
            } else throw new NotFoundHttpException('Файл не найден');
        } else throw new NotFoundHttpException('Такого файла не существует');
    }

    /**
     * @param $id
     * @return Bank null|static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Bank::findOne($id);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
    }

    /**
     * @param \yii\base\Model $model
     * @return array
     */
    protected function ajaxValidate($model)
    {
        $model->load(\Yii::$app->request->post());
        if (\Yii::$app->request->post('Bank')['has_logo']) {
            $model->logoImage = Bank::LOGO_IMAGE;
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return ActiveForm::validate($model);
    }
}
