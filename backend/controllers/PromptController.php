<?php

namespace backend\controllers;

use backend\components\BackendController;
use backend\models\Prompt;
use backend\models\PromptHelper;
use backend\models\PromptSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;


/**
 * Class PromptController
 * @package backend\controllers
 */
class PromptController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Prompt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PromptSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Prompt model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Prompt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Prompt();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->video_link) {
                $model->video_link = $model->getNewVideoLink();
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось создать подсказку. Для данного типа компании на выбранной странице активирована другая подсказка ');

                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Prompt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->video_link) {
                $model->video_link = $model->getNewVideoLink();
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось создать подсказку. Для данного типа компании на выбранной странице активирована другая подсказка ');

                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Prompt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);

        $promptsQuery = (new PromptHelper())->getPrompts(Prompt::ACTIVE, $model->for_whom_prompt_id, $model->page_type_id);

        if (count($promptsQuery) === 1) {
            $newPrompts = $this->findModel($promptsQuery[0]['id']);

            if ($newPrompts !== null) {
                $newPrompts->status = Prompt::BLOCKED;
                if ($newPrompts->save(true, ['status'])) {
                    $model->status = Prompt::ACTIVE;
                    if ($model->save(true, ['status'])) {
                        Yii::$app->session->setFlash('success', 'Подсказка активирована');

                        return $this->redirect(['index']);
                    }
                }
            }
        } elseif (count($promptsQuery) < 1) {
            $model->status = Prompt::ACTIVE;
            if ($model->save(true, ['status'])) {
                Yii::$app->session->setFlash('success', 'Подсказка активирована');

                return $this->redirect(['index']);
            }
        }
        Yii::$app->session->setFlash('error', 'Не удалось активировать подсказку');

        return $this->redirect(['index']);
    }

    public function actionBlock($id)
    {
        $model = $this->findModel($id);

        $model->status = Prompt::BLOCKED;

        if ($model->save(true, ['status'])) {
            Yii::$app->session->setFlash('success', 'Подсказка заблокирована');

            return $this->redirect(['index']);
        }
        Yii::$app->session->setFlash('error', 'Не удалось заблокировать подсказку');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Prompt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Prompt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Prompt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
