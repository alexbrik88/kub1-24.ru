<?php

namespace backend\models;

use common\models\news\News;
use yii\data\ActiveDataProvider;

class NewsSearch extends News
{
    public function search(): ActiveDataProvider {
        $query = News::find()->andWhere(['status' => News::STATUS_ACTIVE]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['date' => SORT_DESC],
            ],
        ]);
    }
}
