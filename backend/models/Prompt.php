<?php

namespace backend\models;

use common\models\prompt\ForWhomPrompt;
use common\models\prompt\PageType;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "prompt".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $page_type_id
 * @property integer $for_whom_prompt_id
 * @property string $video_link
 * @property string $title
 * @property string $text
 *
 */
class Prompt extends ActiveRecord
{

    /**
     *
     */
    const ACTIVE = 1;
    /**
     *
     */
    const BLOCKED = 0;

    const ALL = 2;

    public static $PromptStatus = [
        self::ACTIVE => 'Активированная',
        self::BLOCKED => 'Заблокированная',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prompt';
    }

    public static function find()
    {
        return new PromptQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'page_type_id', 'for_whom_prompt_id', 'text', 'title'], 'required'],
            [['status', 'page_type_id', 'for_whom_prompt_id'], 'integer'],
            [['text'], 'string'],
            [['video_link', 'title'], 'string', 'max' => 255],
            [['page_type_id'], 'exist', 'targetClass' => PageType::className(), 'targetAttribute' => 'id',],
            [['for_whom_prompt_id'], 'exist', 'targetClass' => ForWhomPrompt::className(), 'targetAttribute' => 'id',],
            [['status'], 'validateStatus'],
        ];
    }

    public function validateStatus()
    {
        if ($this->status == Prompt::ACTIVE) {
            $promptsQuery = (new PromptHelper())->getPrompts(Prompt::ACTIVE, $this->for_whom_prompt_id, $this->page_type_id);

            if (count($promptsQuery) >= 1 && $promptsQuery[0]['id'] !== $this->id) {
                $this->addError('status');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'page_type_id' => 'Страница',
            'for_whom_prompt_id' => 'Для кого',
            'video_link' => 'Видео',
            'title' => 'Заголовок',
            'text' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageType()
    {
        return $this->hasOne(PageType::className(), ['id' => 'page_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForWhomPrompt()
    {
        return $this->hasOne(ForWhomPrompt::className(), ['id' => 'for_whom_prompt_id']);
    }

    public function getNewVideoLink()
    {
        // https://vimeo.com/156520621
        // https://player.vimeo.com/video/156520621?title=0&byline=0&portrait=0

        $url = 'https://player.vimeo.com/video/';
        $settings = '?title=0&byline=0&portrait=0';
        $id = preg_replace("/[^0-9]/", '', $this->video_link);

        return $url . $id . $settings;
    }
}
