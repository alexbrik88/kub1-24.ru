<?php

namespace backend\models;

use common\components\ImageHelper;
use common\components\validators\PhoneValidator;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * This is the model class for table "bank".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $bank_name
 * @property string $description
 * @property string $url
 * @property string $email
 * @property string $phone
 * @property string $contact_person
 * @property string $logo_link
 * @property integer $request_count
 * @property boolean $is_blocked
 * @property string $little_logo_link
 * @property string $out_bank_name
 */
class Bank extends \common\models\bank\Bank
{
    /**
     * @var UploadedFile
     */
    public $logoImage;

    /**
     * @var UploadedFile
     */
    public $littleLogoImage;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * @return BankQuery
     */
    public static function find()
    {
        return new BankQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_name', 'bik', 'out_bank_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_at', 'updated_at', 'request_count'], 'integer'],
            [['description'], 'string'],
            [['is_special_offer'], 'boolean'],
            [['bank_name', 'url', 'contact_person', 'logo_link', 'bik', 'little_logo_link', 'out_bank_name'], 'string', 'max' => 255],
            ['email', 'email'],
            ['phone', PhoneValidator::className()],
            [['phone'], 'string', 'max' => 130],
            ['is_blocked', 'boolean'],
            [['logoImage'], 'file', 'extensions' => 'gif, jpg, png, jpeg, bmp', 'skipOnEmpty' => !$this->isNewRecord],
            [['littleLogoImage'], 'file', 'extensions' => 'gif, jpg, png, jpeg, bmp', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);
        $this->logoImage = UploadedFile::getInstance($this, 'logoImage');
        $this->littleLogoImage = UploadedFile::getInstance($this, 'littleLogoImage');

        return $load || (boolean)$this->logoImage;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bik' => 'БИК',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'bank_name' => 'Наименование банка',
            'description' => 'Описание',
            'url' => 'Ссылка',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'contact_person' => 'Контактное лицо',
            'logo_link' => 'Логотип',
            'little_logo_link' => 'Логотип 2',
            'request_count' => 'Количество заявок',
            'is_blocked' => 'Заблокирована',
            'logoImage' => 'Логотип',
            'littleLogoImage' => 'Логотип 2',
            'is_special_offer' => 'Спец.предложение',
            'out_bank_name' => 'Выводим название',
        ];
    }

    /**
     * @return string
     */
    public function getUploadDirectory()
    {
        $path = parent::getUploadDirectory();
        if (!is_dir($path)) {
            FileHelper::createDirectory($path);
        }

        return $path;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function _saveLogo()
    {
        if ($this->logoImage instanceof UploadedFile) {
            if ($this->logo_link) {
                $oldLogo = $this->getUploadDirectory() . $this->logo_link;
                if (file_exists($oldLogo)) {
                    unlink($oldLogo);
                }
            }
            $ext = $this->logoImage->extension;
            $fileName = DIRECTORY_SEPARATOR . self::LOGO_IMAGE . '.' . $ext;
            $imagePath = $this->getUploadDirectory() . $fileName;
            $this->logoImage->saveAs($imagePath);
            $resizeImage = Image::make($imagePath);
            $resizeImage->resize(250, 150)->save($imagePath, 100);
            $this->logo_link = $fileName;

            if (!$this->save(false, ['logo_link'])) {
                return false;
            }
        }
        if ($this->littleLogoImage instanceof UploadedFile) {
            if ($this->little_logo_link) {
                $oldLogo = $this->getUploadDirectory() . $this->little_logo_link;
                if (file_exists($oldLogo)) {
                    unlink($oldLogo);
                }
            }
            $ext = $this->littleLogoImage->extension;
            $fileName = DIRECTORY_SEPARATOR . self::LITTLE_LOGO_IMAGE . '.' . $ext;
            $imagePath = $this->getUploadDirectory() . $fileName;
            $this->littleLogoImage->saveAs($imagePath);
            $resizeImage = Image::make($imagePath);
            $resizeImage->resize(64, 64)->save($imagePath, 100);
            $this->little_logo_link = $fileName;

            if (!$this->save(false, ['little_logo_link'])) {
                return false;
            }
        }
        if ($this->logo_link) {
            return true;
        }
        $this->addError('logoImage', 'Необходимо загрузить «Логотип».');

        return false;
    }

    /**
     * @param $countOneLine
     * @return string
     */
    public static function getLogoBankList($countOneLine)
    {
        $result = '<div class="bank-logo">';
        /* @var $banks Bank[] */
        $banks = self::find()->byStatus(!self::BLOCKED)->all();
        $counter = 1;
        foreach ($banks as $bank) {
            if (($counter - 1) !== 0) {
                if ((($counter - 1) % $countOneLine) == 0) {
                    $result .= '</div><div class="bank-logo">';
                }
            }
            $result .= '<div class="cont-img_bank-logo">' .
                ImageHelper::getThumb(
                    $bank->getUploadDirectory() . $bank->logo_link, [250, 150],
                    [
                        'class' => 'bank-logo',
                        'style' => 'padding-right: 15px; cursor: pointer;',
                        'data-toggle' => 'modal',
                        'href' => '#apply-to-the-bank-' . $bank->id,
                    ]
                ) . '</div>';
            $counter++;
        }
        $result .= '</div>';

        return $result;
    }

    /**
     * @param $countOneLine
     * @return string
     */
    public static function getSpecialOfferLogoBankList($countOneLine)
    {
        $result = '<div class="bank-logo">';
        /* @var $banks Bank[] */
        $banks = self::find()->andWhere([
            'is_special_offer' => true,
            'is_blocked' => false,
        ])->all();
        $counter = 1;
        foreach ($banks as $bank) {
            if (($counter - 1) !== 0) {
                if ((($counter - 1) % $countOneLine) == 0) {
                    $result .= '</div><div class="bank-logo">';
                }
            }
            $result .= '<div class="cont-img_bank-logo">' .
                ImageHelper::getThumb(
                    $bank->getUploadDirectory() . $bank->logo_link, [250, 150],
                    [
                        'class' => 'bank-logo',
                        'style' => 'padding-right: 15px; cursor: pointer;',
                        'data-toggle' => 'modal',
                        'href' => '#apply-to-the-bank-' . $bank->id,
                    ]
                ) . '</div>';
            $counter++;
        }
        $result .= '</div>';

        return $result;
    }
}
