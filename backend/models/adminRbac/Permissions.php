<?php
namespace backend\models\adminRbac;

use yii\base\BaseObject;

class Permissions extends BaseObject
{
    const ALL = '@';
    const MANAGER = 'manager';

    public static $adminDefault = [
        self::ALL,
    ];

    public static $managerDefault = [
        self::MANAGER,
    ];

    public static $adminRequired = [
    ];

    public static $managerRequired = [
        self::MANAGER,
    ];
}
