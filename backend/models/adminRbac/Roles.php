<?php
namespace backend\models\adminRbac;

use yii\base\BaseObject;

class Roles extends BaseObject
{
    const ADMIN = 'admin';
    const MANAGER = 'manager';

    public static $items = [
        self::ADMIN => 'Администратор',
        self::MANAGER => 'Менеджер',
    ];
}
