<?php
/**
 * Created by PhpStorm.
 * User: valik
 * Date: 11.02.2016
 * Time: 16:34
 */

namespace backend\models;


class PromptHelper extends Prompt
{
    public function getPrompts($status, $forWhomPromptId, $pageTypeId, $page = false)
    {
        $query = static::find();

        $query
            ->byStatus($status)
            ->byForWhomPromptId($forWhomPromptId)
            ->byPage($pageTypeId);

        $promptQuery = $query->all();

        if ($page === true) {
            if (!empty($promptQuery)) {
                $prompt = new Prompt(reset($promptQuery));
                if ($prompt !== null) {
                    return $prompt;
                }
            }
            return null;
        }

        return $promptQuery;
    }
}