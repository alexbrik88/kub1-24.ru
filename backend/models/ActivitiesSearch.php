<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.05.2017
 * Time: 17:16
 */

namespace backend\models;


use common\models\company\Activities;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class AttendanceSearch
 * @package backend\models
 */
class ActivitiesSearch extends Activities
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Activities::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ],
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}