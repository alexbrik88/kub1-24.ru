<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Inquirer;
use frontend\components\StatisticPeriod;

/**
 * InquirerSearch represents the model behind the search form about `common\models\Inquirer`.
 */
class InquirerSearch extends Inquirer
{
    public $status;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'employee_id', 'plan_id', 'created_at', 'status'], 'integer'],
            [['difficulty', 'problem', 'rating'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $dateRange = StatisticPeriod::getSessionPeriod();
        $query = self::find();
        $query->joinWith(['company', 'employee'])
            ->andWhere(['between', 'date(from_unixtime({{company}}.[[created_at]]))', $dateRange['from'], $dateRange['to']]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['id'] = [
            'asc' => ['company.id' => SORT_ASC],
            'desc' => ['company.id' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['company_id'] = [
            'asc' => ['company.name_short' => SORT_ASC],
            'desc' => ['company.name_short' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['created_at'] = [
            'asc' => ['company.created_at' => SORT_ASC],
            'desc' => ['company.created_at' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['employee_id'] = [
            'asc' => ['employee.email' => SORT_ASC],
            'desc' => ['employee.email' => SORT_DESC],
        ];

        $dataProvider->sort->defaultOrder = [
            'created_at' => SORT_DESC,
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company.activation_type' => $this->status,
            'plan_id' => $this->plan_id,
        ]);
        if ($this->rating) {
            $query->andWhere(['rating' => explode('-', $this->rating)]);
        }

        return $dataProvider;
    }
}
