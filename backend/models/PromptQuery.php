<?php
/**
 * Created by PhpStorm.
 * User: valik
 * Date: 11.02.2016
 * Time: 16:28
 */

namespace backend\models;


use common\models\prompt\ForWhomPrompt;
use yii\db\ActiveQuery;

/**
 * Class PromptQuery
 * @package backend\models
 */
class PromptQuery extends ActiveQuery
{
    /**
     * @param $status
     * @return $this
     */
    public function byStatus($status)
    {
        return $this->andWhere([
            'status' => $status,
        ]);
    }

    /**
     * @param $forWhomPromptId
     * @return $this
     */
    public function byForWhomPromptId($forWhomPromptId)
    {
        if ($forWhomPromptId !== ForWhomPrompt::ALL) {
            $forWhomArray = [ForWhomPrompt::ALL, $forWhomPromptId,];
        } else {
            $forWhomArray = [ForWhomPrompt::ALL, ForWhomPrompt::OOO, ForWhomPrompt::ZAO, ForWhomPrompt::PAO];
        }

        return $this->andWhere([
            'in', 'for_whom_prompt_id', $forWhomArray,
        ]);
    }

    /**
     * @param $pageTypeId
     * @return $this
     */
    public function byPage($pageTypeId)
    {
        return $this->andWhere(['page_type_id' => $pageTypeId]);
    }
}