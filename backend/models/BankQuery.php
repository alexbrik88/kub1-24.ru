<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 31.07.2016
 * Time: 9:34
 */

namespace backend\models;


use yii\db\ActiveQuery;

/**
 * Class BankQuery
 * @package backend\models
 */
class BankQuery extends ActiveQuery
{
    /**
     * @param int $blocked
     * @return $this
     */
    public function byStatus($blocked = Bank::BLOCKED)
    {
        return $this->andWhere([
            'is_blocked' => $blocked,
        ]);
    }
}