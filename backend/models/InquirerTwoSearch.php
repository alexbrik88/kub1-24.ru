<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.10.2017
 * Time: 17:49
 */

namespace backend\models;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\company\Activities;
use common\models\ServiceMoreStock;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;

/**
 * Class InquirerTwoSearch
 * @package backend\models
 */
class InquirerTwoSearch extends ServiceMoreStock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activities_id', 'specialization', 'average_invoice_count', 'employees_count', 'accountant_id',
                'has_logo'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        $query = ServiceMoreStock::find()
            ->joinWith('company')
            ->andWhere(['between', 'date(from_unixtime(' . ServiceMoreStock::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    ServiceMoreStock::tableName() . '.created_at' => SORT_DESC,
                ],
                'attributes' => [
                    ServiceMoreStock::tableName() . '.created_at',
                    Company::tableName() . '.created_at',
                ],
            ],
        ]);

        $this->load($params);

        if (($activities = Activities::find()->where(['name' => $this->activities_id])->one()) !== null) {
            $query->andFilterWhere(['or',
                [ServiceMoreStock::tableName() . '.activities_id' => $activities->id],
                [ServiceMoreStock::tableName() . '.company_type_text' => $this->activities_id],
            ]);
        } else {
            $query->andFilterWhere([ServiceMoreStock::tableName() . '.company_type_text' => $this->activities_id]);
        }

        $query->andFilterWhere([ServiceMoreStock::tableName() . '.specialization' => $this->specialization]);

        $query->andFilterWhere([ServiceMoreStock::tableName() . '.average_invoice_count' => $this->average_invoice_count]);

        $query->andFilterWhere([ServiceMoreStock::tableName() . '.employees_count' => $this->employees_count]);

        $query->andFilterWhere([ServiceMoreStock::tableName() . '.accountant_id' => $this->accountant_id]);

        $query->andFilterWhere([ServiceMoreStock::tableName() . '.has_logo' => $this->has_logo]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getActivitiesFilter()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::merge(ArrayHelper::map(Activities::find()->all(), 'name', 'id'), ArrayHelper::map(ServiceMoreStock::find()
            ->andWhere(['not', ['company_type_text' => '']])
            ->andWhere(['not', ['company_type_text' => null]])
            ->andWhere(['between', 'date(from_unixtime(' . ServiceMoreStock::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->groupBy('company_type_text')
            ->all(), 'company_type_text', 'company_type_text')));
    }

    /**
     * @return array
     */
    public function getSpecializationFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ServiceMoreStock::$specializationArray);
    }

    /**
     * @return array
     */
    public function getAverageInvoiceCountFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ServiceMoreStock::$averageInvoiceCountArray);
    }

    /**
     * @return array
     */
    public function getEmployeesCountFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ServiceMoreStock::$employeesCountArray);
    }

    /**
     * @return array
     */
    public function getAccountantFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ServiceMoreStock::$accountantArray);
    }

    /**
     * @return array
     */
    public function getLogoFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ServiceMoreStock::$hasLogoArray);
    }
}