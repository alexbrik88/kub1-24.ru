<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class PromptSearch
 * @package backend\models
 */
class PromptSearch extends Prompt
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['status', 'for_whom_prompt_id',], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prompt::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'status' => $this->status,
            'for_whom_prompt_id' => $this->for_whom_prompt_id,
        ]);

        return $dataProvider;
    }
}