<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.07.2016
 * Time: 18:06
 */

namespace backend\models;


use yii\data\ActiveDataProvider;

/**
 * Class BankSearch
 * @package backend\models
 */
class BankSearch extends Bank
{
    /**
     * @var
     */
    public $search;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['search',], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->byStatus(!Bank::BLOCKED);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'bank_name',
                    'request_count',
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'bank_name', $this->search]);

        return $dataProvider;
    }
}