<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.07.2017
 * Time: 9:12
 */

namespace backend\modules\report\models;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\company\RegistrationPageType;
use common\models\report\ReportState;
use common\models\service\Payment;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\Url;

/**
 * Class StateSearch
 * @package backend\modules\report\models
 */
class StateSearch extends ReportState
{
    /**
     * @var int
     */
    public $pageSize = 10;

    /**
     * @var
     */
    public $tariffType;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pageSize', 'company_type_id'], 'integer'],
            [['company_taxation_type_name', 'tariffType'], 'string'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $dateRange = StatisticPeriod::getSessionPeriod();
        $query = $this->getBaseQuery($dateRange);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
            'sort' => [
                'attributes' => [
                    'created_at',
                    'pay_date',
                    'invoice_count',
                    'act_count',
                    'packing_list_count',
                    'invoice_facture_count',
                    'upd_count',
                    'product_count',
                    'service_count',
                    'has_logo',
                    'has_print',
                    'has_signature',
                    'download_statement_count',
                    'download_1c_count',
                    'days_without_payment',
                    'sum_company_images',
                    'employees_count',
                    'import_xls_product_count',
                    'import_xls_service_count',
                    'company.form_source',
                    'company.form_keyword',
                    'company.form_region',
                    'company.activities_id',
                    'pay_sum',
                    'registrationPageType' => [
                        'asc' => [
                            RegistrationPageType::tableName() . '.`name`' => SORT_ASC,
                        ],
                        'desc' => [
                            RegistrationPageType::tableName() . '.`name`' => SORT_DESC,
                        ],
                    ],
                ],
                'defaultOrder' => [
                    'pay_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([ReportState::tableName() . '.company_type_id' => $this->company_type_id]);

        $query->andFilterWhere([ReportState::tableName() . '.company_taxation_type_name' => $this->company_taxation_type_name]);

        $tariffID = preg_replace("/[^0-9]/", '', $this->tariffType);
        if (strpos($this->tariffType, 'subscribe') !== false) {
            $query->andFilterWhere([ReportState::tableName() . '.tariff_id' => $tariffID]);
        } elseif (strpos($this->tariffType, 'invoice') !== false) {
            $query->andFilterWhere([ReportState::tableName() . '.invoice_tariff_id' => $tariffID]);
        }

        $dataProvider->pagination->pageSize = $this->pageSize;

        return $dataProvider;
    }

    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     */
    public function searchModelsForExcel($params)
    {
        $dateRange = StatisticPeriod::getSessionPeriod();
        $query = $this->getBaseQuery($dateRange);

        $this->load($params);

        $query->andFilterWhere([ReportState::tableName() . '.company_type_id' => $this->company_type_id]);

        $query->andFilterWhere([ReportState::tableName() . '.company_taxation_type_name' => $this->company_taxation_type_name]);

        return $query->all();
    }

    /**
     * @return array
     */
    public function getCompanyTypeFilter()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return ArrayHelper::merge(['null' => 'Все'], ArrayHelper::map($this
            ->getBaseQuery($dateRange)
            ->joinWith('companyType')
            ->groupBy('company_type_id')
            ->all(), 'companyType.id', 'companyType.name_short'));
    }

    /**
     * @return array
     */
    public function getCompanyTaxationTypeNameFilter()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return ArrayHelper::merge(['null' => 'Все'], ArrayHelper::map($this
            ->getBaseQuery($dateRange)
            ->groupBy('company_taxation_type_name')
            ->all(), 'company_taxation_type_name', 'company_taxation_type_name'));
    }

    /**
     * @return array
     */
    public function getTariffFilter()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        $filter[null] = 'Все';
        $subscribeTariffs = [];
        foreach ($this->getBaseQuery($dateRange)
                     ->select('tariff_id')
                     ->andWhere(['not', ['tariff_id' => null]])
                     ->groupBy('tariff_id')
                     ->column() as $tariffID) {
            $tariff = SubscribeTariff::findOne($tariffID);
            if ($tariff) {
                $subscribeTariffs[$tariffID] = $tariff->tariffGroup->name .' '. $tariff->getTariffName();
            }
        }
        $invoiceTariffs = ArrayHelper::map($this->getBaseQuery($dateRange)
            ->andWhere(['not', ['invoice_tariff_id' => null]])
            ->joinWith('invoiceTariff')
            ->groupBy('invoice_tariff_id')
            ->all(), 'invoice_tariff_id', 'invoiceTariff.invoice_count');

        foreach ($subscribeTariffs as $tariffID => $subscribeTariffName) {
            $filter['subscribe' . $tariffID] = $subscribeTariffName;
        }
        foreach ($invoiceTariffs as $tariffID => $invoiceTariffName) {
            $filter['invoice' . $tariffID] = 'Счетов: ' . $invoiceTariffName;
        }

        return $filter;
    }

    /**
     * @param $dateRange
     * @return ActiveQuery
     */
    public function getBaseQuery($dateRange)
    {
        $query = ReportState::find()
            ->joinWith('company')
            ->joinWith('company.registrationPageType')
            ->andWhere([Company::tableName() . '.test' => false])
            ->andWhere(['between', 'date(from_unixtime(' . ReportState::tableName() . '.pay_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59']);

        return $query;
    }

    /**
     * @return string
     */
    public function getDownloadExcelUrl()
    {
        $getParams = \Yii::$app->request->get();
        $data[] = 'state-report-excel';
        if (!empty($getParams) && is_array(current($getParams))) {
            foreach (current($getParams) as $attribute => $getParam) {
                $data['StateSearch'][$attribute] = $getParam;
            }
        }

        return Url::to($data);
    }
}