<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2017
 * Time: 8:17
 */

namespace backend\modules\report\models;


use backend\components\helpers\DashBoardHelper;
use common\components\date\DateHelper;
use common\models\Company;
use common\models\cash\CashBankStatementUpload;
use common\models\company\Attendance;
use common\models\company\CompanyFirstEvent;
use common\models\company\RegistrationPageType;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\file\File;
use common\models\selling\SellingSubscribe;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use frontend\models\Documents;
use frontend\modules\export\models\export\Export;
use yii\db\ActiveQuery;

/**
 * Class ReportSearch
 * @package backend\modules\report\models
 */
class ReportSearch extends Company
{
    /**
     *
     */
    const DAY = 1;
    /**
     *
     */
    const WEEK = 2;
    /**
     *
     */
    const QUARTER = 4;
    /**
     *
     */
    const MONTH = 3;

    /**
     * @var int
     */
    public $periodType = self::DAY;

    /**
     * @var int
     */
    public $periodDayList = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['periodType', 'integer'],
            ['periodDayList', 'string'],
        ];
    }

    /**
     * @param $period
     * @param $type
     * @return int
     */
    public function getRegistrations($period, $type)
    {
        return (int)$this->getCompanyQuery($period, $type)
            ->andWhere('`' . Company::tableName() . '`.`id` = `' . Company::tableName() . '`.`main_id`')
            ->andWhere(['blocked' => Company::UNBLOCKED])
            ->count();
    }

    /**
     * @param $period
     * @param $type
     * @return int
     */
    public function getCompleteProfile($period, $type)
    {
        return (int)$this->getCompanyQuery($period, $type)
            ->addSelect([
                'DATE_FORMAT(FROM_UNIXTIME(created_at), "%Y-%m-%d") as created_at_format',
                'DATE_FORMAT(FROM_UNIXTIME(strict_mode_date), "%Y-%m-%d") as strict_mode_date_format',
            ])->andHaving('created_at_format = strict_mode_date_format')->count();
    }

    /**
     * @param $period
     * @param $type
     * @return int
     */
    public function getExhibitedInvoices($period, $type)
    {
        return (int)$this->getCompanyQuery($period, $type)
            ->joinWith('invoices')
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
            ->addSelect([
                'DATE_FORMAT(FROM_UNIXTIME(' . Company::tableName() . '.created_at), "%Y-%m-%d") as created_at_format',
                'DATE_FORMAT(FROM_UNIXTIME(' . Company::tableName() . '.strict_mode_date), "%Y-%m-%d") as strict_mode_date_format',
                'DATE_FORMAT(FROM_UNIXTIME(' . Company::tableName() . '.created_at), "%Y-%m-%d") as company_created_at_format',
                'DATE_FORMAT(FROM_UNIXTIME(' . Invoice::tableName() . '.created_at), "%Y-%m-%d") as invoice_created_at_format',
            ])
            ->andHaving('created_at_format = strict_mode_date_format')
            ->andHaving('company_created_at_format = invoice_created_at_format')
            ->groupBy(Invoice::tableName() . '.company_id')
            ->count();
    }

    /**
     * @param $period
     * @param $type
     * @return int
     */
    public function getSendingInvoices($period, $type)
    {
        return (int)$this->getCompanyQuery($period, $type)->addSelect([
            'FROM_UNIXTIME(created_at, "%Y-%m-%d") as created_at_format',
            'FROM_UNIXTIME(first_send_invoice_date, "%Y-%m-%d") as first_send_invoice_date_format',
        ])->andHaving('created_at_format = first_send_invoice_date_format')->count();
    }

    /**
     * @param $period
     * @param $type
     * @return int
     */
    public function getUploadedFiles($period, $type, $inDayOfRegistration = false)
    {
        $query = $this->getCompanyQuery($period, $type)
            ->innerJoinWith('companyImageUploadLogs log')
            ->andWhere(['not', ['log.type' => Company::IMAGE_CHIEF_ACCOUNTANT_SIGNATURE]])
            ->groupBy('log.company_id');

        if ($inDayOfRegistration) {
            $query->andWhere('DATE(FROM_UNIXTIME({{log}}.[[created_at]])) = DATE(FROM_UNIXTIME({{company}}.[[created_at]]))');
        }

        return (int)$query->count();
    }

    /**
     * @return array
     */
    public function getPeriod()
    {
        $result = [];
        switch ($this->periodType) {
            case (self::DAY):
                if ($this->periodDayList == 0) {
                    for ($i = 0; $i < 14; $i++) {
                        $result[] = [
                            'date' => strtotime('-' . $i . ' days'),
                        ];
                    }
                } else {
                    if ($this->periodDayList > 0) {
                        $endPeriodDate = strtotime('+' . $this->periodDayList * 7 . ' days');
                        for ($i = 0; $i < 14; $i++) {
                            $result[] = [
                                'date' => strtotime('-' . $i . ' days', $endPeriodDate),
                            ];
                        }
                    } else {
                        $abc = $this->periodDayList * 7 - 13;
                        $endPeriodDate = strtotime($abc . ' days');
                        for ($i = 0; $i < 14; $i++) {
                            $result[] = [
                                'date' => strtotime('+' . $i . ' days', $endPeriodDate),
                            ];
                        }
                    }
                }
                asort($result);
                $result['type'] = self::DAY;
                break;
            case (self::WEEK):
                $currentWeekBegin = date('N') == 1 ? time() : strtotime('previous Monday');
                $currentWeekEnd = time();
                $previousWeekBegin = $currentWeekBegin - (7 * (24 * 60 * 60));
                $previousWeekEnd = strtotime('previous Sunday');

                $result[] = [
                    'from' => $currentWeekBegin,
                    'to' => $currentWeekEnd,
                ];
                $result[] = [
                    'from' => $previousWeekBegin,
                    'to' => $previousWeekEnd,
                ];
                for ($i = 1; $i < 11; $i++) {
                    $result[] = [
                        'from' => $previousWeekBegin - ($i * 7 * (24 * 60 * 60)),
                        'to' => $previousWeekEnd - ($i * 7 * (24 * 60 * 60)),
                    ];
                }
                krsort($result);
                $result['type'] = self::WEEK;
                break;
            case (self::MONTH):
                $currentMonthBegin = date('01.m.Y');
                $currentMonthEnd = date('d.m.Y');
                $result[] = [
                    'from' => strtotime($currentMonthBegin),
                    'to' => strtotime($currentMonthEnd),
                ];
                $cycleDate = date('Y-m-d');
                for ($i = 1; $i < 12; $i++) {
                    $previousMonthEnd = strtotime($cycleDate . ' last day of last month');
                    $result[] = [
                        'from' => strtotime($cycleDate . ' first day of last month'),
                        'to' => $previousMonthEnd,
                    ];
                    $cycleDate = date('d.m.Y', $previousMonthEnd);
                }
                krsort($result);
                $result['type'] = self::MONTH;
                break;
            case (self::QUARTER):
                for ($i = 1; $i <= 4; $i++) {
                    $result[] = [
                        'from' => strtotime('01.' . str_pad($i * 3 - 2, 2, '0', STR_PAD_LEFT) . '.' . date('Y')),
                        'to' => strtotime(cal_days_in_month(CAL_GREGORIAN, $i * 3, date('Y')) . '.' . str_pad($i * 3, 2, '0', STR_PAD_LEFT) . '.' . date('Y')),
                    ];
                }
                $result['type'] = self::QUARTER;
                break;
            default:
                for ($i = 1; $i < 15; $i++) {
                    $result[] = [
                        'date' => strtotime('-' . $i . ' days'),
                    ];
                }
                krsort($result);
                $result['type'] = self::DAY;
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getXAxisTitle($period, $type)
    {
        $xAxisTitle = [];
        foreach ($period as $onePeriod) {
            if ($type == self::DAY) {
                $xAxisTitle[] = date(DateHelper::FORMAT_USER_DATE, $onePeriod['date']);
            } else {
                $xAxisTitle[] = date(DateHelper::FORMAT_USER_DATE, $onePeriod['from']) . ' - ' . date(DateHelper::FORMAT_USER_DATE, $onePeriod['to']);
            }
        }

        return $xAxisTitle;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getAllRegistrations($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = $this->getRegistrations($onePeriod, $type);
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getAllCompleteProfileCohorts($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = (int)$this->getCohortsQuery($onePeriod, $type)
                ->andWhere(['strict_mode' => Company::OFF_STRICT_MODE])
                ->count();
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return int
     */
    public function getExhibitedInvoicesCohorts($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = (int)$this->getCohortsQuery($onePeriod, $type)
                ->joinWith('invoices')
                ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                ->andWhere(['not', [Invoice::tableName() . '.id' => null]])
                ->groupBy(Company::tableName() . '.id')
                ->count();
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @param $minCount
     * @param null $maxCount
     * @return array
     */
    public function getExhibitedInvoicesCohortsByCount($period, $type, $minCount, $maxCount = null)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $query = $this->getCohortsQuery($onePeriod, $type)
                ->select([Company::tableName() . '.*', 'COUNT(' . Invoice::tableName() . '.id) as invoiceCount'])
                ->joinWith('invoices')
                ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT]);
            if ($maxCount) {
                $query->andHaving(['and',
                    ['>', 'invoiceCount', $minCount],
                    ['<=', 'invoiceCount', $maxCount],
                ]);
            } else {
                $query->andHaving(['>', 'invoiceCount', $minCount]);
            }
            $result[] = (int)$query->groupBy(Company::tableName() . '.id')->count();
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getWithLogoCohorts($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = (int)$this->getCohortsQuery($onePeriod, $type)
                ->andWhere(['or',
                    ['not', ['logo_link' => null]],
                    ['not', ['print_link' => null]],
                    ['not', ['chief_signature_link' => null]],
                ])
                ->count();
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getFreeTariffCohorts($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = (int)$this->getCohortsQuery($onePeriod, $type)
                ->joinWith('sellingSubscribes')
                ->andWhere([SellingSubscribe::tableName() . '.type' => SellingSubscribe::TYPE_TARIFF_FREE])
                ->andWhere(['not', [SellingSubscribe::tableName() . '.id' => null]])
                ->groupBy(Company::tableName() . '.id')
                ->count();
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getFirstPayedCohorts($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = (int)$this->getCohortsQuery($onePeriod, $type)
                ->joinWith('payments')
                ->andWhere(['and',
                    ['not', ['type_id' => PaymentType::TYPE_PROMO_CODE]],
                    ['in', 'tariff_id', SubscribeTariff::paidStandartIds()],
                    ['is_confirmed' => 1],
                    ['not', [Payment::tableName() . '.id' => null]],
                ])
                ->groupBy(Company::tableName() . '.id')
                ->count();
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getFirstPayedCohortsSum($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = (int)$this->getCohortsQuery($onePeriod, $type)
                ->select([Company::tableName() . '.*', Payment::tableName() . '.sum'])
                ->joinWith('payments')
                ->andWhere(['and',
                    ['not', ['type_id' => PaymentType::TYPE_PROMO_CODE]],
                    ['in', 'tariff_id', SubscribeTariff::paidStandartIds()],
                    ['is_confirmed' => 1],
                    ['not', [Payment::tableName() . '.id' => null]],
                ])
                ->orderBy([Payment::tableName() . '.created_at' => SORT_ASC])
                ->groupBy(Company::tableName() . '.id')
                ->sum('sum');
        }

        return $result;
    }

    /**
     * @param $sum
     * @param $count
     * @return array
     */
    public function getAverageCheckCohorts($sum, $count)
    {
        $result = [];
        foreach ($sum as $key => $value) {
            if ($count[$key] == 0) {
                $result[] = 0;
            } else {
                $result[] = round($value / $count[$key], 2);
            }
        }

        return $result;
    }

    /**
     * @param $registrations
     * @param $data
     * @return array
     */
    public function getCohortsPercent($registrations, $data)
    {
        $result = [];
        foreach ($registrations as $key => $registration) {
            if ($data[$key] == 0) {
                $result[] = 0;
            } else {
                $result[] = round($data[$key] / $registration * 100, 2);
            }
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return ActiveQuery
     */
    public function getCohortsQuery($period, $type)
    {
        return $this->getCompanyQuery($period, $type)
            ->andWhere('`' . Company::tableName() . '`.`id` = `' . Company::tableName() . '`.`main_id`')
            ->andWhere(['blocked' => Company::UNBLOCKED]);
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getAllCompleteProfile($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = $this->getCompleteProfile($onePeriod, $type);
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getAllExhibitedInvoices($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = $this->getExhibitedInvoices($onePeriod, $type);
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getAllSendingInvoices($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = $this->getSendingInvoices($onePeriod, $type);
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getAllUploadedFiles($period, $type, $inDayOfRegistration = false)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = $this->getUploadedFiles($onePeriod, $type, $inDayOfRegistration);
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getRegistrationPercents($period, $type)
    {
        $completeProfile = [];
        $exhibitedInvoices = [];
        $sendingInvoices = [];
        // $uploadedFiles = [];
        foreach ($period as $onePeriod) {
            $registrations = $this->getRegistrations($onePeriod, $type);
            $registrations = $registrations ? $registrations : 1;
            $completeProfile[] = round($this->getCompleteProfile($onePeriod, $type) / $registrations * 100, 2);
            $exhibitedInvoices[] = round($this->getExhibitedInvoices($onePeriod, $type) / $registrations * 100, 2);
            $sendingInvoices[] = round($this->getSendingInvoices($onePeriod, $type) / $registrations * 100, 2);
            // $uploadedFiles[] = round($this->getUploadedFiles($onePeriod, $type) / $registrations * 100, 2);
        }

        return [
            'completeProfile' => $completeProfile,
            'exhibitedInvoices' => $exhibitedInvoices,
            'sendingInvoices' => $sendingInvoices,
            // 'uploadedFiles' => $uploadedFiles,
        ];
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getActivity($period, $type)
    {
        $allActivity = [];
        $payedCompanies = [];
        $trialCompanies = [];
        $freeCompanies = [];
        $allActivityCompanies = [];
        foreach ($period as $onePeriod) {
            if ($type == self::DAY) {
                $newPeriod = [
                    'from' => date(DateHelper::FORMAT_DATE, $onePeriod['date']),
                    'to' => date(DateHelper::FORMAT_DATE, $onePeriod['date']),
                ];
            } else {
                $newPeriod = [
                    'from' => date(DateHelper::FORMAT_DATE, $onePeriod['from']),
                    'to' => date(DateHelper::FORMAT_DATE, $onePeriod['to']),
                ];
            }
            $dashBoardHelper = new DashBoardHelper($newPeriod);

            $allActivity[] = (int)$dashBoardHelper->getAllActivity();

            $payed = (int)$dashBoardHelper->getPayedCompanies();
            $trial = (int)$dashBoardHelper->getTrialCompanies();
            $free = (int)$dashBoardHelper->getFreeTariffCompanies();

            $payedCompanies[] = $payed;
            $trialCompanies[] = $trial;
            $freeCompanies[] = $free;
            $allActivityCompanies[] = $payed + $trial + $free;
        }

        return [
            'all' => $allActivity,
            'payed' => $payedCompanies,
            'trial' => $trialCompanies,
            'free' => $freeCompanies,
            'allActivityCompanies' => $allActivityCompanies,
        ];
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getMainStatistic($period, $type)
    {
        $outInvoices = [];
        $sendEmail = [];
        $inData = [];
        $coefficient = [];
        foreach ($period as $onePeriod) {
            if ($type == self::DAY) {
                $newPeriod = [
                    'from' => date(DateHelper::FORMAT_DATE, $onePeriod['date']),
                    'to' => date(DateHelper::FORMAT_DATE, $onePeriod['date']),
                ];
            } else {
                $newPeriod = [
                    'from' => date(DateHelper::FORMAT_DATE, $onePeriod['from']),
                    'to' => date(DateHelper::FORMAT_DATE, $onePeriod['to']),
                ];
            }
            $dashBoardHelper = new DashBoardHelper($newPeriod);

            $allActivity = $dashBoardHelper->getAllActivity();
            $allSum = $dashBoardHelper->getPayedCompanies() + $dashBoardHelper->getTrialCompanies() + $dashBoardHelper->getFreeTariffCompanies();
            $allSumCoefficientValue = $allSum ? $allSum : 1;

            $outInvoices[] = (int)$dashBoardHelper->getDocumentsCount(Invoice::className(), Documents::IO_TYPE_OUT);
            $sendEmail[] = $dashBoardHelper->getSentInvoicesCount();
            $inData[] = (int)$dashBoardHelper->getFilesCount(Invoice::className(), Documents::IO_TYPE_OUT) +
                (int)$dashBoardHelper->getFilesCount(Act::className(), Documents::IO_TYPE_OUT) +
                (int)$dashBoardHelper->getFilesCount(PackingList::className(), Documents::IO_TYPE_OUT) +
                (int)$dashBoardHelper->getFilesCount(InvoiceFacture::className(), Documents::IO_TYPE_OUT) +
                (int)$dashBoardHelper->getFilesCount(Invoice::className(), Documents::IO_TYPE_IN) +
                (int)$dashBoardHelper->getFilesCount(Act::className(), Documents::IO_TYPE_IN) +
                (int)$dashBoardHelper->getFilesCount(PackingList::className(), Documents::IO_TYPE_IN) +
                (int)$dashBoardHelper->getFilesCount(InvoiceFacture::className(), Documents::IO_TYPE_IN) +
                (int)$dashBoardHelper->getImport1C() +
                (int)$dashBoardHelper->getReportFile() +
                (int)$dashBoardHelper->getSpecificDocument();

            $coefficient[] = round($allActivity / $allSumCoefficientValue, 2);
        }

        return [
            'outInvoice' => $outInvoices,
            'sendEmail' => $sendEmail,
            'inData' => $inData,
            'coefficient' => $coefficient,
        ];
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getAttendance($period, $type)
    {
        $result = [];
        foreach ($period as $onePeriod) {
            $query = Attendance::find()
                ->joinWith('company')
                ->andWhere(['!=', Company::tableName() . '.test', Company::TEST_COMPANY]);
            if ($type == self::DAY) {
                $query->andWhere(['date' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
            } else {
                $query->andWhere(['between', 'date', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
            }
            $result[] = (int)$query->groupBy(Company::tableName() . '.id')->count();
        }

        return $result;
    }

    /**
     * @param $period
     * @param $type
     * @param $activationType
     * @return array
     */
    public function getActivationEmptyProfileChartInfo($period, $type, $activationType)
    {
        $attendanceCompanyCount = [];
        $completeProfileCompanyCount = [];
        $createInvoiceCompanyCount = [];
        $sendInvoiceCompanyCount = [];
        foreach ($period as $onePeriod) {
            if ($activationType == Company::ACTIVATION_EMPTY_PROFILE) {
                $completeProfileCompanyQuery = $this->getActivationBaseQuery($type, $onePeriod, $activationType);
                if ($type == self::DAY) {
                    $completeProfileCompanyQuery->andWhere(['from_unixtime(strict_mode_date, "%Y-%m-%d")' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
                } else {
                    $completeProfileCompanyQuery->andWhere(['between', 'from_unixtime(strict_mode_date, "%Y-%m-%d")', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
                }
                $completeProfileCompanyCount[] = (int)$completeProfileCompanyQuery->groupBy(Company::tableName() . '.id')->count();
            }

            if ($activationType == Company::ACTIVATION_NO_INVOICE || $activationType == Company::ACTIVATION_EMPTY_PROFILE) {
                $createInvoiceCompanyQuery = $this->getActivationBaseQuery($type, $onePeriod, $activationType)
                    ->leftJoin('invoice', 'invoice.company_id = company.id');
                $createInvoiceCompanyQuery->andWhere([
                    'type' => Documents::IO_TYPE_OUT,
                    'is_deleted' => 0,
                ]);
                if ($type == self::DAY) {
                    $createInvoiceCompanyQuery->andWhere(['from_unixtime(`invoice`.`created_at`, "%Y-%m-%d")' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
                    if ($activationType == Company::ACTIVATION_EMPTY_PROFILE) {
                        $createInvoiceCompanyQuery->andWhere(['from_unixtime(`company`.`strict_mode_date`, "%Y-%m-%d")' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
                    }
                } else {
                    $createInvoiceCompanyQuery->andWhere(['between', 'from_unixtime(`invoice`.`created_at`, "%Y-%m-%d")', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
                    if ($activationType == Company::ACTIVATION_EMPTY_PROFILE) {
                        $createInvoiceCompanyQuery->andWhere(['between', 'from_unixtime(`company`.`strict_mode_date`, "%Y-%m-%d")', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
                    }
                }
                $createInvoiceCompanyCount[] = (int)$createInvoiceCompanyQuery->groupBy([Attendance::tableName() . '.id'])->count();
            }

            $sendInvoiceCompanyQuery = $this->getActivationBaseQuery($type, $onePeriod, $activationType);
            if ($type == self::DAY) {
                $sendInvoiceCompanyQuery->andWhere(['from_unixtime(first_send_invoice_date, "%Y-%m-%d")' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
                if ($activationType == Company::ACTIVATION_EMPTY_PROFILE) {
                    $sendInvoiceCompanyQuery->andWhere(['from_unixtime(`company`.`strict_mode_date`, "%Y-%m-%d")' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
                }
            } else {
                $sendInvoiceCompanyQuery->andWhere(['between', 'from_unixtime(first_send_invoice_date, "%Y-%m-%d")', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
                if ($activationType == Company::ACTIVATION_EMPTY_PROFILE) {
                    $sendInvoiceCompanyQuery->andWhere(['between', 'from_unixtime(`company`.`strict_mode_date`, "%Y-%m-%d")', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
                }
            }

            $sendInvoiceCompanyCount[] = (int)$sendInvoiceCompanyQuery->groupBy(Company::tableName() . '.id')->count();
            $attendanceCompanyCount[] = (int)$this->getActivationBaseQuery($type, $onePeriod, $activationType)->groupBy(Company::tableName() . '.id')->count();
        }

        return [
            'attendanceCompanyCount' => $attendanceCompanyCount,
            'completeProfileCompanyCount' => $completeProfileCompanyCount,
            'createInvoiceCompanyCount' => $createInvoiceCompanyCount,
            'sendInvoiceCompanyCount' => $sendInvoiceCompanyCount,
        ];
    }

    /**
     * @param $type
     * @param $onePeriod
     * @param $activityStatus
     * @return ActiveQuery
     */
    public function getActivationBaseQuery($type, $onePeriod, $activityStatus)
    {
        $query = Attendance::find()
            ->joinWith('company')
            ->andWhere(['!=', Company::tableName() . '.test', Company::TEST_COMPANY])
            ->andWhere(['activity_status' => $activityStatus])
            ->andWhere(['is_first_visit' => true]);
        if ($type == self::DAY) {
            $query->andWhere(['date' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
        } else {
            $query->andWhere(['between', 'date', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
        }

        return $query;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getFirstTimeCompaniesOut($period, $type)
    {
        $outActCompanyCount = [];
        $outInvoiceFactureCompanyCount = [];
        $outPackingListCompanyCount = [];
        $filesCompanyCount = [];
        $inInvoiceCompanyCount = [];
        $inActCompanyCount = [];
        $inPackingListCompanyCount = [];
        $inInvoiceFactureCompanyCount = [];
        $exportCompanyCount = [];
        $statementUploadCount = [];
        $uploadedFiles = [];

        foreach ($period as $onePeriod) {
            $outActCompanyCount[] = $this->getFirstTimeActCompanyCount($onePeriod, $type, Documents::IO_TYPE_OUT);
            $outPackingListCompanyCount[] = $this->getFirstTimePackingListCompanyCount($onePeriod, $type, Documents::IO_TYPE_OUT);
            $outInvoiceFactureCompanyCount[] = $this->getFirstTimeInvoiceFactureCompanyCount($onePeriod, $type, Documents::IO_TYPE_OUT);
            $filesCompanyCount[] = $this->getFirstTimeFilesCompanyCount($onePeriod, $type);
            $inInvoiceCompanyCount[] = $this->getFirstTimeInvoiceCompanyCount($onePeriod, $type, Documents::IO_TYPE_IN);
            $inActCompanyCount[] = $this->getFirstTimeActCompanyCount($onePeriod, $type, Documents::IO_TYPE_IN);
            $inPackingListCompanyCount[] = $this->getFirstTimePackingListCompanyCount($onePeriod, $type, Documents::IO_TYPE_IN);
            $inInvoiceFactureCompanyCount[] = $this->getFirstTimeInvoiceFactureCompanyCount($onePeriod, $type, Documents::IO_TYPE_IN);
            $exportCompanyCount[] = $this->getFirstTimeExport1c($onePeriod, $type);
            $statementUploadCount[] = $this->getFirstTimeStatementUpload($onePeriod, $type);
            $uploadedFiles[] = $this->getFirstTimeUploadedFiles($onePeriod, $type);
        }

        return [
            'outActCompanyCount' => $outActCompanyCount,
            'outPackingListCompanyCount' => $outPackingListCompanyCount,
            'outInvoiceFactureCompanyCount' => $outInvoiceFactureCompanyCount,
            'filesCompanyCount' => $filesCompanyCount,
            'inInvoiceCompanyCount' => $inInvoiceCompanyCount,
            'inActCompanyCount' => $inActCompanyCount,
            'inPackingListCompanyCount' => $inPackingListCompanyCount,
            'inInvoiceFactureCompanyCount' => $inInvoiceFactureCompanyCount,
            'exportCompanyCount' => $exportCompanyCount,
            'statementUploadCount' => $statementUploadCount,
            'uploadedFiles' => $uploadedFiles,
        ];
    }

    /**
     * @param $onePeriod
     * @param $type
     * @param $ioType
     * @return int|string
     */
    public function getFirstTimeInvoiceCompanyCount($onePeriod, $type, $ioType)
    {
        $query = Company::find()
            ->select(['MIN(`invoice_facture`.`document_date`) as firstInvoiceFactureDate'])
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->joinWith('invoices')
            ->leftJoin('invoice_facture', 'invoice_facture.invoice_id = invoice.id')
            ->andWhere([Invoice::tableName() . '.type' => $ioType]);
        if ($type == self::DAY) {
            $query->andHaving(['firstInvoiceFactureDate' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
        } else {
            $query->andHaving(['between', 'firstInvoiceFactureDate', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
        }

        return (int)$query->groupBy(Invoice::tableName() . '.company_id')->count();
    }

    /**
     * @param $onePeriod
     * @param $type
     * @param $ioType
     * @return int
     */
    public function getFirstTimeActCompanyCount($onePeriod, $type, $ioType)
    {
        $query = Company::find()
            ->select(['MIN(`act`.`document_date`) as firstActDate'])
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->joinWith('invoices')
            ->leftJoin('act', 'act.invoice_id = invoice.id')
            ->andWhere([Invoice::tableName() . '.type' => $ioType]);
        if ($type == self::DAY) {
            $query->andHaving(['firstActDate' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
        } else {
            $query->andHaving(['between', 'firstActDate', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
        }

        return (int)$query->groupBy(Invoice::tableName() . '.company_id')->count();
    }

    /**
     * @param $onePeriod
     * @param $type
     * @param $ioType
     * @return int
     */
    public function getFirstTimePackingListCompanyCount($onePeriod, $type, $ioType)
    {
        $query = Company::find()
            ->select(['MIN(`packing_list`.`document_date`) as firstPackingListDate'])
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->joinWith('invoices')
            ->leftJoin('packing_list', 'packing_list.invoice_id = invoice.id')
            ->andWhere([Invoice::tableName() . '.type' => $ioType]);
        if ($type == self::DAY) {
            $query->andHaving(['firstPackingListDate' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
        } else {
            $query->andHaving(['between', 'firstPackingListDate', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
        }

        return (int)$query->groupBy(Invoice::tableName() . '.company_id')->count();
    }

    /**
     * @param $onePeriod
     * @param $type
     * @param $ioType
     * @return int
     */
    public function getFirstTimeInvoiceFactureCompanyCount($onePeriod, $type, $ioType)
    {
        $query = Company::find()
            ->select(['MIN(`invoice_facture`.`document_date`) as firstInvoiceFactureDate'])
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->joinWith('invoices')
            ->leftJoin('invoice_facture', 'invoice_facture.invoice_id = invoice.id')
            ->andWhere([Invoice::tableName() . '.type' => $ioType]);
        if ($type == self::DAY) {
            $query->andHaving(['firstInvoiceFactureDate' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
        } else {
            $query->andHaving(['between', 'firstInvoiceFactureDate', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
        }

        return (int)$query->groupBy(Invoice::tableName() . '.company_id')->count();
    }

    /**
     * @param $onePeriod
     * @param $type
     * @return int
     */
    public function getFirstTimeFilesCompanyCount($onePeriod, $type)
    {
        $query = Company::find()
            ->select(['MIN(`file`.`created_at`) as firstFileDate'])
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->joinWith('files');
        if ($type == self::DAY) {
            $query->andHaving(['from_unixtime(firstFileDate, "%Y-%m-%d")' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
        } else {
            $query->andHaving(['between', 'from_unixtime(firstFileDate, "%Y-%m-%d")', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
        }

        return (int)$query->groupBy(File::tableName() . '.company_id')->count();
    }

    /**
     * @param $onePeriod
     * @param $type
     * @return int
     */
    public function getFirstTimeExport1c($onePeriod, $type)
    {
        $query = Company::find()
            ->select(['MIN(`export`.`created_at`) as firstExportDate'])
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->joinWith('export');
        if ($type == self::DAY) {
            $query->andHaving(['from_unixtime(firstExportDate, "%Y-%m-%d")' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
        } else {
            $query->andHaving(['between', 'from_unixtime(firstExportDate, "%Y-%m-%d")', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
        }

        return (int)$query->groupBy(Export::tableName() . '.company_id')->count();
    }

    /**
     * @param $onePeriod
     * @param $type
     * @return int
     */
    public function getFirstTimeStatementUpload($onePeriod, $type)
    {
        $stTable = CashBankStatementUpload::tableName();
        $query = Company::find()
            ->select(["MIN({{{$stTable}}}.[[created_at]]) as firstDate"])
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->joinWith('cashBankStatementUploads');
        if ($type == self::DAY) {
            $query->andHaving(['from_unixtime(firstDate, "%Y-%m-%d")' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
        } else {
            $query->andHaving(['between', 'from_unixtime(firstDate, "%Y-%m-%d")', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
        }

        return (int)$query->groupBy(CashBankStatementUpload::tableName() . '.company_id')->count();
    }

    /**
     * @param $period
     * @param $type
     * @return int
     */
    public function getFirstTimeUploadedFiles($onePeriod, $type)
    {
        $excludeIdArray = Company::find()
            ->select('log.company_id')
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->innerJoinWith('companyImageUploadLogs log')
            ->andWhere(['not', ['log.type' => Company::IMAGE_CHIEF_ACCOUNTANT_SIGNATURE]])
            ->andWhere('DATE(FROM_UNIXTIME({{log}}.[[created_at]])) = DATE(FROM_UNIXTIME({{company}}.[[created_at]]))')
            ->groupBy('log.company_id')
            ->column();


        $query = Company::find()
            ->select(["MIN({{log}}.[[created_at]]) as firstDate"])
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->innerJoinWith('companyImageUploadLogs log')
            ->andWhere(['not', ['log.type' => Company::IMAGE_CHIEF_ACCOUNTANT_SIGNATURE]])
            ->andWhere(['not', ['log.company_id' => $excludeIdArray]])
            ->groupBy('log.company_id');

        if ($type == self::DAY) {
            $query->andHaving(['from_unixtime(firstDate, "%Y-%m-%d")' => date(DateHelper::FORMAT_DATE, $onePeriod['date'])]);
        } else {
            $query->andHaving(['between', 'from_unixtime(firstDate, "%Y-%m-%d")', date(DateHelper::FORMAT_DATE, $onePeriod['from']), date(DateHelper::FORMAT_DATE, $onePeriod['to'])]);
        }

        return (int)$query->count();
    }

    /**
     * @param $period
     * @param $type
     * @return ActiveQuery
     */
    public function getCompanyQuery($period, $type)
    {
        $query = Company::find()
            ->andWhere(['!=', 'test', Company::TEST_COMPANY]);
        if ($type == self::DAY) {
            $query->andWhere(['between', 'DATE_FORMAT(FROM_UNIXTIME(' . Company::tableName() . '.created_at), "%Y-%m-%d")', date('Y-m-d', $period['date']), date('Y-m-d', $period['date'])]);
        } else {
            $query->andWhere(['between', 'DATE_FORMAT(FROM_UNIXTIME(' . Company::tableName() . '.created_at), "%Y-%m-%d")', date('Y-m-d', $period['from']), date('Y-m-d', $period['to'])]);
        }

        return $query;
    }

    public function getRobotViews($period, $type, $byCompanies, $event_id)
    {
        $firstPeriod = reset($period);

        $result = [];
        foreach ($period as $onePeriod) {
            $result[] = $this->getRobotOneView($onePeriod, $type, $byCompanies, $event_id, $firstPeriod);
        }

        return $result;
    }

    public function getRobotOneView($period, $type, $byCompanies, $event_id, $firstPeriod)
    {
        $query = CompanyFirstEvent::find()
            ->andWhere('`' . CompanyFirstEvent::tableName() . '`.`event_id` = ' . (int)$event_id);

        if ($type == self::DAY) {
            $query->andWhere(['between', 'DATE_FORMAT(FROM_UNIXTIME(' . CompanyFirstEvent::tableName() . '.created_at), "%Y-%m-%d")', date('Y-m-d', $period['date']), date('Y-m-d', $period['date'])]);
        } else {
            $query->andWhere(['between', 'DATE_FORMAT(FROM_UNIXTIME(' . CompanyFirstEvent::tableName() . '.created_at), "%Y-%m-%d")', date('Y-m-d', $period['from']), date('Y-m-d', $period['to'])]);
        }

        switch ($byCompanies) {
            case 'new':
                $companyCondition = '>=';
                break;
            case 'old':
                $companyCondition = '<';
                break;
            default:
                $companyCondition = null;
                break;
        }

        $query->innerJoin(Company::tableName(), 'company.id = company_first_event.company_id');
        $query->andWhere(['!=', Company::tableName() . '.test', Company::TEST_COMPANY]);

        if ($companyCondition) {
            if ($type == self::DAY) {
                $query->andWhere([$companyCondition, 'DATE_FORMAT(FROM_UNIXTIME(' . Company::tableName() . '.created_at), "%Y-%m-%d")', date('Y-m-d', $firstPeriod['date'])]);
            } else {
                $query->andWhere([$companyCondition, 'DATE_FORMAT(FROM_UNIXTIME(' . Company::tableName() . '.created_at), "%Y-%m-%d")', date('Y-m-d', $firstPeriod['from'])]);
            }
        }

        return (int)$query->count();
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getRegistrationsByChannelSeries($period, $type)
    {
        $series = [];
        /* @var $registrationPageType RegistrationPageType */
        foreach (RegistrationPageType::find()->all() as $registrationPageType) {
            $data = [];
            foreach ($period as $onePeriod) {
                $data[] = (int)$this->getCompanyQuery($onePeriod, $type)
                    ->andWhere(['registration_page_type_id' => $registrationPageType->id])
                    ->count();
            }
            $series[] = [
                'type' => 'column',
                'name' => $registrationPageType->name,
                'pointWidth' => 5,
                'data' => $data,
            ];
        }

        return $series;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getCompleteProfileByChannelSeries($period, $type)
    {
        $series = [];
        /* @var $registrationPageType RegistrationPageType */
        foreach (RegistrationPageType::find()->all() as $registrationPageType) {
            $data = [];
            foreach ($period as $onePeriod) {
                $data[] = (int)$this->getCompanyQuery($onePeriod, $type)
                    ->addSelect([
                        'DATE_FORMAT(FROM_UNIXTIME(created_at), "%Y-%m-%d") as created_at_format',
                        'DATE_FORMAT(FROM_UNIXTIME(strict_mode_date), "%Y-%m-%d") as strict_mode_date_format',
                    ])
                    ->andWhere(['registration_page_type_id' => $registrationPageType->id])
                    ->andHaving('created_at_format = strict_mode_date_format')->count();
            }
            $series[] = [
                'type' => 'column',
                'name' => $registrationPageType->name,
                'pointWidth' => 5,
                'data' => $data,
            ];
        }

        return $series;
    }

    /**
     * @param $period
     * @param $type
     * @return array
     */
    public function getExhibitedInvoicesByChannelSeries($period, $type)
    {
        $series = [];
        /* @var $registrationPageType RegistrationPageType */
        foreach (RegistrationPageType::find()->all() as $registrationPageType) {
            $data = [];
            foreach ($period as $onePeriod) {
                $data[] = (int)$this->getCompanyQuery($onePeriod, $type)
                    ->joinWith('invoices')
                    ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                    ->addSelect([
                        'DATE_FORMAT(FROM_UNIXTIME(' . Company::tableName() . '.created_at), "%Y-%m-%d") as company_created_at_format',
                        'DATE_FORMAT(FROM_UNIXTIME(' . Invoice::tableName() . '.created_at), "%Y-%m-%d") as invoice_created_at_format',
                    ])
                    ->andWhere(['registration_page_type_id' => $registrationPageType->id])
                    ->andHaving('company_created_at_format = invoice_created_at_format')
                    ->groupBy(Company::tableName() . '.id')
                    ->count();
            }
            $series[] = [
                'type' => 'column',
                'name' => $registrationPageType->name,
                'pointWidth' => 5,
                'data' => $data,
            ];
        }

        return $series;
    }

    public function getChannelPercent($period, $type)
    {
        $series = [];
        /* @var $registrationPageType RegistrationPageType */
        foreach (RegistrationPageType::find()->all() as $registrationPageType) {
            $data = [];
            foreach ($period as $onePeriod) {
                $registrationCount = (int)$this->getCompanyQuery($onePeriod, $type)
                    ->andWhere(['registration_page_type_id' => $registrationPageType->id])
                    ->count();
                $completeProfileCount = (int)$this->getCompanyQuery($onePeriod, $type)
                    ->addSelect([
                        'DATE_FORMAT(FROM_UNIXTIME(created_at), "%Y-%m-%d") as created_at_format',
                        'DATE_FORMAT(FROM_UNIXTIME(strict_mode_date), "%Y-%m-%d") as strict_mode_date_format',
                    ])
                    ->andWhere(['registration_page_type_id' => $registrationPageType->id])
                    ->andHaving('created_at_format = strict_mode_date_format')->count();
                $data[] = round($completeProfileCount / ($registrationCount ? $registrationCount : 1), 2) * 100;
            }
            $series[] = [
                'type' => 'column',
                'name' => $registrationPageType->name,
                'pointWidth' => 5,
                'data' => $data,
            ];
        }

        return $series;
    }
}