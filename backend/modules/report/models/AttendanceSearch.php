<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.02.2017
 * Time: 16:28
 */

namespace backend\modules\report\models;


use common\models\Company;
use common\models\company\Attendance;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class AttendanceSearch
 * @package backend\modules\report\models
 */
class AttendanceSearch extends Attendance
{
    public $pageSize = 10;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'pageSize'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attendance::find()
            ->joinWith('company')
            ->andWhere([Company::tableName() . '.test' => false]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['company_id' => $this->company_id]);

        $dataProvider->pagination->pageSize = $this->pageSize;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getCompanyFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(Attendance::find()
            ->joinWith('company')
            ->andWhere([Company::tableName() . '.test' => false])
            ->groupBy(Company::tableName() . '.id')
            ->all(), 'company.id', 'company.shortName'));
    }
}