<?php

namespace backend\modules\report\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use common\models\pattern\Pattern;
use common\models\pattern\PatternStepEvent;
use common\models\company\Event;

/**
 * EventSearch
 */
class EventSearch extends \yii\base\Model
{
    protected $_pattern;
    protected $_columns = [];

    /**
     * @inheritdoc
     */
    public function __construct(Pattern $pattern, $config = [])
    {
        $this->_pattern = $pattern;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $steps = range(1, $this->_pattern->step_count);
        foreach ($steps as $step) {
            $this->_columns[$step] = 'step' . $step;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function getColumns()
    {
        return $this->_columns;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Event::find()->select('event.*');
        $select = [];
        $attributes = ['name'];
        foreach ($this->columns as $key => $value) {
            $select[$value] = "IFNULL({{t{$key}}}.[[event_count]], 0)";
            $attributes[] = $value;
            $query->leftJoin(["t{$key}" => PatternStepEvent::tableName()], "
                {{t{$key}}}.[[event_id]] = {{event}}.[[id]]
                AND {{t{$key}}}.[[step_pattern_id]] = {$this->_pattern->id}
                AND {{t{$key}}}.[[step_step]] = {$key}
            ");
        }
        $query->addSelect($select);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        return $dataProvider;
    }
}
