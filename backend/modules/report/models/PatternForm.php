<?php

namespace backend\modules\report\models;

use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\company\Event;
use common\models\pattern\Pattern;
use common\models\pattern\PatternStep;
use common\models\pattern\PatternStepEvent;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\db\Query;

/**
 * Pattern form
 *
 * @property string $start
 * @property string $end
 */
class PatternForm extends Model
{
    public $start;
    public $end;

    protected $_pattern;
    protected $events;
    protected $companyIds = [];
    protected $columns;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $start = new \DateTime('first day of -1 month');
        $end = new \DateTime('last day of -1 month');

        $this->start = $start->format('d.m.Y');
        $this->end = $end->format('d.m.Y');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start', 'end'], 'required'],
            [['start', 'end'], 'date', 'format' => 'php:d.m.Y'],
            [['end'], function ($attribute, $params) {
                $dateStart = date_create_from_format('d.m.Y', $this->start);
                $dateEnd = date_create_from_format('d.m.Y', $this->end);

                if ($dateStart > $dateEnd) {
                    $this->addError($attribute, 'Конец отчета не может быть раньше начала.');
                }
            }, 'skipOnError' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'start' => 'Начало отчета',
            'end' => 'Конец отчета',
        ];
    }

    /**
     * @return Pattern | null
     */
    public function getPattern()
    {
        return $this->_pattern;
    }

    /**
     * @return boolean
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $result = Yii::$app->db->transaction(function (Connection $db) {
            $dateStart = date_create_from_format('d.m.Y', $this->start);
            $dateEnd = date_create_from_format('d.m.Y', $this->end);
            $companyWhere = [
                'and',
                ['company.blocked' => false],
                ['between', 'company.created_at', $dateStart->getTimestamp(), $dateEnd->getTimestamp()],
            ];
            $stepCount = (int) CompanyFirstEvent::find()
                ->joinWith('company', false)
                ->andWhere($companyWhere)
                ->max('step');
            $payQuery = Subscribe::find()
                ->alias('s')
                ->select('company.id')
                ->distinct()
                ->joinWith('company', false, 'INNER JOIN')
                ->andWhere($companyWhere)
                ->andWhere([
                    'tariff_id' => SubscribeTariff::paidStandartIds(),
                    'status_id' => SubscribeStatus::$realSubscribes,
                ])
                ->andWhere(['between', 's.created_at', $dateStart->getTimestamp(), $dateEnd->getTimestamp()]);

            $this->_pattern = new Pattern([
                'created_at' => time(),
                'date_start' => $dateStart->format('Y-m-d'),
                'date_end' => $dateEnd->format('Y-m-d'),
                'interval' => date_diff($dateStart, $dateEnd)->days + 1,
                'signup_count' => Company::find()->where($companyWhere)->count('company.id'),
                'payment_count' => $payQuery->count(),
                'step_count' => max(1, $stepCount),
            ]);

            if ($this->_pattern->save()) {
                $eventArray = Event::find()->all();
                $columnArray = range(1, $this->_pattern->step_count);
                $rows = [];
                foreach ($columnArray as $column) {
                    $step = new PatternStep(['pattern_id' => $this->_pattern->id, 'step' => $column]);
                    if ($step->save(false)) {
                        $data = CompanyFirstEvent::find()->alias('link')
                            ->joinWith('company', false, 'INNER JOIN')
                            ->select(['count' => 'COUNT(*)', 'link.event_id'])
                            ->andWhere($companyWhere)
                            ->andWhere(['link.step' => $column])
                            ->groupBy('link.event_id')
                            ->indexBy('event_id')
                            ->column();

                        foreach ($eventArray as $event) {
                            $rows[] = [
                                $this->_pattern->id,
                                $column,
                                $event->id,
                                isset($data[$event->id]) ? $data[$event->id] : 0,
                            ];
                        }
                    } else {
                        $db->transaction->rollBack()->batchInsert();

                        return false;
                    }
                }

                if ($rows) {
                    $command = Yii::$app->db->createCommand()->batchInsert(PatternStepEvent::tableName(), [
                        'step_pattern_id',
                        'step_step',
                        'event_id',
                        'event_count',
                    ], $rows);

                    try {
                        if ($command->execute()) {
                            return true;
                        }
                    } catch (\Exception $e) {
                    }
                }
            }
            $db->transaction->rollBack();

            return false;
        });

        if (!$result) {
            Yii::$app->session->setFlash('error', 'Во время создания отчета произошла ошибка');
        }

        return $result;
    }
}
