<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.11.2017
 * Time: 7:59
 */

namespace backend\modules\report\models;


use common\models\Company;
use common\models\document\Invoice;
use common\models\service\Payment;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * Class LeaveCompaniesSearch
 * @package backend\modules\report\models
 */
class LeaveCompaniesSearch extends Subscribe
{
    /**
     * @var int
     */
    public $pageSize = 10;

    public $tariffType;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pageSize', 'tariffType'], 'integer'],
        ];
    }

    private $_subscribesIds;

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $subscribeIDs = $this->getSubscribeIds();

        $query = Subscribe::find()
            ->select([Subscribe::tableName() . '.*',
                'CAST(' . Payment::tableName() . '.sum AS UNSIGNED) as paymentSum',
                Payment::tableName() . '.payment_date as paymentDate',
                'MAX(' . Invoice::tableName() . '.document_date) as maxInvoiceDocumentDate',
                'COUNT(' . Invoice::tableName() . '.id) as invoiceCount',
                'MAX({{visit}}.[[time]]) as lastVisitAt',
                'MAX(' . Subscribe::tableName() . '.expired_at) as lastExpiredAt',
            ])
            ->joinWith('payment')
            ->joinWith('company')
            ->leftJoin([
                'visit' => common\models\company\CompanyLastVisit::tableName(),
            ], '{{visit}}.[[company_id]]='.Subscribe::tableName().'.[[company_id]]')
            ->leftJoin(Invoice::tableName(), Invoice::tableName() . '.company_id = ' . Company::tableName() . '.id
                AND ' . Invoice::tableName() . '.type = ' . Documents::IO_TYPE_OUT . ' AND ' . Invoice::tableName() . '.is_deleted = 0')
            ->andWhere(['in', Subscribe::tableName() . '.id', $subscribeIDs])
            ->groupBy([Subscribe::tableName() . '.id']);

        $this->load($params);
        $query->andFilterWhere([Subscribe::tableName() . '.tariff_id' => $this->tariffType]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
            'sort' => [
                'attributes' => [
                    'expired_at',
                    'paymentSum',
                    'paymentDate',
                    'maxInvoiceDocumentDate',
                    'invoiceCount',
                    'lastVisitAt',
                    'lastExpiredAt',
                ],
                'defaultOrder' => [
                    'expired_at' => SORT_ASC,
                ],
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->pagination->pageSize = $this->pageSize;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getSubscribeIds()
    {
        if ($this->_subscribesIds)
            return $this->_subscribesIds;

        $dateRange = StatisticPeriod::getSessionPeriod();
        $subscribeIDs = [];

        $tariffsByGroups = SubscribeTariffGroup::getTariffsByGroups();

        // Correct daterange on Today
        $rangeTo = date('Ymd', strtotime($dateRange['to']));
        $rangeFrom = date('Y-m-d', strtotime($dateRange['from']));
        if ($rangeFrom == date('Y-01-01') && $rangeTo >= date('Ymd')) {
            $dateRange['to'] = date("Y-m-d");
        }

        foreach ($tariffsByGroups as $groupTariffs) {

            $companiesWithEndSubscribe = Subscribe::find()
                ->select(['company_id', 'MAX(expired_at) as maxExpiredAt', Subscribe::tableName() . '.id as subscribeID', Subscribe::tableName() . '.tariff_id as tariffID'])
                ->andWhere([Subscribe::tableName() . '.tariff_id' => $groupTariffs])
                ->andWhere(['and',
                    ['!=', 'tariff_id', SubscribeTariff::TARIFF_TRIAL],
                    ['not', ['tariff_id' => null]],
                ])
                ->joinWith('company')
                ->andWhere(['!=', 'test', Company::TEST_COMPANY])
                ->andWhere(['between', 'date(from_unixtime(`' . Subscribe::tableName() . '`.`expired_at`))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])
                ->orderBy([Subscribe::tableName() . '.expired_at' => SORT_ASC])
                ->groupBy('company_id')
                ->asArray()->all();

            foreach ($companiesWithEndSubscribe as $data) {
                if ($data['maxExpiredAt'] !== null) {
                    $lastActivatedSubscribe = Subscribe::find()
                        ->andWhere(['not', [Subscribe::tableName() . '.id' => $data['subscribeID']]])
                        ->andWhere(['tariff_id' => $groupTariffs])
                        ->byStatus([SubscribeStatus::STATUS_PAYED, SubscribeStatus::STATUS_ACTIVATED])
                        ->byCompany($data['company_id'])
                        ->andWhere(['OR',
                            ['>', 'date(from_unixtime(`' . Subscribe::tableName() . '`.`expired_at`))', $dateRange['to'] . ' 23:59:59'],
                            ['IS', 'expired_at', null],
                        ])
                        ->orderBy(['expired_at' => SORT_DESC])
                        ->one();

                    if (!$lastActivatedSubscribe && in_array($data['tariffID'], $groupTariffs)) {
                        $subscribeIDs[] = $data['subscribeID'];
                    }
                }
            }
        }

        $this->_subscribesIds = array_unique($subscribeIDs);

        return $this->_subscribesIds;
    }

    /**
     * @return array
     */
    public function getTariffFilter()
    {
        $result[null] = 'Все';
        $subscribesIds = $this->getSubscribeIds();

        foreach (Subscribe::find()->where(['id' => $subscribesIds])
                     ->select(Subscribe::tableName() . '.tariff_id as tariff')
                     ->groupBy(Subscribe::tableName() . '.tariff_id')
                     ->column() as $tariffID) {
            $tariff = SubscribeTariff::findOne($tariffID);
            if ($tariff) {
                $result[$tariffID] = $tariff->tariffGroup->name .' '. $tariff->getTariffName();
            }
        }

        return $result;
    }
}