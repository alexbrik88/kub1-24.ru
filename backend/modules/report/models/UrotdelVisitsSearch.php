<?php
declare(strict_types=1);

namespace backend\modules\report\models;

use common\components\helpers\ArrayHelper;
use common\models\UrotdelVisit;
use frontend\components\StatisticPeriod;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;

final class UrotdelVisitsSearch extends Model {

    public $company_name_short;

    public $company_phone;

    public $company_fio;

    public $company_registration_page_type;

    public $company_utm_source;

    public function rules(): array {
        return [
            [['company_name_short', 'company_phone', 'company_fio', 'company_registration_page_type', 'company_utm_source'], 'safe'],
        ];
    }

    public function search(array $params = []) {
        $this->load($params);

        $query = $this->getQuery()->orderBy('created_at DESC');

        $query->andFilterWhere(['c.registration_page_type_id' => $this->company_registration_page_type]);
        $query->andFilterWhere(['c.utm_source' => $this->company_utm_source]);

        if ($this->company_name_short) {
            $query->andWhere([
                'AND',
                ['NOT', ['c.name_short' => null]],
                ['!=', 'c.name_short', ''],
            ]);
        } elseif ($this->company_name_short === '0') {
            $query->andWhere(['c.name_short' => null]);
        } else {
            $query->andFilterWhere(['c.name_short' => $this->company_phone]);
        }

        if ($this->company_phone) {
            $query->andWhere([
                'AND',
                ['NOT', ['c.phone' => null]],
                ['NOT', ['c.phone' => '']],
            ]);
        } elseif ($this->company_phone === '0') {
            $query->andWhere([
                'AND',
                ['c.phone' => null],
                ['c.phone' => ''],
            ]);
        } else {
            $query->andFilterWhere(['c.phone' => $this->company_phone]);
        }

        if ($this->company_fio) {
            $query->andWhere([
                'OR',
                ['NOT', ['c.chief_firstname' => null]],
                ['NOT', ['c.chief_lastname' => null]],
                ['NOT', ['c.chief_patronymic' => null]],
            ]);
        } elseif ($this->company_fio === '0') {
            $query->andWhere([
                'AND',
                ['c.chief_firstname' => null],
                ['c.chief_lastname' => null],
                ['c.chief_patronymic' => null],
            ]);
        }

        return new ArrayDataProvider([
            'allModels' => $query->all(),
            'sort' => [
                'attributes' => [
                    'uv.created_at',
                ],
            ],
        ]);
    }

    public function getRegistrationPageTypeFilter(): array {
        $registrationPageTypeArray[null] = 'Все';
        $registrationPageTypeArray += ArrayHelper::map($this->getQuery()
            ->andWhere(['or',
                ['not', ['c.registration_page_type_id' => null]],
                ['not', ['c.registration_page_type_id' => '']],
            ])
            ->groupBy('c.registration_page_type_id')
            ->orderBy('registration_page_type.name')
            ->all(), 'company.registrationPageType.id', 'company.registrationPageType.name');

        return $registrationPageTypeArray;
    }

    public function getUtmSourceFilter(): array {
        $utmSourceArray[null] = 'Все';
        $utmSourceArray += ArrayHelper::map($this->getQuery()
            ->andWhere(['or',
                ['not', ['c.utm_source' => null]],
                ['not', ['c.utm_source' => '']],
            ])
            ->groupBy('c.utm_source')
            ->orderBy('c.utm_source')
            ->all(), 'company.utm_source', 'company.utm_source');

        return $utmSourceArray;
    }

    private function getQuery(): ActiveQuery {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return UrotdelVisit::find()
            ->alias('uv')
            ->joinWith('company c')
            ->joinWith('company.registrationPageType')
            ->andWhere(['between', 'date(from_unixtime(`uv`.`created_at`))', $dateRange['from'], $dateRange['to']]);
    }

}
