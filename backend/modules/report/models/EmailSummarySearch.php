<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.08.2017
 * Time: 15:54
 */

namespace backend\modules\report\models;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class EmailSummarySearch
 * @package backend\modules\report\models
 */
class EmailSummarySearch extends Company
{
    /**
     * @var int
     */
    public $pageSize = 10;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pageSize'], 'integer'],
            [['form_keyword', 'form_region', 'form_source', 'company_type_id'], 'string'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->getCompanies();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['form_keyword' => $this->form_keyword]);

        $query->andFilterWhere(['form_region' => $this->form_region]);

        $query->andFilterWhere(['form_source' => $this->form_source]);

        $query->andFilterWhere(['company_type_id' => $this->company_type_id]);

        $dataProvider->pagination->pageSize = $this->pageSize;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getFormKeyWordFilter()
    {
        $result = $this->getCompanies()->select('form_keyword')->distinct()->indexBy('form_keyword')->column();

        return ArrayHelper::merge($result, [null => 'Все']);
    }

    /**
     * @return array
     */
    public function getFormKeyRegionFilter()
    {
        $result = $this->getCompanies()->select('form_region')->distinct()->indexBy('form_region')->column();

        return ArrayHelper::merge($result, [null => 'Все']);
    }

    /**
     * @return array
     */
    public function getFormKeySourceFilter()
    {
        $result = $this->getCompanies()->select('form_source')->distinct()->indexBy('form_source')->column();

        return ArrayHelper::merge($result, [null => 'Все']);
    }

    /**
     * @return array
     */
    public function getCompanyTypeFilter()
    {
        $result = $this->getCompanies()->joinWith('companyType type')->select([
            'type.name_short',
            'type.id',
        ])->distinct()->indexBy('id')->column();

        return ArrayHelper::merge($result, [null => 'Все']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCompanies()
    {
        return Company::find()
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->orderBy(['created_at' => SORT_DESC]);
    }
}