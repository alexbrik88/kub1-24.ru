<?php
namespace backend\modules\report\models;

use common\models\Company;
use common\models\document\Invoice;
use common\models\service\Payment;
use common\models\service\PaymentType;
use frontend\models\Documents;

/**
 * Class CoefficientCompanyHelper
 */
class CoefficientCompanyHelper
{
    public static function getExcelColumn($num) {
        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            return self::getExcelColumn($num2) . $letter;
        } else {
            return $letter;
        }
    }

    public static function getDateRange($month, $year) {

        $date = new \DateTime();
        $date->setDate($year, $month, 1);

        return [
            'from' => $date->format('Y-m-d'),
            'to' => $date->format('Y-m-t')
        ];
    }

    public static function getPaymentsSum($company_id, $month, $year, $tariffIds = []) {

        $dateRange = self::getDateRange($month, $year);

        return Payment::find()
            ->joinWith('company', false)
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andWhere(['not', ['service_payment.type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->andWhere(['service_payment.tariff_id' => $tariffIds])
            ->andWhere(['service_payment.is_confirmed' => 1])
            ->andWhere(['company_id' => $company_id])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . 'service_payment.payment_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])
            ->groupBy('company_id')
            ->sum('sum');
    }

    public static function getInvoicesCount($company_id, $month, $year, $tariffIds = []) {

        $dateRange = self::getDateRange($month, $year);

        return Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted()
            ->andWhere(['between', 'invoice.document_date', $dateRange['from'], $dateRange['to']])
            ->byCompany($company_id)
            ->count();
    }

    public static function getTotalPaymentsSum($company_id, $month, $year, $tariffIds = []) {

        $dateRange = self::getDateRange($month, $year);

        return Payment::find()
            ->joinWith('company', false)
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andWhere(['not', ['service_payment.type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->andWhere(['service_payment.tariff_id' => $tariffIds])
            ->andWhere(['service_payment.is_confirmed' => 1])
            ->andWhere(['company_id' => $company_id])
            ->andWhere(['>', 'DATE(FROM_UNIXTIME(' . 'service_payment.payment_date))', $dateRange['from'] . ' 00:00:00'])
            ->sum('sum');
    }
}