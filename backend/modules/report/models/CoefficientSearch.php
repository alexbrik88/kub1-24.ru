<?php
namespace backend\modules\report\models;


use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\company\Attendance;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\report\ReportState;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\ServiceModule;
use common\models\service\ServiceModuleVisit;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\components\StatisticPeriod;
use function GuzzleHttp\Psr7\str;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class SellingPaymentSearch
 * @package backend\modules\service\models
 */
class CoefficientSearch
{
    const DAY = 1;
    const WEEK = 2;
    const MONTH = 3;

    /**
     * @var
     */
    public $year;

    /**
     * @var
     */
    public $month;

    /**
     * @var
     */
    public $searchByYear = false;

    /**
     * @var
     */
    public $returnCompaniesIds = false;

    /**
     * @var
     */
    public static $tariffByGroup;

    public static $moduleByTariffGroup = [
        SubscribeTariffGroup::STANDART => ServiceModule::STANDART,
        SubscribeTariffGroup::LOGISTICS => ServiceModule::LOGISTICS,
        SubscribeTariffGroup::B2B_PAYMENT => ServiceModule::B2B_PAYMENT,
        SubscribeTariffGroup::TAX_DECLAR_IP_USN_6 => ServiceModule::IP_USN_6,
        SubscribeTariffGroup::TAX_IP_USN_6 => ServiceModule::IP_USN_6,
        SubscribeTariffGroup::ANALYTICS => ServiceModule::ANALYTICS,
    ];

    private static $_groupIds;

    public static function getGroupIds()
    {
        if (!isset(self::$_groupIds)) {
            self::$_groupIds = SubscribeTariffGroup::find()->active()->select('id')->orderBy([
                '([[id]] = :standart)' => SORT_DESC,
                'name' => SORT_ASC,
            ])->addParams([
                ':standart' => SubscribeTariffGroup::STANDART,
            ])->column();
        }

        return self::$_groupIds;
    }

    public function init()
    {
        foreach (self::getGroupIds() as $groupId) {
            self::$tariffByGroup[$groupId] = SubscribeTariff::find()->paid()->where([
                'tariff_group_id' => $groupId,
            ])->select('id')->column();
        }
    }

    private function getSubscribeBaseQuery() {

        return Subscribe::find()
            ->joinWith('company', false)
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andWhere(['and',
                ['!=', 'service_subscribe.tariff_id', SubscribeTariff::TARIFF_TRIAL],
                ['not', ['service_subscribe.tariff_id' => null]],
            ])
            ->andWhere(['service_subscribe.status_id' => [SubscribeStatus::STATUS_PAYED, SubscribeStatus::STATUS_ACTIVATED]]);
    }

    private function getPaymentBaseQuery() {

        return Payment::find()
            ->joinWith('company', false)
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andWhere(['not', ['service_payment.type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->andWhere(['and',
                ['!=', 'service_payment.tariff_id', SubscribeTariff::TARIFF_TRIAL],
                ['not', ['service_payment.tariff_id' => null]],
            ])
            ->andWhere(['service_payment.is_confirmed' => 1]);
    }

    /**
     * Кол-во платящих клиентов
     * @param $tariffGroupId
     * @return false|string|array|null
     */
    public function getCountPayers($tariffGroupId, $dateOffset = null)
    {
        $dateRange = $this->getDateRange($dateOffset);
        $endMonthTime = strtotime($dateRange['to'] . ' 23:59:59');

        $query2 = $this->getSubscribeBaseQuery()
            ->andWhere(['tariff_group_id' => $tariffGroupId])
            ->andWhere(['>', 'expired_at', $endMonthTime])
            ->andWhere(['<', 'activated_at', $endMonthTime])
            ->groupBy('company_id');

        if ($this->returnCompaniesIds) {
            return (new Query())->select('company_id')->from($query2)->column() ?: [];
        }

        return (new Query())->select('COUNT(*)')->from($query2)->scalar() ?: 0;
    }

    /**
     * Кол-во новых клиентов
     * @param $tariffGroupId
     * @return false|string|array|null
     */
    public function getCountNewPayers($tariffGroupId)
    {
        $dateRange = $this->getDateRange();

        $query = ReportState::find()
            ->joinWith('company')
            ->andWhere([Company::tableName() . '.test' => false])
            ->andWhere(['tariff_id' => self::$tariffByGroup[$tariffGroupId]])
            ->andWhere(['between', 'date(from_unixtime(' . ReportState::tableName() . '.pay_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59']);

        if ($this->returnCompaniesIds) {
            return $query->select('company.id')->column() ?: [];
        }

        return $query->count() ?: 0;
    }

    /**
     * Кол-во вернувшихся клиентов
     * @param $tariffGroupId
     * @return false|string|null|array
     */
    public function getCountReturnedPayers($tariffGroupId)
    {
        $dateRange = $this->getDateRange();

        $returnedCompanies = 0;
        $returnedCompaniesArr = [];

        $query2 = $this->getPaymentBaseQuery()
            ->andWhere(['tariff_id' => self::$tariffByGroup[$tariffGroupId]])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Payment::tableName() . '.payment_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])
            ->groupBy('company_id');

        $payedCompaniesArr = $query2->select('company_id')->asArray()->all();

        foreach ($payedCompaniesArr as $data) {
            if ($this->getSubscribeBaseQuery()
                    ->byCompany($data['company_id'])
                    ->andWhere(['tariff_group_id' => $tariffGroupId])
                    ->andWhere(['OR',
                        ['between', 'DATE(FROM_UNIXTIME(' . Subscribe::tableName() . '.expired_at))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'],
                        ['IS', 'expired_at', null],
                    ])
                    ->orderBy(['expired_at' => SORT_DESC])
                    ->count() == 0

                &&

                $this->getSubscribeBaseQuery()
                    ->byCompany($data['company_id'])
                    ->andWhere(['tariff_group_id' => $tariffGroupId])
                    ->andWhere(['OR',
                        ['<', 'DATE(FROM_UNIXTIME(' . Subscribe::tableName() . '.expired_at))', $dateRange['from'] . ' 00:00:00'],
                        ['IS', 'expired_at', null],
                    ])
                    ->orderBy(['expired_at' => SORT_DESC])
                    ->count() > 0

            ) {
                //var_dump($data['company_id']);die;
                $returnedCompaniesArr[] = $data['company_id'];
                $returnedCompanies++;
            }
        }

        if ($this->returnCompaniesIds) {
            return $returnedCompaniesArr;
        }

        return $returnedCompanies ?: 0;
    }

    /**
     * Кол-во не оплативших повторно клиентов
     * @param $tariffGroupId
     * @return false|string|array|null
     */
    public function getCountGonePayers($tariffGroupId)
    {
        $dateRange = $this->getDateRange();

        $subscribeIDs = [];
        $companiesIDs = [];

        $tariffsByGroups = SubscribeTariffGroup::getTariffsByGroups();

        $companiesWithEndSubscribe = $this->getSubscribeBaseQuery()
            ->select(new Expression('
                `service_subscribe`.`company_id`,
                MAX(`service_subscribe`.`expired_at`) as maxExpiredAt,
                `service_subscribe`.`id` as subscribeID,
                `service_subscribe`.`tariff_id` as tariffID
            '))
            ->andWhere(['`service_subscribe`.`tariff_id`' => self::$tariffByGroup[$tariffGroupId]])
            ->andWhere(['between', 'date(from_unixtime(`service_subscribe`.`expired_at`))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])
            ->orderBy(['`service_subscribe`.`expired_at`' => SORT_DESC])
            ->joinWith('payment', false)
            ->andWhere(['not', ['service_payment.type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->groupBy('`service_subscribe`.`company_id`')
            ->asArray()->all();

        foreach ($companiesWithEndSubscribe as $data) {
            if ($data['maxExpiredAt'] !== null) {
                $lastActivatedSubscribe = Subscribe::find()
                    ->andWhere(['not', [Subscribe::tableName() . '.id' => $data['subscribeID']]])
                    ->andWhere(['service_subscribe.tariff_id' => self::$tariffByGroup[$tariffGroupId]])
                    ->andWhere(['service_subscribe.status_id' => [SubscribeStatus::STATUS_PAYED, SubscribeStatus::STATUS_ACTIVATED]])
                    ->andWhere(['service_subscribe.company_id' => $data['company_id']])
                    ->andWhere(['OR',
                        ['>', 'date(from_unixtime(`service_subscribe`.`expired_at`))', $dateRange['to'] . ' 23:59:59'],
                        ['IS', 'expired_at', null],
                    ])
                    ->joinWith('payment', false)
                    ->andWhere(['not', ['service_payment.type_id' => PaymentType::TYPE_PROMO_CODE]])
                    ->orderBy(['service_subscribe.expired_at' => SORT_DESC])
                    ->one();

                if (!$lastActivatedSubscribe && isset($tariffsByGroups[$tariffGroupId]) && in_array($data['tariffID'], $tariffsByGroups[$tariffGroupId])) {
                    $subscribeIDs[] = $data['subscribeID'];
                    $companiesIDs[] = $data['company_id'];
                }
            }
        }

        $subscribeIDs = array_unique($subscribeIDs);
        $companiesIDs = array_unique($companiesIDs);

        if ($this->returnCompaniesIds) {
            return $companiesIDs;
        }

        return $subscribeIDs ? count($subscribeIDs): 0;
    }

    /**
     * Месячная выручка
     * @param $tariffGroupId
     * @param bool $onlyNew
     * @return false|string|null
     */
    public function getMRR($tariffGroupId)
    {
        $dateRange = $this->getDateRange();

        $query = $this->getPaymentBaseQuery()
            ->andWhere(['tariff_id' => self::$tariffByGroup[$tariffGroupId]])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Payment::tableName() . '.payment_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59']);

        return $query->select('SUM(`service_payment`.`sum`)')->scalar();
    }

    public function getNewMRR($tariffGroupId)
    {
        $dateRange = $this->getDateRange();

        return ReportState::find()
            ->joinWith('company')
            ->joinWith('company.registrationPageType')
            ->andWhere([Company::tableName() . '.test' => false])
            ->andWhere(['tariff_id' => self::$tariffByGroup[$tariffGroupId]])
            ->andWhere(['between', 'date(from_unixtime(' . ReportState::tableName() . '.pay_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])->sum('pay_sum');
    }

    /**
     * Кол-во всех пользователей, включая бесплатников и триал период, которые посещают приложение хотя бы 1 раз в месяц
     * @param $serviceModuleId
     * @return false|string|null
     */
    public function getMAU($serviceModuleId)
    {
        if (!isset(self::$moduleByTariffGroup[$serviceModuleId])) {
            return '';
        }

        $dateRange = $this->getDateRange();

        $query2 = ServiceModuleVisit::find()
            ->andWhere(['module_id' => self::$moduleByTariffGroup[$serviceModuleId]])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->groupBy('company_id')
            ->having('COUNT(`date`) > 0');

        return (new Query())->select('COUNT(*)')->from($query2)->scalar() ?: '';
    }

    /**
     * Среднее арифметическое пользователей за месяц
     * @param $serviceModuleId
     * @return false|string|null
     */
    public function getDAU($serviceModuleId)
    {
        if (!isset(self::$moduleByTariffGroup[$serviceModuleId])) {
            return '';
        }

        $dateRange = $this->getDateRange();

        $count = ServiceModuleVisit::find()
            ->andWhere(['module_id' => self::$moduleByTariffGroup[$serviceModuleId]])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])->count();

        $lastMonthDay = date('t', strtotime($dateRange['to']));

        return round($count / $lastMonthDay);
    }

    /**
     * Кол-во всех пользователей, включая бесплатников и триал период, которые посещают приложение хотя бы 1 раз в неделю
     * @param $serviceModuleId
     * @param bool $onlyNew
     * @return false|string|null
     */
    public function getUniqueVisitorsPerWeek($serviceModuleId)
    {
        if (!isset(self::$moduleByTariffGroup[$serviceModuleId])) {
            return '';
        }

        $dateRange = $this->getDateRange();

        $query2 = ServiceModuleVisit::find()
            ->andWhere(['module_id' => self::$moduleByTariffGroup[$serviceModuleId]])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->groupBy('company_id')
            ->having('COUNT(`date`) > 4');

        return (new Query())->select('COUNT(*)')->from($query2)->scalar() ?: '';
    }

    /**
     * Кол-во всех пользователей, включая бесплатников и триал период, которые посещают приложение хотя бы 1 раз в день
     * @param $serviceModuleId
     * @param bool $onlyNew
     * @return false|string|null
     */
    public function getUniqueVisitorsPerDay($serviceModuleId)
    {
        if (!isset(self::$moduleByTariffGroup[$serviceModuleId])) {
            return '';
        }

        $dateRange = $this->getDateRange();

        $query2 = ServiceModuleVisit::find()
            ->andWhere(['module_id' => self::$moduleByTariffGroup[$serviceModuleId]])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->groupBy('company_id')
            ->having('COUNT(`date`) > 22');

        return (new Query())->select('COUNT(*)')->from($query2)->scalar() ?: '';
    }

    /**
     * Месячная выручка
     * @param $tariffGroupId
     * @param bool $onlyNew
     * @return false|string|null
     */
    public function getMRRTotals($tariffGroupId, $paymentFor = '')
    {
        $dateRange = $this->getDateRange();

        $query = Payment::find()->alias('p')->joinWith('orders o', false)
            ->andWhere(['p.is_confirmed' => 1])
            ->andWhere(['not', ['p.type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME({{p}}.[[payment_date]]))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59']);

        if ($tariffGroupId) {
            $query->andWhere(['o.tariff_id' => self::$tariffByGroup[$tariffGroupId]]);
        }

        if ($paymentFor) {
            $query->andWhere(['p.payment_for' => $paymentFor]);
        }

        return $query->sum('o.sum');
    }

    private function getDateRange($offset = null)
    {
        if ($this->searchByYear) {
            return [
                'from' => date("{$this->year}-01-01"),
                'to' => ($this->year == date('Y')) ?
                    date("{$this->year}-m-d") : date("{$this->year}-12-31")
            ];
        }

        $date = new \DateTime();
        $date->setDate($this->year, $this->month, 1);

        if ($offset)
            $date->modify($offset);

        return [
            'from' => $date->format('Y-m-d'),
            'to' => ($this->year == date('Y') && $this->month == (int)date('m')) ?
                date('Y-m-d') : $date->format('Y-m-t')
        ];
    }

    /**
     * @param $type
     * @return array
     * @throws \Exception
     */
    public function getVisitsPeriod($type)
    {
        $breakLine = function($date) {
            return $date->format('Y') > date('Y') || $date->format('Ymd') > date('Ymd');
        };

        $date = new \DateTime();
        $date->setDate($this->year, 1, 1);
        $period = [];

        if ($type == self::DAY) {
            for ($i=0; $i<365; $i++) {

                if ($breakLine($date))
                    break;

                $period[] = [
                    'from' => $date->format('Y-m-d'),
                    'to' => $date->format('Y-m-d'),
                ];

                $date->modify("+1 day");
            }
        }
        if ($type == self::WEEK) {
            for ($i=0; $i<52; $i++) {

                if ($breakLine($date))
                    break;

                $period[] = [
                    'from' => $date->format('Y-m-d'),
                    'to' => $date->modify("+7 day")->modify("-1 day")->format('Y-m-d')
                ];

                $date->modify("+1 day");
            }
        }
        if ($type == self::MONTH) {
            for ($i=0; $i<12; $i++) {

                if ($breakLine($date))
                    break;

                $period[] = [
                    'from' => $date->format('Y-m-d'),
                    'to' => $date->modify("+1 month")->modify("-1 day")->format('Y-m-d')
                ];

                $date->modify("+1 day");
            }
        }

        return $period;
    }

    public function getVisitsX($type)
    {
        $period = $this->getVisitsPeriod($type);
        $xAxisTitle = [];
        foreach ($period as $onePeriod) {
            $from = date('d.m', strtotime($onePeriod['from']));
            $to = date('d.m', strtotime($onePeriod['to']));

            if (date('Ymd', strtotime($onePeriod['to'])) > date('Ymd'))
                $to = date('d.m');

            if ($type == self::DAY) {
                $xAxisTitle[] = $from;
            } else {
                $xAxisTitle[] =  $from;
            }
        }

        return $xAxisTitle;
    }

    public function getVisitsY($type, $tariffGroup = 1)
    {
        $period = $this->getVisitsPeriod($type);
        if (isset(self::$moduleByTariffGroup[$tariffGroup])) {
            $serviceModuleId = self::$moduleByTariffGroup[$tariffGroup];
        } else {
            $serviceModuleId = null;
        }

        $result = [];
        foreach ($period as $onePeriod) {
            if ($serviceModuleId) {
                $query = ServiceModuleVisit::find()
                    ->joinWith('company')
                    ->andWhere(['!=', Company::tableName() . '.test', Company::TEST_COMPANY])
                    ->andWhere(['module_id' => $serviceModuleId])
                    ->andWhere(['between', 'date',$onePeriod['from'], $onePeriod['to']]);

                $result[] = (int)$query->groupBy(Company::tableName() . '.id')->count();
            } else {
                $result[] = 0;
            }

        }

        return $result;
    }
}
