<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.09.2018
 * Time: 10:43
 */

namespace backend\modules\report\models;


use common\components\date\DateHelper;
use common\models\Company;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use frontend\components\StatisticPeriod;
use yii\data\ArrayDataProvider;

/**
 * Class RepeatStateSearch
 * @package backend\modules\report\models
 */
class RepeatStateSearch extends Company
{
    /**
     * @var int
     */
    public $pageSize = 10;

    /**
     * @var array
     */
    public $averageByPayment = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pageSize',], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        $data = [];
        $sortAttributes = [];
        $dateRange = StatisticPeriod::getSessionPeriod();
        $maxPayments = $this->getMaxPayments();
        $columns = [
            'company_id' => null,
            'company_name' => null,
        ];
        foreach (range(1, $maxPayments) as $paymentNumber) {
            $columns['payment' . $paymentNumber] = null;
            $columns['payment' . $paymentNumber . 'diff'] = null;
            $columns['payment' . $paymentNumber . 'amount'] = null;
            $sortAttributes['payment' . $paymentNumber] = [
                'asc' => ['payment' . $paymentNumber . 'diff' => SORT_ASC, 'payment' . $paymentNumber . 'amount' => SORT_ASC],
                'desc' => ['payment' . $paymentNumber . 'diff' => SORT_DESC, 'payment' . $paymentNumber . 'amount' => SORT_DESC],
                'label' => 'payment' . $paymentNumber,
            ];
            $this->averageByPayment['payment' . $paymentNumber]['diff'] = null;
            $this->averageByPayment['payment' . $paymentNumber]['count'] = null;
            $this->averageByPayment['payment' . $paymentNumber]['amount'] = null;
        }
        $columns['averagePayment'] = null;
        $this->averageByPayment['averagePayment']['diff'] = null;
        $this->averageByPayment['averagePayment']['count'] = null;
        $this->averageByPayment['averagePayment']['amount'] = null;
        $sortAttributes[] = 'averagePayment';
        $sortAttributes[] = 'registration_page_type';

        /* @var $companies \common\models\Company[] */
        $companies = Company::find()
            ->joinWith('subscribes')
            ->andWhere(['not', ['payment_type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
            ->andWhere(['not', ['status_id' => SubscribeStatus::STATUS_NEW]])
            ->andWhere(['in', 'tariff_id', SubscribeTariff::paidStandartIds()])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->all();

        $this->load($params);

        foreach ($companies as $company) {
            $data[$company->id] = $columns;
            $data[$company->id]['company_id'] = $company->id;
            $data[$company->id]['company_name'] = $company->getShortName();
            $data[$company->id]['registration_page_type'] = $company->registrationPageType ? $company->registrationPageType->name : null;

            /* @var $subscribe Subscribe */
            /* @var $previousSubscribe Subscribe */
            $companySubscribes = $company->getSubscribes()
                ->joinWith('payment')
                ->andWhere(['not', [Payment::tableName() . '.type_id' => PaymentType::TYPE_PROMO_CODE]])
                ->andWhere([Payment::tableName() . '.is_confirmed' => true])
                ->andWhere(['in', Payment::tableName() . '.tariff_id', SubscribeTariff::paidStandartIds()])
                ->orderBy([Payment::tableName() . '.payment_date' => SORT_ASC])
                ->all();
            $totalDiff = 0;
            $totalPayments = 0;
            foreach ($companySubscribes as $key => $subscribe) {
                $key++;
                $value = $subscribe->payment->sum;
                $data[$company->id]['payment' . $key . 'amount'] = $subscribe->payment->sum;
                $this->averageByPayment['payment' . $key]['amount'] += $subscribe->payment->sum;
                if ($key == 1) {
                    $dateFrom = new \DateTime(date(DateHelper::FORMAT_DATE, $subscribe->payment->payment_date));
                    $dateTo = new \DateTime(date(DateHelper::FORMAT_DATE, $company->created_at));
                    $daysDiff = date_diff($dateFrom, $dateTo)->days;
                    $daysDiff = $dateFrom >= $dateTo ? $daysDiff : ('-' . $daysDiff);
                    $value .= ' / ' . $daysDiff;
                    $data[$company->id]['payment' . $key . 'diff'] = $daysDiff;
                    $this->averageByPayment['payment' . $key]['diff'] += $daysDiff;
                    $this->averageByPayment['payment' . $key]['count'] += 1;
                } else {
                    $previousSubscribe = $companySubscribes[$key - 2];
                    $dateFrom = new \DateTime(date(DateHelper::FORMAT_DATE, $subscribe->payment->payment_date));
                    $dateTo = new \DateTime(date(DateHelper::FORMAT_DATE, $previousSubscribe->expired_at));
                    $daysDiff = date_diff($dateFrom, $dateTo)->days;
                    $daysDiff = $dateFrom >= $dateTo ? $daysDiff : ('-' . $daysDiff);
                    $value .= ' / ' . $daysDiff;
                    $totalDiff += $daysDiff;
                    $totalPayments += 1;
                    $data[$company->id]['payment' . $key . 'diff'] = $daysDiff;
                    $this->averageByPayment['payment' . $key]['diff'] += $daysDiff;
                    $this->averageByPayment['payment' . $key]['count'] += 1;
                }
                $data[$company->id]['payment' . $key] = $value;
            }
            $data[$company->id]['averagePayment'] = $totalPayments ? (int)round($totalDiff / $totalPayments) : null;
            $this->averageByPayment['averagePayment']['diff'] += $data[$company->id]['averagePayment'];
            $this->averageByPayment['averagePayment']['count'] += $totalPayments ? 1 : 0;
        }

        return new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
            'sort' => [
                'attributes' => $sortAttributes,
            ],
        ]);
    }

    /**
     * @return mixed
     */
    public function getMaxPayments()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Subscribe::find()
            ->select('COUNT(' . Subscribe::tableName() . '.id) as paymentsCount')
            ->joinWith('company')
            ->joinWith('payment')
            ->andWhere(['not', [Payment::tableName() . '.type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->andWhere([Payment::tableName() . '.is_confirmed' => true])
            ->andWhere(['not', [Subscribe::tableName() . '.status_id' => SubscribeStatus::STATUS_NEW]])
            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
            ->andWhere(['in', Payment::tableName() . '.tariff_id', SubscribeTariff::paidStandartIds()])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->groupBy(Subscribe::tableName() . '.company_id')
            ->max('paymentsCount');

    }
}