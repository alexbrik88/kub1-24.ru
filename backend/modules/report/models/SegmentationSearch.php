<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.05.2017
 * Time: 5:02
 */

namespace backend\modules\report\models;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\out\OutInvoice;
use common\models\product\Product;
use common\models\product\ProductStore;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\store\StoreCompanyContractor;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use Yii;

/**
 * Class SegmentationSearch
 * @package frontend\modules\reports\models
 */
class SegmentationSearch extends Company
{
    const TAB_ACTIVITIES = 1;
    const TAB_WHY = 2;
    const TAB_REGISTRATION_PAGE = 3;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['activities_id'], 'integer'],
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getActivities()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->select(['activities_id, COUNT(' . Company::tableName() . '.id) as count_activities_companies,

            SUM((SELECT COUNT(*) FROM ' . Invoice::tableName() . '
            WHERE ' . Invoice::tableName() . '.company_id = ' . Company::tableName() . '.id
            AND ' . Invoice::tableName() . '.type = ' . Documents::IO_TYPE_OUT . '
            AND ' . Invoice::tableName() . '.is_deleted = ' . Invoice::NOT_IS_DELETED . '
            AND (NOT (' . Invoice::tableName() . '.invoice_status_id = ' . InvoiceStatus::STATUS_AUTOINVOICE . ')
            OR (' . Invoice::tableName() . '.invoice_status_id IS NULL)))) AS invoice_count,

            SUM((SELECT COUNT(*) FROM ' . Subscribe::tableName() . '
            WHERE ' . Subscribe::tableName() . '.company_id = ' . Company::tableName() . '.id
            AND NOT (' . Subscribe::tableName() . '.tariff_id = ' . SubscribeTariff::TARIFF_TRIAL . ')
            AND ' . Subscribe::tableName() . '.status_id IN  (' . SubscribeStatus::STATUS_PAYED . ',' . SubscribeStatus::STATUS_ACTIVATED . ')
            AND NOT (' . Subscribe::tableName() . '.payment_type_id = ' . PaymentType::TYPE_PROMO_CODE . '))) as subscribe_count'])
            ->joinWith('activities')
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->andWhere(['in', 'activation_type', [Company::ACTIVATION_NOT_SEND_INVOICES, Company::ACTIVATION_ALL_COMPLETE]])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->andWhere(['not', ['activities_id' => null]])
            ->groupBy(['activities.id'])
            ->orderBy(['count_activities_companies' => SORT_DESC])
            ->asArray()
            ->all();
    }

    /**
     * @return int|string
     */
    public function getCompaniesCountWithActivities()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->andWhere(['in', 'activation_type', [Company::ACTIVATION_NOT_SEND_INVOICES, Company::ACTIVATION_ALL_COMPLETE]])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->andWhere(['not', ['activities_id' => null]])
            ->count();
    }

    /**
     * @return int|string
     */
    public function getInvoiceCountWithActivities()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->andWhere(['in', 'activation_type', [Company::ACTIVATION_NOT_SEND_INVOICES, Company::ACTIVATION_ALL_COMPLETE]])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->andWhere(['not', ['activities_id' => null]])
            ->joinWith('invoices')
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
            ->count();
    }

    /**
     * @return int|string
     */
    public function getSubscribeCountWithActivities()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->andWhere(['in', 'activation_type', [Company::ACTIVATION_NOT_SEND_INVOICES, Company::ACTIVATION_ALL_COMPLETE]])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->andWhere(['not', ['activities_id' => null]])
            ->joinWith('subscribes')
            ->andWhere(['not', [Subscribe::tableName() . '.tariff_id' => SubscribeTariff::TARIFF_TRIAL]])
            ->andWhere(['in', Subscribe::tableName() . '.status_id', [SubscribeStatus::STATUS_PAYED, SubscribeStatus::STATUS_ACTIVATED]])
            ->andWhere(['not', [Subscribe::tableName() . '.payment_type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->count();
    }

    /**
     * @return int|string
     */
    public function getPayedCompaniesCountWithActivities()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->andWhere(['in', 'activation_type', [Company::ACTIVATION_NOT_SEND_INVOICES, Company::ACTIVATION_ALL_COMPLETE]])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->andWhere(['not', ['activities_id' => null]])
            ->joinWith('subscribes')
            ->andWhere(['not', [Subscribe::tableName() . '.tariff_id' => SubscribeTariff::TARIFF_TRIAL]])
            ->andWhere(['in', Subscribe::tableName() . '.status_id', [SubscribeStatus::STATUS_PAYED, SubscribeStatus::STATUS_ACTIVATED]])
            ->andWhere(['not', [Subscribe::tableName() . '.payment_type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->groupBy(Company::tableName() . '.id')
            ->count();
    }

    /**
     * @param $activitiesID
     * @return int|string
     */
    public function getPayedCompaniesCountByActivities($activitiesID)
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->andWhere(['in', 'activation_type', [Company::ACTIVATION_NOT_SEND_INVOICES, Company::ACTIVATION_ALL_COMPLETE]])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->andWhere(['not', ['activities_id' => null]])
            ->andWhere(['activities_id' => $activitiesID])
            ->joinWith('subscribes')
            ->andWhere(['not', [Subscribe::tableName() . '.tariff_id' => SubscribeTariff::TARIFF_TRIAL]])
            ->andWhere(['in', Subscribe::tableName() . '.status_id', [SubscribeStatus::STATUS_PAYED, SubscribeStatus::STATUS_ACTIVATED]])
            ->andWhere(['not', [Subscribe::tableName() . '.payment_type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->groupBy(Company::tableName() . '.id')
            ->count();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        $query = $this->getBaseQuery($dateRange);

        $query->leftJoin(['inv' => Invoice::find()
            ->select([
                'id', 'company_id',
            ])
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted(Invoice::NOT_IS_DELETED)
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
        ], 'inv.company_id = ' . Company::tableName() . '.id');

        $query->leftJoin(['act' => Act::find()
            ->select(['id', 'invoice_id'])
            ->byIOType(Documents::IO_TYPE_OUT)
        ], 'act.invoice_id = inv.id');

        $query->leftJoin(['packingList' => PackingList::find()
            ->select(['id', 'invoice_id'])
            ->byIOType(Documents::IO_TYPE_OUT)
        ], 'packingList.invoice_id = inv.id');

        $query->leftJoin(['invoiceFacture' => InvoiceFacture::find()
            ->select(['id', 'invoice_id'])
            ->byIOType(Documents::IO_TYPE_OUT)
        ], 'invoiceFacture.invoice_id = inv.id');

        $query->leftJoin(['product' => Product::find()
            ->select([
                Product::tableName() . '.company_id',
                'COUNT(*) as product_count',
            ])
            ->byProductionType(Product::PRODUCTION_TYPE_GOODS)
            ->byDeleted()
            ->groupBy('company_id')
        ], 'product.company_id = ' . Company::tableName() . '.id');

        $query->leftJoin(['employee_company' => EmployeeCompany::find()
            ->select([
                EmployeeCompany::tableName() . '.company_id',
                'COUNT(*) as employee_count',
            ])
            ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
            ->groupBy(EmployeeCompany::tableName() . '.company_id')
        ], 'employee_company.company_id = ' . Company::tableName() . '.id');

        $query->leftJoin(['contractor' => Contractor::find()
            ->select(['id', 'company_id'])
            ->byContractor(Contractor::TYPE_CUSTOMER)
            ->byDeleted()
            ->byStatus(Contractor::ACTIVE)
            ->joinWith('storeCompanyContractors')
            ->andWhere(['not', [StoreCompanyContractor::tableName() . '.store_company_id' => null]])
        ], Contractor::tableName() . '.company_id = ' . Company::tableName() . '.id');

        $query->leftJoin(['store' => StoreCompanyContractor::find()
            ->select(['contractor_id', 'store_company_id'])
            ->andWhere([StoreCompanyContractor::tableName() . '.status' => StoreCompanyContractor::STATUS_ACTIVE])
        ], 'store.contractor_id = ' . Contractor::tableName() . '.id');

        $query->leftJoin(['storeInv' => Invoice::find()
            ->select([
                'id',
                'company_id',
            ])
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted(Invoice::NOT_IS_DELETED)
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
            ->andWhere(['from_store_cabinet' => true])
        ], 'storeInv.company_id = ' . Company::tableName() . '.id');

        $query->leftJoin(['out_invoice' => OutInvoice::find()
            ->andWhere(['status' => OutInvoice::ACTIVE])
        ], 'out_invoice.company_id = ' . Company::tableName() . '.id');

        $query->leftJoin(['outInv' => Invoice::find()
            ->select([
                'id',
                'company_id',
            ])
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted(Invoice::NOT_IS_DELETED)
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
            ->andWhere(['from_out_invoice' => true])
        ], 'outInv.company_id = ' . Company::tableName() . '.id');

        $query->addSelect(['company.*,
            COUNT(distinct(inv.id)) as invoice_count,
            COUNT(distinct(act.id)) as act_count,
            COUNT(distinct(packingList.id)) as packing_list_count,
            COUNT(distinct(invoiceFacture.id)) as invoice_facture_count,
            product.product_count as product_count,
            employee_company.employee_count as employee_count,
            CONCAT(COUNT(distinct(store.store_company_id)), " (", COUNT(distinct(storeInv.id)), ")") as store_count,
            CONCAT(COUNT(distinct(out_invoice.id)), " (", COUNT(distinct(outInv.id)), ")") as out_invoice_count,

           (SELECT COUNT(*) FROM ' . Subscribe::tableName() . '
            WHERE ' . Subscribe::tableName() . '.company_id = company.id
            AND NOT (' . Subscribe::tableName() . '.tariff_id = ' . SubscribeTariff::TARIFF_TRIAL . ')
            AND ' . Subscribe::tableName() . '.status_id IN  (' . SubscribeStatus::STATUS_PAYED . ',' . SubscribeStatus::STATUS_ACTIVATED . ')
            AND NOT (' . Subscribe::tableName() . '.payment_type_id = ' . PaymentType::TYPE_PROMO_CODE . ')) as subscribe_count
            ']);

        $query->groupBy('company.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'name_short',
                    'created_at',
                    'email',
                    'invoice_count',
                    'act_count',
                    'packing_list_count',
                    'invoice_facture_count',
                    'product_count',
                    'employee_count',
                    'store_count',
                    'out_invoice_count',
                    'subscribe_count',
                ],
            ],
        ]);

        $this->load($params);

        if ($this->activities_id == -1) {
            $query->andWhere(['activities_id' => null]);
        } else {
            $query->andFilterWhere(['activities_id' => $this->activities_id]);
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getActivitiesFilter()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return ArrayHelper::merge([null => 'Все', -1 => '---'], ArrayHelper::map($this->getBaseQuery($dateRange)
            ->joinWith('activities')
            ->andWhere(['not', ['activities_id' => null]])
            ->groupBy('activities_id')
            ->orderBy('name')
            ->all(), 'activities.id', 'activities.name'));
    }

    /**
     * @param $dateRange
     * @return ActiveQuery
     */
    public function getBaseQuery($dateRange)
    {
        return Company::find()
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->andWhere(['in', 'activation_type', [Company::ACTIVATION_NOT_SEND_INVOICES, Company::ACTIVATION_ALL_COMPLETE]])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']]);
    }

    /**
     * @param $registrationPageType
     * @return int|string
     */
    public static function getCompaniesByRegistrationPage($registrationPageType)
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->andWhere(['registration_page_type_id' => $registrationPageType])
            ->count();
    }

    /**
     * @param $registrationPageType
     * @return int|string
     */
    public static function getFirstPayByRegistrationPage($registrationPageType)
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->select([
                'company.id',
                'pay_at' => 'MIN({{payment}}.[[payment_date]])',
            ])
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->andWhere(['registration_page_type_id' => $registrationPageType])
            ->joinWith(['subscribes subscribe', 'subscribes.payment payment'], false)
            ->andWhere([
                'payment.is_confirmed' => true,
                'subscribe.tariff_id' => SubscribeTariff::paidStandartIds(),
            ])
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->having(['between', 'date(from_unixtime(pay_at))', $dateRange['from'], $dateRange['to']])
            ->groupBy('company.id')
            ->indexBy('id')
            ->count();
    }
}