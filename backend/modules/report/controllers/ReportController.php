<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2017
 * Time: 5:15
 */

namespace backend\modules\report\controllers;


use backend\components\BackendController;
use backend\modules\report\models\AttendanceSearch;
use backend\modules\report\models\EmailSummarySearch;
use backend\modules\report\models\LeaveCompaniesSearch;
use backend\modules\report\models\RepeatStateSearch;
use backend\modules\report\models\ReportSearch;
use backend\modules\report\models\SegmentationSearch;
use backend\modules\report\models\StateSearch;
use backend\modules\report\models\UrotdelVisitsSearch;
use common\components\date\DateHelper;
use common\models\Company;
use common\models\report\ReportState;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use frontend\components\PageSize;
use common\components\excel\Excel;
use frontend\components\StatisticPeriod;
use frontend\components\XlsHelper;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use himiklab\thumbnail\FileNotFoundException;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class ReportController
 * @package backend\modules\report\controllers
 */
class ReportController extends BackendController
{
    public function beforeAction($action)
    {
        \Yii::$app->db->createCommand('SET session wait_timeout=600')->execute();
        \Yii::$app->db->createCommand('SET session interactive_timeout=600')->execute();

        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionRegistration()
    {
        $searchModel = new ReportSearch();
        $searchModel->load(\Yii::$app->request->get());
        $period = $searchModel->getPeriod();
        $type = $period['type'];
        unset($period['type']);

        return $this->render('registration', [
            'searchModel' => $searchModel,
            'period' => $period,
            'type' => $type,
        ]);
    }

    /**
     * @return string
     */
    public function actionChannel()
    {
        $searchModel = new ReportSearch();
        $searchModel->load(\Yii::$app->request->get());
        $period = $searchModel->getPeriod();
        $type = $period['type'];
        unset($period['type']);

        return $this->render('channel', [
            'searchModel' => $searchModel,
            'period' => $period,
            'type' => $type,
        ]);
    }

    /**
     * @return string
     */
    public function actionActivity()
    {
        $searchModel = new ReportSearch();
        $searchModel->load(\Yii::$app->request->get());
        $period = $searchModel->getPeriod();
        $type = $period['type'];
        unset($period['type']);

        return $this->render('activity', [
            'searchModel' => $searchModel,
            'period' => $period,
            'type' => $type,
        ]);
    }

    /**
     * @return string
     */
    public function actionAttendance()
    {
        $searchModel = new AttendanceSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('attendance', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionActivation()
    {
        $searchModel = new ReportSearch();
        $searchModel->load(\Yii::$app->request->get());
        $period = $searchModel->getPeriod();
        $type = $period['type'];
        unset($period['type']);

        return $this->render('activation', [
            'searchModel' => $searchModel,
            'period' => $period,
            'type' => $type,
        ]);
    }

    /**
     * @return string
     */
    public function actionFirstTime()
    {
        $searchModel = new ReportSearch();
        $searchModel->load(\Yii::$app->request->get());
        $period = $searchModel->getPeriod();
        $type = $period['type'];
        unset($period['type']);

        return $this->render('first-time', [
            'searchModel' => $searchModel,
            'period' => $period,
            'type' => $type,
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     */
    public function actionSegmentation($activeTab = SegmentationSearch::TAB_ACTIVITIES)
    {
        if ($activeTab == SegmentationSearch::TAB_ACTIVITIES) {
            $searchModel = new SegmentationSearch();
            $involvedActivities = $searchModel->getActivities();
            $companiesCountWithActivities = $searchModel->getCompaniesCountWithActivities();
            $invoicesCountWithActivities = $searchModel->getInvoiceCountWithActivities();
            $subscribeCountWithActivities = $searchModel->getSubscribeCountWithActivities();
            $payedCompaniesCountWithActivities = $searchModel->getPayedCompaniesCountWithActivities();
            $dataProvider = $searchModel->search(\Yii::$app->request->get());
            $dataProvider->pagination->pageSize = PageSize::get();
        } elseif ($activeTab == SegmentationSearch::TAB_WHY) {
            $afterRegistrationData = Company::getAfterRegistrationData();
        }

        return $this->render('segmentation', [
            'involvedActivities' => isset($involvedActivities) ? $involvedActivities : null,
            'companiesCountWithActivities' => isset($companiesCountWithActivities) ? $companiesCountWithActivities : null,
            'invoicesCountWithActivities' => isset($invoicesCountWithActivities) ? $invoicesCountWithActivities : null,
            'subscribeCountWithActivities' => isset($subscribeCountWithActivities) ? $subscribeCountWithActivities : null,
            'payedCompaniesCountWithActivities' => isset($payedCompaniesCountWithActivities) ? $payedCompaniesCountWithActivities : null,
            'searchModel' => isset($searchModel) ? $searchModel : null,
            'dataProvider' => isset($dataProvider) ? $dataProvider : null,
            'comID' => \Yii::$app->request->get('comID'),
            'afterRegistrationData' => isset($afterRegistrationData) ? $afterRegistrationData : null,
            'activeTab' => $activeTab,
        ]);
    }

    /**
     * @return string
     */
    public function actionState()
    {
        $searchModel = new StateSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('state', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionRepeatState()
    {
        $searchModel = new RepeatStateSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('repeat-state', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionEmailSummary()
    {
        $searchModel = new EmailSummarySearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('email-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionLeaveCompanies()
    {
        $searchModel = new LeaveCompaniesSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('leave-companies', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array|bool
     * @throws NotFoundHttpException
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionUploadEmailSummary()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $file = UploadedFile::getInstanceByName('uploadfile');
        if ($file !== null) {
            $xlsHelper = new XlsHelper($file);
            //   $xlsHelper->saveFile();
            $result = $xlsHelper->readForEmailSummary();

            return [
                'message' => 'Успешно загружено ' . $result['successCount'] . ' email-ов <br>
                    Не удалось найти ' . $result['notFoundCount'] . ' email-ов (' . $result['notFoundEmails'] . ') <br>
                    Загружено с ошибкой ' . $result['errorCount'] . ' email-ов',
                'successProducts' => $result['success'],
            ];
        }


        return false;
    }

    /**
     * @throws FileNotFoundException
     */
    public function actionDownloadTemplate()
    {
        $fileName = 'EmailSvodka.xlsx';
        $path = \Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'xls' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . $fileName;
        if (file_exists($path)) {
            return \Yii::$app->response->sendFile($path, $fileName, ['mimeType' => mime_content_type($path)])->send();
        } else {
            throw new FileNotFoundException('File not found');
        }
    }

    /**
     *
     */
    public function actionStateReportExcel()
    {
        $searchModel = new StateSearch();
        $dateRange = StatisticPeriod::getSessionPeriod();

        $excel = new Excel();

        $fileName = 'Первые_оплаты_' . DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $columns[] = [
            'attribute' => 'company_id',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->company_id;
            }
        ];
        $columns[] = [
            'attribute' => 'company.name_short',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->company->getShortName();
            }
        ];
        $columns[] = [
            'attribute' => 'company_type_id',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->company->companyType->name_short;
            }
        ];
        $columns[] = [
            'attribute' => 'company_taxation_type_name',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->company_taxation_type_name;
            }
        ];
        $columns[] = [
            'attribute' => 'company.activities_id',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->company->activities ? $model->company->activities->name : '';
            }
        ];
        $columns[] = [
            'attribute' => 'company.created_at',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return date(DateHelper::FORMAT_USER_DATE, $model->company->created_at);
            }
        ];
        $columns[] = [
            'attribute' => 'pay_date',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return date(DateHelper::FORMAT_USER_DATE, $model->pay_date);
            }
        ];
        $columns[] = [
            'attribute' => 'days_without_payment',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->days_without_payment;
            }
        ];
        $columns[] = [
            'attribute' => 'pay_sum',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->pay_sum;
            }
        ];

        foreach (FlowOfFundsReportSearch::$month as $monthNumber => $month) {
            $columns[] = [
                'attribute' => 'month_' . $monthNumber,
                'format' => 'raw',
                'value' => function (ReportState $model) use ($monthNumber) {
                    $sum = $model->company->getPayments()
                        ->andWhere(['in', 'tariff_id', SubscribeTariff::paidStandartIds()])
                        ->andWhere(['is_confirmed' => true])
                        ->andWhere(['not', ['type_id' => PaymentType::TYPE_PROMO_CODE]])
                        ->andWhere(['between', 'payment_date',
                            strtotime('01.' . $monthNumber . '.' . date('Y')),
                            strtotime(cal_days_in_month(CAL_GREGORIAN, $monthNumber, date('Y')) . '.' . $monthNumber . '.' . date('Y'))])
                        ->sum('sum');

                    return $sum ? $sum : 0;
                }
            ];
        }

        $columns[] = [
            'attribute' => 'invoice_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->invoice_count;
            }
        ];
        $columns[] = [
            'attribute' => 'act_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->act_count;
            }
        ];
        $columns[] = [
            'attribute' => 'packing_list_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->packing_list_count;
            }
        ];
        $columns[] = [
            'attribute' => 'invoice_facture_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->invoice_facture_count;
            }
        ];
        $columns[] = [
            'attribute' => 'upd_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->upd_count;
            }
        ];
        $columns[] = [
            'attribute' => 'has_logo',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->has_logo;
            }
        ];
        $columns[] = [
            'attribute' => 'has_print',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->has_print;
            }
        ];
        $columns[] = [
            'attribute' => 'has_signature',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->has_signature;
            }
        ];
        $columns[] = [
            'attribute' => 'sum_company_images',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->sum_company_images;
            }
        ];
        $columns[] = [
            'attribute' => 'product_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->product_count;
            }
        ];
        $columns[] = [
            'attribute' => 'service_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->service_count;
            }
        ];
        $columns[] = [
            'attribute' => 'employees_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->employees_count;
            }
        ];
        $columns[] = [
            'attribute' => 'download_statement_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->download_statement_count;
            }
        ];
        $columns[] = [
            'attribute' => 'download_1c_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->download_1c_count;
            }
        ];
        $columns[] = [
            'attribute' => 'import_xls_product_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->import_xls_product_count;
            }
        ];
        $columns[] = [
            'attribute' => 'import_xls_service_count',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->import_xls_service_count;
            }
        ];
        $columns[] = [
            'attribute' => 'company.form_source',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->company->form_source ? $model->company->form_source : '';
            }
        ];
        $columns[] = [
            'attribute' => 'company.form_keyword',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->company->form_keyword ? $model->company->form_keyword : '';
            }
        ];
        $columns[] = [
            'attribute' => 'company.form_region',
            'format' => 'raw',
            'value' => function (ReportState $model) {
                return $model->company->form_region ? $model->company->form_region : '';
            }
        ];

        $headers['company_id'] = 'id';
        $headers['company.name_short'] = 'Название';
        $headers['company_type_id'] = 'Тип';
        $headers['company_taxation_type_name'] = 'СНО';
        $headers['company.activities_id'] = 'Вид деятельности';
        $headers['company.created_at'] = 'Дата регистрации';
        $headers['pay_date'] = 'Дата оплаты';
        $headers['pay_sum'] = 'Сумма оплаты';

        foreach (FlowOfFundsReportSearch::$month as $monthNumber => $month) {
            $headers['month_' . $monthNumber] = $month;
        }

        $headers['days_without_payment'] = 'Кол-во дней';
        $headers['sum'] = 'Сумма оплаты';
        $headers['invoice_count'] = 'Счет';
        $headers['act_count'] = 'Акт';
        $headers['packing_list_count'] = 'ТН';
        $headers['invoice_facture_count'] = 'СФ';
        $headers['upd_count'] = 'УПД';
        $headers['has_logo'] = 'Логотип';
        $headers['has_print'] = 'Печать';
        $headers['has_signature'] = 'Подпись';
        $headers['sum_company_images'] = 'Итого';
        $headers['product_count'] = 'Товар';
        $headers['service_count'] = 'Услуги';
        $headers['employees_count'] = 'Кол-во сотрудников';
        $headers['download_statement_count'] = 'Загрузка выписки';
        $headers['download_1c_count'] = 'Выгрузка 1С';
        $headers['import_xls_product_count'] = 'Загружено товаров из Excel';
        $headers['import_xls_service_count'] = 'Загружено услуг из Excel';
        $headers['company.form_source'] = 'Источник';
        $headers['company.form_keyword'] = 'Ключевое слово';
        $headers['company.form_region'] = 'Регион';

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $searchModel->searchModelsForExcel(\Yii::$app->request->get()),
            'title' => 'Акты',
            'rangeHeader' => array_merge(range('A', 'Z'), ['AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH',
                'AI', 'AJ', 'AK', 'AL', 'AM', 'AN']),
            'columns' => $columns,
            'headers' => $headers,
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);

    }

    /**
     * @return string
     */
    public function actionCohorts()
    {
        $searchModel = new ReportSearch();
        $searchModel->load(\Yii::$app->request->get());
        if (!isset(\Yii::$app->request->get('ReportSearch')['periodType']) && !isset(\Yii::$app->request->get('ReportSearch')['periodDayList'])) {
            $searchModel->periodType = ReportSearch::WEEK;
        }
        $period = $searchModel->getPeriod();
        $type = $period['type'];
        unset($period['type']);

        $xAxisCategories = $searchModel->getXAxisTitle($period, $type);

        $registrations = $searchModel->getAllRegistrations($period, $type);
        $completeProfile = $searchModel->getAllCompleteProfileCohorts($period, $type);
        $exhibitedInvoices = $searchModel->getExhibitedInvoicesCohorts($period, $type);
        $freeTariff = $searchModel->getFreeTariffCohorts($period, $type);
        $firstPayed = $searchModel->getFirstPayedCohorts($period, $type);
        $firstPayedSum = $searchModel->getFirstPayedCohortsSum($period, $type);
        $exhibitedInvoicesOne = $searchModel->getExhibitedInvoicesCohortsByCount($period, $type, 0, 1);
        $exhibitedInvoicesTwo = $searchModel->getExhibitedInvoicesCohortsByCount($period, $type, 1, 5);
        $exhibitedInvoicesFive = $searchModel->getExhibitedInvoicesCohortsByCount($period, $type, 5, 10);
        $exhibitedInvoicesTen = $searchModel->getExhibitedInvoicesCohortsByCount($period, $type, 10);
        $withLogo = $searchModel->getWithLogoCohorts($period, $type);

        return $this->render('cohorts', [
            'searchModel' => $searchModel,
            'registrations' => $registrations,
            'completeProfile' => $completeProfile,
            'exhibitedInvoices' => $exhibitedInvoices,
            'freeTariff' => $freeTariff,
            'firstPayed' => $firstPayed,
            'xAxisCategories' => $xAxisCategories,
            'firstPayedSum' => $firstPayedSum,
            'exhibitedInvoicesOne' => $exhibitedInvoicesOne,
            'exhibitedInvoicesTwo' => $exhibitedInvoicesTwo,
            'exhibitedInvoicesFive' => $exhibitedInvoicesFive,
            'exhibitedInvoicesTen' => $exhibitedInvoicesTen,
            'withLogo' => $withLogo,
        ]);
    }

    /**
     * @return string
     */
    public function actionRobot()
    {
        // TEST
        if (\Yii::$app->request->get('check_datetime')) {
            var_dump((new Query())->select(['now()'])->all());
            var_dump(date('Y-m-d H:i:s', time()));
        }

        $this->layout = 'robot';

        $searchModel = new ReportSearch();
        $searchModel->load(\Yii::$app->request->get());
        $period = $searchModel->getPeriod();
        $type = $period['type'];
        unset($period['type']);

        return $this->render('robot', [
            'searchModel' => $searchModel,
            'period' => $period,
            'type' => $type,
        ]);
    }

    /**
     * @return string
     */
    public function actionRobotDownload()
    {
        $this->layout = 'robot';

        $searchModel = new ReportSearch();
        $searchModel->load(\Yii::$app->request->get());
        $period = $searchModel->getPeriod();
        $type = $period['type'];
        unset($period['type']);

        return $this->render('robot-download', [
            'searchModel' => $searchModel,
            'period' => $period,
            'type' => $type,
        ]);
    }

    public function actionUrotdel(): string {
        $this->layout = 'main';
        $searchModel = new UrotdelVisitsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('urotdel', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}