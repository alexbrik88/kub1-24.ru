<?php
namespace backend\modules\report\controllers;

use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use Yii;
use backend\components\BackendController;
use backend\modules\company\models\CompanyAdditionalSearch;
use backend\modules\report\models\CoefficientSearch;
use backend\modules\report\models\CoefficientCompanySearch;
use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\Month;
use common\models\Company;
use common\models\service\SubscribeTariffGroup;
use frontend\components\StatisticPeriod;
use yii\web\NotFoundHttpException;
use backend\modules\report\models\CoefficientCompanyHelper;

/**
 * Class CoefficientController
 * @package backend\modules\report\controllers
 */
class CoefficientController extends BackendController
{

    /**
     * @param $year
     * @param $month
     * @param $tariffGroup
     * @param $paramName
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFilterCompanies($year, $month, $tariffGroup, $paramName)
    {
        $this->layout = 'coefficient';

        $groupModel = SubscribeTariffGroup::findOne($tariffGroup);
        if (!$groupModel) {
            throw new NotFoundHttpException('Параметр $tariffGroup не определен');
        } elseif ($month < 1 || $month > 12) {
            throw new NotFoundHttpException('Параметр $month не определен');
        } elseif ($year < 1 || $year > 3000) {
            throw new NotFoundHttpException('Параметр $year не определен');
        } elseif (!in_array($paramName, ['countPayers', 'countNewPayers', 'countReturnedPayers', 'countGonePayers'])) {
            throw new NotFoundHttpException('Параметр $paramName не определен');
        }

        $coefficientModel = new CoefficientSearch();
        $coefficientModel->init();
        $coefficientModel->year = $year;
        $coefficientModel->month = $month;
        $coefficientModel->returnCompaniesIds = true;

        $monthName = mb_strtolower(Month::$monthGenitiveRU[str_pad($month, 2, "0", STR_PAD_LEFT)]);

        switch ($paramName) {
            case 'countPayers':
                $companiesIds = $coefficientModel->getCountPayers($tariffGroup);
                $companiesTitle = 'Кол-во платящих на конец ' . $monthName . " {$year} года.";
                break;
            case 'countNewPayers':
                $companiesIds = $coefficientModel->getCountNewPayers($tariffGroup);
                $companiesTitle = 'Кол-во новых на конец ' . $monthName . " {$year} года.";
                break;
            case 'countReturnedPayers':
                $companiesIds = $coefficientModel->getCountReturnedPayers($tariffGroup);
                $companiesTitle = 'Кол-во вернувшихся на конец ' . $monthName . " {$year} года.";
                break;
            case 'countGonePayers':
                $companiesIds = $coefficientModel->getCountGonePayers($tariffGroup);
                $companiesTitle = 'Кол-во не оплативших повторно на конец ' . $monthName . " {$year} года.";
                break;
            default:
                $companiesIds = [0];
                $companiesTitle = '';
                break;
        }

        $companiesTitle .= " Тариф " . $groupModel->name . '. ';

        // for excel export
        \Yii::$app->session->set('CoefficientController.Companies.Ids', $companiesIds);
        \Yii::$app->session->set('CoefficientController.Companies.Title', $companiesTitle);
        \Yii::$app->session->set('CoefficientController.Companies.TariffGroup', $tariffGroup);

        $searchModel = new CoefficientCompanySearch();
        $searchModel->filterByIds = $companiesIds;
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('filter-companies', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $companiesTitle
        ]);
    }

    /**
     *
     */
    public function actionFilterCompaniesExcel()
    {
        $searchModel = new CoefficientCompanySearch();
        $searchModel->filterByIds = Yii::$app->session->get('CoefficientController.Companies.Ids');

        $fileName = str_replace(' ', '_', \Yii::$app->session->get('CoefficientController.Companies.Title'));
        $models = $searchModel->searchModelsForExcel(\Yii::$app->request->get());
        $tariffGroup = Yii::$app->session->get('CoefficientController.Companies.TariffGroup');
        $tariffIds = SubscribeTariff::find()->where(['tariff_group_id' => $tariffGroup])->andWhere(['!=', 'id', SubscribeTariff::TARIFF_TRIAL])->select('id')->column();

        // Excel params
        $alignCenter = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER;
        $fillSolid = \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID;
        $fillColors = [
            'blue' => "#b8cce4",
            'red' => "#da9694",
            'yellow' => "#fcd5b4",
            'pink' => "#f2dcdb"
        ];

        $xls = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $sheet->setTitle('Компании');

        $data = [
            1 => ['header' => 'id', 'bg' => 'B8CCE4'],
            2 => ['header' => 'Дата регистрации', 'bg' => 'B8CCE4'],
            3 => ['header' => 'Дата активации', 'bg' => 'B8CCE4'],
            4 => ['header' => 'Дата окончания', 'bg' => 'B8CCE4'],
            5 => ['header' => 'Название компании', 'bg' => 'B8CCE4'],
            6 => ['header' => 'Телефон', 'bg' => 'B8CCE4'],
            7 => ['header' => 'Руководитель', 'bg' => 'B8CCE4'],
            8 => ['header' => 'Банк (основной)', 'bg' => 'B8CCE4'],
            9 => ['header' => 'ИНН', 'bg' => 'B8CCE4'],
            10 => ['header' => 'Юр. адрес', 'bg' => 'B8CCE4'],
            11 => ['header' => 'E-mail', 'bg' => 'B8CCE4'],
            12 => ['header' => 'Система налогообложения', 'bg' => 'B8CCE4'],
            13 => ['header' => '2018 Оплата', 'bg' => 'FCD5B4', 'columns' => 7,  'year' => 2018, 'month' => [6,7,8,9,10,11,12], 'method' => 'getPaymentsSum'],
            14 => ['header' => '2018 Счета', 'bg' => 'F2DCDB', 'columns' => 7,  'year' => 2018, 'month' => [6,7,8,9,10,11,12], 'method' => 'getInvoicesCount'],
            15 => ['header' => '2019 Оплата', 'bg' => 'FCD5B4', 'columns' => 12, 'year' => 2019, 'month' => [1,2,3,4,5,6,7,8,9,10,11,12], 'method' => 'getPaymentsSum'],
            16 => ['header' => '2019 Счета', 'bg' => 'F2DCDB',  'columns' => 12, 'year' => 2019, 'month' => [1,2,3,4,5,6,7,8,9,10,11,12], 'method' => 'getInvoicesCount'],
            17 => ['header' => 'Итого оплат', 'bg' => 'FCD5B4'],
        ];

        $sheet->getStyle("A1:AY2")->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );

        $num = 1;
        foreach ($data as $value) {

            if (isset($value['columns'])) {
                $row = 1;
                $col = CoefficientCompanyHelper::getExcelColumn($num);
                $subNum = $num;
                $num += $value['columns'] - 1;
                $rightCol = CoefficientCompanyHelper::getExcelColumn($num);
                $sheet->mergeCells("{$col}{$row}:{$rightCol}{$row}");
                $sheet->getStyle("{$col}{$row}")->getAlignment()->setHorizontal($alignCenter);
                foreach ($value['month'] as $monthNum) {
                    $subRow = 2;
                    $subCol = CoefficientCompanyHelper::getExcelColumn($subNum);
                    $sheet->setCellValue("{$subCol}{$subRow}", Month::$monthShort[$monthNum]);
                    if (isset($value['bg'])) {
                        $sheet->getStyle("{$subCol}{$subRow}")->getFill()->setFillType($fillSolid);
                        $sheet->getStyle("{$subCol}{$subRow}")->getFill()->getStartColor()->setRGB($value['bg']);
                        $sheet->getStyle("{$subCol}{$subRow}")->getFont()->setBold( true );
                    }

                    $subNum++;
                }
            } else {
                $row = 2;
                $col = CoefficientCompanyHelper::getExcelColumn($num);
            }

            $sheet->setCellValue("{$col}{$row}", $value['header']);
            if (isset($value['bg'])) {
                $sheet->getStyle("{$col}{$row}")->getFill()->setFillType($fillSolid);
                $sheet->getStyle("{$col}{$row}")->getFill()->getStartColor()->setRGB($value['bg']);
                $sheet->getStyle("{$col}{$row}")->getFont()->setBold( true );
            }
            $num++;
        }

        /** @var Company $model */
        foreach ($models as $model) {

            $mainCheckingAccount = $model->mainCheckingAccountant;
            $subscribe = Subscribe::find()
                ->andWhere(['>', 'expired_at', 0])
                ->andWhere(['company_id' => $model->id])
                ->andWhere(['tariff_id' => [1,2,3]])
                ->orderBy(['expired_at' => SORT_DESC])->one();

            if (!$subscribe)
                $subscribe = new Subscribe(['company_id' => $model->id]);

            $num = 1;
            $row++;
            foreach ($data as $param => $value) {

                $col = CoefficientCompanyHelper::getExcelColumn($num);

                if ($param == 1) {
                    $sheet->setCellValue("{$col}{$row}", $model->id);
                } elseif ($param == 2) {
                    $sheet->setCellValue("{$col}{$row}", date('d.m.Y', $model->created_at));
                } elseif ($param == 3) {
                    $sheet->setCellValue("{$col}{$row}", date('d.m.Y', $subscribe->activated_at));
                } elseif ($param == 4) {
                    $sheet->setCellValue("{$col}{$row}", date('d.m.Y', $subscribe->expired_at));
                } elseif ($param == 5) {
                    $sheet->setCellValue("{$col}{$row}", $model->getShortName());
                } elseif ($param == 6) {
                    $sheet->setCellValue("{$col}{$row}", $model->phone);
                } elseif ($param == 7) {
                    $sheet->setCellValue("{$col}{$row}", join(', ', array_filter([$model->chief_post_name, $model->getChiefFio()])));
                } elseif ($param == 8) {
                    $sheet->setCellValue("{$col}{$row}", ($mainCheckingAccount) ? $mainCheckingAccount->bank_name : '');
                } elseif ($param == 9) {
                    $sheet->setCellValue("{$col}{$row}", $model->inn);
                } elseif ($param == 10) {
                    $sheet->setCellValue("{$col}{$row}", $model->getAddressLegalFull());
                } elseif ($param == 11) {
                    $sheet->setCellValue("{$col}{$row}", $model->email);
                } elseif ($param == 12) {
                    $sheet->setCellValue("{$col}{$row}", $model->companyTaxationType->name);
                } elseif (in_array($param, [13,14,15,16])) {
                    $subNum = $num;
                    $num += $value['columns'] - 1;
                    foreach ($value['month'] as $month) {
                        $subCol = CoefficientCompanyHelper::getExcelColumn($subNum);
                        $year = $value['year'];
                        $sheet->setCellValue("{$subCol}{$row}", CoefficientCompanyHelper::{$value['method']}($model->id, $month, $year, $tariffIds));
                        $subNum++;
                    }
                } elseif ($param == 17) {
                    $sheet->setCellValue("{$col}{$row}", CoefficientCompanyHelper::getTotalPaymentsSum($model->id, 6, 2018, $tariffIds)); // from june 2018
                }

                $sheet->getColumnDimension($col)->setAutoSize(true);

                $num++;
            }
        }

        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=".$fileName.".xls" );

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($xls, 'Xlsx');
        $objWriter->save('php://output');
        exit;
    }

    public function actionByGroup($group_id, $year = null)
    {
        if (!in_array($group_id, CoefficientSearch::getGroupIds())) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->layout = 'coefficient';

        if (!$year)
            $year = date('Y');

        $searchModel = new CoefficientSearch();
        $searchModel->init();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'tariffGroup' => $group_id,
            'summaryData' => $this->getDataByTariff($searchModel, $year, $group_id),
            'summaryTotal' => $this->getTotalByTariff($searchModel, $year, $group_id)
        ]);
    }

    private function getDataByTariff(CoefficientSearch $searchModel, $year, $tariffGroup)
    {
        $data = [];

        $currYear = (int)date('Y');
        $currMonth = (int)date('m');

        for ($month = 1; $month <= 12; $month++) {

            $searchModel->year = $year;
            $searchModel->month = $month;

            if ($year == $currYear && $month > $currMonth) {
                $data[$month] = [
                    'month' => Month::$monthShort[$month],
                    'prevMonthCountPayers' => '',
                    'countPayers' => '',
                    'countNewPayers' => '',
                    'countReturnedPayers' => '',
                    'countGonePayers' => '',
                    'MRR' => '',
                    'NEW_MRR' => '',
                    'MAU' => '',
                    'WAU' => '',
                    'DAU' => '',
                    'UVW' => '',
                    'UVD' => '',
                    'href' => '#',
                ];
            } else {
                $data[$month] = [
                    'month' => Month::$monthShort[$month],
                    'prevMonthCountPayers' => $searchModel->getCountPayers($tariffGroup, "-1 month"),
                    'countPayers' => $searchModel->getCountPayers($tariffGroup),
                    'countNewPayers' => $searchModel->getCountNewPayers($tariffGroup),
                    'countReturnedPayers' => $searchModel->getCountReturnedPayers($tariffGroup),
                    'countGonePayers' => $searchModel->getCountGonePayers($tariffGroup),
                    'MRR' => $searchModel->getMRR($tariffGroup),
                    'NEW_MRR' => $searchModel->getNewMRR($tariffGroup),
                    'MAU' => $searchModel->getMAU($tariffGroup),
                    'WAU' => '',
                    'DAU' => $searchModel->getDAU($tariffGroup),
                    'UVW' => $searchModel->getUniqueVisitorsPerWeek($tariffGroup),
                    'UVD' => $searchModel->getUniqueVisitorsPerDay($tariffGroup),
                    'href' => \yii\helpers\Url::to([
                        '/report/coefficient/filter-companies',
                        'year' => $year,
                        'month' => $month,
                        'tariffGroup' => $tariffGroup,
                    ]),
                ];
            }
        }

        return $data;
    }

    private function getTotalByTariff($searchModel, $year, $tariffGroup)
    {
        $searchModel->year = $year;
        $searchModel->searchByYear = true;

        return [
            'prevMonthCountPayers' => $searchModel->getCountPayers($tariffGroup, "-1 year"),
            'countPayers' => $searchModel->getCountPayers($tariffGroup),
            'countNewPayers' => $searchModel->getCountNewPayers($tariffGroup),
            'countReturnedPayers' => $searchModel->getCountReturnedPayers($tariffGroup),
            'countGonePayers' => $searchModel->getCountGonePayers($tariffGroup),
            'MRR' => $searchModel->getMRR($tariffGroup),
            'NEW_MRR' => $searchModel->getNewMRR($tariffGroup),
            'MAU' => $searchModel->getMAU($tariffGroup),
            'WAU' => '',
            'DAU' => '', // calculated in view
            'UVW' => '', // calculated in view
            'UVD' => ''  // calculated in view
        ];
    }

    /**
     * @return string
     */
    public function actionTotals($year = null)
    {
        $this->layout = 'coefficient';

        if (!$year)
            $year = date('Y');

        $searchModel = new CoefficientSearch();
        $searchModel->init();
        $data = $this->getTotalsData($searchModel, $year);
        $dataPrevYear = $this->getTotalsData($searchModel, $year - 1);
        $searchModel->searchByYear = true;
        $totals = $this->getTotalsTotals($searchModel, $year);
        $totalsPrevYear = $this->getTotalsTotals($searchModel, $year - 1);

        return $this->render('totals', [
            'year' => $year,
            'summaryData' => $data,
            'summaryTotal' => $totals,
            'summaryDataPrevYear' => $dataPrevYear,
            'summaryTotalPrevYear' => $totalsPrevYear
        ]);
    }

    // TEMP
    public function actionTestLeaveCompanies()
    {
        return $this->render('test-leave-companies');
    }

    // TEMP
    public function actionTestNewCompanies()
    {
        return $this->render('test-new-companies');
    }

    private function getTotalsData($searchModel, $year)
    {
        $currYear = (int)date('Y');
        $currMonth = (int)date('m');
        $data = [];
        $groupIds = CoefficientSearch::getGroupIds();

        for ($month = 1; $month <= 12; $month++) {

            $searchModel->year = $year;
            $searchModel->month = $month;

            $monthData = ['month' => Month::$monthShort[$month]];
            if ($year == $currYear && $month > $currMonth) {
                foreach ($groupIds as $groupId) {
                    $monthData[$groupId] = '';
                }
                $monthData['add_invoice'] = '';
            } else {
                foreach ($groupIds as $groupId) {
                    $monthData[$groupId] = $searchModel->getMRRTotals($groupId);
                }
                $monthData['add_invoice'] = $searchModel->getMRRTotals(null, 'add_invoice');
            }

            $data[$month] = $monthData;
        }

        return $data;
    }

    private function getTotalsTotals($searchModel, $year)
    {
        $searchModel->year = $year;
        $groupIds = CoefficientSearch::getGroupIds();
        $data = [];
        foreach ($groupIds as $groupId) {
            $data[$groupId] = $searchModel->getMRRTotals($groupId);
        }
        $data['add_invoice'] = $searchModel->getMRRTotals(null, 'add_invoice');

        return $data;
    }
}
