<?php

namespace backend\modules\report;

/**
 * reports module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\report\controllers';
    /**
     * @inheritdoc
     */
    public $layout = 'report';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
