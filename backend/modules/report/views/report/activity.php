<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.02.2017
 * Time: 3:49
 */

use backend\modules\report\models\ReportSearch;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $searchModel ReportSearch */
/* @var $this yii\web\View */
/* @var $period [] */
/* @var $type integer */

$this->title = 'Отчеты';
$activity = $searchModel->getActivity($period, $type);
$mainStatistic = $searchModel->getMainStatistic($period, $type);
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <?php if ($type == ReportSearch::DAY): ?>
                        <span class="glyphicon glyphicon-chevron-left change-period"
                              style="cursor: pointer;" data-id="minus"></span>
                        <span
                            class="glyphicon glyphicon-chevron-right change-period"
                            style="cursor: pointer;" data-id="plus"></span>
                        <?= Html::beginForm(Url::to(['activity']), 'GET', [
                            'id' => 'list-date',
                        ]); ?>
                        <?= Html::activeHiddenInput($searchModel, 'periodDayList'); ?>
                        <?= Html::endForm(); ?>
                    <?php endif; ?>
                </div>
                <div class="actions">
                    <?= Html::beginForm(Url::to(['activity']), 'GET', [
                        'id' => 'search-report',
                    ]); ?>
                    <div class="btn-group btn-group-devided"
                         data-toggle="buttons">
                        <?= Html::activeRadioList($searchModel, 'periodType', [
                            ReportSearch::DAY => 'День',
                            ReportSearch::WEEK => 'Неделя',
                            ReportSearch::MONTH => 'Месяц',
                        ], [
                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                    $class = $searchModel->periodType == $value ? 'active' : '';
                                    $return = '<label class="btn btn-transparent green btn-outline btn-circle btn-sm ' . $class . '">';
                                    $return .= '<input type="radio" name="ReportSearch[periodType]" class="toggle hidden" value="' . $value . '">';
                                    $return .= $label;
                                    $return .= '</label>';

                                    return $return;
                                },
                            ]
                        ); ?>
                    </div>
                    <?= Html::endForm();; ?>
                </div>
            </div>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => 'yellow',
                            'name' => 'Посещаемость',
                            'pointWidth' => 10,
                            'data' => $searchModel->getAttendance($period, $type),
                        ],
                        [
                            'type' => 'column',
                            'color' => 'green',
                            'name' => 'Итого активных',
                            'pointWidth' => 10,
                            'data' => $activity['allActivityCompanies'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Платники',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $activity['payed'],

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Туториал',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $activity['trial'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Бесплатно',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $activity['free'],
                        ],
                    ],
                ],
            ]); ?>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => '#dfba49',
                            'name' => 'Итого активность',
                            'pointWidth' => 10,
                            'data' => $activity['all'],
                        ],
                        [
                            'type' => 'column',
                            'color' => '#45b6af',
                            'name' => 'Исходящие счета',
                            'pointWidth' => 10,
                            'data' => $mainStatistic['outInvoice'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Отправлено по e-mail',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $mainStatistic['sendEmail'],

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Загрузки',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $mainStatistic['inData'],
                        ],
                        [
                            'type' => 'spline',
                            'name' => 'Коэф-т: Активность на 1 активную компанию',
                            'marker' => [
                                'lineWidth' => 2,
                                'lineColor' => 'orange',
                                'fillColor' => 'white',
                            ],
                            'data' => $mainStatistic['coefficient'],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>