<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.08.2017
 * Time: 14:59
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\modules\report\models\EmailSummarySearch;
use yii\data\ActiveDataProvider;
use common\components\grid\GridView;
use common\models\Company;
use common\components\grid\DropDownDataColumn;

/* @var $searchModel EmailSummarySearch */
/* @var $dataProvider ActiveDataProvider */
/* @var $this yii\web\View */

$this->title = 'Имейл сводка';
?>
<div class="row portlet box" style="margin-left: 0;margin-right: 0;">
    <div class="btn-group pull-right title-buttons">
        <?= Html::button('<i class="fa fa-download" style="padding-left: 10px;"></i> Загрузить из Excel', [
            'class' => 'btn yellow w100proc no-padding',
            'data' => [
                'toggle' => 'modal',
                'target' => '#import-xls',
            ],
            'style' => 'width: 200px;',
        ]); ?>
    </div>
    <?= $this->render('@backend/views/xls/_import_xls', [
        'header' => 'Загрузка из Excel',
        'text' => '<p>Для загрузки списка email из Excel,
                   <br>
                   заполните шаблон таблицы и загрузите ее тут.
                   </p>',
        'formData' => null,
        'uploadXlsTemplateUrl' => Url::to(['download-template',]),
        'uploadAction' => Url::to(['upload-email-summary']),
        'progressAction' => Url::to(['progress-upload-email-summary']),
    ]); ?>
</div>
<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-3 col-sm-3">
            Список компаний <?= $dataProvider->query->count(); ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable status_nowrap overfl_text_hid invoice-table',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'form_keyword',
                        'label' => 'Ключевое слово',
                        'class' => DropDownDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getFormKeyWordFilter(),
                        'value' => function (Company $model) {
                            return $model->form_keyword;
                        },
                    ],
                    [
                        'attribute' => 'form_region',
                        'label' => 'Регион',
                        'class' => DropDownDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getFormKeyRegionFilter(),
                        'value' => function (Company $model) {
                            return $model->form_region;
                        },
                    ],
                    [
                        'attribute' => 'form_source',
                        'label' => 'Источник',
                        'class' => DropDownDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getFormKeySourceFilter(),
                        'value' => function (Company $model) {
                            return $model->form_source;
                        },
                    ],
                    [
                        'attribute' => 'name_short',
                        'label' => 'Название компании',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            return Html::a($model->getShortName(), Url::to(['/company/company/view', 'id' => $model->id]));
                        },
                    ],
                    [
                        'attribute' => 'company_type_id',
                        'label' => 'ФО',
                        'class' => DropDownDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getCompanyTypeFilter(),
                        'value' => function (Company $model) {
                            return $model->companyType->name_short;
                        },
                    ],
                    [
                        'attribute' => 'companyTaxationType',
                        'label' => 'СНО',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            return $model->companyTaxationType ? $model->companyTaxationType->name : '';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

