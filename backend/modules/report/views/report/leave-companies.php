<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.11.2017
 * Time: 8:08
 */

use common\components\TextHelper;
use common\components\grid\DropDownSearchDataColumn;
use common\models\report\ReportState;
use common\models\service\SubscribeTariffGroup;
use php_rutils\RUtils;
use yii\data\ActiveDataProvider;
use backend\modules\report\models\LeaveCompaniesSearch;
use frontend\widgets\RangeButtonWidget;
use yii\bootstrap\Html;
use common\components\grid\GridView;
use common\components\grid\GridViewHelper;
use yii\helpers\StringHelper;
use common\models\Company;
use yii\helpers\Url;
use common\models\service\SubscribeHelper;
use common\components\date\DateHelper;
use common\models\service\Subscribe;
use common\models\service\Payment;
use common\models\document\Invoice;
use frontend\models\Documents;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeStatus;

/* @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel LeaveCompaniesSearch
 */

$this->title = 'Ушедшие платники';
$query = $dataProvider->query;
$subscribeIds = $searchModel->getSubscribeIds();
?>
<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <div class="col-md-6 col-md-pull-3">
        <?php
        $tariffsByGroup = SubscribeTariffGroup::getTariffsByGroups();
        $queryBase = Payment::find()->leftJoin(Subscribe::tableName(), 'service_payment.id=service_subscribe.payment_id')->where([Subscribe::tableName() . '.id' => $subscribeIds]);

        // TEST
        if (Yii::$app->request->get('test')) {
            echo 'companies  Standart: <br>';
            echo '<textarea>';
            echo implode(',', $queryBase->andWhere([Subscribe::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::STANDART]])->select('service_subscribe.company_id')->column());
            echo '</textarea>';
        }

        ?>
        <div id="w2" style="overflow-x: auto; margin: 10px 0; width: 100%">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid" style="margin-top: 0!important;">
                <thead>
                <tr class="heading">
                    <th>Первые оплаты</th>
                    <th>Кол-во (шт)</th>
                    <th>Сумма (руб.)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Выставление счетов</td>
                    <td class="text-right"><?php $newQuery = (clone $queryBase)->andWhere([Subscribe::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::STANDART]]);
                        echo $newQuery->count(); ?></td>
                    <td class="text-right"><?php
                        echo TextHelper::numberFormat($newQuery->sum(Payment::tableName() . '.sum') ?: 0); ?></td>
                </tr>
                <tr>
                    <td>Бухгалтерия ИП</td>
                    <td class="text-right"><?php $newQuery = (clone $queryBase)->andWhere([Subscribe::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::IP_USN_6]]);
                        echo $newQuery->count(); ?></td>
                    <td class="text-right"><?php
                        echo TextHelper::numberFormat($newQuery->sum(Payment::tableName() . '.sum') ?: 0); ?></td>
                </tr>
                <tr>
                    <td>Модуль В2В</td>
                    <td class="text-right"><?php $newQuery = (clone $queryBase)->andWhere([Subscribe::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::B2B_PAYMENT]]);
                        echo $newQuery->count(); ?></td>
                    <td class="text-right"><?php
                        echo TextHelper::numberFormat($newQuery->sum(Payment::tableName() . '.sum') ?: 0); ?></td>
                </tr>
                <tr>
                    <td>ФинДиректор</td>
                    <td class="text-right"><?php $newQuery = (clone $queryBase)->andWhere([Subscribe::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::ANALYTICS]]);
                        echo $newQuery->count(); ?></td>
                    <td class="text-right"><?php
                        echo TextHelper::numberFormat($newQuery->sum(Payment::tableName() . '.sum') ?: 0); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-3 col-sm-3">
            <?= $this->title . ' ' . $dataProvider->totalCount; ?>
        </div>
    </div>
    <div class="portlet-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                'aria-describedby' => 'datatable_ajax_info',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'options' => [
                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],
            'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n" .
                '<ul class="pagination pull-left pageSize">
                            <li><span class="no-style">Выводить по количеству строк:</span></li>' .
                GridViewHelper::getPagination(StringHelper::basename(get_class($searchModel)), $searchModel->pageSize, ['leave-companies']) .
                '</ul>' . "{pager}",

            'columns' => [
                [
                    'attribute' => 'name',
                    'label' => 'Компания',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return Html::a($model->company->getShortName(), Url::to(['/company/company/view', 'id' => $model->company_id]));
                    }
                ],
                [
                    'attribute' => 'paymentDate',
                    'label' => 'Дата последней оплаты',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->payment->payment_date);
                    }
                ],
                [
                    'attribute' => 'paymentSum',
                    'label' => 'Сумма последней оплаты',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return $model->payment->sum;
                    }
                ],
                [
                    'attribute' => 'tariffType',
                    'class' => DropDownSearchDataColumn::className(),
                    'label' => 'Тип подписки',
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                        'width' => '10%',
                    ],
                    'selectPluginOptions' => [
                        'width' => '300px'
                    ],
                    'format' => 'html',
                    'filter' => $searchModel->getTariffFilter(),
                    'value' => function (Subscribe $model) {
                        $content = '';
                        if ($tariff = $model->tariff) {
                            $content = $tariff->tariffGroup->name . '<br>' . $tariff->getTariffName();
                        }

                        return $content;
                    },
                ],
                [
                    'attribute' => 'lastExpiredAt',
                    'label' => 'Дата окончания',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        $tariffs = SubscribeTariff::getTariffsByTariff($model->tariff_id);
                        return date(DateHelper::FORMAT_USER_DATE, $model->company->getSubscribes()
                            ->andWhere(['in', 'tariff_id', $tariffs])
                            ->max('expired_at'));
                    }
                ],
                [
                    'attribute' => 'expired_at',
                    'label' => 'Дата следующей оплаты',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        $actualSubscribes = SubscribeHelper::getPayedSubscriptions($model->company->id, $model->tariff_group_id);
                        $expireDate = SubscribeHelper::getExpireDate($actualSubscribes);
                        if (!$expireDate) {

                            $tariffs = SubscribeTariff::getTariffsByTariff($model->tariff_id);
                            $expireDate = $model->company->getSubscribes()
                                ->andWhere(['in', 'tariff_id', $tariffs])
                                ->max('expired_at') + 3600 * 24;

                        }

                        return ($expireDate) ? date(DateHelper::FORMAT_USER_DATE, $expireDate) : '';
                    }
                ],
                [
                    'attribute' => 'maxInvoiceDocumentDate',
                    'label' => 'Дата последнего счета',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return DateHelper::format($model->company->getInvoices()->andWhere(['and',
                            [Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT],
                            [Invoice::tableName() . '.is_deleted' => false],
                        ])->max('document_date'), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    }
                ],
                [
                    'attribute' => 'invoiceCount',
                    'label' => 'Всего выставлено счетов',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return $model->company->getOutInvoiceCount();
                    }
                ],
                [
                    'attribute' => 'lastVisitAt',
                    'label' => 'Дата последнего посещения',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->company->last_visit_at);
                    }
                ],
                [
                    'attribute' => 'phone',
                    'label' => 'Телефон',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return $model->company->phone;
                    }
                ],
                [
                    'attribute' => 'email',
                    'label' => 'Email',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return $model->company->email;
                    }
                ],
                [
                    'attribute' => 'chief_fio',
                    'label' => 'ФИО',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return $model->company->getChiefFio();
                    }
                ],
            ],
        ]); ?>
    </div>
</div>