<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2017
 * Time: 5:16
 */

use yii\bootstrap\Html;
use backend\modules\report\models\ReportSearch;
use yii\helpers\Url;

/* @var $searchModel ReportSearch */
/* @var $this yii\web\View */
/* @var $period [] */
/* @var $type integer */

$this->title = 'Отчеты';
$registrationPercents = $searchModel->getRegistrationPercents($period, $type);
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <?php if ($type == ReportSearch::DAY): ?>
                        <span class="glyphicon glyphicon-chevron-left change-period"
                              style="cursor: pointer;" data-id="minus"></span>
                        <span
                            class="glyphicon glyphicon-chevron-right change-period"
                            style="cursor: pointer;" data-id="plus"></span>
                        <?= Html::beginForm(Url::to(['robot']), 'GET', [
                            'id' => 'list-date',
                        ]); ?>
                        <?= Html::activeHiddenInput($searchModel, 'periodDayList'); ?>
                        <?= Html::endForm(); ?>
                    <?php endif; ?>
                </div>
                <div class="actions">
                    <?= Html::beginForm(Url::to(['robot']), 'GET', [
                        'id' => 'search-report',
                    ]); ?>
                    <div class="btn-group btn-group-devided"
                         data-toggle="buttons">
                        <?= Html::activeRadioList($searchModel, 'periodType', [
                            ReportSearch::DAY => 'День',
                            ReportSearch::WEEK => 'Неделя',
                            ReportSearch::MONTH => 'Месяц',
                        ], [
                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                    $class = $searchModel->periodType == $value ? 'active' : '';
                                    $return = '<label class="btn btn-transparent green btn-outline btn-circle btn-sm ' . $class . '">';
                                    $return .= '<input type="radio" name="ReportSearch[periodType]" class="toggle hidden" value="' . $value . '">';
                                    $return .= $label;
                                    $return .= '</label>';

                                    return $return;
                                },
                            ]
                        ); ?>
                    </div>
                    <?= Html::endForm();; ?>
                </div>
            </div>
            <h4 style="font-weight:bold;">Нулевая декларация</h4>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => '#45b6af',
                            'name' => 'Клик на ссылку',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, null, 89),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Скачали',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, null, 90),

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Распечатали Опись',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, null, 91),
                        ],
                    ],
                ],
            ]); ?>
            <h4 style="font-weight:bold;">Декларация</h4>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => '#45b6af',
                            'name' => 'Клик на ссылку',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, null, 92),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Скачали',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, null, 93),

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Распечатали Опись',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, null, 94),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Скачали платёжку',
                            'color' => '#FFA500',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, null, 95),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Распечатали платёжку',
                            'color' => 'green',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, null, 96),
                        ],
                    ],
                ],
            ]); ?>

        </div>
    </div>
</div>