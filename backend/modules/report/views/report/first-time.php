<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.04.2017
 * Time: 11:50
 */

use backend\modules\report\models\ReportSearch;
use yii\bootstrap\Html;
use yii\helpers\Url;
use common\models\Company;

/* @var $searchModel ReportSearch */
/* @var $this yii\web\View */
/* @var $period [] */
/* @var $type integer */

$this->title = 'Отчеты';
$firstTime = $searchModel->getFirstTimeCompaniesOut($period, $type);
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <?php if ($type == ReportSearch::DAY): ?>
                        <span class="glyphicon glyphicon-chevron-left change-period"
                              style="cursor: pointer;" data-id="minus"></span>
                        <span
                            class="glyphicon glyphicon-chevron-right change-period"
                            style="cursor: pointer;" data-id="plus"></span>
                        <?= Html::beginForm(Url::to(['first-time']), 'GET', [
                            'id' => 'list-date',
                        ]); ?>
                        <?= Html::activeHiddenInput($searchModel, 'periodDayList'); ?>
                        <?= Html::endForm(); ?>
                    <?php endif; ?>
                </div>
                <div class="actions">
                    <?= Html::beginForm(Url::to(['first-time']), 'GET', [
                        'id' => 'search-report',
                    ]); ?>
                    <div class="btn-group btn-group-devided"
                         data-toggle="buttons">
                        <?= Html::activeRadioList($searchModel, 'periodType', [
                            ReportSearch::DAY => 'День',
                            ReportSearch::WEEK => 'Неделя',
                            ReportSearch::MONTH => 'Месяц',
                        ], [
                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                    $class = $searchModel->periodType == $value ? 'active' : '';
                                    $return = '<label class="btn btn-transparent green btn-outline btn-circle btn-sm ' . $class . '">';
                                    $return .= '<input type="radio" name="ReportSearch[periodType]" class="toggle hidden" value="' . $value . '">';
                                    $return .= $label;
                                    $return .= '</label>';

                                    return $return;
                                },
                            ]
                        ); ?>
                    </div>
                    <?= Html::endForm();; ?>
                </div>
            </div>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => 'yellow',
                            'name' => 'Создали исх Акт',
                            'pointWidth' => 10,
                            'data' => $firstTime['outActCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'color' => 'green',
                            'name' => 'Создали исх ТН',
                            'pointWidth' => 10,
                            'data' => $firstTime['outPackingListCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Создали исх СФ',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $firstTime['outInvoiceFactureCompanyCount'],

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Загрузили выписку из банка',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $firstTime['statementUploadCount'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Прикрепили скан к документу',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $firstTime['filesCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Загружен (лого, печать, подпись)',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $firstTime['uploadedFiles'],
                        ],
                    ],
                ],
            ]); ?>

            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => 'yellow',
                            'name' => 'Создали вх Счет',
                            'pointWidth' => 10,
                            'data' => $firstTime['inInvoiceCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'color' => 'green',
                            'name' => 'Создали вх Акт',
                            'pointWidth' => 10,
                            'data' => $firstTime['inActCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Создали вх ТН',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $firstTime['inPackingListCompanyCount'],

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Создали вх СФ',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $firstTime['inInvoiceFactureCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Выгрузили данные в 1С',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $firstTime['exportCompanyCount'],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>