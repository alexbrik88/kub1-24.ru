<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.09.2017
 * Time: 15:39
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use backend\modules\report\models\ReportSearch;

/* @var $searchModel ReportSearch
 * @var $this yii\web\View
 * @var $registrations
 * @var $completeProfile
 * @var $exhibitedInvoices
 * @var $freeTariff
 * @var $firstPayed
 * @var $xAxisCategories
 * @var $firstPayedSum
 * @var $exhibitedInvoicesTwo
 * @var $exhibitedInvoicesFive
 * @var $exhibitedInvoicesTen
 * @var $withLogo
 * @var $exhibitedInvoicesOne
 */

$this->title = 'Когорты';
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <?php if ($searchModel->periodType == ReportSearch::DAY): ?>
                        <span class="glyphicon glyphicon-chevron-left change-period"
                              style="cursor: pointer;" data-id="minus"></span>
                        <span
                            class="glyphicon glyphicon-chevron-right change-period"
                            style="cursor: pointer;" data-id="plus"></span>
                        <?= Html::beginForm(Url::to(['cohorts']), 'GET', [
                            'id' => 'list-date',
                        ]); ?>
                        <?= Html::activeHiddenInput($searchModel, 'periodDayList'); ?>
                        <?= Html::endForm(); ?>
                    <?php endif; ?>
                </div>
                <div class="actions">
                    <?= Html::beginForm(Url::to(['cohorts']), 'GET', [
                        'id' => 'search-report',
                    ]); ?>
                    <div class="btn-group btn-group-devided"
                         data-toggle="buttons">
                        <?= Html::activeRadioList($searchModel, 'periodType', [
                            ReportSearch::DAY => 'День',
                            ReportSearch::WEEK => 'Неделя',
                            ReportSearch::MONTH => 'Месяц',
                            ReportSearch::QUARTER => 'Квартал',
                        ], [
                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                    $class = $searchModel->periodType == $value ? 'active' : '';
                                    $return = '<label class="btn btn-transparent green btn-outline btn-circle btn-sm ' . $class . '">';
                                    $return .= '<input type="radio" name="ReportSearch[periodType]" class="toggle hidden" value="' . $value . '">';
                                    $return .= $label;
                                    $return .= '</label>';

                                    return $return;
                                },
                            ]
                        ); ?>
                    </div>
                    <?= Html::endForm(); ?>
                </div>
            </div>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $xAxisCategories,
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => '#dfba49',
                            'name' => 'Кол-во регистраций',
                            'pointWidth' => 10,
                            'data' => $registrations,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во заполнивших профиль',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $completeProfile,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших счет',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $exhibitedInvoices,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во бесплатный тариф',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $freeTariff,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших = 1 счетов',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $exhibitedInvoicesOne,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших > 1 счетов',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $exhibitedInvoicesTwo,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших > 5 счетов',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $exhibitedInvoicesFive,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших > 10 счетов',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $exhibitedInvoicesTen,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во загрузивших логотип, подпись, печать',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $withLogo,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во сделавших первую оплату',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $firstPayed,
                        ],
                    ],
                ],
            ]); ?>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $xAxisCategories,
                    ],
                    'yAxis' => [
                        'title' => ['text' => '%']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'name' => 'Кол-во заполнивших профиль',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $searchModel->getCohortsPercent($registrations, $completeProfile),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших счет',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $searchModel->getCohortsPercent($registrations, $exhibitedInvoices),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во бесплатный тариф',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getCohortsPercent($registrations, $freeTariff),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших = 1 счетов',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' =>  $searchModel->getCohortsPercent($registrations, $exhibitedInvoicesOne),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших > 1 счетов',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getCohortsPercent($registrations, $exhibitedInvoicesTwo),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших > 5 счетов',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getCohortsPercent($registrations, $exhibitedInvoicesFive),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во выставивших > 10 счетов',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getCohortsPercent($registrations, $exhibitedInvoicesTen),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во загрузивших логотип, подпись, печать',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getCohortsPercent($registrations, $withLogo),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во сделавших первую оплату',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getCohortsPercent($registrations, $firstPayed),
                        ],
                    ],
                ],
            ]); ?>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $xAxisCategories,
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Сумма']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'name' => 'Сумма первых оплат',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $firstPayedSum,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Средний чек',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $searchModel->getAverageCheckCohorts($firstPayedSum, $firstPayed),
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
