<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.07.2017
 * Time: 9:14
 */

use common\components\grid\DropDownSearchDataColumn;
use common\models\service\Payment;
use common\models\service\StoreOutInvoiceTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\components\StatisticPeriod;
use php_rutils\RUtils;
use yii\data\ActiveDataProvider;
use backend\modules\report\models\StateSearch;
use frontend\widgets\RangeButtonWidget;
use common\components\grid\GridView;
use common\components\grid\GridViewHelper;
use yii\helpers\StringHelper;
use common\models\report\ReportState;
use yii\bootstrap\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\models\company\CompanyType;
use common\models\service\SubscribeTariff;
use common\models\service\PaymentType;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\TextHelper;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel StateSearch
 */
$this->title = 'Первые оплаты';
$query = $dataProvider->query;
$countStates = $dataProvider->query->count();

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);

// TEST
if (Yii::$app->request->get('test')) {
    $tariffsByGroup = SubscribeTariffGroup::getTariffsByGroups();
    $queryBase = $query;
    $newQuery = (clone $queryBase)->andWhere([ReportState::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::STANDART]]);
    echo 'companies Standart: <br>';
    echo '<textarea>';
    echo implode(',', $newQuery->select('company_id')->column());
    echo '</textarea>';
}

?>
<div class="row">
    <div class="col-xs-12 col-md-12 col-lg-12">
        <div class="portlet-body employee-wrapper">
            <div class="tabbable tabbable-tabdrop">
                <ul class="nav nav-tabs li-border float-r" style="float: right;">
                    <li class="active">
                        <?= Html::a('Первые оплаты', Url::to(['/report/report/state']), ['aria-expanded' => 'false',]); ?>
                    </li>
                    <li>
                        <?= Html::a('Повторные оплаты', Url::to(['/report/report/repeat-state']), ['aria-expanded' => 'false',]); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-md-push-9">
        <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <div id="w1" style="overflow-x: auto;">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid" style="margin-top: 0!important;">
                <thead>
                <tr class="heading">
                    <th>Среднее кол-во дней</th>
                    <th>Среднее кол-во счетов</th>
                    <th>Кол-во ИП</th>
                    <th>Кол-во ООО</th>
                    <th>Кол-во УСН</th>
                    <th>Кол-во ОСНО</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= $countStates ? round($dataProvider->query->sum('days_without_payment') / $countStates, 2) : 0; ?></td>
                    <td><?= $countStates ? round($dataProvider->query->sum('invoice_count') / $countStates, 2) : 0; ?></td>
                    <td><?php $newQuery = clone $query;
                        echo $newQuery->andWhere([ReportState::tableName() . '.company_type_id' => CompanyType::TYPE_IP])->count(); ?>
                    </td>
                    <td><?php $newQuery = clone $query;
                        echo $newQuery->andWhere([ReportState::tableName() . '.company_type_id' => CompanyType::TYPE_OOO])->count(); ?>
                    </td>
                    <td><?php $newQuery = clone $query;
                        echo $newQuery->andWhere([ReportState::tableName() . '.company_taxation_type_name' => 'УСН'])->count(); ?>
                    </td>
                    <td><?php $newQuery = clone $query;
                        echo $newQuery->andWhere([ReportState::tableName() . '.company_taxation_type_name' => 'ОСНО'])->count(); ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-6">
        <?php
            $tariffsByGroup = SubscribeTariffGroup::getTariffsByGroups();
            $queryBase = $query; // (new StateSearch())->getBaseQuery(StatisticPeriod::getSessionPeriod());
        ?>
        <div id="w2" style="overflow-x: auto; margin: 10px 0">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid" style="margin-top: 0!important;">
                <thead>
                <tr class="heading">
                    <th>Первые оплаты</th>
                    <th>Кол-во (шт)</th>
                    <th>Сумма (руб.)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Выставление счетов</td>
                    <td class="text-right"><?php $newQuery = (clone $queryBase)->andWhere([ReportState::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::STANDART]]);
                        echo $newQuery->count(); ?></td>
                    <td class="text-right"><?php
                        echo TextHelper::numberFormat($newQuery->sum('pay_sum')); ?></td>
                </tr>
                <tr>
                    <td>Бухгалтерия ИП</td>
                    <td class="text-right"><?php $newQuery = (clone $queryBase)->andWhere([ReportState::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::IP_USN_6]]);
                        echo $newQuery->count(); ?></td>
                    <td class="text-right"><?php
                        echo TextHelper::numberFormat($newQuery->sum('pay_sum')); ?></td>
                </tr>
                <tr>
                    <td>Модуль В2В</td>
                    <td class="text-right"><?php $newQuery = (clone $queryBase)->andWhere([ReportState::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::B2B_PAYMENT]]);
                        echo $newQuery->count(); ?></td>
                    <td class="text-right"><?php
                        echo TextHelper::numberFormat($newQuery->sum('pay_sum')); ?></td>
                </tr>
                <tr>
                    <td>ФинДиректор</td>
                    <td class="text-right"><?php $newQuery = (clone $queryBase)->andWhere([ReportState::tableName() . '.tariff_id' => $tariffsByGroup[SubscribeTariffGroup::ANALYTICS]]);
                        echo $newQuery->count(); ?></td>
                    <td class="text-right"><?php
                        echo TextHelper::numberFormat($newQuery->sum('pay_sum')); ?></td>
                </tr>
                <tr>
                    <td>Счета штучные</td>
                    <td class="text-right"><?php $newQuery = (clone $queryBase)->andWhere(['>', ReportState::tableName() . '.invoice_tariff_id', 0]);
                        echo $newQuery->count(); ?></td>
                    <td class="text-right"><?php
                        echo TextHelper::numberFormat($newQuery->sum('pay_sum')); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-8">
            <span style="margin-right: 20px;">
                <?= $this->title . ': ' . $dataProvider->totalCount; ?>
            </span>
            <span>
                Сумма первых оплат: <?= $dataProvider->query->sum('pay_sum') ?>
            </span>
        </div>
        <div class="caption list_recip col-md-3"
             style="float: right;text-align: right;">
            <?= Html::a('Выгрузить в Excel', Url::to($searchModel->getDownloadExcelUrl()), [
                'style' => 'color: white;',
            ]); ?>
        </div>
    </div>
    <div class="portlet-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                'aria-describedby' => 'datatable_ajax_info',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'options' => [
                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],
            'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n" .
                '<ul class="pagination pull-left pageSize">
                            <li><span class="no-style">Выводить по количеству строк:</span></li>' .
                GridViewHelper::getPagination(StringHelper::basename(get_class($searchModel)), $searchModel->pageSize, ['state']) .
                '</ul>' . "{pager}",

            'columns' => [
                [
                    'attribute' => 'company_id',
                    'label' => 'id',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return Html::a($model->company_id, Url::to(['/company/company/view', 'id' => $model->company_id]));
                    }
                ],
                [
                    'attribute' => 'name_short',
                    'label' => 'Название',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->company->getShortName();
                    }
                ],
                [
                    'attribute' => 'company_type_id',
                    'label' => 'Тип',
                    'class' => DropDownDataColumn::className(),
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                        'width' => '10%',
                    ],
                    'format' => 'raw',
                    'filter' => $searchModel->getCompanyTypeFilter(),
                    'value' => function (ReportState $model) {
                        return $model->company->companyType->name_short;
                    }
                ],
                [
                    'attribute' => 'company_taxation_type_name',
                    'label' => 'СНО',
                    'class' => DropDownDataColumn::className(),
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                        'width' => '10%',
                    ],
                    'format' => 'raw',
                    'filter' => $searchModel->getCompanyTaxationTypeNameFilter(),
                    'value' => function (ReportState $model) {
                        return $model->company_taxation_type_name;
                    }
                ],
                [
                    'attribute' => 'company.activities_id',
                    'label' => 'Вид деятельности',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '10%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->company->activities ? $model->company->activities->name : null;
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата регистрации',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '15%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->company->created_at);
                    }
                ],
                [
                    'attribute' => 'registrationPageType',
                    'label' => 'Страница регистрации',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '10%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->company->registrationPageType ? $model->company->registrationPageType->name : null;
                    }
                ],
                [
                    'attribute' => 'pay_date',
                    'label' => 'Дата оплаты',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '10%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->pay_date);
                    }
                ],
                [
                    'attribute' => 'days_without_payment',
                    'label' => 'Кол-во дней',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->days_without_payment;
                    }
                ],
                [
                    'attribute' => 'pay_sum',
                    'label' => 'Сумма оплаты',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->pay_sum;
                    }
                ],
                [
                    'attribute' => 'tariffType',
                    'class' => DropDownSearchDataColumn::className(),
                    'label' => 'Тип подписки',
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                    ],
                    'selectPluginOptions' => [
                        'width' => '300px'
                    ],
                    'format' => 'html',
                    'filter' => $searchModel->getTariffFilter(),
                    'value' => function (ReportState $model) {
                        $content = '';
                        if ($model->invoice_tariff_id > 0) {
                            $content = $model->invoiceTariff->invoice_count . ' ' .
                                RUtils::numeral()->choosePlural($model->invoiceTariff->invoice_count, [
                                    'счет', //1
                                    'счета', //2
                                    'счетов' //5
                                ]);
                        } else if ($tariff = $model->tariff) {
                            $content = $tariff->tariffGroup->name . '<br>' . $tariff->getTariffName();
                        }

                        return $content;
                    },
                ],
                [
                    'attribute' => 'invoice_count',
                    'label' => 'Счет',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->invoice_count;
                    }
                ],
                [
                    'attribute' => 'act_count',
                    'label' => 'Акт',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->act_count;
                    }
                ],
                [
                    'attribute' => 'packing_list_count',
                    'label' => 'ТН',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->packing_list_count;
                    }
                ],
                [
                    'attribute' => 'invoice_facture_count',
                    'label' => 'СФ',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->invoice_facture_count;
                    }
                ],
                [
                    'attribute' => 'upd_count',
                    'label' => 'УПД',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->upd_count;
                    }
                ],
                [
                    'attribute' => 'has_logo',
                    'label' => 'Логотип',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '11%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->has_logo;
                    }
                ],
                [
                    'attribute' => 'has_print',
                    'label' => 'Печать',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '11%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->has_print;
                    }
                ],
                [
                    'attribute' => 'has_signature',
                    'label' => 'Подпись',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->has_signature;
                    }
                ],
                [
                    'attribute' => 'sum_company_images',
                    'label' => 'Итого',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->sum_company_images;
                    }
                ],
                [
                    'attribute' => 'product_count',
                    'label' => 'Товар',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->product_count;
                    }
                ],
                [
                    'attribute' => 'service_count',
                    'label' => 'Услуги',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->service_count;
                    }
                ],
                [
                    'attribute' => 'employees_count',
                    'label' => 'Кол-во сотрудников',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '5%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->employees_count;
                    }
                ],
                [
                    'attribute' => 'download_statement_count',
                    'label' => 'Загрузка выписок',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->download_statement_count;
                    }
                ],
                [
                    'attribute' => 'download_1c_count',
                    'label' => 'Выгрузка в 1С',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->download_1c_count;
                    }
                ],
                [
                    'attribute' => 'import_xls_product_count',
                    'label' => 'Загружено товаров из Excel',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->import_xls_product_count;
                    }
                ],
                [
                    'attribute' => 'import_xls_service_count',
                    'label' => 'Загружено услуг из Excel',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->import_xls_service_count;
                    }
                ],
                [
                    'attribute' => 'company.form_source',
                    'label' => 'Источник',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->company->form_source;
                    }
                ],
                [
                    'attribute' => 'company.form_keyword',
                    'label' => 'Ключевое слово',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->company->form_keyword;
                    }
                ],
                [
                    'attribute' => 'company.form_region',
                    'label' => 'Регион',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '7%',
                    ],
                    'format' => 'raw',
                    'value' => function (ReportState $model) {
                        return $model->company->form_region;
                    }
                ],
            ],
        ]); ?>
    </div>
</div>

