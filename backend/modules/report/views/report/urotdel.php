<?php

/* @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel \backend\modules\report\models\UrotdelVisitsSearch
 */

use common\components\date\DateHelper;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\employee\Employee;
use common\models\UrotdelVisit;
use frontend\widgets\RangeButtonWidget;

$this->title = 'Список посещений страницы ЮрОтдел';
?>
<div class="row">
    <div class="col-md-9 col-sm-9">
    </div>
    <div class="col-md-3 col-sm-3">
        <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row']); ?>
    </div>
</div>
<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-6 col-sm-6">
            <?= "{$this->title} ({$dataProvider->totalCount})"; ?>
        </div>
    </div>
    <div class="portlet-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
            'options' => [
                'style' => 'overflow-x: auto;',
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],
            'columns' => [
                [
                    'attribute' => 'company.id',
                    'label' => 'ID',
                    'format' => 'raw',
                    'value' => function (UrotdelVisit $model) {
                        return Html::a($model->company->id, ['view', 'id' => $model->company->id, 'backUrl' => '/report/report/urotdel']);
                    },
                ],
                [
                    'label' => 'Название организации',
                    'attribute' => 'company_name_short',
                    'class' => DropDownSearchDataColumn::class,
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => 'dropdown-filter',
                    ],
                    'format' => 'raw',
                    'filter' => [
                        null => 'Все',
                        1 => 'Есть название',
                        0 => 'Нет названия',
                    ],
                    'value' => function (UrotdelVisit $model) {
                        return Html::a($model->company->name_short, ['view', 'id' => $model->company->id, 'backUrl' => '/report/report/urotdel']);
                    },
                ],
                [
                    'attribute' => 'company.email',
                    'label' => 'Email',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'value' => function (UrotdelVisit $model) {
                        return $model->company->email;
                    },
                ],
                [
                    'attribute' => 'company_phone',
                    'label' => 'Телефон',
                    'class' => DropDownSearchDataColumn::class,
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => [
                        null => 'Все',
                        1 => 'Есть телефон',
                        0 => 'Нет телефона',
                    ],
                    'value' => function (UrotdelVisit $model) {
                        return $model->company->phone;
                    },
                ],
                [
                    'attribute' => 'company.created_by',
                    'label' => 'ФИО регистрация',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (UrotdelVisit $model) {
                        return ($model->company->created_by && ($createdByEmployee = Employee::findOne($model->company->created_by)))
                            ? $createdByEmployee->getFio()
                            : '---';
                    },
                ],
                [
                    'class' => DropDownSearchDataColumn::class,
                    'attribute' => 'company_fio',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => [
                        null => 'Все',
                        1 => 'Есть ФИО',
                        0 => 'Нет ФИО',
                    ],
                    'label' => 'ФИО руководителя',
                    'value' => function (UrotdelVisit $model) {
                        return $model->company->getChiefFio();
                    }
                ],
                [
                    'attribute' => 'company.created_at',
                    'label' => 'Дата регистрации',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'sorting',
                    ],
                    'value' => function (UrotdelVisit $model) {
                        return date(DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS, $model->company->created_at);
                    }
                ],
                [
                    'class' => DropDownSearchDataColumn::class,
                    'attribute' => 'company_registration_page_type',
                    'label' => 'Страница регистрации',
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                        'width' => '10%',
                    ],
                    'filter' => $searchModel->getRegistrationPageTypeFilter(),
                    'format' => 'raw',
                    'value' => function (UrotdelVisit $model) {
                        return $model->company->registrationPageType ? $model->company->registrationPageType->name : '';
                    }
                ],
                [
                    'class' => DropDownSearchDataColumn::class,
                    'attribute' => 'company_utm_source',
                    'label' => 'Источник',
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                        'width' => '10%',
                    ],
                    'filter' => $searchModel->getUtmSourceFilter(),
                    'value' => function (UrotdelVisit $model) {
                        return $model->company->utm_source;
                    },
                ],
                [
                    'attribute' => 'company.inn',
                    'label' => 'ИНН',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (UrotdelVisit $model) {
                        return $model->company->inn;
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата посещения',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (UrotdelVisit $model) {
                        return date(DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS, $model->created_at);
                    },
                ],
                [
                    'attribute' => 'duration',
                    'label' => 'Длительность',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (UrotdelVisit $model) {
                        return "{$model->duration} секунд";
                    },
                ],
            ],
        ]); ?>
    </div>
</div>