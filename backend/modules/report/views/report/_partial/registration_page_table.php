<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.12.2018
 * Time: 14:05
 */

use common\models\company\RegistrationPageType;
use backend\modules\report\models\SegmentationSearch;

/* @var $registrationPage RegistrationPageType */
$totalCompanyCount = SegmentationSearch::getCompaniesByRegistrationPage(RegistrationPageType::find()->column());
?>
<div class="col-md-12">
    <table class="table table-striped table-bordered table-hover">
        <tbody>
        <tr role="row">
            <td class="text-left" width="40%">
                <b>Пришли с стр.</b>
            </td>
            <td class="text-left" width="20%">
                <b>Кол-во</b>
            </td>
            <td class="text-left" width="20%">
                <b>%%</b>
            </td>
            <td class="text-left" width="20%">
                <b>Оплат</b>
            </td>
        </tr>
        <?php foreach (RegistrationPageType::find()->all() as $registrationPage): ?>
            <?php $count = SegmentationSearch::getCompaniesByRegistrationPage($registrationPage->id); ?>
            <tr>
                <td>
                    <?= $registrationPage->name; ?>
                </td>
                <td>
                    <?= $count; ?>
                </td>
                <td>
                    <?= $totalCompanyCount ? round(($count * 100) / $totalCompanyCount, 2) : 0; ?>
                </td>
                <td>
                    <?= SegmentationSearch::getFirstPayByRegistrationPage($registrationPage->id); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <tr role="row">
            <td><b>Итого</b></td>
            <td><b><?= $totalCompanyCount; ?></b></td>
            <td><b><?= $totalCompanyCount ? 100 : 0; ?></b></td>
            <td><b><?= SegmentationSearch::getFirstPayByRegistrationPage(RegistrationPageType::find()->column()); ?></b></td>
        </tr>
        </tbody>
    </table>
</div>