<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.05.2017
 * Time: 11:18
 */

use backend\modules\report\models\SegmentationSearch;

/* @var $this yii\web\View
 * @var $involvedActivities []
 * @var $companiesCountWithActivities integer
 * @var $invoicesCountWithActivities integer
 * @var $subscribeCountWithActivities integer
 * @var $payedCompaniesCountWithActivities integer
 * @var $searchModel SegmentationSearch
 */
?>

<table class="table table-striped table-bordered table-hover">
    <thead>
    </thead>
    <tbody>
    <tr role="row">
        <td class="text-left" width="10%">
            <b>Вид деятельности</b>
        </td>
        <td class="text-right" width="10%">
            <b>Кол-во</b>
        </td>
        <td class="text-right" width="10%">
            <b>%%</b>
        </td>
        <td class="text-right" width="10%">
            <b>Кол-во Исх Счетов</b>
        </td>
        <td class="text-right" width="10%">
            <b>%%</b>
        </td>
        <td class="text-right" width="10%">
            <b>Кол-во оплат</b>
        </td>
        <td class="text-right" width="10%">
            <b>%%</b>
        </td>
        <td class="text-right" width="10%">
            <b>Кол-во оплативших компаний</b>
        </td>
        <td class="text-right" width="10%">
            <b>%% оплативших</b>
        </td>
        <td class="text-right" width="10%">
            <b>Кол-во ИСХ счетов на компанию</b>
        </td>
    </tr>
    <?php foreach ($involvedActivities as $activities): ?>
        <tr role="row">
            <td class="text-left" width="10%">
                <?= $activities['activities']['name']; ?>
            </td>
            <td class="text-right" width="10%">
                <?= $activities['count_activities_companies']; ?>
            </td>
            <td class="text-right" width="10%">
                <?= $companiesCountWithActivities ? round(($activities['count_activities_companies'] * 100) / $companiesCountWithActivities, 2) : 0; ?>
            </td>
            <td class="text-right" width="10%">
                <?= $activities['invoice_count']; ?>
            </td>
            <td class="text-right" width="10%">
                <?= $invoicesCountWithActivities ? round(($activities['invoice_count'] * 100) / $invoicesCountWithActivities, 2) : 0; ?>
            </td>
            <td class="text-right" width="10%">
                <?= $activities['subscribe_count']; ?>
            </td>
            <td class="text-right" width="10%">
                <?= $subscribeCountWithActivities ? round(($activities['subscribe_count'] * 100) / $subscribeCountWithActivities, 2) : 0; ?>
            </td>
            <td class="text-right" width="10%">
                <?= $searchModel->getPayedCompaniesCountByActivities($activities['activities']['id']); ?>
            </td>
            <td class="text-right" width="10%">
                <?= $activities['count_activities_companies'] ? round(($searchModel->getPayedCompaniesCountByActivities($activities['activities']['id']) / $activities['count_activities_companies']) * 100, 2) : 0; ?>
            </td>
            <td class="text-right" width="10%">
                <?= $activities['count_activities_companies'] ? round($activities['invoice_count'] / $activities['count_activities_companies'], 2) : 0; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <tr role="row">
        <td class="text-left" width="10%">
            <b>Итого</b>
        </td>
        <td class="text-right" width="10%">
            <b><?= $companiesCountWithActivities; ?></b>
        </td>
        <td class="text-right" width="10%">
            <b><?= $companiesCountWithActivities > 0 ? 100 : 0; ?></b>
        </td>
        <td class="text-right" width="10%">
            <b><?= $invoicesCountWithActivities; ?></b>
        </td>
        <td class="text-right" width="10%">
            <b><?= $invoicesCountWithActivities > 0 ? 100 : 0; ?></b>
        </td>
        <td class="text-right" width="10%">
            <b><?= $subscribeCountWithActivities; ?></b>
        </td>
        <td class="text-right" width="10%">
            <b><?= $subscribeCountWithActivities > 0 ? 100 : 0; ?></b>
        </td>
        <td class="text-right" width="10%">
            <b><?= $payedCompaniesCountWithActivities; ?></b>
        </td>
        <td class="text-right" width="10%">
            <b><?= $companiesCountWithActivities ? round($payedCompaniesCountWithActivities / $companiesCountWithActivities * 100, 2) : 0; ?></b>
        </td>
        <td class="text-right" width="10%">
            <b><?= $companiesCountWithActivities ? round($invoicesCountWithActivities / $companiesCountWithActivities, 2) : 0; ?></b>
        </td>
    </tr>
    </tbody>
</table>
