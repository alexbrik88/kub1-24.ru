<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.03.2018
 * Time: 7:33
 */

use common\models\Company;
use frontend\widgets\RangeButtonWidget;

/* @var $afterRegistrationData []
 */
?>
<div class="col-md-12">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        </thead>
        <tbody>
        <tr role="row">
            <td class="text-left" width="60%">
                <b>Пришел за...</b>
            </td>
            <td class="text-left" width="20%">
                <b>Кол-во</b>
            </td>
            <td class="text-left" width="20%">
                <b>%%</b>
            </td>
        </tr>
        <?php foreach (Company::$afterRegistrationBlock as $afterRegistrationBlockType => $afterRegistrationBlockName): ?>
            <?php if (!$afterRegistrationBlockType) continue; ?>
            <tr role="row">
                <td><?= $afterRegistrationBlockName; ?></td>
                <td><?= $afterRegistrationData[$afterRegistrationBlockType]; ?></td>
                <td><?= $afterRegistrationData['all'] ?
                        round($afterRegistrationData[$afterRegistrationBlockType] / $afterRegistrationData['all'] * 100, 2) : 0; ?></td>
            </tr>
        <?php endforeach; ?>
        <tr role="row">
            <td><b>Итого</b></td>
            <td><b><?= $afterRegistrationData['all']; ?></b></td>
            <td><b>100</b></td>
        </tr>
        </tbody>
    </table>
</div>
