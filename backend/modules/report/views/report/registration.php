<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2017
 * Time: 5:16
 */

use yii\bootstrap\Html;
use backend\modules\report\models\ReportSearch;
use yii\helpers\Url;

/* @var $searchModel ReportSearch */
/* @var $this yii\web\View */
/* @var $period [] */
/* @var $type integer */

$this->title = 'Отчеты';
$registrationPercents = $searchModel->getRegistrationPercents($period, $type);
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <?php if ($type == ReportSearch::DAY): ?>
                        <span class="glyphicon glyphicon-chevron-left change-period"
                              style="cursor: pointer;" data-id="minus"></span>
                        <span
                            class="glyphicon glyphicon-chevron-right change-period"
                            style="cursor: pointer;" data-id="plus"></span>
                        <?= Html::beginForm(Url::to(['registration']), 'GET', [
                            'id' => 'list-date',
                        ]); ?>
                        <?= Html::activeHiddenInput($searchModel, 'periodDayList'); ?>
                        <?= Html::endForm(); ?>
                    <?php endif; ?>
                </div>
                <div class="actions">
                    <?= Html::beginForm(Url::to(['registration']), 'GET', [
                        'id' => 'search-report',
                    ]); ?>
                    <div class="btn-group btn-group-devided"
                         data-toggle="buttons">
                        <?= Html::activeRadioList($searchModel, 'periodType', [
                            ReportSearch::DAY => 'День',
                            ReportSearch::WEEK => 'Неделя',
                            ReportSearch::MONTH => 'Месяц',
                        ], [
                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                    $class = $searchModel->periodType == $value ? 'active' : '';
                                    $return = '<label class="btn btn-transparent green btn-outline btn-circle btn-sm ' . $class . '">';
                                    $return .= '<input type="radio" name="ReportSearch[periodType]" class="toggle hidden" value="' . $value . '">';
                                    $return .= $label;
                                    $return .= '</label>';

                                    return $return;
                                },
                            ]
                        ); ?>
                    </div>
                    <?= Html::endForm();; ?>
                </div>
            </div>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => '#dfba49',
                            'name' => 'Регистрации',
                            'pointWidth' => 10,
                            'data' => $searchModel->getAllRegistrations($period, $type),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Заполнен профиль',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $searchModel->getAllCompleteProfile($period, $type),

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Выставлен счет',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $searchModel->getAllExhibitedInvoices($period, $type),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Отправлен счет по e-mail',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getAllSendingInvoices($period, $type),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Загружен (лого, печать, подпись)',
                            //'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getAllUploadedFiles($period, $type, true),
                        ],
                    ],
                ],
            ]); ?>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => '#45b6af',
                            'name' => 'Коэффициент Заполнен профиль',
                            'pointWidth' => 10,
                            'data' => $registrationPercents['completeProfile'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Коэффициент Выставлен счет',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $registrationPercents['exhibitedInvoices'],

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Коэффициент Отправлен счет по e-mail',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $registrationPercents['sendingInvoices'],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>