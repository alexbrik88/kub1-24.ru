<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2017
 * Time: 5:16
 */

use yii\bootstrap\Html;
use backend\modules\report\models\ReportSearch;
use yii\helpers\Url;

/* @var $searchModel ReportSearch */
/* @var $this yii\web\View */
/* @var $period [] */
/* @var $type integer */

$this->title = 'Отчеты';
$registrationPercents = $searchModel->getRegistrationPercents($period, $type);
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <?php if ($type == ReportSearch::DAY): ?>
                        <span class="glyphicon glyphicon-chevron-left change-period"
                              style="cursor: pointer;" data-id="minus"></span>
                        <span
                            class="glyphicon glyphicon-chevron-right change-period"
                            style="cursor: pointer;" data-id="plus"></span>
                        <?= Html::beginForm(Url::to(['robot']), 'GET', [
                            'id' => 'list-date',
                        ]); ?>
                        <?= Html::activeHiddenInput($searchModel, 'periodDayList'); ?>
                        <?= Html::endForm(); ?>
                    <?php endif; ?>
                </div>
                <div class="actions">
                    <?= Html::beginForm(Url::to(['robot']), 'GET', [
                        'id' => 'search-report',
                    ]); ?>
                    <div class="btn-group btn-group-devided"
                         data-toggle="buttons">
                        <?= Html::activeRadioList($searchModel, 'periodType', [
                            ReportSearch::DAY => 'День',
                            ReportSearch::WEEK => 'Неделя',
                            ReportSearch::MONTH => 'Месяц',
                        ], [
                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                    $class = $searchModel->periodType == $value ? 'active' : '';
                                    $return = '<label class="btn btn-transparent green btn-outline btn-circle btn-sm ' . $class . '">';
                                    $return .= '<input type="radio" name="ReportSearch[periodType]" class="toggle hidden" value="' . $value . '">';
                                    $return .= $label;
                                    $return .= '</label>';

                                    return $return;
                                },
                            ]
                        ); ?>
                    </div>
                    <?= Html::endForm();; ?>
                </div>
            </div>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => '#dfba49',
                            'name' => 'Кол-во всего открывших модуль Робота-бухгалтера',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'all', 84),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во новых по рекламе',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'new', 84),

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во текущих пользователей',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'old', 84),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Кол-во из попапа «Уменьшим Ваши налоги»',
                            'color' => '#39E444',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'old', 99),
                        ],
                    ],
                ],
            ]); ?>
            <h4 style="font-weight: bold">Новые пользователи по рекламе</h4>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'name' => 'Кол-во новых по рекламе',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'new', 84),

                        ],
                        [
                            'type' => 'column',
                            'color' => '#778899',
                            'name' => 'Заполнены данные на шаге 1',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'new', 85),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Просмотрен шаг 3',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'new', 86),

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Загрузил выписку',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'new', 97),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Робот-бухгалтер оплачен',
                            'color' => '#dfba49',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'new', 88),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Скачал нулевую декларацию',
                            'color' => 'green',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'new', 90),

                        ],
                    ],
                ],
            ]); ?>
            <h4 style="font-weight: bold">Текущие пользователи</h4>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'name' => 'Кол-во текущих пользователей',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'old', 84),
                        ],
                        [
                            'type' => 'column',
                            'color' => '#778899',
                            'name' => 'Заполнены данные на шаге 1',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'old', 85),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Просмотрен шаг 3',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'old', 86),

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Загрузил выписку',
                            'color' => 'blue',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'old', 97),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Робот-бухгалтер оплачен',
                            'color' => '#dfba49',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'old', 88),
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Скачал нулевую декларацию',
                            'color' => 'green',
                            'pointWidth' => 10,
                            'data' => $searchModel->getRobotViews($period, $type, 'old', 90),

                        ],
                    ],
                ],
            ]); ?>            
        </div>
    </div>
</div>