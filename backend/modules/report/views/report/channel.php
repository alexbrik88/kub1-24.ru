<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.03.2019
 * Time: 23:25
 */

use backend\modules\report\models\ReportSearch;
use common\components\helpers\Html;
use yii\helpers\Url;
use \miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $searchModel ReportSearch */
/* @var $period [] */
/* @var $type integer */

$this->title = 'Отчеты';
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <?php if ($type == ReportSearch::DAY): ?>
                        <span class="glyphicon glyphicon-chevron-left change-period"
                              style="cursor: pointer;" data-id="minus"></span>
                        <span
                                class="glyphicon glyphicon-chevron-right change-period"
                                style="cursor: pointer;" data-id="plus"></span>
                        <?= Html::beginForm(Url::to(['channel']), 'GET', [
                            'id' => 'list-date',
                        ]); ?>
                        <?= Html::activeHiddenInput($searchModel, 'periodDayList'); ?>
                        <?= Html::endForm(); ?>
                    <?php endif; ?>
                </div>
                <div class="actions">
                    <?= Html::beginForm(Url::to(['channel']), 'GET', [
                        'id' => 'search-report',
                    ]); ?>
                    <div class="btn-group btn-group-devided"
                         data-toggle="buttons">
                        <?= Html::activeRadioList($searchModel, 'periodType', [
                            ReportSearch::DAY => 'День',
                            ReportSearch::WEEK => 'Неделя',
                            ReportSearch::MONTH => 'Месяц',
                        ], [
                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                    $class = $searchModel->periodType == $value ? 'active' : '';
                                    $return = '<label class="btn btn-transparent green btn-outline btn-circle btn-sm ' . $class . '">';
                                    $return .= '<input type="radio" name="ReportSearch[periodType]" class="toggle hidden" value="' . $value . '">';
                                    $return .= $label;
                                    $return .= '</label>';

                                    return $return;
                                },
                            ]
                        ); ?>
                    </div>
                    <?= Html::endForm();; ?>
                </div>
            </div>
            <?= Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Регистраций'],
                    ],
                    'series' => $searchModel->getRegistrationsByChannelSeries($period, $type),
                ],
            ]); ?>
            <?= Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Заполнили профиль'],
                    ],
                    'series' => $searchModel->getCompleteProfileByChannelSeries($period, $type),
                ],
            ]); ?>
            <?= Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Выставили счет'],
                    ],
                    'series' => $searchModel->getExhibitedInvoicesByChannelSeries($period, $type),
                ],
            ]); ?>
            <?= Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Заполнил профиль/Зарегистрировался'],
                    ],
                    'tooltip' => [
                        'valueSuffix' => '%',
                    ],
                    'series' => $searchModel->getChannelPercent($period, $type),
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php $this->registerJs('
    $(".highcharts-legend-item.highcharts-column-series").click(function (e) {
        if (e.isTrigger == undefined) {
            var $class = $(this).attr("class");
            
            $class = $class.replace(/ /g, ".");
            $class = $class.replace(".highcharts-legend-item-hidden", "");
            if ($(this).hasClass("highcharts-legend-item-hidden")) {
                $("." + $class + ":not(.highcharts-legend-item-hidden)").click();
            } else {
                $("." + $class + "highcharts-legend-item-hidden").click();
            }
        }
    });
'); ?>
