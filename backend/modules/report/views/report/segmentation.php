<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.05.2017
 * Time: 4:49
 */

use frontend\widgets\RangeButtonWidget;
use yii\data\ActiveDataProvider;
use backend\modules\report\models\SegmentationSearch;
use common\components\grid\GridView;
use common\models\Company;
use common\components\date\DateHelper;
use common\models\product\Product;
use yii\helpers\Html;
use common\models\company\Activities;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\components\grid\DropDownSearchDataColumn;
use kartik\select2\Select2;
use Cake\Utility\Text;

/* @var $this yii\web\View
 * @var $involvedActivities []
 * @var $companiesCountWithActivities integer
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel SegmentationSearch
 * @var $comID integer|null
 * @var $invoicesCountWithActivities integer
 * @var $subscribeCountWithActivities integer
 * @var $payedCompaniesCountWithActivities integer
 * @var $afterRegistrationData []
 * @var $activeTab integer
 */
$actionId = Yii::$app->controller->action->id;

$this->title = 'Сегментация';
?>
<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <div class="col-md-9 col-md-pull-3">
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-md-3 col-lg-4" style="width: auto; float: right;">
        <div class="portlet-body employee-wrapper">
            <div class="tabbable tabbable-tabdrop">
                <ul class="nav nav-tabs li-border float-r segmentation-tabs">
                    <li class="<?= $activeTab == SegmentationSearch::TAB_ACTIVITIES ? 'active' : null; ?>">
                        <?= Html::a('Вид деятельности', Url::to(['segmentation', 'activeTab' => SegmentationSearch::TAB_ACTIVITIES]), [
                            'aria-expanded' => 'false',
                            'style' => 'margin-right: 0;',
                        ]); ?>
                    </li>
                    <li class="<?= $activeTab == SegmentationSearch::TAB_REGISTRATION_PAGE ? 'active' : null; ?>">
                        <?= Html::a('Пришли с стр.', Url::to(['segmentation', 'activeTab' => SegmentationSearch::TAB_REGISTRATION_PAGE]), [
                            'aria-expanded' => 'false',
                            'style' => 'margin-right: 0;',
                        ]); ?>
                    </li>
                    <li class="<?= $activeTab == SegmentationSearch::TAB_WHY ? 'active' : null; ?>"
                        style="margin-right: 0;float: right;">
                        <?= Html::a('Пришел за...', Url::to(['segmentation', 'activeTab' => SegmentationSearch::TAB_WHY]), [
                            'aria-expanded' => 'false',
                            'style' => 'margin-right: 0;',
                        ]); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="tab-content">
    <div class="row tab-pane fade in active">
        <?php if ($activeTab == SegmentationSearch::TAB_WHY): ?>
            <?= $this->render('_partial/after_registration_table', [
                'afterRegistrationData' => $afterRegistrationData,
            ]); ?>
        <?php elseif ($activeTab == SegmentationSearch::TAB_REGISTRATION_PAGE): ?>
            <?= $this->render('_partial/registration_page_table'); ?>
        <?php elseif ($activeTab == SegmentationSearch::TAB_ACTIVITIES): ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $this->render('_partial/segmentation_table_header', [
                        'involvedActivities' => $involvedActivities,
                        'companiesCountWithActivities' => $companiesCountWithActivities,
                        'invoicesCountWithActivities' => $invoicesCountWithActivities,
                        'subscribeCountWithActivities' => $subscribeCountWithActivities,
                        'payedCompaniesCountWithActivities' => $payedCompaniesCountWithActivities,
                        'searchModel' => $searchModel,
                    ]); ?>
                </div>
            </div>
            <div class="portlet box darkblue blk_wth_srch">
                <div class="portlet-title row-fluid">
                    <div class="caption list_recip col-md-3 col-sm-3">
                        Список компаний <?= $dataProvider->query->count(); ?>
                    </div>
                </div>
                <div class="portlet-body accounts-list">
                    <div class="table-container" style="">
                        <?= GridView::widget([
                            'id' => 'segmentation-grid',
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered table-hover dataTable status_nowrap invoice-table',
                                'aria-describedby' => 'datatable_ajax_info',
                                'role' => 'grid',
                            ],

                            'headerRowOptions' => [
                                'class' => 'heading',
                            ],

                            'options' => [
                                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                            ],
                            'pager' => [
                                'options' => [
                                    'class' => 'pagination pull-right',
                                ],
                            ],
                            'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                            'columns' => [
                                [
                                    'attribute' => 'name_short',
                                    'label' => 'Название компании',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Company $model) use ($comID) {
                                        $anchor = null;
                                        if ($model->id == $comID) {
                                            $anchor = '<a name="anchor"></a>';
                                        }
                                        return $anchor . Html::a($model->name_short, Url::to(['/company/company/view', 'id' => $model->id]));
                                    },
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'label' => 'Дата регистрации',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Company $model) {
                                        return date(DateHelper::FORMAT_USER_DATE, $model->created_at);
                                    },
                                ],
                                [
                                    'attribute' => 'email',
                                    'label' => 'E-mail',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                        'width' => '20%',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'text-ellipsis',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Company $model) {
                                        return $model->email;
                                    },
                                ],
                                [
                                    'label' => 'Название услуг',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Company $model) {
                                        return '<span title="' . $model->getFirstTenProducts(Product::PRODUCTION_TYPE_SERVICE) . '">' . Text::truncate($model->getFirstTenProducts(Product::PRODUCTION_TYPE_SERVICE), 45, [
                                                'ellipsis' => '...',
                                                'exact' => true,
                                            ]) . '</span>';
                                    },
                                ],
                                [
                                    'label' => 'Название товаров',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'contentOptions' => [
                                        'style' => 'white-space: inherit;',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Company $model) {
                                        return '<span title="' . $model->getFirstTenProducts(Product::PRODUCTION_TYPE_GOODS) . '">' . Text::truncate($model->getFirstTenProducts(Product::PRODUCTION_TYPE_GOODS), 45, [
                                                'ellipsis' => '...',
                                                'exact' => true,
                                            ]) . '</span>';
                                    },
                                ],
                                [
                                    'attribute' => 'activities_id',
                                    'label' => 'Вид деятельности',
                                    'class' => DropDownSearchDataColumn::className(),
                                    'headerOptions' => [
                                        'class' => 'dropdown-filter',
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                    'filter' => $searchModel->getActivitiesFilter(),
                                    'value' => function (Company $model) use ($comID) {
                                        return Select2::widget([
                                            'model' => $model,
                                            'attribute' => 'activities_id',
                                            'data' => ArrayHelper::merge([null => '---'], ArrayHelper::map(Activities::find()->orderBy('name')->all(), 'id', 'name')),
                                            'options' => [
                                                'id' => "company{$model->id}-activities_id",
                                                'class' => 'form-control activities-drop-down',
                                                'data-url' => Url::to(['/company/company/change-activities', 'id' => $model->id, 'comID' => $comID]),
                                            ],
                                        ]);
                                    },
                                ],
                                [
                                    'attribute' => 'invoice_count',
                                    'label' => 'Исх. Счет',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'act_count',
                                    'label' => 'Исх. Акт',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'packing_list_count',
                                    'label' => 'Исх. ТН',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'invoice_facture_count',
                                    'label' => 'Исх. СФ',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'product_count',
                                    'label' => 'Товаров',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'employee_count',
                                    'label' => 'Сотрудников',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'store_count',
                                    'label' => 'Кабинет',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'out_invoice_count',
                                    'label' => 'Виджет счета',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'subscribe_count',
                                    'label' => 'Количество оплат',
                                    'headerOptions' => [
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>