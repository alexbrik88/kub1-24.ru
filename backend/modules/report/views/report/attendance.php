<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.02.2017
 * Time: 16:14
 */

use common\components\grid\GridView;
use yii\data\ActiveDataProvider;
use common\models\company\Attendance;
use common\components\date\DateHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\report\models\AttendanceSearch;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridViewHelper;
use yii\helpers\StringHelper;

/* @var $dataProvider ActiveDataProvider */
/* @var $searchModel AttendanceSearch */

?>
<div class="portlet-body accounts-list">
    <div class="table-container" style="">
        <div
            class="dataTables_wrapper dataTables_extended_wrapper">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table overfl_text_hid',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n" .
                    '<ul class="pagination pull-left pageSize">
                            <li><span class="no-style">Выводить по количеству строк:</span></li>' .
                    GridViewHelper::getPagination(StringHelper::basename(get_class($searchModel)), $searchModel->pageSize, ['attendance']) .
                    '</ul>' . "{pager}",

                'columns' => [
                    [
                        'attribute' => 'date',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'value' => function (Attendance $model) {
                            return DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                        }
                    ],
                    [
                        'attribute' => 'company_id',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '80%',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getCompanyFilter(),
                        'value' => function (Attendance $model) {
                            return Html::a($model->company->getShortName(), Url::to(['/company/company/view', 'id' => $model->company->id]));
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
