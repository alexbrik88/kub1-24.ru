<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.02.2017
 * Time: 3:49
 */

use backend\modules\report\models\ReportSearch;
use yii\bootstrap\Html;
use yii\helpers\Url;
use common\models\Company;

/* @var $searchModel ReportSearch */
/* @var $this yii\web\View */
/* @var $period [] */
/* @var $type integer */

$this->title = 'Отчеты';
$activationEmptyProfile = $searchModel->getActivationEmptyProfileChartInfo($period, $type, Company::ACTIVATION_EMPTY_PROFILE);
$activationNoInvoice = $searchModel->getActivationEmptyProfileChartInfo($period, $type, Company::ACTIVATION_NO_INVOICE);
$activationNotSendInvoice = $searchModel->getActivationEmptyProfileChartInfo($period, $type, Company::ACTIVATION_NOT_SEND_INVOICES);
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <?php if ($type == ReportSearch::DAY): ?>
                        <span class="glyphicon glyphicon-chevron-left change-period"
                              style="cursor: pointer;" data-id="minus"></span>
                        <span
                            class="glyphicon glyphicon-chevron-right change-period"
                            style="cursor: pointer;" data-id="plus"></span>
                        <?= Html::beginForm(Url::to(['activation']), 'GET', [
                            'id' => 'list-date',
                        ]); ?>
                        <?= Html::activeHiddenInput($searchModel, 'periodDayList'); ?>
                        <?= Html::endForm(); ?>
                    <?php endif; ?>
                </div>
                <div class="actions">
                    <?= Html::beginForm(Url::to(['activation']), 'GET', [
                        'id' => 'search-report',
                    ]); ?>
                    <div class="btn-group btn-group-devided"
                         data-toggle="buttons">
                        <?= Html::activeRadioList($searchModel, 'periodType', [
                            ReportSearch::DAY => 'День',
                            ReportSearch::WEEK => 'Неделя',
                            ReportSearch::MONTH => 'Месяц',
                        ], [
                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                    $class = $searchModel->periodType == $value ? 'active' : '';
                                    $return = '<label class="btn btn-transparent green btn-outline btn-circle btn-sm ' . $class . '">';
                                    $return .= '<input type="radio" name="ReportSearch[periodType]" class="toggle hidden" value="' . $value . '">';
                                    $return .= $label;
                                    $return .= '</label>';

                                    return $return;
                                },
                            ]
                        ); ?>
                    </div>
                    <?= Html::endForm(); ?>
                </div>
            </div>
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => 'yellow',
                            'name' => 'Зашел повторно',
                            'pointWidth' => 10,
                            'data' => $activationEmptyProfile['attendanceCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'color' => 'green',
                            'name' => 'Заполнил профиль',
                            'pointWidth' => 10,
                            'data' => $activationEmptyProfile['completeProfileCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Выставлен счет',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $activationEmptyProfile['createInvoiceCompanyCount'],

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Отправлен счет по e-mail',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $activationEmptyProfile['sendInvoiceCompanyCount'],
                        ],
                    ],
                ],
            ]); ?>

            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => 'yellow',
                            'name' => 'Зашел повторно',
                            'pointWidth' => 10,
                            'data' => $activationNoInvoice['attendanceCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Выставлен счет',
                            'color' => '#45b6af',
                            'pointWidth' => 10,
                            'data' => $activationNoInvoice['createInvoiceCompanyCount'],

                        ],
                        [
                            'type' => 'column',
                            'name' => 'Отправлен счет по e-mail',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $activationNoInvoice['sendInvoiceCompanyCount'],
                        ],
                    ],
                ],
            ]); ?>

            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $searchModel->getXAxisTitle($period, $type),
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Колличество']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => 'yellow',
                            'name' => 'Зашел повторно',
                            'pointWidth' => 10,
                            'data' => $activationNotSendInvoice['attendanceCompanyCount'],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Отправлен счет по e-mail',
                            'color' => '#f3565d',
                            'pointWidth' => 10,
                            'data' => $activationNotSendInvoice['sendInvoiceCompanyCount'],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>