<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.09.2018
 * Time: 10:25
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\RangeButtonWidget;
use yii\data\ArrayDataProvider;
use backend\modules\report\models\RepeatStateSearch;
use common\components\grid\GridView;
use common\components\grid\GridViewHelper;
use yii\helpers\StringHelper;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $dataProvider ArrayDataProvider
 * @var $searchModel RepeatStateSearch
 */
$this->title = 'Повторные оплаты';
$columns[] = [
    'attribute' => 'company_name',
    'label' => 'Компания',
    'headerOptions' => [
        'width' => '5%',
    ],
    'format' => 'raw',
    'value' => function ($data) {
        return Html::a($data['company_name'], Url::to(['/company/company/view', 'id' => $data['company_id']]));
    }
];
$columns[] = [
    'attribute' => 'registration_page_type',
    'label' => 'Странциа регистрации',
    'headerOptions' => [
        'width' => '5%',
        'class' => 'sorting',
    ],
    'format' => 'raw',
    'value' => function ($data) {
        return $data['registration_page_type'] ?: '';
    }
];
$maxPayments = $searchModel->getMaxPayments();
if ($maxPayments) {
    foreach (range(1, $maxPayments) as $paymentNumber) {
        $attribute = 'payment' . $paymentNumber;
        $columns[] = [
            'attribute' => $attribute,
            'label' => $paymentNumber . ' пл',
            'headerOptions' => [
                'width' => '5%',
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function ($data) use ($attribute) {
                return $data[$attribute] ? $data[$attribute] : '';
            }
        ];
    }
}
$columns[] = [
    'attribute' => 'averagePayment',
    'label' => 'Среднее 2-N',
    'headerOptions' => [
        'width' => '5%',
        'class' => 'sorting',
    ],
    'format' => 'raw',
    'value' => function ($data) {
        return $data['averagePayment'] ? $data['averagePayment'] : '';
    }
];
$averageColumns = '';
$headerTableRows = null;
if ($maxPayments) {
    $i = 0;
    $totalPaymentCount = 0;
    $totalPaymentAmount = 0;
    $averageColumns = '<tr>';
    $averageColumns .= '<td class="text-right">Среднее</td>';
    foreach ($searchModel->averageByPayment as $key => $item) {
        $i++;
        $value = $item['count'] ? round($item['diff'] / $item['count']) : '';
        $averageColumns .= "<td>{$value}</td>";
        $averagePaymentAmount = $item['count'] ? round($item['amount'] / $item['count']) : '';

        if ($key !== 'averagePayment') {
            $totalPaymentCount += $item['count'];
            $totalPaymentAmount += $item['amount'];

            $headerTableRows .= "<tr>";
            $headerTableRows .= "<td>Платеж №{$i}</td>";
            $headerTableRows .= "<td>{$item['count']}</td>";
            $headerTableRows .= "<td>{$value}</td>";
            $headerTableRows .= "<td>".TextHelper::numberFormat($item['amount'])."</td>";
            $headerTableRows .= "<td>".TextHelper::numberFormat($averagePaymentAmount)."</td>";
            $headerTableRows .= "</tr>";
        }
    }
    $averageColumns .= '</tr>';

    $headerTableRows .= "<tr>";
    $headerTableRows .= "<td class='bold'>Итого</td>";
    $headerTableRows .= "<td class='bold'>$totalPaymentCount</td>";
    $headerTableRows .= "<td></td>";
    $headerTableRows .= "<td class='bold'>$totalPaymentAmount</td>";
    $headerTableRows .= "<td></td>";
    $headerTableRows .= "</tr>";
}
?>
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="portlet-body employee-wrapper">
                <div class="tabbable tabbable-tabdrop">
                    <ul class="nav nav-tabs li-border float-r" style="float: right;">
                        <li>
                            <?= Html::a('Первые оплаты', Url::to(['/report/report/state']), ['aria-expanded' => 'false',]); ?>
                        </li>
                        <li class="active">
                            <?= Html::a('Повторные оплаты', Url::to(['/report/report/repeat-state']), ['aria-expanded' => 'false',]); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-push-9" style="margin-bottom: 5px;">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
        <div class="col-md-9 col-md-pull-3 created-scroll">
            <table class="table table-striped table-bordered table-hover">
                <tbody>
                <tr class="heading">
                    <th>№№ Платежа</th>
                    <th>Кол-во оплат</th>
                    <th>Сред. кол-во дн.</th>
                    <th>Сумма платежей</th>
                    <th>Средняя сумма</th>
                </tr>
                <?= $headerTableRows; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="portlet box darkblue blk_wth_srch">
        <div class="portlet-title row-fluid">
            <div class="caption list_recip col-md-8">
                <?= Html::encode($this->title); ?>
            </div>
        </div>
        <div class="portlet-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table repeat-state-table',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => "<div class='scroll-table-wrapper'>
                {items}
                <table class='hidden average-repeat-state-table'>
                    {$averageColumns}
                </table>
                </div>\n" .
                    '<ul class="pagination pull-left pageSize">
                            <li><span class="no-style">Выводить по количеству строк:</span></li>' .
                    GridViewHelper::getPagination(StringHelper::basename(get_class($searchModel)), $searchModel->pageSize, ['repeat-state']) .
                    '</ul>' . "{pager}",

                'columns' => $columns,
            ]); ?>
        </div>
    </div>
<?php $this->registerJs('
    $(document).ready(function () {
        $averageTr = $(".average-repeat-state-table tr").clone();
        $(".repeat-state-table tbody").append($averageTr);
    });
'); ?>