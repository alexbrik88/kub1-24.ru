<?php

$this->beginContent('@backend/views/layouts/main.php');
?>
<div class="row">
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>
