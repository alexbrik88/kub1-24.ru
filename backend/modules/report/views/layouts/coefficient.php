<?php

use common\models\service\SubscribeTariffGroup;
use yii\helpers\Html;
use yii\helpers\Url;


$actionId = Yii::$app->controller->action->id;
$groups = SubscribeTariffGroup::find()->active()->select('name')->indexBy('id')->orderBy([
    'priority' => SORT_ASC,
])->column();
$groupId = Yii::$app->getRequest()->get('group_id');

$this->beginContent('@backend/views/layouts/main.php');
?>

<style>
    .li-border .dropdown.open > .dropdown-toggle {
        background: none;
        color: #0d638f;
        border-color: transparent;
    }

    .li-border .dropdown.open > .dropdown-toggle:hover {
        color: #fff;
    }

    .pad-car .dropdown-menu:before {
        position: absolute;
        top: -7px;
        right: 14px;
        left: auto;
        display: inline-block;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #ccc;
        border-left: 7px solid transparent;
        border-bottom-color: rgba(0, 0, 0, 0.2);
        content: '';
    }

    .li-border > li {
        border: 1px solid #ddd;
        border-bottom-color: transparent;
        margin-right: 5px;
    }

    .li-border > li > a:hover {
        border: 1px solid #4276a4;
        cursor: pointer;
        background: #4276a4;
        color: #ffffff;
    }

    .li-border > li:hover {
        background: #4276a4
    }

    .li-border > .active > a {
        color: #ffffff !important;
        cursor: pointer !important;
        background-color: #4276a4 !important;
        border: none !important;
        padding: 12px 15px !important;
    }

    .li-border > li {
        border: 1px solid #ddd;
        border-bottom-color: transparent;
        margin-right: 5px;
    }

    .li-border > .active {
        border: none !important;
    }

    .li-border > .active > a:hover {
        color: #ffffff !important;
        cursor: pointer !important;
        background-color: #4276a4 !important;
        border: none !important;
    }
    .dropdown > .dropdown-menu:before, .dropdown-toggle > .dropdown-menu:before, .btn-group > .dropdown-menu:before {
        left: 144px;
    }

</style>

<div class="row">
    <div class="col-xs-12 col-md-4 col-lg-3" style="height: 45px;
        padding: 6px 0 10px 32px;
        color: #666;
        font-size: 28px;
        font-weight: 300;
        font-family: 'Open Sans', sans-serif;
        line-height: 28px;
    ">
        Коэффициенты
    </div>

    <div class="col-xs-12 col-md-8 col-lg-9">
        <div class="portlet-body employee-wrapper">
            <div class="tabbable tabbable-tabdrop">
                <ul class="nav nav-tabs li-border float-r">
                    <li class="dropdown pull-right tabdrop pad-car hide">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu"></ul>
                    </li>
                    <?php foreach ($groups as $key => $value) : ?>
                        <li class="<?= $groupId == $key ? 'active' : '' ?>">
                            <?= Html::a($value, Url::to(['/report/coefficient/by-group', 'group_id' => $key]), ['aria-expanded' => 'false',]); ?>
                        </li>
                    <?php endforeach ?>
                    <li class="<?= $actionId == 'totals' ? 'active' : '' ?>">
                        <?= Html::a('Свод', Url::to(['/report/coefficient/totals']), ['aria-expanded' => 'false',]); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 debt-report-content">
        <?= $content; ?>
    </div>
</div>

<?php $this->endContent(); ?>