<?php
/**
 * @var $searchModel \backend\modules\report\models\CoefficientSearch
 * @var $tariffGroup integer
 */

$chartHeight = '200px';
?>
<div class="visits-charts">
    <div id="dau_visits">
        <?= \miloschuman\highcharts\Highcharts::widget([
            'scripts' => [
                'themes/grid-light',
            ],
            'options' => [
                'title' => [
                    'text' =>'DAU',
                    'floating' => true,
                    'align' => 'left',
                ],
                'chart' => [
                    'height' => $chartHeight,
                ],
                'xAxis' => [
                    'categories' => $searchModel->getVisitsX($searchModel::DAY),
                    'labels' => [
                        'enabled' => true
                    ]
                ],
                'yAxis' => [
                    'title' => ['text' => false],
                    'labels' => [
                        'enabled' => false
                    ]
                ],
                'tooltip' => [
                    'shared' => true,
                    'headerFormat' => '{point.x}.'.$searchModel->year.'<br>',
                    'pointFormat' => '{series.name}: {point.y}',
                ],
                'legend' => false,
                'series' => [
                    [
                        'type' => 'spline',
                        'color' => 'blue',
                        'name' => 'Посещаемость',
                        'pointWidth' => 10,
                        'data' => $searchModel->getVisitsY($searchModel::DAY, $tariffGroup),
                    ],
                ],
            ],
        ]); ?>
    </div>
    <div id="wau_visits">
        <?= \miloschuman\highcharts\Highcharts::widget([
            'scripts' => [
                'themes/grid-light',
            ],
            'options' => [
                'title' => [
                    'text' =>'WAU',
                    'floating' => true,
                    'align' => 'left',
                ],
                'chart' => [
                    'height' => $chartHeight,
                ],
                'xAxis' => [
                    'categories' => $searchModel->getVisitsX($searchModel::WEEK),
                    'labels' => [
                        'enabled' => true
                    ]
                ],
                'yAxis' => [
                    'title' => ['text' => false],
                    'labels' => [
                        'enabled' => false
                    ]
                ],
                'tooltip' => [
                    'shared' => false,
                    'formatter' => new \yii\web\JsExpression("function() {
                    var dm = this.x.split('.'); 
                    var dateFrom = new Date(".$searchModel->year.", dm[1]-1, dm[0], 0, 0, 0, 0);
                    var dateTo = new Date();
                    var res = dateTo.setTime(dateFrom.getTime() + (6 * 24 * 60 * 60 * 1000));
                    dateTo = new Date(res);
                    return dateFrom.toLocaleDateString()+' - '+dateTo.toLocaleDateString()+'<br/>'+this.series.name + ': ' + this.y }")
                    //'headerFormat' => '{point.x}.'.$searchModel->year.'<br>',
                    //'pointFormat' => '{series.name}: <b>{point.y}</b><br>',
                ],
                'legend' => false,
                'series' => [
                    [
                        'type' => 'spline',
                        'color' => 'blue',
                        'name' => 'Посещаемость',
                        'pointWidth' => 10,
                        'data' => $searchModel->getVisitsY($searchModel::WEEK, $tariffGroup),
                    ],
                ],
            ],
        ]); ?>
    </div>
    <div id="mau_visits">
        <?= \miloschuman\highcharts\Highcharts::widget([
            'scripts' => [
                'themes/grid-light',
            ],
            'options' => [
                'title' => [
                    'text' =>'MAU',
                    'floating' => true,
                    'align' => 'left',
                ],
                'chart' => [
                    'height' => $chartHeight,
                ],
                'xAxis' => [
                    'categories' => $searchModel->getVisitsX($searchModel::MONTH),
                    'labels' => [
                        'enabled' => true
                    ]
                ],
                'yAxis' => [
                    'title' => ['text' => false],
                    'labels' => [
                       'enabled' => false
                    ]
                ],
                'tooltip' => [
                    'shared' => false,
                    'formatter' => new \yii\web\JsExpression("function() {
                    var dm = this.x.split('.'); 
                    var dateFrom = new Date(".$searchModel->year.", dm[1]-1, dm[0], 0, 0, 0, 0);
                    var dateTo = new Date(".$searchModel->year.", dm[1], 0, 0, 0, 0, 0);
                    return dateFrom.toLocaleDateString()+' - '+dateTo.toLocaleDateString()+'<br/>'+this.series.name + ': ' + this.y }")
                ],
                'legend' => false,
                'series' => [
                    [
                        'type' => 'spline',
                        'color' => 'blue',
                        'name' => 'Посещаемость',
                        'pointWidth' => 10,
                        'data' => $searchModel->getVisitsY($searchModel::MONTH, $tariffGroup),
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>