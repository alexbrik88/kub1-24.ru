<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\service\SubscribeTariffGroup;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $summaryData array
 * @var $summaryTotal array
 * @var $summaryDataPrevYear array
 * @var $summaryTotalPrevYear array
 * @var $year int
 */
$this->title = 'Коэффициенты';

$currYear = Yii::$app->request->get('year', date('Y'));
$dropYears = [];
for ($i=date('Y'); $i>=2015; $i--) {
    $dropYears[] = [
        'label' => $i,
        'url' => Url::current(['year' => $i]),
    ];
}

function showColumns($data, $key, $tag = 'td', $isMoney = false) {
    $str = '';
    foreach ($data as $value) {
        $val = ArrayHelper::getValue($value, $key);
        if ($val && $isMoney) {
            $val = TextHelper::numberFormat($val);
        }

        $str .= "<{$tag}>" . $val . "</{$tag}>";
    }

    return $str;
}

foreach ($summaryData as &$data) {
    $data['totals'] = array_sum($data) ?: '';
}
$summaryTotal['totals'] = array_sum(ArrayHelper::getColumn($summaryData, 'totals'));

foreach ($summaryDataPrevYear as &$data) {
    $data['totals_by_date'] = array_sum($data) ?: '';
}
$summaryTotalPrevYear['totals_by_date'] = array_sum(ArrayHelper::getColumn($summaryDataPrevYear, 'totals_by_date'));

$titles = SubscribeTariffGroup::find()->select('name')->indexBy('id')->orderBy([
    'priority' => SORT_ASC,
])->column() + [
    'add_invoice' => 'Счета штучные',
    'totals' => 'Итого',
    'totals_by_date' => 'Итого на ' . ($year - 1)
];

?>
<style>
    table.table-coefficients .heading th:last-child {
        border-right: 1px solid #ddd!important;
    }
    table.table-coefficients tr > td:not(:first-child) {
        text-align: right;
    }
    table.table-coefficients tr:nth-last-child(1) > td,
    table.table-coefficients tr:nth-last-child(2) > td
    {
        font-weight: bold;
    }
</style>

<div class="selling-subscribe-index">
    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <div class="dropdown pull-right">
                        <div style="display: inline-block; vertical-align: top; padding: 12px 8px 0 0;">Период</div>
                        <div style="display: inline-block; padding:4px 4px 0 0;">
                            <div class="dropdown">
                                <?= \yii\helpers\Html::a(' ' . $currYear . ' <span class="caret"></span>', null, [
                                    'class' => 'btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom doc-gray-button btn_select_days btn_row',
                                    'id' => 'dropdownMenuYear',
                                    'data-toggle' => 'dropdown',
                                    'aria-expanded' => true,
                                ]); ?>
                                <?= Dropdown::widget([
                                    'items' => $dropYears,
                                    'options' => [
                                        'style' => 'top: 36px;',
                                        'aria-labelledby' => 'dropdownMenuYear',
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-xs-12">
                     <div class="mrr-data-tab" style="overflow-x: auto;">
                            <table class="table table-striped table-bordered table-hover dataTable table-coefficients" role="grid">
                                <thead>
                                <tr class="heading bb">
                                    <th rowspan="2">MRR (Monthly Recurring Revenue)</th>
                                    <th colspan="12" style="text-align: center"><?= $currYear ?></th>
                                    <th rowspan="2">Итого</th>
                                </tr>
                                <tr class="heading">
                                    <?= showColumns($summaryData, 'month', 'th') ?>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach (reset($summaryData) as $key=>$value): ?>
                                        <?php if ($key == 'month') continue; ?>
                                        <tr>
                                            <td>
                                                <?= $titles[$key] ?>
                                            </td>
                                            <?= showColumns($summaryData, $key, 'td', true) ?>
                                            <td><?= TextHelper::numberFormat($summaryTotal[$key]) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <?php foreach (reset($summaryDataPrevYear) as $key=>$value): ?>
                                        <?php if ($key != 'totals_by_date') continue; ?>
                                        <tr>
                                            <td>
                                                <?= $titles[$key] ?>
                                            </td>
                                            <?= showColumns($summaryDataPrevYear, $key, 'td', true) ?>
                                            <td><?= TextHelper::numberFormat($summaryTotalPrevYear[$key]) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.mrr-section').on('click', function(e) {
        e.preventDefault();
        var tab = $(this).attr('data-tab');
        $('.mrr-data-tab').hide().filter('[data-tab="' + tab + '"]').show();
        $('.mrr-section').removeClass('active').filter('[data-tab="' + tab + '"]').addClass('active');
    });
</script>