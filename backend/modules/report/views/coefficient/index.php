<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $summaryData array
 * @var $summaryTotal array
 * @var $searchModel \backend\modules\report\models\CoefficientSearch
 * @var $tariffGroup integer
 */
$this->title = 'Коэффициенты';

$currYear = Yii::$app->request->get('year', date('Y'));
$dropYears = [];
for ($i=date('Y'); $i>=2015; $i--) {
    $dropYears[] = [
        'label' => $i,
        'url' => Url::current(['year' => $i]),
    ];
}

function showColumns($data, $key, $tag = 'td', $isMoney = false, $inPercent = false) {
    $str = '';
    foreach ($data as $value) {
        $val = ArrayHelper::getValue($value, $key);
        if ($val && $inPercent) {
            $val = 100 * $val;
        }
        if ($val && $isMoney) {
            $val = TextHelper::numberFormat($val);
        }

        $str .= "<{$tag}>" . ($val ?: '') . ($inPercent && $val ? '%' : '') . "</{$tag}>";
    }

    return $str;
}
function showColumnsA($data, $paramName, $tag = 'td') {
    $str = '';

    foreach ($data as $value) {
        $val = ArrayHelper::getValue($value, $paramName);
        $href = $value['href'] . '&paramName=' . $paramName;

        $str .= "<{$tag}>" . "<a href=\"{$href}\" target=\"_blank\" style=\"color:#333\">" . ($val ?: '') . "</a></{$tag}>";
    }

    return $str;
}

function getARPU($data) {
    return (!empty($data['MRR']) && !empty($data['MAU']) && $data['MAU'] > 0) ? $data['MRR'] / $data['MAU'] : '';
}
function getARPPU($data) {
    return (!empty($data['MRR']) && !empty($data['countPayers']) && $data['countPayers'] > 0) ? $data['MRR'] / $data['countPayers'] : '';
}
function getPS($data) {
    return (!empty($data['ARPU']) && !empty($data['ARPPU']) && $data['ARPPU'] > 0) ? $data['ARPU'] / $data['ARPPU'] : '';
}
function getCCR($data) {
    return (!empty($data['countGonePayers']) && !empty($data['prevMonthCountPayers']) && $data['prevMonthCountPayers'] > 0) ? $data['countGonePayers'] / $data['prevMonthCountPayers'] : '';
}
function getLTV($data) {
    return (!empty($data['ARPPU']) && !empty($data['CCR']) && $data['CCR'] > 0) ? $data['ARPPU'] / $data['CCR'] : '';
}

$uvw = $uvd = $dau = 0;
foreach ($summaryData as $monthNum => &$data) {
    $data['ARPU']  = getARPU($data);
    $data['ARPPU'] = getARPPU($data);
    $data['PS']    = getPS($data);
    $data['CCR']   = getCCR($data);
    $data['LTV']   = getLTV($data);

    // Average by full months
    if ($currYear < date('Y') || ($currYear == date('Y') && $monthNum < (int)date('m'))) {
        $uvw += (int)$data['UVW'];
        $uvd += (int)$data['UVD'];
        $dau += (int)$data['DAU'];
    }
}

$summaryTotal['ARPU']  = getARPU($summaryTotal);
$summaryTotal['ARPPU'] = getARPPU($summaryTotal);
$summaryTotal['PS']    = getPS($summaryTotal);
$summaryTotal['CCR']   = getCCR($summaryTotal);
$summaryTotal['LTV']   = getLTV($summaryTotal);

$lastFullMonth = (int)date('m') - 1;
$summaryTotal['UVW'] = ($lastFullMonth) ? $uvw / $lastFullMonth : 0;
$summaryTotal['UVD'] = ($lastFullMonth) ? $uvd / $lastFullMonth : 0;
$summaryTotal['DAU'] = ($lastFullMonth) ? $dau / $lastFullMonth : 0;

?>
<div class="coefficients-index">
    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <div class="dropdown pull-right">
                        <div style="display: inline-block; vertical-align: top; padding: 12px 8px 0 0;">Период</div>
                        <div style="display: inline-block; padding:4px 4px 0 0;">
                            <div class="dropdown">
                                <?= \yii\helpers\Html::a(' ' . $currYear . ' <span class="caret"></span>', null, [
                                    'class' => 'btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom doc-gray-button btn_select_days btn_row',
                                    'id' => 'dropdownMenuYear',
                                    'data-toggle' => 'dropdown',
                                    'aria-expanded' => true,
                                ]); ?>
                                <?= Dropdown::widget([
                                    'items' => $dropYears,
                                    'options' => [
                                        'style' => 'top: 36px;',
                                        'aria-labelledby' => 'dropdownMenuYear',
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div style="overflow-x: auto;">
                <table class="table table-striped table-bordered table-hover dataTable table-coefficients" role="grid">
                    <thead>
                    <tr class="heading bb">
                        <th rowspan="2">Коэффициенты</th>
                        <th colspan="12" style="text-align: center"><?= $currYear ?></th>
                        <th rowspan="2">Итого</th>
                    </tr>
                    <tr class="heading">
                        <?= showColumns($summaryData, 'month', 'th') ?>
                    </tr>
                    </thead>
                    <tbody>
                        <tr style="display: none">
                            <td>Кол-во платящих на конец прошлого месяца</td>
                            <?= showColumns($summaryData, 'prevMonthCountPayers') ?>
                            <td><?= $summaryTotal['prevMonthCountPayers'] ?: '' ?></td>
                        </tr>
                        <tr>
                            <td>Кол-во платящих на конец месяца</td>
                            <?= showColumnsA($summaryData, 'countPayers') ?>
                            <td><?= $summaryTotal['countPayers'] ?: '' ?></td>
                        </tr>
                        <tr>
                            <td>Кол-во новых</td>
                            <?= showColumnsA($summaryData, 'countNewPayers') ?>
                            <td><?= $summaryTotal['countNewPayers'] ?: '' ?></td>
                        </tr>
                        <tr>
                            <td>Кол-во вернувшихся</td>
                            <?= showColumnsA($summaryData, 'countReturnedPayers') ?>
                            <td><?= $summaryTotal['countReturnedPayers'] ?: '' ?></td>
                        </tr>
                        <tr>
                            <td>Кол-во не оплативших повторно</td>
                            <?= showColumnsA($summaryData, 'countGonePayers') ?>
                            <td><?= $summaryTotal['countGonePayers'] ?: '' ?></td>
                        </tr>
                        <tr>
                            <td>MRR (Monthly Recurring Revenue)</td>
                            <?= showColumns($summaryData, 'MRR', 'td', true) ?>
                            <td><?= $summaryTotal['MRR'] ? TextHelper::numberFormat($summaryTotal['MRR']) : '' ?></td>
                        </tr>
                        <tr>
                            <td>NEW MRR</td>
                            <?= showColumns($summaryData, 'NEW_MRR', 'td', true) ?>
                            <td><?= $summaryTotal['NEW_MRR'] ? TextHelper::numberFormat($summaryTotal['NEW_MRR']) : '' ?></td>
                        </tr>
                        <tr>
                            <td>MAU (Monthly Active Users)</td>
                            <?= showColumns($summaryData, 'MAU') ?>
                            <td><?= $summaryTotal['MAU'] ? TextHelper::numberFormat($summaryTotal['MAU']) : '' ?></td>
                        </tr>
                        <tr>
                            <td>DAU (Daily Active Users)</td>
                            <?= showColumns($summaryData, 'DAU') ?>
                            <td><?= $summaryTotal['DAU'] ? TextHelper::numberFormat($summaryTotal['DAU']) : '' ?></td>
                        </tr>
                        <tr>
                            <td>Unique visitors per Week, visited each Week</td>
                            <?= showColumns($summaryData, 'UVW') ?>
                            <td><?= $summaryTotal['UVW'] ? TextHelper::numberFormat($summaryTotal['UVW']) : '' ?></td>
                        </tr>
                        <tr>
                            <td>Unique visitors per Day, visited each Day</td>
                            <?= showColumns($summaryData, 'UVD') ?>
                            <td><?= $summaryTotal['UVD'] ? TextHelper::numberFormat($summaryTotal['UVD']) : '' ?></td>
                        </tr>
                        <tr>
                            <td>ARPU (Average Revenue Per User)</td>
                            <?= showColumns($summaryData, 'ARPU', 'td', true) ?>
                            <td><?= $summaryTotal['ARPU'] ? TextHelper::numberFormat($summaryTotal['ARPU']) : '' ?></td>
                        </tr>
                        <tr>
                            <td>ARPPU (Average Revenue Per Paing User)</td>
                            <?= showColumns($summaryData, 'ARPPU', 'td', true) ?>
                            <td><?= $summaryTotal['ARPPU'] ? TextHelper::numberFormat($summaryTotal['ARPPU']) : '' ?></td>
                        </tr>
                        <tr>
                            <td>Paying Share (доля платящих клиентов)</td>
                            <?= showColumns($summaryData, 'PS', 'td', true, true) ?>
                            <td><?= $summaryTotal['PS'] ? (TextHelper::numberFormat(100 * $summaryTotal['PS']).'%') : '' ?></td>
                        </tr>
                        <tr>
                            <td>LTV (Lifetime Value)</td>
                            <?= showColumns($summaryData, 'LTV', 'td', true) ?>
                            <td><?= $summaryTotal['LTV'] ? TextHelper::numberFormat($summaryTotal['LTV']) : '' ?></td>
                        </tr>
                        <tr>
                            <td>Сustomer Churn Rate</td>
                            <?= showColumns($summaryData, 'CCR', 'td', true, true) ?>
                            <td><?= $summaryTotal['CCR'] ? (TextHelper::numberFormat(100 * $summaryTotal['CCR']).'%') : '' ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <?= $this->render('_visit_charts', [
                'searchModel' => $searchModel,
                'tariffGroup' => $tariffGroup
            ]) ?>
        </div>
    </div>
</div>

<style>
    .coefficients-index {
        margin-bottom: 100px;
    }
    table.table-coefficients .heading th:last-child {
        border-right: 1px solid #ddd!important;
    }
    table.table-coefficients tr > td:not(:first-child) {
        text-align: right;
    }
</style>