<?php

use common\models\Company;
use common\models\document\Invoice;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use frontend\models\Documents;
use yii\widgets\ActiveForm;

if ($comp_ids = \Yii::$app->request->post('company_ids'))
    $comp_ids = explode(',', $comp_ids);

function getDateRange($month, $year) {
    if ($year == '2019' && $month == '10') {
        return [
            'from' => '01-10-2019',
            'to' => '26-10-2019'
        ];
    }

    $date = new \DateTime();
    $date->setDate($year, $month, 1);

    return [
        'from' => $date->format('Y-m-d'),
        'to' => $date->format('Y-m-t')
    ];
}

function getPaymentsSum($company_id, $month, $year) {

    $dateRange = getDateRange($month, $year);

    return Payment::find()
        ->joinWith('company', false)
        ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
        ->andWhere(['not', ['service_payment.type_id' => PaymentType::TYPE_PROMO_CODE]])
        ->andWhere(['service_payment.tariff_id' => [1,2,3]])
        ->andWhere(['service_payment.is_confirmed' => 1])
        ->andWhere(['company_id' => $company_id])
        ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . 'service_payment.payment_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])
        ->groupBy('company_id')
        ->sum('sum');
}

function getInvoicesCount($company_id, $month, $year) {

    $dateRange = getDateRange($month, $year);

    return Invoice::find()
        ->byIOType(Documents::IO_TYPE_OUT)
        ->byDeleted()
        ->andWhere(['between', 'invoice.document_date', $dateRange['from'], $dateRange['to']])
        ->byCompany($company_id)
        ->count();
}

?>

<?php if (!$comp_ids): ?>
<?php $form = ActiveForm::begin([
    'method' => 'POST',
]); ?>
    <textarea name="company_ids"></textarea>
    <br/>
    <button type="submit">Submit</button>
<?php $form->end(); ?>
<br/>
<br/>
<?php endif; ?>
<?php if ($comp_ids): ?>
    <table class="table table-bordered" style="width: 100%">
        <tr>
            <th rowspan="2">№</th>
            <th rowspan="2">ID подписки</th>
            <th rowspan="2">ID компании</th>
            <th rowspan="2">Дата активации</th>
            <th rowspan="2">Дата окончания</th>
            <th rowspan="2">Название компании</th>

            <th rowspan="2">ИНН</th>
            <th rowspan="2">Юр. адрес</th>
            <th rowspan="2">Телефон</th>
            <th rowspan="2">E-mail</th>
            <th rowspan="2">Руководитель</th>

            <th rowspan="2">Система налогообложения</th>
            <th rowspan="2">Дата регистрации</th>
            <th rowspan="2">Банк (основной)</th>
            <th colspan="7">2018 Оплата</th>
            <th colspan="7">2018 Счета</th>
            <th colspan="12">2019 Оплата</th>
            <th colspan="12">2019 Счета</th>
        </tr>
        <tr>
            <?php for ($i=6; $i<=12; $i++): ?>
                <th><?= \common\components\helpers\Month::$monthShort[$i] ?></th>
            <?php endfor; ?>
            <?php for ($i=6; $i<=12; $i++): ?>
                <th><?= \common\components\helpers\Month::$monthShort[$i] ?></th>
            <?php endfor; ?>
            <?php for ($i=1; $i<=12; $i++): ?>
                <th><?= \common\components\helpers\Month::$monthShort[$i] ?></th>
            <?php endfor; ?>
            <?php for ($i=1; $i<=12; $i++): ?>
                <th><?= \common\components\helpers\Month::$monthShort[$i] ?></th>
            <?php endfor; ?>
        </tr>
        <?php $n = 1; foreach ($comp_ids as $id): ?>
            <?php if ($company = Company::findOne($id)): ?>
                <?php if ($subscribe = Subscribe::find()
                    ->andWhere(['>', 'expired_at', 0])
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['tariff_id' => [1,2,3]])
                    ->orderBy(['expired_at' => SORT_DESC])->one()): ?>
                    <?php $mainCheckingAccount = $company->mainCheckingAccountant; ?>
                    <tr>
                        <td><?= $n ?></td>
                        <td><?= $subscribe->id ?></td>
                        <td><?= $company->id ?></td>
                        <td><?= date('d.m.Y', $subscribe->activated_at) ?></td>
                        <td><?= date('d.m.Y', $subscribe->expired_at) ?></td>
                        <td><?= $company->getShortName() ?></td>

                        <td><?= $company->inn ?></td>
                        <td><?= $company->getAddressLegalFull() ?></td>
                        <td><?= $company->phone ?></td>
                        <td><?= $company->email ?></td>
                        <td><?= join(', ', array_filter([$company->chief_post_name, $company->getChiefFio()])); ?></td>

                        <td><?= $company->companyTaxationType->name ?></td>
                        <td><?= date('d.m.Y', $company->created_at) ?></td>
                        <td><?= ($mainCheckingAccount) ? $mainCheckingAccount->bank_name : '' ?></td>
                        <?php for ($i=6; $i<=12; $i++): ?>
                            <th><?= getPaymentsSum($company->id, $i, '2018') ?></th>
                        <?php endfor; ?>
                        <?php for ($i=6; $i<=12; $i++): ?>
                            <th><?= getInvoicesCount($company->id, $i, '2018') ?></th>
                        <?php endfor; ?>
                        <?php for ($i=1; $i<=12; $i++): ?>
                            <th><?= getPaymentsSum($company->id, $i, '2019') ?></th>
                        <?php endfor; ?>
                        <?php for ($i=1; $i<=12; $i++): ?>
                            <th><?= getInvoicesCount($company->id, $i, '2019') ?></th>
                        <?php endfor; ?>
                    </tr>
                    <?php $n++; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
<?php exit; ?>

