<?php

use common\models\bank\BankingParams;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\product\Product;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\widgets\ActiveForm;
use common\models\company\CompanyFirstEvent;
use common\components\helpers\ArrayHelper;
use backend\modules\employee\models\Employee;
use backend\modules\company\models\CompanyStatistic;

if ($comp_ids = \Yii::$app->request->post('company_ids'))
    $comp_ids = explode(',', $comp_ids);

$eventName = \common\models\company\Event::find()->select('name')->indexBy('id')->asArray()->column();

function hasBankIntegration(Company $company) {
    if ($accountants = $company->checkingAccountants) {
        foreach ($accountants as $accountant) {
            foreach (Banking::$modelClassArray as $banking) {
                if (in_array($accountant->bik, $banking::$bikList)) {
                    if (BankingParams::find()
                        ->andWhere(['bank_alias' => $banking::ALIAS])
                        ->andWhere(['param_name' => 'access_token'])
                        ->andWhere(['company_id' => $accountant->company_id])
                        ->count())

                        return 'Да';
                }
            }
        }
    }

    return 'Нет';
}

function getPaymentBaseQuery($company_id)
{
    return Payment::find()
        ->andWhere(['not', ['service_payment.type_id' => PaymentType::TYPE_PROMO_CODE]])
        ->andWhere(['service_payment.tariff_id' => [1,2,3]])
        ->andWhere(['service_payment.is_confirmed' => 1])
        ->andWhere(['company_id' => $company_id]);
}

function getFirstPayment($company_id) {

    return getPaymentBaseQuery($company_id)
        ->orderBy(['service_payment.payment_date' => SORT_ASC])
        ->one();
}

function getPaymentsCount($company_id) {

    return getPaymentBaseQuery($company_id)
        ->count();
}

function getPaymentsSum($company_id) {

    return getPaymentBaseQuery($company_id)
        ->sum('service_payment.sum');
}

function hasActiveSubscribeByDate($company_id, $date = '2019-10-26') {

    return Subscribe::find()
        ->andWhere(['OR',
            ['>', 'DATE(FROM_UNIXTIME(expired_at))', $date . ' 00:00:00'],
            ['IS', 'expired_at', null],
        ])
        ->andWhere(['company_id' => $company_id])
        ->andWhere(['tariff_id' => [1,2,3]])
        ->exists();
}

?>

<?php if (!$comp_ids): ?>
<?php $form = ActiveForm::begin([
    'method' => 'POST',
]); ?>
    <textarea name="company_ids"></textarea>
    <br/>
    <button type="submit">Submit</button>
<?php $form->end(); ?>
<br/>
<br/>
<?php endif; ?>

<?php if ($comp_ids): ?>
    <table class="table table-bordered" style="width: 100%">
        <tr>
            <th rowspan="2">№</th>
            <th rowspan="2">ID компании</th>
            <th rowspan="2">Название компании</th>

            <th rowspan="2">ИНН</th>
            <th rowspan="2">Юр. адрес</th>
            <th rowspan="2">Телефон</th>
            <th rowspan="2">E-mail</th>
            <th rowspan="2">Руководитель</th>

            <th rowspan="2">Система налогообложения</th>
            <th rowspan="2">Дата регистрации</th>
            <th rowspan="2">Источник или канал</th>
            <th rowspan="2">Ключевое слово</th>
            <th rowspan="2">Регион</th>
            <th rowspan="2">Банк (основной)</th>
            <th rowspan="2">Есть интеграция</th>
            <th rowspan="2">Кол-во сотрудников</th>
            <th rowspan="2">Автосчета</th>
            <th rowspan="2">Кол-во позиций товара</th>
            <th rowspan="2">Кол-во позиций услуг</th>
            <th rowspan="2">Название услуг</th>
            <th rowspan="2">Название товаров</th>
            <th rowspan="2">Дата первой оплаты</th>
            <th rowspan="2">Сумма первой оплаты</th>
            <th rowspan="2">Кол-во оплат</th>
            <th rowspan="2">Сумма всех оплат</th>
            <th rowspan="2">На 26.10.2019 платящий?</th>

            <th>Действия/Шаги</th>
        </tr>
        <tr>
            <?php for ($i=1; $i<=98; $i++): ?>
                <th><?= $eventName[$i] ?></th>
            <?php endfor; ?>
        </tr>
        <?php $n = 1; foreach ($comp_ids as $id): ?>
            <?php if ($company = Company::findOne($id)): ?>
                <?php $mainCheckingAccount = $company->mainCheckingAccountant; ?>
                <?php $events = CompanyFirstEvent::find()->where(['company_id' => $company->id])->select('step')->indexBy('event_id')->asArray()->column(); ?>
                <?php $companyStatistic = new CompanyStatistic([
                            'company' => $company,
                        ]); ?>
                <?php $productStat = $companyStatistic->getProductStat(); ?>
                <?php $firstPayment = getFirstPayment($company->id); ?>
                <tr>
                    <td><?= $n ?></td>
                    <td><?= $company->id ?></td>
                    <td><?= $company->getShortName() ?></td>

                    <td><?= $company->inn ?></td>
                    <td><?= $company->getAddressLegalFull() ?></td>
                    <td><?= $company->phone ?></td>
                    <td><?= $company->email ?></td>
                    <td><?= join(', ', array_filter([$company->chief_post_name, $company->getChiefFio()])); ?></td>

                    <td><?= $company->companyTaxationType->name ?></td>
                    <td><?= date('d.m.Y', $company->created_at) ?></td>
                    <td><?= $company->form_source; ?></td>
                    <td><?= $company->form_keyword; ?></td>
                    <td><?= $company->form_region; ?></td>
                    <td><?= ($mainCheckingAccount) ? $mainCheckingAccount->bank_name : '' ?></td>
                    <td><?= ($mainCheckingAccount) ? hasBankIntegration($company) : '' ?></td>
                    <td><?= Employee::find()->joinWith('employeeCompany')->andWhere(['employee_company.company_id' => $company->id])->count(); ?></td>
                    <td><?= Autoinvoice::find()->where(['company_id' => $company->id])->andWhere(['status' => Autoinvoice::ACTIVE])->count() ?: 'Нет' ?></td>
                    <td><?= $productStat[Product::PRODUCTION_TYPE_GOODS]; ?></td>
                    <td><?= $productStat[Product::PRODUCTION_TYPE_SERVICE]; ?></td>
                    <td style="min-width: 500px;"><?= $company->getFirstTenProducts(Product::PRODUCTION_TYPE_SERVICE) ?></td>
                    <td style="min-width: 500px;"><?= $company->getFirstTenProducts(Product::PRODUCTION_TYPE_GOODS) ?></td>
                    <td><?= ($firstPayment) ? date('d.m.Y', $firstPayment->payment_date) : '' ?></td>
                    <td><?= ($firstPayment) ? $firstPayment->sum : '' ?></td>
                    <td><?= getPaymentsCount($company->id) ?></td>
                    <td><?= getPaymentsSum($company->id) ?></td>
                    <td><?= hasActiveSubscribeByDate($company->id) ? 'Да' : 'Нет' ?></td>

                    <?php for ($i=1; $i<=98; $i++): ?>
                        <td><?= ArrayHelper::getValue($events, $i) ?></td>
                    <?php endfor; ?>
                </tr>
                <?php $n++; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
<?php exit; ?>
