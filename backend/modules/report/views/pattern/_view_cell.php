<?php

/* @var $this yii\web\View */
/* @var $value integer|null */

?>
<div class="step-cell-inner" value="<?= $value ? : 0; ?>">
    <div class="text"><?= $value ? : '&nbsp;' ?></div>
    <div class="footer">
        <?php if ($value) : ?>
            <span class="bullet">&#8226;</span>
        <?php else : ?>
            <span class="bullet">&nbsp;</span>
        <?php endif ?>
    </div>
</div>