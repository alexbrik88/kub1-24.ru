<?php

use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\report\models\PatternForm */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="pattern-form">
    <?php Pjax::begin([
        'id' => 'pattern-form-pjax',
        'enablePushState' => false,
        'linkSelector' => false,
    ]); ?>

        <?= Alert::widget(); ?>

        <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
            'id' => 'pattern-form',
            'action' => ['create'],
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'options' => [
                'class' => 'form-horizontal',
                'data-pjax' => '',
            ],
        ])); ?>

            <?= $form->field($model, 'start', [
                'template' => Yii::$app->params['formDatePickerTemplate'],
            ])->textInput([
                'class' => 'form-control date-picker',
            ]) ?>

            <?= $form->field($model, 'end', [
                'template' => Yii::$app->params['formDatePickerTemplate'],
            ])->textInput([
                'class' => 'form-control date-picker',
            ]) ?>

            <div style="margin: 30px 0 0;">
                <?= Html::submitButton('Сформировать', ['class' => 'btn darkblue text-white']) ?>
                <?= Html::button('Отмена', [
                    'class' => 'btn darkblue text-white pull-right',
                    'type' => 'button',
                    'data-dismiss' => 'modal',
                    'aria-hidden' => 'true',
                ]) ?>
            </div>

        <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>
