<?php

use common\components\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\pattern\Pattern */

$this->title = 'Паттерн - отчет';
$this->params['breadcrumbs'][] = ['label' => 'Patterns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'name',
        'label' => 'Действия\Шаги',
        'headerOptions' => [
            'class' => 'sorting',
            'style' => 'min-width: 250px !important; max-width: 500px !important;',
        ],
        'contentOptions' => [
            'style' => 'min-width:250px !important; max-width: 500px !important;',
        ],
    ],
];
foreach ($searchModel->columns as $key => $value) {
    $columns[] = [
        'attribute' => $value,
        'label' => $key,
        'headerOptions' => [
            'class' => 'sorting step-column',
            'data-step' =>$key,
        ],
        'contentOptions' => [
            'class' => 'step-cell-' . $key,
            'style' => 'text-align: center; padding: 3px; vertical-align: middle;',
        ],
        'format' => 'raw',
        'value' => function ($model) use ($value, $key) {
            return $this->render('_view_cell', ['key' => $key, 'value' => $model->$value ? : null]);
        }
    ];
}
?>

<div class="pattern-view">
    <?= Html::a('Назад к списку', ['index']) ?>
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-8">
            <h3><?= Html::encode($this->title) ?></h3>
        </div>
    </div>
    <table style="width: auto;">
        <tr>
            <td style="padding: 5px 10px; font-weight: bold;">
                Дата отчета
            </td>
            <td>
                <?= date('d.m.Y', $model->created_at) ?>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px 10px; font-weight: bold;">
                Период
            </td>
            <td>
                <?= date_create_from_format('Y-m-d', $model->date_start)->format('d.m.Y') ?>
                -
                <?= date_create_from_format('Y-m-d', $model->date_end)->format('d.m.Y') ?>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px 10px; font-weight: bold;">
                Регистраций
            </td>
            <td>
                <?= $model->signup_count ?>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px 10px; font-weight: bold;">
                Первых оплат
            </td>
            <td>
                <?= $model->payment_count ?>
            </td>
        </tr>
    </table>

    <?php $pjax = Pjax::begin([
        'id' => 'first-event-pjax',
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]); ?>

        <?= GridView::widget([
            'id' => 'first-event-grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                'style' => 'width: auto; margin: 20px auto 20px 0;',
            ],

            'headerRowOptions' => [
                'class' => 'heading',
            ],

            'options' => [
                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            ],
            'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>",

            'columns' => $columns,
        ]); ?>

    <?php $pjax->end(); ?>
</div>

<?php $this->registerJs('
    var checkMaxStepValue = function() {
        var grid = $("#first-event-grid");
        $("th.step-column", grid).each(function() {
            var step = $(this).data("step");
            var maxValue = 0;
            $("td.step-cell-"+step+" .step-cell-inner", grid).each(function() {
                var value = parseInt($(this).attr("value"));
                if (value > maxValue) maxValue = value;
            });
            if (maxValue > 0) {
                $("td.step-cell-"+step+" .step-cell-inner[value="+maxValue+"]", grid).addClass("max");
            }
        });
    }
    checkMaxStepValue();

    $(document).on("pjax:complete", "#first-event-pjax", function() {
        checkMaxStepValue();
    });
') ?>