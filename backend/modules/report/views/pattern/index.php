<?php

use backend\modules\report\models\PatternForm;
use common\components\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\report\models\PatternSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Паттерн';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pattern-index">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-6">
            <h3><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-sm-6">
            <div style="margin-top: 20px; text-align: right;">
                <button class="btn yellow text-white" type="button" data-toggle="modal" data-target="#pattern-modal">Создать</button>
            </div>
        </div>
    </div>

    <?php $pjax = Pjax::begin([
        'id' => 'pattern-pjax',
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]); ?>

        <?= GridView::widget([
            'id' => 'pattern-grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
            ],

            'headerRowOptions' => [
                'class' => 'heading',
            ],

            'options' => [
                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            ],
            'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

            'columns' => [
                'created_at:datetime',
                'signup_count',
                'payment_count',
                'date_start:date',
                'date_end:date',
                'interval',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {delete}',
                ],
            ],
        ]); ?>

    <?php $pjax->end(); ?>
</div>

<?php Modal::begin([
    'id' => 'pattern-modal',
    'header' => '<h1>Новый отчет</h1>',
]); ?>

    <?= $this->render('_form', [
        'model' => new PatternForm,
    ]) ?>

<?php Modal::end(); ?>

<?php $this->registerJs('
$(document).on("pjax:complete", "#pattern-pjax", function(event) {
    $(".date-picker", this).datepicker({"language":"ru","autoclose":true});
});
') ?>