<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.08.2017
 * Time: 7:37
 */

namespace backend\modules\api\models;


use common\models\api\ApiPartners;
use yii\data\ActiveDataProvider;

class PartnersSearch extends ApiPartners
{
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApiPartners::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'partner_name',
                    'start_date',
                    'end_date',
                ],
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}