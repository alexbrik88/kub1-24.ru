<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.08.2017
 * Time: 6:17
 */
use common\widgets\Modal;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use common\models\api\ApiPartners;
use common\components\date\DateHelper;

/* @var $this yii\web\View
 * @var $partner ApiPartners
 * @var $title string
 * @var $modalID string
 */

$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-2 control-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-10 inp_one_line-product',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
?>
<?php Modal::begin([
    'header' => '<h3 style="text-align: center; margin: 0">' . $title . '</h3>',
    'id' => $modalID,
]); ?>

<?php $createPartnerForm = ActiveForm::begin([
    'action' => $partner->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $partner->id]),
    'options' => [
        'class' => 'form-horizontal form-' . $modalID,
        'id' => 'form-' . $modalID,
    ],
    'enableClientValidation' => true,
]); ?>

<?= $createPartnerForm->field($partner, 'partner_name', $textInputConfig); ?>

<?= $createPartnerForm->field($partner, 'partner_site', $textInputConfig); ?>

<?php if ($partner->api_key != null): ?>
    <?= $createPartnerForm->field($partner, 'api_key', [
        'options' => [
            'class' => 'form-group',
        ],
        'labelOptions' => [
            'class' => 'col-md-2 control-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-10 inp_one_line-product input-group',
            'style' => 'padding-right: 30px;padding-left: 15px;',
        ],
        'inputOptions' => [
            'disabled' => true,
        ],
        'template' => "{label}\n{beginWrapper}\n{input}" .
            Html::activeHiddenInput($partner, 'generatedApiKey', [
                'value' => $partner->api_key,
            ]) .
            "<i class='icon-refresh' data-url='" .
            Url::to(['generate-api-key']) . "'></i>\n{error}\n{hint}\n{endWrapper}",
    ]); ?>
<?php else: ?>
    <div class="form-group field-apipartners-api_key">
        <label class="col-md-2 control-label bold-text"
               for="apipartners-api_key">API</label>

        <div class="col-md-10 inp_one_line-product input-group" style="padding-right: 30px;padding-left: 15px;">
            <?= Html::button('Сгенерировать API KEY', [
                'class' => 'btn darkblue text-white generate-api-key',
                'data-url' => Url::to(['generate-api-key']),
            ]); ?>
            <?= Html::activeTextInput($partner, 'api_key', [
                'class' => 'form-control',
                'style' => 'display:none;',
                'disabled' => true,
            ]); ?>
            <?= Html::activeHiddenInput($partner, 'generatedApiKey', [
                'value' => $partner->api_key,
            ]); ?>
            <i class="icon-refresh" data-url="<?= Url::to(['generate-api-key']); ?>"
               style="display: none;"></i>
        </div>
    </div>
<?php endif; ?>

<?= $createPartnerForm->field($partner, 'start_date', array_merge($textInputConfig, [
    'wrapperOptions' => [
        'class' => 'col-md-4 inp_one_line-product',
    ],
]))->textInput([
    'class' => 'form-control date-picker',
    'size' => 16,
    'data-date-viewmode' => 'years',
    'value' => DateHelper::format($partner->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
]); ?>

<?= $createPartnerForm->field($partner, 'end_date', array_merge($textInputConfig, [
    'wrapperOptions' => [
        'class' => 'col-md-4 inp_one_line-product',
    ],
]))->textInput([
    'class' => 'form-control date-picker',
    'size' => 16,
    'data-date-viewmode' => 'years',
    'value' => DateHelper::format($partner->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
]); ?>

<?= $createPartnerForm->field($partner, 'contractFile', $textInputConfig)->fileInput()->label('Договор'); ?>

<?= $createPartnerForm->field($partner, 'contact_person_fio', $textInputConfig); ?>

<?= $createPartnerForm->field($partner, 'phone', $textInputConfig)->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
    'options' => [
        'class' => 'form-control field-width field-w inp_one_line_company',
        'id' => 'apipartners-phone-' . $partner->id,
        'placeholder' => '+7(XXX) XXX-XX-XX',
    ],
]); ?>

<?= $createPartnerForm->field($partner, 'email', $textInputConfig); ?>

<div class="form-actions" style="margin-bottom: 10px;">
    <div class="row action-buttons">
        <div class="col-sm-3 col-xs-3">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn darkblue text-white hidden-md hidden-sm hidden-xs',
                'style' => 'width: 100% !important',
            ]); ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => 'btn darkblue text-white hidden-lg',
                'title' => 'Сохранить',
                'style' => 'width: 100% !important',
            ]); ?>
        </div>
        <div class="col-sm-6 col-xs-6"></div>
        <div class="col-sm-3 col-xs-3">
            <?= Html::a('Отменить', null, [
                'class' => 'btn darkblue hidden-md hidden-sm hidden-xs close-modal-button',
                'style' => 'width: 100% !important',
            ]); ?>
            <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', null, [
                'class' => 'btn darkblue hidden-lg close-modal-button',
                'title' => 'Отменить',
                'style' => 'width: 100% !important',
            ]); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>
