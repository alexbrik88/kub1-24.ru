<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.08.2017
 * Time: 7:29
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use backend\modules\api\models\PartnersSearch;
use common\models\api\ApiPartners;
use common\components\date\DateHelper;
use common\widgets\Modal;

/* @var $this yii\web\View
 * @var $searchModel PartnersSearch
 * @var $dataProvider ActiveDataProvider
 * @var $partner ApiPartners
 */

$this->title = 'Партнеры';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', null, [
            'class' => 'btn yellow',
            'data-toggle' => 'modal',
            'data-target' => '#create-partner',
        ]); ?>
    </div>
    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            <?= $this->title; ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div
                class="dataTables_wrapper dataTables_extended_wrapper">
                <?= common\components\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'style' => 'overflow-x: auto;',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '3%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return $model->id;
                            },
                        ],
                        [
                            'attribute' => 'partner_name',
                            'label' => 'Партнер',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return $model->partner_name;
                            },
                        ],
                        [
                            'attribute' => 'partner_site',
                            'label' => 'Сайт',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return Html::a($model->partner_site, $model->partner_site, [
                                    'target' => '_blank',
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'api_key',
                            'label' => 'API',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return $model->api_key ? '+' : '-';
                            },
                        ],
                        [
                            'attribute' => 'start_date',
                            'label' => 'Дата начала',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return DateHelper::format($model->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'end_date',
                            'label' => 'Дата завершения',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return DateHelper::format($model->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'contract',
                            'label' => 'Договор',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                if ($model->contract) {
                                    return Html::a(
                                        '<span class="pull-center icon icon-paper-clip"></span>',
                                        Url::to(['upload-contract', 'id' => $model->id]),
                                        ['target' => '_blank',]
                                    );
                                }

                                return '';
                            },
                        ],
                        [
                            'attribute' => 'contact_person_fio',
                            'label' => 'Контактное лицо (ФИО)',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return $model->contact_person_fio;
                            },
                        ],
                        [
                            'attribute' => 'phone',
                            'label' => 'Телефон',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return $model->phone;
                            },
                        ],
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return Html::mailto($model->email);
                            },
                        ],
                        [
                            'label' => '',
                            'headerOptions' => [
                                'width' => '10%',
                                'style' => 'display: none;',
                            ],
                            'contentOptions' => [
                                'style' => 'display: none;',
                            ],
                            'format' => 'raw',
                            'value' => function (ApiPartners $model) {
                                return $this->render('form', [
                                    'partner' => $model,
                                    'title' => 'Обновить партнера',
                                    'modalID' => 'update-partner-' . $model->id,
                                ]);
                            },
                        ],
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'template' => '{update} {block} {unblock} {delete}',
                            'headerOptions' => [
                                'width' => '80',
                            ],
                            'urlCreator' => function ($action, ApiPartners $model) {
                                return Url::to(['/api/partners/' . $action, 'id' => $model->id]);
                            },
                            'buttons' => [
                                'update' => function ($url, ApiPartners $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                                        'title' => 'Редактировать',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#update-partner-' . $model->id,
                                    ]);
                                },
                                'delete' => function ($url, ApiPartners $model) {
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                        'title' => 'Удалить',
                                        'data-confirm' => 'Вы уверены, что хотите удалить партнера?',
                                        'data-method' => 'post',

                                    ]);
                                },
                                'block' => function ($url, ApiPartners $model) {
                                    if (!$model->is_blocked) {
                                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
                                            'title' => 'Заблокировать',
                                            'data-confirm' => 'Заблокировать партнера?',
                                            'data-method' => 'post',
                                        ]);
                                    }

                                    return null;
                                },
                                'unblock' => function ($url, ApiPartners $model) {
                                    if ($model->is_blocked) {
                                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                                            'title' => 'Разблокировать',
                                            'data-confirm' => 'Разблокировать партнера?',
                                            'data-method' => 'post',
                                        ]);
                                    }

                                    return null;
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?= $this->render('form', [
    'partner' => $partner,
    'title' => 'Создать партнера',
    'modalID' => 'create-partner',
]) ?>