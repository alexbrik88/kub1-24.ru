<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.08.2017
 * Time: 7:27
 */

namespace backend\modules\api\controllers;


use backend\components\BackendController;
use backend\modules\api\models\PartnersSearch;
use common\models\api\ApiPartners;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class PartnersController
 * @package backend\modules\api\controllers
 */
class PartnersController extends BackendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PartnersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $partner = new ApiPartners();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'partner' => $partner,
        ]);
    }

    /**
     * @return mixed|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ApiPartners();
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if (!($model->_load() && $model->_save())) {
            Yii::$app->session->setFlash('error', 'Ошибка при создании партнера.');
        }

        return $this->redirect('index');
    }

    /**
     * @param $id
     * @return mixed|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if (!($model->_load() && $model->_save())) {
            Yii::$app->session->setFlash('error', 'Ошибка при обновлении партнера.');
        }

        return $this->redirect('index');
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionUploadContract($id)
    {
        $model = $this->findModel($id);
        if ($model->contract) {
            $filePath = $model->getUploadPath() . $model->contract;
            if (file_exists($filePath)) {
                \Yii::$app->response->sendFile($filePath, $model->contract, ['mimeType' => '*/*'])->send();
            } else throw new NotFoundHttpException('Файл не найден');
        } else throw new NotFoundHttpException('Такого файла не существует');
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionBlock($id)
    {
        $model = $this->findModel($id);
        $model->is_blocked = ApiPartners::BLOCKED;
        if (!$model->save(true, ['is_blocked'])) {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при блокировке партнера.');
        }

        return $this->redirect('index');
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUnblock($id)
    {
        $model = $this->findModel($id);
        $model->is_blocked = ApiPartners::UNBLOCKED;
        if (!$model->save(true, ['is_blocked'])) {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при разблокировке партнера.');
        }

        return $this->redirect('index');
    }

    /**
     * @return array
     */
    public function actionGenerateApiKey()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        do {
            $apiKey = Yii::$app->security->generateRandomString();
        } while (ApiPartners::find()->where(['api_key' => $apiKey])->exists());

        return [
            'api_key' => $apiKey,
        ];
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect('index');
    }

    /**
     * @param $id
     *
     * @return ApiPartners
     * @throws NotFoundHttpException
     */
    protected function findModel($id = null)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }
        $model = ApiPartners::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}