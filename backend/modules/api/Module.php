<?php

namespace backend\modules\api;

/**
 * Class Module
 * @package backend\modules\api
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'backend\modules\api\controllers';

    /**
     *
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
