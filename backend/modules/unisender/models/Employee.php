<?php

namespace backend\modules\unisender\models;

use common\models\Company;
use common\models\employee\EmployeeDevice;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $email
 * @property integer $created_at
 * @property string $email_confirm_key
 * @property integer $is_email_confirmed
 * @property string $password
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $author_id
 * @property string $lastname
 * @property string $firstname
 * @property string $firstname_initial
 * @property string $patronymic
 * @property string $patronymic_initial
 * @property integer $sex
 * @property string $birthday
 * @property string $date_hiring
 * @property string $date_dismissal
 * @property string $position
 * @property integer $is_working
 * @property integer $employee_role_id
 * @property string $phone
 * @property integer $notify_nearly_report
 * @property integer $notify_new_features
 * @property integer $is_active
 * @property integer $is_deleted
 * @property integer $is_registration_completed
 * @property integer $time_zone_id
 * @property string $view_notification_date
 * @property integer $last_visit_at
 * @property integer $wp_id
 * @property integer $main_company_id
 * @property string $my_companies
 * @property integer $alert_close_count
 * @property integer $alert_close_time
 * @property string $statistic_range_date_from
 * @property string $statistic_range_date_to
 * @property string $statistic_range_name
 * @property integer $knows_about_sending_invoice
 * @property integer $need_look_service_video
 * @property string $access_token
 * @property integer $socket_id
 * @property string $chat_photo
 * @property integer $chat_volume
 * @property integer $duplicate_notification_to_sms
 * @property integer $duplicate_notification_to_email
 * @property integer $push_notification_new_message
 * @property integer $push_notification_create_closest_document
 * @property integer $push_notification_overdue_invoice
 * @property integer $show_scan_popup
 * @property string $push_token
 *
 * @property Company $company
 * @property EmployeeRole $employeeRole
 * @property EmployeeCompany[] $employeeCompanies
 * @property Company[] $companies
 * @property EmployeeDevice[] $employeeDevices
 */
class Employee extends \yii\db\ActiveRecord
{
    public $shortFio = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Компания',
            'email' => 'Email',
            'password' => 'Пароль',
            'created_at' => 'Дата создания',
            'fio' => 'ФИО',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'firstname_initial' => 'Инициал имени',
            'patronymic' => 'Отчество',
            'patronymic_initial' => 'Инициал отчества',
            'sex' => 'Пол',
            'birthday' => 'Дата рождения',
            'date_hiring' => 'Дата приёма на работу',
            'date_dismissal' => 'Дата увольнения',
            'position' => 'Должность',
            'is_working' => 'Работает',
            'is_deleted' => 'Удалён',
            'employee_role_id' => 'Роль',
            'phone' => 'Телефон',
            'notify_nearly_report' => 'Оповещать о приближающихся отчётах',
            'notify_new_features' => 'Оповещать о новых функциях сервиса',
            'time_zone_id' => 'Часовой пояс',
            'view_notification_date' => 'Дата просмотра новостей',
            'last_visit_at' => 'Дата последней активности',
            'wp_id' => 'Wp ID',
            'duplicate_notification_to_sms' => 'Дублировать уведомления по SMS',
            'duplicate_notification_to_email' => 'Дублировать уведомления по электронной почте',
            'push_notification_new_message' => 'Push-уведомление по новому сообщению в чате',
            'push_notification_create_closest_document' => 'Push-уведомление по необходимости создания закрывающего документа',
            'push_notification_overdue_invoice' => 'Push-уведомление по появлению просроченного счета',
            'push_token' => 'Push token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeRole()
    {
        return $this->hasOne(EmployeeRole::className(), ['id' => 'employee_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompanies()
    {
        return $this->hasMany(EmployeeCompany::className(), ['employee_id' => 'id'])
                ->andOnCondition(['employee_company.is_working' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])
                ->via('employeeCompanies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeDevices()
    {
        return $this->hasMany(EmployeeDevice::className(), ['employee_id' => 'id']);
    }

    /**
     * Returns FIO
     * @param bool|false $short
     * @return string
     */
    public function getFio()
    {
        return join(' ', array_filter([
            $this->lastname,
            mb_substr($this->firstname, 0, $this->shortFio ? 1 : null) . ($this->shortFio ? '.' : null),
            mb_substr($this->patronymic, 0, $this->shortFio ? 1 : null) . ($this->shortFio ? '.' : null),
        ], function ($value) {
            return $value && $value != '.';
        }));
    }
}
