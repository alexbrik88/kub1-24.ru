<?php

namespace backend\modules\unisender\models;

use common\components\sender\unisender\UniSender;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * EmployeeSearch represents the model behind the search form about `backend\modules\unisender\models\Employee`.
 */
class EmployeeSearch extends Employee
{
    public $contact_name;
    public $company;
    public $test;
    public $blocked;
    public $list_ids;

    protected $_searchParams = [
        'list_ids',
        'is_working',
        'employee_role_id',
        'company',
        //'last_visit_at',
        'email',
        'contact_name',
        'position',
        'phone',
        'test',
        'blocked',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->_searchParams, 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function getSearchParams()
    {
        return $this->_searchParams;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'list_ids' => 'ID списка',
            'test' => 'Тест',
            'contact_name' => 'ФИО',
            'company' => 'Компания',
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $eagerLoading = true)
    {
        $query = Employee::find();

        $query->joinWith('companies', $eagerLoading)
            ->andWhere(['employee.is_deleted' => false])
            ->groupBy('employee.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'company',
                    'email',
                    'lastname',
                    'phone',
                    'is_working' => [
                        'asc' => [
                            'employee_company.is_working' => SORT_ASC,
                        ],
                        'desc' => [
                            'employee_company.is_working' => SORT_DESC,
                        ],
                    ],
                    'employee_role_id' => [
                        'asc' => [
                            'employee_company.employee_role_id' => SORT_ASC,
                        ],
                        'desc' => [
                            'employee_company.employee_role_id' => SORT_DESC,
                        ],
                    ],
                    'position' => [
                        'asc' => [
                            new Expression('IF({{employee_company}}.[[position]] = "" OR {{employee_company}}.[[position]] IS NULL,1,0)'),
                            'employee_company.position' => SORT_ASC,
                        ],
                        'desc' => [
                            'employee_company.position' => SORT_DESC,
                        ],
                    ],
                    'test' => [
                        'asc' => [
                            'company.test' => SORT_ASC,
                        ],
                        'desc' => [
                            'company.test' => SORT_DESC,
                        ],
                    ],
                    'blocked' => [
                        'asc' => [
                            'company.blocked' => SORT_ASC,
                        ],
                        'desc' => [
                            'company.blocked' => SORT_DESC,
                        ],
                    ],
                    'company' => [
                        'asc' => [
                            new Expression('IF({{company}}.[[id]] IS NULL,1,0)'),
                        ],
                        'desc' => [
                            new Expression('IF({{company}}.[[id]] IS NULL,0,1)'),
                        ],
                    ],
                    'contact_name' => [
                        'asc' => [
                            new Expression('IF({{employee}}.[[lastname]] = "" OR {{employee}}.[[lastname]] IS NULL,1,0)'),
                            'employee.lastname' => SORT_ASC,
                            'employee.firstname' => SORT_ASC,
                            'employee.patronymic' => SORT_ASC,
                        ],
                        'desc' => [
                            'employee.lastname' => SORT_DESC,
                            'employee.firstname' => SORT_DESC,
                            'employee.patronymic' => SORT_DESC,
                        ],
                    ],
                ],
            ],
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'employee.id' => $this->id,
            'employee.sex' => $this->sex,
            'employee.birthday' => $this->birthday,
            'employee_company.is_working' => $this->is_working,
            'employee_company.employee_role_id' => $this->employee_role_id,
            'employee.is_active' => $this->is_active,
            'employee.is_deleted' => $this->is_deleted,
            'company.test' => $this->test,
            'company.blocked' => $this->blocked,
        ]);

        $query->andFilterWhere(['like', 'employee.email', $this->email])
            ->andFilterWhere([
                'or',
                ['company.id' => $this->company],
                ['like', 'company.name_short', $this->company],
            ])
            ->andFilterWhere([
                'or',
                ['like', 'employee.lastname', $this->contact_name],
                ['like', 'employee.firstname', $this->contact_name],
                ['like', 'employee.patronymic', $this->contact_name],
            ])
            ->andFilterWhere(['like', 'employee_company.position', $this->position])
            ->andFilterWhere(['like', 'employee.phone', $this->phone]);

        return $dataProvider;
    }

    /**
     * Import contacts to Unisender
     *
     * @param array $params
     */
    public function import($params)
    {
        $dataProvider = $this->search($params, false);
        $query = clone $dataProvider->query;
        $query->with = [];
        $query->select([
            "employee.email",
            "employee.firstname",
        ])->andWhere([
            'employee.is_mailing_active' => true,
        ])->asArray();

        Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

        $uniSender = new UniSender();
        $offset = 0;
        $summary = [
            "total" => 0,
            "inserted" => 0,
            "updated" => 0,
            "deleted" => 0,
            "new_emails" => 0,
            "invalid" => 0,
        ];

        do {
            $query
                ->limit(UniSender::EMPLOYEE_SELECT_LIMIT)
                ->offset($offset++ * UniSender::EMPLOYEE_SELECT_LIMIT);

            $contactArray = $query->all();

            if ($contactArray) {
                $importData = [
                    'field_names' => [
                        'email',
                        'email_list_ids',
                        'Name',
                    ],
                    'data' => [],
                ];
                foreach ($contactArray as $contact) {
                    $importData['data'][] = [
                        $contact['email'],
                        $this->list_ids,
                        $contact['firstname'],
                    ];
                }
                $response = $uniSender->importContacts($importData);
                $data = json_decode($response, true);
                if (isset($data['result']) && is_array($data['result'])) {
                    $summary['total'] += ArrayHelper::getValue($data['result'], 'total');
                    $summary['inserted'] += ArrayHelper::getValue($data['result'], 'inserted');
                    $summary['updated'] += ArrayHelper::getValue($data['result'], 'updated');
                    $summary['deleted'] += ArrayHelper::getValue($data['result'], 'deleted');
                    $summary['new_emails'] += ArrayHelper::getValue($data['result'], 'new_emails');
                    $summary['invalid'] += ArrayHelper::getValue($data['result'], 'invalid');
                } else {
                    Yii::warning($response);
                }
            }
        } while ($contactArray);

        return $summary;
    }
}
