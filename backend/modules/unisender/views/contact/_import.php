<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\unisender\models\EmployeeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="import-contact import-form collapse in">

    <?php $form = ActiveForm::begin([
        'id' => 'import-contact-form',
        'action' => ['import'],
        'method' => 'post',
        'options' => [
            'class' => 'import-contact-form',
        ],
    ]); ?>

    <?php foreach ($model->searchParams as $attribute) : ?>
        <?= Html::activeHiddenInput($model, $attribute) ?>
    <?php endforeach; ?>

    <?= $form->field($model, 'list_ids')->textInput(); ?>

    <div class="">
        <?= Html::submitButton('Отправить', [
            'class' => 'btn darkblue text-white submit mt-ladda-btn ladda-button',
            'data-style' => 'expand-right',
        ]) ?>
        <?= Html::button('Отмена', [
            'class' => 'btn darkblue text-white pull-right',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div class="import-contact import-result collapse">

    <table class="table">
        <tr>
            <td><label>Отправлено</label></td>
            <td><span class="result-total"></span></td>
        </tr>
        <tr>
            <td><label>Добавлено</label></td>
            <td><span class="result-inserted"></span></td>
        </tr>
        <tr>
            <td><label>Обновлено</label></td>
            <td><span class="result-updated"></span></td>
        </tr>
        <tr>
            <td><label>Удалено</label></td>
            <td><span class="result-deleted"></span></td>
        </tr>
        <tr>
            <td><label>Новых</label></td>
            <td><span class="result-new_emails"></span></td>
        </tr>
        <tr>
            <td><label>Не добавлено</label></td>
            <td><span class="result-invalid"></span></td>
        </tr>
    </table>

    <div class="form-group">
        <?= Html::button('Закрыть', [
            'class' => 'btn darkblue text-white pull-right',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>
</div>
