<?php

use common\models\employee\EmployeeRole;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\unisender\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Импорт контактов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Импорт', [
            'class' => 'btn darkblue text-white',
            'data' => [
                'toggle' => 'modal',
                'target' => '#import-contacts-modal',
            ],
        ]) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'email',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'format' => 'email',
            ],
            [
                'attribute' => 'contact_name',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'label' => 'ФИО',
                'value' => 'fio',
            ],
            [
                'attribute' => 'position',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    $value = '';
                    foreach ($model->employeeCompanies as $item) {
                        $value .= Html::tag('div', Html::encode($item->position), [
                            'style' => 'max-width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ]);
                    }

                    return $value;
                },
            ],
            [
                'attribute' => 'employee_role_id',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'label' => 'Роль',
                'filter' => EmployeeRole::find()->actual()->select('name')->indexBy('id')->column(),
                'format' => 'raw',
                'value' => function ($model) {
                    $value = '';
                    foreach ($model->employeeCompanies as $item) {
                        $value .= Html::tag('div', $item->employeeRole->name, [
                            'style' => 'max-width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ]);
                    }

                    return $value;
                },
            ],
            [
                'attribute' => 'company',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'label' => 'Компания',
                'format' => 'raw',
                'value' => function ($model) {
                    $value = '';
                    foreach ($model->employeeCompanies as $item) {
                        $link = Html::a(Html::encode($item->company->getTitle(true)), [
                            '/company/company/view',
                            'id' => $item->company_id,
                        ]);
                        $value .= Html::tag('div', $link, [
                            'style' => 'max-width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ]);
                    }

                    return $value;
                },
            ],
            [
                'attribute' => 'is_working',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'format' => 'html',
                'value' => function ($model) {
                    $value = '';
                    foreach ($model->employeeCompanies as $item) {
                        $value .= Html::tag('div', $item->is_working ? 'Да' : 'Нет');
                    }

                    return $value;
                },
                'filter' => [1 => 'Да', 0 => 'Нет'],
            ],
            [
                'attribute' => 'test',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'format' => 'html',
                'value' => function ($model) {
                    $value = '';
                    foreach ($model->employeeCompanies as $item) {
                        $value .= Html::tag('div', $item->company->test ? 'Test' : 'Real');
                    }

                    return $value;
                },
                'filter' => [1 => 'Test', 0 => 'Real'],
            ],
            [
                'attribute' => 'blocked',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'format' => 'html',
                'value' => function ($model) {
                    $value = '';
                    foreach ($model->employeeCompanies as $item) {
                        $value .= Html::tag('div', $item->company->blocked ? 'Blocked' : 'Active');
                    }

                    return $value;
                },
                'filter' => [0 => 'Active', 1 => 'Blocked'],
            ],
        ],
    ]); ?>
</div>

<?php Modal::begin([
    'id' => 'import-contacts-modal',
    'header' => '<h1>Импорт контактов</h1>',
    'toggleButton' => [
        'label' => 'Импорт',
        'class' => 'btn darkblue text-white'
    ],
]); ?>
<div id="import-contacts-content">
    <?= $this->render('_import', ['model' => $searchModel]); ?>
</div>
<?php Modal::end(); ?>

<?php

$js = <<<JS
$('.import-contact').collapse({
    toggle: false
});
$(document).on('submit', '.import-contact-form', function(e) {
    e.preventDefault();
    var form = this;
    $.ajax({
        type: "post",
        url: form.action,
        data: $(form).serialize(),
        success: function(data) {
            Ladda.stopAll();
            $('span.result-total').text(data.total);
            $('span.result-inserted').text(data.inserted);
            $('span.result-updated').text(data.updated);
            $('span.result-deleted').text(data.deleted);
            $('span.result-new_emails').text(data.new_emails);
            $('span.result-invalid').text(data.invalid);
            $('.import-contact.import-form').collapse('hide');
            $('.import-contact.import-result').collapse('show');
        }
    });
});
$(document).on('hidden.bs.modal, show.bs.modal', '#import-contacts-modal', function(e) {
    Ladda.stopAll();
    $('.import-contact.import-form').collapse('show');
    $('.import-contact.import-result').collapse('hide');
});
JS;

$this->registerJs($js);
