<?php

namespace backend\modules\unisender\controllers;

use Yii;
use backend\modules\unisender\models\Employee;
use backend\modules\unisender\models\EmployeeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;

/**
 * ContactController implements the CRUD actions for Employee model.
 */
class ContactController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'import' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => 'common\components\filters\AjaxFilter',
                'only' => ['import'],
            ],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeeSearch([
            'is_working' => 1,
            'test' => 0,
            'blocked' => 0,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionImport()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $searchModel = new EmployeeSearch();
        $result = $searchModel->import(Yii::$app->request->post());

        return $result;
    }
}
