<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.08.2017
 * Time: 15:50
 */

namespace backend\modules\service\models;


use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class SellingInvoiceSearch
 * @package backend\modules\service\models
 */
class SellingInvoiceSearch extends Invoice
{
    /**
     * @var
     */
    public $search;

    /**
     * @var
     */
    public $paymentType;

    /**
     * @var
     */
    public $companyNameShort;

    /**
     * @var
     */
    public $tariffType;

    /**
     * @var
     */
    public $subscribeStatus;

    /**
     * @var
     */
    public $contractorName;

    /**
     *
     */
    const SUBSCRIBE_STATUS_CREATED = 1;

    /**
     *
     */
    const SUBSCRIBE_STATUS_PAYED = 2;

    /**
     *
     */
    const SUBSCRIBE_STATUS_OVERDUE = 3;

    /**
     * @var array
     */
    public static $subscribeStatusArray = [
        self::SUBSCRIBE_STATUS_CREATED => 'Выставлен',
        self::SUBSCRIBE_STATUS_PAYED => 'Оплачен',
        self::SUBSCRIBE_STATUS_OVERDUE => 'Просрочен',
    ];

    /**
     * @var array
     */
    public static $subscribeStatusDashBoardArray = [
        self::SUBSCRIBE_STATUS_CREATED => 'Выставлено',
        self::SUBSCRIBE_STATUS_PAYED => 'Оплачено',
        self::SUBSCRIBE_STATUS_OVERDUE => 'Просрочено',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'safe'],
            [['paymentType', 'document_author_id', 'companyNameShort',
                'tariffType', 'subscribeStatus', 'contractorName'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = $this->getBaseQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'document_date',
                    'document_number' => [
                        'asc' => [
                            'abs(document_number)' => SORT_ASC,
                        ],
                        'desc' => [
                            'abs(document_number)' => SORT_DESC,
                        ],
                    ],
                    'total_amount_with_nds',
                    'document_author_id',
                    'subscribe_id',
                    'payment_date',
                ],
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getPaymentTypeFilter()
    {
        $result[null] = 'Все';
        foreach ($this->getBaseQuery()
                     ->select(Payment::tableName() . '.type_id as payment_type')
                     ->groupBy(Payment::tableName() . '.type_id')
                     ->column() as $paymentTypeID) {
            $paymentType = PaymentType::findOne($paymentTypeID);
            $result[$paymentTypeID] = $paymentType->name;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getContractorFilter()
    {
        return ArrayHelper::merge([null => 'Все'],
            ArrayHelper::map($this->getBaseQuery()
                ->joinWith('contractor')
                ->leftJoin(CompanyType::tableName(), CompanyType::tableName() . '.id = ' . Contractor::tableName() . '.company_type_id')
                ->orderBy(CompanyType::tableName() . '.name_short, ' . Contractor::tableName() . '.name')
                ->groupBy('contractor_id')
                ->all(), 'contractor_id', 'contractor.nameWithType'));
    }

    /**
     * @return array
     */
    public function getCompanyFilter()
    {
        return ArrayHelper::merge([null => 'Все'],
            ArrayHelper::map($this->getBaseQuery()
                ->leftJoin(['subscribeCompany' => Company::tableName()], Company::tableName() . '.id = ' . Payment::tableName() . '.company_id')
                ->leftJoin(CompanyType::tableName(), CompanyType::tableName() . '.id = ' . Company::tableName() . '.company_type_id')
                ->orderBy(CompanyType::tableName() . '.name_short, ' . Company::tableName() . '.name_short')
                ->all(), 'payment.company_id', 'payment.company.shortName'));
    }

    /**
     * @return array
     */
    public function getInvoiceStatusFilter()
    {
        return ArrayHelper::merge([null => 'Все'], self::$subscribeStatusArray);
    }

    /**
     * @return array
     */
    public function getTariffFilter()
    {
        $result[null] = 'Все';
        foreach ($this->getBaseQuery()
                     ->select(Payment::tableName() . '.tariff_id as tariff')
                     ->groupBy(Payment::tableName() . '.tariff_id')
                     ->column() as $tariffID) {
            $tariff = SubscribeTariff::findOne($tariffID);
            if ($tariff) {
                $result[$tariffID] = $tariff->tariffGroup->name .' '. $tariff->getTariffName();
            }
        }

        return $result;
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        $query = Invoice::find()
            ->joinWith('subscribe')
            ->joinWith('payment')
            ->joinWith('company')
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted(Invoice::NOT_IS_DELETED)
            ->andWhere(['and',
                ['is_subscribe_invoice' => 1],
                ['not', ['service_payment_id' => null]],
            ])
            ->andWhere(['between', Invoice::tableName() . '.document_date', $dateRange['from'], $dateRange['to']]);

        $query->andFilterWhere([Payment::tableName() . '.type_id' => $this->paymentType]);

        $query->andFilterWhere([Invoice::tableName() . '.contractor_id' => $this->contractorName]);

        $query->andFilterWhere([Payment::tableName() . '.company_id' => $this->companyNameShort]);

        $query->andFilterWhere([Payment::tableName() . '.tariff_id' => $this->tariffType]);

        switch ($this->subscribeStatus) {
            case self::SUBSCRIBE_STATUS_CREATED:
                $query->andWhere(['and',
                    ['<=', 'DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(`' . Payment::tableName() . '`.`created_at`)))', 10],
                    ['is_confirmed' => 0],
                ]);
                break;
            case self::SUBSCRIBE_STATUS_OVERDUE:
                $query->andWhere(['and',
                    ['>', 'DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(`' . Payment::tableName() . '`.`created_at`)))', 10],
                    ['is_confirmed' => 0],
                ]);
                break;
            case self::SUBSCRIBE_STATUS_PAYED:
                $query->andWhere([Payment::tableName() . '.is_confirmed' => 1]);
                break;
        }

        if (filter_var($this->search, FILTER_VALIDATE_EMAIL)) {
            $employee = Employee::findOne(['email' => $this->search]);
            $query->andFilterWhere(['OR',
                [Company::tableName() . '.email' => $this->search],
                [Company::tableName() . '.id' => $employee ? $employee->my_companies : null],
            ]);
        } else {
            $query->andFilterWhere(['OR',
                ['like', Company::tableName() . '.inn', $this->search],
                ['like', Company::tableName() . '.id', $this->search],
                ['like', Company::tableName() . '.name_short', $this->search],
            ]);
        }

        return $query;
    }
}