<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 12/21/15
 * Time: 12:39 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace backend\modules\service\models;
use common\models\service\PromoCode;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class PromoCodeStatisticSearch
 * @package backend\modules\service\models
 */
class PromoCodeStatisticSearch extends PromoCode
{
    public function rules()
    {
        return [
            [['group_id', 'tariff_group_id', 'for_company_experience_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return Model::behaviors();
    }

    public function search($params)
    {
        $query = PromoCode::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        $query->from(['promo-code' => PromoCode::tableName()]);
        $query->joinWith('payments', true);
        $query->groupBy(['promo-code.id']);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'group_id' => $this->group_id,
            'tariff_group_id' => $this->tariff_group_id,
            'for_company_experience_id' => $this->for_company_experience_id,
        ]);

        return $dataProvider;
    }

}
