<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.07.2017
 * Time: 8:39
 */

namespace backend\modules\service\models;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\company\CompanyType;
use yii\data\ActiveDataProvider;

class AffiliateSearch extends Company
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name_short'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find()
            ->andWhere(['not', ['company.affiliate_link_created_at' => null]])
            ->isBlocked(Company::UNBLOCKED);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'invited_companies_count' => SORT_DESC,
                ],
                'attributes' => [
                    'affiliate_link_created_at',
                    'total_affiliate_sum',
                    'payed_reward_for_kub',
                    'payed_reward_for_rs',
                    'payed_reward_for_card',
                    'affiliate_sum',
                    'invited_companies_count',
                    'active_reward_requests',
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere(['id' => $this->name_short]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getCompanyFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(Company::find()
            ->andWhere(['not', ['company.affiliate_link_created_at' => null]])
            ->isBlocked(Company::UNBLOCKED)
            ->leftJoin(CompanyType::tableName(), CompanyType::tableName() . '.id = ' . Company::tableName() . '.company_type_id')
            ->orderBy(CompanyType::tableName() . '.name_short, ' . Company::tableName() . '.name_short')
            ->groupBy(Company::tableName() . '.name_short')
            ->all(), 'id', 'shortName'));
    }
}