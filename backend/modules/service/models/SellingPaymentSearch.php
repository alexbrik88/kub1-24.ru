<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.08.2017
 * Time: 10:19
 */

namespace backend\modules\service\models;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class SellingPaymentSearch
 * @package backend\modules\service\models
 */
class SellingPaymentSearch extends Payment
{
    const ORGANIC_GOOGLE_TYPE = 'google / organic';
    const ORGANIC_YANDEX_TYPE = 'yandex / organic';
    const ORGANIC_VK_TYPE = 'vk / organic';
    const ORGANIC_FACEBOOK_TYPE = 'facebook / organic';

    const CPC_GOOGLE_TYPE = 'google / cpc';
    const CPC_YANDEX_TYPE = 'yandex / cpc';
    const CPC_VK_TYPE = 'vk / cpc';
    const CPC_FACEBOOK_TYPE = 'facebook / cpc';

    const EMAIL_WP = 'wp_server / email';
    const EMAIL_UNISENDER = 'unisender / email';

    const CHANNEL_DIRECT_TYPE = '(direct) / (none)';
    const CHANNEL_REFERRAL_TYPE = '/ referral';
    const CHANNEL_BLOG_TYPE = '/ blog';
    const CHANNEL_NOT_SET = 'not set';

    /**
     * @var array
     */
    public $organicTypes = [
        self::ORGANIC_GOOGLE_TYPE => 'Google',
        self::ORGANIC_YANDEX_TYPE => 'Yandex',
        self::ORGANIC_VK_TYPE => 'Вконтакте',
        self::ORGANIC_FACEBOOK_TYPE => 'Facebook',
    ];

    /**
     * @var array
     */
    public $cpcTypes = [
        self::CPC_GOOGLE_TYPE => 'Google',
        self::CPC_YANDEX_TYPE => 'Yandex',
        self::CPC_VK_TYPE => 'Вконтакте',
        self::CPC_FACEBOOK_TYPE => 'Facebook',
    ];

    /**
     * @var array
     */
    public $emailTypes = [
        self::EMAIL_WP => 'Wp_server',
        self::EMAIL_UNISENDER => 'Unisender',
    ];

    /**
     * @var array
     */
    public $channelTypes = [
        self::CHANNEL_DIRECT_TYPE => 'Direct',
        self::CHANNEL_REFERRAL_TYPE => 'Referral',
        self::CHANNEL_BLOG_TYPE => 'Blog',
        self::CHANNEL_NOT_SET => 'not SET',
    ];

    /**
     * @var
     */
    public $search;

    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $outContractor;

    /**
     * @var
     */
    public $companyNameShort;

    /**
     * @var
     */
    public $tariff;

    /**
     * @var
     */
    public $subscribeNumber;

    /**
     * @var
     */
    public $serial_number;

    /**
     * @var
     */
    public $form_source;

    /**
     * @var
     */
    public $form_region;

    /**
     * @var
     */
    public $registrationPageType;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search', 'form_source', 'form_region'], 'safe'],
            [['type', 'outContractor', 'companyNameShort',
                'subscribeNumber', 'serial_number'], 'integer'],
            [['tariff', 'registrationPageType'], 'string'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->getBaseQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    Payment::tableName() . '.id',
                    Company::tableName() . '.created_at',
                    Company::tableName() . '.form_source',
                    Company::tableName() . '.form_keyword',
                    Company::tableName() . '.form_region',
                    'payment_date',
                    'invoice.document_date',
                    'invoice.document_number' => [
                        'asc' => [
                            'abs(invoice.document_number)' => SORT_ASC,
                        ],
                        'desc' => [
                            'abs(invoice.document_number)' => SORT_DESC,
                        ],
                    ],
                    'sum' => [
                        'asc' => [
                            'abs(sum)' => SORT_ASC,
                        ],
                        'desc' => [
                            'abs(sum)' => SORT_DESC,
                        ],
                    ],
                ],
                'defaultOrder' => [
                    Payment::tableName() . '.id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        $this->filterQuery($query);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getTypeFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->joinWith('type')
            ->groupBy('type_id')
            ->all(), 'type_id', 'type.name'));
    }

    /**
     * @return array
     */
    public function getEmployeeFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->leftJoin(CompanyType::tableName(), CompanyType::tableName() . '.id = ' . Contractor::tableName() . '.company_type_id')
            ->orderBy(CompanyType::tableName() . '.name_short, ' . Contractor::tableName() . '.name')
            ->groupBy(Invoice::tableName() . '.contractor_id')
            ->all(), 'outInvoice.contractor_id', 'outInvoice.contractor.nameWithType'));
    }

    /**
     * @return array
     */
    public function getCompanyFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->leftJoin(CompanyType::tableName(), CompanyType::tableName() . '.id = ' . Company::tableName() . '.company_type_id')
            ->orderBy(CompanyType::tableName() . '.name_short, ' . Company::tableName() . '.name_short')
            ->groupBy(Company::tableName() . '.id')
            ->all(), Company::tableName() . '.id', Company::tableName() . '.shortName'));
    }

    /**
     * @return array
     */
    public function getTariffFilter()
    {
        $filter[null] = 'Все';
        $subscribeTariffs = [];
        foreach ($this->getBaseQuery()
                     ->select('tariff_id')
                     ->andWhere(['not', ['tariff_id' => null]])
                     ->groupBy('tariff_id')
                     ->column() as $tariffID) {
            $tariff = SubscribeTariff::findOne($tariffID);
            if ($tariff) {
                $subscribeTariffs[$tariffID] = $tariff->tariffGroup->name .' '. $tariff->getTariffName();
            }
        }

        $storeTariffs = ArrayHelper::map($this->getBaseQuery()
            ->andWhere(['not', ['store_tariff_id' => null]])
            ->joinWith('storeTariff')
            ->groupBy('store_tariff_id')
            ->all(), 'store_tariff_id', 'storeTariff.cabinets_count');
        $outInvoiceTariffs = ArrayHelper::map($this->getBaseQuery()
            ->andWhere(['not', ['out_invoice_tariff_id' => null]])
            ->joinWith('outInvoiceTariff')
            ->groupBy('out_invoice_tariff_id')
            ->all(), 'out_invoice_tariff_id', 'outInvoiceTariff.links_count');
        $invoiceTariffs = ArrayHelper::map($this->getBaseQuery()
            ->andWhere(['not', ['invoice_tariff_id' => null]])
            ->joinWith('invoiceTariff')
            ->groupBy('invoice_tariff_id')
            ->all(), 'invoice_tariff_id', 'invoiceTariff.invoice_count');
        foreach ($subscribeTariffs as $tariffID => $subscribeTariffName) {
            $filter['subscribe' . $tariffID] = $subscribeTariffName;
        }
        foreach ($invoiceTariffs as $tariffID => $invoiceTariffName) {
            $filter['invoice' . $tariffID] = 'Счетов: ' . $invoiceTariffName;
        }
        foreach ($storeTariffs as $tariffID => $storeTariffName) {
            $filter['store' . $tariffID] = 'Кабинетов: ' . $storeTariffName;
        }
        foreach ($outInvoiceTariffs as $tariffID => $outInvoiceTariffName) {
            $filter['outInvoice' . $tariffID] = 'Ссылок: ' . $outInvoiceTariffName;
        }
        if ($this->getBaseQuery()->andWhere(['payment_for' => Payment::FOR_ODDS])->exists()) {
            $filter['odds'] = 'Настройка финансовых отчетов';
        }

        return $filter;
    }

    /**
     * @return array
     */
    public function getSubscribeNumberFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->andWhere(['not', ['serial_number' => null]])
            ->groupBy('serial_number')
            ->all(), 'serial_number', 'serial_number'));
    }

    /**
     * @return array
     */
    public function getFormSourceFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->andWhere(['not', [Company::tableName() . '.form_source' => null]])
            ->groupBy(Company::tableName() . '.form_source')
            ->asArray()
            ->all(), 'company.form_source', 'company.form_source'));
    }

    /**
     * @return array
     */
    public function getFormRegionFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->andWhere(['not', [Company::tableName() . '.form_region' => null]])
            ->groupBy(Company::tableName() . '.form_region')
            ->asArray()
            ->all(), 'company.form_region', 'company.form_region'));
    }

    /**
     * @return mixed
     */
    public function getRegistrationPageTypeFilter()
    {
        $registrationPageTypeFilter[null] = 'Все';
        $registrationPageTypeFilter[-1] = 'Не указано';

        $registrationPageTypeFilter = $registrationPageTypeFilter + ArrayHelper::map($this->getBaseQuery()
                ->select(['*', 'registration_page_type.name as registrationPageTypeName'])
                ->andWhere(['not', [Company::tableName() . '.registration_page_type_id' => null]])
                ->groupBy(Company::tableName() . '.registration_page_type_id')
                ->asArray()
                ->all(), 'registration_page_type_id', 'registrationPageTypeName');

        return $registrationPageTypeFilter;
    }

    /**
     * @param $formSource
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getStatisticByFormSource($formSource)
    {
        $query = $this->getBaseQuery()
            ->select([
                Payment::tableName() . '.id',
                Payment::tableName() . '.company_id',
                'COUNT(' . Payment::tableName() . '.id) as count',
                'SUM(' . Payment::tableName() . ' . sum) as totalSum',
            ]);
        if (in_array($formSource, [self::CHANNEL_REFERRAL_TYPE, self::CHANNEL_BLOG_TYPE,])) {
            $query->andWhere(['like', Company::tableName() . '.form_source', $formSource]);
        } elseif ($formSource == self::CHANNEL_NOT_SET) {
            $query->andWhere(['or',
                    [Company::tableName() . '.form_source' => null],
                    ['and',
                        ['not', ['in', Company::tableName() . '.form_source', [
                            self::ORGANIC_GOOGLE_TYPE, self::ORGANIC_YANDEX_TYPE, self::ORGANIC_VK_TYPE,
                            self::ORGANIC_FACEBOOK_TYPE, self::CPC_GOOGLE_TYPE, self::CPC_YANDEX_TYPE,
                            self::CPC_VK_TYPE, self::CPC_FACEBOOK_TYPE, self::EMAIL_WP, self::EMAIL_UNISENDER, self::CHANNEL_DIRECT_TYPE,
                        ]]],
                        ['not', ['like', Company::tableName() . '.form_source', self::CHANNEL_REFERRAL_TYPE]],
                        ['not', ['like', Company::tableName() . '.form_source', self::CHANNEL_BLOG_TYPE]],
                    ],
                ]
            );
        } else {
            $query->andWhere([Company::tableName() . '.form_source' => $formSource]);
        }
        $query = $this->filterQuery($query);

        return $query->asArray()->one();
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getCampaigns()
    {
        $query = $this->getBaseQuery();
        $query = $this->filterQuery($query);

        return $query->asArray()->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();
        $from = (new \DateTime($dateRange['from']))->modify('today')->getTimestamp();
        $to = (new \DateTime($dateRange['to']))->modify('tomorrow')->modify('1 second ago')->getTimestamp();

        return Payment::find()
            ->joinWith('outInvoice')
            ->joinWith('company')
            ->leftJoin(RegistrationPageType::tableName(), RegistrationPageType::tableName() . '.id = ' . Company::tableName() . '.registration_page_type_id')
            ->leftJoin(Employee::tableName(), Employee::tableName() . '.id = ' . Invoice::tableName() . '.document_author_id')
            ->leftJoin(Contractor::tableName(), Contractor::tableName() . '.id = ' . Invoice::tableName() . '.contractor_id')
            ->andWhere(['is_confirmed' => 1])
            ->andWhere(['not', ['type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->andWhere(['or',
                ['and',
                    ['in', 'payment_for', [Payment::FOR_SUBSCRIBE, Payment::FOR_TAXROBOT]],
                    ['not', ['tariff_id' => SubscribeTariff::TARIFF_TRIAL]],
                ],
                ['in', 'payment_for', [Payment::FOR_OUT_INVOICE, Payment::FOR_STORE_CABINET, Payment::FOR_ODDS, Payment::FOR_ADD_INVOICE]],
            ])
            ->andWhere(['between', Payment::tableName() . '.payment_date', $from, $to]);
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    public function filterQuery(ActiveQuery $query)
    {
        $query->andFilterWhere(['type_id' => $this->type]);

        $query->andFilterWhere([Invoice::tableName() . '.contractor_id' => $this->outContractor]);

        $query->andFilterWhere([Company::tableName() . '.id' => $this->companyNameShort]);

        $tariffID = preg_replace("/[^0-9]/", '', $this->tariff);
        if (strpos($this->tariff, 'subscribe') !== false) {
            $query->andFilterWhere(['tariff_id' => $tariffID]);
        } elseif (strpos($this->tariff, 'invoice') !== false) {
            $query->andFilterWhere(['invoice_tariff_id' => $tariffID]);
        } elseif (strpos($this->tariff, 'store') !== false) {
            $query->andFilterWhere(['store_tariff_id' => $tariffID]);
        } elseif (strpos($this->tariff, 'outInvoice') !== false) {
            $query->andFilterWhere(['out_invoice_tariff_id' => $tariffID]);
        } elseif ($this->tariff == 'odds') {
            $query->andWhere(['payment_for' => Payment::FOR_ODDS]);
        }

        $query->andFilterWhere(['serial_number' => $this->serial_number]);

        $query->andFilterWhere([Company::tableName() . '.form_source' => $this->form_source]);

        $query->andFilterWhere([Company::tableName() . '.form_region' => $this->form_region]);

        if ($this->registrationPageType == -1) {
            $query->andWhere([Company::tableName() . '.registration_page_type_id' => null]);
        } else {
            $query->andFilterWhere([Company::tableName() . '.registration_page_type_id' => $this->registrationPageType]);
        }

        return $query;
    }
}