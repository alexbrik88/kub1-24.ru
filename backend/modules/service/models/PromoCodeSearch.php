<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 7.12.15
 * Time: 11.46
 * Email: t.kanstantsin@gmail.com
 */

namespace backend\modules\service\models;

use common\models\service\PromoCode;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PromoCodeSearchSearch represents the model behind the search form about `common\models\service\PromoCode`.
 */
class PromoCodeSearch extends PromoCode
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'tariff_group_id', 'for_company_experience_id', 'code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PromoCode::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'activated_at' => SORT_DESC,
                ],
                'attributes' => [
                    'activated_at',
                    'created_at',
                    'code',
                    'company_id',
                    'name',
                    'started_at',
                    'expired_at',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'group_id' => $this->group_id,
            'tariff_group_id' => $this->tariff_group_id,
            'for_company_experience_id' => $this->for_company_experience_id,
        ]);

        $query->andFilterWhere([
            'like', 'code', $this->code,
        ]);

        return $dataProvider;
    }
}