<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.07.2017
 * Time: 11:27
 */

namespace backend\modules\service\models;


use common\models\service\RewardRequest;
use yii\data\ActiveDataProvider;

/**
 * Class RewardRequestSearch
 * @package backend\modules\service\models
 */
class RewardRequestSearch extends RewardRequest
{
    /**
     * @param $companyID
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($companyID, $params)
    {
        $query = RewardRequest::find()
            ->andWhere(['company_id' => $companyID])
            ->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'id',
                ],
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}