<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.02.2017
 * Time: 9:25
 */

namespace backend\modules\service\models;


use common\models\service\Payment;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class PaymentSearch
 * @package backend\modules\service\models
 */
class PaymentSearch extends Payment
{
    public $search;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere(['type_id' => $this->type_id]);

        if (!empty($this->search)) {
            $query->joinWith(['company company']);
            $query->andFilterWhere([
                'or',
                ['company.id' => $this->search],
                ['company.inn' => $this->search],
                ['company.email' => $this->search],
                ['like', 'company.name_full', $this->search],
                ['like', 'company.name_short', $this->search],
            ]);
        }

        return $dataProvider;
    }

    public function getPaymentTypeFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(Payment::find()->joinWith('type')->orderBy('service_payment_type.id')->all(), 'type.id', 'type.name'));
    }
}