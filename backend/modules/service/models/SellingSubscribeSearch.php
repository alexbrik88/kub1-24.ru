<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.08.2017
 * Time: 7:20
 */

namespace backend\modules\service\models;


use common\models\Company;
use common\models\company\CompanyType;
use common\models\employee\Employee;
use common\models\selling\SellingSubscribe;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class SellingSubscribeSearch
 * @package backend\modules\service\models
 */
class SellingSubscribeSearch extends SellingSubscribe
{
    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $main_type;

    /**
     * @var
     */
    public $companyName;

    /**
     * @var
     */
    public $status = self::STATUS_ACTIVE;

    /**
     * @var
     */
    public $search;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'main_type', 'status', 'companyName'], 'integer'],
            [['search'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = $this->getBaseQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'invoice_number' => [
                        'asc' => [
                            'abs(selling_subscribe.invoice_number)' => SORT_ASC,
                        ],
                        'desc' => [
                            'abs(selling_subscribe.invoice_number)' => SORT_DESC,
                        ],
                    ],
                    'creation_date',
                    'activation_date',
                    'end_date',
                    'company.email',
                    'invoice_date' => [
                        'asc' => [
                            'invoice.document_date' => SORT_ASC,
                        ],
                        'desc' => [
                            'invoice.document_date' => SORT_DESC,
                        ],
                    ],
                ],
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getCompanyFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->leftJoin(CompanyType::tableName(), CompanyType::tableName() . '.id = ' . Company::tableName() . '.company_type_id')
            ->orderBy(CompanyType::tableName() . '.name_short, ' . Company::tableName() . '.name_short')
            ->groupBy(Company::tableName() . '.name_short')
            ->all(), 'company_id', 'company.shortName'));
    }

    /**
     * @return mixed
     */
    public function getTypeFilter()
    {
        $result[null] = 'Все';
        foreach (ArrayHelper::map($this->getBaseQuery()->groupBy(SellingSubscribe::tableName() . '.type')->all(), 'type', 'type') as $type) {
            $result[$type] = $this->typeArray[$type];
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getMainTypeFilter()
    {
        $result[null] = 'Все';
        foreach (ArrayHelper::map($this->getBaseQuery()->groupBy('main_type')->all(), 'main_type', 'main_type') as $mainType) {
            $result[$mainType] = $this->mainTypeArray[$mainType];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getStatusFilter()
    {
        return [
            null => 'Все',
            self::STATUS_ACTIVE => 'Активна',
            self::STATUS_NOT_ACTIVE => 'Неактивна',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        $query = SellingSubscribe::find()
            ->joinWith('company')
            ->joinWith('subscribe.payment')
            ->leftJoin('invoice', 'service_payment.id = invoice.service_payment_id')
            ->andWhere(['between', 'date(from_unixtime(' . SellingSubscribe::tableName() . '.creation_date))', $dateRange['from'], $dateRange['to']]);

        $query->andFilterWhere([SellingSubscribe::tableName() . '.type' => $this->type]);

        $query->andFilterWhere([SellingSubscribe::tableName() . '.main_type' => $this->main_type]);

        $query->andFilterWhere([SellingSubscribe::tableName() . '.company_id' => $this->companyName]);

        switch ($this->status) {
            case self::STATUS_ACTIVE:
                $query->andWhere(['or',
                    ['>=', 'end_date', time()],
                    ['end_date' => null],
                ]);
                break;
            case self::STATUS_NOT_ACTIVE:
                $query->andWhere(['<', 'end_date', time()]);
                break;
        }

        if (filter_var($this->search, FILTER_VALIDATE_EMAIL)) {
            $employee = Employee::findOne(['email' => $this->search]);
            $query->andFilterWhere(['OR',
                [Company::tableName() . '.email' => $this->search],
                [Company::tableName() . '.id' => $employee ? $employee->my_companies : null],
            ]);
        } else {
            $query->andFilterWhere(['OR',
                ['like', Company::tableName() . '.inn', $this->search],
                ['like', Company::tableName() . '.id', $this->search],
                ['like', Company::tableName() . '.name_short', $this->search],
            ]);
        }

        return $query;
    }
}