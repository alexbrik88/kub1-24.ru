<?php
namespace backend\modules\service\models;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\report\ReportState;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * Class SellingPaymentSearch
 * @package backend\modules\service\models
 */
class SummaryPaymentSearch
{
    /**
     * @var
     */
    public $year;

    /**
     * @var
     */
    public $month;

    /**
     * @var
     */
    public static $tariffByGroup;

    public function init() {

        self::$tariffByGroup = [
            SubscribeTariffGroup::STANDART => SubscribeTariff::find()->where(['tariff_group_id' => SubscribeTariffGroup::STANDART])->andWhere(['!=', 'id', SubscribeTariff::TARIFF_TRIAL])->select('id')->column(),
            SubscribeTariffGroup::IP_USN_6 => SubscribeTariff::find()->where(['tariff_group_id' => SubscribeTariffGroup::IP_USN_6])->select('id')->column(),
            SubscribeTariffGroup::B2B_PAYMENT => SubscribeTariff::find()->where(['tariff_group_id' => SubscribeTariffGroup::B2B_PAYMENT])->select('id')->column(),
        ];
    }

    /**
     * @param $tariffGroupId
     * @return array|\yii\db\ActiveRecord|null
     * @throws \Exception
     */
    public function getNewCount($tariffGroupId)
    {
        $dateRange = $this->getDateRange();

        $query = ReportState::find()
            ->joinWith('company')
            ->andWhere([Company::tableName() . '.test' => false])
            ->andWhere(['tariff_id' => self::$tariffByGroup[$tariffGroupId]])
            ->andWhere(['between', 'date(from_unixtime(' . ReportState::tableName() . '.pay_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59']);

        return [
            'cnt' => (clone $query)->count(),
            'sum' => (clone $query)->sum('pay_sum'),
        ];
    }

    /**
     * @param $tariffGroupId
     * @return array|\yii\db\ActiveRecord[]
     * @throws \Exception
     */
    public function getRepeatedCount($tariffGroupId)
    {
        $dateRange = $this->getDateRange();

        $newSubscribes = ReportState::find()
            ->select('subscribe_id')
            ->andWhere(['tariff_id' => self::$tariffByGroup[$tariffGroupId]])
            ->andWhere(['between', 'date(from_unixtime(' . ReportState::tableName() . '.pay_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])
            ->asArray()
            ->column();
        $newPayments = Subscribe::find()
            ->select('payment_id')
            ->where(['id' => $newSubscribes])
            ->asArray()
            ->column();
        $repeatedCompanies = [];

        $query2 = $this->getPaymentBaseQuery()
            ->andWhere(['tariff_id' => self::$tariffByGroup[$tariffGroupId]])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Payment::tableName() . '.payment_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])
            ->groupBy('company_id');

        $payedCompaniesArr = $query2->select('company_id')->asArray()->all();

        foreach ($payedCompaniesArr as $data) {
            if ($this->getSubscribeBaseQuery()
                    ->byCompany($data['company_id'])
                    ->andWhere(['tariff_group_id' => $tariffGroupId])
                    ->andWhere(['OR',
                        ['between', 'DATE(FROM_UNIXTIME(' . Subscribe::tableName() . '.expired_at))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'],
                        ['IS', 'expired_at', null],
                    ])
                    ->orderBy(['expired_at' => SORT_DESC])
                    ->count() == 0

                &&

                $this->getSubscribeBaseQuery()
                    ->byCompany($data['company_id'])
                    ->andWhere(['tariff_group_id' => $tariffGroupId])
                    ->andWhere(['OR',
                        ['<', 'DATE(FROM_UNIXTIME(' . Subscribe::tableName() . '.expired_at))', $dateRange['from'] . ' 00:00:00'],
                        ['IS', 'expired_at', null],
                    ])
                    ->orderBy(['expired_at' => SORT_DESC])
                    ->count() > 0

            ) {
                $repeatedCompanies[] = $data['company_id'];
            }
        }

        $sum = 0;
        if ($repeatedCompanies) {
            $sum = $this->getPaymentBaseQuery()
                ->andWhere(['company_id' => $repeatedCompanies])
                ->andWhere(['tariff_id' => self::$tariffByGroup[$tariffGroupId]])
                ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Payment::tableName() . '.payment_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])
                ->andWhere('`service_payment`.`id` NOT IN('.implode(',', $newPayments ?: [0]).')')
                ->sum('sum');
        }

        return [
            'cnt' => count($repeatedCompanies),
            'sum' => $sum
        ];
    }

    /**
     * @param $tariffGroupId
     * @return array|\yii\db\ActiveRecord[]
     * @throws \Exception
     */
    public function getAddInvoiceCount()
    {
        $dateRange = $this->getDateRange();

        return $this->getPaymentBaseQuery()
            ->andWhere(['payment_for' => Payment::FOR_ADD_INVOICE])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Payment::tableName() . '.payment_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59'])
            ->select('COUNT(`service_payment`.`sum`) AS cnt, SUM(`service_payment`.`sum`) AS sum')->asArray()->one();
    }


    /**
     * @param $tariffGroupId
     * @param string $paymentFor
     * @return array
     */
    public function getTotalCount($tariffGroupId, $paymentFor = '')
    {
        $dateRange = $this->getDateRange();

        $query = Payment::find()
            ->joinWith('company', false)
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andWhere(['not', ['service_payment.type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->andWhere(['service_payment.is_confirmed' => 1])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Payment::tableName() . '.payment_date))', $dateRange['from'] . ' 00:00:00', $dateRange['to'] . ' 23:59:59']);

        if ($tariffGroupId) {
            $query->andWhere(['tariff_id' => self::$tariffByGroup[$tariffGroupId]]);
        }

        if ($paymentFor) {
            $query->andWhere(['payment_for' => $paymentFor]);
        }

        return [
            'cnt' => (clone $query)->select('COUNT(`service_payment`.`sum`)')->scalar(),
            'sum' => (clone $query)->select('SUM(`service_payment`.`sum`)')->scalar()
        ];
    }

    private function getSubscribeBaseQuery() {

        return Subscribe::find()
            ->joinWith('company', false)
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andWhere(['and',
                ['!=', 'service_subscribe.tariff_id', SubscribeTariff::TARIFF_TRIAL],
                ['not', ['service_subscribe.tariff_id' => null]],
            ])
            ->andWhere(['service_subscribe.status_id' => [SubscribeStatus::STATUS_PAYED, SubscribeStatus::STATUS_ACTIVATED]]);
    }

    private function getPaymentBaseQuery() {

        return Payment::find()
            ->joinWith('company', false)
            ->andWhere(['!=', 'company.test', Company::TEST_COMPANY])
            ->andWhere(['not', ['service_payment.type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->andWhere(['and',
                ['!=', 'service_payment.tariff_id', SubscribeTariff::TARIFF_TRIAL],
                ['not', ['service_payment.tariff_id' => null]],
            ])
            ->andWhere(['service_payment.is_confirmed' => 1]);
    }

    private function getDateRange()
    {
        $date = $dateTill = new \DateTime();
        $date->setDate($this->year, $this->month, 1);

        return [
            'from' => $date->format('Y-m-d'),
            'to' => $date->format('Y-m-t')
        ];
    }
}