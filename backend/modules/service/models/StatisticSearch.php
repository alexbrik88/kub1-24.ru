<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 7.12.15
 * Time: 11.46
 * Email: t.kanstantsin@gmail.com
 */

namespace backend\modules\service\models;

use common\models\service\Statistic;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * StatisticSearch represents the model behind the search form about `common\models\service\Statistic`.
 */
class StatisticSearch extends Statistic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Statistic::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'date-sort' => SORT_DESC,
                ],
                'attributes' => [
                    'date-sort' => [
                        'asc' => [
                            'year' => SORT_ASC,
                            'month' => SORT_ASC,
                        ],
                        'desc' => [
                            'year' => SORT_DESC,
                            'month' => SORT_DESC,
                        ],
                    ],
                    'invoice_count',
                    'payed_sum',
                    'payed_invoice_count',
                    'payed_sum_by_novice',
                    'payed_invoice_count_by_novice',
                    'not_payed_sum',
                    'not_payed_invoice_count',
                    'tariff_1',
                    'tariff_2',
                    'tariff_3',
                    'payment_type_1',
                    'payment_type_2',
                    'payment_type_3',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}