<?php

namespace backend\modules\service\controllers;

use backend\components\BackendController;
use common\components\date\DateHelper;
use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\cash\CashFactory;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\EmployeeRole;
use common\models\selling\SellingSubscribe;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\PromoCodeGroup;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use frontend\modules\documents\forms\InvoiceFlowForm;
use frontend\rbac\permissions\Service;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Connection;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * SubscribeController implements the CRUD actions for Subscribe model.
 */
class SubscribeController extends BackendController
{

    /**
     * Lists all Subscribe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Subscribe::find()->addSelect('')->joinWith('payment'),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
                'attributes' => [
                    'id',
                    'company_id',
                    'tariff_id',
                    'created_at',
                    'updated_at',
                    'activated_at',
                    'expired_at',
                    'status_id',
                    'payment' => [
                        'asc' => [
                            'service_payment.id' => SORT_ASC,
                        ],
                        'desc' => [
                            'service_payment.id' => SORT_DESC,
                        ],
                    ]
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Subscribe model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Subscribe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subscribe();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Subscribe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Subscribe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @param integer|null $company_id
     */
    public function actionDelete($id, $company_id = null)
    {
        $subscribe = $this->findModel($id);

        if ($subscribe->companyInActive !== null) {
            \Yii::$app->session->setFlash('error', 'Невозможно удалить активную (текущую) подписку компании.');
        } elseif ($subscribe->payment !== null) {
            \Yii::$app->session->setFlash('error', 'Подписка имеет зарегистрированный платёж. Чтобы удлалить подписку, необходимо удалить платёж.');
        } elseif ($subscribe->delete()) {
            \Yii::$app->session->setFlash('success', 'Подписка успешно удалёна.');
        }

        if ($company_id === null) {
            return $this->redirect(['index']);
        } else {
            return $this->redirect(['/company/company/view', 'id' => $company_id,]);
        }
    }

    /**
     * Set subscribe status to `payed`.
     * @param $id
     * @param $company_id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionPay($id, $company_id)
    {
        $subscribe = $this->findModel($id);
        $company = $subscribe->company;
        $hasActualSubscription = $company->hasActualSubscription;
        \Yii::$app->params['paymentUser'] = $company->getEmployeeChief();

        if (\Yii::$app->params['paymentUser'] === null) {
            \Yii::$app->session->setFlash('success', 'Не удалось оплатить подписку, руководитель компании не найден.');

            return $this->redirect(['/company/company/view', 'id' => $company_id,]);
        }

        if (($payment = $subscribe->payment) == null) {
            $price = $subscribe->tariff->price;
            $discount = $subscribe->discount;
            $sum = $price - round($price * $discount / 100);
            $payment = new Payment([
                'price' => (string) $price,
                'discount' => $discount,
                'sum' => (string) $sum,
                'type_id' => $subscribe->payment_type_id,
                'subscribe_id' => $subscribe->id,
                'by_novice' => 0,
            ]);
        }
        $payment->is_confirmed = 1;
        $payment->payment_date = time();

        $subscribe->status_id = SubscribeStatus::STATUS_PAYED;

        $saved = \Yii::$app->db->transaction(function (Connection $db) use ($payment, $subscribe) {

            $issaved = $payment->save();

            if ($issaved) {
                $issaved = ($issaved && $subscribe->save(true, ['status_id', 'reward']));
            }
            if ($issaved && $subscribe->outInvoice !== null) {
                $flowForm = new InvoiceFlowForm([
                    'invoice' => $subscribe->outInvoice,
                ]);
                $flowForm->amount = (string) $subscribe->tariff->price;
                $flowForm->date_pay = (string) date(DateHelper::FORMAT_USER_DATE);
                $flowForm->flowType = (string) CashFactory::$paymentToFlowType[$subscribe->payment_type_id];
                $flowForm->isAccounting = '1';
                $flowForm->createNew = '1';

                $issaved = ($issaved && $flowForm->validate() && $flowForm->save());
            }

            if ($issaved) {
                return true;
            } else {
                $db->transaction->rollBack();

                return false;
            }
        });

        if ($saved) {
            $subscribe->createReportState();
            if (!$hasActualSubscription) {
                $subscribe->activate();
                $company->setActiveSubscribe($subscribe);
            }
            $sellingSubscribe = new SellingSubscribe();
            $sellingSubscribe->company_id = $subscribe->company->id;
            $sellingSubscribe->subscribe_id = $payment->subscribe->id;
            switch ($subscribe->tariff_id) {
                case SubscribeTariff::TARIFF_1:
                    $sellingSubscribe->type = SellingSubscribe::TYPE_TARIFF_1;
                    $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_PAY;
                    break;
                case SubscribeTariff::TARIFF_2:
                    $sellingSubscribe->type = SellingSubscribe::TYPE_TARIFF_2;
                    $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_PAY;
                    break;
                case SubscribeTariff::TARIFF_3:
                    $sellingSubscribe->type = SellingSubscribe::TYPE_TARIFF_3;
                    $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_PAY;
                    break;
                case SubscribeTariff::TARIFF_TRIAL:
                    $sellingSubscribe->type = SellingSubscribe::TYPE_TRIAL;
                    $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_FREE;
                    break;
                case null:
                    if ($subscribe->payment->promoCode) {
                        if ($subscribe->payment->promoCode->code == '77777') {
                            $sellingSubscribe->type = SellingSubscribe::TYPE_PROMO_CODE_77777;
                        } else {
                            if ($subscribe->payment->promoCode->group_id == PromoCodeGroup::GROUP_INDIVIDUAL) {
                                $sellingSubscribe->type = SellingSubscribe::TYPE_PROMO_CODE_INDIVIDUAL;
                            } else {
                                $sellingSubscribe->type = SellingSubscribe::TYPE_PROMO_CODE_GROUPED;
                            }
                        }
                        $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_FREE;
                    }
            }
            $sellingSubscribe->invoice_number = null;
            $sellingSubscribe->creation_date = $subscribe->created_at;
            $sellingSubscribe->activation_date = $subscribe->activated_at;
            $sellingSubscribe->end_date = $subscribe->expired_at;
            $sellingSubscribe->save();
            $user = $subscribe->company->getEmployeeChief();
            if ($user !== null && $user->email) {
                $mail = \Yii::$app->mailer->compose([
                    'html' => 'system/subscribe/confirm-payment/html',
                    'text' => 'system/subscribe/confirm-payment/text',
                ], [
                    'data' => ['sum' => $payment->sum],
                    'subject' => 'Платеж успешно принят',
                ])
                    ->setSubject('Платеж успешно принят')
                    ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                    ->setTo($user->email);

                if (($invoice = $subscribe->outInvoice) && ($invoice->act || $invoice->createAct())) {
                    $act = $invoice->act;
                    $mail->attachContent(Act::getRenderer(null, $act, PdfRenderer::DESTINATION_STRING)->output(false), [
                        'fileName' => $act->pdfFileName,
                        'contentType' => 'application/pdf',
                    ]);
                }

                $mail->send();
            }
            \Yii::$app->session->setFlash('success', 'Подписка успешно оплачена.');
        } else {
            \Yii::$app->session->setFlash('success', 'Не удалось оплатить подписку');
        }

        return $this->redirect(['/company/company/view', 'id' => $company_id,]);
    }

    /**
     * Set subscribe status to `new`
     * @param $id
     * @param $company_id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUndo($id, $company_id)
    {
        $subscribe = $this->findModel($id);

        $subscribe->status_id = SubscribeStatus::STATUS_NEW;
        if ($subscribe->reportState) {
            $subscribe->reportState->delete();
        }
        if ($subscribe->payment !== null && $subscribe->payment->subscribe_id == $id && $subscribe->status_id) {
            $subscribe->payment->is_confirmed = 0;
            $subscribe->payment->payment_date = null;
            $subscribe->payment->save(true, ['is_confirmed', 'payment_date']);
        }

        if ($subscribe->companyInActive !== null && $subscribe->companyInActive->active_subscribe_id == $id) {
            $company = $subscribe->company;
            $company->active_subscribe_id = null;
            $company->save(false, ['active_subscribe_id']);
        }

        if ($subscribe->save(true, ['status_id',])) {
            \Yii::$app->session->setFlash('success', 'Подписка успешно отменена.');
        } else {
            \Yii::$app->session->setFlash('success', 'Не удалось отменить подписку');
        }

        return $this->redirect(['/company/company/view', 'id' => $company_id,]);
    }

    /**
     * Finds the Subscribe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subscribe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subscribe::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
