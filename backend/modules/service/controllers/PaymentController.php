<?php

namespace backend\modules\service\controllers;

use common\components\date\DateHelper;
use common\components\pdf\PdfRenderer;
use common\models\cash\CashFactory;
use common\models\company\CompanyAffiliateLeadRewardsPayment;
use common\models\document\Act;
use common\models\selling\SellingSubscribe;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\PromoCodeGroup;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use backend\components\BackendController;
use backend\modules\service\models\PaymentSearch;
use common\models\service\SubscribeTariff;
use frontend\models\Documents;
use frontend\modules\documents\forms\InvoiceFlowForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Connection;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends BackendController
{

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Payment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $company_id = null)
    {
        $payment = $this->findModel($id);

        if ($payment->deleteCompletely()) {
            \Yii::$app->session->setFlash('success', 'Платеж успешно удален.');
        } else {
            \Yii::$app->session->setFlash('error', 'Не удалось удалить платеж');
        }

        return $this->redirect($company_id ? ['/company/company/view', 'id' => $company_id] : ['index']);
    }

    /**
     * @param $id
     * @param null $company_id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \Throwable
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionPaid($id, $company_id = null)
    {
        $payment = $this->findModel($id);
        if ($payment->paid()) {
            if ($payment->payment_for == Payment::FOR_SUBSCRIBE) {
                CompanyAffiliateLeadRewardsPayment::setRewardsPayment($payment->company->invited_by_company_id,
                    $payment->company->id,
                    $payment->inInvoice->id ?? null,
                    $payment->outInvoice->getTotalAmountWithNds()
                );
                $payment->subscribe->createReportState();
                $userArray = $payment->company->getEmployeeChief(true);
                $emailArray = ArrayHelper::getColumn($userArray, 'email');
                if ($emailArray) {
                    $mail = \Yii::$app->mailer->compose([
                        'html' => 'system/subscribe/confirm-payment/html',
                        'text' => 'system/subscribe/confirm-payment/text',
                    ], [
                        'data' => ['sum' => $payment->sum],
                        'subject' => 'Платеж успешно принят',
                    ])
                        ->setSubject('Платеж успешно принят')
                        ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                        ->setTo($emailArray);

                    if (($invoice = $payment->outInvoice) !== null) {
                        if ($payment->type_id == PaymentType::TYPE_INVOICE) {
                            $content = $this->renderFile('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
                                'model' => $invoice,
                            ]);
                            $name = ($invoice->is_invoice_contract && $invoice->type == Documents::IO_TYPE_OUT) ? 'счет-договора' : 'счета';
                            $fileName = 'Платежное_поручение_для_' . $name . '_№' .
                                mb_ereg_replace("([^\w\s\d\-_])", '', $invoice->fullNumber) . '.txt';
                            $mail->attachContent($content, [
                                'fileName' => $fileName,
                                'contentType' => 'text/plain',
                            ]);
                        }
                        if ($invoice->act || $invoice->createAct()) {
                            $act = $invoice->act;
                            $renderer = Act::getRenderer(null, $act, PdfRenderer::DESTINATION_STRING);
                            $content = $renderer->output(false);
                            $mail->attachContent($content, [
                                'fileName' => $act->pdfFileName,
                                'contentType' => 'application/pdf',
                            ]);
                        }
                    }

                    $mail->send();
                }
            } elseif ($payment->payment_for == Payment::FOR_ADD_INVOICE) {
                $payment->createReportStateForAddInvoice();
            }
            Yii::$app->session->setFlash('success', 'Подписка успешно оплачена.');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось оплатить подписку');
        }

        return $this->redirect(
            Yii::$app->request->referrer ?:
                ($company_id ? ['/company/company/view', 'id' => $company_id] : ['/service/payment/index'])
        );
    }

    /**
     * Set payment not confirm status and deleting subscribes
     * @param integer $id
     * @param integer $company_id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUnpaid($id, $company_id)
    {
        $payment = $this->findModel($id);

        foreach ($payment->subscribes as $subscribe) {
            if ($subscribe->reportState) {
                $subscribe->reportState->delete();
            }
            if ($subscribe->sellingSubscribe) {
                $subscribe->sellingSubscribe->delete();
            }
        }

        if ($payment->unpaid()) {
            \Yii::$app->session->setFlash('success', 'Подписка успешно отменена.');
        } else {
            \Yii::$app->session->setFlash('error', 'Не удалось отменить подписку');
        }

        return $this->redirect(['/company/company/view', 'id' => $company_id,]);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
