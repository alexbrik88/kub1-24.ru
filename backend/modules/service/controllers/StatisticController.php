<?php

namespace backend\modules\service\controllers;

use backend\components\BackendController;
use backend\modules\service\models\StatisticSearch;
use common\models\Company;
use common\models\service\StatisticHelper;
use Yii;

/**
 * StatisticController.
 */
class StatisticController extends BackendController
{

    /**
     * Lists all Statistic models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StatisticSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate()
    {
        if (StatisticHelper::isSubscribeChanged()) {
            StatisticHelper::updateStatistics(StatisticHelper::getUpdatedPeriods());

            \Yii::$app->session->setFlash('success', 'Статистика обновлена.');
        } else {
            \Yii::$app->session->setFlash('info', 'Нет обновлений.');
        }

        return $this->redirect(['index']);
    }

}
