<?php

namespace backend\modules\service\controllers;

use backend\components\BackendController;
use backend\modules\company\forms\PromocodeSendForm;
use backend\modules\service\models\PromoCodeSearch;
use backend\modules\service\models\PromoCodeStatisticSearch;
use common\components\date\DateHelper;
use common\models\Company;
use common\models\service\PromoCodeGroup;
use Yii;
use common\models\service\PromoCode;
use common\models\service\SubscribeTariffGroup;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\employee\EmployeeRole;
use common\models\employee\Employee;

/**
 * PromoCodeController implements the CRUD actions for PromoCode model.
 */
class PromoCodeController extends BackendController
{

    /**
     * Lists all PromoCode models.
     * @return mixed
     */
    public function actionStatistic()
    {
        $searchModel = new PromoCodeStatisticSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('statistic', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Lists all PromoCode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PromoCodeSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single PromoCode model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PromoCode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer|null $company_id
     * @return mixed
     */
    public function actionCreate($company_id = null)
    {
        $model = new PromoCode();
        $model->group_id = $company_id ? PromoCodeGroup::GROUP_INDIVIDUAL : PromoCodeGroup::DEFAULT_GROUP;
        $model->tariff_group_id = SubscribeTariffGroup::STANDART;
        $model->started_at = date('d.m.Y');
        $model->expired_at = date_create('+7 days')->format('d.m.Y');

        if ($company_id) {
            $model->company_id = $company_id;
        }

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PromoCode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PromoCode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer|null $company_id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id, $company_id = null)
    {
        $promoCode = $this->findModel($id);

        if ($promoCode->is_used) {
            \Yii::$app->session->setFlash('error', 'Промокод уже использован и не может быть удалён.');
        } else {
            if ($promoCode->delete()) {
                \Yii::$app->session->setFlash('success', 'Промокод успешно удалён.');
            } else {
                \Yii::$app->session->setFlash('error', 'Ошибка при удалении промокода.');
            }
        }

        if ($company_id === null) {
            return $this->redirect(['index']);
        } else {
            return $this->redirect(['/company/company/view', 'id' => $company_id,]);
        }
    }

    /**
     * @param integer $id
     * @param integer $company_id
     * @return mixed
     */
    public function actionSend($id, $company_id)
    {
        $this->layout = 'empty';

        $promoCode = $this->findModel($id);

        $company = Company::findOne($company_id);

        $sendForm = new PromocodeSendForm([
            'promoCode' => $promoCode,
            'company' => $company,
        ]);

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        $message = null;
        if (Yii::$app->request->isPost) {
            $sendForm->load(Yii::$app->request->post());

            if ($sendForm->validate()) {
                if ($sendForm->send()) {
                    $promoCode->email_send = 1;
                    $promoCode->save(false, ['email_send']);
                    Yii::$app->session->setFlash('success', 'Сообщение отправлено');
                } else {
                    Yii::$app->session->setFlash('error', 'При отправке сообщения произошла ошибка');
                }
            }
        }

        return $this->redirect(['/company/company/view', 'id' => $company_id,]);
    }

    /**
     * Finds the PromoCode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PromoCode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PromoCode::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
