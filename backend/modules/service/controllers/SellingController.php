<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.08.2017
 * Time: 15:40
 */

namespace backend\modules\service\controllers;


use backend\components\BackendController;
use backend\modules\service\models\SellingInvoiceSearch;
use backend\modules\service\models\SellingPaymentSearch;
use backend\modules\service\models\SellingSubscribeSearch;
use backend\modules\service\models\SummaryPaymentSearch;
use common\components\helpers\Month;
use common\components\pdf\PdfRenderer;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\selling\SellingSubscribe;
use common\models\service\Payment;
use common\models\service\PromoCodeGroup;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class SellingController
 * @package backend\modules\service\controllers
 */
class SellingController extends BackendController
{
    /**
     * @return string
     */
    public function actionSubscribe()
    {
        $searchModel = new SellingSubscribeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('subscribe', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @return string
     */
    public function actionInvoice()
    {
        $searchModel = new SellingInvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('invoice', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @return string
     */
    public function actionPayment()
    {
        $searchModel = new SellingPaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('payment', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionPaidInvoice($id)
    {
        $model = $this->findInvoiceModel($id);

        $payment = $model->payment;

        if (($paymentDate = ArrayHelper::getValue(Yii::$app->request->post('Payment'), 'payment_date')) !== null) {
            $date = date_create_from_format('d.m.Y|', $paymentDate);
            $paymentDate = $date ? $date->getTimestamp() : null;
        }

        if ($model->is_subscribe_invoice) {
            if ($payment->paid($paymentDate)) {
                $user = $payment->company->getEmployeeChief();
                if ($payment->payment_for == Payment::FOR_SUBSCRIBE) {
                    $payment->subscribe->createReportState();
                } elseif ($payment->payment_for == Payment::FOR_ADD_INVOICE) {
                    $payment->createReportStateForAddInvoice();
                }
                if ($user !== null && $user->email) {
                    $mail = \Yii::$app->mailer->compose([
                        'html' => 'system/subscribe/confirm-payment/html',
                        'text' => 'system/subscribe/confirm-payment/text',
                    ], [
                        'data' => ['sum' => $payment->sum],
                        'subject' => 'Платеж успешно принят',
                    ])
                        ->setSubject('Платеж успешно принят')
                        ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                        ->setTo($user->email);

                    if (($invoice = $payment->outInvoice) && ($invoice->act || $invoice->createAct())) {
                        $act = $invoice->act;
                        $mail->attachContent(Act::getRenderer(null, $act, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => $act->pdfFileName,
                            'contentType' => 'application/pdf',
                        ]);
                    }

                    $mail->send();
                }
                \Yii::$app->session->setFlash('success', 'Счет успешно оплачен.');
            } else {
                \Yii::$app->session->setFlash('error', 'Не удалось оплатить счет.');
            }
        }
        $filter = isset(Yii::$app->request->get(1)['SellingInvoiceSearch']) ? Yii::$app->request->get(1)['SellingInvoiceSearch'] : null;

        return $this->redirect(['invoice', 'SellingInvoiceSearch' => $filter]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionUndo($id)
    {
        $model = $this->findInvoiceModel($id);
        $payment = $model->payment;

        if ($payment->subscribe) {
            if ($payment->subscribe->reportState) {
                $payment->subscribe->reportState->delete();
            }
            if ($payment->subscribe->sellingSubscribe) {
                $payment->subscribe->sellingSubscribe->delete();
            }
        }
        if ($payment->unpaid()) {
            \Yii::$app->session->setFlash('success', 'Счет успешно отменен.');
        } else {
            \Yii::$app->session->setFlash('error', 'Не удалось отменить счет.');
        }
        $filter = isset(Yii::$app->request->get(1)['SellingInvoiceSearch']) ? Yii::$app->request->get(1)['SellingInvoiceSearch'] : null;

        return $this->redirect(['invoice', 'SellingInvoiceSearch' => $filter]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdatePaymentDate($id)
    {
        $model = $this->findInvoiceModel($id);

        $payment = $model->subscribe->payment;
        $paymentDate = isset(Yii::$app->request->post('Payment')['payment_date']) ?
            strtotime(Yii::$app->request->post('Payment')['payment_date']) :
            null;

        if ($paymentDate) {
            $payment->payment_date = $paymentDate;
            $payment->save(true, ['payment_date']);
        }

        return $this->redirect(['invoice']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDeleteInvoice($id)
    {
        $model = $this->findInvoiceModel($id);

        if ($model->is_subscribe_invoice && $model->payment) {
            $payment = $model->payment;
            if ($payment->subscribe) {
                if ($payment->subscribe->reportState) {
                    $payment->subscribe->reportState->delete();
                }
                if ($payment->subscribe->sellingSubscribe) {
                    $payment->subscribe->sellingSubscribe->delete();
                }
            }
            if ($payment->unpaid() && $payment->delete()) {
                \Yii::$app->session->setFlash('success', 'Счет успешно удален.');
            } else {
                \Yii::$app->session->setFlash('error', 'Не удалось удалить счет');
            }
        }

        return $this->redirect(['invoice']);
    }

    /**
     * @param $id
     * @return Invoice
     * @throws NotFoundHttpException
     */
    protected function findInvoiceModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @return string
     */
    public function actionSummary($year = null)
    {
        if (!$year)
            $year = date('Y');

        $searchModel = new SummaryPaymentSearch();
        $searchModel->init();

        $data = [];
        for ($month = 1; $month <= 12; $month++) {
            $searchModel->year = $year;
            $searchModel->month = $month;
            $data[$month] = [
                'month' => Month::$monthShort[$month] . ' ' . $year,
                'kub_standart' => $searchModel->getTotalCount(SubscribeTariffGroup::STANDART),
                'kub_standart_new' => $searchModel->getNewCount(SubscribeTariffGroup::STANDART),
                'kub_standart_repeated' => $searchModel->getRepeatedCount(SubscribeTariffGroup::STANDART),
                'kub_add_invoice' => $searchModel->getTotalCount(null, 'add_invoice'),
                'kub_ip_usn' => $searchModel->getTotalCount(SubscribeTariffGroup::IP_USN_6),
                'kub_ip_usn_new' => $searchModel->getNewCount(SubscribeTariffGroup::IP_USN_6),
                'kub_b2b' => $searchModel->getTotalCount(SubscribeTariffGroup::B2B_PAYMENT),
            ];
        }

        // var_dump($data);die;

        return $this->render('summary', [
            'summaryData' => $data,
        ]);
    }
}