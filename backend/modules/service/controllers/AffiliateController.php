<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.07.2017
 * Time: 8:38
 */

namespace backend\modules\service\controllers;


use backend\components\BackendController;
use backend\modules\service\models\AffiliateSearch;
use common\models\service\RewardRequest;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class AffiliateController
 * @package backend\modules\service\controllers
 */
class AffiliateController extends BackendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AffiliateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionPay($id)
    {
        $model = $this->findModel($id);
        $model->status = RewardRequest::STATUS_PAYED;
        if ($model->save()) {
            $model->company->active_reward_requests -= 1;
            $model->company->save(true, ['active_reward_requests']);
            $model->sendMessage();
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUndo($id)
    {
        $model = $this->findModel($id);
        $model->status = RewardRequest::STATUS_REJECT;
        if ($model->save()) {
            $company = $model->company;
            $company->affiliate_sum += $model->sum;
            if ($model->type == RewardRequest::RS_TYPE) {
                $company->payed_reward_for_rs -= $model->sum;
            } elseif ($model->type == RewardRequest::INDIVIDUAL_ACCOUNT_TYPE) {
                $company->payed_reward_for_card -= $model->sum;
            }
            $company->active_reward_requests -= 1;
            $company->save(true, ['affiliate_sum', 'payed_reward_for_rs', 'payed_reward_for_card', 'active_reward_requests']);

        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RewardRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RewardRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Компания системы не установлена.');
        }
    }
}