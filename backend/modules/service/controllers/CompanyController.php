<?php

namespace backend\modules\service\controllers;

use backend\components\BackendController;
use common\models\Company;
use common\models\CompanyHelper;
use common\models\service\Payment;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * CompanyController implements the CRUD actions for Payment model.
 */
class CompanyController extends BackendController
{

    public $defaultAction = 'view';

    /**
     * Displays a single Payment model.
     * @return mixed
     */
    public function actionView()
    {
        return $this->render('view', [
            'model' => $this->findModel(\Yii::$app->params['service']['company_id']),
        ]);
    }

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate()
    {
        /** @var Company $model */
        $model = $this->findModel(\Yii::$app->params['service']['company_id']);
        $model->setScenario(Company::SCENARIO_USER_UPDATE);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if (CompanyHelper::load($model) && CompanyHelper::validate($model) && CompanyHelper::save($model)) {
            return $this->redirect(['view']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Компания системы не установлена.');
        }
    }
}
