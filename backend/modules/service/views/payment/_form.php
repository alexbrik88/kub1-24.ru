<?php

use common\components\date\DateHelper;
use common\models\service\PaymentType;
use common\models\service\PromoCode;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\service\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'payment_date')->textInput([
        'class' => 'form-control datetime-picker',
        'data-date-viewmode' => 'years',
        'value' => DateHelper::format($model->payment_date, DateHelper::FORMAT_USER_DATETIME, DateHelper::FORMAT_DATETIME),
    ]); ?>

    <?= $form->field($model, 'invoice_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(PaymentType::find()->all(), 'id', 'name'), [
        'prompt' => '',
    ]); ?>

    <?= $form->field($model, 'promo_code_id')->dropDownList(ArrayHelper::map(PromoCode::find()->isUsed(false)->all(), 'id', 'code'), [
        'prompt' => '',
    ]); ?>

    <?= $form->field($model, 'is_confirmed')->checkbox(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
