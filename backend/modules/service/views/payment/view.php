<?php

use common\models\service\Payment;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\service\Payment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Платежи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$subscribes = [];
foreach ($model->subscribes as $subscribe) {
    $subscribes[] = Html::a($subscribe->id, ['subscribe/view', 'id' => $subscribe->id,]);
}
?>
<div class="payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот платёж?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'datetime',
            ],
            [
                'attribute' => 'payment_date',
                'format' => 'datetime',
            ],
            'invoice_number',
            [
                'attribute' => 'subscribes',
                'format' => 'raw',
                'value' => implode(', ', $subscribes),
            ],
            'sum',
            'type_id',
            [
                'attribute' => 'promo_code_id',
                'format' => 'raw',
                'value' => $model->promoCode ? Html::a($model->promoCode->code, ['promo-code/view', 'id' => $model->promo_code_id,]) : null,
            ],
            [
                'attribute' => 'is_confirmed',
                'format' => 'boolean',
                'visible' => $model->type_id == \common\models\service\PaymentType::TYPE_ONLINE,
            ],
        ],
    ]) ?>

</div>
