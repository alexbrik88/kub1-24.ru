<?php

use common\components\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\grid\DropDownSearchDataColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\modules\service\models\PaymentSearch */

$this->title = 'Платежи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

    <div class="portlet box">
        <div class="btn-group pull-right title-buttons">
            <?= Html::a('<i class="fa fa-plus"></i> Добавить', Url::to(['create']), ['class' => 'btn yellow']); ?>
        </div>
        <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="portlet box darkblue blk_wth_srch">
        <div class="portlet-title row-fluid">
            <div class="caption list_recip col-sm-3">
                <?= $this->title; ?>
            </div>
            <div class="tools tools_button col-sm-9">
                <div class="form-body row">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => ['index'],
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="col-xs-9">
                        <div style="max-width: 400px; margin: 0 0 0 auto;">
                            <?= $form->field($searchModel, 'search')->textInput([
                                'placeholder' => 'Поиск по плательщику (ID, ИНН, название, Email)',
                            ]); ?>
                        </div>
                    </div>
                    <div class="col-xs-3 text-right">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn default btn_marg_down green-haze',
                            'style' => 'margin-right: 5px;',
                        ]) ?>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div class="dataTables_wrapper dataTables_extended_wrapper">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,

                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable documents_table overfl_text_hid',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",

                        'columns' => [
                            [
                                'attribute' => 'id',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                ],
                            ],
                            [
                                'label' => 'Компания',
                                'format' => 'raw',
                                'value' => function (\common\models\service\Payment $model) {
                                    return Html::a($model->company->getShortName(), Url::to(['/company/company/view', 'id' =>  $model->company_id]));
                                }
                            ],
                            [
                                'label' => 'Компания',
                                'format' => 'raw',
                                'value' => function (\common\models\service\Payment $model) {
                                    $content = '';
                                    $companyArray = $model->orderCompanies ? : [$model->company];
                                    foreach ($companyArray as $company) {
                                        $content .= Html::tag('div', Html::a($company->getShortName(), [
                                            '/company/company/view',
                                            'id' =>  $company->id,
                                        ]), ['class' => 'text-overflow-ellipsis']);
                                    }
                                    return $content;
                                }
                            ],
                            [
                                'attribute' => 'created_at',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                ],
                                'value' => function ($model) {
                                    return date('d.m.Y H:i', $model->created_at);
                                }
                            ],
                            [
                                'attribute' => 'payment_date',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                ],
                                'value' => function ($model) {
                                    return $model->payment_date ? date('d.m.Y H:i', $model->payment_date) : 'Не оплачен';
                                }
                            ],
                            [
                                'attribute' => 'invoice_number',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                ],
                            ],
                            [
                                'attribute' => 'subscribe_id',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (\common\models\service\Payment $model) {
                                    $content = '';
                                    foreach ($model->subscribes as $subscribe) {
                                        $content .= Html::tag('div', Html::a($subscribe->id, ['subscribe/view', 'id' => $subscribe->id,]));
                                    }
                                    return $content;
                                },
                            ],
                            [
                                'attribute' => 'sum',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                ],
                            ],
                            [
                                'attribute' => 'type_id',
                                'label' => 'Тип платежа',
                                'class' => DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'class' => 'dropdown-filter',
                                ],
                                'filter' => $searchModel->getPaymentTypeFilter(),
                                'value' => 'type.name',
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view} {update} {delete} {paid}',
                                'buttons' => [
                                    'view',
                                    'update',
                                    'delete' => function ($url, $model, $key) {
                                        $title = Yii::t('yii', ucfirst('delete'));
                                        $options = [
                                            'title' => $title,
                                            'aria-label' => $title,
                                            'data-pjax' => '0',
                                            'data-confirm' => "Вы уверенны, что хотите удалить этот платеж?\nБудут так же удалены все созданные подписки и счета.",
                                            'data-method' => 'post',
                                        ];
                                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-trash"]);
                                        return Html::a($icon, $url, $options);
                                    },
                                    'paid' => function ($url, $model, $key) {
                                        if (!$model->is_confirmed) {
                                            return Html::a('Оплачен', $url, [
                                                'class' => 'btn btn-primary btn-success btn_in_table',
                                                'title' => 'Оплачен',
                                                'aria-label' => 'Оплачен',
                                                'data-confirm' => 'Вы подтверждаете оплату этого счета?',
                                                'data-method' => 'post',
                                                'data-pjax' => '0',
                                                'style' => 'margin-top: 5px;',
                                            ]);
                                        }
                                    },
                                ],
                            ],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>

</div>
