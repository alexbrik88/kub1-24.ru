<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\service\Payment */

$this->title = 'Создать платёж';
$this->params['breadcrumbs'][] = ['label' => 'Платежи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
