<?php

use common\models\service;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel \backend\modules\service\models\StatisticSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статистика по счетам';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 class="page-title"><?= Html::encode($this->title) ?></h1>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            <?= $this->title; ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <?= Html::a('обновить статистику', ['update'], [
            'class' => 'btn btn-info',
            'data' => [
                'confirm' => "Обновить статистику?\nСтатистика автоматически обновляется раз в сутки.",
                'method' => 'post',
            ]
        ]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,

            'options' => [
                'class' => 'overflow-x',
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],

            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],
            'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",

            'columns' => [
                [
                    'attribute' => 'date-sort',
                    'label' => 'Год.месяц',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'value' => function (\common\models\service\Statistic $model) {
                        return $model->year . '.' . $model->month;
                    },
                ],
                [
                    'attribute' => 'payed_sum',
                    'label' => 'Оплачено на сумму, руб.',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'payed_invoice_count',
                    'label' => 'Оплачено счетов, шт.',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'payed_sum_by_novice',
                    'label' => 'Оплачено новыми, руб.',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'payed_invoice_count_by_novice',
                    'label' => 'Оплачено новыми, шт.',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'tariff_1',
                    'label' => 'Тариф ' . (service\SubscribeTariff::findOne(service\SubscribeTariff::TARIFF_1)->getTariffName()),
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'tariff_2',
                    'label' => 'Тариф ' . (service\SubscribeTariff::findOne(service\SubscribeTariff::TARIFF_2)->getTariffName()),
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'tariff_3',
                    'label' => 'Тариф ' . (service\SubscribeTariff::findOne(service\SubscribeTariff::TARIFF_3)->getTariffName()),
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'payment_type_1',
                    'label' => 'Оплата через Robokassa',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'payment_type_2',
                    'label' => 'Оплата через Сбербанк',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'payment_type_3',
                    'label' => 'Оплата через счёт',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'not_payed_sum',
                    'label' => 'Не оплачено на сумму, руб.',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'not_payed_invoice_count',
                    'label' => 'Не оплачено счетов, шт.',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'invoice_count',
                    'label' => 'Выставлено счетов, шт.',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
