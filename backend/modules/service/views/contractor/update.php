<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/19/15
 * Time: 3:17 PM
 * Email: t.kanstantsin@gmail.com
 */

/* @var $this yii\web\View */
/* @var $model \common\models\Contractor */

$this->title = 'Обновить контрагента: ' . ' ' . $model->name;
?>

<?= $this->render('@frontend/views/contractor/form/_form', [
    'model' => $model,
    'cancelUrl' => \yii\helpers\Url::to(['view']),
    'face_type_opt' => \common\models\Contractor::TYPE_LEGAL_PERSON,
]) ?>
