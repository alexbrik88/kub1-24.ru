<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/19/15
 * Time: 3:13 PM
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\Contractor;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->context->layoutWrapperCssClass = 'customer';
?>

<div class="row">
    <div class="col-md-6">
        <div class="portlet customer-info">
            <div class="portlet-title">
                <div class="caption">
                    <?= Html::encode($this->title); ?>
                </div>

                <div class="actions">
                    <a class="btn darkblue btn-sm info-button"
                       data-toggle="modal" href="#basic">
                        <i class="icon-info" style="color:white"></i>
                    </a>

                    <a href="<?= Url::to(['contractor/update', 'type' => $model->type, 'id' => $model->id]) ?>"
                       class="darkblue btn-sm btn-link" title="Редактировать">
                        <i class="icon-pencil"></i>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <table id="datatable_ajax" class="table">
                    <tr>
                        <td>
                            <span class="customer-characteristic">Тип:</span>
                            <span><?= ($model->type == Contractor::TYPE_CUSTOMER) ? 'покупатель' : 'поставщик'; ?></span>
                        </td>
                        <td>
                            <?php $name = $model->getRealContactName(); ?>
                            <?php if (!empty($name)): ?>
                                <span
                                    class="customer-characteristic">Контакт:</span>
                                <span><?= $name; ?></span>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="customer-characteristic">Налогообложение:</span>
                            <span><?php
                                if ($model->taxation_system == Contractor::WITH_NDS) {
                                    echo 'с ндс';
                                } elseif ($model->taxation_system == Contractor::WITHOUT_NDS) {
                                    echo 'без ндс';
                                } else {
                                    echo Contractor::NOT_SET;
                                }
                                ?>
                            </span>
                        </td>
                        <td>
                            <?php $phone = $model->getRealContactPhone(); ?>
                            <?php if (!empty($phone)): ?>
                                <span
                                    class="customer-characteristic">Телефон:</span>
                                <span><?= $phone; ?></span>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="customer-characteristic">Статус:</span>
                            <span><?= ($model->status == Contractor::ACTIVE) ? 'активен' : 'не активен'; ?></span>
                        </td>
                        <td>
                            <?php $email = $model->getRealContactEmail(); ?>
                            <?php if (!empty($email)): ?>
                                <span
                                    class="customer-characteristic">E-mail:</span>

                                <a href="mailto:ivan.rus@yahoo.com"><?= $email; ?></a>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="basic" tabindex="-1" role="basic"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="uneditable created-by" style="text-align: center">
                    создал
                    <span><?= date(DateHelper::FORMAT_USER_DATE, $model->created_at); ?></span>
                    <span><?= $model->employee ? $model->employee->fio : ''; ?></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">
                    OK
                </button>
            </div>
        </div>
    </div>
</div>