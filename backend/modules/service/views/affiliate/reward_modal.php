<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.07.2017
 * Time: 10:59
 */

use yii\bootstrap\Modal;
use common\models\service\RewardRequest;
use common\models\Company;
use backend\modules\service\models\RewardRequestSearch;
use common\components\grid\GridView;
use common\components\date\DateHelper;
use common\components\helpers\Html;
use yii\helpers\Url;

/* @var $company Company */

$rewardSearchModel = new RewardRequestSearch();
$rewardDataProvider = $rewardSearchModel->search($company->id, []);

Modal::begin([
    'id' => 'modal-reward-' . $company->id,
    'header' => '<h1>Заявка на вознаграждение</h1>',
    'toggleButton' => false,
]); ?>

<?= GridView::widget([
    'dataProvider' => $rewardDataProvider,
    'filterModel' => $rewardSearchModel,
    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
    'options' => [
        'class' => 'overflow-x',
    ],
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'columns' => [
        [
            'attribute' => 'created_at',
            'label' => 'дата',
            'format' => 'raw',
            'value' => function (RewardRequest $model) {
                return date(DateHelper::FORMAT_USER_DATE, $model->created_at);
            },
        ],
        [
            'attribute' => 'sum',
            'label' => 'сумма, руб.',
            'format' => 'raw',
            'value' => function (RewardRequest $model) {
                return $model->sum;
            },
        ],
        [
            'attribute' => 'type',
            'label' => 'тип',
            'format' => 'raw',
            'value' => function (RewardRequest $model) {
                return $model->type == RewardRequest::RS_TYPE ? 'на р/с' : 'на карту';
            },
        ],
        [
            'label' => '',
            'contentOptions' => [
                'class' => 'reward-actions-column',
            ],
            'format' => 'raw',
            'value' => function (RewardRequest $model) {
                if ($model->status == RewardRequest::STATUS_PAYED) {
                    return '<span class="glyphicon glyphicon-ok-circle" title="Оплачено"></span>';
                } elseif ($model->status == RewardRequest::STATUS_REJECT) {
                    return '<span class="glyphicon glyphicon-ban-circle" title="Отменено"></span>';
                } else {
                    return Html::a('<span class="glyphicon glyphicon-ok-circle" title="Оплатить"></span>',
                        Url::to(['pay', 'id' => $model->id])) .
                    Html::a('<span class="glyphicon glyphicon-ban-circle" title="Отменить"></span>',
                        Url::to(['undo', 'id' => $model->id]), [
                            'style' => 'margin-left: 5px;',
                        ]);
                }
            },
        ],
    ],
]); ?>


<?php Modal::end(); ?>
