<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.07.2017
 * Time: 8:47
 */

use backend\modules\service\models\AffiliateSearch;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Html;
use common\components\grid\GridView;
use common\models\Company;
use yii\helpers\Url;
use common\components\date\DateHelper;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\components\grid\DropDownSearchDataColumn;

/* @var $this yii\web\View
 * @var $searchModel AffiliateSearch
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Вознаграждение';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>
<div class="payment-index">

    <div class="portlet box">
        <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
        </div>
        <div class="portlet-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                'options' => [
                    'class' => 'overflow-x',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'ID',
                        'format' => 'raw',
                        'headerOptions' => [
                            'rowspan' => 2,
                            'colspan' => 1,
                        ],
                    ],
                    [
                        'attribute' => 'name_short',
                        'class' => DropDownSearchDataColumn::className(),
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'rowspan' => 2,
                            'colspan' => 1,
                        ],
                        'filter' => $searchModel->getCompanyFilter(),
                        'value' => function (Company $model) {
                            return Html::a($model->getShortName(), Url::to(['/company/company/view', 'id' => $model->id]));
                        },
                    ],
                    [
                        'attribute' => 'affiliate_link_created_at',
                        'label' => 'Дата активации',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'rowspan' => 2,
                            'colspan' => 1,
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            return date(DateHelper::FORMAT_USER_DATE, $model->affiliate_link_created_at);
                        },
                    ],
                    [
                        'attribute' => 'invited_companies_count',
                        'label' => 'Привлечено компаний',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'rowspan' => 2,
                            'colspan' => 1,
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            $companies = null;
                            foreach ($model->invitedCompanies as $invitedCompany) {
                                $companies .= Html::a($invitedCompany->getShortName(), Url::to(['/company/company/view', 'id' => $invitedCompany->id]));
                                $companies .= '<br>';
                            }

                            $result = Html::beginTag('span', [
                                'id' => 'invited-companies-' . $model->id,
                                'class' => $model->invitedCompanies ? 'tooltip2' : null,
                                'data-tooltip-content' => '#tooltip-invited-companies-' . $model->id,
                            ]);
                            $result .= $model->getInvitedCompanies()->count();
                            $result .= Html::endTag('span');
                            $result .= Html::beginTag('span', [
                                'class' => 'tooltip-template',
                                'style' => 'display:none;',
                            ]);
                            $result .= Html::beginTag('span', [
                                'id' => 'tooltip-invited-companies-' . $model->id,
                                'style' => 'display: inline-block; text-align: center;',
                            ]);
                            $result .= $companies;
                            $result .= Html::endTag('span');
                            $result .= Html::endTag('span');

                            return $result;
                        },
                    ],
                    [
                        'attribute' => 'total_affiliate_sum',
                        'label' => 'Заработано всего, руб.',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'rowspan' => 2,
                            'colspan' => 1,
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            return $model->total_affiliate_sum;
                        },
                    ],
                    [
                        'label' => 'Оплата',
                        'headerOptions' => [
                            'type' => 'header',
                            'class' => 'text-center',
                            'rowspan' => 1,
                            'colspan' => 3,
                            'style' => 'border-bottom: 1px solid #ddd;',
                        ],
                    ],
                    [
                        'attribute' => 'payed_reward_for_kub',
                        'label' => 'За КУБ',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'rowspan' => 1,
                            'colspan' => 1,
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            return $model->payed_reward_for_kub;
                        },
                    ],
                    [
                        'attribute' => 'payed_reward_for_rs',
                        'label' => 'На р/с',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'rowspan' => 1,
                            'colspan' => 1,
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            return $model->payed_reward_for_rs;
                        },
                    ],
                    [
                        'attribute' => 'payed_reward_for_card',
                        'label' => 'На карту',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'rowspan' => 1,
                            'colspan' => 1,
                            'style' => 'border-right: 1px solid #ddd;',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            return $model->payed_reward_for_card;
                        },
                    ],
                    [
                        'attribute' => 'affiliate_sum',
                        'label' => 'Остаток, руб.',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'rowspan' => 2,
                            'colspan' => 1,
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            return $model->affiliate_sum;
                        },
                    ],
                    [
                        'attribute' => 'active_reward_requests',
                        'label' => 'Заявка на вывод',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'rowspan' => 2,
                            'colspan' => 1,
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            return Html::a($model->active_reward_requests . '/' . $model->getRewardRequests()->count(), null, [
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-reward-' . $model->id,
                            ]) . $this->render('reward_modal', [
                                'company' => $model,
                            ]);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>


