<?php

use common\models\service;
use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\service\PromoCode;

/* @var $this yii\web\View */
/* @var $model service\PromoCode */
/* @var $form yii\widgets\ActiveForm */

$groups = service\SubscribeTariffGroup::find()->select('name')->andWhere(['is_active' => true])->indexBy('id')->column();
$groupsOptions = [];
foreach ($groups as $key => $value) {
    $groupsOptions[$key] = [
        'data-limit-text' => service\SubscribeTariffGroup::$limitDescription[$key] ?? 'Нет лимита',
    ];
}
?>

<div class="promo-code-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'tariff_group_id')->dropDownList($groups, ['options' => $groupsOptions]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'tariff_limit')->textInput(); ?>
        </div>
        <div class="col-md-6">
            <div class="form-group field-promocode-tariff_limit">
                <label class="control-label">&nbsp;</label>
                <div class="tariff_limit_description" style="padding: 7px 0;">
                    <?= service\SubscribeTariffGroup::$limitDescription[$model->tariff_group_id] ?? 'Нет лимита'; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'type')->dropDownList(PromoCode::$typeArray); ?>
        </div>
        <div class="cabinets-count-field col-md-3<?= $model->type == PromoCode::TYPE_CABINET ? null : ' hidden'; ?>">
            <?= $form->field($model, 'cabinets_count')->textInput(); ?>
        </div>
        <div class="out-invoice-count-field col-md-3<?= $model->type == PromoCode::TYPE_OUT_INVOICE ? null : ' hidden'; ?>">
            <?= $form->field($model, 'out_invoice_count')->textInput(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'duration_month')->textInput(); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'duration_day')->textInput(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'days_from_registration')->textInput(); ?>
        </div>
    </div>

    <?= $form->field($model, 'code')->textInput([
        'maxlength' => true,
    ]); ?>

    <?= $form->field($model, 'name')->textInput([
        'maxlength' => true,
    ]); ?>

    <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(service\PromoCodeGroup::find()->all(), 'id', 'name')); ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'started_at')->textInput([
                'class' => 'form-control date-picker',
                'data-date-viewmode' => 'years',
                'value' => DateHelper::format($model->started_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'expired_at')->textInput([
                'class' => 'form-control date-picker',
                'data-date-viewmode' => 'years',
                'value' => DateHelper::format($model->expired_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
        </div>
    </div>

    <br/>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJs('
    var $cabinetsCountField = $(".cabinets-count-field");
    var $outInvoiceCountField = $(".out-invoice-count-field");

    $("#promocode-type").change(function () {
        if ($(this).val() == 1) {
            $cabinetsCountField.addClass("hidden");
            $outInvoiceCountField.addClass("hidden");
        } else if ($(this).val() == 2) {
            $cabinetsCountField.removeClass("hidden");
            $outInvoiceCountField.addClass("hidden");
        } else {
            $outInvoiceCountField.removeClass("hidden");
            $cabinetsCountField.addClass("hidden");
        }
    });

    $(document).on("change", "#promocode-tariff_group_id", function () {
        console.log($(":selected", this).data("limit-text"));
        $(".tariff_limit_description", this.form).text($(":selected", this).data("limit-text"))
    })
'); ?>
