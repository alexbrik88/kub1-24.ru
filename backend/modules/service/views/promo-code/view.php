<?php

use common\components\date\DateHelper;
use common\models\service\PromoCode;
use common\models\service\SubscribeTariffGroup;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\service\PromoCode */

$this->title = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Промо коды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-code-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'tariff_group_id',
                'value' => $model->tariffGroup->name,
            ],
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => PromoCode::$typeArray[$model->type],
            ],
            [
                'attribute' => 'cabinets_count',
                'format' => 'raw',
                'value' => $model->cabinets_count,
                'visible' => $model->type == PromoCode::TYPE_CABINET,
            ],
            [
                'attribute' => 'out_invoice_count',
                'format' => 'raw',
                'value' => $model->out_invoice_count,
                'visible' => $model->type == PromoCode::TYPE_OUT_INVOICE,
            ],
            [
                'label' => 'Длительность',
                'value' => \common\models\service\SubscribeHelper::getReadableDuration($model),
            ],
            [
                'attribute' => 'tariff_limit',
                'label' => sprintf('Лимит (%s)', SubscribeTariffGroup::$limitDescription[$model->tariff_group_id] ?? '---'),
            ],
            'code',
            'name',
            'is_used:boolean',
            'created_at:date',
            'activated_at:date',
            [
                'attribute' => 'started_at',
                'format' => 'raw',
                'value' => DateHelper::format($model->started_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ],
            [
                'attribute' => 'expired_at',
                'format' => 'raw',
                'value' => DateHelper::format($model->expired_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ],
        ],
    ]) ?>

</div>
