<?php

use common\models\service\SubscribeTariffGroup;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\modules\service\models\PromoCodeSearch */

$this->title = 'Промо коды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-code-index">

    <div class="portlet box">
        <div class="btn-group pull-right title-buttons">
            <?= Html::a('<i class="fa fa-plus"></i> Добавить', Url::to(['create']), ['class' => 'btn yellow']); ?>
        </div>
        <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="portlet box darkblue blk_wth_srch">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'action' => ['index'],
            'method' => 'GET',
            'fieldConfig' => [
                'template' => "{input}\n{error}",
                'options' => [
                    'class' => '',
                ],
            ],
        ]); ?>
        <div class="search-form-default">
            <div class="col-md-8 col-sm-8 pull-right serveces-search m-t-0">
                <div class="input-group">
                    <div class="input-cont">
                        <?= $form->field($searchModel, 'code')->textInput([
                            'placeholder' => 'Поиск по промокоду',
                        ]); ?>
                    </div>
                                <span class="input-group-btn m-t-sm">
                                    <?= Html::submitButton('ПРИМЕНИТЬ', [
                                        'class' => 'btn green-haze',
                                    ]) ?>
                                </span>
                </div>
            </div>
        </div>
        <?php $form->end(); ?>
        <div class="portlet-title">
            <div class="caption list_recip col-md-3 col-sm-3">
                <?= $this->title; ?>
            </div>
        </div>
        <div class="portlet-body">
            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                'options' => [
                    'style' => 'overflow-x: auto;',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],

                'columns' => [
                    'created_at:date',
                    'code',
                    [
                        'attribute' => 'tariff_group_id',
                        'filter' => ['' => 'Все'] + SubscribeTariffGroup::find()->select('name')->indexBy('id')->column(),
                        'value' => 'tariffGroup.name',
                    ],
                    [
                        'attribute' => 'group_id',
                        'filter' => FilterHelper::getFilterArray(\common\models\service\PromoCodeGroup::find()->all(), 'id', 'name', true, false),
                        'value' => 'group.name',
                    ],
                    [
                        'attribute' => 'for_company_experience_id',
                        'filter' => FilterHelper::getFilterArray(\common\models\company\Experience::find()->all(), 'id', 'name', true, false),
                        'value' => 'forCompanyExperience.name',
                    ],
                    [
                        'attribute' => 'company_id',
                        'format' => 'raw',
                        'value' => function (\common\models\service\PromoCode $promoCode) {
                            return $promoCode->company_id !== null
                                ? Html::a($promoCode->company->getTitle(true), ['/company/company/view', 'id' => $promoCode->company_id,])
                                : '';
                        },
                    ],
                    'name',

                    'started_at:date',
                    'expired_at:date',

                    [
                        'label' => 'Длительность',
                        'value' => function (\common\models\service\PromoCode $promoCode) {
                            return \common\models\service\SubscribeHelper::getReadableDuration($promoCode);
                        },
                    ],
                    'activated_at:date',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>

</div>
