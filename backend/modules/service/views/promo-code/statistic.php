<?php

use common\components\grid\GridView;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\modules\service\models\PromoCodeStatisticSearch */

$this->title = 'Статистика по промокодам';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-code-index">

    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'layout' => "{items}\n{pager}",
        'options' => [
            'style' => 'overflow-x: auto;',
        ],
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover dataTable accounts-list',
        ],
        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'pager' => [
            'options' => [
                'class' => 'pagination pull-right',
            ],
        ],

        'columns' => [
            'name',
            [
                'attribute' => 'group_id',
                'filter' => FilterHelper::getFilterArray(\common\models\service\PromoCodeGroup::find()->all(), 'id', 'name', true, false),
                'value' => 'group.name',
            ],
            [
                'attribute' => 'for_company_experience_id',
                'label' => 'Для кого',
                'filter' => FilterHelper::getFilterArray(\common\models\company\Experience::find()->all(), 'id', 'name', true, false),
                'value' => function (\common\models\service\PromoCode $model) {
                    return $model->forCompanyExperience
                        ? $model->forCompanyExperience->name
                        : '---';
                },
            ],
            [
                'label' => 'Бонус',
                'value' => function (\common\models\service\PromoCode $model) {
                    return \common\models\service\SubscribeHelper::getReadableDuration($model);
                },
            ],
            'started_at:date',
            'expired_at:date',
            [
                'attribute' => 'used_count',
                'label' => 'Активировано',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
