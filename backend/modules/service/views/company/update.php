<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/19/15
 * Time: 3:17 PM
 * Email: t.kanstantsin@gmail.com
 */

/* @var $this yii\web\View */
/* @var $model \common\models\Company */

$this->title = 'Обновить компанию: ' . ' ' . $model->name_short;
?>
<div class="edit-profile">
    <?= $this->render('@frontend/views/company/form/_form', [
        'model' => $model,
        'cancelUrl' => \yii\helpers\Url::to(['view']),
        'admin' => true,
    ]) ?>
</div>

