<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/19/15
 * Time: 3:13 PM
 * Email: t.kanstantsin@gmail.com
 */

use common\models\Company;
use common\models\company\CompanyType;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\Company */

$this->title = $model->name_short;

$view = $model->company_type_id == CompanyType::TYPE_IP ?
        Company::$companyFormView[CompanyType::TYPE_IP] :
        Company::$companyFormView[CompanyType::TYPE_OOO];
?>

<div class="edit-profile form-horizontal">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption bold-text">
                <?= $model->name_short; ?>
            </div>

            <div class="actions">
                <a href="<?php echo Url::to(['update',]); ?>" title="Редактировать" class=" darkblue btn-sm">
                    <i class="icon-pencil"></i>
                </a>
            </div>
        </div>

        <?= $this->render('@frontend/views/company/view-profile/' . $view, [
            'company' => $model,
            'admin' => true,
        ]); ?>
    </div>
</div>
