<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.08.2017
 * Time: 10:26
 */

use common\models\Company;
use yii\data\ActiveDataProvider;
use backend\modules\service\models\SellingPaymentSearch;
use frontend\widgets\RangeButtonWidget;
use common\components\TextHelper;
use yii\bootstrap\Html;
use common\components\grid\GridView;
use common\models\service\Payment;
use common\components\date\DateHelper;
use common\models\service\PaymentType;
use common\components\grid\DropDownSearchDataColumn;
use yii\helpers\Url;
use php_rutils\RUtils;
use common\models\service\StoreOutInvoiceTariff;
use common\components\helpers\ArrayHelper;

/* @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel SellingPaymentSearch
 */
$this->title = 'Оплаты';
?>
<div class="selling-subscribe-index">
    <div class="row">
        <div class="col-md-3 col-md-push-9">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
        <div class="col-md-9 col-md-pull-3">
            <h1 class="page-title" style="font-size: 20px;">Кол-во
                оплат: <?= $dataProvider->totalCount; ?></h1>

            <h1 class="page-title" style="font-size: 20px;">Сумма
                оплат: <?= TextHelper::moneyFormat($dataProvider->query->sum('sum'), 2); ?></h1>
        </div>
    </div>
    <div class="row" id="widgets">
        <div class="col-md-3 col-sm-3">
            <div class="dashboard-stat _yellow" style="position: relative;">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details" style="padding-top: 5px;">
                    <?php $totalCount = $totalAmount = 0; ?>
                    <?php foreach ($searchModel->organicTypes as $organicType => $organicTypeText): ?>
                        <?php $data = $searchModel->getStatisticByFormSource($organicType);
                        $totalCount += $data['count'];
                        $totalAmount += $data['totalSum']; ?>
                        <div class="desc" style="font-size: 15px;">
                            <?= $organicTypeText . ' - ' . $data['count'] . 'шт / ' .
                            TextHelper::moneyFormat($data['totalSum'] ? $data['totalSum'] : 0, 2) . ' р.'; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="more">
                    ORGANIC: <?= $totalCount; ?> шт. / <?= TextHelper::moneyFormat($totalAmount, 2) ?> р.
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="dashboard-stat _red" style="position: relative;">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details" style="padding-top: 5px;">
                    <?php $totalCount = $totalAmount = 0; ?>
                    <?php foreach ($searchModel->cpcTypes as $cpcType => $cpcTypeText): ?>
                        <?php $data = $searchModel->getStatisticByFormSource($cpcType);
                        $totalCount += $data['count'];
                        $totalAmount += $data['totalSum']; ?>
                        <div class="desc" style="font-size: 15px;">
                            <?= $cpcTypeText . ' - ' . $data['count'] . 'шт / ' .
                            TextHelper::moneyFormat($data['totalSum'] ? $data['totalSum'] : 0, 2) . ' р.'; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="more">
                    CPC: <?= $totalCount; ?> шт. / <?= TextHelper::moneyFormat($totalAmount, 2) ?> р.
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="dashboard-stat _green" style="position: relative;">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details" style="padding-top: 5px;">
                    <?php $totalCount = $totalAmount = 0; ?>
                    <?php foreach ($searchModel->emailTypes as $emailType => $emailTypeText): ?>
                        <?php $data = $searchModel->getStatisticByFormSource($emailType);
                        $totalCount += $data['count'];
                        $totalAmount += $data['totalSum']; ?>
                        <div class="desc" style="font-size: 15px;">
                            <?= $emailTypeText . ' - ' . $data['count'] . 'шт / ' .
                            TextHelper::moneyFormat($data['totalSum'] ? $data['totalSum'] : 0, 2) . ' р.'; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="more">
                    E-mail: <?= $totalCount; ?> шт. / <?= TextHelper::moneyFormat($totalAmount, 2) ?> р.
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="dashboard-stat _yellow" style="position: relative;">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details" style="padding-top: 5px;">
                    <?php $totalCount = $totalAmount = 0; ?>
                    <?php foreach ($searchModel->channelTypes as $channelType => $channelTypeText): ?>
                        <?php $data = $searchModel->getStatisticByFormSource($channelType);
                        $totalCount += $data['count'];
                        $totalAmount += $data['totalSum']; ?>
                        <div class="desc" style="font-size: 15px;">
                            <?= $channelTypeText . ' - ' . $data['count'] . 'шт / ' .
                            TextHelper::moneyFormat($data['totalSum'] ? $data['totalSum'] : 0, 2) . ' р.'; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="more">
                    Каналы: <?= $totalCount; ?> шт. / <?= TextHelper::moneyFormat($totalAmount, 2) ?> р.
                </div>
            </div>
        </div>
    </div>

    <?php if ($searchModel->form_source && array_key_exists($searchModel->form_source, $searchModel->cpcTypes)): ?>
        <?php
        $raw = $searchModel->getCampaigns();
        $campaigns = [];
        foreach ($raw as $campaign) {
            parse_str(trim($campaign['company']['form_utm'], '?'), $output);
            if ($campaignName = ArrayHelper::getValue($output, 'utm_campaign')) {
                if (!isset($campaigns[$campaignName]))
                    $campaigns[$campaignName] = 0;

                $campaigns[$campaignName]++;
            } else {
                if (!isset($campaigns['---']))
                    $campaigns['---'] = 0;

                $campaigns['---']++;
            }
        }
        ?>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12" style="min-width: 280px;">
                <div class="portlet box darkblue">
                    <div class="portlet-title">
                        <div class="caption">
                            Статистика по UTM campaign
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dataTable documents_table">
                            <thead>
                                <tr>
                                    <th>UTM Campaign</th>
                                    <th>Кол-во компаний</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($campaigns as $campaignName => $companiesCount): ?>
                                    <tr>
                                        <td><?= $campaignName ?></td>
                                        <td class="text-right"><?= $companiesCount ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => ['invoice'],
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input text-middle"
                             style="width: 83%!important;">
                            <?= $form->field($searchModel, 'search')->textInput([
                                'placeholder' => 'Поиск по ID, названию, ИНН или E-mail',
                            ]); ?>
                        </div>
                        <div class="wimax_button" style="float: right;">
                            <?= Html::submitButton('ПРИМЕНИТЬ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'options' => [
                    'class' => 'overflow-x',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'columns' => [
                    [
                        'attribute' => 'company.created_at',
                        'label' => 'Дата регистрации',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Payment $model) {
                            return date(DateHelper::FORMAT_USER_DATE, $model->company->created_at);
                        },
                    ],
                    [
                        'attribute' => 'payment_date',
                        'label' => 'Дата оплаты',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Payment $model) {
                            return $model->type_id == PaymentType::TYPE_ONLINE ?
                                date(DateHelper::FORMAT_USER_DATETIME, $model->payment_date) :
                                date(DateHelper::FORMAT_USER_DATE, $model->payment_date);
                        },
                    ],
                    [
                        'attribute' => 'invoice.document_date',
                        'label' => 'Дата счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Payment $model) {
                            if ($model->outInvoice) {
                                return DateHelper::format($model->outInvoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'invoice.document_number',
                        'label' => '№ счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Payment $model) {
                            if ($model->outInvoice) {
                                return $model->outInvoice->getFullNumber();
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'sum',
                        'label' => 'Сумма',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Payment $model) {
                            return TextHelper::moneyFormat($model->sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'type',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Тип платежа',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getTypeFilter(),
                        'value' => function (Payment $model) {
                            return $model->type->name;
                        },
                    ],
                    [
                        'attribute' => 'outContractor',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Покупатель',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getEmployeeFilter(),
                        'value' => function (Payment $model) {
                            if ($model->outInvoice) {
                                return "{$model->outInvoice->contractor->nameWithType}<br>(ID = {$model->company->id})";
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'outContractor.email',
                        'label' => 'email',
                        'format' => 'raw',
                        'value' => function (Payment $model) {
                            return Html::a($model->company->email, 'mailto:' . $model->company->email);
                        },
                    ],
                    [
                        'attribute' => 'companyNameShort',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Компания',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'html',
                        'filter' => $searchModel->getCompanyFilter(),
                        'value' => function (Payment $model) {
                            return Html::a($model->company->getShortName(), Url::to(['/company/company/view', 'id' => $model->company->id]));
                        },
                    ],
                    [
                        'attribute' => 'tariff',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Тип подписки',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'selectPluginOptions' => [
                            'width' => '300px'
                        ],
                        'format' => 'html',
                        'filter' => $searchModel->getTariffFilter(),
                        'value' => function (Payment $model) {
                            $content = '';
                            if ($model->payment_for == Payment::FOR_STORE_CABINET) {
                                $period = (int)$model->sum == (int)$model->storeTariff->total_amount ?
                                    'год' : 'месяц';
                                $content = $model->storeTariff->cabinets_count . ' ' .
                                    RUtils::numeral()->choosePlural($model->storeTariff->cabinets_count, [
                                        'кабинет', //1
                                        'кабинета', //2
                                        'кабинетов' //5
                                    ]) . " на 1 {$period}.";
                            } elseif ($model->payment_for == Payment::FOR_OUT_INVOICE) {
                                $content = 'Ссылок ' . $model->outInvoiceTariff->links_count .
                                    ($model->outInvoiceTariff->id == StoreOutInvoiceTariff::UNLIM_TIME_AND_LINKS_TARIFF ? ' Безлимитный' : ' на 1 год');
                            } elseif ($model->payment_for == Payment::FOR_ODDS) {
                                $content = 'Настройка финансовых отчетов';
                            } elseif ($model->payment_for == Payment::FOR_ADD_INVOICE) {
                                $content = $model->invoiceTariff->invoice_count . ' ' .
                                    RUtils::numeral()->choosePlural($model->invoiceTariff->invoice_count, [
                                        'счет', //1
                                        'счета', //2
                                        'счетов' //5
                                    ]);
                            } elseif ($tariff = $model->tariff) {
                                $content = $tariff->tariffGroup->name . ' <br>' . $tariff->getTariffName();
                            }

                            return $content;
                        },
                    ],
                    [
                        'attribute' => 'serial_number',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => '№ платежа',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getSubscribeNumberFilter(),
                        'value' => function (Payment $model) {
                            return $model->serial_number;
                        },
                    ],
                    [
                        'attribute' => 'form_source',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Источник или канал',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getFormSourceFilter(),
                        'value' => function (Payment $model) {
                            return $model->company->form_source;
                        },
                    ],
                    [
                        'attribute' => 'company.form_keyword',
                        'label' => 'Ключевое слово',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Payment $model) {
                            return $model->company->form_keyword;
                        },
                    ],
                    [
                        'attribute' => 'form_region',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Регион',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getFormRegionFilter(),
                        'value' => function (Payment $model) {
                            return $model->company->form_region;
                        },
                    ],
                    [
                        'attribute' => 'registrationPageType',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Страница регистрации',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getRegistrationPageTypeFilter(),
                        'value' => function (Payment $model) {
                            return $model->company->registrationPageType ? $model->company->registrationPageType->name : '';
                        }
                    ],
                    [
                        'attribute' => 'utm_campaign',
                        'label' => 'UTM campaign',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'value' => function (Payment $model) {
                            parse_str(trim($model->company->form_utm, '?'), $output);
                            return ArrayHelper::getValue($output, 'utm_campaign');
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
