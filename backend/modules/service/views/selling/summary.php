<?php

use frontend\widgets\RangeButtonWidget;
use common\components\helpers\Html;
use yii\bootstrap\Dropdown;
use yii\data\ActiveDataProvider;
use backend\modules\service\models\SellingSubscribeSearch;
use common\components\grid\GridView;
use common\models\selling\SellingSubscribe;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\components\grid\DropDownSearchDataColumn;
use yii\web\View;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $summaryData array
 */
$this->title = 'Свод';

$currYear = Yii::$app->request->get('year', date('Y'));
$dropYears = [];
for ($i=date('Y'); $i>=2015; $i--) {

    $dropYears[] = [
        'label' => $i,
        'url' => Url::to(['summary', 'year' => $i]),
    ];

}

?>
<div class="selling-subscribe-index">
    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <div class="dropdown pull-right">
                        <div style="display: inline-block; vertical-align: top; padding: 12px 8px 0 0;">Период</div>
                        <div style="display: inline-block; padding:4px 4px 0 0;">
                            <div class="dropdown">
                                <?= \yii\helpers\Html::a(' ' . $currYear . ' <span class="caret"></span>', null, [
                                    'class' => 'btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom doc-gray-button btn_select_days btn_row',
                                    'id' => 'dropdownMenuYear',
                                    'data-toggle' => 'dropdown',
                                    'aria-expanded' => true,
                                ]); ?>
                                <?= Dropdown::widget([
                                    'items' => $dropYears,
                                    'options' => [
                                        'style' => 'top: 36px;',
                                        'aria-labelledby' => 'dropdownMenuYear',
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div id="w1" style="overflow-x: auto;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                    <tr class="heading bb">
                        <th rowspan="2">Месяц, год</th>
                        <th colspan="2">Новые<br>КУБ.Выставление счетов</th>
                        <th colspan="2">Повторные<br>КУБ.Выставление счетов</th>
                        <th colspan="2">Пролонг-ные<br>КУБ.Выставление счетов</th>
                        <th colspan="2">Разовые счета</th>
                        <th colspan="2">Итого<br>КУБ.Выставление счетов</th>
                        <th colspan="2">Новые<br>Бухгалтерия ИП</th>
                        <th colspan="2">Повторные<br>Бухгалтерия ИП</th>
                        <th colspan="2">Итого<br>Бухгалтерия ИП</th>
                        <th colspan="2">Модуль В2В</th>
                        <th colspan="2">Итого</th>
                    </tr>
                    <tr class="heading">
                        <th>Кол-во</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($summaryData as $data): ?>
                        <?php
                        $standart_cnt = ArrayHelper::getValue($data['kub_standart'], 'cnt', 0);
                        $standart_sum = ArrayHelper::getValue($data['kub_standart'], 'sum', 0);
                        $standart_new_cnt = ArrayHelper::getValue($data['kub_standart_new'], 'cnt', 0);
                        $standart_new_sum = ArrayHelper::getValue($data['kub_standart_new'], 'sum', 0);
                        $standart_repeated_cnt = ArrayHelper::getValue($data['kub_standart_repeated'], 'cnt', 0);
                        $standart_repeated_sum = ArrayHelper::getValue($data['kub_standart_repeated'], 'sum', 0);
                        $add_invoice_cnt = ArrayHelper::getValue($data['kub_add_invoice'], 'cnt', 0);
                        $add_invoice_sum = ArrayHelper::getValue($data['kub_add_invoice'], 'sum', 0);
                        $ip_usn_new_cnt = ArrayHelper::getValue($data['kub_ip_usn_new'], 'cnt', 0);
                        $ip_usn_new_sum = ArrayHelper::getValue($data['kub_ip_usn_new'], 'sum', 0);
                        $ip_usn_cnt = ArrayHelper::getValue($data['kub_ip_usn'], 'cnt', 0);
                        $ip_usn_sum = ArrayHelper::getValue($data['kub_ip_usn'], 'sum', 0);
                        $b2b_cnt = ArrayHelper::getValue($data['kub_b2b'], 'cnt', 0);
                        $b2b_sum = ArrayHelper::getValue($data['kub_b2b'], 'sum', 0);

                        $standart_prolong_cnt = max(0, $standart_cnt - $standart_new_cnt - $standart_repeated_cnt);
                        $standart_prolong_sum = max(0, $standart_sum - $standart_new_sum - $standart_repeated_sum);
                        $ip_usn_repeated_cnt = max(-0,$ip_usn_cnt - $ip_usn_new_cnt);
                        $ip_usn_repeated_sum = max(0, $ip_usn_sum - $ip_usn_new_sum);
                        ?>
                        <tr>
                            <td><?= $data['month'] ?></td>
                            <td><?= $standart_new_cnt ?></td>
                            <td><?= TextHelper::numberFormat($standart_new_sum) ?></td>
                            <td><?= $standart_repeated_cnt ?></td>
                            <td><?= TextHelper::numberFormat($standart_repeated_sum) ?></td>
                            <td><?= $standart_prolong_cnt ?></td>
                            <td><?= TextHelper::numberFormat($standart_prolong_sum) ?></td>
                            <td><?= $add_invoice_cnt ?></td>
                            <td><?= TextHelper::numberFormat($add_invoice_sum) ?></td>
                            <td><b><?= $standart_cnt ?></b></td>
                            <td><b><?= TextHelper::numberFormat($standart_sum) ?></b></td>
                            <td><?= $ip_usn_new_cnt ?></td>
                            <td><?= TextHelper::numberFormat($ip_usn_new_sum) ?></td>
                            <td><?= $ip_usn_repeated_cnt ?></td>
                            <td><?= TextHelper::numberFormat($ip_usn_repeated_sum) ?></td>
                            <td><b><?= $ip_usn_cnt ?></b></td>
                            <td><b><?= TextHelper::numberFormat($ip_usn_sum) ?></b></td>
                            <td><?= $b2b_cnt ?></td>
                            <td><?= TextHelper::numberFormat($b2b_sum) ?></td>
                            <td><b><?= $standart_cnt + $add_invoice_cnt + $ip_usn_cnt + $b2b_cnt ?></b></td>
                            <td><b><?= TextHelper::numberFormat($standart_sum + $add_invoice_sum + $ip_usn_sum + $b2b_sum) ?></b></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>