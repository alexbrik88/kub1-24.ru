<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.08.2017
 * Time: 7:02
 */

use frontend\widgets\RangeButtonWidget;
use common\components\helpers\Html;
use yii\data\ActiveDataProvider;
use backend\modules\service\models\SellingSubscribeSearch;
use common\components\grid\GridView;
use common\models\selling\SellingSubscribe;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\components\grid\DropDownSearchDataColumn;

/* @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel SellingSubscribeSearch
 */
$this->title = 'Подписки';
?>
<div class="selling-subscribe-index">
    <div class="row">
        <div class="col-md-3 col-md-push-9">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
        <div class="col-md-9 col-md-pull-3"></div>
    </div>
    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => ['subscribe'],
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input text-middle"
                             style="width: 83%!important;">
                            <?= $form->field($searchModel, 'search')->textInput([
                                'placeholder' => 'Поиск по ID, названию, ИНН или E-mail',
                            ]); ?>
                        </div>
                        <div class="wimax_button" style="float: right;">
                            <?= Html::submitButton('ПРИМЕНИТЬ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'options' => [
                    'class' => 'overflow-x',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (SellingSubscribe $model) {
                            return $model->id;
                        },
                    ],
                    [
                        'attribute' => 'companyName',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Компания',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getCompanyFilter(),
                        'value' => function (SellingSubscribe $model) {
                            return Html::a($model->company->getShortName(), Url::to(['/company/company/view', 'id' => $model->company_id]));
                        },
                    ],
                    [
                        'attribute' => 'company.email',
                        'label' => 'E-mail',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (SellingSubscribe $model) {
                            return Html::a($model->company->email, 'mailto:' . $model->company->email);
                        },
                    ],
                    [
                        'attribute' => 'type',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Подписка',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getTypeFilter(),
                        'value' => function (SellingSubscribe $model) {
                            return $model->typeArray[$model->type];
                        },
                    ],
                    [
                        'attribute' => 'main_type',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Тип',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getMainTypeFilter(),
                        'value' => function (SellingSubscribe $model) {
                            return $model->mainTypeArray[$model->main_type];
                        },
                    ],
                    [
                        'attribute' => 'invoice_number',
                        'label' => 'Счет',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (SellingSubscribe $model) {
                            return $model->invoice_number ? $model->invoice_number : '';
                        },
                    ],
                    [
                        'attribute' => 'invoice_date',
                        'label' => 'Дата счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (SellingSubscribe $model) {
                            return $model->subscribe && $model->subscribe->payment && $model->subscribe->payment->outInvoice ?
                                DateHelper::format($model->subscribe->payment->outInvoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                                '';
                        },
                    ],
                    [
                        'attribute' => 'creation_date',
                        'label' => 'Дата оплаты',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (SellingSubscribe $model) {
                            return date(DateHelper::FORMAT_USER_DATE, $model->creation_date);
                        },
                    ],
                    [
                        'attribute' => 'activation_date',
                        'label' => 'Дата активации',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (SellingSubscribe $model) {
                            return $model->activation_date ? date(DateHelper::FORMAT_USER_DATE, $model->activation_date) : null;
                        },
                    ],
                    [
                        'attribute' => 'end_date',
                        'label' => 'Дата окончания',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (SellingSubscribe $model) {
                            return $model->end_date ? date(DateHelper::FORMAT_USER_DATE, $model->end_date) : '-';
                        },
                    ],
                    [
                        'attribute' => 'status',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Статус подписки',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getStatusFilter(),
                        'value' => function (SellingSubscribe $model) {
                            return $model->end_date >= time() || $model->end_date == null ? 'Активна' : 'Не активна';
                        },
                    ]
                ],
            ]); ?>
        </div>
    </div>
</div>