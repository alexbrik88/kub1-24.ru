<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.08.2017
 * Time: 6:49
 */

use yii\bootstrap\Modal;
use common\models\document\Invoice;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\components\helpers\Html;
use common\components\date\DateHelper;

/* @var $this yii\web\View
 * @var $model Invoice
 * @var $url string
 * @var $modalID string
 * @var $title string
 */
?>
<?php Modal::begin([
    'header' => '<h3 style="text-align: center; margin: 0">' . $title . '</h3>',
    'id' => $modalID,
]); ?>

<?php $paidInvoiceForm = ActiveForm::begin([
    'action' => $url,
    'options' => [
        'class' => 'form-horizontal form-' . $model->id,
        'id' => 'form-' . $model->id,
    ],
    'enableClientValidation' => true,
]); ?>

<?php if ($model->payment): ?>
    <?= $paidInvoiceForm->field($model->payment, 'payment_date', [
        'options' => [
            'class' => 'form-group',
        ],
        'labelOptions' => [
            'class' => 'col-md-3 control-label bold-text',
        ],
        'inputOptions' => [
            'class' => 'form-control date-picker',
            'value' => $model->payment->payment_date ?
                date(DateHelper::FORMAT_USER_DATE, $model->payment->payment_date) :
                date(DateHelper::FORMAT_USER_DATE),
        ],
        'wrapperOptions' => [
            'class' => 'col-md-8 inp_one_line-product',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ])->label('Дата оплаты:'); ?>
<?php else: ?>
    <div class="form-group field-payment-payment_date">
        <label class="col-md-3 control-label bold-text"
               for="payment-payment_date">Дата оплаты:</label>

        <div class="col-md-8 inp_one_line-product">
            <?= Html::textInput('Payment[payment_date]', date(DateHelper::FORMAT_USER_DATE), [
                'id' => 'payment-payment_date',
                'class' => 'form-control date-picker',
            ]); ?>
            <p class="help-block help-block-error"></p>
        </div>
    </div>
<?php endif; ?>

    <div class="form-actions" style="margin-bottom: 10px;">
        <div class="row action-buttons">
            <div class="col-sm-3 col-xs-3">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn darkblue text-white hidden-md hidden-sm hidden-xs',
                    'style' => 'width: 100% !important',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue text-white hidden-lg',
                    'title' => 'Сохранить',
                    'style' => 'width: 100% !important',
                ]); ?>
            </div>
            <div class="col-sm-6 col-xs-6"></div>
            <div class="col-sm-3 col-xs-3">
                <?= Html::a('Отменить', null, [
                    'class' => 'btn darkblue hidden-md hidden-sm hidden-xs close-modal-button',
                    'style' => 'width: 100% !important',
                ]); ?>
                <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', null, [
                    'class' => 'btn darkblue hidden-lg close-modal-button',
                    'title' => 'Отменить',
                    'style' => 'width: 100% !important',
                ]); ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>