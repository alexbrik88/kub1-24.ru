<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.08.2017
 * Time: 6:53
 */

use backend\modules\service\models\SellingInvoiceSearch;
use common\components\TextHelper;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $color string
 * @var $status integer
 * @var $sum string
 * @var $count integer
 */
?>
<div class="dashboard-stat <?= $color; ?>" style="position: relative;">
    <div class="visual">
        <i class="fa fa-comments"></i>
    </div>
    <div class="details">
        <div class="number fontSizeSmall">
            <span class="details-sum"
                  data-value="<?= $sum ?>"><?= TextHelper::invoiceMoneyFormat($sum, 2); ?></span>
            <i class="fa fa-rub"></i>
        </div>
        <div class="desc">
            <?= SellingInvoiceSearch::$subscribeStatusDashBoardArray[$status]; ?>
        </div>
    </div>
    <div class="more">
        Количество счетов: <?= $count; ?>
    </div>
    <?= Html::a(null, Url::to(['invoice', 'SellingInvoiceSearch[subscribeStatus]' => $status]), [
        'style' => 'display: inline-block; position: absolute; top: 0; bottom: 0; left: 0; right: 0;',
    ]); ?>
</div>
