<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.08.2017
 * Time: 16:12
 */

use yii\data\ActiveDataProvider;
use backend\modules\service\models\SellingInvoiceSearch;
use frontend\widgets\RangeButtonWidget;
use yii\bootstrap\Html;
use common\models\document\Invoice;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\components\date\DateHelper;
use common\models\service\PaymentType;
use common\components\grid\DropDownSearchDataColumn;
use yii\helpers\Url;
use common\models\document\status\InvoiceStatus;
use common\models\service\Subscribe;
use common\models\service\Payment;
use php_rutils\RUtils;
use common\models\service\StoreOutInvoiceTariff;

/* @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel SellingInvoiceSearch
 */
$this->title = 'Счета';
?>
<div class="selling-subscribe-index">
    <div class="row" id="widgets">
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_partial/dashboard_block', [
                'color' => '_yellow',
                'status' => SellingInvoiceSearch::SUBSCRIBE_STATUS_CREATED,
                'sum' => $dataProvider->query->sum('total_amount_with_nds'),
                'count' => $dataProvider->totalCount,
            ]); ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_partial/dashboard_block', [
                'color' => '_red',
                'status' => SellingInvoiceSearch::SUBSCRIBE_STATUS_OVERDUE,
                'sum' => $searchModel->getBaseQuery()
                    ->andWhere(['and',
                        ['>', 'DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(`' . Payment::tableName() . '`.`created_at`)))', 10],
                        ['is_confirmed' => 0],
                    ])
                    ->sum('total_amount_with_nds'),
                'count' => $searchModel->getBaseQuery()
                    ->andWhere(['and',
                        ['>', 'DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(`' . Payment::tableName() . '`.`created_at`)))', 10],
                        ['is_confirmed' => 0],
                    ])
                    ->count(),
            ]); ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_partial/dashboard_block', [
                'color' => '_green',
                'status' => SellingInvoiceSearch::SUBSCRIBE_STATUS_PAYED,
                'sum' => $searchModel->getBaseQuery()
                    ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                    ->sum('total_amount_with_nds'),
                'count' => $searchModel->getBaseQuery()
                    ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                    ->count(),
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
    </div>
    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => ['invoice'],
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input text-middle"
                             style="width: 83%!important;">
                            <?= $form->field($searchModel, 'search')->textInput([
                                'placeholder' => 'Поиск по ID, названию, ИНН или E-mail',
                            ]); ?>
                        </div>
                        <div class="wimax_button" style="float: right;">
                            <?= Html::submitButton('ПРИМЕНИТЬ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'options' => [
                    'class' => 'overflow-x',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover documents_table dataTable',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'columns' => [
                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата счёта',
                        'headerOptions' => [
                            //'class' => 'sorting',
                        ],
                        'value' => function (Invoice $model) {
                            return date(DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS, $model->payment->created_at);
                        },
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№ счёта',
                        'headerOptions' => [
                            //'class' => 'sorting',
                        ],
                        'value' => function (Invoice $model) {
                            return $model->getFullNumber();
                        },
                    ],
                    [
                        'attribute' => 'total_amount_with_nds',
                        'label' => 'Сумма',
                        'headerOptions' => [
                            //'class' => 'sorting',
                        ],
                        'value' => function (Invoice $model) {
                            return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                        },
                    ],
                    [
                        'attribute' => 'paymentType',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Тип платежа',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getPaymentTypeFilter(),
                        'value' => function (Invoice $model) {
                            return $model->payment->type->name;
                        },
                    ],
                    [
                        'attribute' => 'contractorName',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Компания',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getContractorFilter(),
                        'value' => function (Invoice $model) {
                            return $model->contractor->nameWithType;
                        },
                    ],
                    [
                        'attribute' => 'companyNameShort',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Компания',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getCompanyFilter(),
                        'value' => function (Invoice $model) {
                            return Html::a($model->payment->company->getShortName(),
                                Url::to(['/company/company/view', 'id' => $model->payment->company->id]));
                        },
                    ],
                    [
                        'attribute' => 'subscribeStatus',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Статус',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getInvoiceStatusFilter(),
                        'value' => function (Invoice $model) {
                            $dateDiff = floor((time() - $model->payment->created_at) / 60 / 60 / 24);
                            if ($model->payment) {
                                if ($model->payment->is_confirmed) {
                                    return SellingInvoiceSearch::$subscribeStatusArray[SellingInvoiceSearch::SUBSCRIBE_STATUS_PAYED];
                                }
                            }
                            if ($dateDiff >= 10) {
                                return SellingInvoiceSearch::$subscribeStatusArray[SellingInvoiceSearch::SUBSCRIBE_STATUS_OVERDUE];

                            }

                            return SellingInvoiceSearch::$subscribeStatusArray[SellingInvoiceSearch::SUBSCRIBE_STATUS_CREATED];
                        },
                    ],
                    [
                        'attribute' => 'payment_date',
                        'label' => 'Дата оплаты',
                        'headerOptions' => [
                            //'class' => 'sorting',
                        ],
                        'value' => function (Invoice $model) {
                            if ($model->payment) {
                                if ($model->payment->payment_date) {
                                    if ($model->payment->type_id == PaymentType::TYPE_ONLINE) {
                                        return date(DateHelper::FORMAT_USER_DATETIME, $model->payment->payment_date);
                                    }
                                    return date(DateHelper::FORMAT_USER_DATE, $model->payment->payment_date);
                                }
                            }
                            return '';
                        },
                    ],
                    [
                        'attribute' => 'tariffType',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Тип подписки',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'selectPluginOptions' => [
                            'width' => '300px'
                        ],
                        'filter' => $searchModel->getTariffFilter(),
                        'format' => 'html',
                        'value' => function (Invoice $model) {
                            if ($model->payment->storeTariff) {
                                return $model->payment->storeTariff->cabinets_count . ' ' .
                                    RUtils::numeral()->choosePlural($model->payment->storeTariff->cabinets_count, [
                                        'кабинет', //1
                                        'кабинета', //2
                                        'кабинетов' //5
                                    ]);
                            } elseif ($model->payment->outInvoiceTariff) {
                                if ($model->payment->outInvoiceTariff->id == StoreOutInvoiceTariff::UNLIM_TARIFF) {
                                    return $model->payment->outInvoiceTariff->links_count . ' ссылок';
                                }
                                return $model->payment->outInvoiceTariff->links_count . ' ' .
                                    RUtils::numeral()->choosePlural($model->payment->outInvoiceTariff->links_count, [
                                        'ссылка', //1
                                        'ссылки', //2
                                        'ссылок' //5
                                    ]);
                            } elseif ($model->payment->payment_for == Payment::FOR_ODDS) {
                                return 'Настройка финансовых отчетов';
                            } elseif ($model->payment->payment_for == Payment::FOR_ADD_INVOICE) {
                                return $model->payment->invoiceTariff->invoice_count . ' ' .
                                    RUtils::numeral()->choosePlural($model->payment->invoiceTariff->invoice_count, [
                                        'счет', //1
                                        'счета', //2
                                        'счетов' //5
                                    ]);
                            } elseif ($orders = $model->payment->orders) {
                                $content = '';
                                foreach ($orders as $k => $o) {
                                    $tariff = $o->tariff;
                                    $content .= Html::tag('div', $tariff->tariffGroup->name.'<br>'.$tariff->getTariffName(), [
                                        'class' => $k ? 'mt-2' : null,
                                    ]);
                                }

                                return $content;
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'subscribe_id',
                        'label' => 'ID подписки',
                        'headerOptions' => [
                            //'class' => 'sorting',
                        ],
                        'value' => function (Invoice $model) {
                            return $model->payment->subscribe ? $model->payment->subscribe->id : '-';
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {undo} {paid} {delete}',
                        'buttons' => [
                            'update' => function ($url, Invoice $model, $key) {
                                if (!$model->payment->is_confirmed &&
                                    $model->payment->type_id !== PaymentType::TYPE_ONLINE
                                ) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                                            'title' => 'Редактировать',
                                            'aria-label' => 'Редактировать',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#update-payment-date-' . $model->id,
                                        ]) . $this->render('_partial/modal_invoice', [
                                            'model' => $model,
                                            'url' => Url::to(['update-payment-date', 'id' => $model->id]),
                                            'title' => 'Редиктировать дату оплаты',
                                            'modalID' => 'update-payment-date-' . $model->id,
                                        ]);
                                }
                            },
                            'undo' => function ($url, Invoice $model, $key) {
                                if ($model->payment->is_confirmed) {
                                    return Html::a('<span class="glyphicon glyphicon-ban-circle"></span>',
                                        Url::to(['undo', 'id' => $model->id, Yii::$app->request->get()]), [
                                            'title' => 'Отменить',
                                            'aria-label' => 'Отменить',
                                            'data-confirm' => 'Вы действительно хотите отменить этот счет?',
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                        ]);
                                }
                            },
                            'delete' => function ($url, Invoice $model, $key) {
                                $options = [
                                    'title' => 'Удалить',
                                    'aria-label' => 'Удалить',
                                    'data-pjax' => '0',
                                    'data-confirm' => "Вы уверенны, что хотите удалить этот счет?",
                                    'data-method' => 'post',
                                ];
                                $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-trash"]);

                                return Html::a($icon, Url::to(['delete-invoice', 'id' => $model->id]), $options);
                            },
                            'paid' => function ($url, Invoice $model, $key) {
                                if (!$model->payment || !$model->payment->is_confirmed) {
                                    return Html::a('Оплачен', null, [
                                            'class' => 'btn btn-primary btn-success btn_in_table',
                                            'title' => 'Оплачен',
                                            'aria-label' => 'Оплачен',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#paid-invoice-' . $model->id,
                                            'style' => 'margin-top: 5px;',
                                        ]) . $this->render('_partial/modal_invoice', [
                                            'model' => $model,
                                            'url' => Url::to(['paid-invoice', 'id' => $model->id, Yii::$app->request->get()]),
                                            'title' => 'Оплата счета',
                                            'modalID' => 'paid-invoice-' . $model->id,
                                        ]);
                                }
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
