<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\service\Subscribe */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Подписки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-view">

    <h1><?= Html::encode($this->title); ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => '№ счета',
                'value' => $model->invoice ? $model->invoice->document_number : 'Нет',
            ],
            'company_id',
            'tariff_id',
            'created_at:datetime',
            'updated_at:datetime',
            'activated_at:datetime',
            'expired_at:datetime',
            [
                'attribute' => 'status_id',
                'value' => $model->status->name,
            ],

            [
                'label' => 'Оплата',
                'format' => 'raw',
                'value' => $model->payment !== null
                    ? Html::a($model->payment->id, ['payment/view', 'id' => $model->payment->id,])
                    : null,
            ],
        ],
    ]); ?>

</div>
