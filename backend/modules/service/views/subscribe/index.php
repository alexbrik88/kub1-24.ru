<?php

use common\models\service;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подписки';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="payment-index">

    <div class="portlet box">
        <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
        </div>
        <div class="portlet-body">
            <?= \common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,

                'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                'options' => [
                    'class' => 'overflow-x',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],

                'columns' => [
                    'id',
                    [
                        'attribute' => 'company_id',
                        'value' => function (service\Subscribe $model) {
                            return '(' . $model->company_id . ') ' . $model->company->name_short;
                        },
                    ],
                    [
                        'attribute' => 'tariff_id',
                        'value' => function (service\Subscribe $model) {
                            return $model->tariff !== null ? $model->tariff->getTariffName() : null;
                        },
                    ],
                    [
                        'attribute' => 'payment',
                        'label' => 'Оплата',
                        'format' => 'raw',
                        'value' => function (service\Subscribe $model) {
                            return $model->payment !== null
                                ? Html::a($model->payment->id, ['payment/view', 'id' => $model->payment->id,])
                                : null;
                        },
                    ],
                    'created_at:datetime',
                    'updated_at:datetime',
                    'activated_at:datetime',
                    'expired_at:datetime',
                    [
                        'attribute' => 'status_id',
                        'value' => 'status.name',
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>

</div>
