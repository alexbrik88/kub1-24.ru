<?php

namespace backend\modules\service;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\service\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
