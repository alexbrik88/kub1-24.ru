<?php

/* @var \yii\web\View $this */
/* @var common\models\service\PromoCode $promoCode */
/* @var \yii\mail\BaseMessage $message */
/* @var common\models\Company $company */
?>


<style>
    div, p, span, strong, b, em, i, a, li, td {
        -webkit-text-size-adjust: none;
    }
</style>

<table width="600" cellpadding="0" cellspacing="0" border="1" style="border-collapse: collapse;">
    <tbody>
    <tr>
        <td style="padding: 10px;"><strong><?= $company->getTitle() ?></strong></td>
    </tr>

    <tr>
        <td style="padding: 10px;">
            <p>Добрый день!</p>

            <p>
                Ваш промокод <?= $promoCode->code ?>
            </p>

            <p>
               Для активации промокода, введите его в разделе оплата
            </p>
        </td>
    </tr>

    <tr>
        <td style="padding: 10px;">
            <table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                <tbody>
                <tr>
                    <td>
                        Это письмо было отправлено <br/>
                        через он-лайн сервис для бизнеса КУБ <br/>

                        <?= \yii\helpers\Html::a('<span style="color: #267f00;">Попробовать бесплатно</span>',
                            Yii::$app->params['serviceSite'], Yii::$app->params['email']['link']); ?>
                    </td>
                    <td align="right">
                        <?= \yii\helpers\Html::img($message->embed(Yii::getAlias(Yii::$app->params['email']['logo']))); ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
