<?php

namespace backend\modules\company\controllers;


use backend\components\BackendController;
use backend\modules\company\models\PartnerSearch;

use Yii;
use yii\helpers\Url;

class PartnerController extends BackendController
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['site/index']));
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $searchModel = new PartnerSearch();
        $dataProvider = $searchModel->search(Yii::$app->requestedParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalCount' => $dataProvider->totalCount,
        ]);
    }
}