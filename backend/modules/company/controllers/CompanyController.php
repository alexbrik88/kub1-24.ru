<?php
namespace backend\modules\company\controllers;

use backend\components\BackendController;
use backend\modules\company\models\AffiliateSearch;
use backend\modules\company\models\CompanyAdditionalSearch;
use backend\modules\company\models\CompanyDocumentsSearch;
use backend\modules\company\models\CompanySearch;
use backend\modules\company\models\PaymentSearch;
use backend\modules\company\models\PromoCodeSearch;
use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CompanyAffiliateLeadRewards;
use common\models\company\CompanyAffiliateLeadRewardsSearch;
use common\models\company\CompanyAffiliateLink;
use common\models\company\CompanyAffiliateLinkSearch;
use common\models\company\CompanyAffiliateSearch;
use common\models\company\CompanyInvoiceSearch;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\service\ServiceModule;
use frontend\components\PageSize;
use frontend\components\StatisticPeriod;
use frontend\models\EmployeeCompanySearch;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\modules\subscribe\forms\PromoCodeForm;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * Invoice controller
 */
class CompanyController extends BackendController
{

    /**
     * Lists models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexAdditional()
    {
        $this->layout = 'company';
        $searchModel = new CompanyAdditionalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-additional', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexDocuments()
    {
        $this->layout = 'company';
        $searchModel = new CompanyDocumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-documents', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoice model.
     *
     * @param $id
     * @param string $backUrl
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id, $backUrl = 'index')
    {
        $this->layout = 'view-company';

        /** @var Company $model */
        $model = $this->findModel($id);

        $paymentSearchModel = new PaymentSearch([
            'company_id' => $model->id,
        ]);
        $paymentDataProvider = $paymentSearchModel->search([]);

        $promocodeSearchModel = new PromoCodeSearch([
            'company_id' => $model->id,
        ]);
        $promocodeDataProvider = $promocodeSearchModel->search([]);

        $employeeSearchModel = new EmployeeCompanySearch([
            'company_id' => $model->id,
        ]);
        $employeeDataProvider = $employeeSearchModel->search([], $model->id);

        $affiliateSearchModel = new AffiliateSearch();
        $affiliateDataProvider = $affiliateSearchModel->search($model, Yii::$app->request->get());

        $affiliateLeadRewardsSearchModel = new CompanyAffiliateLeadRewardsSearch([
            'company_id' => $model->id
        ]);
        $dataLeadRewardsProvider = $affiliateLeadRewardsSearchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $model,
            'paymentSearchModel' => $paymentSearchModel,
            'paymentDataProvider' => $paymentDataProvider,
            'promocodeSearchModel' => $promocodeSearchModel,
            'promocodeDataProvider' => $promocodeDataProvider,
            'employeeSearchModel' => $employeeSearchModel,
            'employeeDataProvider' => $employeeDataProvider,
            'affiliateSearchModel' => $affiliateSearchModel,
            'affiliateDataProvider' => $affiliateDataProvider,
            'affiliateLeadRewardsSearchModel' => $affiliateLeadRewardsSearchModel,
            'dataLeadRewardsProvider' => $dataLeadRewardsProvider,
            'backUrl' => $backUrl,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewFormCard($id)
    {
        /** @var Company $model */
        $model = $this->findModel($id);

        return $this->render('view-form-card', [
            'model' => $model,
        ]);
    }

    /**
     * @param $company_id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewPartnerCabinet($company_id)
    {
        /** @var Company $model */
        $model = $this->findModel($company_id);
        $tab = \Yii::$app->request->get('tab', null);

        if ($model->is_partner) {
            CompanyAffiliateLink::generateAffiliateLinks($company_id);
            CompanyAffiliateLeadRewards::generateAffiliateRewards($company_id);
        }

        $searchModel = new CompanyAffiliateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $dataProvider->pagination->pageSize = PageSize::get();

        $invoiceSearchModel = new CompanyInvoiceSearch();
        $invoiceDataProvider = $invoiceSearchModel->search(Yii::$app->request->get());

        $invoiceDataProvider->pagination->pageSize = PageSize::get();

        $affiliateSearchModel = new CompanyAffiliateLinkSearch();
        $affiliateDataProvider = $affiliateSearchModel->search(Yii::$app->request->get());

        $affiliateDataProvider->pagination->pageSize = PageSize::get();

        $affiliateLeadRewardsSearchModel = new CompanyAffiliateLeadRewardsSearch(['company_id' => $company_id]);
        $affiliateLeadRewardsDataProvider = $affiliateLeadRewardsSearchModel->search(Yii::$app->request->get());

        $affiliateLeadRewardsDataProvider->pagination->pageSize = PageSize::get();


        return $this->render('view-partner-cabinet', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'invoiceSearchModel' => $invoiceSearchModel,
            'invoiceDataProvider' => $invoiceDataProvider,
            'affiliateSearchModel' => $affiliateSearchModel,
            'affiliateDataProvider' => $affiliateDataProvider,
            'affiliateLeadRewardsSearchModel' => $affiliateLeadRewardsSearchModel,
            'affiliateLeadRewardsDataProvider' => $affiliateLeadRewardsDataProvider,

            'tab' => $tab,
        ]);
    }

    /**
     * @return false|string
     */
    public function actionEditAffiliateLink()
    {
        $id = \Yii::$app->request->post('id');
        $name = \Yii::$app->request->post('name');

        $model = CompanyAffiliateLink::findOne(['id' => $id]);

        $model->name = $name;

        if ($model->save(true)) {
            return json_encode(['success' => 'true']);
        }

        return json_encode(['success' => 'false']);
    }

    /**
     * @return array|string[]
     * @throws NotFoundHttpException
     */
    public function actionLeadRewardsSave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = (int)Yii::$app->request->post('id');
        $company_id = (int)Yii::$app->request->post('company_id');

        /** @var CompanyAffiliateLeadRewards $model */
        $model = CompanyAffiliateLeadRewards::find()
            ->andWhere(['and',
                ['id' => $id],
                ['company_id' => $company_id],
            ])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('Ошибка обработки входящих данных');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            CompanyAffiliateLeadRewards::setIsCurrentRewardsLevel($company_id);
            return ['success' => 'true'];
        }

        return ['success' => 'false'];
    }

    /**
     * @return array
     */
    public function actionCustomLinkValidate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new CompanyAffiliateLink();

        if ($model->load(Yii::$app->request->post())) {
            return ActiveForm::validate($model);
        }

        return [];
    }

    /**
     * @param $company_id
     * @return string
     * @throws Exception
     */
    public function actionCreateCustomLink($company_id)
    {
        $model = new CompanyAffiliateLink();

        $params = Yii::$app->request->post();

        if (!empty($params)) {
            CompanyAffiliateLink::generateCustomAffiliateLink($company_id, Yii::$app->request->post('CompanyAffiliateLink'));

            Yii::$app->session->setFlash('success', 'Ссылка создана.');

            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('_viewPartials/affiliate/_edit_links_form', [
            'model' => $model,
            'products' => ServiceModule::$serviceModuleLabel,
        ]);
    }

    /**
     * @param $company_id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdateAffiliateLink($company_id)
    {
        $id = (int)Yii::$app->request->get('id', null);

        $model = $this->findModelAffiliateLink($id, $company_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Ссылка обновлена.');

            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('_viewPartials/affiliate/_edit_links_form', [
            'model' => $model,
            'products' => ServiceModule::$serviceModuleLabel,
        ]);
    }

    /**
     * @param null $id
     * @param $company_id
     * @return array|ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModelAffiliateLink($id = null, $company_id = null)
    {
        $model = CompanyAffiliateLink::find()
            ->andWhere([
                'id' => $id,
                'company_id' => $company_id,
            ])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested model does not exist.');
        }

        return $model;
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSubscribeHistory($id)
    {
        /** @var Company $model */
        $model = $this->findModel($id);

        $paymentSearchModel = new PaymentSearch([
            'company_id' => $model->id,
        ]);
        $paymentDataProvider = $paymentSearchModel->search(Yii::$app->request->queryParams);

        return $this->render('_viewPartials/_partial_payment_history', [
            'model' => $model,
            'paymentSearchModel' => $paymentSearchModel,
            'paymentDataProvider' => $paymentDataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPromocodes($id)
    {
        /** @var Company $model */
        $model = $this->findModel($id);

        $promocodeSearchModel = new PromoCodeSearch([
            'company_id' => $model->id,
        ]);
        $promocodeDataProvider = $promocodeSearchModel->search(Yii::$app->request->queryParams);

        return $this->render('_viewPartials/_partial_promocodes', [
            'model' => $model,
            'promocodeSearchModel' => $promocodeSearchModel,
            'promocodeDataProvider' => $promocodeDataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEmployees($id)
    {
        /** @var Company $model */
        $model = $this->findModel($id);

        $searchModel = new EmployeeCompanySearch([
            'company_id' => $model->id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $model->id);

        return $this->render('_viewPartials/_partial_employees', [
            'company' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company([
            'company_type_id' => CompanyType::TYPE_OOO,
        ]);
        $model->setScenario(Company::SCENARIO_ADMIN_INSERT);
        $companyTaxation = new CompanyTaxationType();
        $model->populateRelation('companyTaxationType', $companyTaxation);

        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $companyTaxation->load(Yii::$app->request->post())) {

            $saved = Yii::$app->db->transaction(function (Connection $db) use ($model, $companyTaxation) {
                if ($model->save() && Contractor::createFounder($model) && $model->createTrialSubscribe()) {
                    $companyTaxation->company_id = $model->id;
                    if ($companyTaxation->save()) {
                        return true;
                    }
                }

                $db->transaction->rollBack();

                return false;
            });

            if ($saved) {
                $model->main_id = $model->id;
                if ($model->save(true, ['main_id'])) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view'
     * page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $searchModel = new CheckingAccountantSearch(['company_id' => $model->id]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get(), false);
        $checkingAccountant = new CheckingAccountant([
            'company_id' => $model->id,
        ]);
        $model->setScenario(Company::SCENARIO_ADMIN_UPDATE);
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if (Yii::$app->request->isGet && !Yii::$app->request->isAjax) {
            $uploadPath = $model->getUploadPath(true, true);
            FileHelper::removeDirectory($uploadPath);
            FileHelper::createDirectory($uploadPath);
        }

        if (Yii::$app->request->isPost) {
            $isOldKubTheme = ArrayHelper::getValue(Yii::$app->request->post('Employee'), 'is_old_kub_theme');
            if (1 == strlen($isOldKubTheme)) {
                foreach ($model->employees as $employee) {
                    $employee->is_old_kub_theme = ($isOldKubTheme) ? 1 : 0;
                    $employee->save(false);
                }
            }
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->logoImage = UploadedFile::getInstance($model, 'logoImage');
            $model->printImage = UploadedFile::getInstance($model, 'printImage');
            $model->chiefSignatureImage = UploadedFile::getInstance($model, 'chiefSignatureImage');

            if ($model->validate() &&
                $model->companyTaxationType->load(Yii::$app->request->post()) &&
                $model->companyTaxationType->validate()
            ) {
                $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                    if ($model->save() && $model->companyTaxationType->save()) {
                        return true;
                    }
                });

                if ($isSaved) {
                    return $this->redirect(Yii::$app->request->get('return_url') ?: ['view', 'id' => $model->id]);
                }
            }
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            \common\components\helpers\ModelHelper::logErrors($model->companyTaxationType, __METHOD__);
        }

        return $this->render('update', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'checkingAccountant' => $checkingAccountant,
        ]);
    }

    /**
     * Blocked/unblocked an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionBlock($id)
    {
        $model = $this->findModel($id);

        if ($model->blocked == Company::BLOCKED) {
            $model->blocked = Company::UNBLOCKED;
        } else {
            $model->blocked = Company::BLOCKED;
            /* @var $employeeCompany EmployeeCompany
             * @var $randomEmployeeCompany EmployeeCompany
             */
            foreach (EmployeeCompany::find()->andWhere(['company_id' => $model->id])->all() as $employeeCompany) {
                $employee = $employeeCompany->employee;
                $randomEmployeeCompany = EmployeeCompany::find()
                    ->joinWith('company')
                    ->andWhere(['!=', 'company_id', $model->id])
                    ->andWhere(['employee_id' => $employee->id])
                    ->andWhere(['company.blocked' => Company::UNBLOCKED])
                    ->one();
                if ($randomEmployeeCompany) {
                    $employee->company_id = $randomEmployeeCompany->company_id;
                    $employee->save(true, ['company_id']);
                }
            }
        }

        $model->save(true, ['blocked']);

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionVisitCard($id)
    {
        $company = $this->findModel($id);

        return $this->render('visit-card', [
            'company' => $company,
        ]);
    }

    /**
     * @return Response
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function actionMakePayment($id)
    {
        $company = $this->findModel($id);

        \Yii::$app->params['paymentUser'] = $company->employeeChief;
        if (Yii::$app->params['paymentUser'] === null) {
            \Yii::$app->session->setFlash('error', 'Ошибка при создании подписки. Не найден руководитель компании.');

            return $this->redirect(['view', 'id' => $company->id]);
        }

        if ($company->mainCheckingAccountant === null) {
            \Yii::$app->session->setFlash('error', 'Ошибка при создании подписки. Не найден расчетный счет компании.');

            return $this->redirect(['view', 'id' => $company->id]);
        }

        if (!filter_var($company->email, FILTER_VALIDATE_EMAIL)) {
            $company->email = \Yii::$app->params['paymentUser']->email;
            if (!filter_var($company->email, FILTER_VALIDATE_EMAIL)) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создании подписки. Email не найден.');

                return $this->redirect(['view', 'id' => $company->id]);
            }
        }

        if (!$company->strict_mode) {
            $model = new PaymentForm($company);

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }

            if ($model->load(\Yii::$app->request->post())) {
                if (!$model->makePayment()) {
                    \Yii::$app->session->setFlash('error', 'Форма оплаты заполнена неправильно.');
                }
            }
        } else {
            \Yii::$app->session->setFlash('emptyCompany',
                '<div id="w1-error-0" class="alert-danger alert fade in">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="display: block">×</button>
                Данное действие недоступно. ' . Html::a('Заполните информацию о компании.', Url::to([
                    'update', 'id' => $company->id,
                ])) . '</div>');

        }

        return $this->redirect(['view', 'id' => $company->id]);
    }

    /**
     * @return string|Response
     * @throws \Exception
     */
    public function actionActivatePromoCode($id)
    {
        $this->layout = '@frontend/views/layouts/pdf';
        $company = $this->findModel($id);
        $hasActualSubscription = $company->hasActualSubscription;

        \Yii::$app->params['paymentUser'] = $company->getEmployees()->andWhere(['employee_role_id' => EmployeeRole::ROLE_CHIEF])->one();
        $model = new PromoCodeForm([
            'company' => $company,
        ]);

        $saved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
                return true;
            }

            $db->transaction->rollBack();

            return false;
        });

        if ($saved) {
            Yii::$app->session->setFlash('success', 'Промокод  успешно зарегистирован.');
            if (!$hasActualSubscription) {
                $model->subscribe->activate();
                $company->setActiveSubscribe($model->subscribe);
            }

            return $this->redirect(['view', 'id' => $company->id]);
        } else {
            return $this->renderAjax('_viewPartials/_partial_promocode_form', [
                'promoCodeForm' => $model,
                'company' => $company,
            ]);
        }
    }

    /**
     * @param $companyId
     * @return mixed|string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreateCheckingAccountant($companyId)
    {
        $company = $this->findModel($companyId);
        $model = new CheckingAccountant([
            'company_id' => $company->id,
        ]);
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::to(['update', 'id' => $company->id]));
        }

        return $this->render('form/_create_checking_accountant', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @param $companyId
     * @return mixed|string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateCheckingAccountant($id, $companyId)
    {
        $company = $this->findModel($companyId);
        /* @var $model CheckingAccountant */
        $model = $company->getCheckingAccountants()->andWhere(['id' => $id])->one();
        if ($model == null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::to(['update', 'id' => $company->id]));
        }

        return $this->render('form/_update_checking_accountant', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @param $companyId
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDeleteCheckingAccountant($id, $companyId)
    {
        $company = $this->findModel($companyId);
        /* @var $model CheckingAccountant */
        $model = $company->getCheckingAccountants()->andWhere(['id' => $id])->one();
        if ($model == null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        $model->delete();

        return $this->redirect(Url::to(['update', 'id' => $company->id]));
    }

    /**
     * @param $id
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionChangeStatus($id)
    {
        $company = $this->findModel($id);
        $company->load(\Yii::$app->request->post());
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $company->save(true, ['test']);
    }

    /**
     * @param $company_id
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionChangePartnerStatus($company_id)
    {
        /** @var Company $company */
        $company = $this->findModel($company_id);

        $status = Yii::$app->request->post('status');

        try {
            if (!$company->invitedCompanies || !empty($status)) {
                $company->updateAttributes(['is_partner' => !empty($status),]);
                CompanyAffiliateLink::generateAffiliateLinks($company->id);

                if (empty($status)) {
                    $company->updateAttributes(['affiliate_link_deleted_at' => time(),]);
                }
            }
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage());
            return json_encode(['success' => 'false']);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return json_encode(['success' => 'true']);
    }

    /**
     * @param $id
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionChangeComment()
    {
        if (Yii::$app->request->isAjax){
            $company = $this->findModel(Yii::$app->request->post('id'));
            $company->comment = Yii::$app->request->post('comment');
            \Yii::$app->response->format = Response::FORMAT_JSON;

            return $company->save(true, ['comment']);
        }

    }

    /**
     * @param $id
     * @param null $comID
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionChangeActivities($id, $comID = null)
    {
        $model = $this->findModel($id);
        $activitiesID = Yii::$app->request->post('activities_id');
        $model->activities_id = $activitiesID;
        $model->save(true, ['activities_id']);
        $redirectUrl = Yii::$app->request->referrer;
        if ($comID) {
            $redirectUrl = str_replace('comID=' . $comID, 'comID=' . $id, $redirectUrl);
            $redirectUrl = str_replace('#anchor', '', $redirectUrl);
            $redirectUrl .= '#anchor';
        } else {
            $redirectUrl .= stristr($redirectUrl, '?') == false ? '?' : '&';
            $redirectUrl .= 'comID=' . $id . '#anchor';
        }

        return $this->redirect($redirectUrl);
    }

    /**
     *
     */
    public function actionCompanyAdditionalExcel()
    {
        $searchModel = new CompanyAdditionalSearch();

        $dateRange = StatisticPeriod::getSessionPeriod();

        $excel = new Excel();

        $fileName = 'Расширенный_список_компаний_' . DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $searchModel->searchModelsForExcel(\Yii::$app->request->get()),
            'title' => 'Компании',
            'rangeHeader' => array_merge(range('A', 'Z'), ['AA']),
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => function (Company $model) {
                        return $model->id;
                    },
                ],
                [
                    'attribute' => 'name_short',
                    'value' => function (Company $model) {
                        return $model->name_short;
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'phone',
                    'value' => function (Company $model) {
                        return $model->phone;
                    },
                ],
                [
                    'attribute' => 'createdByFio',
                    'value' => function (Company $model) {
                        return ($model->created_by && ($createdByEmployee = Employee::findOne($model->created_by)))
                            ? $createdByEmployee->getFio()
                            : '---';
                    },
                ],
                [
                    'attribute' => 'chiefFio',
                    'value' => function (Company $model) {
                        return $model->chiefFio;
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'value' => function ($model) {
                        return date(DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS, $model->created_at);
                    }
                ],
                [
                    'attribute' => 'inn',
                    'value' => function (Company $model) {
                        return $model->inn;
                    },
                ],
                [
                    'attribute' => 'company_type_id',
                    'value' => function (Company $model) {
                        return $model->companyType->name_short;
                    },
                ],
                [
                    'attribute' => 'companyTaxationType',
                    'value' => function (Company $model) {
                        return $model->companyTaxationType->name;
                    },
                ],
                [
                    'attribute' => 'bankName',
                    'value' => function (Company $model) {
                        return $model->mainCheckingAccountant ? $model->mainCheckingAccountant->bank_name : '(Не задано)';
                    },
                ],
                [
                    'attribute' => 'outInvoiceCount',
                    'value' => function (Company $model) {
                        return $model->outInvoiceCount;
                    },
                ],
                [
                    'attribute' => 'inInvoiceCount',
                    'value' => function (Company $model) {
                        return $model->inInvoiceCount;
                    },
                ],
                [
                    'attribute' => 'outInvoiceSendEmailCount',
                    'value' => function (Company $model) {
                        return $model->outInvoiceSendEmailCount;
                    },
                ],
                [
                    'attribute' => 'isFileLogo',
                    'value' => function (Company $model) {
                        return $model->isFileLogo;
                    },
                ],
                [
                    'attribute' => 'isFilePrint',
                    'value' => function (Company $model) {
                        return $model->isFilePrint;
                    },
                ],
                [
                    'attribute' => 'isFileSignature',
                    'value' => function (Company $model) {
                        return $model->isFileSignature;
                    },
                ],
                [
                    'attribute' => 'filesCount',
                    'value' => function (Company $model) {
                        return $model->filesCount;
                    },
                ],
                [
                    'attribute' => 'activation_type',
                    'value' => function (Company $model) {
                        return isset(Company::$activationType[$model->activation_type]) ? Company::$activationType[$model->activation_type] : '';
                    },
                ],
                [
                    'attribute' => 'activeTariff',
                    'value' => function (Company $model) {
                        return $model->hasActualSubscription ?
                            $model->activeSubscribe->getTariffName() :
                            ($model->isFreeTariff ? 'Тариф "Бесплатно"' : 'Нет');
                    },
                ],
                [
                    'attribute' => 'activeSubscribeDaysCount',
                    'value' => function (Company $model) {
                        return $model->activeSubscribeDaysCount;
                    },
                ],
                [
                    'attribute' => 'customersCount',
                    'value' => function (Company $model) {
                        return $model->getCustomersCount();
                    },
                ],
                [
                    'attribute' => 'sellersCount',
                    'value' => function (Company $model) {
                        return $model->getSellersCount();
                    },
                ],
                [
                    'attribute' => 'autoInvoicesCount',
                    'value' => function (Company $model) {
                        return $model->getAutoInvoicesCount();
                    },
                ],
                [
                    'attribute' => 'outActCount',
                    'value' => function (Company $model) {
                        return $model->getOutActCount();
                    },
                ],
                [
                    'attribute' => 'paymentOrderCount',
                    'value' => function (Company $model) {
                        return $model->getPaymentOrderCount();
                    },
                ],
                [
                    'attribute' => 'import_xls_product_count',
                    'value' => function (Company $model) {
                        return (int)$model->import_xls_product_count;
                    },
                ],
                [
                    'attribute' => 'import_xls_service_count',
                    'value' => function (Company $model) {
                        return (int)$model->import_xls_service_count;
                    },
                ],
                [
                    'attribute' => 'goodsCount',
                    'value' => function (Company $model) {
                        return $model->getGoodsCount();
                    },
                ],
                [
                    'attribute' => 'servicesCount',
                    'value' => function (Company $model) {
                        return $model->getServicesCount();
                    },
                ],
            ],
            'headers' => [
                'id' => 'ID',
                'name_short' => 'Название организации',
                'email' => 'Email',
                'phone' => 'Телефон',
                'createdByFio' => 'ФИО регистрация',
                'chiefFio' => 'ФИО руководителя',
                'created_at' => 'Дата регистрации',
                'inn' => 'ИНН',
                'company_type_id' => ' ФС  ',
                'companyTaxationType' => ' СНО  ',
                'bankName' => 'Банк',
                'outInvoiceCount' => 'Кол-во ИСХ счетов',
                'inInvoiceCount' => 'Кол-во ВХ счетов',
                'outInvoiceSendEmailCount' => 'Количество отправленных счетов',
                'isFileLogo' => 'Лого',
                'isFilePrint' => 'Печать',
                'isFileSignature' => 'Подпись',
                'filesCount' => 'Итого',
                'activation_type' => 'Статус',
                'activeTariff' => 'Тарифный план',
                'activeSubscribeDaysCount' => 'Осталось дней (Всего)',
                'customersCount' => 'Покупатели',
                'sellersCount' => 'Продавцы',
                'autoInvoicesCount' => 'Автосчета',
                'outActCount' => 'Актсверки',
                'paymentOrderCount' => 'Платежки',
                'import_xls_product_count' => 'Товары из Excel',
                'import_xls_service_count' => 'Услуги из Excel',
                'goodsCount' => 'Товары',
                'servicesCount' => 'Услуги',
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->_delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     *
     * @return Company
     * @throws NotFoundHttpException
     */
    protected function findModel($id = null)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }

        $model = Company::findOne($id);

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}
