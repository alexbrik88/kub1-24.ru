<?php

namespace backend\modules\company\controllers;

use backend\components\BackendController;
use common\models\Company;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\TimeZone;
use frontend\rbac\permissions;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends BackendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-remote' => ['post'],
                    'owner' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $companyId
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($companyId, $id)
    {
        $company = $this->findCompanyModel($companyId);
        $model = EmployeeCompany::findOne(['employee_id' => $id, 'company_id' => $company->id]);
        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $model,
            'company' => $company,
            'companyId' => $companyId,
        ]);
    }

    /**
     * Creates a new Employee model.
     * @param $companyId
     * @return mixed
     */
    public function actionCreate($companyId)
    {
        $company = $this->findCompanyModel($companyId);

        $model = new EmployeeCompany([
            'scenario' => 'create',
            'company_id' => $company->id,
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
        ]);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Сотрудник добавлен');

            return $this->redirect(Yii::$app->request->get('return_url') ?: ['view', 'companyId' => $company->id, 'id' => $model->employee_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'companyId' => $companyId,
        ]);
    }

    /**
     * Update a single Employee model.
     * @param $companyId
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($companyId, $id)
    {
        $company = $this->findCompanyModel($companyId);
        $model = EmployeeCompany::findOne(['employee_id' => $id, 'company_id' => $company->id]);
        if ($model === null) {
            throw new NotFoundHttpException();
        }

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Сотрудник изменен');

            return $this->redirect(['/company/company/view', 'id' => $company->id,]);
        }

        return $this->render('update', [
            'model' => $model,
            'companyId' => $companyId,
        ]);
    }

    /**
     * Set Employee as owner of the Company.
     * @param integer $companyId
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionOwner($companyId, $id)
    {
        $company = $this->findCompanyModel($companyId);
        $model = EmployeeCompany::findOne(['employee_id' => $id, 'company_id' => $company->id]);
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        if (!$model->is_working) {
            Yii::$app->session->setFlash(
                'error',
                'Нельзя назначить владельцем учетной записи компании уволенного сотрудника.'
            );
        } elseif (!$model->employee->is_active || $model->employee->is_deleted) {
            Yii::$app->session->setFlash(
                'error',
                'Нельзя назначить владельцем учетной записи компании заблокированного или удаленного пользователя.'
            );
        } else {
            if ($company->owner_employee_id != $id) {
                $company->updateAttributes([
                    'owner_employee_id' => $id,
                ]);
            }
            if ($model->employee_role_id != EmployeeRole::ROLE_CHIEF) {
                $model->updateAttributes([
                    'employee_role_id' => EmployeeRole::ROLE_CHIEF,
                    'updated_at' => time(),
                ]);
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['view', 'companyId' => $companyId, 'id' => $id]);
    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $companyId
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($companyId, $id)
    {
        $company = $this->findCompanyModel($companyId);
        $model = $this->findModel($id);
        $employeeCompany = EmployeeCompany::findOne(['employee_id' => $model->id, 'company_id' => $company->id]);
        if ($employeeCompany === null) {
            throw new NotFoundHttpException();
        }
        if ($employeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
            \Yii::$app->session->setFlash('error', 'Нельзя удалить руководителя.');
            return $this->redirect(['view', 'companyId' => $company->id, 'id' => $model->id,]);
        }
        if ($company->getEmployees0()->isActual()->count() == 1) {
            \Yii::$app->session->setFlash('error', 'Нельзя удалить последнего сотрудника.');
            return $this->redirect(['view', 'companyId' => $company->id, 'id' => $model->id,]);
        }

        $model->unlink('companies', $company, true);
        $companyArray = $model
            ->getCompanies()
            ->andWhere([
                Company::tableName() . '.blocked' => Company::UNBLOCKED
            ])
            ->orderBy([Company::tableName() . '.strict_mode' => SORT_ASC])
            ->indexBy('id')
            ->all();

        if (count($companyArray) > 0) {
            if (!isset($companyArray[$model->main_company_id])) {
                $model->main_company_id = array_values($companyArray)[0]->id;
            }
            if (!isset($companyArray[$model->company_id])) {
                $model->company_id = $model->main_company_id;
            }
            $model->save(false, ['company_id', 'main_company_id']);
        } else {
            $model->is_active = Employee::NOT_ACTIVE;
            $model->is_deleted = Employee::DELETED;
            $model->save(false, [
                'is_active', 'is_deleted',
            ]);
        }

        return $this->redirect(['/company/company/view', 'id' => $company->id,]);
    }

    /**
     * @param $companyId
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDeleteRemote($companyId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = $this->findCompanyModel($companyId);
        $employeeCompany = EmployeeCompany::findOne([
            'employee_id' => ArrayHelper::getValue(Yii::$app->params, ['service', 'remote_employee_id']),
            'company_id' => $company->id,
        ]);

        if ($employeeCompany && !$employeeCompany->delete()) {
            return [
                'deleted' => 0,
            ];
        }

        return [
            'deleted' => 1,
        ];
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCompanyModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
