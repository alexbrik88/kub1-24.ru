<?php

namespace backend\modules\company\forms;

use common\models\Company;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\PromoCode;
use yii\base\Model;
use common\components\sender\unisender\UniSender;

/**
 * Class InvoiceSendForm
 *
 * @package backend\modules\company\forms
 */
class PromocodeSendForm extends Model
{
    /**
     * @var PromoCode
     */
    public $promoCode;

    /**
     * @var Company
     */
    public $company;

    /**
     * @inheritdoc
     */
    public function send()
    {
        if ($chief = $this->company->getEmployeeChief()) {
            return \Yii::$app->mailer->compose([
                    'html' => 'system/promocode/html',
                    'text' => 'system/promocode/text',
                ], [
                    'promoCode' => $this->promoCode,
                    'company' => $this->company,
                    'supportEmail' => \Yii::$app->params['emailList']['support'],
                    'user' => $chief,
                    'subject' => 'Промокод',
                ])
                ->setFrom([\Yii::$app->params['emailList']['support'] => \Yii::$app->params['emailFromName']])
                ->setTo($chief->email)
                ->setSubject('Промокод')
                ->send();
        }

        return false;
    }
}
