<?php

namespace backend\modules\company\modules\import;

/**
 * import module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\company\modules\import\controllers';

    /**
     * {@inheritdoc}
     */
    public $layout = '@backend/modules/company/views/layouts/view-company';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
