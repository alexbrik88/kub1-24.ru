<?php

namespace backend\modules\company\modules\import\components;

use Yii;
use common\models\Company;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ImportBaseController controller for the `import` module
 */
class ImportBaseController extends Controller
{
    public function getCompany() : Company
    {
        $model = Company::findOne(Yii::$app->request->get('id'));

        if ($model === null) {
            throw new NotFoundHttpException('The requested model does not exist.');
        }

        return $model;
    }
}
