<?php

namespace backend\modules\company\modules\import\models;

use common\models\Company;
use common\models\EmployeeCompany;
use common\models\cash\CashFlowsBase;
use common\models\company\CheckingAccountant;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\web\UploadedFile;

/**
 * EnrichmentForm
 */
class EnrichmentForm extends \yii\base\Model
{
    const ATTRIBUTES = [
        'A' => 'date',
        'B' => 'amount',
        'C' => 'description',
        'D' => 'item',
        'E' => 'project',
        'F' => 'contractor',
        'G' => 'recognition_date',
        'H' => 'payment_order_number',
    ];

    public $file;

    private Company $company;
    private ?CheckingAccountant $account = null;
    private bool $success = false;
    private int $itemsCount = 0;
    private int $existsCount = 0;
    private int $successCount = 0;
    private string $flowClass = '';
    private string $errorMessage = '';
    private array $flows = [];
    private array $items = [];
    private array $result = [];
    private array $incomeItems = [];
    private array $expenseItems = [];
    private array $projects = [];
    private array $matchedFlows = [];
    private array $updatedFlows = [];
    private array $updatedAttr = [];

    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getResult()
    {
        return $this->result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx, xls, csv'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);

        $this->file = UploadedFile::getInstance($this, 'file');

        return $load || $this->file !== null;
    }

    public function save(bool $validate = true) : bool
    {
        $this->account = null;
        $this->success = false;
        $this->itemsCount = 0;
        $this->existsCount = 0;
        $this->successCount = 0;
        $this->errorMessage = '';
        $this->matchedFlows = [];
        $this->updatedFlows = [];
        $this->updatedAttr = [];
        $this->items = [];
        $this->result = [];
        $rowCellCount = count(self::ATTRIBUTES);

        if ($validate && !$this->validate()) {
            return false;
        }

        $xls = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->file->tempName);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        if ($sheet->getHighestRow() < 5) {
            $this->addError('file', 'В файле не найдены операции.');

            return false;
        }

        $rs = trim($sheet->getCellByColumnAndRow(2, 2)->getValue());

        if ($rs) {
            $this->account = $this->company->getCheckingAccountants()->andWhere([
                'rs' => $rs,
            ])->orderBy([
                'type'=> SORT_ASC,
            ])->one();
        }

        if ($this->account === null) {
            $this->addError('file', 'Не найден расчетный счет компании');

            return false;
        }

        $this->flowClass = $this->account->getCashFlowClass();
        $flowQuery = $this->flowClass::find()->andWhere([
            'company_id' => $this->company->id,
            'rs' => $this->account->rs,
        ]);

        $this->flows = (clone $flowQuery)->select('id')->column();
        $this->flows = array_combine($this->flows, $this->flows);

        $rowIterator = $sheet->getRowIterator();

        $rowIterator->resetStart(5);

        foreach ($rowIterator as $key => $row) {
            $data = [];
            $cellIterator = $row->getCellIterator();

            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                $attr = self::ATTRIBUTES[$cellKey] ?? null;
                if ($attr === null || count($data) == $rowCellCount) {
                    break;
                }
                $data[$attr] = trim($cell->getCalculatedValue());
            }

            if (empty($data['date']) || count($data) !== $rowCellCount) {
                break;
            }

            $this->itemsCount++;
            $data['row_id'] = $key;
            $data['amount'] = is_numeric($data['amount']) ? intval(bcmul($data['amount'], 100)) : null;
            if ($data['date'] && is_numeric($data['date'])) {
                $data['date'] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($data['date']);
            }
            if ($data['recognition_date'] && is_numeric($data['recognition_date'])) {
                $data['recognition_date'] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($data['recognition_date']);
            }
            $this->processData($data, clone $flowQuery);
        }

        if ($this->itemsCount === 0) {
            $this->addError('file', 'В файле не найдены операции.');

            return false;
        }

        $ids = array_keys($this->flows);

        $this->result = [
            'dataProvider1' => new ArrayDataProvider([
                'allModels' => $this->updatedFlows,
                'pagination' => false,
            ]),
            'dataProvider2' => new ActiveDataProvider([
                'query' => $flowQuery->andWhere(['not', ['id' => array_keys($this->matchedFlows)]]),
                'pagination' => false,
            ]),
            'dataProvider3' => new ArrayDataProvider([
                'allModels' => $this->items,
                'pagination' => false,
            ]),
            'updatedAttr' => $this->updatedAttr,
            'itemsCount' => $this->itemsCount,
            'existsCount' => count($this->matchedFlows),
            'successCount' => count($this->updatedFlows),
        ];

        return true;
    }

    private function processData(array $data, \yii\db\Query $flowQuery) : void
    {
        if ($data['date'] && $data['amount'] && $data['payment_order_number']) {
            $query = $flowQuery->andWhere([
                'flow_type' => $data['amount'] < 0 ? CashFlowsBase::FLOW_TYPE_EXPENSE : CashFlowsBase::FLOW_TYPE_INCOME,
                'amount' => abs($data['amount']),
                'date' => $data['date'] ? $data['date']->format('Y-m-d') : null,
                'payment_order_number' => $data['payment_order_number'],
            ]);

            /* @var $flow CashFlowsBase */
            $flow = $query->one();
        } else {
            $flow = null;
        }

        if ($flow === null) {
            $this->items[] = $data;

            return;
        }

        $this->matchedFlows[$flow->id] = $flow;

        $updateAttributes = [];

        if ($data['item']) {
            $name = mb_strtolower($data['item']);
            if ($flow->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
                if (!isset($this->incomeItems[$name])) {
                    $this->incomeItems[$name] = InvoiceIncomeItem::find()->andWhere([
                        'or',
                        [
                            'company_id' => null,
                            'company_id' => $this->company->id,
                        ]
                    ])->andWhere([
                        'name' => $data['item'],
                    ])->select('id')->scalar() ?: false;
                }
                if ($this->incomeItems[$name] && $flow->income_item_id != $this->incomeItems[$name]) {
                    $updateAttributes['income_item_id'] = $this->incomeItems[$name];
                }
            } elseif ($flow->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                if (!isset($this->expenseItems[$name])) {
                    $this->expenseItems[$name] = InvoiceExpenditureItem::find()->andWhere([
                        'or',
                        [
                            'company_id' => null,
                            'company_id' => $this->company->id,
                        ]
                    ])->andWhere([
                        'name' => $data['item'],
                    ])->select('id')->scalar() ?: false;
                }
                if ($this->expenseItems[$name] && $flow->expenditure_item_id != $this->expenseItems[$name]) {
                    $updateAttributes['expenditure_item_id'] = $this->expenseItems[$name];
                }
            }
        }
        if ($data['recognition_date']) {
            $recognition_date = $data['recognition_date']->format('Y-m-d');
            if ($recognition_date != $flow->recognition_date) {
                $updateAttributes['recognition_date'] = $recognition_date;
            }
        }
        if ($data['project']) {
            $name = mb_strtolower($data['project']);
            if (!isset($this->projects[$name])) {
                $this->projects[$name] = Project::find()->andWhere([
                    'company_id' => $this->company->id,
                    'name' => $data['project'],
                ])->select('id')->scalar() ?: false;
            }
            if ($this->projects[$name] && $flow->project_id != $this->projects[$name]) {
                $updateAttributes['project_id'] = $this->projects[$name];
            }
        }

        if (!empty($updateAttributes)) {
            $this->updatedFlows[] = $flow;
            $this->updatedAttr[$flow->id] = $updateAttributes;
            $flow->updateAttributes($updateAttributes);
            $this->successCount++;
        }
    }

    private function findIncomeItem($name) {

    }
}
