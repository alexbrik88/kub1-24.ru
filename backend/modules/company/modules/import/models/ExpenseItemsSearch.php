<?php

namespace backend\modules\company\modules\import\models;

use backend\modules\company\modules\import\models\ExpenseItemsSearch;
use common\models\Company;
use common\models\document\InvoiceExpenditureItem;
use yii\data\ActiveDataProvider;

/**
 * ExpenseItemsSearch
 */
class ExpenseItemsSearch extends InvoiceExpenditureItem
{
    private $company;

    public function setCompany(Company $company) : void
    {
        $this->company = $company;
    }

    public function getCompany() : Company
    {
        return $this->company;
    }

    public function search($params = [])
    {
        $query = InvoiceExpenditureItem::find()->alias('i')->select('i.id, i.name, ff.is_visible')->leftJoin([
            'ff' => 'expense_item_flow_of_funds',
        ], '{{ff}}.[[expense_item_id]] = {{i}}.[[id]] AND {{ff}}.[[company_id]] = :company', [
            ':company' => $this->company->id,
        ])->andWhere([
            'OR',
            ['i.company_id' => null],
            ['i.company_id' => $this->company->id],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
