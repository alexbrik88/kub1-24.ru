<?php

namespace backend\modules\company\modules\import\models;

use common\models\Company;
use common\models\project\Project;
use yii\data\ActiveDataProvider;

/**
 * ProjectSearch
 */
class ProjectSearch extends Project
{
    private $company;

    public function setCompany(Company $company) : void
    {
        $this->company = $company;
    }

    public function getCompany() : Company
    {
        return $this->company;
    }

    public function search($params = [])
    {
        $query = Project::find()->andWhere([
            'company_id' => $this->company->id,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
