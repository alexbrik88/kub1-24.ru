<?php

namespace backend\modules\company\modules\import\models;

use backend\modules\company\modules\import\models\IncomeItemsSearch;
use common\models\Company;
use common\models\document\InvoiceIncomeItem;
use yii\data\ActiveDataProvider;

/**
 * IncomeItemsSearch
 */
class IncomeItemsSearch extends InvoiceIncomeItem
{
    private $company;

    public function setCompany(Company $company) : void
    {
        $this->company = $company;
    }

    public function getCompany() : Company
    {
        return $this->company;
    }

    public function search($params = [])
    {
        $query = IncomeItemsSearch::find()->alias('i')->select('i.id, i.name, ff.is_visible')->leftJoin([
            'ff' => 'income_item_flow_of_funds',
        ], '{{ff}}.[[income_item_id]] = {{i}}.[[id]] AND {{ff}}.[[company_id]] = :company', [
            ':company' => $this->company->id,
        ])->andWhere([
            'OR',
            ['i.company_id' => null],
            ['i.company_id' => $this->company->id],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
