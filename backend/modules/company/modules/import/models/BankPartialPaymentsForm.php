<?php

namespace backend\modules\company\modules\import\models;

use Yii;
use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\form\CashBankFlowsForm;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\company\CheckingAccountant;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use yii\db\Connection;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * EnrichmentForm
 */
class BankPartialPaymentsForm extends \yii\base\Model {

    const START_ROW = 5;

    const ATTRIBUTES = [
        'A' => 'date',
        'B' => 'amount',
        'C' => 'description',
        'D' => 'item',
        'E' => 'project',
        'F' => 'contractor',
        'G' => 'recognition_date',
    ];

    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var CheckingAccountant
     */
    private $account;

    /**
     * @var CashBankFlows|CashBankForeignCurrencyFlows
     */
    private $flowClass;

    /**
     * @var array
     */
    private $uploadedRows;
    private $doubleRows;
    private $skippedRows;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx, xls, csv'],
        ];
    }

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);

        /** @var UploadedFile $file */
        $this->file = UploadedFile::getInstance($this, 'file');

        return $load || $this->file !== null;
    }

    public function setAccount($rs)
    {
        if (strlen($rs)) {
            $this->account = $this->company->getCheckingAccountants()->andWhere(['rs' => $rs])->orderBy(['type' => SORT_ASC])->one();
            if ($this->account) {
                $this->flowClass = $this->account->getCashFlowClass();
            }
        }
    }

    public function save(bool $validate = true) : bool
    {
        $rowCellCount = count(self::ATTRIBUTES);

        if ($validate && !$this->validate()) {
            return false;
        }

        $xls = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->file->tempName);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();

        if ($sheet->getHighestRow() < self::START_ROW) {
            $this->addError('file', 'В файле не найдены операции.');
            return false;
        }

        $rs = trim($sheet->getCellByColumnAndRow(2, 2)->getValue());
        $this->setAccount($rs);

        if (!$this->account) {
            $this->addError('file', 'Не найден расчетный счет компании');
            return false;
        }

        if ($this->account->isForeign) {
            $this->addError('file', 'Разбить операции можно только на рубевых счетах');
        }

        $parentData = [];

        $this->uploadedRows = [];
        $this->skippedRows = [];
        $rowIterator = $sheet->getRowIterator();
        $rowIterator->resetStart(self::START_ROW);
        foreach ($rowIterator as $key => $row) {
            $data = [];
            $cellIterator = $row->getCellIterator();

            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                $attr = self::ATTRIBUTES[$cellKey] ?? null;
                if ($attr === null || count($data) == $rowCellCount) {
                    break;
                }
                $data[$attr] = trim($cell->getCalculatedValue());
            }

            if (empty($data['date'])) {
                break;
            }

            $data['row_id'] = $key;

            $data['amount'] = is_numeric($data['amount']) ? intval(bcmul($data['amount'], 100)) : null;
            if ($data['date'] && is_numeric($data['date'])) {
                $data['date'] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($data['date']);
                if ($data['date'] instanceof \DateTime)
                    $data['date'] = $data['date']->format('d.m.Y');
            }
            if ($data['recognition_date'] && is_numeric($data['recognition_date'])) {
                $data['recognition_date'] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($data['recognition_date']);
                if ($data['recognition_date'] instanceof \DateTime)
                    $data['recognition_date'] = $data['recognition_date']->format('d.m.Y');
            }

            $data['row_uid'] = self::getRowUID($data);
            $data['validation_errors'] = '';

            ////////////////////////////////////////
            $parentData[$data['row_uid']][] = $data;
            ////////////////////////////////////////
        }

        foreach ($parentData as $rowUID => $dataArr) {
            $parentAmount = array_sum(array_column($dataArr, 'amount'));
            $parentDate = DateHelper::format($dataArr[0]['date'], 'Y-m-d', 'd.m.Y');
            $parentDescription = trim($dataArr[0]['description']);
            $parentContractor = self::normalizeContractorName($dataArr[0]['contractor']);
            $parentContractorID = $this->findContractorId($parentContractor);

            $parentFlowQuery = $this->flowClass::find()
                ->andWhere([
                    'company_id' => $this->company->id,
                    'rs' => $this->account->rs
                ]);

            /** @var CashBankFlows $parentFlow */
            $parentFlow = (clone $parentFlowQuery)->andWhere([
                'date' => $parentDate,
                'contractor_id' => $parentContractorID,
                'description' => $parentDescription,
                'amount' => $parentAmount
            ])->one();

            if ($parentFlow) {

                // operation is already split
                if ($parentFlow->children) {
                    foreach ($dataArr as $data) {
                        $this->doubleRows[] = $data;
                    }

                    continue;

                // try to split
                } else {

                    $childFlows = [];
                    $childFlowsValidated = true;
                    foreach ($dataArr as $key => $data) {

                        $childFlow = new CashBankFlowsForm([
                            'scenario' => 'create',
                            'parent_id' => $parentFlow->id,
                            'company_id' => $parentFlow->company_id,
                            'contractor_id' => $parentFlow->contractor_id,
                            'rs' => $parentFlow->rs,
                            'date' => $parentFlow->date,
                            'description' => $parentFlow->description,
                            'bank_name' => $parentFlow->bank_name,
                            'flow_type' => $parentFlow->flow_type,
                            'income_item_id' => null,
                            'expenditure_item_id' => null,
                            'source' => $parentFlow->source,
                            'amount' => round($data['amount'] / 100, 2),
                            'recognitionDateInput' => $data['recognition_date'],
                        ]);

                        if ($data['project']) {
                            if ($projectId = $this->findProjectId($data['project'])) {
                                $childFlow->project_id = (int)$projectId;
                            } else {
                                self::markErrorCell($dataArr[$key]['project']);
                                $childFlow->project_id = -1;
                            }
                        }

                        if ($parentFlow->flow_type == CashBankFlows::FLOW_TYPE_INCOME) {
                            if ($itemId = $this->findIncomeItemId($data['item'])) {
                                $childFlow->income_item_id = (int)$itemId;
                            } else {
                                self::markErrorCell($dataArr[$key]['item']);
                                $childFlow->income_item_id = -1;
                            }
                        } else {
                            if ($itemId = $this->findExpenditureItemId($data['item'])) {
                                $childFlow->expenditure_item_id = (int)$itemId;
                            } else {
                                self::markErrorCell($dataArr[$key]['item']);
                                $childFlow->expenditure_item_id = -1;
                            }
                        }

                        if (!$childFlow->validate()) {
                            $dataArr[$key]['validation_errors'] = self::formatValidationErrors($childFlow->getErrors());
                            $childFlowsValidated = false;
                        } else {
                            $childFlows[] = $childFlow;
                        }
                    }

                    if ($childFlowsValidated) {
                        $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($parentFlow, $childFlows) {
                            foreach ($childFlows as $flow) {
                                if (!$flow->save(false)) {
                                    $db->transaction->rollBack();
                                    return false;
                                }
                            }

                            $parentFlow->checkTinChildren();

                            return true;
                        });
                    } else {
                        $isSaved = false;
                    }

                    foreach ($dataArr as $data) {
                        if ($isSaved) {
                            $this->uploadedRows[] = $data;
                        } else {
                            $this->skippedRows[] = $data;
                        }
                    }

                    continue;
                }

            } else {

                $hasParentFlowWithout = [
                    'date' => (clone $parentFlowQuery)->andWhere(['contractor_id' => $parentContractorID, 'description' => $parentDescription, 'amount' => $parentAmount])->exists(),
                    'description' => (clone $parentFlowQuery)->andWhere(['date' => $parentDate, 'contractor_id' => $parentContractorID, 'amount' => $parentAmount])->exists(),
                    'amount' => (clone $parentFlowQuery)->andWhere(['date' => $parentDate, 'contractor_id' => $parentContractorID, 'description' => $parentDescription])->exists(),
                ];

                foreach ($dataArr as $data) {

                    $data['validation_errors'] = 'Платёж не найден';

                    if (!$parentContractorID) {
                        self::markErrorCell($data['contractor']);
                    } elseif ($hasParentFlowWithout['date']) {
                        self::markErrorCell($data['date']);
                    } elseif ($hasParentFlowWithout['description']) {
                        self::markErrorCell($data['description']);
                    } elseif ($hasParentFlowWithout['amount']) {
                        self::markErrorCell($data['amount']);
                    } else {
                        //
                    }

                    $this->skippedRows[] = $data;
                }
            }
        }

        return true;
    }

    private static function formatValidationErrors($errors)
    {
        $ret = [];
        foreach ($errors as $errorKey => $error) {
            $ret[] = $errorKey.': '.implode(', ', $error);
        }

        return implode('<br>', $ret);
    }

    private static function markErrorCell(&$cell)
    {
        $cell = Html::tag('span', $cell, ['style' => 'color:red']);
    }

    private static function getRowUID($data)
    {
        $date = $data['date'];
        $contractor = $data['contractor'];
        $description = $data['description'];

        return base64_encode($date.'|||'.$contractor.'|||'.$description);
    }

    private static function normalizeContractorName($fullName)
    {
        $nameArr = explode(' ', str_replace('  ', ' ', trim($fullName)));
        $companyType = array_shift($nameArr);
        switch ($companyType) {
            case 'ИП':
                $typeId = CompanyType::TYPE_IP;
                break;
            case 'ЗАО':
                $typeId = CompanyType::TYPE_ZAO;
                break;
            case 'ПАО':
                $typeId = CompanyType::TYPE_PAO;
                break;
            case 'ОАО':
                $typeId = CompanyType::TYPE_OAO;
                break;
            case 'ООО':
                $typeId = CompanyType::TYPE_OOO;
                break;
            default:
                // not recognized
                $nameArr = array_merge([$companyType], $nameArr);
                break;
        }

        $name = implode(' ', $nameArr);

        return $name;
    }

    private function findContractorId($contractorName)
    {
        // todo: add cache
        return Contractor::find()->where([
            'company_id' => $this->company->id
        ])->andWhere(['or',
            ['name' => '"'.$contractorName.'"'],
            ['name' => $contractorName]
        ])->select('id')->scalar();
    }
    
    private function findIncomeItemId($itemName)
    {
        // todo: add cache
        return InvoiceIncomeItem::find()
            ->where(['name' => $itemName])
            ->andWhere(['or',
                ['company_id' => null],
                ['company_id' => $this->company->id],
            ])->select('id')->scalar();
    }

    private function findExpenditureItemId($itemName)
    {
        // todo: add cache
        return InvoiceExpenditureItem::find()
            ->where(['name' => $itemName])
            ->andWhere(['or',
                ['company_id' => null],
                ['company_id' => $this->company->id],
            ])->select('id')->scalar();
    }

    private function findProjectId($projectName)
    {
        // todo: add cache
        return Project::find()->andWhere([
            'company_id' => $this->company->id,
            'name' => mb_strtolower($projectName),
        ])->select('id')->scalar();
    }

    /**
     * @return array
     */
    public function getUploadedRows()
    {
        return $this->uploadedRows ?: [];
    }

    /**
     * @return array
     */
    public function getSkippedRows()
    {
        return $this->skippedRows ?: [];
    }

    /**
     * @return array
     */
    public function getDoubleRows()
    {
        return $this->doubleRows ?: [];
    }
}