<?php

namespace backend\modules\company\modules\import\models;

use common\models\Company;
use common\models\IncomeItemFlowOfFunds;
use common\models\ExpenseItemFlowOfFunds;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\analytics\models\AbstractFinance;
use yii\web\UploadedFile;

/**
 * ItemsImportForm
 */
class ItemsImportForm extends \yii\base\Model
{
    const TYPE_INCOME = 0;
    const TYPE_EXPENSE = 1;

    const TYPE_ITEM_MAP = [
        self::TYPE_INCOME => InvoiceIncomeItem::class,
        self::TYPE_EXPENSE => InvoiceExpenditureItem::class,
    ];

    const TYPE_ITEM_FLOW_OF_FUNDS_MAP = [
        self::TYPE_INCOME => IncomeItemFlowOfFunds::class,
        self::TYPE_EXPENSE => ExpenseItemFlowOfFunds::class,
    ];

    private Company $company;
    private int $type;
    private bool $success = false;
    private int $itemsCount = 0;
    private int $successCount = 0;
    private array $invalidItems = [];

    public $file;
    public $activity;

    public static $activityList = [
        AbstractFinance::OPERATING_ACTIVITIES_BLOCK => 'Операционная деятельность',
        AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => 'Финансовая деятельность',
        AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => 'Инвестиционная деятельность',
    ];

    public function __construct(Company $company, int $type, array $config = [])
    {
        $this->company = $company;
        $this->type = $type;

        parent::__construct($config);
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getResult()
    {
        return [
            'success' => $this->success,
            'itemsCount' => $this->itemsCount,
            'successCount' => $this->successCount,
            'invalidItems' => $this->invalidItems,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['activity', 'required'],
            ['activity', 'in', 'range' => array_keys(self::$activityList)],
            [['file'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
            'activity' => 'Вид деятельности',
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);

        $this->file = UploadedFile::getInstance($this, 'file');

        return $load || $this->file !== null;
    }

    public function save(bool $validate = true) : bool
    {
        $this->success = false;
        $this->itemsCount = 0;
        $this->successCount = 0;
        $this->invalidItems = [];
        if ($validate && !$this->validate()) {
            return false;
        }
        $xls = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->file->tempName);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;

            $cellIterator = $row->getCellIterator();
            $name = null;

            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                if ($cellKey == 'A') {
                    $name = trim($cell->getValue());
                } else {
                    break;
                }
            }
            if (empty($name)) {
                break;
            }
            $this->itemsCount++;
            if ($this->createItem($name)) {
                $this->successCount++;
            } else {
                $this->invalidItems[] = $name;
            }
        }

        if ($this->itemsCount === 0) {
            $this->addError('file', 'В файле не найдены статьи расхода.');

            return false;
        }

        $this->success = $this->itemsCount > 0 && $this->itemsCount > count($this->invalidItems);

        return true;
    }

    private function createItem(string $name) : bool
    {
        $itemClassName = self::TYPE_ITEM_MAP[$this->type];
        /** @var InvoiceIncomeItem|InvoiceExpenditureItem $model1 */
        $model1 = new $itemClassName();
        $model1->company_id = $this->company->id;
        $model1->name = mb_substr($name, 0, $itemClassName::NAME_MAX_LENGTH);
        $model1->parent_id = null;
        if ($model1->save()) {
            $itemFlowOfFundsClassName = self::TYPE_ITEM_FLOW_OF_FUNDS_MAP[$this->type];
            /** @var IncomeItemFlowOfFunds|ExpenseItemFlowOfFunds $model2 */
            $model2 = new $itemFlowOfFundsClassName();
            $model2->company_id = $this->company->id;
            $model2->flow_of_funds_block = $this->activity;
            $model2->is_visible = true;
            $model2->is_prepayment = true;
            if ($this->type === self::TYPE_INCOME) {
                $model2->income_item_id = $model1->id;
            } else {
                $model2->expense_item_id = $model1->id;
            }

            $model2->save();

            return true;
        }

        return false;
    }
}
