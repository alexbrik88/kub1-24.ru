<?php

namespace backend\modules\company\modules\import\models;

use common\models\Company;
use common\models\EmployeeCompany;
use common\models\project\Project;
use yii\web\UploadedFile;

/**
 * ProjectImportForm
 */
class ProjectImportForm extends \yii\base\Model
{
    const TYPE_INCOME = 0;
    const TYPE_EXPENSE = 1;

    const TYPE_ITEM_MAP = [
        self::TYPE_INCOME => InvoiceIncomeItem::class,
        self::TYPE_EXPENSE => InvoiceExpenditureItem::class,
    ];

    const TYPE_ITEM_FLOW_OF_FUNDS_MAP = [
        self::TYPE_INCOME => IncomeItemFlowOfFunds::class,
        self::TYPE_EXPENSE => ExpenseItemFlowOfFunds::class,
    ];

    private Company $company;
    private EmployeeCompany $employeeCompany;
    private bool $success = false;
    private int $itemsCount = 0;
    private int $successCount = 0;
    private array $invalidItems = [];

    public $file;

    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;
        $this->employeeCompany = $company->chiefEmployeeCompany;

        parent::__construct($config);
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getResult()
    {
        return [
            'success' => $this->success,
            'itemsCount' => $this->itemsCount,
            'successCount' => $this->successCount,
            'invalidItems' => $this->invalidItems,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);

        $this->file = UploadedFile::getInstance($this, 'file');

        return $load || $this->file !== null;
    }

    public function save(bool $validate = true) : bool
    {
        $this->success = false;
        $this->itemsCount = 0;
        $this->successCount = 0;
        $this->invalidItems = [];

        if ($validate && !$this->validate()) {
            return false;
        }

        $xls = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->file->tempName);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;

            $cellIterator = $row->getCellIterator();
            $name = null;

            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                if ($cellKey == 'A') {
                    $name = trim($cell->getValue());
                } else {
                    break;
                }
            }
            if (empty($name)) {
                break;
            }
            $this->itemsCount++;
            if ($this->createItem($name)) {
                $this->successCount++;
            } else {
                $this->invalidItems[] = $name;
            }
        }

        if ($this->itemsCount === 0) {
            $this->addError('file', 'В файле не найдены проекты.');

            return false;
        }

        $this->success = $this->itemsCount > 0 && $this->itemsCount > count($this->invalidItems);

        return true;
    }

    private function createItem(string $name) : bool
    {
        $query = Project::find()->andWhere([
            'company_id' => $this->company->id,
            'name' => $name,
        ]);

        if ($query->exists()) {
            return false;
        }

        $startDate = new \DateTime();
        $endDate = new \DateTime('last day of december this year');
        $model = new Project();
        $model->company_id = $this->company->id;
        $model->responsible = $this->employeeCompany->employee_id;
        $model->number = Project::getNextProjectNumber($this->company, null, $startDate->format('Y-m-d'));
        $model->addition_number = '';
        $model->name = mb_substr($name, 0, 255);
        $model->status = Project::STATUS_INPROGRESS;
        $model->start_date = $startDate->format('Y-m-d');
        if ($model->save(false)) {
            return true;
        }

        return false;
    }
}
