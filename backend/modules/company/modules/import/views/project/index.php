<?php

use common\models\Company;
use common\models\project\Project;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model backend\modules\company\modules\import\models\ProjectImportForm */
/* @var $searchModel backend\modules\company\modules\import\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['company'] = $company;
$this->title = "Проекты";
?>


<div class="import-default-index">
    <div class="d-flex justify-content-between">
        <h1><?= $this->title ?></h1>
        <div class="ml-auto">
            <?= Html::button('Импорт', [
                'class' => 'btn yellow',
                'data-toggle' => 'modal',
                'data-target' => '#import-form-modal',
            ]) ?>
        </div>
    </div>

        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

            'layout' => "{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table table-bordered table-hover dataTable',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],
            'columns' => [
                'id',
                'name',
                [
                    'attribute' => 'number',
                    'label' => '№ проекта',
                    'value' => function ($model) {
                        return  $model['number'] . $model['addition_number'];
                    },
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return Project::$statuses[$model->status] ?? '---';
                    },
                ],
                'start_date:date',
                'end_date:date',
            ],
        ]); ?>
</div>

<?php Modal::begin([
    'id' => 'import-form-modal',
    'header' => '<h3>Загрузить '.$this->title.' из excel</h3>',
    'toggleButton' => false,
]) ?>

    <?= $this->render('import', ['model' => $model]) ?>

<?php Modal::end() ?>
