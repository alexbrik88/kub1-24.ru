<?php

use yii\data\ArrayDataProvider;
use common\components\TextHelper;
use common\components\grid\GridView;

/** @var ArrayDataProvider $dataProvider */

echo  GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}",
    'tableOptions' => [
        'class' => 'table table-bordered table-hover dataTable',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'columns' => [
        [
            'attribute' => 'row_id',
            'label' => '',
            'headerOptions' => [
                'width' => '1%'
            ]
        ],
        [
            'attribute' => 'date',
            'label' => 'Дата',
            'format' => 'raw',
        ],
        [
            'attribute' => 'amount',
            'label' => 'Сумма',
            'format' => 'raw',
            'contentOptions' => [
                'class' => 'text-nowrap',
            ],
            'value' => function($data) {
                $amount = trim(strip_tags($data['amount']));
                $formattedAmount = TextHelper::invoiceMoneyFormat($amount, 2);
                return str_replace($amount, $formattedAmount, $data['amount']);
            }
        ],
        [
            'attribute' => 'description',
            'label' => 'Назначение платежа',
            'format' => 'raw',
        ],
        [
            'attribute' => 'item',
            'label' => 'Статья',
            'format' => 'raw'
        ],
        [
            'attribute' => 'project',
            'label' => 'Проект',
            'format' => 'raw'
        ],
        [
            'attribute' => 'contractor',
            'label' => 'Контрагент',
            'format' => 'raw'
        ],
        [
            'attribute' => 'recognition_date',
            'label' => 'Дата признания',
            'format' => 'date',
        ],
        [
            'attribute' => 'validation_errors',
            'label' => 'Ошибки',
            'format' => 'raw'
        ],
    ],
]); ?>
