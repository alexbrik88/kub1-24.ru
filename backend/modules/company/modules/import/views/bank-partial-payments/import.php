<?php

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$config = [
    'options' => [
        'class' => 'd-flex my-5',
    ],
    'labelOptions' => [
        'class' => 'control-label bold-text w-130 mr-3',
    ],
    'wrapperOptions' => [
        'class' => 'flex-grow-1',
    ],
    'hintOptions' => [
        'tag' => 'small',
        'class' => 'text-muted',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
?>

<style type="text/css">
    #import-form .w-130 {
        width: 130px;
    }
    #enrichmentform-file-styler {
        width: 100%;
        display: flex !important;
        flex-direction: row-reverse;
        justify-content: flex-end;
    }
</style>

<?php $form = ActiveForm::begin([
    'id' => 'import-form',
]) ?>

    <?= $form->field($model, 'file', $config)->fileInput([
        'class' => 'js_filetype',
        'accept' => '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel',
    ])->label('Файл') ?>

    <div class="action-buttons d-flex justify-content-between mt-5">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue ladda-button',
        ]) ?>
        <?= Html::button('Отмена', [
            'class' => 'btn darkblue',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

<?php ActiveForm::end() ?>
