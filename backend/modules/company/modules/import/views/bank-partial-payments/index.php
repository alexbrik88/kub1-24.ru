<?php

use common\models\Company;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\components\grid\GridView;
use backend\modules\company\modules\import\models\BankPartialPaymentsForm;

/* @var $this yii\web\View */
/* @var $company Company */
/* @var $model BankPartialPaymentsForm */

$this->params['company'] = $company;
$this->title = '"Разбитые" операции по банку';

$uploadedRowsCount = count($model->getUploadedRows());
$skippedRowsCount = count($model->getSkippedRows());
$doubleRowsCount = count($model->getDoubleRows());

$result = $uploadedRowsCount || $skippedRowsCount || $doubleRowsCount;

$skippedDataProvider = new ArrayDataProvider([
    'allModels' => $model->getSkippedRows(),
    'pagination' => false,
]);
$uploadedDataProvider = new ArrayDataProvider([
    'allModels' => $model->getUploadedRows(),
    'pagination' => false,
]);
?>

<div class="import-default-index">
    <div class="d-flex justify-content-between">
        <h1><?= $this->title ?></h1>
        <div class="ml-auto">
            <?= Html::button('Импорт', [
                'class' => 'btn yellow',
                'data-toggle' => 'modal',
                'data-target' => '#import-form-modal',
            ]) ?>
        </div>
    </div>
    <?php if ($result) : ?>
        <div>
            <strong>Загружено операций:</strong>
            <?= $uploadedRowsCount ?>
        </div>
        <div>
            <strong>Дубли загрузки:</strong>
            <?= $doubleRowsCount ?>
        </div>
        <div>
            <strong>Не загружено операций:</strong>
            <?= $skippedRowsCount ?>
        </div>

        <h2>"Разбитые" операции</h2>

        <?= $this->render('_table', ['dataProvider' => $uploadedDataProvider]) ?>

        <h2 class="pt-5">Операции в Эксель, по которым нет совпадений в КУБе</h2>

        <?= $this->render('_table', ['dataProvider' => $skippedDataProvider]) ?>

    <?php else : ?>
        <div class="mt-5">
            <?= Html::a('Загрузите файл', '#import-form-modal', [
                'data-toggle' => 'modal',
            ]) ?>
        </div>
        <div class="mt-5">
            <?= Html::a('Скачать шаблон таблицы', 'download-template', [
                'download' => 1
            ]) ?>
        </div>
    <?php endif ?>
</div>

<?php Modal::begin([
    'id' => 'import-form-modal',
    'header' => '<h3>"Разбитые" операции по банку из excel</h3>',
    'toggleButton' => false,
]) ?>

    <?= $this->render('import', ['model' => $model]) ?>

<?php Modal::end() ?>

<?php
if ($model->hasErrors()) {
    $this->registerJs('$("#import-form-modal").modal("show");');
}
?>
