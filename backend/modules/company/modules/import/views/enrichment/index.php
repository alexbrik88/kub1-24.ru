<?php

use common\components\TextHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\project\Project;
use frontend\modules\analytics\models\PlanCashFlows;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model backend\modules\company\modules\import\models\ProjectImportForm */
/* @var $searchModel backend\modules\company\modules\import\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['company'] = $company;
$this->title = '"Обогащение" выписки';
?>


<div class="import-default-index">
    <div class="d-flex justify-content-between">
        <h1><?= $this->title ?></h1>
        <div class="ml-auto">
            <?= Html::button('Импорт', [
                'class' => 'btn yellow',
                'data-toggle' => 'modal',
                'data-target' => '#import-form-modal',
            ]) ?>
        </div>
    </div>
    <?php if ($result !== null) : ?>
        <?php
        $updatedAttr = $result['updatedAttr'];
        ?>
        <div>
            <strong>Количество операций в Эксель:</strong>
            <?= $result['itemsCount'] ?>
        </div>
        <div>
            <strong>Количество совпавших операций:</strong>
            <?= $result['existsCount'] ?>
        </div>
        <div>
            <strong>Количество "обогащенных" операций:</strong>
            <?= $result['successCount'] ?>
        </div>
        <h2>"Обогащенные" операции</h2>
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $result['dataProvider1'],
            'layout' => "{items}",
            'tableOptions' => [
                'class' => 'table table-bordered table-hover dataTable',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'columns' => [
                'date:date',
                [
                    'attribute' => 'amountIncome',
                    'label' => 'Приход',
                    'value' => function ($data) {
                        if ($data->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
                            return TextHelper::invoiceMoneyFormat($data->amount, 2);
                        }

                        return '-';
                    },
                ],
                [
                    'attribute' => 'amountExpense',
                    'label' => 'Расход',
                    'value' => function ($data) {
                        if ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                            return TextHelper::invoiceMoneyFormat($data->amount, 2);
                        }

                        return '-';
                    },
                ],
                [
                    'attribute' => 'contractor_ids',
                    'label' => 'Контрагент',
                    'value' => function ($data) {
                        if ($data->is_internal_transfer)
                            return '';

                        if (is_numeric($data->contractor_id) && ($contractor = Contractor::findOne($data->contractor_id)) !== null) {
                            return $contractor->nameWithType;
                        } else {
                            $model = CashBankFlows::findOne($data->id);

                            if ($model && $model->cashContractor) {
                                return $model->cashContractor->text;
                            }
                        }

                        return '---';
                    },
                ],
                [
                    'attribute' => 'description',
                    'label' => 'Назначение',
                ],
                [
                    'attribute' => 'billPaying',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::encode($data->credit_id ? $data->getCreditPaying(false) : $data->getBillPaying(false));
                    }
                ],
                [
                    'attribute' => 'reason_ids',
                    'label' => 'Статья',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->flow_type == CashBankFlows::FLOW_TYPE_INCOME) {
                            if ($item = \common\models\document\InvoiceIncomeItem::findOne($data->income_item_id)) {
                                return $item->name;
                            }
                        } else {
                            if ($item = \common\models\document\InvoiceExpenditureItem::findOne($data->expenditure_item_id)) {
                                return $item->name;
                            }
                        }

                        return '-';
                    },
                ],
                [
                    'label' => 'Изменены',
                    'value' => function($data) use ($updatedAttr) {
                        $items = [];
                        foreach (($updatedAttr[$data->id] ?? []) as $key => $value) {
                            $items[] = $data->getAttributeLabel($key);
                        }

                        return implode(', ', $items);
                    }
                ]
            ],
        ]); ?>
        <h2>Операции в КУБе, по которым нет совпадений в Эксель</h2>
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $result['dataProvider2'],
            'layout' => "{items}",
            'tableOptions' => [
                'class' => 'table table-bordered table-hover dataTable',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'columns' => [
                [
                    'attribute' => 'date',
                    'format' => 'date',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'amountIncome',
                    'label' => 'Приход',
                    'value' => function ($data) {
                        if ($data->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
                            return TextHelper::invoiceMoneyFormat($data->amount, 2);
                        }

                        return '-';
                    },
                ],
                [
                    'attribute' => 'amountExpense',
                    'label' => 'Расход',
                    'value' => function ($data) {
                        if ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                            return TextHelper::invoiceMoneyFormat($data->amount, 2);
                        }

                        return '-';
                    },
                ],
                [
                    'attribute' => 'contractor_ids',
                    'label' => 'Контрагент',
                    'value' => function ($data) {
                        if ($data->is_internal_transfer)
                            return '';

                        if (is_numeric($data->contractor_id) && ($contractor = Contractor::findOne($data->contractor_id)) !== null) {
                            return $contractor->nameWithType;
                        } else {
                            $model = CashBankFlows::findOne($data->id);

                            if ($model && $model->cashContractor) {
                                return $model->cashContractor->text;
                            }
                        }

                        return '---';
                    },
                ],
                [
                    'attribute' => 'description',
                    'label' => 'Назначение',
                ],
                [
                    'attribute' => 'billPaying',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::encode($data->credit_id ? $data->getCreditPaying(false) : $data->getBillPaying(false));
                    }
                ],
                [
                    'attribute' => 'reason_ids',
                    'label' => 'Статья',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->flow_type == CashBankFlows::FLOW_TYPE_INCOME) {
                            if ($item = \common\models\document\InvoiceIncomeItem::findOne($data->income_item_id)) {
                                return $item->name;
                            }
                        } else {
                            if ($item = \common\models\document\InvoiceExpenditureItem::findOne($data->expenditure_item_id)) {
                                return $item->name;
                            }
                        }

                        return '-';
                    },
                ],
            ],
        ]); ?>
        <h2 class="pt-5">Операции в Эксель, по которым нет совпадений в КУБе</h2>
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $result['dataProvider3'],
            'layout' => "{items}",
            'tableOptions' => [
                'class' => 'table table-bordered table-hover dataTable',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'columns' => [
                'row_id',
                [
                    'attribute' => 'date',
                    'label' => 'Дата',
                    'format' => 'date',
                ],
                [
                    'attribute' => 'amount',
                    'label' => 'Сумма',
                    'format' => 'money',
                    'contentOptions' => [
                        'class' => 'text-nowrap',
                    ],
                ],
                [
                    'attribute' => 'description',
                    'label' => 'Назначение платежа',
                ],
                [
                    'attribute' => 'item',
                    'label' => 'Статья',
                ],
                [
                    'attribute' => 'project',
                    'label' => 'Проект',
                ],
                [
                    'attribute' => 'contractor',
                    'label' => 'Контрагент',
                ],
                [
                    'attribute' => 'recognition_date',
                    'label' => 'Дата признания',
                    'format' => 'date',
                ],
                [
                    'attribute' => 'payment_order_number',
                    'label' => '№  п.п.',
                ],
            ],
        ]); ?>
    <?php else : ?>
        <div class="mt-5">
            <?= Html::a('Загрузите файл', '#import-form-modal', [
                'data-toggle' => 'modal',
            ]) ?>
        </div>
    <?php endif ?>
</div>

<?php Modal::begin([
    'id' => 'import-form-modal',
    'header' => '<h3>"Обогащение" выписки из excel</h3>',
    'toggleButton' => false,
]) ?>

    <?= $this->render('import', ['model' => $model]) ?>

<?php Modal::end() ?>

<?php
if ($model->hasErrors()) {
    $this->registerJs('$("#import-form-modal").modal("show");');
}
?>
