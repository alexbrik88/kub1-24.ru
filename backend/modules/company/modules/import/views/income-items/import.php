<?php

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\company\modules\import\models\ItemsImportForm */

$config = [
    'options' => [
        'class' => 'd-flex my-5',
    ],
    'labelOptions' => [
        'class' => 'control-label bold-text w-130 mr-3',
    ],
    'wrapperOptions' => [
        'class' => 'flex-grow-1',
    ],
    'hintOptions' => [
        'tag' => 'small',
        'class' => 'text-muted',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
?>

<?php Pjax::begin([
    'id' => 'import-form-pjax',
    'enablePushState' => false,
]) ?>

<style type="text/css">
    #import-form .w-130 {
        width: 130px;
    }
    #itemsimportform-file-styler {
        width: 100%;
        display: flex !important;
        flex-direction: row-reverse;
        justify-content: flex-end;
    }
</style>

<?php $form = ActiveForm::begin([
    'id' => 'import-form',
    'action' => ['import', 'id' => $model->company->id],
    'options' => [
        'enctype' => 'multipart/form-data',
        'data-pjax' => true,
    ],
]) ?>

    <?= $form->field($model, 'file', $config)->fileInput([
        'class' => 'js_filetype',
        'accept' => '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel',
    ])->label('Файл') ?>

    <?= $form->field($model, 'activity', $config)->widget(Select2::class, [
        'hideSearch' => true,
        'data' => $model::$activityList,
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]); ?>

    <div class="action-buttons d-flex justify-content-between mt-5">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue ladda-button',
        ]) ?>
        <?= Html::button('Отмена', [
            'class' => 'btn darkblue',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

<?php ActiveForm::end() ?>

<script type="text/javascript">
    $('#import-form .js_filetype').styler({
        'fileBrowse': '<span class="glyphicon glyphicon-plus-sign"></span> Выбрать файл'
    });
</script>

<?php Pjax::end() ?>
