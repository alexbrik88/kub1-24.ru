<?php

namespace backend\modules\company\modules\import\controllers;

use Yii;
use backend\modules\company\modules\import\components\ImportBaseController;
use backend\modules\company\modules\import\models\ProjectSearch;
use backend\modules\company\modules\import\models\ProjectImportForm;
use common\models\Company;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Project controller for the `import` module
 */
class ProjectController extends ImportBaseController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $company = $this->getCompany();
        $model = new ProjectImportForm($company);

        $searchModel = new ProjectSearch(['company' => $company]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'company' => $company,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionImport()
    {
        $company = $this->getCompany();

        $model = new ProjectImportForm($company);

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $result = $model->getResult();
                $text = [];
                $text[] = sprintf('%s %s', Html::tag('strong', 'Загружено проектов:'), $result['successCount']);
                if ($result['invalidItems']) {
                    $text[] = Html::tag('strong', 'Данные проекты не загрузились, т.к. такие названия уже имеются у клиента:');
                    foreach ($result['invalidItems'] as $key => $value) {
                        $text[] = sprintf('%s. %s', ++$key, $value);
                    }
                }
                $text = implode(Html::tag('br'), $text);
                Yii::$app->session->setFlash($result['success'] ? 'success' : 'error', $text);

                return $this->redirect(['index', 'id' => $company->id]);
            }
        }

        return $this->renderAjax('import', ['model' => $model]);
    }
}
