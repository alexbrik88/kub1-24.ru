<?php

namespace backend\modules\company\modules\import\controllers;

use frontend\components\XlsHelper;
use himiklab\thumbnail\FileNotFoundException;
use Yii;
use backend\modules\company\modules\import\models\BankPartialPaymentsForm;
use backend\modules\company\modules\import\components\ImportBaseController;
use yii\web\NotFoundHttpException;

/**
 * BankPartialPayments controller for the `import` module
 */
class BankPartialPaymentsController extends ImportBaseController
{
    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        ini_set('max_execution_time', '90');
        ini_set('memory_limit', '1024M');

        $company = $this->getCompany();
        $model = new BankPartialPaymentsForm($company);

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }

        return $this->render('index', [
            'company' => $company,
            'model' => $model,
        ]);
    }

    /**
     * @param $type
     * @throws FileNotFoundException
     * @throws NotFoundHttpException
     */
    public function actionDownloadTemplate()
    {
        $fileName = 'split_bank_operations.xlsx';
        $path = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'xls' . DIRECTORY_SEPARATOR . 'backend-template' . DIRECTORY_SEPARATOR . $fileName;
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path, $fileName, ['mimeType' => mime_content_type($path)])->send();
        } else {
            throw new FileNotFoundException('File not found');
        }
    }
}
