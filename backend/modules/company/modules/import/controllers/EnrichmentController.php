<?php

namespace backend\modules\company\modules\import\controllers;

use Yii;
use backend\modules\company\modules\import\components\ImportBaseController;
use backend\modules\company\modules\import\models\EnrichmentForm;
use common\models\Company;
use yii\helpers\Html;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Enrichment controller for the `import` module
 */
class EnrichmentController extends ImportBaseController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $company = $this->getCompany();
        $model = new EnrichmentForm($company);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('index', [
                'company' => $company,
                'model' => $model,
                'result' => $model->getResult(),
            ]);
        }

        return $this->render('index', [
            'company' => $company,
            'model' => $model,
            'result' => null,
        ]);
    }
}
