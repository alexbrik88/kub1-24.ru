<?php

use common\components\TextHelper;
use yii\web\View;

/* @var $this View
 * @var $color string
 * @var $status integer
 * @var $value string
 * @var $isMoney boolean
 */

?>

<div class="dashboard-stat <?= $color; ?>" style="position: relative;">
    <div class="visual">
        <i class="fa fa-comments"></i>
    </div>
    <div class="details">
        <div class="number fontSizeSmall">
            <span class="details-sum"
                  <?= 'data-value="'.$value.'"' ?>><?= isset($isMoney) && $isMoney
                    ? TextHelper::invoiceMoneyFormat($value * 100, 2)
                    : intval($value); ?>
            </span>
        </div>
        <div class="desc mt-4"><?= isset($description) ? $description : '&nbsp;'; ?></div>
    </div>
    <div class="more">
        &nbsp;
    </div>
</div>