<?php

use backend\modules\company\models\PartnerSearch;
use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\Company;
use common\models\service\Subscribe;
use frontend\components\StatisticPeriod;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableViewWidget;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/** @var PartnerSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */
/** @var integer $totalCount */

$this->title = "Партнеры";

$this->registerCss(<<<CSS
    .dashboard-block {
        width: 19%;
        float: left;

        margin: 0 0.5%;
    }

    .dashboard-block:first-child {
        margin-left: 0;
    }

    .dashboard-block:last-child {
        margin-right: 0;
    }

    .dashboard-stat .more {
        background: none!important;
    }
CSS
);

$dateRange = StatisticPeriod::getSessionPeriod();

?>

<div class="portlet">
    <div class="portlet-title">
        <div class="title bold-text col-6 pull-left">
            <h3><?= $this->title; ?><?= ' '; ?><?= $totalCount ? '(' . $totalCount . ')' : ''; ?></h3>
        </div>
        <div class="col-6 pull-right">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row mt-5 pt-5 pb-5 pl-4 pr-2" id="widgets">
            <div class="dashboard-block">
                <?= $this->render('_partials/dashboard_block', [
                    'color' => '_yellow',
                    'description' => 'Новых партнеров',
                    'value' => $searchModel
                        ->getBaseQuery()
                        ->andWhere(['IS NOT', Company::tableName() . '.affiliate_link', new Expression('NULL')])
                        ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.affiliate_link_created_at))', $dateRange['from'], $dateRange['to']])
                        ->count(),
                ]); ?>
            </div>
            <div class="dashboard-block">
                <?= $this->render('_partials/dashboard_block', [
                    'color' => '_green',
                    'description' => 'Регистраций',
                    'value' => $searchModel
                        ->getBaseQuery()
                        ->andWhere(['and',
                            ['IS NOT', Company::tableName() . '.invited_by_company_id', new Expression('NULL')],
                            ['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']]
                        ])
                        ->count(),
                ]); ?>
            </div>
            <div class="dashboard-block">
                <?= $this->render('_partials/dashboard_block', [
                    'color' => '_green',
                    'description' => 'Счетов',
                    'value' => $searchModel
                        ->getBaseQuery()
                        ->andWhere(['and',
                            ['IS NOT', Company::tableName() . '.affiliate_link', new Expression('NULL')],
                            ['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']]
                        ])
                        ->groupBy(Company::tableName() . '.id')
                        ->sum('paidCount'),
                ]); ?>
            </div>
            <div class="dashboard-block">
                <?= $this->render('_partials/dashboard_block', [
                    'color' => '_green',
                    'description' => 'Оплат',
                    'value' => $searchModel
                        ->getBaseQuery()
                        ->andWhere(['and',
                            ['IS NOT', Company::tableName() . '.affiliate_link', new Expression('NULL')],
                            ['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']]
                        ])
                        ->groupBy(Company::tableName() . '.id')
                        ->sum('paidInvoicesCount'),
                ]); ?>
            </div>
            <div class="dashboard-block">
                <?= $this->render('_partials/dashboard_block', [
                    'color' => '_red',
                    'description' => 'Сумма оплат',
                    'isMoney' => true,
                    'value' => $searchModel
                        ->getBaseQuery()
                        ->andWhere(['and',
                            ['IS NOT', Company::tableName() . '.affiliate_link', new Expression('NULL')],
                            ['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']]
                        ])
                        ->groupBy(Company::tableName() . '.id')
                        ->sum('paidInvoicesSum'),
                ]); ?>
            </div>
        </div>
    </div>
    <div class="portlet box darkblue  mt-5 pt-5">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
        </div>
        <div class="portlet-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,

                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'options' => [
                        'style' => 'overflow-x: auto;',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination list-clr pull-right',
                        ],
                    ],

                    'columns' => [
                        [
                            'attribute' => 'registrationDate',
                            'label' => 'Дата регистрации (компания)',
                            'value' => function (Company $model) {
                                return date(DateHelper::FORMAT_USER_DATE, $model->registrationDate);
                            }
                        ],
                        [
                            'attribute' => 'partnerRegistrationDate',
                            'label' => 'Дата регистрации (партнер)',
                            'value' => function (Company $model) {
                                return date(DateHelper::FORMAT_USER_DATE, $model->partnerRegistrationDate);
                            }
                        ],
                        [
                            'attribute' => 'partnerName',
                            'label' => 'Название',
                            'headerOptions' => [
                                'width' => '120px',
                            ],
                            'value' => function (Company $model) {
                                return $model->partnerName;
                            }
                        ],
                        [
                            'attribute' => 'tariff',
                            'label' => 'Тип подписки',
                            'format' => 'html',
                            'value' => function (Company $model) {
                                /** @var Subscribe[] $activeTariffs */
                                $activeTariffs = $model->activeSubscribes;
                                $content = "";

                                /** @var Subscribe $activeTariff */
                                foreach ($activeTariffs as $activeTariff) {
                                    $content .= Html::encode($activeTariff->tariffGroup->name);
                                    $content .= ' ';
                                    $content .= Html::encode($activeTariff->getTariffName());
                                    $content .= '; ';
                                    $content .= '<br />';
                                }

                                return $content ?: 'Нет';
                            }
                        ],
                        [
                            'attribute' => 'allProduct',
                            'label' => 'Все',
                            'value' => function (Company $model) {
                                return ($model->analysisCount
                                    + $model->b2bCount
                                    + $model->bookkeepingCount
                                    + $model->priceListsCount
                                    + $model->invoiceCount) ?: '-';
                            }
                        ],
                        [
                            'attribute' => 'analysisCount',
                            'label' => 'ФинДиректор',
                            'value' => function (Company $model) {
                                return $model->analysisCount ?: '-';
                            }
                        ],
                        [
                            'attribute' => 'b2bCount',
                            'label' => 'Бизнес платежи',
                            'value' => function (Company $model) {
                                return $model->b2bCount ?: '-';
                            }
                        ],
                        [
                            'attribute' => 'bookkeepingCount',
                            'label' => 'Бухгалтерия ИП',
                            'value' => function (Company $model) {
                                return $model->bookkeepingCount ?: '-';
                            }
                        ],
                        [
                            'attribute' => 'priceListsCount',
                            'label' => 'Прайс-листы',
                            'value' => function (Company $model) {
                                return $model->priceListsCount ?: '-';
                            }
                        ],
                        [
                            'attribute' => 'invoiceCount',
                            'label' => 'Выствавление счетов',
                            'value' => function (Company $model) {
                                return $model->invoiceCount ?: '-';
                            }
                        ],
                        [
                            'attribute' => 'paidCount',
                            'label' => 'Количество счетов',
                            'value' => function (Company $model) {
                                return $model->paidCount ?: '-';
                            }
                        ],
                        [
                            'attribute' => 'paidSum',
                            'label' => 'Сумма счетов',
                            'headerOptions' => [
                                'width' => '80px',
                            ],
                            'value' => function (Company $model) {
                                return TextHelper::invoiceMoneyFormat($model->paidSum * 100, 2);
                            }
                        ],
                        [
                            'attribute' => 'paidInvoicesCount',
                            'label' => 'Количество оплат',
                            'value' => function (Company $model) {
                                return $model->paidInvoicesCount ?: '-';
                            }
                        ],
                        [
                            'attribute' => 'paidInvoicesSum',
                            'label' => 'Сумма оплат',
                            'headerOptions' => [
                                'width' => '80px',
                            ],
                            'value' => function (Company $model) {
                                return TextHelper::invoiceMoneyFormat($model->paidInvoicesSum * 100, 2);
                            }
                        ],
                    ],
                ]); ?>
        </div>
    </div>
</div>
