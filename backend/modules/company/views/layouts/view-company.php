<?php

use common\components\date\DateHelper;
use common\models\Company;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

$this->beginContent('@backend/views/layouts/main.php');

$backUrl = $this->params['backUrl'] ?? [Yii::$app->request->get('backUrl', '/company/company/index')];

$model = $this->params['company'];

$route = Yii::$app->requestedRoute;
?>
<div class="form-horizontal">
    <div class="portlet">
        <div class="portlet-title">
            <?= Html::a('Назад к списку', $backUrl, [
                'class' => 'back-to-customers',
            ]); ?>

            <p>
                ID <?= $model->id ?>,
                зарегистрирована <?= date(DateHelper::FORMAT_USER_DATE, $model->created_at) ?>
            </p>

            <div class="d-flex justify-content-between flex-wrap mt-2">
                <div class="d-flex justify-content-start mr-auto mt-3">
                    <div class="actions mr-3">
                        <?= Html::a(' <i class="icon-pencil"></i>', ['/company/company/update', 'id' => $model->id,], [
                            'title' => 'Редактировать',
                            'class' => 'darkblue btn-sm',
                        ]); ?>
                    </div>
                    <div class="bold-text mr-3" style="font-size: 18px; line-height: 18px;">
                        <?= $model->getTitle(true); ?>
                        <?php if ($model->blocked == Company::BLOCKED): ?>
                            <span class="label label-default">Компания заблокирована</span>
                        <?php endif; ?>
                    </div>
                    <form>
                        <?= Html::activeCheckbox($model, 'test', [
                            'class' => 'form-control',
                            'label' => 'Тестовая компания',
                            'data' => [
                                'url' => Url::to(['/company/company/change-status', 'id' => $model->id]),
                            ]
                        ]); ?>
                    </form>
                </div>
                <div class="navbar-company ml-auto">
                    <ul id="w3" class="d-flex flex-nowrap navbar-nav navbar-right nav">
                        <li class="<?= 'company/company/view' == $route ? 'active' : '' ?>">
                            <?= Html::a('Профиль', ['/company/company/view', 'id' => $model->id]); ?>
                        </li>
                        <li>
                            <?= Html::a('Карточка компании', ['/company/company/view-form-card', 'id' => $model->id]); ?>
                        </li>
                        <li>
                            <?= Html::a('Партнерский кабинет', ['/company/company/view-partner-cabinet', 'company_id' => $model->id]); ?>
                        </li>
                        <li class="dropdown <?= $this->context->module->id == 'import' ? 'active' : '' ?>">
                            <?= Html::a('Импорт', null, [
                                'data-toggle' => 'dropdown',
                            ]); ?>
                            <?= Dropdown::widget([
                                'items' => [
                                    [
                                        'label' => 'Статьи прихода',
                                        'url' => ['/company/import/income-items/index', 'id' => $model->id],
                                    ],
                                    [
                                        'label' => 'Статьи расхода',
                                        'url' => ['/company/import/expense-items/index', 'id' => $model->id],
                                    ],
                                    [
                                        'label' => 'Проекты',
                                        'url' => ['/company/import/project/index', 'id' => $model->id],
                                    ],
                                    [
                                        'label' => '"Обогащение" выписки',
                                        'url' => ['/company/import/enrichment/index', 'id' => $model->id],
                                    ],
                                    [
                                        'label' => '"Разбитые" операции',
                                        'url' => ['/company/import/bank-partial-payments/index', 'id' => $model->id],
                                    ],
                                ],
                            ]); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div>
            <?= $content ?>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>
