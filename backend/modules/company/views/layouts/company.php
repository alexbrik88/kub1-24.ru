<?php
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@backend/views/layouts/main.php');
?>

<div class="invoice-report-content container-fluid" style="padding: 0; margin-top: -10px;">

<?php
NavBar::begin([
    'brandLabel' => 'Расширенный список',
    'brandUrl' => null,
    'options' => [
        'class' => 'navbar-report navbar-default',
    ],
    'brandOptions' => [
        'style' => 'margin-left: 0;'
    ],
    'innerContainerOptions' => [
        'class' => 'container-fluid',
        'style' => 'padding: 0;'
    ],
]);
echo Nav::widget([
    'items' => [
        ['label' => 'Компании', 'url' => ['/company/company/index-additional']],
        ['label' => 'Закрывающие документы', 'url' => ['/company/company/index-documents']],
    ],
    'options' => ['class' => 'navbar-nav navbar-right'],
]);
NavBar::end();
?>

<?php echo $content ?>

</div>

<?php $this->endContent(); ?>
