<?php

use common\components\date\DateHelper;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\TimeZone;

/* @var $this yii\web\View */
/* @var $model EmployeeCompany */
/* @var $companyId integer */

$initialConfig = [
    'wrapperOptions' => [
        'class' => 'col-md-2',
    ],
];
?>

<?php $form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'employee-form',
    'method' => 'POST',
    'options' => [
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate',
    ],

    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-2 control-label bold-text label-width',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-7',
        ],
        'hintOptions' => [
            'tag' => 'small',
            'class' => 'text-muted',
        ],
        'horizontalCssClasses' => [
            'offset' => 'col-md-offset-2',
            'hint' => 'col-md-7',
        ],
    ],

    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
])); ?>

<div class="form-body form-body_width">
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'email', [
        'hintOptions' => ['tag' => 'div',],
    ])->widget(\common\components\widgets\EmployeeTypeahead::className(), [
        'remoteUrl' => Url::to(['/dictionary/employee', 'companyId' => $companyId]),
        'admin' => true,
        'related' => [
            '#' . Html::getInputId($model, 'lastname') => 'lastname',
            '#' . Html::getInputId($model, 'firstname') => 'firstname',
            '#' . Html::getInputId($model, 'firstname_initial') => 'firstname_initial',
            '#' . Html::getInputId($model, 'patronymic') => 'patronymic',
            '#' . Html::getInputId($model, 'patronymic_initial') => 'patronymic_initial',
            '#' . Html::getInputId($model, 'time_zone_id') => 'time_zone_id',
            '#' . Html::getInputId($model, 'sex') => 'sex',
            '#' . Html::getInputId($model, 'birthday') => 'birthday_format',
            '#' . Html::getInputId($model, 'date_hiring') => 'date_hiring_format',
            '#' . Html::getInputId($model, 'date_dismissal') => 'date_dismissal_format',
            '#' . Html::getInputId($model, 'position') => 'position',
            '#' . Html::getInputId($model, 'employee_role_id') => 'employee_role_id',
            '#' . Html::getInputId($model, 'phone') => 'phone',
        ],
    ])->textInput([
        'placeholder' => 'Автозаполнение по email',
        'disabled' => $model->scenario !== 'create',
    ]); ?>

    <?= $form->field($model, 'lastname')->textInput([
        'maxlength' => true,
    ]); ?>
    <?= $form->field($model, 'firstname')->textInput([
        'maxlength' => true,
    ]); ?>
    <?= $form->field($model, 'patronymic')->textInput([
        'maxlength' => true,
    ]); ?>
    <?php if ($model->time_zone_id === null) {
        $model->time_zone_id = TimeZone::DEFAULT_TIME_ZONE;
    } ?>
    <?= $form->field($model, 'time_zone_id')->dropDownList(
        ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone')); ?>

    <?= $form->field($model, 'sex')
        ->radioList(Employee::$sex_message, [
            'uncheck' => null,
            'item' => function ($index, $label, $name, $checked, $value) {
                $radio = Html::radio($name, $checked, [
                    'checked' => $checked,
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'class' => 'radio-inline m-l-n-md',
                    ],
                ]);

                return Html::tag('div', $radio, [
                    'class' => 'col-md-p m-l-n-pd',
                ]);
            },
        ]); ?>

    <?= $form->field($model, 'birthday', [
        'wrapperOptions' => [
            'class' => 'col-md-2',
        ],
        'template' => Yii::$app->params['formDatePickerTemplate'],
    ])->textInput([
        'class' => 'form-control date-picker',
        'data-date-viewmode' => 'years',
        'value' => DateHelper::format($model->birthday, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
    ]); ?>


    <div class="form-group">
        <?= $form->field($model, 'date_hiring', [
            'options' => [
                'class' => '',
            ],
            'wrapperOptions' => [
                'class' => 'col-md-2',
            ],
            'template' => Yii::$app->params['formDatePickerTemplate'],
        ])->textInput([
            'class' => 'form-control date-picker',
            'data-date-viewmode' => 'years',
            'value' => DateHelper::format($model->date_hiring, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
        ]); ?>

        <?= $form->field($model, 'date_dismissal', [
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'class' => 'col-md-3 control-label bold-text label-width',
            ],
            'wrapperOptions' => [
                'class' => 'col-md-2',
            ],
            'template' => Yii::$app->params['formDatePickerTemplate'],
        ])->textInput([
            'class' => 'form-control date-picker',
            'data-date-viewmode' => 'years',
            'value' => DateHelper::format($model->date_dismissal, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
        ]); ?>
    </div>

    <?= $form->field($model, 'position')->textInput([
        'placeholder' => $model->getAttributeLabel('position'),
    ]); ?>
    <?= $form->field($model, 'employee_role_id')->dropDownList(
        ArrayHelper::map(\common\models\employee\EmployeeRole::find()->actual()->all(), 'id', 'name'),
        [
            'prompt' => '',
        ]
    ); ?>

    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control',
            'placeholder' => '+7(XXX) XXX-XX-XX',
        ],
    ]); ?>
</div>

<div class="form-actions">
    <div class="row action-buttons" id="buttons-fixed">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            ]); ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <a href="<?= Url::to($model->isNewRecord
                ? ['/company/company/view', 'id' => $model->company_id]
                : ['/company/employee/view', 'companyId' => $model->company_id, 'id' => $model->employee_id,]) ?>"
               class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">
                Отменить
            </a>
            <a href="<?= Url::to($model->isNewRecord
                ? ['/company/company/view', 'id' => $model->company_id]
                : ['/company/employee/view', 'companyId' => $model->company_id, 'id' => $model->employee_id,]) ?>"
               class="btn darkblue widthe-100 hidden-lg" title="Отменить">
                <i class="fa fa-reply fa-2x"></i>
            </a>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
    </div>
</div>

<?php $form->end(); ?>
