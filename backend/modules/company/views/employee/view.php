<?php

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\employee\Employee;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\EmployeeCompany */
/* @var $company common\models\Company */
/* @var $companyId integer */

$this->title = $model->fio;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';
?>

<div class="page-content-in">
    <a class="back-to-customers" href="<?= Url::to(['/company/company/view', 'id' => $companyId,]) ?>">Назад к комании</a>

    <div class="row">

        <div class="col-md-9">
            <div class="portlet customer-info customer-info-cash">
                <div class="portlet-title">
                    <div class="col-md-10 caption">
                        <?= $model->fio; ?>
                    </div>

                    <div class="actions">
                        <a class="btn darkblue btn-sm info-button" data-toggle="modal" href="#basic" style="width:33px; height: 27px;">
                            <i class="icon-info" style="color:black"></i>
                        </a>
                        <a href="<?= Url::to(['update', 'companyId' => $companyId, 'id' => $model->employee_id,]); ?>" title="Редактировать" class="btn darkblue btn-sm">
                            <i class="icon-pencil"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table id="datatable_ajax" class="table">
                        <tr>
                            <td class="bold-text border-space">Пол:</td>
                            <td class=""><?= ArrayHelper::getValue(Employee::$sex_message, $model->sex, 'не указано'); ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Дата рождения:</td>
                            <td><?= DateHelper::format($model->birthday, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Дата приёма на работу:</td>
                            <td><?= DateHelper::format($model->date_hiring, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Должность:</td>
                            <td><?= $model->position; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Роль:</td>
                            <td><?= $model->employeeRole->name; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Телефон:</td>
                            <td><?= $model->phone; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">E-mail:</td>
                            <td><?= $model->email; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="uneditable table-link-info">
                                    Указанный e-mail является также логином для доступа в систему
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="hide edit-employee-form" id="edit-cash-order">
                <div class="portlet">
                    <div class="portlet-body">
                        <?= $this->render('_form', [
                            'model' => $model,
                            'companyId' => $companyId,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row action-buttons buttons-fixed" id="buttons-fixed">
    <div class="button-bottom-page col-md-1">
        <?php if ($company->owner_employee_id != $model->employee_id) : ?>
            <?= \frontend\widgets\ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Владелец',
                    'class' => 'btn darkblue',
                ],
                'confirmUrl' => Url::to(['owner', 'companyId' => $companyId, 'id' => $model->employee_id,]),
                'message' => 'Вы уверены, что хотите сделать сотрудника владельцем учетной записи компании?',
            ]); ?>
        <?php endif ?>
    </div>
    <div class="button-bottom-page col-md-1">
    </div>
    <div class="button-bottom-page col-md-1">
    </div>
    <div class="button-bottom-page col-md-1">
    </div>
    <div class="button-bottom-page col-md-1">
    </div>
    <div class="button-bottom-page col-md-1">
    </div>
    <div class="button-bottom-page col-md-1">
        <button type="button" class="btn darkblue btn-cancel darkblue hide">Отменить</button>
    </div>
    <div class="button-bottom-page col-md-1">
        <?= \frontend\widgets\ConfirmModalWidget::widget([
            'toggleButton' => [
                'label' => 'Удалить',
                'class' => 'btn darkblue',
            ],
            'confirmUrl' => Url::to(['delete', 'companyId' => $companyId, 'id' => $model->employee_id,]),
            'message' => 'Вы уверены, что хотите удалить сотрудника?',
        ]); ?>
    </div>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="uneditable created-by" style="text-align: center">создан
                    <span><?= date("d.m.Y", $model->employee->created_at); ?></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>