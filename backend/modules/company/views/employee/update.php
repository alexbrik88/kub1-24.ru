<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\employee\Employee */
/* @var $companyId integer */

$this->title = 'Редактировать сотрудника';
?>
<div class="employee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'companyId' => $companyId,
    ]) ?>
</div>
