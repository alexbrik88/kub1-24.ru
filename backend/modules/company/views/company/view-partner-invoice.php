<?php

use common\models\company\CompanyInvoiceSearch;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Html;
use common\models\document\Invoice;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\components\date\DateHelper;
use common\models\service\PaymentType;
use common\components\grid\DropDownSearchDataColumn;
use common\models\service\Payment;
use php_rutils\RUtils;
use common\models\service\StoreOutInvoiceTariff;

/* @var $this yii\web\View
 * @var $model Invoice
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel CompanyInvoiceSearch
 */

$this->title = 'Счета';

?>

<div class="view-partner-invoice">
    <div class="row mt-5 pt-5" id="widgets">
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_viewPartials/affiliate/dashboard_block', [
                'color' => '_yellow',
                'array' => CompanyInvoiceSearch::$subscribeStatusArray,
                'status' => CompanyInvoiceSearch::SUBSCRIBE_STATUS_CREATED,
                'isMoney' => true,
                'sum' => $searchModel->getBaseQuery()->sum('total_amount_with_nds'),
                'count' => $searchModel->getBaseQuery()->count(),
            ]); ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_viewPartials/affiliate/dashboard_block', [
                'color' => '_red',
                'array' => CompanyInvoiceSearch::$subscribeStatusArray,
                'status' => CompanyInvoiceSearch::SUBSCRIBE_STATUS_OVERDUE,
                'isMoney' => true,
                'sum' => $searchModel->getBaseQuery()
                    ->andWhere(['and',
                        ['>', 'DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(`' . Payment::tableName() . '`.`created_at`)))', 10],
                        ['is_confirmed' => 0],
                    ])
                    ->sum('total_amount_with_nds'),
                'count' => $searchModel->getBaseQuery()
                    ->andWhere(['and',
                        ['>', 'DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(`' . Payment::tableName() . '`.`created_at`)))', 10],
                        ['is_confirmed' => 0],
                    ])
                    ->count(),
            ]); ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_viewPartials/affiliate/dashboard_block', [
                'color' => '_green',
                'array' => CompanyInvoiceSearch::$subscribeStatusArray,
                'status' => CompanyInvoiceSearch::SUBSCRIBE_STATUS_PAYED,
                'isMoney' => true,
                'sum' => $searchModel->getBaseQuery()
                    ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                    ->sum('total_amount_with_nds'),
                'count' => $searchModel->getBaseQuery()
                    ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                    ->count(),
            ]); ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_viewPartials/affiliate/dashboard_block', [
                'color' => '_green',
                'array' => CompanyInvoiceSearch::$subscribeStatusArray,
                'status' => CompanyInvoiceSearch::SUBSCRIBE_REWARDS,
                'isMoney' => true,
                'sum' => $searchModel->getBaseQuery()
                    ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                    ->sum('`rewards_amount`'),
                'count' => $searchModel->getBaseQuery()
                    ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                    ->count(),
            ]); ?>
        </div>
    </div>
    <div class="portlet box darkblue  mt-5 pt-5">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => ['view-partner-cabinet'],
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input text-middle"
                             style="width: 83%!important;">
                            <?= $form->field($searchModel, 'search')->textInput([
                                'placeholder' => 'Поиск по ID, названию, ИНН или E-mail',
                            ]); ?>
                        </div>
                        <div class="wimax_button" style="float: right;">
                            <?= Html::submitButton('ПРИМЕНИТЬ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'options' => [
                    'class' => 'overflow-x',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover documents_table',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'columns' => [
                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Invoice $model) {
                            return date(DateHelper::FORMAT_USER_DATE, $model->payment->created_at);
                        },
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№ счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Invoice $model) {
                            return $model->getFullNumber();
                        },
                    ],
                    [
                        'attribute' => 'total_amount_with_nds',
                        'label' => 'Сумма',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'style' => 'min-width: 100px',
                        ],
                        'value' => function (Invoice $model) {
                            return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                        },
                    ],
                    [
                        'attribute' => 'paymentType',
                        'class' => DropDownSearchDataColumn::className(),
                        'selectPluginOptions' => [
                            'width' => '180px',
                        ],
                        'label' => 'Тип платежа',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getPaymentTypeFilter(),
                        'value' => function (Invoice $model) {
                            return $model->payment->type->name;
                        },
                    ],
                    [
                        'attribute' => 'contractorName',
                        'class' => DropDownSearchDataColumn::className(),
                        'selectPluginOptions' => [
                            'width' => '180px',
                        ],
                        'label' => 'Компания',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getContractorFilter(),
                        'value' => function (Invoice $model) {
                            $companyName = $model->company->name_short;
                            return $companyName ? $model->company->companyType->name . ' ' . $companyName : "";
                        },
                    ],
                    [
                        'attribute' => 'subscribeStatus',
                        'class' => DropDownSearchDataColumn::className(),
                        'selectPluginOptions' => [
                            'width' => '180px',
                        ],
                        'label' => 'Статус',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getInvoiceStatusFilter(),
                        'value' => function (Invoice $model) {
                            $dateDiff = floor((time() - $model->payment->created_at) / 60 / 60 / 24);
                            if ($model->payment) {
                                if ($model->payment->is_confirmed) {
                                    return CompanyInvoiceSearch::$subscribeStatusArray[CompanyInvoiceSearch::SUBSCRIBE_STATUS_PAYED];
                                }
                            }
                            if ($dateDiff >= 10) {
                                return CompanyInvoiceSearch::$subscribeStatusArray[CompanyInvoiceSearch::SUBSCRIBE_STATUS_OVERDUE];

                            }

                            return CompanyInvoiceSearch::$subscribeStatusArray[CompanyInvoiceSearch::SUBSCRIBE_STATUS_CREATED];
                        },
                    ],
                    [
                        'attribute' => 'payment_date',
                        'label' => 'Дата оплаты',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Invoice $model) {
                            if ($model->payment) {
                                if ($model->payment->payment_date) {
                                    if ($model->payment->type_id == PaymentType::TYPE_ONLINE) {
                                        return date(DateHelper::FORMAT_USER_DATETIME, $model->payment->payment_date);
                                    }

                                    return date(DateHelper::FORMAT_USER_DATE, $model->payment->payment_date);
                                }
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'tariffType',
                        'class' => DropDownSearchDataColumn::className(),
                        'selectPluginOptions' => [
                            'width' => '200px',
                        ],
                        'label' => 'Тип подписки',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getTariffFilter(),
                        'format' => 'html',
                        'value' => function (Invoice $model) {
                            if ($model->payment->storeTariff) {
                                return $model->payment->storeTariff->cabinets_count . ' ' .
                                    RUtils::numeral()->choosePlural($model->payment->storeTariff->cabinets_count, [
                                        'кабинет', //1
                                        'кабинета', //2
                                        'кабинетов' //5
                                    ]);
                            } elseif ($model->payment->outInvoiceTariff) {
                                if ($model->payment->outInvoiceTariff->id == StoreOutInvoiceTariff::UNLIM_TARIFF) {
                                    return $model->payment->outInvoiceTariff->links_count . ' ссылок';
                                }
                                return $model->payment->outInvoiceTariff->links_count . ' ' .
                                    RUtils::numeral()->choosePlural($model->payment->outInvoiceTariff->links_count, [
                                        'ссылка', //1
                                        'ссылки', //2
                                        'ссылок' //5
                                    ]);
                            } elseif ($model->payment->payment_for == Payment::FOR_ODDS) {
                                return 'Настройка финансовых отчетов';
                            } elseif ($model->payment->payment_for == Payment::FOR_ADD_INVOICE) {
                                return $model->payment->invoiceTariff->invoice_count . ' ' .
                                    RUtils::numeral()->choosePlural($model->payment->invoiceTariff->invoice_count, [
                                        'счет', //1
                                        'счета', //2
                                        'счетов' //5
                                    ]);
                            } elseif ($tariff = $model->payment->tariff) {
                                return $tariff->tariffGroup->name . '<br>' . $tariff->getTariffName();
                            }

                            return '';
                        },
                    ],
                    [
                        'label' => '%%',
                        'value' => function (Invoice $model) {
                            return $model->rewards_percent ?: "20";
                        },
                    ],
                    [
                        'attribute' => 'rewards_amount',
                        'label' => 'Сумма вознаграждения',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Invoice $model) {
                            $rewardsAmount = $model->rewards_amount;
                            if (!$rewardsAmount && $model->payment->is_confirmed) {
                                $rewardsAmount =  ($model->total_amount_with_nds / 100) * 20;
                            }
                            return TextHelper::invoiceMoneyFormat($rewardsAmount, 2);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
