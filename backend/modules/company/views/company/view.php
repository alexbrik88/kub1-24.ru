<?php
use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\Company;
use common\models\employee\Employee;
use yii\helpers\Url;
use backend\modules\company\models\AffiliateSearch;

/* @var $this yii\web\View */
/* @var Company $model */
/* @var Employee $employeeModel */
/* @var $subscribeSearchModel \backend\modules\company\models\SubscribeSearch */
/* @var $subscribeDataProvider yii\data\ActiveDataProvider */
/* @var $promocodeSearchModel \backend\modules\company\models\PromoCodeSearch */
/* @var $promocodeDataProvider yii\data\ActiveDataProvider */
/* @var $employeeSearchModel common\models\employee\EmployeeSearch */
/* @var $employeeDataProvider yii\data\ActiveDataProvider */
/* @var $affiliateSearchModel AffiliateSearch */
/* @var $affiliateDataProvider yii\data\ActiveDataProvider */
/* @var $backUrl string */

$this->title = 'Профиль компании';
$this->context->layoutWrapperCssClass = 'edit-profile';

$this->params['company'] = $model;
?>

<?= $this->render('_viewPartials/_partial_basic_info', [
    'model' => $model,
]); ?>

<?= $this->render('_viewPartials/_partial_create_invoice', [
    'model' => $model,
]); ?>

<?= $this->render('_viewPartials/_partial_payment_history', [
    'model' => $model,
    'paymentSearchModel' => $paymentSearchModel,
    'paymentDataProvider' => $paymentDataProvider,
]); ?>

<?= $this->render('_viewPartials/_partial_promocodes', [
    'model' => $model,
    'promocodeSearchModel' => $promocodeSearchModel,
    'promocodeDataProvider' => $promocodeDataProvider,
]); ?>

<?= $this->render('_viewPartials/_partial_affiliate', [
    'model' => $model,
    'affiliateSearchModel' => $affiliateSearchModel,
    'affiliateDataProvider' => $affiliateDataProvider,
]); ?>

<?= $this->render('_viewPartials/_partial_statistic', [
    'model' => $model,
]); ?>

<?= $this->render('_viewPartials/_partial_integration', [
    'model' => $model,
]); ?>

<?= $this->render('_viewPartials/_partial_profile', [
    'model' => $model,
]); ?>

<?= $this->render('_viewPartials/_partial_checking_accountant', [
    'model' => $model,
]); ?>

<?= $this->render('_viewPartials/_partial_employees', [
    'company' => $model,
    'searchModel' => $employeeSearchModel,
    'dataProvider' => $employeeDataProvider,
]); ?>
