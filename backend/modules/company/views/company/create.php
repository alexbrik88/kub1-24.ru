<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = 'Создать компанию';
?>
<div class="help-article-create">

    <?= $this->render('_viewPartials/_form', [
        'model' => $model,
    ]) ?>

</div>
