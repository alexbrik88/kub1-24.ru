<?php
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\CompanyAffiliateSearch;
use common\models\service\Payment;
use common\components\date\DateHelper;
use common\models\service\ServiceModule;
use frontend\themes\kub\helpers\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;

/** @var $this View */
/** @var Company $model */
/** @var CompanyAffiliateSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */

?>


<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]); ?>


<div class="selling-subscribe-index">
    <div class="row mt-5 pt-5" id="widgets">
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_viewPartials/affiliate/dashboard_block', [
                'color' => '_yellow',
                'array' => CompanyAffiliateSearch::$payedStatusArray,
                'status' => CompanyAffiliateSearch::SUBSCRIBE_AFFILIATE,
                'isMoney' => false,
                'sum' => $searchModel->getBaseQuery()
                    ->groupBy('`company`.`id`')
                    ->count(),
            ]); ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_viewPartials/affiliate/dashboard_block', [
                'color' => '_red',
                'array' => CompanyAffiliateSearch::$payedStatusArray,
                'status' => CompanyAffiliateSearch::SUBSCRIBE_AFFILIATE_GET_INVOICE,
                'isMoney' => false,
                'sum' => $searchModel->getBaseQuery()
                    ->joinWith('invoices', true, 'INNER JOIN')
                    ->groupBy('`company`.`id`')
                    ->count(),
            ]); ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_viewPartials/affiliate/dashboard_block', [
                'color' => '_red',
                'array' => CompanyAffiliateSearch::$payedStatusArray,
                'status' => CompanyAffiliateSearch::SUBSCRIBE_AFFILIATE_INVOICE_PAYED,
                'isMoney' => false,
                'sum' => $searchModel->getBaseQuery()
                    ->joinWith('invoices', true, 'INNER JOIN')
                    ->andWhere(['and',
                        ['is_confirmed' => 1],
                    ])
                    ->groupBy('`company`.`id`')
                    ->count(),
            ]); ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $this->render('_viewPartials/affiliate/dashboard_block', [
                'color' => '_green',
                'array' => CompanyAffiliateSearch::$payedStatusArray,
                'status' => CompanyAffiliateSearch::SUBSCRIBE_AFFILIATE_PAYED,
                'isMoney' => true,
                'sum' => $searchModel->getBaseQuery()
                    ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                    ->sum('sum') * 100,
                'count' => $searchModel->getBaseQuery()
                    ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                    ->count(),
            ]); ?>
        </div>
    </div>
    <div class="portlet box darkblue  mt-5 pt-5">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => ['view-partner-cabinet'],
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input text-middle"
                             style="width: 83%!important;">
                            <?= $form->field($searchModel, 'search')->textInput([
                                'placeholder' => 'Поиск по ID, названию, ИНН или E-mail',
                            ]); ?>
                        </div>
                        <div class="wimax_button" style="float: right;">
                            <?= Html::submitButton('ПРИМЕНИТЬ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                'options' => [
                    'style' => 'overflow-x: auto;',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination list-clr pull-right',
                    ],
                ],

                'rowOptions' =>
                    function ($data) {
                        /* @var $data Company */
                        return ['class' => ($data->blocked == Company::BLOCKED) ? 'danger' : ''];
                    },
                'columns' => [
                    [
                        'attribute' => 'created_at',
                        'label' => 'Дата регистрации',
                        'value' => function (Company $model) {
                            return DateHelper::format(date(DateHelper::FORMAT_DATE, $model->created_at), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                        },
                    ],
                    [
                        'attribute' => 'name_short',
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            $companyName = $model->name_short ? $model->companyType->name . ' ' . trim($model->name_short) : 'Не заполнено';
                            return Html::a($companyName, Url::to(['/company/company/view', 'id' => $model->id]), ['target' => '_blank']);
                        },
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'raw',
                        'value' => function (Company $model) {
                            $companyEmail = substr($model->email, 0, mb_strpos($model->email, '@'));
                            $tooltipster = Html::tag('span', '<i class="fa fa-question-circle"></i>', [
                                'class' => 'tooltip2',
                                'data-tooltip-content' => '#tooltip_email',
                            ]);
                            return Html::tag('div', Html::encode($companyEmail) . ' ' . $tooltipster, [
                                'style' => 'min-width: 120px',
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'product',
                        'label' => 'Продукт',
                        'class' => DropDownSearchDataColumn::className(),
                        'filter' => $searchModel->getAffiliateProductFilter(),
                        'value' => function (Company $model) {
                            return ServiceModule::$serviceModuleLabel[
                                ServiceModule::$serviceModuleArray[$model->product]
                            ];
                        },
                    ],
                    [
                        'attribute' => 'activeTariff',
                        'label' => 'Тарифный план',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'filter' => $searchModel->getTariffFilter(),
                        'value' => function (Company $model) {
                            return $model->hasActualSubscription ?
                                $model->activeSubscribe->getTariffName() :
                                ($model->isFreeTariff ? 'Тариф "Бесплатно"' : 'Нет');
                        },
                    ],
                    [
                        'attribute' => 'paidCount',
                        'label' => 'Количество счетов',
                        'value' => function (Company $model) {
                            return $model->paidCount ?: '-';
                        },
                    ],
                    [
                        'attribute' => 'paidSum',
                        'label' => 'Сумма счетов',
                        'headerOptions' => [
                            'style' => 'min-width: 100px'
                        ],
                        'value' => function (Company $model) {
                            return TextHelper::invoiceMoneyFormat($model->paidSum * 100, 2);
                        },
                    ],

                    [
                        'attribute' => 'paidInvoicesCount',
                        'label' => 'Количество оплат',
                        'headerOptions' => [
                            'style' => 'min-width: 100px'
                        ],
                        'value' => function (Company $model) {
                            return $model->getPayments()->andWhere(['is_confirmed' => 1])->count() ?: '-';
                        },
                    ],

                    [
                        'attribute' => 'paidInvoicesSum',
                        'label' => 'Сумма оплат',
                        'headerOptions' => [
                            'style' => 'min-width: 100px'
                        ],
                        'value' => function (Company $model) {
                            $sum = $model->getPayments()->andWhere(['is_confirmed' => 1])->sum('sum');
                            return TextHelper::invoiceMoneyFormat($sum * 100, 2);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>


<div class="tootip-template" style="display: none">
    <span id="tooltip_email" style="display: inline-block; text-align: center;">Согласно закону о защите персональных данных email отображается не полностью</span>
</div>