<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\service\SubscribeHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\company\models\CompanySearch;
use common\components\grid\DropDownSearchDataColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\modules\company\models\CompanySearch */
/* @var $ioType int */

$this->title = 'Компании';
?>
<style>
    .wimax_radiogroup {
        width: 300px;
        margin-right: 0px;
    }
</style>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', Url::to(['create']), ['class' => 'btn yellow']); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>
<div class="row">

</div>

<div class="row">
    <?= $this->render('_viewPartials/_registration_statistic'); ?>
</div>

<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-3 col-sm-3">
            <?= $this->title; ?>
        </div>
        <div class="tools tools_button col-md-9 col-sm-9">
            <div class="form-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'GET',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input text-middle">
                        <?= $form->field($searchModel, 'search')->textInput([
                            'placeholder' => 'Поиск по ID, названию, ИНН или E-mail',
                        ]); ?>
                    </div>
                    <div class="wimax_radiogroup">
                        <?= $form->field($searchModel, 'blockedStatus')->label(false)->radioList([
                            CompanySearch::ALL => 'Все',
                            CompanySearch::BLOCKED_STATUS_INACTIVE => 'Активные',
                            CompanySearch::BLOCKED_STATUS_ACTIVE => 'Заблокированные',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('ПРИМЕНИТЬ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

            'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
            'options' => [
                'style' => 'overflow-x: auto;',
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],

            'rowOptions' =>
                function ($data) {
                    /* @var $data Company */
                    return ['class' => ($data->blocked == Company::BLOCKED) ? 'danger' : ''];
                },
            'columns' => [
                [
                    'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function (Company $model) {
                        return Html::a($model->id, ['view', 'id' => $model->id, 'backUrl' => 'index']);
                    },
                ],
                [
                    'attribute' => 'name_short',
                    'format' => 'raw',
                    'value' => function (Company $model) {
                        return Html::a($model->name_short, ['view', 'id' => $model->id, 'backUrl' => 'index']);
                    },
                ],
                'inn:text',
                [
                    'label' => 'Статус',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'attribute' => 'blocked',
                    'value' => function (Company $model) {
                        return $model->blocked ? 'Заблокирован' : 'Активен';
                    },
                ],
                [
                    'attribute' => 'last_visit_at',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return $model->last_visit_at
                            ? date(\common\components\date\DateHelper::FORMAT_USER_DATETIME, $model->last_visit_at)
                            : 'Не было';
                    },
                ],
                [
                    'attribute' => 'activeTariff',
                    'label' => 'Текущая подписка',
                    'class' => DropDownSearchDataColumn::className(),
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => $searchModel->getSubscribeFilter(),
                    'value' => function (Company $model) {
                        return $model->hasActualSubscription ?
                            $model->actualSubscription->getTariffName() :
                            ($model->isFreeTariff ? 'Тариф "Бесплатно"' : 'Нет');
                    },
                ],
                [
                    'attribute' => 'activeSubscribeDaysCount',
                    'label' => 'Доступно дней',
                    //   'value' => function (Company $model) {
                    //       $subscribeArray = SubscribeHelper::getPayedSubscriptions($model->id);
                    //       $expireDate = SubscribeHelper::getExpireDate($subscribeArray);
                    //
                    //       return SubscribeHelper::getExpireLeftDays($expireDate);
                    //   },
                ],
                [
                    'attribute' => 'notPaidInvoiceCount',
                    'label' => 'Не оплачено счетов',
                //    'value' => function (Company $model) {
                //        return (int) \common\models\service\Subscribe::find()
                //            ->byCompany($model->id)
                //            ->byStatus([\common\models\service\SubscribeStatus::STATUS_NEW])
                //            ->count();
                //    },
                ],
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{update} {block}',
                    'headerOptions' => [
                        'width' => '80',
                    ],
                    'urlCreator' => function($action, $model) {
                        return Url::to(['company/' . $action, 'id' => $model['id']]);
                    },
                    'buttons' => [
                        'block' => function ($url, $data) {

                            /* @var $data Company */
                            if ($data->blocked == Company::BLOCKED) {
                                $button = Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', $url, [
                                    'title' => 'Разблокировать',
                                    'data-confirm' => 'Разблокировать компанию?',
                                    'data-method' => 'post',
                                ]);
                            } else {
                                $button = Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', $url, [
                                    'title' => 'Заблокировать',
                                    'data-confirm' => 'Вы уверены, что хотите заблокировать компанию?',
                                    'data-method' => 'post',
                                ]);
                            }

                            return $button;
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
