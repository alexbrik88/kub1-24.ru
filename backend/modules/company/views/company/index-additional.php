<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.12.2016
 * Time: 17:04
 */

use common\models\Company;
use common\models\company\CompanyType;
use yii\helpers\Html;
use common\components\grid\DropDownSearchDataColumn;
use common\components\helpers\GridViewHelper;
use yii\helpers\StringHelper;
use common\models\service\SubscribeHelper;
use yii\helpers\Url;
use common\models\cash\CashBankStatementUpload;
use common\models\employee\Employee;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\modules\company\models\CompanyAdditionalSearch */

$this->title = 'Компании';
?>
<div class="row">
    <div class="col-md-9 col-sm-9">
    </div>
    <div class="col-md-3 col-sm-3">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
</div>
<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-3 col-sm-3">
            <?= $this->title . ' (' . $dataProvider->totalCount . ')'; ?>
        </div>
        <div class="caption list_recip col-md-3 col-sm-3"
             style="float: right;text-align: right;">
            <?= Html::a('Выгрузить в Excel', Url::to($searchModel->getDownloadExcelUrl()), [
                'style' => 'color: white;',
            ]); ?>
        </div>
    </div>
    <div class="portlet-body">
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

            'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
            'options' => [
                'style' => 'overflow-x: auto;',
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],

            'rowOptions' =>
                function ($data) {
                    /* @var $data Company */
                    return ['class' => ($data->blocked == Company::BLOCKED) ? 'danger' : ''];
                },
            'columns' => [
                [
                    'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function (Company $model) {
                        return Html::a($model->id, ['view', 'id' => $model->id, 'backUrl' => 'index-additional']);
                    },
                ],
                [
                    'label' => 'Название организации',
                    'attribute' => 'name_short',
                    'class' => DropDownSearchDataColumn::className(),
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => 'dropdown-filter',
                    ],
                    'format' => 'raw',
                    'filter' => $searchModel->getNameFilter(),
                    'value' => function (Company $model) {
                        return Html::a($model->name_short, ['view', 'id' => $model->id, 'backUrl' => 'index-additional']);
                    },
                ],
                [
                    'attribute' => 'email',
                    'label' => 'Email',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'phone',
                    'label' => 'Телефон',
                    'class' => DropDownSearchDataColumn::className(),
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => $searchModel->getPhoneFilter(),
                    'value' => function (Company $model) {
                        return $model->phone;
                    },
                ],
                [
                    'attribute' => 'created_by',
                    'label' => 'ФИО регистрация',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return ($model->created_by && ($createdByEmployee = Employee::findOne($model->created_by)))
                            ? $createdByEmployee->getFio()
                            : '---';
                    },
                ],
                [
                    'class' => DropDownSearchDataColumn::className(),
                    'attribute' => 'fio',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => $searchModel->getChiefFioFilter(),
                    'label' => 'ФИО руководителя',
                    'value' => function (Company $model) {
                        return $model->getChiefFio();
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата регистрации',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'sorting',
                    ],
                    'value' => function ($model) {
                        return date(\common\components\date\DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS, $model->created_at);
                    }
                ],
                [
                    'class' => DropDownSearchDataColumn::className(),
                    'attribute' => 'registrationPageType',
                    'label' => 'Страница регистрации',
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                        'width' => '10%',
                    ],
                    'filter' => $searchModel->getRegistrationPageTypeFilter(),
                    'format' => 'raw',
                    'value' => function (Company $model) {
                        return $model->registrationPageType ? $model->registrationPageType->name : '';
                    }
                ],
                [
                    'class' => DropDownSearchDataColumn::className(),
                    'attribute' => 'utm_source',
                    'label' => 'Источник',
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                        'width' => '10%',
                    ],
                    'filter' => $searchModel->getUtmSourceFilter(),
                    'value' => function (Company $model) {
                        return $model->utm_source;
                    },
                ],
                [
                    'attribute' => 'inn',
                    'label' => 'ИНН',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return $model->inn;
                    },
                ],
                [
                    'attribute' => 'company_type_id',
                    'label' => 'ФС',
                    'class' => DropDownSearchDataColumn::className(),
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => $searchModel->getCompanyTypesArray(),
                    'value' => function (Company $model) {

                        return $model->companyType->name_short;
                    },
                ],
                [
                    'attribute' => 'companyTaxationType',
                    'label' => 'СНО',
                    'class' => DropDownSearchDataColumn::className(),
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => $searchModel->getCompanyTaxationTypesArray(),
                    'value' => function (Company $model) {

                        return $model->companyTaxationType->name;
                    },
                ],
                [
                    'attribute' => 'bankName',
                    'label' => 'Банк',
                    'class' => DropDownSearchDataColumn::className(),
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => $searchModel->getBankNameArray(),
                    'value' => function (Company $model) {
                        return $model->mainCheckingAccountant ? $model->mainCheckingAccountant->bank_name : '(Не задано)';
                    },
                ],
                [
                    'attribute' => 'outInvoiceCount',
                    'label' => 'Кол-во ИСХ счетов',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'InInvoiceCount',
                    'label' => 'Кол-во ВХ счетов',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'outInvoiceSendEmailCount',
                    'label' => 'Количество отправленных счетов',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'statementFromBank',
                    'label' => 'Кол-во загруженных выписок',
                ],
                [
                    'attribute' => 'isFileLogo',
                    'label' => 'Лого',
                ],
                [
                    'attribute' => 'isFilePrint',
                    'label' => 'Печать',
                ],
                [
                    'attribute' => 'isFileSignature',
                    'label' => 'Подпись',
                ],
                [
                    'attribute' => 'filesCount',
                    'label' => 'Итого',
                ],
                [
                    'attribute' => 'activation_type',
                    'class' => DropDownSearchDataColumn::className(),
                    'label' => 'Статус',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => $searchModel->getActivationStatusFilter(),
                    'value' => function (Company $model) {
                        return isset(Company::$activationType[$model->activation_type]) ? Company::$activationType[$model->activation_type] : '';
                    },
                ],
                [
                    'attribute' => 'activeTariff',
                    'class' => DropDownSearchDataColumn::className(),
                    'label' => 'Тарифный план',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => $searchModel->getTariffNameFilter(),
                    'value' => function (Company $model) {
                        return $model->actualSubscription ?
                            $model->actualSubscription->getTariffName() :
                            ($model->isFreeTariff ? 'Тариф "Бесплатно"' : 'Нет');
                    },
                ],
                [
                    'attribute' => 'activeSubscribeDaysCount',
                    'label' => 'Осталось дней (Всего)',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'sorting',
                    ],
                ],
                [
                    'attribute' => 'customersCount',
                    'label' => 'Покупатели',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return $model->getCustomersCount();
                    },
                ],
                [
                    'attribute' => 'sellersCount',
                    'label' => 'Продавцы',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return $model->getSellersCount();
                    },
                ],
                [
                    'attribute' => 'autoInvoicesCount',
                    'label' => 'Автосчета',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return $model->getAutoInvoicesCount();
                    },
                ],
                [
                    'attribute' => 'outActCount',
                    'label' => 'Актсверки',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return $model->getOutActCount();
                    },
                ],
                [
                    'attribute' => 'paymentOrderCount',
                    'label' => 'Платежки',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return $model->getPaymentOrderCount();
                    },
                ],
                [
                    'attribute' => 'import_xls_product_count',
                    'label' => 'Товары из Excel',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return (int)$model->import_xls_product_count;
                    },
                ],
                [
                    'attribute' => 'import_xls_service_count',
                    'label' => 'Услуги из Excel',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return (int)$model->import_xls_service_count;
                    },
                ],
                [
                    'attribute' => 'goodsCount',
                    'label' => 'Товары',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return $model->getGoodsCount();
                    },
                ],
                [
                    'attribute' => 'servicesCount',
                    'label' => 'Услуги',
                    'headerOptions' => [
                        'width' => '20%',
                    ],
                    'value' => function (Company $model) {
                        return $model->getServicesCount();
                    },
                ],
            ],
        ]); ?>
    </div>
</div>