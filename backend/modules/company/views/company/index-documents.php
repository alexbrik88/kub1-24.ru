<?php

use common\models\Company;
use common\models\product\Product;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\modules\company\models\CompanyDocumentsSearch */

$this->title = 'Закрывающие документы';
?>
<div class="row">
    <div class="col-md-9 col-sm-9">
    </div>
    <div class="col-md-3 col-sm-3">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
</div>
<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-3 col-sm-3">
            <?= $this->title; ?>
        </div>
    </div>
    <div class="portlet-body">
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

            'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
            'options' => [
                'style' => 'overflow-x: auto;',
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],

            'rowOptions' =>
                function ($data) {
                    /* @var $data Company */
                    return ['class' => ($data->blocked == Company::BLOCKED) ? 'danger' : ''];
                },
            'columns' => [
                [
                    'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function (Company $model) {
                        return Html::a($model->id, ['view', 'id' => $model->id,]);
                    },
                ],
                [
                    'attribute' => 'name_short',
                    'label' => 'Название компании',
                    'format' => 'raw',
                    'value' => function (Company $model) {
                        return Html::a($model->name_short, ['view', 'id' => $model->id,]);
                    },
                ],
                [
                    'attribute' => 'email',
                    'label' => 'Email',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'format' => 'email',
                ],
                [
                    'attribute' => 'chiefFio',
                    'label' => 'ФИО руководителя',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата регистрации',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => 'sorting',
                    ],
                    'format' => 'date',
                ],
                [
                    'attribute' => 'notCreatedInvoice',
                    'label' => 'Счет',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                ],
                [
                    'attribute' => 'notCreatedAct',
                    'label' => 'Акт',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                ],
                [
                    'attribute' => 'notCreatedPL',
                    'label' => 'ТН',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                ],
                [
                    'attribute' => 'notCreatedIF',
                    'label' => 'СФ',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'value' => function (Company $model) {
                        return $model->hasNds() ? $model->notCreatedIF : '---';
                    },
                ],
            ],
        ]); ?>
    </div>
</div>