<?php
use common\models\Company;
use common\models\company\CompanyAffiliateLink;
use common\models\company\CompanyAffiliateLinkSearch;
use common\models\company\CompanyAffiliateSearch;
use common\models\company\CompanyInvoiceSearch;
use frontend\widgets\RangeButtonWidget;
use yii\bootstrap\Html;
use common\components\date\DateHelper;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use common\models\ServiceMoreStock;
use yii\web\View;

/** @var $this yii\web\View */
/** @var Company $model */
/** @var string $tab */
/** @var CompanyAffiliateSearch $searchModel */
/** @var CompanyInvoiceSearch $invoiceSearchModel */
/** @var CompanyAffiliateLinkSearch $affiliateSearchModel */
/** @var ActiveDataProvider $dataProvider */
/** @var ActiveDataProvider $invoiceDataProvider */
/** @var ActiveDataProvider $affiliateDataProvider */


$this->title = 'Карточка компании';
$this->context->layoutWrapperCssClass = 'edit-profile';

$this->registerJs(<<<JS
    $('.company-is-partner').find('input').click(function () {
        $.post($(this).data('url'), {status: +$(this).is(':checked')},
            function (result) {
                if (result) {
                    location.reload(true);
                }
            }
        )
    });
JS, View::POS_READY
);

$this->registerCss(<<<CSS
    .company-is-partner span {
        padding-top: 5px;
        
        font-size: 14px;
        line-height: 14px;
        
        vertical-align: baseline;
    }
CSS
);

?>

<div class="form-horizontal">
    <div class="portlet">
        <div class="portlet-title">
            <?= Html::a('Назад к списку', ['index'], [
                'class' => 'back-to-customers',
            ]); ?>

            <p>
                ID <?= $model->id ?>,
                зарегистрирована <?= date(DateHelper::FORMAT_USER_DATE, $model->created_at) ?>
            </p>

            <div class="caption bold-text">
                <?= $model->getTitle(true); ?>
                <?php if ($model->blocked == Company::BLOCKED): ?>
                    <span
                        class="label label-default">Компания заблокирована</span>
                <?php endif; ?>
            </div>
            <div class="company-type-test" style="float: left;">
                <?= Html::checkbox('Company[test]', $model->test, [
                    'class' => 'form-control',
                    'data' => [
                        'url' => Url::to(['/company/company/change-status', 'id' => $model->id]),
                    ]
                ]); ?>
                <label>Тестовая компания</label>
            </div>
            <div class="col-md-7 navbar-company pull-right">
                <ul id="w3" class="navbar-nav navbar-right nav"
                    style="margin-bottom: -15px;">
                    <li>
                        <?= Html::a('Профиль', Url::to(['view', 'id' => $model->id])); ?>
                    </li>
                    <li>
                        <?= Html::a('Карточка компании', Url::to(['view-form-card', 'id' => $model->id])); ?>
                    </li>
                    <li class="active">
                        <?= Html::a('Партнерский кабинет', Url::to(['view-partner-cabinet', 'company_id' => $model->id])); ?>
                    </li>
                </ul>
            </div>
            <div class="actions">
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 pb-5 company-is-partner pull-left">
                <div class="pull-left">
                    <?= Html::checkbox('Company[is_partner]', $model->is_partner, [
                        'class' => 'form-control',
                        'data' => [
                            'url' => Url::to(['/company/company/change-partner-status', 'company_id' => $model->id]),
                        ],
                    ]); ?>
                    <span>Партнер</span>
                </div>
                <div class="pull-left ml-2 pt-0">
                    <span>
                        [
                    </span>
                    <span>
                        <?= isset($model->affiliate_link_created_at) ? 'Начало: ' . date(DateHelper::FORMAT_USER_DATE, $model->affiliate_link_created_at) . ';' : ''; ?>
                    </span>
                    <span>
                        <?= isset($model->affiliate_link_deleted_at) ? "&nbsp;Окончание: " : ''; ?>
                    </span>
                    <span>
                        <?= isset($model->affiliate_link_deleted_at) && !$model->is_partner ? date(DateHelper::FORMAT_USER_DATE, $model->affiliate_link_deleted_at) . ';' : ''; ?>
                    </span>
                    <span>
                        ]
                    </span>

                </div>
            </div>
            <?php if ($model->is_partner): ?>
                <div class="col-md-3 pull-right">
                    <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
                </div>
            <?php endif; ?>
        </div>
        <?php
        if (!$model->is_partner) {
            return true;
        }
        ?>
        <div class="row">
            <div class="navbar-company pull-left">
                <ul class="navbar-nav nav">
                    <li class="<?= !$tab || $tab == 'client' ? 'active' : ''?>">
                        <?= Html::a('Клиенты', Url::to(['view-partner-cabinet', 'company_id' => $model->id, 'tab' => 'client'])); ?>
                    </li>
                    <li class="<?= $tab == 'invoice' ? 'active' : ''?>">
                        <?= Html::a('Счета', Url::to(['view-partner-cabinet', 'company_id' => $model->id, 'tab' => 'invoice'])); ?>
                    </li>
                    <li class="<?= $tab == 'payment' ? 'active' : ''?>">
                        <?= Html::a('Выплаты', Url::to(['view-partner-cabinet', 'company_id' => $model->id, 'tab' => 'payment'])); ?>
                    </li>
                    <li class="<?= $tab == 'referral' ? 'active' : ''?>">
                        <?= Html::a('Реферальные ссылки', Url::to(['view-partner-cabinet', 'company_id' => $model->id, 'tab' => 'referral'])); ?>
                    </li>
                    <li class="<?= $tab == 'reward' ? 'active' : ''?>">
                        <?= Html::a('% Вознаграждения', Url::to(['view-partner-cabinet', 'company_id' => $model->id, 'tab' => 'reward'])); ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <?php
                switch ($tab) {
                    case 'invoice':
                        echo $this->render('view-partner-invoice', [
                            'model' => $model,
                            'searchModel' => $invoiceSearchModel,
                            'dataProvider' => $invoiceDataProvider,
                        ]);
                        break;
                    case 'payment':
                        echo $this->render('view-partner-empty');
                        break;
                    case 'reward':
                        echo $this->render('view-partner-rewards', [
                            'searchModel' => $affiliateLeadRewardsSearchModel,
                            'dataProvider' => $affiliateLeadRewardsDataProvider,
                        ]);
                        break;
                    case 'referral':
                        echo $this->render('view-partner-referral', [
                            'model' => (new CompanyAffiliateLink()),
                            'companyModel' => $model,
                            'searchModel' => $affiliateSearchModel,
                            'dataProvider' => $affiliateDataProvider,
                        ]);
                        break;
                    default:
                        echo $this->render('view-partner-client', [
                            'model' => $model,
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                        ]);
                        break;
                }
            ?>
        </div>
    </div>
</div>