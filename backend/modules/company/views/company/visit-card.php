<?php
use common\models\Company;
use common\components\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var Company $company */

$this->title = 'Визитка компании';
?>

<?= Html::a('Назад к профилю', Url::to(['company/view', 'id' => $company->id])); ?>

<h3 class="page-title">Визитка</h3>

<h3>
    <b><?php echo $company->getTitle(true); ?></b>
</h3>

<table style="border-collapse: separate; border-spacing: 5px;"
       class="table_marg">
    <tbody>
    <tr>
        <th><?= $company->getAttributeLabel('taxation_type_id'); ?>:</th>
        <td><?= $company->taxationType->name; ?></td>
    </tr>

    <tr>
        <th>&nbsp;</th>
    </tr>

    <tr>
        <th>Юридический адрес:</th>
        <td><?= $company->getAddressLegalFull(); ?></td>
    </tr>
    <tr>
        <th>Фактический адрес:</th>
        <td><?= $company->getAddressActualFull(); ?></td>
    </tr>
    </tbody>

    <tr>
        <th>&nbsp;</th>
    </tr>

    <?php if ($company->company_type_id == \common\models\company\CompanyType::TYPE_IP): ?>
        <tr>
            <th><?= $company->getAttributeLabel('egrip'); ?>:</th>
            <td><?= $company->egrip; ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('inn'); ?>:</th>
            <td><?= $company->inn; ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('okved'); ?>:</th>
            <td><?= $company->okved; ?></td>
        </tr>
    <?php else: ?>
        <tr>
            <th><?= $company->getAttributeLabel('ogrn'); ?>:</th>
            <td><?= $company->ogrn; ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('inn'); ?>:</th>
            <td><?= $company->inn; ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('kpp'); ?>:</th>
            <td><?= $company->kpp; ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('okved'); ?>:</th>
            <td><?= $company->okved; ?></td>
        </tr>
    <?php endif; ?>

    <tr>
        <th>&nbsp;</th>
    </tr>

    <tr>
        <th><?= $company->getAttributeLabel('rs'); ?>:</th>
        <td><?= $company->rs; ?></td>
    </tr>
    <tr>
        <th><?= $company->getAttributeLabel('bank_name'); ?>:</th>
        <td><?= $company->bank_name; ?></td>
    </tr>
    <tr>
        <th><?= $company->getAttributeLabel('ks'); ?>:</th>
        <td><?= $company->ks; ?></td>
    </tr>
    <tr>
        <th><?= $company->getAttributeLabel('bik'); ?>:</th>
        <td><?= $company->bik; ?></td>
    </tr>

    <?php if ($company->company_type_id != \common\models\company\CompanyType::TYPE_IP): ?>
        <tr>
            <th>&nbsp;</th>
        </tr>

        <tr>
            <th><?= $company->chief_post_name; ?>:</th>
            <td><?= $company->getChiefFio(); ?></td>
        </tr>
        <tr>
            <th>Главный бухгалтер:</th>
            <td><?= $company->getChiefAccountantFio(); ?></td>
        </tr>
    <?php endif; ?>

    <tr>
        <th>&nbsp;</th>
    </tr>

    <tr>
        <th><?= $company->getAttributeLabel('phone'); ?>:</th>
        <td><?= $company->phone; ?></td>
    </tr>

</table>
