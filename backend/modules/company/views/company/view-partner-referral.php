<?php

use common\components\helpers\Html;
use common\models\Company;
use common\models\company\CompanyAffiliateLink;
use common\components\date\DateHelper;
use common\models\company\CompanyAffiliateSearch;
use common\models\service\ServiceModule;
use common\widgets\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\web\View;

/** @var $this yii\web\View */
/** @var Company $companyModel */
/** @var CompanyAffiliateSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */

$this->title = 'Реферальные ссылки';

?>

<div class="affiliate-link">
    <div class="col-6 text-right">
        <?= Html::button('<i class="fa fa-plus"></i><span class="pl-2">Добавить</span>',
            [
                'class' => 'button-regular button-regular_red button-width ml-auto create_affiliate_link',
                'data-company-id' => $companyModel->id,
            ]
        ); ?>
    </div>
    <div class="portlet box darkblue  mt-5 pt-5">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
        </div>
        <div class="portlet-body">
            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                'options' => [
                    'style' => 'overflow-x: auto;',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],

                'rowOptions' =>
                    function (CompanyAffiliateLink $data) {
                        return ['class' => ($data->company->blocked == Company::BLOCKED) ? 'danger' : ''];
                    },

                'columns' => [
                    [
                        'attribute' => 'creationDate',
                        'label' => 'Дата создания',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'value' => function (CompanyAffiliateLink $data) {
                            return DateHelper::format(
                                date(DateHelper::FORMAT_DATE, $data->created_at),
                                DateHelper::FORMAT_USER_DATE,
                                DateHelper::FORMAT_DATE
                            );
                        },
                    ],
                    [
                        'attribute' => 'product',
                        'label' => 'Продукт',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'value' => function (CompanyAffiliateLink $data) {
                            return ServiceModule::$serviceModuleLabel[$data->service_module_id];
                        },
                    ],
                    [
                        'attribute' => 'link',
                        'label' => 'Ссылка для ваших клиентов',
                        'headerOptions' => [
                            'width' => '40%',
                        ],
                        'format' => 'html',
                        'value' => function (CompanyAffiliateLink $data) {
                            $link = \Yii::$app->params['siteModules'][$data->service_module_id] . '?req=';

                            return Html::tag('span', $link . $data->link, [
                                'class' => 'clipboard-affiliate-link-id-' . $data->id,
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'countAffiliateCompanies',
                        'label' => 'Количество регистраций',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'html',
                        'value' => function (CompanyAffiliateLink $data) {
                            return $data->countAffiliateCompanies ?: 'Нет приглашенных клиентов';
                        },
                    ],
                    [
                        'attribute' => 'name',
                        'label' => 'Название',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'format' => 'html',
                        'value' => function (CompanyAffiliateLink $data) {
                            return $data->name ?: '-';
                        },
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{copy}',
                        'buttons' => [
                            'copy' => function ($url, $data) {
                                return Html::a('Копировать', '#copy', [
                                    'title' => 'Копировать',
                                    'class' => 'edit-affiliate-link-copy link pl-1 pr-1',
                                    'data' => [
                                        'affiliate-link-id' => $data['id']
                                    ],
                                ]);
                            },
                        ],
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{edit}',
                        'buttons' => [
                            'edit' => function ($url, $data) {
                                return Html::a('<i class="fa fa-pencil"></i>', '#edit', [
                                    'title' => 'Редактировать',
                                    'class' => 'update_affiliate_link link pl-1 pr-1 ' . (!$data['name'] ? 'hidden' : ''),
                                    'data' => [
                                        'affiliate-link-id' => $data['id'],
                                        'company-id' => $data['company_id'],
                                    ],
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'modal_update_link',
    'header' => '<h4>Добавить ссылку</h4>',
    'closeButton' => [
        'class' => 'modal-close close',
    ],
]); ?>

<?= $this->render('_viewPartials/affiliate/_edit_links_form', [
    'model' => (new CompanyAffiliateLink()),
    'products' => ServiceModule::$serviceModuleLabel,
]); ?>

<?php Modal::end(); ?>


<?php

$this->registerJs(<<<JS
    $(document).on('click', '.edit-affiliate-link-name', function(e){
        e.preventDefault();
        var id = $(this).data('affiliate-link-id'),
            name = $('.edit-affiliate-link-id-' + id),
            text = name.text();
        
        name
            .html('<input type="text" ' +
             ' class="edit-affiliate-link-name-' + id + '"' +
             ' value="' + text + '" />')
            .append('<input type="button" ' +
                '           value="Сохранить" ' +
                '           class="edit-affiliate-link-save" ' +
                '           data-affiliate-link-id="'+id+'" />');
        
    })
    
    $(document).on('click', '.edit-affiliate-link-save', function(e){
        e.preventDefault();
        var id = $(this).data('affiliate-link-id'),
            span = $('.edit-affiliate-link-id-' + id),
            name = $('.edit-affiliate-link-name-' + id),
            text = name.val();
        
        $.post('edit-affiliate-link', {'id': id, 'name': text}, function(data) {
            if ('true' === data.success) {
                span.html(text);
            }
        }, 'json')
    });

    $(document).on('click', '.edit-affiliate-link-copy', function(e) {
        e.preventDefault();
        
        var id = $(this).data('affiliate-link-id'),
            span = $('.clipboard-affiliate-link-id-' + id),
            textarea = $('<textarea id="clipboard"></textarea>'),
            body = $('body');
        
        textarea.html(span.text());
        body.append(textarea);

        textarea.select();

        var clipboard = document.execCommand('copy');

        $('#clipboard').remove();
    });
    
    $('.create_affiliate_link').on('click', function(e) {
        e.preventDefault();
        var company_id = $(this).data('company-id');
        
        $.pjax({
            url: 'create-custom-link',
            data: {'company_id': company_id},
            container: '#modal-item-pjax',
            push: false
        })
        
        $(document).on('pjax:success', function() {
            $('#modal_update_link').modal('show');
        });
    
    });
    
    $('.update_affiliate_link').on('click', function(e) {
        e.preventDefault();
    
        var id = $(this).data('affiliate-link-id');
        var company_id = $(this).data('company-id');

        $.pjax({
            url: 'update-affiliate-link',
            data: {'id': id, 'company_id': company_id},
            container: '#modal-item-pjax',
            push: false
        })
        
        $(document).on('pjax:success', function() {
            $('.modal-header h4').html('Редактировать ссылку');
            $('#modal_update_link').modal('show');
        });
        
    });
JS, View::POS_READY)

?>