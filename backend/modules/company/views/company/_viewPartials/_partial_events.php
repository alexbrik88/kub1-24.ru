<?php

use common\components\grid\GridView;
use backend\modules\company\models\CompanyFirstEventSearch;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;

/* @var $model common\models\Company*/

$searchModel = new CompanyFirstEventSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageSize = \frontend\components\PageSize::get('event-per-page');
?>

<div class="portlet box darkblue details">
    <div class="portlet-title">
        <div class="caption">Первые действия</div>
    </div>
    <div class="portlet-body">
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">

                <?php $pjax = Pjax::begin([
                    'id' => 'first-event-pjax',
                    'enablePushState' => false,
                    'enableReplaceState' => false,
                ]); ?>

                    <?= GridView::widget([
                        'id' => 'first-event-grid',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'formatter' => [
                            'class' => 'yii\i18n\Formatter',
                            'nullDisplay' => '---',
                        ],
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],

                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                            'pagination' => [
                                'pageSizeParam' => 'event-per-page',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout', [
                            'totalCount' => $dataProvider->totalCount,
                            'pageSizeParam' => 'event-per-page',
                        ]),

                        'columns' => [
                            'name',
                            'step',
                            'visit',
                            'created_at:datetime',
                        ],
                    ]); ?>

                <?php $pjax->end(); ?>
            </div>
        </div>
    </div>
</div>



