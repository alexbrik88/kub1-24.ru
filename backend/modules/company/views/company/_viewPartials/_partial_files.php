<?php
/* @var $model common\models\Company */
use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\models\Company;

/* @var $form yii\widgets\ActiveForm */
?>
    <style>
        .company_img img {
            height: 70px;
            width: 200px;
        }
    </style>
    <div class="row small-boxes">
        <div class="col-md-4">
            <div class="portlet box darkblue">
                <div class="portlet-title">
                    <div class="caption">Логотип компании</div>
                </div>
                <div class="portlet-body">
                    <div class="form-group">
                        <div class="company_img">
                            <div class="portlet align-center" id="company_logo">
                                <?php
                                $imgPath = $model->getImage('logoImage');
                                if (is_file($imgPath)) {
                                    echo EasyThumbnailImage::thumbnailImg($imgPath, 200, 200, EasyThumbnailImage::THUMBNAIL_INSET, [
                                        'style' => 'max-width: 100%; max-height: 100%;'
                                    ]);
                                } else {
                                    echo '<img src="" class="signature_image">';
                                }; ?>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <?= \yii\helpers\Html::activeFileInput($model, 'logoImage', [
                                'label' => false,
                                'class' => 'js_filetype',
                                'accept' => 'image/gif, image/jpeg, image/png, image/bmp, ',
                            ]); ?>

                            <?= \yii\helpers\Html::activeCheckbox($model, 'deleteLogoImage', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                            ]); ?>
                        </div>
                    </div>
                    <div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($imgPath);
                        if ($modificationDate) {
                            echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="portlet box darkblue">
                <div class="portlet-title">
                    <div class="caption">Печать компании</div>
                </div>
                <div class="portlet-body">
                    <div class="form-group">
                        <div class="company_img">
                            <div class="portlet align-center"
                                 id="company_print">
                                <?php
                                $imgPath = $model->getImage('printImage');
                                if (is_file($imgPath)) {
                                    echo EasyThumbnailImage::thumbnailImg($imgPath, 200, 200, EasyThumbnailImage::THUMBNAIL_INSET, [
                                        'style' => 'max-width: 100%; max-height: 100%;'
                                    ]);
                                } else {
                                    echo '<img src="" class="signature_image">';
                                }; ?>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <?= \yii\helpers\Html::activeFileInput($model, 'printImage', [
                                'label' => false,
                                'class' => 'js_filetype',
                                'accept' => 'image/gif, image/jpeg, image/png',
                            ]); ?>

                            <?= \yii\helpers\Html::activeCheckbox($model, 'deletePrintImage', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                            ]); ?>
                        </div>
                    </div>
                    <div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($imgPath);
                        if ($modificationDate) {
                            echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="portlet box darkblue">
                <div class="portlet-title">
                    <div class="caption">Подпись руководителя</div>
                </div>
                <div class="portlet-body">
                    <div class="form-group">
                        <div class="company_img">
                            <div class="portlet align-center"
                                 id="company_chief">
                                <?php
                                $imgPath = $model->getImage('chiefSignatureImage');
                                if (is_file($imgPath)) {
                                    echo EasyThumbnailImage::thumbnailImg($imgPath, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET, [
                                        'style' => 'max-width: 100%; max-height: 100%;'
                                    ]);
                                } else {
                                    echo '<img src="" class="signature_image">';
                                }; ?>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <?= \yii\helpers\Html::activeFileInput($model, 'chiefSignatureImage', [
                                'label' => false,
                                'class' => 'js_filetype',
                                'accept' => 'image/gif, image/jpeg, image/png',
                            ]); ?>

                            <?= \yii\helpers\Html::activeCheckbox($model, 'deleteChiefSignatureImage', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                            ]); ?>
                        </div>
                    </div>
                    <div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($imgPath);
                        if ($modificationDate) {
                            echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$this->registerJS(<<<JS
$('#company-logoimage').change(function () {
        var preview = document.querySelector('div[id=company_logo] img');
        var file    = document.querySelector('input[id=company-logoimage]').files[0];
        previewFile(preview, file);
});
$('#company-printimage').change(function () {
        var preview = document.querySelector('div[id=company_print] img');
        var file    = document.querySelector('input[id=company-printimage]').files[0];
        previewFile(preview, file);
});
$('#company-chiefsignatureimage').change(function () {
        var preview = document.querySelector('div[id=company_chief] img');
        var file    = document.querySelector('input[id=company-chiefsignatureimage]').files[0];
        previewFile(preview, file);
});
function previewFile(preview, file) {
    var reader  = new FileReader();

        reader.onloadend = function () {
            preview.src = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.src = "";
        }
}
JS
);