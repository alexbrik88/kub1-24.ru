<?php

/* @var $this yii\web\View */

use common\models\employee\Employee;
use common\models\EmployeeCompany;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $company \common\models\Company */
/* @var $searchModel common\models\employee\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$addUrl = Url::to([
    '/company/employee/create',
    'companyId' => $company->id,
    'return_url' => $return_url ?? null,
]);

$isRemoteEmployee = EmployeeCompany::find()->where([
    'employee_id' => ArrayHelper::getValue(Yii::$app->params, ['service', 'remote_employee_id']),
    'company_id' => $company->id,
    'is_working' => 1,
])->exists();
?>

<div class="portlet box d-flex align-items-center">
    <h3 class="page-title mb-0">
        Сотрудники
    </h3>
    <?php if ($isRemoteEmployee) : ?>
        <div id="remote_employee_off" class="ml-4 mr-3 d-flex align-items-end">
            <label class="rounded-switch m-0" for="activate_remote_employee" style="">
                <?= Html::checkbox('remote_employee', $isRemoteEmployee, [
                    'id' => 'activate_remote_employee',
                    'class' => 'switch',
                    'data-url' => Url::to([
                        '/company/employee/delete-remote',
                        'companyId' => $company->id,
                    ]),
                ]); ?>
                <span class="sliderr no-gray yes-yellow round"></span>
            </label>
            <span class="ml-3">Отключить доступ сотруднику технической поддержки</span>
        </div>
    <?php endif ?>
    <div class="btn-group ml-auto title-buttons">
        <a href="<?= $addUrl ?>" class="btn yellow">
            <i class="fa fa-plus"></i> ДОБАВИТЬ
        </a>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список сотрудников
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <?php Pjax::begin([
            'id' => 'employee-pjax',
            'enablePushState' => false,
            'enableReplaceState' => false,
            'linkSelector' => '#employee-pjax thead a, #employee-pjax .pagination a',
        ]); ?>

        <?php $dataProvider->sort->route = '/company/company/employees'; ?>
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

            'id' => 'employee-table',
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap',
            ],

            'headerRowOptions' => [
                'class' => 'heading',
            ],

            'options' => [
                'class' => 'overflow-x',
            ],

            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],

            'layout' => "{items}\n{pager}",

            'columns' => [
                [
                    'attribute' => 'fio',
                    'label' => 'Фамилия Имя Отчество',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $name = trim($data->fio);
                        if (empty($name)) {
                            $name = '(не задано)';
                        }

                        return Html::a($name, ['/company/employee/view', 'companyId' => $data->company_id, 'id' => $data->employee_id]);
                    },
                ],

                'position',

                [
                    'attribute' => 'employee_role_id',
                    'filter' => $searchModel->getEmployeeRoleArray($company->id),
                    'value' => 'employeeRole.name',
                ],

                [
                    'attribute' => 'is_owner',
                    'label' => 'Владелец',
                    'filter' => $searchModel->getOwnerArray(),
                    'format' => 'boolean',
                    'value' => function ($data) use ($company) {
                        return $data->employee_id == $company->owner_employee_id;
                    },
                ],

                [
                    'attribute' => 'is_working',
                    'label' => 'Статус',
                    'filter' => $searchModel->getEmployeeStatusArray(),
                    'value' => function ($data) {
                        return Employee::$status_message[$data->is_working];
                    },
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).on("change", "#activate_remote_employee", function () {
        if (!this.checked) {
            $.post($(this).data("url"), function (data) {
                if (data.deleted == 1) {
                    $("#remote_employee_off").remove();
                }
            });
        }
    });
</script>