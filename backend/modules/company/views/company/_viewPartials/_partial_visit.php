<?php

use common\components\grid\GridView;
use common\models\company\CompanyLastVisit;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;

/* @var $model common\models\Company*/

$dataProvider = new ActiveDataProvider([
    'query' => CompanyLastVisit::find()->andWhere([
        'company_id' => $model->id,
    ]),
    'pagination' => [
        'pageSize' => \frontend\components\PageSize::get('visit-per-page'),
    ],
    'sort' => [
        'defaultOrder' => [
            'ip' => SORT_ASC,
        ],
    ],
]);
?>

<div class="portlet box darkblue details">
    <div class="portlet-title">
        <div class="caption">Посещаемость</div>
    </div>
    <div class="portlet-body">
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">

                <?php $pjax = Pjax::begin([
                    'id' => 'company-visit-pjax',
                    'enablePushState' => false,
                    'enableReplaceState' => false,
                ]); ?>

                    <?= GridView::widget([
                        'id' => 'company-visit-grid',
                        'dataProvider' => $dataProvider,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],

                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                            'pagination' => [
                                'pageSizeParam' => 'visit-per-page',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout', [
                            'totalCount' => $dataProvider->totalCount,
                            'pageSizeParam' => 'visit-per-page',
                        ]),

                        'columns' => [
                            'ip',
                            'time:time',
                            'time:date',
                        ],
                    ]); ?>

                <?php $pjax->end(); ?>
            </div>
        </div>
    </div>
</div>
