<?php

use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\Company;
use common\models\service;
use frontend\modules\subscribe\forms\PaymentForm;

/* @var Company $model */
/* @var \yii\web\View $this */
/* @var \yii\data\ActiveDataProvider $paymentDataProvider */
/* @var \frontend\modules\subscribe\forms\PaymentSearch $paymentSearchModel */
/* @var service\Subscribe[] $actualSubscribes */

$this->title = 'Оплата сервиса';

$paymentMethodForm = new PaymentForm($model);

$actualSubscribes = service\SubscribeHelper::getPayedSubscriptions($model->id);
$expireDate = service\SubscribeHelper::getExpireDate($actualSubscribes);
$expireDays = service\SubscribeHelper::getExpireLeftDays($expireDate);

?>

<?= Yii::$app->session->getFlash('emptyCompany'); ?>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Продлить подписку
        </div>
        <div class="tools">
            <a href="javascript:;" class="expand" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body accounts-list" style="display: none;">
        <div class="subscribe-tariff-row row">
            <div class="col-md-9 col-sm-9">
                <div class="row">
                    <?php foreach (service\SubscribeTariff::find()->actual()->all() as $tariff): /* @var service\SubscribeTariff $tariff */ ?>
                        <?php $discount = $model->getDiscount($tariff->id); ?>
                        <div class="subscribe-tariff col-md-4 col-sm-4"
                             data-tariff-id="<?= $tariff->id; ?>"
                             data-total-cost="<?= $tariff->price; ?>"
                             data-discount="<?= $discount; ?>"
                            >
                            <div>
                                <button
                                    class="tariff btn btn-large subscribe-length but_weight"
                                    style="width: 100%;<?= $discount ? ' padding: 15px 0px;' : ''; ?>">
                                    <?= $tariff->tariffGroup->name ?><br/>
                                    на <?= $tariff->getTariffName(); ?>
                                    <br>
                                    <?php if ($discount) : ?>
                                        <span style="text-decoration: line-through;">
                                            <?= $price = $tariff->price / $tariff->duration_month ?>
                                            руб./месяц
                                        </span>
                                        <br>
                                        <span style="">
                                            <?= $price - $price * $discount / 100; ?>
                                            руб./месяц
                                        </span>
                                    <?php else : ?>
                                        <?= $tariff->price / $tariff->duration_month ?>
                                        руб./месяц
                                    <?php endif ?>
                                </button>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="subscribe-tariff col-md-3 col-sm-3" data-promo-code="1">
                <div>
                    <?= Html::button('Активировать <br> промокод', [
                        'class' => 'promo-code-tariff btn btn-large subscribe-length but_weight color-www',
                        'style' => 'width: 100%; height: 90px;',
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="subscribe-tariff-payment row">
            <div id="subscribe-tariff-block"
                 class="hide subscr-tariff-blk sum_payment_amount">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'id' => 'subscribe-tariff-form',
                    'action' => ['make-payment', 'id' => $model->id],
                ]); ?>

                <div class="col-sm-12">
                    <?= Html::activeHiddenInput($paymentMethodForm, 'tariffId', [
                        'id' => 'payment-method-tariff',
                    ]); ?>

                    <p>
                        <span style="display: inline-block; width: 125px;">Скидка %:</span>
                        <?= Html::activeTextInput($paymentMethodForm, 'adminDiscount', [
                            'style' => 'width: 100px; margin-left: 15px; font-size: 20px; padding: 0 0 0 5px; line-height: 30px;',
                            'type' => 'number',
                            'min'=> '0',
                            'max'=> '99.9999',
                            'step' => 'any',
                        ]); ?>
                    </p>
                    <p>
                        <span style="display: inline-block; width: 125px;">Сумма скидки:</span>
                        <span class="subscribe-discount font-bold btn-success sum_btn"></span>
                    </p>
                    <p>
                        <span style="display: inline-block; width: 125px;">Сумма к оплате:</span>
                        <span class="subscribe-tariff-price font-bold btn-success sum_btn"></span>
                    </p>

                    <p>Выбор способа платежа:</p>
                </div>


                <div class="">
                    <div class="col-md-9 col-sm-9">
                        <div class="row">
                            <?php $inputId = 'payment-type-' . service\PaymentType::TYPE_INVOICE; ?>
                            <div class="payment-type col-md-4 col-sm-4">
                                <div class="bg_payment">
                                    <div class="title">
                                        <?= \yii\bootstrap\Html::label('Выставить счёт', $inputId, [
                                            'class' => 'payment-type-choose btn btn-large btn-success',
                                            'data-id' => service\PaymentType::TYPE_INVOICE,
                                        ]); ?>
                                        <?= Html::radio(Html::getInputName($paymentMethodForm, 'paymentTypeId'), true, [
                                            'id' => $inputId,
                                            'value' => service\PaymentType::TYPE_INVOICE,
                                            'class' => 'hide',
                                        ]); ?>
                                    </div>

                                    <div class="payment-type_text_container">
                                        Счёт будет выслан вам на e-mail

                                        <?= $form->field($paymentMethodForm, 'createInvoice')->checkbox(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php $form->end(); ?>
            </div>

            <div id="promo-code-block" class="hide">

                <?= $this->render('_partial_promocode_form', [
                    'promoCodeForm' => new \frontend\modules\subscribe\forms\PromoCodeForm(),
                    'company' => $model,
                ]); ?>

            </div>
        </div>

    </div>
</div>

<?php
$js = <<<JS
    var formDiscount = $('#paymentform-admindiscount').val();
    var tariffPrice = 0;
    var companyDiscount = 0;
    var totalDiscount = 0;
    var totalPrice = 0;
    var form = $('#subscribe-tariff-form');
    var tariff = $('#payment-method-tariff');

    var subscribeTariffBlock = $('#subscribe-tariff-block');
    var promocodeBlock = $('#promo-code-block');

    var calculatePrice = function() {
        totalPrice = Math.round((tariffPrice - tariffPrice * Math.max(formDiscount, companyDiscount) / 100) * 100) / 100;
        totalDiscount = Math.round((tariffPrice - totalPrice) * 100) / 100;
        $('.subscribe-discount').text(totalDiscount + ' руб.');
        $('.subscribe-tariff-price').text(totalPrice + ' руб.');
        return totalPrice;
    }

    $(document).on('click', '.subscribe-tariff .tariff', function (e) {
        var subscribeTariff = $(this).parents('.subscribe-tariff');
        tariffPrice = subscribeTariff.data('total-cost');
        companyDiscount = subscribeTariff.data('discount');
        calculatePrice();

        subscribeTariffBlock.removeClass('hide');
        promocodeBlock.addClass('hide');

        tariff.val(subscribeTariff.data('tariff-id'));
    });

    $(document).on('change', '#paymentform-admindiscount', function () {
        formDiscount = $(this).val();
        console.log(formDiscount);
        calculatePrice();
    });

    $(document).on('click', '.subscribe-tariff .promo-code-tariff', function (e) {
        subscribeTariffBlock.addClass('hide');
        promocodeBlock.removeClass('hide');
    });

    $(document).on('click', '.payment-type-choose', function (e) {
        $(this.form).trigger('submit');
    });

    /*$(document).on('click', '.but_weight', function (e) {
        $(this).toggleClass('bg_but_color');
    });*/

    $(document).ready(function() {
        $( ".but_weight" ).bind("click", function() {
            if(!$(this).hasClass('bg_but_color', 'sp_weight')) {
                remove_selected_2();
                $(this).addClass('bg_but_color');
                $(this).parent().parent().addClass('sp_weight');
            }
        });
    });

    function remove_selected_2() {
        $('.but_weight').removeClass('bg_but_color');
        $('.subscribe-tariff').removeClass('sp_weight');
    }
JS;
$this->registerJs($js);
?>