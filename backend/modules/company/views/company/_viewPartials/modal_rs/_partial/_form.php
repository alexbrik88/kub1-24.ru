<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.08.2016
 * Time: 5:24
 */

use common\models\company\CheckingAccountant;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Html;
use common\components\widgets\BikTypeahead;
use common\models\Company;

/* @var $checkingAccountant CheckingAccountant */
/* @var $checkingAccountantForm yii\bootstrap\ActiveForm */
/* @var $company Company */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-4 control-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line-product',
    ],
    'hintOptions' => [
        'tag' => 'small',
        'class' => 'text-muted',
    ],
    'horizontalCssClasses' => [
        'offset' => 'col-md-offset-4',
        'hint' => 'col-md-8',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$checkingAccountantForm = ActiveForm::begin([
    'action' => $checkingAccountant->isNewRecord ? Url::to(['create-checking-accountant', 'companyId' => $company->id]) : Url::to(['update-checking-accountant', 'id' => $checkingAccountant->id, 'companyId' => $company->id]),
    'options' => [
        'class' => 'form-horizontal',
        'id' => 'form-checking-accountant-' . $checkingAccountant->id,
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
]);
?>
<div class="form-body">
    <?= $checkingAccountantForm->field($checkingAccountant, 'bik', $config)
        ->widget(BikTypeahead::classname(), [
            'remoteUrl' => Url::to(['/dictionary/bik']),
            'related' => [
                '#checkingaccountant-bank_name-' . $checkingAccountant->id => 'name',
                '#checkingaccountant-ks-' . $checkingAccountant->id => 'ks',
            ],
            'options' => [
                'id' => 'checkingaccountant-bik-' . $checkingAccountant->id,
            ],
        ]); ?>

    <?= $checkingAccountantForm->field($checkingAccountant, 'bank_name', $config)->textInput([
        'disabled' => '',
        'id' => 'checkingaccountant-bank_name-' . $checkingAccountant->id,
    ]); ?>

    <?= $checkingAccountantForm->field($checkingAccountant, 'ks', $config)->textInput([
        'disabled' => '',
        'id' => 'checkingaccountant-ks-' . $checkingAccountant->id,
    ]); ?>

    <?= $checkingAccountantForm->field($checkingAccountant, 'rs', $config)->textInput([
        'id' => 'checkingaccountant-rs-' . $checkingAccountant->id,
    ]); ?>

    <?php if ($checkingAccountant->type !== CheckingAccountant::TYPE_MAIN) : ?>
        <?= $checkingAccountantForm->field($checkingAccountant, 'isMain', $config)->checkbox([], false); ?>

        <?php if (!$checkingAccountant->isNewRecord && count($company->checkingAccountants) !== 1) : ?>
            <?= $checkingAccountantForm->field($checkingAccountant, 'isClosed', $config)->checkbox([], false) ?>
        <?php endif ?>
    <?php endif; ?>

    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                ]); ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg back',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php $checkingAccountantForm->end(); ?>
