<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.07.2017
 * Time: 17:45
 */

use common\components\helpers\Html;
use common\components\ImageHelper;
use common\components\date\DateHelper;
use backend\modules\company\models\AffiliateSearch;
use yii\widgets\Pjax;
use common\components\grid\GridView;
use common\models\service\Subscribe;
use yii\helpers\Url;

/* @var common\models\Company $model
 * @var $affiliateSearchModel AffiliateSearch
 * @var $affiliateDataProvider yii\data\ActiveDataProvider
 */

?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Партнерская программа
        </div>
        <div class="tools">
            <a href="javascript:;" class="expand" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body" style="display: none;">
        <div class="affiliate-program-block-admin">
            <table
                class="table table-striped table-bordered table-hover dataTable customers_table overfl_text_hid"
                role="grid">
                <tbody>
                <tr>
                    <td width="50%" style="padding-right: 8px!important;">
                        <ul class="affiliate-program-list p-l-0">
                            <li>
                                Партнерская ссылка:
                            </li>
                            <li>
                                <?php if ($model->affiliate_link): ?>
                                    <?= Html::a(Yii::$app->params['serviceSite'] . 'registration?req=' . $model->affiliate_link,
                                        Yii::$app->params['serviceSite'] . 'registration?req=' . $model->affiliate_link, [
                                            'target' => '_blank',
                                            'class' => 'affiliate_invite',
                                        ]); ?>
                                    <?= ImageHelper::getThumb('img/copy.png', [20, 20], [
                                        'class' => 'copy-affiliate-link',
                                        'title' => 'Скопировать ссылку',
                                    ]); ?>
                                <?php else: ?>
                                    ---
                                <?php endif; ?>
                            </li>
                            <li>
                                Активирована: <?= $model->affiliate_link_created_at ?
                                    date(DateHelper::FORMAT_USER_DATE, $model->affiliate_link_created_at) : '---'; ?>
                            </li>
                        </ul>
                    </td>
                    <td width="50%" style="padding-left: 8px!important;">
                        <ul class="affiliate-program-list p-l-0">
                            <li>
                                Вознаграждение: <?= $model->affiliate_sum; ?>
                                руб.
                            </li>
                            <li>
                                Оплата за
                                КУБ: <?= $model->payed_reward_for_kub ?> руб.
                            </li>
                            <li>
                                Вывод: <?= $model->payed_reward_for_kub + $model->payed_reward_for_rs + $model->payed_reward_for_card ?>
                                руб.
                            </li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <?php $pjax = Pjax::begin([
            'id' => 'affiliate-pjax',
            'enablePushState' => false,
            'enableReplaceState' => false,
            'linkSelector' => '#affiliate-pjax thead a, #affiliate-pjax .pagination a',
        ]); ?>
        <?= GridView::widget([
            'dataProvider' => $affiliateDataProvider,
            'filterModel' => $affiliateSearchModel,
            'id' => 'affiliate-table',
            'layout' => "{items}\n{pager}",
            'options' => [
                'class' => 'overflow-x',
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable documents_table overfl_text_hid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],

            'columns' => [
                [
                    'attribute' => 'company.created_at',
                    'label' => 'Дата регистрации',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'value' => function (Subscribe $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->company->created_at);
                    },
                ],
                [
                    'attribute' => 'company.name_short',
                    'label' => 'Компания',
                    'format' => 'raw',
                    'value' => function (Subscribe $model) {
                        return Html::a($model->company->getShortName(), Url::to(['view', 'id' => $model->company->id]));
                    },
                ],
                [
                    'attribute' => 'service_payment.sum',
                    'label' => 'Сумма платежа',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'value' => function (Subscribe $model) {
                        return $model->payment->sum . ' руб.';
                    },
                ],
                [
                    'attribute' => 'service_payment.payment_date',
                    'label' => 'Дата платежа',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'value' => function (Subscribe $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->payment->payment_date);
                    },
                ],
                [
                    'attribute' => 'reward',
                    'label' => 'Вознаграждение',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'value' => function (Subscribe $model) {
                        return $model->reward . ' руб.';
                    },
                ],
            ],
        ]); ?>
        <?php $pjax->end(); ?>
    </div>
</div>
