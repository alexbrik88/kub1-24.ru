<?php

use common\models\Company;
use common\components\date\DateHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\bootstrap\ActiveForm */

$inputConfig = $inputConfigRequired = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfig2 = [
    'labelOptions' => [
        'class' => 'control-label col-md-5',
    ],
    'inputOptions' => [
        'class' => 'form-control field-width',
    ],
];
?>

<div class="portlet box darkblue details">
    <div class="portlet-title">
        <div class="caption">Реквизиты</div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 portlet-body_ins">
                <?= $form->field($model, 'egrip', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okpo', $inputConfig)->textInput([
                    'maxlength' => true,
                    'id' => 'company-ip-okpo',
                ]); ?>
                <?= $form->field($model, 'address_legal', $inputConfig)->widget(MaskedInput::class, [
                    'mask' => '9{6}, ~{1,245}',
                    'definitions' => [
                        '~' => [
                            'validator' => '[ 0-9A-zА-я.,/-]',
                            'cardinality' => 1,
                        ],
                    ],
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Индекс, Адрес',
                    ],
                ]); ?>
                <?= $form->field($model, 'address_actual', $inputConfig)->widget(MaskedInput::class, [
                    'mask' => '9{6}, ~{1,245}',
                    'definitions' => [
                        '~' => [
                            'validator' => '[ 0-9A-Za-z.,/-]',
                            'cardinality' => 1,
                        ],
                    ],
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Индекс, Адрес',
                    ],
                ]); ?>
                <?= $form->field($model, 'okved', $inputConfig2)->textInput([
                    'maxlength' => true,
                    'id' => 'company-ip-okved',
                ]); ?>
            </div>
            <div class="col-md-6 left-column form-horizontal form__ins-2 row">
                <?= $form->field($model, 'oktmo', $inputConfig2)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'fss', $inputConfig2)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'pfr_ip', $inputConfig2)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'pfr_employer', $inputConfig2)->textInput([
                    'maxlength' => true,
                ]); ?>

                <?php $model->tax_authority_registration_date = DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                <?= $form->field($model, 'tax_authority_registration_date', array_merge($inputConfig2, [
                    'options' => [
                        'class' => 'form-group',
                    ],
                ]))->textInput([
                    'class' => 'form-control date-picker m_b',
                ]); ?>
            </div>
        </div>
    </div>
</div>
