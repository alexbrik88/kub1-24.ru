<?php

use common\components\helpers\ArrayHelper;
use common\models\service;

/** @var common\models\Company $model */
/** @var $subscribeSearchModel \backend\modules\company\models\SubscribeSearch */
/** @var $subscribeDataProvider yii\data\ActiveDataProvider */
?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption capt_mini">
            Профиль компании (заполнен на <?= \common\models\CompanyHelper::getProfileFullness($model); ?>%)
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">

                <?= \yii\helpers\Html::a('Визитка компании', \yii\helpers\Url::to(['company/visit-card', 'id' => $model->id]), [
                    'style' => 'margin-left: 0px;!important',
                    'class' => 'caption promo-org',
                ]); ?>
                <p><b>Система налогообложения:</b> <?= $model->companyTaxationType->name; ?></p>
                <p><b>Юридический адрес:</b> <?= $model->getAddressLegalFull(); ?></p>
                <p><b>ИНН:</b> <?= $model->inn; ?></p>
            </div>
            <div class="col-md-6">
                <p>
                    <b>Руководитель (для документов):</b>
                    <?= join(', ', array_filter([
                        $model->chief_post_name,
                        $model->getChiefFio(),
                    ])); ?>
                </p>
                <p><b>Телефон:</b> <?= $model->phone; ?></p>
                <p><b>Электронная почта:</b> <?= $model->email; ?></p>
            </div>
        </div>
    </div>
</div>
