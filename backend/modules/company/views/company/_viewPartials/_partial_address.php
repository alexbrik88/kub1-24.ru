<?php
use common\models\address;
use common\models\dictionary;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $prefix string */

$inputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-6',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputThinConfig = [
    'options' => [
        'class' => '',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}\n{hint}",
];

$remoteUrl = Url::to(['dictionary/address']);
?>


<?php echo $form->field($model, $prefix . '_city', $inputConfig)
    ->label('Город (населённый пункт)')->widget(\common\components\widgets\AddressTypeahead::classname(), [
        'remoteUrl' => $remoteUrl,
        'options' => [
            'data' => [
                'type' => dictionary\address\AddressDictionary::SEARCH_CITY,
            ],
        ],
    ]); ?>
<?php echo $form->field($model, $prefix . '_street', $inputConfig)
    ->label('Улица')->widget(\common\components\widgets\AddressTypeahead::classname(), [
        'remoteUrl' => $remoteUrl,
        'related' => [
            'city' => '#' . Html::getInputId($model, $prefix . '_city'),
        ],
        'options' => [
            'data' => [
                'type' => dictionary\address\AddressDictionary::SEARCH_STREET,
            ],
        ],
    ]); ?>


<div class="form-group required">
    <div class="col-md-6">
        <?php echo $form->field($model, $prefix . '_house_type_id', $inputThinConfig)
            ->label(false)->dropDownList(
                ArrayHelper::map(address\AddressHouseType::find()->all(), 'id', 'name')
            ); ?>
    </div>
    <div class="col-md-3">
        <?php echo $form->field($model, $prefix . '_house', $inputThinConfig)
            ->label(false)->textInput([
                'maxlength' => true,
            ]); ?>
    </div>
</div>
<div class="form-group required">
    <div class="col-md-6">
        <?php echo $form->field($model, $prefix . '_housing_type_id', $inputThinConfig)
            ->label(false)->dropDownList(
                ArrayHelper::map(address\AddressHousingType::find()->all(), 'id', 'name')
            ); ?>
    </div>
    <div class="col-md-3">
        <?php echo $form->field($model, $prefix . '_housing', $inputThinConfig)
            ->label(false)->textInput([
                'maxlength' => true,
            ]); ?>
    </div>
</div>
<div class="form-group required">
    <div class="col-md-6">
        <?php echo $form->field($model, $prefix . '_apartment_type_id', $inputThinConfig)
            ->label(false)->dropDownList(
                ArrayHelper::map(address\AddressApartmentType::find()->all(), 'id', 'name')
            ); ?>
    </div>
    <div class="col-md-3">
        <?php echo $form->field($model, $prefix . '_apartment', $inputThinConfig)
            ->label(false)->textInput([
                'maxlength' => true,
            ]); ?>
    </div>
</div>


<?php echo $form->field($model, $prefix . '_postcode', $inputConfig)
    ->label('Почтовый индекс')->widget(\common\components\widgets\AddressTypeahead::classname(), [
        'remoteUrl' => $remoteUrl,
        'related' => [
            dictionary\address\AddressDictionary::SEARCH_CITY => '#' . Html::getInputId($model, $prefix . '_city'),
            dictionary\address\AddressDictionary::SEARCH_STREET => '#' . Html::getInputId($model, $prefix . '_street'),
        ],
        'options' => [
            'data' => [
                'type' => dictionary\address\AddressDictionary::SEARCH_POSTCODE,
            ],
        ],
        'display' => 'postalcode',
        'pluginOptions' => [
            'minLength' => 0,
        ],
    ]); ?>
