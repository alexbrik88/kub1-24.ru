<?php

use common\models\company\CompanyType;

$ip = CompanyType::TYPE_IP;
$ooo = CompanyType::TYPE_OOO;
$zao = CompanyType::TYPE_ZAO;
$pao = CompanyType::TYPE_PAO;
$oao = CompanyType::TYPE_OAO;

$house = \common\models\address\AddressHouseType::TYPE_HOUSE;
$flat = \common\models\address\AddressApartmentType::TYPE_APARTMENT;

$this->registerJs(<<<JS

var companyType = {
    'ИП' : $ip,
    'ООО' : $ooo,
    'ЗАО' : $zao,
    'ПАО' : $pao,
    'ОАО' : $oao,
};

    if ($('[name="Company[company_type_id]"]').val() == $ip || $('[name="Company[company_type_id]"]').val() == '0') {
        $('.ooo_head').hide();
        $('.ooo_props').hide();
        $('.ooo_head input').prop("disabled", true);
        $('.ooo_props input').prop("disabled", true);
        $('[id = company-name_short]').prop('disabled', true);
        $('[id = company-name_full]').prop('disabled', true);
    } else {
        $('.ip_fio').hide();
        $('.ip_props').hide();
        $('.ip_fio input').prop("disabled", true);
        $('.ip_props input').prop("disabled", true);
    }
    $('[name="Company[company_type_id]"]').change(function () {
        var type = $(this).val();

        if (type == $ip || type == '0') {
            $('.ooo_head').hide();
            $('.ooo_props').hide();
            $('.ip_fio').show();
            $('.ip_props').show();
            $('[id = company-name_short]').prop('disabled', true);
            $('[id = company-name_full]').prop('disabled', true);
            $('.ooo_head input').prop("disabled", true);
            $('.ooo_props input').prop("disabled", true);
            $('.ip_fio input').prop("disabled", false);
            $('.ip_props input').prop("disabled", false);
        } else if (type == $ooo || type == $zao || type == $pao || type == $oao) {
            $('.ip_fio').hide();
            $('.ip_props').hide();
            $('.ooo_head').show();
            $('.ooo_props').show();
            $('[id = company-name_short]').prop('disabled', false);
            $('[id = company-name_full]').prop('disabled', false);
            $('.ooo_head input').prop("disabled", false);
            $('.ooo_props input').prop("disabled", false);
            $('.ip_fio input').prop("disabled", true);
            $('.ip_props input').prop("disabled", true);
        }
    });

    $('#company-inn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            $('#company-inn').val(suggestion.data.inn);

            if (suggestion.data.name !== null) {
                $('#company-name_short').val(suggestion.data.name.short);
                $('#company-name_full').val(suggestion.data.name.full);
            }

            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                var index = suggestion.data.address.data.postal_code;
                var address = suggestion.data.address.value;
                $('#company-address_legal').val(index + ', ' + address);
                $('#company-address_actual').val(index + ', ' + address);
            } else {
                $('#company-address_legal').val('000000, ' + suggestion.data.address.value);
                $('#company-address_actual').val('000000, ' + suggestion.data.address.value);
            }

            if (suggestion.data.address.data !== null) {
                $('#company-oktmo').val(suggestion.data.address.data.oktmo);
            }

            if (companyType[suggestion.data.opf.short] !== undefined) {
                var companyTypeId = $('[name="Company[company_type_id]"][value=' + companyType[suggestion.data.opf.short] + ']');
                companyTypeId.click();
                companyTypeId.parent().attr('class', 'checked');
                for (var i = 1; i <= 4; i++) {
                    if (i !== companyType[suggestion.data.opf.short]) {
                        $('[name="Company[company_type_id]"][value=' + i + ']').parent().removeAttr('class');
                    }
                }
            }

            if (companyType[suggestion.data.opf.short] == $ip) {
                var ipNameFull = suggestion.data.name.full;
                var ipLastName = '';
                var ipFirstName = '';
                var ipPatronymic = '';
                var endIpLastNamePlace = ipNameFull.indexOf(' ');
                for (var i = 0; i < endIpLastNamePlace; i++) {
                    ipLastName += ipNameFull[i];
                }
                var endIpNamePlace = ipNameFull.indexOf(' ', endIpLastNamePlace+1);
                for (var i = endIpLastNamePlace+1; i < endIpNamePlace; i++) {
                    ipFirstName += ipNameFull[i];
                }
                for (var i = endIpNamePlace+1; i < ipNameFull.length; i++) {
                    ipPatronymic += ipNameFull[i];
                }
                $('#company-ip_lastname').val(ipLastName);
                $('#company-ip_firstname').val(ipFirstName);
                $('#company-ip_patronymic').val(ipPatronymic);
                $('#company-okpo').val(suggestion.data.okpo);
                $('#company-okved').val(suggestion.data.okved);

            } else {
                var nameFull = suggestion.data.management.name;
                var lastName = '';
                var firstName = '';
                var patronymic = '';
                var endLastNamePlace = nameFull.indexOf(' ');
                for (var i = 0; i < endLastNamePlace; i++) {
                    lastName += nameFull[i];
                }
                var endNamePlace = nameFull.indexOf(' ', endLastNamePlace+1);
                for (var i = endLastNamePlace+1; i < endNamePlace; i++) {
                    firstName += nameFull[i];
                }
                for (var i = endNamePlace+1; i < nameFull.length; i++) {
                    patronymic += nameFull[i];
                }
                $('#company-company_type_id').val(companyType[suggestion.data.opf.short]);
                $('#company-chief_post_name').val(suggestion.data.management.post);
                $('#company-chief_lastname').val(lastName);
                $('#company-chief_firstname').val(firstName);
                $('#company-chief_patronymic').val(patronymic);
                $('#company-okpo').val(suggestion.data.okpo);
                $('#company-okved').val(suggestion.data.okved);
                $('#company-kpp').val(suggestion.data.kpp);
                $('#company-ogrn').val(suggestion.data.ogrn);
            }
        }
    });
JS
);