<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.08.2016
 * Time: 8:30
 */

use common\models\bank\BankingParams;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use common\components\grid\GridView;
use backend\models\Bank;
use common\components\ImageHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;

/* @var $model common\models\Company*/

?>
<div class="portlet box darkblue details">
    <div class="portlet-title">
        <div class="caption">Расчетные счета</div>
    </div>
    <div class="portlet-body">
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">

                <?php $pjax = Pjax::begin([
                    'id' => 'checking-accountant-pjax',
                    'enablePushState' => false,
                    'enableReplaceState' => false,
                ]); ?>

                    <?= GridView::widget([
                        'dataProvider' => new ActiveDataProvider([
                            'query' => $model->getCheckingAccountants(),
                            'sort' => [
                                'defaultOrder' => [
                                    'type' => SORT_ASC,
                                    'bank_name' => SORT_ASC
                                ],
                            ],
                        ]),
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                            'id' => 'datatable_ajax',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],

                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",

                        'columns' => [
                            [
                                'attribute' => 'bank_name',
                                'headerOptions' => [
                                    'width' => '25%',
                                ],
                            ],
                            [
                                'attribute' => 'has_integration',
                                'headerOptions' => [
                                    'width' => '15%',
                                ],
                                'label' => 'Интеграция с банком',
                                'format' => 'raw',
                                'value' => function (CheckingAccountant $model) {

                                    foreach (Banking::$modelClassArray as $banking) {
                                        if (in_array($model->bik, $banking::$bikList)) {
                                            if (BankingParams::find()
                                                ->andWhere(['bank_alias' => $banking::ALIAS])
                                                ->andWhere(['param_name' => 'access_token'])
                                                ->andWhere(['company_id' => $model->company_id])
                                                ->count())

                                                return 'Да';
                                        }
                                    }

                                    return 'Нет';
                                }
                            ],
                            [
                                'attribute' => 'bik',
                                'headerOptions' => [
                                    'width' => '15%',
                                ],
                            ],
                            [
                                'attribute' => 'ks',
                                'label' => 'К/с',
                                'headerOptions' => [
                                    'width' => '20%',
                                ],
                            ],
                            [
                                'attribute' => 'rs',
                                'label' => 'Р/с',
                                'headerOptions' => [
                                    'width' => '20%',
                                ],
                            ],
                            [
                                'attribute' => 'type',
                                'headerOptions' => [
                                    'width' => '15%',
                                ],
                                'format' => 'raw',
                                'value' => function (CheckingAccountant $model) {
                                    return $model->typeText[$model->type];
                                },
                            ],
                        ],
                    ]); ?>

                <?php $pjax->end(); ?>
            </div>
        </div>
    </div>
</div>



