<?php

use common\models\Company;
use common\components\date\DateHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\bootstrap\ActiveForm */

$inputConfig = $inputConfigRequired = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$fioInitialsConfig = [
    'options' => [
        'class' => 'form-group required',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label',
    ],
    'inputOptions' => [
        'class' => 'form-control field-width initial-field',
    ],
];
?>

<div class="edit-profile__info">
    <div class="portlet-body box-fio">
        <?= $form->field($model, 'ip_lastname', $inputConfigRequired)->label('Фамилия')->textInput(); ?>
        <div class="row">
            <div class="column">
                <?= $form->field($model, 'ip_firstname', $inputConfigRequired)->label('Имя')->textInput(); ?>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <?= $form->field($model, 'ip_patronymic', $inputConfigRequired)->label('Отчество')->textInput(); ?>
            </div>
        </div>
    </div>
</div>
