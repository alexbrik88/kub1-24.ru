<?php

use common\models\Company;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */
/* @var $searchModel CheckingAccountantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $checkingAccountant CheckingAccountant */

$inputConfig = $inputConfigRequired = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigRequired['options']['class'] = 'form-group required';

$inputListConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-8 label-width',
    ],
];
$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-8 label-width',
    ],
    'inputOptions' => [
        'class' => 'form-control field-width field-w',
    ],

];

$isOldKubTheme = ArrayHelper::getValue($model, ['ownerEmployee', 'is_old_kub_theme'], 0);

$companyTypes = ArrayHelper::map(CompanyType::find()->inCompany()->all(), 'id', 'name_short');
$companyTypes[0] = 'Самозанятый';

$cancelUpl = Yii::$app->request->get('return_url') ?: (
    Url::to($model->isNewRecord ? ['/company/company/index'] : ['/company/company/view', 'id' => $model->id])
);

$this->registerJs(<<<JS
$(document).on("change", "#company-company_type_id", function() {
    var form = this.form;
    $("#company-self_employed", form).val(this.value == '0' ? '1' : '0');
});
JS
);
?>
<div class="form-body form-horizontal">
    <div class="help-article-form">
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
        <?= Html::activeHiddenInput($model, 'self_employed') ?>
        <div class="edit-profile__info">
            <?= $form->field($model, 'inn', array_merge($textInputConfig, [
                'wrapperOptions' => [
                    'class' => 'col-md-6',
                ],
                'inputOptions' => [
                    'class' => 'form-control m-l-n',
                    'placeholder' => 'Автозаполнение по ИНН',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
            ]))->textInput([
                'maxlength' => true,
            ]); ?>
            <?php $this->render('_company_inn_api'); ?>

            <?= $form->field($model, 'company_type_id', array_merge($textInputConfig, [
                'wrapperOptions' => [
                    'class' => 'col-md-6',
                ],
                'inputOptions' => [
                    'class' => 'form-control m-l-n',
                    'placeholder' => 'Автозаполнение по ИНН',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
            ]))->dropDownList($companyTypes); ?>
        </div>


        <?= $form->field($model, 'new_template_order', [
            'labelOptions' => [
                'class' => 'control-label col-md-4 label-width lab_width_inline',
            ],
            'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        ])->label()->radioList(['0' => 'старый', '1' => 'новый'], [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label',
                    Html::radio($name, $checked, ['value' => $value]) . $label,
                    [
                        'class' => 'radio-inline p-o radio-padding',
                        'id' => 'company_type',
                    ]);
            },
        ]); ?>

        <div class="form-group">
            <label class="control-label col-md-4 label-width lab_width_inline">Тема КУБ</label>
            <div>
                <label class="radio-inline p-o radio-padding"><input type="radio" name="Employee[is_old_kub_theme]" value="1" <?= $isOldKubTheme ? 'checked':'' ?>>старая</label>
                <label class="radio-inline p-o radio-padding"><input type="radio" name="Employee[is_old_kub_theme]" value="0" <?= !$isOldKubTheme ? 'checked':'' ?>>новая</label>
            </div>
        </div>

        <div class="edit-profile__info">
            <div class="ip_fio">
                <?= $this->render('_partial_ip_fio', [
                    'model' => $model,
                    'form' => $form,
                ]); ?>
            </div>

            <?= $this->render('_partial_description', [
                'model' => $model,
                'form' => $form,
            ]); ?>
        </div>

        <?php
        if (!$model->isNewRecord) {
            echo $this->render('_partial_checking_account', [
                'model' => $model,
                'form' => $form,
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'company' => $model,
            ]);
        }
        ?>

        <div class="ooo_head">
            <?= $this->render('_partial_ooo_head', [
                'model' => $model,
                'form' => $form,
            ]); ?>
        </div>

        <div class="ooo_props">
            <?= $this->render('_partial_ooo_props', [
                'model' => $model,
                'form' => $form,
            ]); ?>
        </div>

        <div class="ip_props">
            <?= $this->render('_partial_ip_props', [
                'model' => $model,
                'form' => $form,
            ]); ?>
        </div>

        <?php
        if (!$model->isNewRecord) {
            echo $this->render('_partial_files', [
                'model' => $model,
                'form' => $form,
            ]);
        }
        ?>

        <div class="form-actions">
            <div class="row action-buttons" id="buttons-fixed">
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    <?= Html::submitButton('Сохранить', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ]); ?>
                    <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Сохранить',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    <a href="<?= $cancelUpl ?>" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">
                        Отменитьqq
                    </a>
                    <a href="<?= $cancelUpl ?>" class="btn darkblue widthe-100 hidden-lg" title="Отменить">
                        <i class="fa fa-reply fa-2x"></i>
                    </a>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
if (!$model->isNewRecord) {
    echo $this->render('modal_rs/_create', [
        'checkingAccountant' => $checkingAccountant,
        'company' => $model,
    ]);
    foreach ($model->checkingAccountants as $checkingAccountant) {
        echo $this->render('modal_rs/_update', [
            'checkingAccountant' => $checkingAccountant,
            'company' => $model,
        ]);
    };
}
?>