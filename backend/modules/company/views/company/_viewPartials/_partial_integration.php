<?php

use common\components\IntegrationCount;

/* @var $this \yii\web\View */
/* @var $model \common\models\Company */

$helper = new \common\components\Integrations(null, null, $model);

$data = $helper->getIntegrationsTimeAll();
?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">Интеграции</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table class="table table-bordered table-hover"role="grid">
                <tbody>
                <tr>
                    <th>Партнер</th>
                    <?php foreach ($data as $item) : ?>
                        <td><?= $item['name'] ?></td>
                    <?php endforeach ?>
                </tr>
                <tr>
                    <th>Дата активации</th>
                    <?php foreach ($data as $item) : ?>
                        <td><?= $item['time'] ? date_create($item['time'])->format('d.m.Y') : '---'; ?></td>
                    <?php endforeach ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


