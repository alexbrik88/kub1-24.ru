<?php
/* @var \yii\web\View $this */
use backend\modules\company\models\CompanyValue;

?>


<div class="row">
    <div class="col-md-12">
        <?php foreach ([
                           [
                               'color' => 'yellow',
                               'count' => CompanyValue::findRegistered(),
                               'description' => 'Зарегистрировано',
                           ],
                           [
                               'color' => 'red',
                               'count' => CompanyValue::findBlocked(),
                               'description' => 'Заблокировано',
                           ],
                           [
                               'color' => 'blue',
                               'count' => CompanyValue::findTrialSubscribed(),
                               'description' => '14 дней бесплатно',
                           ],
                           [
                               'color' => 'green',
                               'count' => CompanyValue::findActive(),
                               'description' => 'Активно',
                           ],
                       ] as $conf): ?>
            <div class="col-md-3">
                <div class="dashboard-stat <?= $conf['color']; ?>">
                    <div class="visual"><i class="fa fa-comments"></i></div>
                    <div class="details">
                        <div class="number fontSizeSmall"><?= $conf['count']; ?></div>
                        <div class="desc"><?= $conf['description']; ?></div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
