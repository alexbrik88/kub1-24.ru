<?php

use common\components\date\DateHelper;
use common\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */

$inputConfig = $inputConfigRequired = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigRequired['options']['class'] = 'form-group required';
?>

<div class="portlet box darkblue details">
    <div class="portlet-title">
        <div class="caption">Реквизиты</div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'ogrn', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'kpp', $inputConfigRequired)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okpo', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'address_legal', $inputConfig)->widget(MaskedInput::class, [
                    'mask' => '9{6}, ~{1,245}',
                    'definitions' => [
                        '~' => [
                            'validator' => '[ 0-9A-zА-я.,/-]',
                            'cardinality' => 1,
                        ],
                    ],
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Индекс, Адрес',
                    ],
                ]); ?>
                <?= $form->field($model, 'address_actual', $inputConfig)->widget(MaskedInput::class, [
                    'mask' => '9{6}, ~{1,245}',
                    'definitions' => [
                        '~' => [
                            'validator' => '[ 0-9A-Za-z.,/-]',
                            'cardinality' => 1,
                        ],
                    ],
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Индекс, Адрес',
                    ],
                ]); ?>
                <?= $form->field($model, 'okved', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okogu', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-md-6 left-column">
                <?= $form->field($model, 'okato', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'oktmo', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okfs', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okopf', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'pfr', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'fss', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>

                <?php $model->tax_authority_registration_date = DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                <?= $form->field($model, 'tax_authority_registration_date', array_merge($inputConfig, [
                    'options' => [
                        'class' => 'form-group',
                    ],
                ]))->textInput([
                    'class' => 'form-control date-picker m_b',
                ]); ?>
            </div>
        </div>
    </div>
</div>
