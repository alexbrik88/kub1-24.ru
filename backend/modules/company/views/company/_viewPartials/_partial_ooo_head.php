<?php

use common\models\Company;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */

$inputConfig = $inputConfigRequired = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-4 control-label',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width inp_width_mini',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigRequired['options']['class'] = 'form-group required';
?>

<div class="row">
    <div class="col-md-6">
        <div class="portlet box darkblue portl_min_inp">
            <div class="portlet-title">
                <div class="caption">Руководитель (для документов)</div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'chief_post_name', $inputConfigRequired)->label('Должность')->textInput(); ?>
                        <?= $form->field($model, 'chief_lastname', $inputConfigRequired)->label('Фамилия')->textInput(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'chief_firstname', $inputConfigRequired)->label('Имя')->textInput(); ?>
                        <?= $form->field($model, 'chief_patronymic', $inputConfigRequired)->label('Отчество')->textInput(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet box box-name darkblue portl_min_inp">
            <div class="portlet-title">
                <div class="caption">Главный бухгалтер (для документов)</div>
            </div>
            <div class="portlet-body">
                <div class="form-group">
                    <label
                        class="checkbox-inline match-with-leader">
                        <?= \yii\helpers\Html::activeCheckbox($model, 'chief_is_chief_accountant', [
                            'id' => 'chief_is_chief_accountant_input',
                            'label' => false,
                        ]); ?>
                        совпадает с руководителем
                    </label>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'chief_accountant_lastname', $inputConfig)->label('Фамилия')->textInput(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'chief_accountant_firstname', $inputConfig)->label('Имя')->textInput(); ?>
                        <?= $form->field($model, 'chief_accountant_patronymic', $inputConfig)->label('Отчество')->textInput(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
