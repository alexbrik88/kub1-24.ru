<?php

use common\components\helpers\Html;
use common\models\TaxationType;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\TimeZone;

/* @var $model common\models\Company */
/* @var $companyTaxation \common\models\company\CompanyTaxationType */
/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-8 label-width',
    ],
    'inputOptions' => [
        'class' => 'form-control field-width field-w',
    ],

];

$inputListConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-8 label-width',
    ],
];

$companyTaxation = new \common\models\company\CompanyTaxationType();
?>

<?= $form->field($model, 'name_short', array_merge($textInputConfig, [
    'wrapperOptions' => [
        'class' => 'col-md-6',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
]))->label()->textInput([
    'maxlength' => true,
    'disabled' => $model->company_type_id == \common\models\company\CompanyType::TYPE_IP,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
        'content' => 'Впишите название компании без организационно-правовой формы.',
    ],
]); ?>

<?php if ($model->isNewRecord): ?>
    <div
        class="taxation-type field-registrationform-taxationtypeosno required">
        <label class="control-label">Система
            налогобложения</label>

        <div class="fields required" style="margin-left: 93px; display: inline-block;">
            <?= $form->field($companyTaxation, 'osno', [
                'options' => [
                    'style' => 'margin-left: 38px; display: inline-block;',
                ],
            ])->checkbox()->error(false); ?>
            <?= $form->field($companyTaxation, 'usn', [
                'options' => [
                    'style' => 'margin-left: 20px; display: inline-block;',
                ],
            ])->checkbox(); ?>
            <?= $form->field($companyTaxation, 'envd', [
                'options' => [
                    'style' => 'margin-left: 20px; display: inline-block;',
                ],
            ])->checkbox(); ?>
            <?= $form->field($companyTaxation, 'psn', [
                'options' => [
                    'style' => 'margin-left: 20px; display: inline-block;',
                ],
            ])->checkbox([
                'disabled' => true,
            ]); ?>
            <div class="help-block"></div>
        </div>
    </div>
<?php else: ?>
    <div
        class="form-group taxationCompany field-company-taxation_type_id required">
        <label class="control-label col-md-8 label-width"
               for="company-taxation_type_id">Система налогообложения</label>

        <div
            class="col-md-2">
            <?php if ($model->companyTaxationType->osno): ?>
                ОСНО
            <?php else: ?>
                УСН
                <?= $form->field($model->companyTaxationType, 'usn_type')->radioList(\common\models\company\CompanyTaxationType::$usnType, [
                    'class' => 'inp_one_line_company',
                    'style' => 'margin-top: 10px;',
                    'item' => function ($index, $label, $name, $checked, $value) use ($form, $model) {
                        $percent = null;
                        $wrapper = null;
                        $endWrapper = null;
                        if (!$value) {
                            $style = 'display: block; margin-left: 10px;';
                        } else {
                            $style = 'display: inline-block';
                            $wrapper = '<div style="margin-left: 10px;">';
                            $percent = $form->field($model->companyTaxationType, 'usn_percent', [
                                    'options' => [
                                        'style' => 'display: inline-block; width: 25%; margin-left: 5px;',
                                    ],
                                ])->label(false) . '%';
                            $endWrapper = '</div>';
                        }

                        return $wrapper . Html::tag('label',
                            Html::radio($name, $checked, ['value' => $value]) . $label,
                            [
                                'class' => 'radio-inline p-o radio-padding',
                                'style' => $style,
                            ]) . $percent . $endWrapper;
                    },
                ])->label(false); ?>
            <?php endif; ?>
        </div>

        <div class="col-md-4">
            а так же
            <?= $form->field($model->companyTaxationType, 'envd')->checkbox(); ?>
            <?= $form->field($model->companyTaxationType, 'psn')->checkbox([
                'disabled' => $model->company_type_id !== \common\models\company\CompanyType::TYPE_OAO,
            ]); ?>
        </div>
        <p class="help-block help-block-error"></p>
    </div>
<?php endif; ?>

<?= $form->field($model, 'name_full', array_merge($textInputConfig, [
    'wrapperOptions' => [
        'class' => 'col-md-6',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
]))->label()->textInput([
    'maxlength' => true,
    'disabled' => $model->company_type_id == \common\models\company\CompanyType::TYPE_IP,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
        'content' => 'Впишите название компании без организационно-правовой формы.',
    ],
]); ?>
<?php if ($model->time_zone_id === null) {
    $model->time_zone_id = TimeZone::DEFAULT_TIME_ZONE;
} ?>
<?= $form->field($model, 'time_zone_id', array_merge($textInputConfig, [
    'wrapperOptions' => [
        'class' => 'col-md-6',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
]))->dropDownList(
    ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone')); ?>

<?php if ($model->companyTaxationType->osno) {
    if ($model->nds == null) {
        $model->nds = \common\models\NdsOsno::WITH_NDS;
    }
    echo $form->field($model, 'nds', $inputListConfig)->label('В счетах цену за товар/услугу указывать:')->radioList(
        ArrayHelper::map(\common\models\NdsOsno::find()->all(), 'id', 'name'),
        [
            'class' => 'inp_one_line_company',
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label',
                    Html::radio($name, $checked, ['value' => $value]) . $label,
                    [
                        'class' => 'radio-inline p-o radio-padding',
                    ]) . Html::a('Пример счета', [Url::to('/company/view-example-invoice'), 'nds' => $value == 1 ? true : false], [
                    'class' => 'radio-inline p-o radio-padding',
                    'target' => '_blank',
                ]);
            },
        ]);
}
?>

<?= $form->field($model, 'phone', array_merge($textInputConfig, [
    'wrapperOptions' => [
        'class' => 'col-md-6',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
]))->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+7(999) 999-99-99',
    'options' => [
        'class' => 'form-control field-width field-w',
        'placeholder' => '+7(XXX) XXX-XX-XX',
    ],
]); ?>

<?= $form->field($model, 'email', array_merge($textInputConfig, [
    'wrapperOptions' => [
        'class' => 'col-md-6',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
]))->label()->textInput([
    'maxlength' => true,
]); ?>
<?= $form->field($model, 'hide_widget_footer', array_merge($textInputConfig, [
    'wrapperOptions' => [
        'class' => 'col-md-6 pad-t-5',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
]))->label()->checkbox([], false); ?>


