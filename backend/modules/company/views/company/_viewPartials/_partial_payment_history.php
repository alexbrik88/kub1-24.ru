<?php

use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\service;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\service\Payment;
use php_rutils\RUtils;
use common\models\service\StoreOutInvoiceTariff;

/** @var common\models\Company $model */
/* @var $paymentSearchModel \backend\modules\company\models\PromoCodeSearch */
/* @var $paymentDataProvider yii\data\ActiveDataProvider */

$company = $model;
$companyId = $model->id;
?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            История платежей
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title=""
               title=""></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php $pjax = Pjax::begin([
            'id' => 'subscribe-history-pjax',
            'enablePushState' => false,
            'enableReplaceState' => false,
            'linkSelector' => '#subscribe-history-pjax thead a, #subscribe-history-pjax .pagination a',
        ]); ?>


        <?php $paymentDataProvider->sort->route = '/company/company/subscribe-history'; ?>
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $paymentDataProvider,
            'filterModel' => $paymentSearchModel,

            'filterUrl' => ['subscribe-history', 'id' => $model->id,],

            'id' => 'payment-history-table',
            'layout' => "{items}\n{pager}",
            'options' => [
                'class' => 'overflow-x',
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable documents_table overfl_text_hid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],

            'columns' => [
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата счёта',
                    'value' => function ($model) {
                        return date(\common\components\date\DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS, $model->created_at);
                    }
                ],
                [
                    'label' => 'Плательщик',
                    'format' => 'raw',
                    'value' => function ($model) use ($companyId) {
                        return Html::a($model->company->getShortName(), ['/company/company/view', 'id' => $model->company_id], [
                            'class' => $model->company_id == $companyId ? 'text-bold' : null,
                        ]);
                    }
                ],
                [
                    'label' => 'Компания',
                    'format' => 'raw',
                    'value' => function ($model) use ($companyId) {
                        $content = '';
                        $companyArray = $model->orderCompanies ?: [$model->company];
                        foreach ($companyArray as $company) {
                            $content .= Html::tag('div', Html::a($company->getShortName(), [
                                '/company/company/view',
                                'id' => $company->id,
                            ]), ['class' => 'text-overflow-ellipsis' . ($company->id == $companyId ? ' text-bold' : null)]);
                        }
                        return $content;
                    }
                ],
                [
                    'attribute' => 'subscribe_id',
                    'label' => 'ID подписки',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $content = '';
                        foreach ($model->subscribes as $subscribe) {
                            $content .= Html::tag('div', Html::a($subscribe->id, ['/service/subscribe/view', 'id' => $subscribe->id,]));
                        }
                        return $content;
                    },
                ],
                [
                    'attribute' => 'invoice_number',
                    'label' => '№ счёта',
                    'value' => function ($model) {
                        return $model->outInvoice ? $model->outInvoice->document_number : 'Нет';
                    },
                ],
                [
                    'attribute' => 'sum',
                    'label' => 'Сумма',
                    'value' => function ($model) {
                        return TextHelper::moneyFormat($model->sum, 2);
                    },
                ],
                [
                    'attribute' => 'tariff_id',
                    'label' => 'Тариф',
                    'format' => 'html',
                    'filter' => $paymentSearchModel->getTariffList(),
                    'value' => function (Payment $model) {
                        if ($tariff = $model->tariff) {
                            $values = [];
                            $orderArray = $model->getOrders()->groupBy('tariff_id')->all();
                            foreach ($orderArray as $order) {
                                if ($tariff = $order->tariff) {
                                    $values[] = $tariff->tariffGroup->name . '<br>' . $tariff->getTariffName();
                                }
                            }
                            return implode('<br>', $values);
                        } elseif ($promoCode = $model->promoCode) {
                            $content = $promoCode->tariffGroup->name;
                            if ($subscribe = $model->getSubscribes()->orderBy(['id' => SORT_ASC])->one()) {
                                $content .= '<br>' . $subscribe->getTariffName();
                            }

                            return $content;
                        } elseif ($model->storeTariff) {
                            return $model->storeTariff->cabinets_count . ' ' .
                                RUtils::numeral()->choosePlural($model->storeTariff->cabinets_count, [
                                'кабинет', //1
                                'кабинета', //2
                                'кабинетов' //5
                            ]);
                        } elseif ($model->outInvoiceTariff) {
                            if ($model->outInvoiceTariff->id == StoreOutInvoiceTariff::UNLIM_TARIFF) {
                                return $model->outInvoiceTariff->links_count . ' ссылок';
                            }
                            return $model->outInvoiceTariff->links_count . ' ' .
                                RUtils::numeral()->choosePlural($model->outInvoiceTariff->links_count, [
                                'ссылка', //1
                                'ссылки', //2
                                'ссылок' //5
                            ]);
                        } elseif ($model->payment_for == Payment::FOR_ODDS) {
                            return 'Настройка финансовых отчетов';
                        } elseif ($model->payment_for == Payment::FOR_ADD_INVOICE) {
                            return $model->invoiceTariff->invoice_count . ' ' .
                                RUtils::numeral()->choosePlural($model->invoiceTariff->invoice_count, [
                                    'счет', //1
                                    'счета', //2
                                    'счетов' //5
                                ]);
                        }

                        return null;
                    },
                ],
                [
                    'attribute' => 'type_id',
                    'label' => 'Способ платежа',
                    'filter' => $paymentSearchModel->getPaymentTypeList(),
                    'value' => 'type.name',
                ],
                [
                    'attribute' => 'is_confirmed',
                    'label' => 'Оплачен',
                    'filter' => [null => 'Все', 1 => 'Да', 0 => 'Нет'],
                    'format' => 'boolean',
                ],
                [
                    'headerOptions' => [
                        'width' => '90px',
                    ],
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{pay} {undo} {delete}',
                    'buttons' => [
                        'pay' => function ($url, $model) {
                            if (!$model->is_confirmed) {
                                return Html::a('Оплачен', $url, [
                                    'class' => 'btn btn-primary btn-success btn_in_table',
                                    'title' => Yii::t('yii', 'Update'),
                                    'aria-label' => Yii::t('yii', 'Update'),
                                    'data-confirm' => Yii::t('yii', 'Вы подтверждаете оплату этого счета?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                        },
                        'undo' => function ($url, $model) {
                            if ($model->is_confirmed) {
                                return Html::a('Отменить', $url, [
                                    'class' => 'btn btn-primary btn-danger btn_in_table',
                                    'title' => Yii::t('yii', 'Update'),
                                    'aria-label' => Yii::t('yii', 'Update'),
                                    'data-confirm' => Yii::t(
                                        'yii',
                                        "Вы действительно хотите отменить оплату этого счета?\nБудут удалены все подписки и платежи по этому счету"
                                    ),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                        },
                        'delete' => function ($url, $model) {
                            if (!$model->is_confirmed) {
                                return Html::a('Удалить', $url, [
                                    'class' => 'btn btn-primary btn-danger btn_in_table',
                                    'title' => Yii::t('yii', 'Delete'),
                                    'aria-label' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить этот счет?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) use ($company) {
                        $url = 0;
                        if ($action === 'delete') {
                            $url = '/service/payment/delete';
                        }
                        if ($action === 'pay') {
                            $url = '/service/payment/paid';
                        }
                        if ($action === 'undo') {
                            $url = '/service/payment/unpaid';
                        }
                        return Url::to([$url, 'id' => $model->id, 'company_id' => $company->id]);
                    },
                ],
                [
                    'attribute' => 'payment_date',
                    'label' => 'Дата оплаты',
                    'value' => function ($model) {
                        return $model->payment_date ? date('d.m.Y H:i', $model->payment_date) : 'Не оплачен';
                    }
                ],
            ],
        ]); ?>
        <?php $pjax->end(); ?>
    </div>
</div>