<?php

use common\models\service;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var common\models\Company $model */
/* @var $promocodeSearchModel \backend\modules\company\models\SubscribeSearch */
/* @var $promocodeDataProvider yii\data\ActiveDataProvider */

$company = $model;
?>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Промокоды
        </div>
        <div class="tools">
            <a href="javascript:;" class="expand" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body portlet_button" style="display: none;">
        <div class="btn-group pull-right title-buttons">
            <a href="<?= Url::to(['/service/promo-code/create', 'company_id' => $company->id]); ?>"
               class="btn yellow">
                <i class="fa fa-plus"></i>
                ДОБАВИТь
            </a>
        </div>
    </div>
    <div class="portlet-body" style="display: none;">
        <?php $pjax = Pjax::begin([
            'id' => 'promocodes-pjax',
            'enablePushState' => false,
            'enableReplaceState' => false,
            'linkSelector' => '#promocodes-pjax thead a, #promocodes-pjax .pagination a',
        ]); ?>
        <?php $promocodeDataProvider->sort->route = '/company/company/promocodes'; ?>
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $promocodeDataProvider,
            'filterModel' => $promocodeSearchModel,

            'filterUrl' => ['promocodes', 'id' => $model->id,],

            'id' => 'promocodes-table',
            'layout' => "{items}\n{pager}",
            'options' => [
                'class' => 'overflow-x',
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable documents_table overfl_text_hid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],

            'columns' => [
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата создания',
                    'value' => function (service\PromoCode $promoCode) {
                        return date('Y-m-d H:i', $promoCode->created_at);
                    },
                ],
                'code',
                'started_at',
                'expired_at',
                [
                    'attribute' => 'group_id',
                    'filter' => FilterHelper::getFilterArray(\common\models\service\PromoCodeGroup::find()->all(), 'id', 'name', true, false),
                    'value' => 'group.name',
                ],
                [
                    'label' => 'Бонус, дни',
                    'value' => function (\common\models\service\PromoCode $promoCode) {
                        return \common\models\service\SubscribeHelper::getReadableDuration($promoCode);
                    },
                ],
                'name',
                [
                    'attribute' => 'is_used',
                    'label' => 'Активирован',
                    'value' => function (\common\models\service\PromoCode $promoCode) {
                        return $promoCode->is_used ? service\PromoCode::USED : service\PromoCode::NOT_USED;
                    },
                ],

                [
                    'headerOptions' => [
                        'width' => '30px',
                    ],
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{delete}',
                    'urlCreator' => function ($action, $model, $key, $index) use ($company) {
                        if ($action === 'delete') {
                            return Url::to(['/service/promo-code/delete', 'id' => $model->id, 'company_id' => $company->id,]);
                        }
                    },
                ],
                [
                    'headerOptions' => [
                        'width' => '20px',
                    ],
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{send}',
                    'buttons' => [
                        'send' => function ($url, \common\models\service\PromoCode $model) {
                                return Html::a('<span class="glyphicon glyphicon-send"></span>', $url, [
                                    'title' => Yii::t('yii', 'Отправить на почту'),
                                    'aria-label' => Yii::t('yii', 'Отправить на почту'),
                                    'data-confirm' => Yii::t('yii', 'Вы действительно хотите отправить этот промокод на почту?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) use ($company) {
                        if ($action === 'send') {
                            return Url::to(['/service/promo-code/send', 'id' => $model->id, 'company_id' => $company->id,]);
                        }
                    },
                ],
            ],
        ]); ?>
        <?php $pjax->end(); ?>
    </div>
</div>
