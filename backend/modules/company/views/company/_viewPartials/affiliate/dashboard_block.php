<?php
use backend\modules\company\models\CompanyAffiliateSearch;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $color string
 * @var $status integer
 * @var $sum string
 * @var $count integer
 */
?>
<div class="dashboard-stat <?= $color; ?>" style="position: relative;">
    <div class="visual">
        <i class="fa fa-comments"></i>
    </div>
    <div class="details">
        <div class="number fontSizeSmall">
            <span class="details-sum"
                  <?= $isMoney ? 'data-value="'.$sum.'"' : '' ?>><?= $isMoney ? TextHelper::invoiceMoneyFormat($sum, 2) . '<i class="fa fa-rub"></i>' : intval($sum); ?></span>
        </div>
        <div class="desc">
            <?= $array[$status]; ?>
        </div>
    </div>
    <div class="more">
        <?= isset($count) ? 'Количество счетов: ' . $count : '&nbsp;'; ?>
    </div>
</div>
