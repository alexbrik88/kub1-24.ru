<?php

use common\models\product\Product;
use common\components\helpers\Month;
use frontend\models\Documents;
use common\models\document\Invoice;
use common\components\TextHelper;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\document\InvoiceFacture;
use common\models\document\Order;
use common\models\document\Autoinvoice;

/** @var common\models\Company $model */
/** @var $subscribeSearchModel \backend\modules\company\models\SubscribeSearch */
/** @var $subscribeDataProvider yii\data\ActiveDataProvider */

$companyStatistic = new \backend\modules\company\models\CompanyStatistic([
    'company' => $model,
]);
$months = Month::getLastTwelveMonth();
$totalInvoiceCount = 0;
$totalInvoiceSum = 0;
$hasInvoicesMonthCount = 0;
$totalActCount = 0;
$totalActSum = 0;
$hasActsMonthCount = 0;
$totalPackingListCount = 0;
$totalPackingListSum = 0;
$hasPackingListsMonthCount = 0;
$totalInvoiceFactureCount = 0;
$totalInvoiceFactureSum = 0;
$hasInvoiceFacturiesMonthCount = 0;
?>
<style>
    .company-statistic table th,
    .company-statistic table td {
        text-align: center;
    }

    .company-statistic table td:first-child {
        text-align: left;
        font-weight: bold;
    }
</style>

<div class="company-statistic portlet box darkblue">
    <div class="portlet-title">
        <div class="caption capt_mini">
            Статистика
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title=""
               title=""></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-bordered table-condensed">
            <tbody>
            <tr>
                <th></th>
                <?php foreach ($months as $oneMonth): ?>
                    <th><?= Month::$monthShort[$oneMonth['month']]; ?></th>
                <?php endforeach; ?>
                <th>Сумма счетов</th>
                <th>Сумма руб.</th>
                <th>Сред. знач.</th>
            </tr>
            <tr>
                <td>Выставлено счетов</td>
                <?php foreach ($months as $oneMonth): ?>
                    <td>
                        <?php $monthNumber = $oneMonth['month'] > 9 ? $oneMonth['month'] : ('0' . $oneMonth['month']);
                        $invoiceData = $model
                            ->getInvoices()
                            ->select([
                                'COUNT(id) as invoiceCount',
                                'SUM(total_amount_with_nds) as invoiceSum',
                            ])
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', $oneMonth['year'] . '-' . $monthNumber . '-01', $oneMonth['year'] . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $oneMonth['year'])])
                            ->asArray()
                            ->all();
                        $invoiceData = current($invoiceData);
                        $totalInvoiceCount += $invoiceData['invoiceCount'];
                        $totalInvoiceSum += $invoiceData['invoiceSum'];
                        if ($invoiceData['invoiceCount']) {
                            $hasInvoicesMonthCount += 1;
                        }

                        echo $invoiceData['invoiceCount']; ?>
                    </td>
                <?php endforeach; ?>
                <td><?= $totalInvoiceCount; ?></td>
                <td><?= TextHelper::invoiceMoneyFormat($totalInvoiceSum, 2); ?></td>
                <td><?= $hasInvoicesMonthCount ? round($totalInvoiceCount / $hasInvoicesMonthCount, 2) : 0; ?></td>
            </tr>
            <tr>
                <td>Выставлено актов</td>
                <?php foreach ($months as $oneMonth): ?>
                    <td>
                        <?php $monthNumber = $oneMonth['month'] > 9 ? $oneMonth['month'] : ('0' . $oneMonth['month']);
                        $actData = $model
                            ->getInvoices()
                            ->joinWith('acts')
                            ->select([
                                Act::tableName() . '.id',
                                'COUNT(' . Act::tableName() . '.id) as actCount',
                                'SUM(' . Act::tableName() . '.order_sum) as actSum',
                            ])
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', Act::tableName() . '.document_date', $oneMonth['year'] . '-' . $monthNumber . '-01', $oneMonth['year'] . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $oneMonth['year'])])
                            ->asArray()
                            ->all();
                        $actData = current($actData);
                        $totalActCount += $actData['actCount'];
                        $totalActSum += $actData['actSum'];
                        if ($actData['actCount']) {
                            $hasActsMonthCount += 1;
                        }

                        echo $actData['actCount']; ?>
                    </td>
                <?php endforeach; ?>
                <td><?= $totalActCount; ?></td>
                <td><?= TextHelper::invoiceMoneyFormat($totalActSum, 2); ?></td>
                <td><?= $hasActsMonthCount ? round($totalActCount / $hasActsMonthCount, 2) : 0; ?></td>
            </tr>
            <tr>
                <td>Выставлено ТН</td>
                <?php foreach ($months as $oneMonth): ?>
                    <td>
                        <?php $monthNumber = $oneMonth['month'] > 9 ? $oneMonth['month'] : ('0' . $oneMonth['month']);
                        $packingListData = $model
                            ->getInvoices()
                            ->joinWith('packingLists')
                            ->select([
                                PackingList::tableName() . '.id',
                                'COUNT(' . PackingList::tableName() . '.id) as packingListCount',
                                'SUM(' . PackingList::tableName() . '.orders_sum) as packingListSum',
                            ])
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', PackingList::tableName() . '.document_date', $oneMonth['year'] . '-' . $monthNumber . '-01', $oneMonth['year'] . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $oneMonth['year'])])
                            ->asArray()
                            ->all();
                        $packingListData = current($packingListData);
                        $totalPackingListCount += $packingListData['packingListCount'];
                        $totalPackingListSum += $packingListData['packingListSum'];
                        if ($packingListData['packingListCount']) {
                            $hasPackingListsMonthCount += 1;
                        }

                        echo $packingListData['packingListCount']; ?>
                    </td>
                <?php endforeach; ?>
                <td><?= $totalPackingListCount; ?></td>
                <td><?= TextHelper::invoiceMoneyFormat($totalPackingListSum, 2); ?></td>
                <td><?= $hasPackingListsMonthCount ? round($totalPackingListCount / $hasPackingListsMonthCount, 2) : 0; ?></td>
            </tr>
            <tr>
                <td>Выставлено СФ</td>
                <?php foreach ($months as $oneMonth): ?>
                    <td>
                        <?php $monthNumber = $oneMonth['month'] > 9 ? $oneMonth['month'] : ('0' . $oneMonth['month']);
                        $invoiceFactureData = $model
                            ->getInvoices()
                            ->joinWith('invoiceFactures')
                            ->joinWith('invoiceFactures.orders')
                            ->select([
                                InvoiceFacture::tableName() . '.id',
                                'COUNT(' . InvoiceFacture::tableName() . '.id) as invoiceFactureCount',
                                'SUM(' . Order::tableName() . '.amount_sales_with_vat) as invoiceFactureSum',
                            ])
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', InvoiceFacture::tableName() . '.document_date', $oneMonth['year'] . '-' . $monthNumber . '-01', $oneMonth['year'] . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $oneMonth['year'])])
                            ->asArray()
                            ->all();
                        $invoiceFactureData = current($invoiceFactureData);
                        $totalInvoiceFactureCount += $invoiceFactureData['invoiceFactureCount'];
                        $totalInvoiceFactureSum += $invoiceFactureData['invoiceFactureSum'];
                        if ($invoiceFactureData['invoiceFactureCount']) {
                            $hasInvoiceFacturiesMonthCount += 1;
                        }

                        echo $invoiceFactureData['invoiceFactureCount']; ?>
                    </td>
                <?php endforeach; ?>
                <td><?= $totalInvoiceFactureCount; ?></td>
                <td><?= TextHelper::invoiceMoneyFormat($totalInvoiceFactureSum, 2); ?></td>
                <td><?= $hasInvoiceFacturiesMonthCount ? round($totalInvoiceFactureCount / $hasInvoiceFacturiesMonthCount, 2) : 0; ?></td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-condensed">
            <tbody>
            <tr>
                <th></th>
                <th style="width: 10%;">Счета</th>
                <th style="width: 10%;">Акты</th>
                <th style="width: 10%;">ТН</th>
                <th style="width: 10%;">СФ</th>
                <th style="width: 10%;">ПП</th>
                <th style="width: 10%;">Визитка</th>
                <th style="width: 10%;">Итого</th>
            </tr>
            <tr>
                <?php $outDocumentStat = $companyStatistic->getOutDocumentStat(); ?>
                <td>Исходящие документы</td>
                <?php foreach ($outDocumentStat as $document => $count): ?>
                    <td><?= $count; ?></td>
                <?php endforeach; ?>
                <td></td>
                <td><?= array_sum($outDocumentStat); ?></td>
            </tr>
            <tr>
                <td>Выслано по e-mail</td>
                <td><?= $companyStatistic->getEmailMessagesFromInvoice() ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?= $companyStatistic->getEmailMessagesFromCompany() ?></td>
                <td><?= $companyStatistic->getEmailMessagesFromCompany() + $companyStatistic->getEmailMessagesFromInvoice() ?></td>
            </tr>
            <tr>
                <?php $inDocumentStat = $companyStatistic->getInDocumentStat(); ?>
                <td>Входящие документы</td>
                <?php foreach ($inDocumentStat as $document => $count): ?>
                    <td><?= $count; ?></td>
                <?php endforeach; ?>
                <td></td>
                <td></td>
                <td><?= array_sum($inDocumentStat); ?></td>
            </tr>
            <tr>
                <?php $countFiles = $companyStatistic->getFileUploadStat(); ?>
                <td>Загружено файлов документов, шт</td>
                <?php foreach ($countFiles as $document => $count): ?>
                    <td><?= $count; ?></td>
                <?php endforeach; ?>
                <td></td>
                <td></td>
                <td><?= array_sum($countFiles); ?></td>
            </tr>
            <tr>
                <?php $sizeFiles = $companyStatistic->getFileUploadSize(); ?>
                <td>Размер файлов загруженных документов, МБ</td>
                <?php foreach ($sizeFiles as $document => $size): ?>
                    <td><?= $size; ?></td>
                <?php endforeach; ?>
                <td></td>
                <td></td>
                <td><?= array_sum($sizeFiles); ?></td>
            </tr>
            </tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <?php $contractorStat = $companyStatistic->getContractorStat(); ?>
                <table class="table table-bordered table-condensed">
                    <tbody>
                    <tr>
                        <th></th>
                        <th>Покупатели</th>
                        <th>Поставщики</th>
                        <th>Итого</th>
                    </tr>
                    <tr>
                        <td>Контрагенты</td>
                        <td><?= $contractorStat[\common\models\Contractor::TYPE_CUSTOMER]; ?></td>
                        <td><?= $contractorStat[\common\models\Contractor::TYPE_SELLER]; ?></td>
                        <td><?= array_sum($contractorStat); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <?php $productStat = $companyStatistic->getProductStat(); ?>
                <table class="table table-bordered table-condensed">
                    <tbody>
                    <tr>
                        <th></th>
                        <th>Товары</th>
                        <th>Услуги</th>
                        <th>Итого</th>
                    </tr>
                    <tr>
                        <td>Каталог</td>
                        <td><?= $productStat[Product::PRODUCTION_TYPE_GOODS]; ?></td>
                        <td><?= $productStat[Product::PRODUCTION_TYPE_SERVICE]; ?></td>
                        <td><?= array_sum($productStat); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <table class="table table-bordered table-condensed">
            <?php $roleArray = \common\models\employee\EmployeeRole::find()->actual()->all(); ?>
            <?php $roleStat = $companyStatistic->getEmployeeStat(); ?>
            <tbody>
            <tr>
                <th></th>
                <?php foreach ($roleArray as $role): ?>
                    <th style="width: 15%;"><?= $role->name; ?></th>
                <?php endforeach; ?>
                <th style="width: 15%;">Итого</th>
            </tr>
            <tr>
                <td>Сотрудники</td>
                <?php foreach ($roleArray as $role): ?>
                    <td><?= $roleStat[$role->id]; ?></td>
                <?php endforeach; ?>
                <td><?= array_sum($roleStat); ?></td>
            </tr>
            </tbody>
        </table>

        <div class="row">
            <div class="col-md-8">
                <table class="table table-bordered table-condensed">
                    <?php $roleArray = \common\models\employee\EmployeeRole::find()->actual()->all(); ?>
                    <?php $roleStat = $companyStatistic->getEmployeeStat(); ?>
                    <tbody>
                    <tr>
                        <th></th>
                        <th>Отчётность</th>
                        <th>Мои документы</th>
                    </tr>
                    <tr>
                        <td>Размер других загруженных файлов, МБ</td>
                        <td><?= $companyStatistic->getReportFileUploadSize(); ?></td>
                        <td><?= $companyStatistic->getMyDocumentsFileUploadSize(); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-4">
                <?php $productStat = $companyStatistic->getProductStat(); ?>
                <table class="table table-bordered table-condensed">
                    <tbody>
                    <tr>
                        <th>Размер всех загруженных файлов, МБ</th>
                    </tr>
                    <tr>
                        <td style="text-align: center;"><?= $companyStatistic->getAllFileUploadSize(); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <table class="table table-bordered table-condensed">
                    <tbody>
                    <tr>
                        <th>Есть автосчёт</th>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <?php echo Autoinvoice::find()->where(['company_id' => $model->id])->andWhere(['status' => Autoinvoice::ACTIVE])->count() ?: 'Нет' ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
