<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 31.07.2016
 * Time: 11:47
 */

use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use common\components\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\widgets\ConfirmModalWidget;
use common\models\Company;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $company Company */


$this->title = 'Расчетные счета';
?>
<div class="portlet box darkblue details">
    <div class="portlet-title">
        <div class="caption"><?= $this->title; ?></div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap overfl_text_hid',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => "{items}\n{pager}",

                'columns' => [
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Банк',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                        'format' => 'raw',
                        'value' => function (CheckingAccountant $model) {
                            return $model->bank_name;
                        },
                    ],
                    [
                        'attribute' => 'bik',
                        'label' => 'БИК',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (CheckingAccountant $model) {
                            return $model->bik;
                        },
                    ],
                    [
                        'attribute' => 'ks',
                        'label' => 'К/с',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'format' => 'raw',
                        'value' => function (CheckingAccountant $model) {
                            return $model->ks;
                        },
                    ],
                    [
                        'attribute' => 'rs',
                        'label' => 'Р/с',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'format' => 'raw',
                        'value' => function (CheckingAccountant $model) {
                            return $model->rs;
                        },
                    ],
                    [
                        'attribute' => 'type',
                        'label' => 'Тип',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (CheckingAccountant $model) {
                            return $model->typeText[$model->type];
                        },
                    ],
                    [
                        'class' => \yii\grid\ActionColumn::className(),
                        'template' => '{update} {delete}',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'buttons' => [
                            'update' => function ($url, CheckingAccountant $data) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#update-company-rs-' . $data->id, [
                                    'data-toggle' => 'modal',
                                    'title' => Yii::t('yii', 'Обновить'),
                                    'aria-label' => Yii::t('yii', 'Обновить'),
                                ]);
                            },
                            'delete' => function ($url, CheckingAccountant $data) {
                                if ($data->type !== CheckingAccountant::TYPE_MAIN && !$data->hasMovement()) {
                                    return ConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                            'class' => '',
                                            'tag' => 'a',
                                        ],
                                        'confirmUrl' => $url,
                                        'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                                    ]);
                                }

                                return '';
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) use ($company) {
                            $url = 0;
                            switch ($action) {
                                case 'update':
                                    $url = 'update';
                                    break;
                                case 'delete':
                                    $url = 'delete-checking-accountant';
                                    break;
                            }

                            return Url::to([$url, 'id' => $model->id, 'companyId' => $company->id]);
                        },
                    ],
                ],
            ]); ?>
        </div>
        <div class="portlet box">
            <div class="btn-group pull-left">
                <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить существующий счет', [
                    'class' => 'btn yellow',
                    'data-toggle' => 'modal',
                    'href' => '#add-company-rs',
                ]); ?>
            </div>
        </div>
        <div class="table-container" style="padding-top: 10px;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid" style="border: none;">
            </table>
        </div>
    </div>
</div>
