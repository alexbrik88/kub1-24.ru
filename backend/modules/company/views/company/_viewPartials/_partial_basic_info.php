<?php

use common\components\date\DateHelper;
use common\models\service\SubscribeHelper;
use yii\bootstrap\Html;
use yii\helpers\Url;
use Cake\Utility\Text;
use common\models\product\Product;
use kartik\select2\Select2;
use common\components\helpers\ArrayHelper;
use common\models\company\Activities;

/* @var \yii\web\View $this */
/** @var common\models\Company $model */

$currentActualSubscribe = $model->hasActualSubscription ? $model->activeSubscribe : null;
$actualSubscribes = SubscribeHelper::getPayedSubscriptions($model->id);

$expireDate = SubscribeHelper::getExpireDate($actualSubscribes);
$expireDays = SubscribeHelper::getExpireLeftDays($expireDate);
?>

<div class="row portlet">
    <div class="col-md-4">
        <p>ИНН: <?= $model->inn == null ? 'Не задано' : $model->inn; ?></p>

        <p><?= $model->blocked == 1 ? 'Заблокирован' : 'Активен'; ?></p>

        <p>
            Текущая подписка:
            <?= !empty($currentActualSubscribe)
                ? $currentActualSubscribe->getTariffName() . ' до ' . date(DateHelper::FORMAT_USER_DATE, $currentActualSubscribe->expired_at)
                : ($model->isFreeTariff ? 'тариф "Бесплатно"' : 'нет активной подписки') ?>
        </p>

        <p>
            Дата начисления счетов:
            <?= !empty($currentActualSubscribe) && $currentActualSubscribe->getMonthsLeft()
                ? date(DateHelper::FORMAT_USER_DATE, $currentActualSubscribe->getLimitNextTime())
                : '—' ?>
        </p>

        <p>
            До следующей оплаты:
            <?= \Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                'n' => $expireDays,
            ]); ?>
            до
            <?= date(DateHelper::FORMAT_USER_DATE, $expireDate); ?>
        </p>

        <p>
            Последнее посещение:
            <?= $model->last_visit_at
                ? date(\common\components\date\DateHelper::FORMAT_USER_DATETIME, $model->last_visit_at)
                : 'Не было'; ?>
        </p>
    </div>
    <div class="col-md-8">
        <p>Комментарий:</p>
        <p><?= Html::textarea('Company[comment]', $model->comment, [
                'class' => 'form-control noresize',
                'id' => 'comment',
                'rows' => 4,
            ]); ?>
        </p>
        <table class="table table-striped table-bordered table-hover company-export-table" style="margin-bottom: 0;width: 97%;">
            <tbody>
            <tr class="heading">
                <th>Название услуг</th>
                <th>Название товаров</th>
                <th>Вид деятельности</th>
            </tr>
            <tr>
                <td>
                    <span title="<?= $model->getFirstTenProducts(Product::PRODUCTION_TYPE_SERVICE); ?>">
                        <?= Text::truncate($model->getFirstTenProducts(Product::PRODUCTION_TYPE_SERVICE), 45, [
                            'ellipsis' => '...',
                            'exact' => true,
                        ]); ?>
                    </span>
                </td>
                <td>
                    <span title="<?= $model->getFirstTenProducts(Product::PRODUCTION_TYPE_GOODS); ?>">
                        <?= Text::truncate($model->getFirstTenProducts(Product::PRODUCTION_TYPE_GOODS), 45, [
                            'ellipsis' => '...',
                            'exact' => true,
                        ]); ?>
                    </span>
                </td>
                <td>
                    <?= Select2::widget([
                        'model' => $model,
                        'attribute' => 'activities_id',
                        'data' => ArrayHelper::merge([null => '---'], ArrayHelper::map(Activities::find()->orderBy('name')->all(), 'id', 'name')),
                        'options' => [
                            'id' => "company{$model->id}-activities_id",
                            'class' => 'form-control activities-drop-down',
                            'data-url' => Url::to(['/company/company/change-activities', 'id' => $model->id,]),
                        ],
                    ]); ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<?php
$changeComment = 'change-comment';
$script = <<< JS
 $(document).ready(function(){

   $("#comment").change(function(){
          jQuery.post({
            url : '$changeComment',
            data : {id: '$model->id',comment : this.value}
        });

      });

   });

JS;
$this->registerJs($script, $this::POS_READY);
?>

