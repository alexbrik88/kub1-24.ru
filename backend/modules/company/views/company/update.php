<?php

use yii\helpers\Html;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $searchModel CheckingAccountantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $checkingAccountant CheckingAccountant */

$this->title = 'Обновить компанию';
$this->params['breadcrumbs'][] = ['label' => 'Help Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="help-article-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_viewPartials/_form', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'checkingAccountant' => $checkingAccountant,
    ]) ?>

</div>
