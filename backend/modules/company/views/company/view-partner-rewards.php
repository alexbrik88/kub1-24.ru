<?php

use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\CompanyAffiliateLeadRewards;
use common\models\company\CompanyAffiliateLink;
use common\components\date\DateHelper;
use common\models\company\CompanyAffiliateSearch;
use common\models\service\ServiceModule;
use common\widgets\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\web\View;

/** @var $this yii\web\View */
/** @var Company $companyModel */
/** @var CompanyAffiliateSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */

$this->title = 'Вознаграждение';

?>

<div class="affiliate-link">
    <div class="portlet box darkblue  mt-5 pt-5">
        <div class="portlet-title">
            <div class="caption">
                <?= $this->title; ?>
            </div>
        </div>
        <div class="portlet-body">
            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                'options' => [
                    'style' => 'overflow-x: auto;',
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],

                'columns' => [
                    [
                        'attribute' => 'rewards_range',
                        'label' => 'Общая сумма оплат (от начала сотрудничества)',
                        'headerOptions' => [
                            'width' => '45%',
                        ],
                        'format' => 'raw',
                        'value' => function (CompanyAffiliateLeadRewards $model) {
                            $bottom = $model->bottom_line ?: '';
                            $top = $model->top_line ?: '';
                            return Html::tag('div',
                                Html::tag('span', ($model->bottom_line ? ' от ' : ' до '), ['class' => 'hints_from_' . $model->id])
                                . Html::tag('span', (is_numeric($bottom) ? TextHelper::invoiceMoneyFormat($bottom * 100, 0) : $bottom), [
                                    'id' => 'bottom_line_rewards_range_' . $model->id,
                                ])
                                . Html::tag('span', ($model->top_line && $model->bottom_line ? ' до ' : ''), ['class' => 'hints_to_' . $model->id])
                                . Html::tag('span', (is_numeric($top) ? TextHelper::invoiceMoneyFormat($top * 100, 0) : $top), [
                                    'id' => 'top_line_rewards_range_' . $model->id,
                                ]));
                        },
                    ],
                    [
                        'attribute' => 'rewards',
                        'label' => 'Размер партнерского вознаграждения, %',
                        'headerOptions' => [
                            'width' => '45%',
                        ],
                        'format' => 'raw',
                        'value' => function (CompanyAffiliateLeadRewards $model) {
                            return Html::tag('div', Html::tag('span', Html::encode($model->rewards), [
                                'id' => 'by_lead_rewards_' . $model->id,
                            ]));
                        },
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{edit}',
                        'buttons' => [
                            'edit' => function ($url, $data) {
                                return Html::tag('div', Html::a('<i class="fa fa-pencil"></i>', '#edit', [
                                    'title' => 'Редактировать',
                                    'class' => 'affiliate_lead_rewards link pl-1 pr-1',
                                    'data' => [
                                        'model-id' => $data['id'],
                                        'model-company-id' => $data['company_id'],
                                    ],
                                ]));
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'modal_update_link',
    'header' => '<h4>Добавить ссылку</h4>',
    'closeButton' => [
        'class' => 'modal-close close',
    ],
]); ?>

<?= $this->render('_viewPartials/affiliate/_edit_links_form', [
    'model' => (new CompanyAffiliateLink()),
    'products' => ServiceModule::$serviceModuleLabel,
]); ?>

<?php Modal::end(); ?>


<?php

$this->registerJs(<<<JS
    $(document).on('click', '.affiliate_lead_rewards', function(e){
        e.preventDefault();
        var id = $(this).data('model-id'),
            company_id = $(this).data('model-company-id'),
            top = $('#top_line_rewards_range_' + id),
            bottom = $('#bottom_line_rewards_range_' + id),
            rewards = $('#by_lead_rewards_' + id),
            hints_from = $('.hints_from_' + id),
            hints_to = $('.hints_to_' + id);
        
        top.html('<input type="text" ' +
             ' class="top_line_rewards_range_' + id + '"' +
             ' value="' + top.text().replace(",", ".").split(" ").join("") + '" />');
        bottom.html('<input type="text" ' +
             ' class="bottom_line_rewards_range_' + id + '"' +
             ' value="' + bottom.text().replace(",", ".").split(" ").join("") + '" />');
        rewards.html('<input type="text" ' +
             ' class="by_lead_rewards_' + id + '"' +
             ' value="' + rewards.text().replace(",", ".").split(" ").join("") + '" />');
        
        hints_from.html("");
        hints_to.html(" - ");
        
        $(this).closest('div').html('<input type="button" ' +
                '           value="Сохранить" ' +
                '           class="affiliate_lead_rewards__save" ' +
                '           data-model-id="'+id+'"' +
                '           data-model-company-id="'+company_id+'" />');
        
    })
    
    $(document).on('click', '.affiliate_lead_rewards__save', function(e){
        e.preventDefault();
        var id = $(this).data('model-id'),
            company_id = $(this).data('model-company-id'),
            top = $('#top_line_rewards_range_' + id),
            bottom = $('#bottom_line_rewards_range_' + id),
            rewards = $('#by_lead_rewards_' + id);
        
        var button = $(this);
        
        $.post('lead-rewards-save', {
                'id': id, 
                'company_id': company_id, 
                'CompanyAffiliateLeadRewards[top_line]': top.find('input').val(), 
                'CompanyAffiliateLeadRewards[bottom_line]': bottom.find('input').val(),
                'CompanyAffiliateLeadRewards[rewards]': rewards.find('input').val()
            }, function(data) {
            if ('true' === data.success) {
                location.reload();
            }
        }, 'json')
    });
JS, View::POS_READY)

?>