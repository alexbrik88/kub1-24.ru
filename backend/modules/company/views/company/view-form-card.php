<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.08.2017
 * Time: 7:02
 */

use common\models\Company;
use yii\bootstrap\Html;
use common\components\date\DateHelper;
use yii\helpers\Url;
use common\models\ServiceMoreStock;

/** @var $this yii\web\View */
/** @var Company $model */

$this->title = 'Карточка компании';
$this->context->layoutWrapperCssClass = 'edit-profile';
?>
<div class="form-horizontal">
    <div class="portlet">
        <div class="portlet-title">
            <?= Html::a('Назад к списку', ['index'], [
                'class' => 'back-to-customers',
            ]); ?>

            <p>
                ID <?= $model->id ?>,
                зарегистрирована <?= date(DateHelper::FORMAT_USER_DATE, $model->created_at) ?>
            </p>

            <div class="caption bold-text">
                <?= $model->getTitle(true); ?>
                <?php if ($model->blocked == Company::BLOCKED): ?>
                    <span
                        class="label label-default">Компания заблокирована</span>
                <?php endif; ?>
            </div>
            <div class="company-type-test" style="float: left;">
                <?= Html::checkbox('Company[test]', $model->test, [
                    'class' => 'form-control',
                    'data' => [
                        'url' => Url::to(['/company/company/change-status', 'id' => $model->id]),
                    ]
                ]); ?>
                <label>Тестовая компания</label>
            </div>
            <div class="col-md-7 navbar-company pull-right">
                <ul id="w3" class="navbar-nav navbar-right nav"
                    style="margin-bottom: -15px;">
                    <li>
                        <?= Html::a('Профиль', Url::to(['view', 'id' => $model->id])); ?>
                    </li>
                    <li class="active">
                        <?= Html::a('Карточка компании', Url::to(['view-form-card', 'id' => $model->id])); ?>
                    </li>
                    <li>
                        <?= Html::a('Партнерский кабинет', Url::to(['view-partner-cabinet', 'company_id' => $model->id])); ?>
                    </li>
                </ul>
            </div>
            <div class="actions">
            </div>
        </div>
    </div>
    <table
        class="table table-striped table-bordered table-hover company-export-table">
        <tbody>
        <tr class="heading">
            <th></th>
            <th>Значение</th>
        </tr>
        <tr>
            <td>Google Analytics ID</td>
            <td><?= $model->form_google_analytics_id; ?></td>
        </tr>
        <tr>
            <td>Страница регистрации</td>
            <td>
                <?= $model->registrationPageType ? $model->registrationPageType->name : null; ?>
                <?php if ($model->business_analytics_first_visit_at): ?>
                    <?= ($model->registrationPageType) ? ', ' : '' ?> Аналитика КУБ
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>Источник или канал</td>
            <td><?= $model->form_source; ?></td>
        </tr>
        <tr>
            <td>Ключевое слово</td>
            <td><?= $model->form_keyword; ?></td>
        </tr>
        <tr>
            <td>UTM</td>
            <td><?= $model->form_utm; ?></td>
        </tr>
        <tr>
            <td>Регион</td>
            <td><?= $model->form_region; ?></td>
        </tr>
        <tr>
            <td>Дата</td>
            <td><?= DateHelper::format($model->form_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
        </tr>
        <tr>
            <td>Час</td>
            <td><?= $model->form_hour; ?></td>
        </tr>
        <tr>
            <td>Страница входа</td>
            <td><?= $model->form_sign_in_page; ?></td>
        </tr>
        <tr>
            <td>Страница выхода</td>
            <td><?= $model->form_entrance_page; ?></td>
        </tr>
        <tr>
            <td>Уникальные события</td>
            <td><?= $model->form_unique_events_count; ?></td>
        </tr>
        <tr>
            <td>При входе выбрал блок</td>
            <td><?= Company::$afterRegistrationBlock[$model->after_registration_block_type]; ?></td>
        </tr>
        <tr>
            <td>Партнерская ссылка</td>
            <td><?= $model->affiliate_link; ?></td>
        </tr>
        <tr>
            <td>Мобильное приложение</td>
            <td><?= implode(', ', array_map(function ($data) {
                return sprintf(
                    '%s(%s)',
                    Html::encode($data->apiPartners ? $data->apiPartners->partner_name : $data->api_partners_id),
                    date_create($data->visit_at)->format('d.m.Y')
                );
            },$model->apiFirstVisits)) ?></td>
        </tr>
        </tbody>
    </table>

    <?php if ($model->serviceMoreStock): ?>
        <table
            class="table table-striped table-bordered table-hover company-export-table">
            <tbody>
            <tr class="heading">
                <th></th>
                <th>Значение</th>
            </tr>
            <tr>
                <td>Вид деятельности</td>
                <td><?= $model->serviceMoreStock->activities ? $model->serviceMoreStock->activities->name : $model->serviceMoreStock->company_type_text; ?></td>
            </tr>
            <tr>
                <td>Специализация</td>
                <td><?= ServiceMoreStock::$specializationArray[$model->serviceMoreStock->specialization]; ?></td>
            </tr>
            <tr>
                <td>Среднее кол-во счетов в месяц</td>
                <td><?= ServiceMoreStock::$averageInvoiceCountArray[$model->serviceMoreStock->average_invoice_count]; ?></td>
            </tr>
            <tr>
                <td>Кол-во сотрудников</td>
                <td><?= ServiceMoreStock::$employeesCountArray[$model->serviceMoreStock->employees_count]; ?></td>
            </tr>
            <tr>
                <td>Кто ведёт бухгалтерию</td>
                <td><?= ServiceMoreStock::$accountantArray[$model->serviceMoreStock->accountant_id]; ?></td>
            </tr>
            <tr>
                <td>Есть логотип?</td>
                <td><?= ServiceMoreStock::$hasLogoArray[$model->serviceMoreStock->has_logo]; ?></td>
            </tr>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<?= $this->render('_viewPartials/_partial_events', [
    'model' => $model,
]); ?>

<?= $this->render('_viewPartials/_partial_findir_events', [
    'model' => $model,
]); ?>

<?= $this->render('_viewPartials/_partial_visit', [
    'model' => $model,
]); ?>
