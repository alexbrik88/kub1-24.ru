<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.07.2017
 * Time: 18:02
 */

namespace backend\modules\company\models;


use common\models\service\Subscribe;
use common\models\Company;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use yii\data\ActiveDataProvider;

/**
 * Class AffiliateSearch
 * @package backend\modules\company\models
 */
class AffiliateSearch extends Subscribe
{
    /**
     * @param Company $company
     * @param $params
     * @return ActiveDataProvider
     */
    public function search(Company $company, $params)
    {
        $query = Subscribe::find()
            ->andWhere(['in', Subscribe::tableName() . '.company_id', $company->getInvitedCompanies()->column()])
            ->andWhere(['in', Subscribe::tableName() . '.tariff_id', SubscribeTariff::paidStandartIds()])
            ->andWhere(['in', 'status_id', [SubscribeStatus::STATUS_PAYED,
                SubscribeStatus::STATUS_ACTIVATED],
            ])
            ->joinWith('company')
            ->joinWith('payment');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'service_payment.payment_date' => SORT_DESC,
                ],
                'attributes' => [
                    'company.created_at',
                    'service_payment.sum',
                    'service_payment.payment_date',
                    'reward',
                ],
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}