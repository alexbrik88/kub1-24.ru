<?php

namespace backend\modules\company\models;

use common\models\Company;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class CompanyValue
 * @package backend\modules\company\models
 */
class CompanyValue extends ActiveRecord
{
    /**
     * @return int
     */
    public static function findRegistered()
    {
        return (int) Company::find()->isTest(!Company::TEST_COMPANY)->count();
    }

    /**
     * @return int
     */
    public static function findBlocked()
    {
        return (int) Company::find()->isBlocked()->isTest(!Company::TEST_COMPANY)->count();
    }

    /**
     * @return int
     */
    public static function findTrialSubscribed()
    {
        return (int) Company::find()
            ->from(['company' => Company::tableName()])
            ->joinWith(['activeSubscribe' => function (ActiveQuery $query) {
                return $query->from(['activeSubscribe' => Subscribe::tableName()]);
            }], true, 'INNER JOIN')
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->andWhere([
                'activeSubscribe.tariff_id' => SubscribeTariff::TARIFF_TRIAL,
            ])
            ->andWhere(['OR',
                ['>', 'activeSubscribe.expired_at', new Expression('UNIX_TIMESTAMP()')],
                ['IS', 'activeSubscribe.expired_at', null],
            ])
            ->count();
    }

    /**
     * @return int
     */
    public static function findActive()
    {
        //$totalCount = static::findRegistered();
        //$blockedCount = static::findBlocked();
        //$subscribedCount = static::findTrialSubscribed();
//
        //return (int) ($totalCount - $blockedCount - $subscribedCount);

        return (int) Company::find()
            ->from(['company' => Company::tableName()])
            ->joinWith(['activeSubscribe' => function (ActiveQuery $query) {
                return $query->from(['activeSubscribe' => Subscribe::tableName()]);
            }], true, 'INNER JOIN')
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->andWhere(['OR',
                ['>', 'activeSubscribe.expired_at', new Expression('UNIX_TIMESTAMP()')],
                ['IS', 'activeSubscribe.expired_at', null],
            ])
            ->count();
    }
}