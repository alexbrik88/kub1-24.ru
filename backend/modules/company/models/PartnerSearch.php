<?php


namespace backend\modules\company\models;


use common\models\Company;
use common\models\service\Payment;
use common\models\service\ServiceModule;
use frontend\components\StatisticPeriod;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class PartnerSearch extends Model
{
    public function search($params)
    {
        $this->load($params);

        $dateRange = StatisticPeriod::getSessionPeriod();

        $query = $this
            ->getBaseQuery()
            ->andWhere(['IS NOT', Company::tableName() . '.affiliate_link', new Expression('NULL')])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'registrationDate',
                    'partnerRegistrationDate',
                    'invoiceCount',
                    'priceListsCount',
                    'b2bCount',
                    'bookkeepingCount',
                    'analysisCount',
                    'paidCount',
                    'paidSum',
                    'paidInvoicesCount',
                    'paidInvoicesSum',
                ],
                'defaultOrder' => [
                    'registrationDate' => SORT_DESC,
                    'partnerRegistrationDate' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    public function getSubQuery($serviceModuleId)
    {
        return Company::find()
            ->addSelect([
                Company::tableName() . '.invited_by_company_id as invited_company',
                Company::tableName() . '.invited_by_product_id as serviceModule',
                'COUNT('.Company::tableName() . '.invited_by_product_id) as count'
            ])
            ->andWhere(['and',
                [Company::tableName() . '.invited_by_product_id' => $serviceModuleId],
            ])
            ->groupBy(Company::tableName() . '.invited_by_company_id');
    }

    public function getBaseQuery()
    {
        $subQueryInvoice = $this->getSubQuery(array_search('invoices', ServiceModule::$serviceModuleArray));
        $subQueryPriceLists = $this->getSubQuery(array_search('price-lists', ServiceModule::$serviceModuleArray));
        $subQueryB2B = $this->getSubQuery(array_search('b2b', ServiceModule::$serviceModuleArray));
        $subQueryBookkeeping = $this->getSubQuery(array_search('bookkeeping', ServiceModule::$serviceModuleArray));
        $subQueryAnalysis = $this->getSubQuery(array_search('analysis', ServiceModule::$serviceModuleArray));

        $payedInvoices = Company::find()
            ->select([
                Company::tableName() . '.invited_by_company_id as company_id',
                'COUNT('.Payment::tableName() . '.sum) as count',
                'SUM('.Payment::tableName() . '.sum) as sum',
            ])
            ->joinWith('payments')
            ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
            ->groupBy(Company::tableName() . '.invited_by_company_id');

        $issuedInvoices = Company::find()
            ->select([
                Company::tableName() . '.invited_by_company_id as company_id',
                'COUNT('.Payment::tableName() . '.sum) as count',
                'SUM('.Payment::tableName() . '.sum) as sum',
            ])
            ->joinWith('payments')
            ->andWhere(['IS NOT', Payment::tableName() . '.id', new Expression('NULL')])
            ->groupBy(Company::tableName() . '.invited_by_company_id');

        $query = Company::find()
            ->addSelect([
                Company::tableName() . '.id as id',
                Company::tableName() . '.created_at as registrationDate',
                'affiliate_link_created_at as partnerRegistrationDate',
                'name_short as partnerName',
                'invoice.count as invoiceCount',
                'priceLists.count as priceListsCount',
                'b2b.count as b2bCount',
                'bookkeeping.count as bookkeepingCount',
                'analysis.count as analysisCount',
                'issuedInvoices.count paidCount',
                'issuedInvoices.sum as paidSum',
                'payedInvoices.count as paidInvoicesCount',
                'payedInvoices.sum as paidInvoicesSum',
            ])
            ->leftJoin(['invoice' => $subQueryInvoice], Company::tableName() . '.id = invoice.invited_company')
            ->leftJoin(['priceLists' => $subQueryPriceLists], Company::tableName() . '.id = priceLists.invited_company')
            ->leftJoin(['b2b' => $subQueryB2B], Company::tableName() . '.id = b2b.invited_company')
            ->leftJoin(['bookkeeping' => $subQueryBookkeeping], Company::tableName() . '.id = bookkeeping.invited_company')
            ->leftJoin(['analysis' => $subQueryAnalysis], Company::tableName() . '.id = analysis.invited_company')
            ->leftJoin(['payedInvoices' => $payedInvoices], Company::tableName() . '.id = payedInvoices.company_id')
            ->leftJoin(['issuedInvoices' => $issuedInvoices], Company::tableName() . '.id = issuedInvoices.company_id');

        return $query;
    }
}