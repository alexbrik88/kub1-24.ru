<?php

namespace backend\modules\company\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\company\CompanyFirstEvent;

/**
 * CompanyFirstEventSearch represents the model behind the search form about `common\models\company\CompanyFirstEvent`.
 */
class CompanyFirstEventSearch extends CompanyFirstEvent
{
    public $name;
    public $category;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->alias('t')
            ->select([
                'e.name',
                't.step',
                't.visit',
                't.created_at',
            ])
            ->joinWith('event e', true, 'RIGHT JOIN')
            ->andWhere([
                't.company_id' => $this->company_id,
                'e.category' => $this->category,
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'name',
                    'step' => [
                        'asc' => [
                            'IF({{t}}.[[step]] IS NULL, 1, 0)' => SORT_ASC,
                            't.step' => SORT_ASC,
                        ],
                        'desc' => [
                            'IF({{t}}.[[step]] IS NULL, 1, 0)' => SORT_ASC,
                            't.step' => SORT_DESC,
                        ],
                    ],
                    'visit' => [
                        'asc' => [
                            'IF({{t}}.[[visit]] IS NULL, 1, 0)' => SORT_ASC,
                            't.visit' => SORT_ASC,
                        ],
                        'desc' => [
                            'IF({{t}}.[[visit]] IS NULL, 1, 0)' => SORT_ASC,
                            't.visit' => SORT_DESC,
                        ],
                    ],
                    'created_at' => [
                        'asc' => [
                            'IF({{t}}.[[created_at]] IS NULL, 1, 0)' => SORT_ASC,
                            't.created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'IF({{t}}.[[created_at]] IS NULL, 1, 0)' => SORT_ASC,
                            't.created_at' => SORT_DESC,
                        ],
                    ],
                ],
                'defaultOrder' => [
                    'step' => SORT_ASC,
                ],
            ],
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'event_id' => $this->event_id,
            'step' => $this->step,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
