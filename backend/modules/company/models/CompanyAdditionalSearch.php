<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.12.2016
 * Time: 8:41
 */

namespace backend\modules\company\models;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\CompanyQuery;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use frontend\components\StatisticPeriod;
use frontend\rbac\permissions\Service;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\Url;

/**
 * Class CompanyAdditionalSearch
 * @package backend\modules\company\models
 */
class CompanyAdditionalSearch extends Company
{
    /**
     * @var
     */
    public $bankName;
    /**
     * @var
     */
    public $activeTariff;
    /**
     * @var int
     */
    public $pageSize = 100;
    /**
     * @var
     */
    public $registrationPageType;

    /**
     * @var
     */
    public $companyTaxationType;

    /**
     * @var
     */
    public $fio;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bankName', 'activeTariff', 'companyTaxationType', 'utm_source'], 'string'],
            [['phone', 'pageSize', 'activation_type', 'fio', 'registrationPageType', 'company_type_id', 'name_short'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        /* @var ActiveQuery $query */
        $query = $this->getCompanies()
            ->orderBy('created_at DESC');

        $this->load($params);

        if ($this->bankName == -1) {
            $query->andWhere(['checking_accountant.bank_name' => null]);
        } else {
            $query->andFilterWhere(['checking_accountant.bank_name' => $this->bankName]);
        }

        if ($this->activation_type == -1) {
            $query->andWhere(['activation_type' => null]);
        } else {
            $query->andFilterWhere(['activation_type' => $this->activation_type]);
        }

        $query->andFilterWhere(['registration_page_type_id' => $this->registrationPageType]);

        $query->andFilterWhere(['company_type_id' => $this->company_type_id]);

        $query->andFilterWhere(['utm_source' => $this->utm_source]);

        if (mb_substr($this->activeTariff, 0, 2) == 'pr') {
            $companies = [];
            /* @var $subscribe Subscribe */
            $subscribe = Subscribe::findOne(str_replace('pr', '', $this->activeTariff));
            $duration = SubscribeHelper::getReadableDuration($subscribe);
            $query->andWhere([
                'and',
                [Subscribe::tableName() . '.tariff_id' => null],
                ['not', [Subscribe::tableName() . '.id' => null]],
            ]);
            /* @var $company Company */
            $cloneQuery = clone $query;
            foreach ($cloneQuery->all() as $company) {
                if (SubscribeHelper::getReadableDuration($company->activeSubscribe) == $duration) {
                    $companies[] = $company->id;
                }
            }
            $query->andWhere(['in', Company::tableName() . '.id', $companies]);

        } elseif ($this->activeTariff == 'free') {
            $query->andWhere(['not', [Company::tableName() . '.free_tariff_start_at' => null]]);
        } elseif ($this->activeTariff == 'no') {
            $query->andWhere([
                Company::tableName() . '.free_tariff_start_at' => null,
                Company::tableName() . '.active_subscribe_id' => null,
            ]);
        } else {
            $query->andFilterWhere([Subscribe::tableName() . '.tariff_id' => $this->activeTariff]);
        }

        if ($this->phone) {
            $query->andWhere(['and',
                    ['not', [Company::tableName() . '.phone' => null]],
                    ['!=', 'phone', ''],
                ]
            );
        } elseif ($this->phone === '0') {
            $query->andWhere(['phone' => null]);
        } else {
            $query->andFilterWhere(['phone' => $this->phone]);
        }

        if ($this->fio) {
            $query->andWhere(['or',
                ['not', ['chief_firstname' => null]],
                ['not', ['chief_lastname' => null]],
                ['not', ['chief_patronymic' => null]],
            ]);
        } elseif ($this->fio === '0') {
            $query->andWhere(['and',
                ['chief_firstname' => null],
                ['chief_lastname' => null],
                ['chief_patronymic' => null],
            ]);
        }

        if ($this->name_short) {
            $query->andWhere(['and',
                    ['not', [Company::tableName() . '.name_short' => null]],
                    ['!=', Company::tableName() . '.name_short', ''],
                ]
            );
        } elseif ($this->name_short === '0') {
            $query->andWhere([Company::tableName() . '.name_short' => null]);
        } else {
            $query->andFilterWhere([Company::tableName() . '.name_short' => $this->phone]);
        }

        if ($this->companyTaxationType) {
            switch ($this->companyTaxationType) {
                case 'osno':
                    $query->andWhere(['company_taxation_type.osno' => 1]);
                    break;
                case 'usn6':
                    $query->andWhere(['company_taxation_type.usn' => 1])->andWhere(['company_taxation_type.usn_percent' => 6]);
                    break;
                case 'usn15':
                    $query->andWhere(['company_taxation_type.usn' => 1])->andWhere(['company_taxation_type.usn_percent' => 15]);
                    break;
            }
        }

        return new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
            'sort' => [
                'attributes' => [
                    'outInvoiceCount',
                    'created_at',
                    'outInvoiceSendEmailCount',
                    'activeSubscribeDaysCount',
                    'isFileLogo',
                    'isFilePrint',
                    'isFileSignature',
                    'filesCount',
                    'customersCount',
                    'sellersCount',
                    'autoInvoicesCount',
                    'outActCount',
                    'paymentOrderCount',
                    'import_xls_product_count',
                    'import_xls_service_count',
                    'goodsCount',
                    'servicesCount',
                    'statementFromBank',
                ],
            ],
        ]);
    }

    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     */
    public function searchModelsForExcel($params)
    {
        /* @var ActiveQuery $query */
        $query = $this->getCompanies()
            ->orderBy('created_at DESC');

        $this->load($params);

        if ($this->bankName == -1) {
            $query->andWhere(['checking_accountant.bank_name' => null]);
        } else {
            $query->andFilterWhere(['checking_accountant.bank_name' => $this->bankName]);
        }

        $query->andFilterWhere(['activation_type' => $this->activation_type]);

        $query->andFilterWhere(['registration_page_type_id' => $this->registrationPageType]);

        $query->andFilterWhere(['company_type_id' => $this->company_type_id]);

        if (mb_substr($this->activeTariff, 0, 2) == 'pr') {
            $companies = [];
            /* @var $subscribe Subscribe */
            $subscribe = Subscribe::findOne(str_replace('pr', '', $this->activeTariff));
            $duration = SubscribeHelper::getReadableDuration($subscribe);
            $query->andWhere([
                'and',
                [Subscribe::tableName() . '.tariff_id' => null],
                ['not', [Subscribe::tableName() . '.id' => null]],
            ]);
            /* @var $company Company */
            $cloneQuery = clone $query;
            foreach ($cloneQuery->all() as $company) {
                if (SubscribeHelper::getReadableDuration($company->activeSubscribe) == $duration) {
                    $companies[] = $company->id;
                }
            }
            $query->andWhere(['in', Company::tableName() . '.id', $companies]);

        } elseif ($this->activeTariff == 'free') {
            $query->andWhere(['not', [Company::tableName() . '.free_tariff_start_at' => null]]);
        } elseif ($this->activeTariff == 'no') {
            $query->andWhere([
                Company::tableName() . '.free_tariff_start_at' => null,
                Company::tableName() . '.active_subscribe_id' => null,
            ]);
        } else {
            $query->andFilterWhere([Subscribe::tableName() . '.tariff_id' => $this->activeTariff]);
        }

        if ($this->phone) {
            $query->andWhere(['and',
                    ['not', [Company::tableName() . '.phone' => null]],
                    ['!=', 'phone', ''],
                ]
            );
        } elseif ($this->phone === '0') {
            $query->andWhere(['phone' => null]);
        } else {
            $query->andFilterWhere(['phone' => $this->phone]);
        }

        if ($this->fio) {
            $query->andWhere(['or',
                ['not', ['chief_firstname' => null]],
                ['not', ['chief_lastname' => null]],
                ['not', ['chief_patronymic' => null]],
            ]);
        } elseif ($this->fio === '0') {
            $query->andWhere(['and',
                ['chief_firstname' => null],
                ['chief_lastname' => null],
                ['chief_patronymic' => null],
            ]);
        }

        if ($this->companyTaxationType) {
            switch ($this->companyTaxationType) {
                case 'osno':
                    $query->andWhere(['company_taxation_type.osno' => 1]);
                    break;
                case 'usn6':
                    $query->andWhere(['company_taxation_type.usn' => 1])->andWhere(['company_taxation_type.usn_percent' => 6]);
                    break;
                case 'usn15':
                    $query->andWhere(['company_taxation_type.usn' => 1])->andWhere(['company_taxation_type.usn_percent' => 15]);
                    break;
            }
        }

        return $query->all();
    }

    /**
     * @return array
     */
    public function getBankNameArray()
    {
        $statusArray[null] = 'Все';

        $statusArray += ArrayHelper::map($this->getCompanies()
            ->groupBy('checking_accountant.bank_name')
            ->orderBy('checking_accountant.bank_name')->all(), 'mainCheckingAccountant.bank_name', 'mainCheckingAccountant.bank_name');
        $statusArray[-1] = '(Не задано)';

        return $statusArray;
    }

    /**
     * @return array
     */
    public function getCompanyTypesArray()
    {
        $typesArray[null] = 'Все';

        $typesArray += ArrayHelper::map($this->getCompanies()
            ->groupBy('company_type.name_short')
            ->orderBy('company_type.name_short')->all(), 'companyType.id', 'companyType.name_short');

        return $typesArray;
    }

    /**
     * @return array
     */
    public function getCompanyTaxationTypesArray()
    {
        $typesArray[null] = 'Все';
        $typesArray['usn6'] = 'УСН 6%';
        $typesArray['usn15'] = 'УСН 15%';
        $typesArray['osno'] = 'ОСНО';

        return $typesArray;
    }

    /**
     * @return array
     */
    public function getRegistrationPageTypeFilter()
    {
        $registrationPageTypeArray[null] = 'Все';
        $registrationPageTypeArray += ArrayHelper::map($this->getCompanies()
            ->andWhere(['or',
                ['not', ['registration_page_type_id' => null]],
                ['not', ['registration_page_type_id' => '']],
            ])
            ->groupBy('registration_page_type_id')
            ->orderBy('registration_page_type.name')
            ->all(), 'registrationPageType.id', 'registrationPageType.name');

        return $registrationPageTypeArray;
    }

    /**
     * @return array
     */
    public function getUtmSourceFilter()
    {
        $utmSourceArray[null] = 'Все';
        $utmSourceArray += ArrayHelper::map($this->getCompanies()
            ->andWhere(['or',
                ['not', ['utm_source' => null]],
                ['not', ['utm_source' => '']],
            ])
            ->groupBy('utm_source')
            ->orderBy('utm_source')
            ->all(), 'utm_source', 'utm_source');

        return $utmSourceArray;
    }

    /**
     * @return mixed
     */
    public function getTariffNameFilter()
    {
        $statusArray = [];
        /* @var $company Company */
        foreach ($this->getCompanies()->all() as $company) {
            if ($company->hasActualSubscription) {
                if ($company->activeSubscribe->tariff) {
                    $statusArray[$company->activeSubscribe->tariff->id] = $company->activeSubscribe->getTariffName();
                } else {
                    $statusArray['pr' . $company->activeSubscribe->id] = 'Промокод (' . SubscribeHelper::getReadableDuration($company->activeSubscribe) . ')';
                }
            } elseif ($company->isFreeTariff) {
                $statusArray['free'] = 'Тариф "Бесплатно"';
            } else {
                $statusArray['no'] = 'Нет';
            }
        }
        $statusArray = array_unique($statusArray);
        natsort($statusArray);

        return [null => 'Все'] + $statusArray;
    }

    /**
     * @return array
     */
    public function getActivationStatusFilter()
    {
        $activationStatusArray = [];
        foreach ($this->getCompanies()->select('activation_type')->groupBy('activation_type')->column() as $activationTypeID) {
            if (isset(Company::$activationType[$activationTypeID])) {
                if ($activationTypeID == null) {
                    $activationStatusArray[-1] = Company::$activationType[$activationTypeID];
                } else {
                    $activationStatusArray[$activationTypeID] = Company::$activationType[$activationTypeID];
                }
            }
        }

        return [null => 'Все'] + $activationStatusArray;
    }

    /**
     * @return array
     */
    public function getPhoneFilter()
    {
        return [
            null => 'Все',
            1 => 'Есть телефон',
            0 => 'Нет телефона',
        ];
    }

    /**
     * @return array
     */
    public function getChiefFioFilter()
    {
        return [
            null => 'Все',
            1 => 'Есть ФИО',
            0 => 'Нет ФИО',
        ];
    }

    /**
     * @return array
     */
    public function getNameFilter()
    {
        return [
            null => 'Все',
            1 => 'Есть название',
            0 => 'Нет названия',
        ];
    }

    /**
     * @return CompanyQuery
     */
    public function getCompanies()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->isBlocked(self::UNBLOCKED)
            ->isTest(!self::TEST_COMPANY)
            ->andWhere(['between', 'date(from_unixtime(`' . self::tableName() . '`.`created_at`))', $dateRange['from'], $dateRange['to']])
            ->joinWith('mainAccountant')
            ->joinWith('activeSubscribe')
            ->joinWith('registrationPageType')
            ->joinWith('companyType')
            ->joinWith('companyTaxationType')
            ->leftJoin(SubscribeTariff::tableName(), [SubscribeTariff::tableName() . '.id' => Subscribe::tableName() . '.tariff_id']);
    }

    /**
     * @return array
     */
    public function getDownloadExcelUrl()
    {
        $getParams = \Yii::$app->request->get();
        $data[] = 'company-additional-excel';
        if (!empty($getParams) && is_array(current($getParams))) {
            foreach (current($getParams) as $attribute => $getParam) {
                $data['CompanyAdditionalSearch'][$attribute] = $getParam;
            }
        }

        return Url::to($data);
    }
}