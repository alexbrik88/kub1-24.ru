<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 7.12.15
 * Time: 11.46
 * Email: t.kanstantsin@gmail.com
 */

namespace backend\modules\company\models;

use common\models\service\Payment;
use common\models\service\PromoCode;
use common\models\service\Subscribe;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * PromoCodeSearchSearch represents the model behind the search form about `common\models\service\PromoCode`.
 */
class PromoCodeSearch extends PromoCode
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'tariff_group_id', 'for_company_experience_id', 'code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PromoCode::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'activated_at' => SORT_DESC,
                ],
                'attributes' => [
                    'activated_at',
                    'created_at',
                    'code',
                    'company_id',
                    'name',
                    'started_at',
                    'expired_at',
                ],
            ],
        ]);

        $this->load($params);

        $query
            ->from(['promocode' => PromoCode::tableName()])
            ->joinWith(['payments' => function (ActiveQuery $query) {
                return $query
                    ->from(['payments' => Payment::tableName(),])
                    ->joinWith(['subscribe' => function (ActiveQuery $query) {
                        return $query->from(['subscribe' => Subscribe::tableName(),]);
                    },], false);
            },], false);

        $query->andWhere(['OR',
            ['promocode.company_id' => $this->company_id],
            ['subscribe.company_id' => $this->company_id],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'group_id' => $this->group_id,
            'tariff_group_id' => $this->tariff_group_id,
            'for_company_experience_id' => $this->for_company_experience_id,
        ]);

        $query->andFilterWhere([
            'like', 'code', $this->code,
        ]);

        return $dataProvider;
    }
}