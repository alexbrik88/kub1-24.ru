<?php

namespace backend\modules\company\models;

use common\models\Company;
use common\models\employee\Employee;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * CompanySearch represents the model behind the search form about `backend\modules\company\models\CompanySearch`.
 */
class CompanySearch extends Company
{
    const ALL = 0;
    const BLOCKED_STATUS_INACTIVE = 1;
    const BLOCKED_STATUS_ACTIVE = 2;

    public $blockedStatus = self::BLOCKED_STATUS_INACTIVE;

    public static $blockedStatusToStatus = [
        self::ALL => null,
        self::BLOCKED_STATUS_ACTIVE => self::BLOCKED,
        self::BLOCKED_STATUS_INACTIVE => self::UNBLOCKED,
    ];
    /**
     * @var
     */
    public $search;
    /**
     * @var
     */
    public $activeTariff;

    protected $_query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blockedStatus'], 'in', 'range' => [
                self::BLOCKED_STATUS_INACTIVE,
                self::BLOCKED_STATUS_ACTIVE,
                self::ALL,
            ],],
            [['search'], 'safe'],
            [['activeTariff'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->getCompanyQuery();

        $cTable = $this->tableName();

        //  $dataProvider = new ActiveDataProvider([
        //      'query' => $query,
        //      'pagination' => [
        //          'pageSize' => 10,
        //      ],
        //      'sort' => [
        //          'defaultOrder' => [
        //              'id' => SORT_DESC,
        //          ],
        //          'attributes' => [
        //              'id',
        //              'name_short',
        //              'inn',
        //              'blocked',
        //              'activeSubscribeDaysCount',
        //          ],
        //      ],
        //  ]);

        $this->load($params);

        $query->andFilterWhere([
            $cTable . '.blocked' => self::$blockedStatusToStatus[(int)$this->blockedStatus],
        ]);

        if (mb_substr($this->activeTariff, 0, 2) == 'pr') {
            $companies = [];
            /* @var $subscribe Subscribe */
            $subscribe = Subscribe::findOne(str_replace('pr', '', $this->activeTariff));
            $duration = SubscribeHelper::getReadableDuration($subscribe);
            $query->andWhere([
                'and',
                [Subscribe::tableName() . '.tariff_id' => null],
                ['not', [Subscribe::tableName() . '.id' => null]],
            ]);
            /* @var $company Company */
            $cloneQuery = clone $query;
            foreach ($cloneQuery->all() as $company) {
                if (SubscribeHelper::getReadableDuration($company->activeSubscribe) == $duration) {
                    $companies[] = $company->id;
                }
            }
            $query->andWhere(['in', Company::tableName() . '.id', $companies]);

        } elseif ($this->activeTariff == 'free') {
            $query->andWhere(['not', [Company::tableName() . '.free_tariff_start_at' => null]]);
        } elseif ($this->activeTariff == 'no') {
            $query->andWhere([
                Company::tableName() . '.free_tariff_start_at' => null,
                Company::tableName() . '.active_subscribe_id' => null,
            ]);
        } else {
            $query->andFilterWhere([Subscribe::tableName() . '.tariff_id' => $this->activeTariff]);
        }

        if (filter_var($this->search, FILTER_VALIDATE_EMAIL)) {
            $employee = Employee::findOne(['email' => $this->search]);
            $query->andFilterWhere(['OR',
                [self::tableName() . '.email' => $this->search],
                $employee ?
                    [self::tableName() . '.id' => $employee->getCompanies()->select('company.id')->column()] :
                    null,
            ]);
        } else {
            $query->andFilterWhere(['OR',
                ['like', self::tableName() . '.inn', $this->search],
                ['like', self::tableName() . '.id', $this->search],
                ['like', self::tableName() . '.name_short', $this->search],
            ]);
        }

        $this->_query = $query;

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
                'attributes' => [
                    'id',
                    'name_short',
                    'inn',
                    'blocked',
                    'activeSubscribeDaysCount',
                    'notPaidInvoiceCount',
                ],
            ],
        ]);

        $dataProvider->getPagination();

        return $dataProvider;
    }

    public function getSubscribeFilter()
    {
        $statusArray = [];
        if ($query = $this->getQuery()) {
            /* @var $company Company */
            foreach ($query->all() as $company) {
                if ($company->hasActualSubscription) {
                    if ($company->activeSubscribe->tariff) {
                        $statusArray[$company->activeSubscribe->tariff->id] = $company->activeSubscribe->getTariffName();
                    } else {
                        $statusArray['pr' . $company->activeSubscribe->id] = 'Промокод (' . SubscribeHelper::getReadableDuration($company->activeSubscribe) . ')';
                    }
                } elseif ($company->isFreeTariff) {
                    $statusArray['free'] = 'Тариф "Бесплатно"';
                } else {
                    $statusArray['no'] = 'Нет';
                }
            }
            $statusArray = array_unique($statusArray);
            natsort($statusArray);
        }

        return [null => 'Все'] + $statusArray;
    }

    /**
     * @return ActiveQuery
     */
    public function getCompanyQuery()
    {
        //return self::find()->joinWith('activeSubscribe')->andWhere(['!=', 'main_id', 0]);
        return self::find()->joinWith('activeSubscribe');
    }

    /**
     * @return ActiveQuery|null
     */
    protected function getQuery()
    {
        return ($this->_query instanceof \yii\db\Query) ? clone $this->_query : null;
    }
}
