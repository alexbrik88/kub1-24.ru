<?php

namespace backend\modules\company\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\company\CompanyVisit;

/**
 * CompanyFirstEventSearch represents the model behind the search form about `common\models\company\CompanyFirstEvent`.
 */
class CompanyVisitSearch extends CompanyVisit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyVisit::find()
            ->select(['*', 'duration' => 'SEC_TO_TIME([[updated_at]] - [[created_at]])'])
            ->andWhere(['company_id' => $this->company_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'number' => SORT_ASC,
                ],
                'attributes' => [
                    'number',
                    'duration',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
