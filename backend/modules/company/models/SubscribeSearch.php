<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 3.12.15
 * Time: 16.09
 * Email: t.kanstantsin@gmail.com
 */

namespace backend\modules\company\models;

use common\components\helpers\ArrayHelper;
use common\models\service;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class SubscribeSearch
 * @package frontend\modules\subscribe\forms
 */
class SubscribeSearch extends service\Subscribe
{
    public $payment_type_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id', 'payment_type_id', 'tariff_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
                'attributes' => [
                    'created_at' => [
                        'asc' => ['subscribe.created_at' => SORT_ASC],
                        'desc' => ['subscribe.created_at' => SORT_DESC],
                    ],
                    'id',
                    'payment-created-at' => [
                        'asc' => ['payment.created_at' => SORT_ASC],
                        'desc' => ['payment.created_at' => SORT_DESC],
                    ],
                    'payment-sum' => [
                        'asc' => ['payment.sum' => SORT_ASC],
                        'desc' => ['payment.sum' => SORT_DESC],
                    ],
                    'payment_created_at' => [
                        'asc' => ['payment.created_at' => SORT_ASC],
                        'desc' => ['payment.created_at' => SORT_DESC],
                    ],
                ],
            ],
        ]);

        $this->load($params);

        $query->from([
            'subscribe' => service\Subscribe::tableName(),
        ]);

        $query->joinWith(['payment' => function (ActiveQuery $query) {
            $query->from(['payment' => service\Payment::tableName()]);
        }]);


        $query->andWhere([
            'subscribe.company_id' => $this->company_id,
        ]);

        $query->andFilterWhere([
            'payment.type_id' => $this->payment_type_id,
            'subscribe.tariff_id' => $this->tariff_id,
            'subscribe.status_id' => $this->status_id,
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        $typeArray = [
            'null' => 'Все',
        ];

        $typeArray += ArrayHelper::map(service\PaymentType::find()->all(), 'id', 'name');

        return $typeArray;
    }

    /**
     * @return array
     */
    public function getTariffList()
    {
        $typeArray = [
            'null' => 'Все',
        ];

        $typeArray += ArrayHelper::map(service\SubscribeTariff::find()->all(), 'id', function (service\SubscribeTariff $model) {
            return $model->getTariffName();
        });

        return $typeArray;
    }

    public function getPaymentTypeList()
    {
        $typeArray = [
            'null' => 'Все',
        ];

        $typeArray += ArrayHelper::map(service\PaymentType::find()->all(), 'id', 'name');

        return $typeArray;
    }

    public function getStatusList()
    {
        $typeArray = [
            'null' => 'Все',
        ];

        $typeArray += ArrayHelper::map(service\SubscribeStatus::find()->all(), 'id', 'name');

        return $typeArray;
    }
}
