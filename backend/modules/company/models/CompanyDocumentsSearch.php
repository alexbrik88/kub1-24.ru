<?php

namespace backend\modules\company\models;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;

/**
 * Class CompanyAdditionalSearch
 * @package backend\modules\company\models
 */
class CompanyDocumentsSearch extends Company
{
    /**
     * Creates data provider instance
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var ActiveQuery $query */
        $query = $this->getCompanies();

        $this->load($params);

        $allModels = array_filter($query->all(), function($model) {
            return ($model->notCreatedInvoice > 0);
        });

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
        ]);

        $dataProvider->sort = [
            'attributes' => [
                'id',
                'name_short',
                'email',
                'chiefFio',
                'created_at',
                'notCreatedInvoice',
                'notCreatedAct',
                'notCreatedPL',
                'notCreatedIF',
            ],
            'defaultOrder' => [
                'created_at' => SORT_DESC
            ],
        ];

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    public function getCompanies()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return self::find()
            ->isBlocked(self::UNBLOCKED)
            ->isTest(!self::TEST_COMPANY)
            ->innerJoin('invoice', '{{company}}.[[id]] = {{invoice}}.[[company_id]]')
            ->andWhere(['invoice.is_deleted' => false])
            ->andWhere(['between', 'invoice.document_date', $dateRange['from'], $dateRange['to']])
            ->groupBy('company.id');
    }
}