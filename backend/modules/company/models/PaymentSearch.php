<?php

namespace backend\modules\company\models;

use common\models\Company;
use common\models\document\Invoice;
use common\models\service\Payment;
use common\models\service\PaymentOrder;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeStatus;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/**
 * Class PaymentSearch
 * @package backend\modules\service\models
 */
class PaymentSearch extends Payment
{
    public $subscribe_id;
    public $invoice_number;
    public $tariff_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'tariff_id', 'is_confirmed'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $table = self::tableName();
        $subscribeTable = Subscribe::tableName();
        $invoiceTable = Invoice::tableName();
        $query = Payment::find()
            ->alias('payment')
            ->joinWith(['orders order', 'outInvoice invoice', 'subscribe subscribe'])
            ->andWhere([
                'or',
                ['payment.company_id' => $this->company_id],
                ['order.company_id' => $this->company_id],
            ])
            ->groupBy('payment.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                    'subscribe_id' => [
                        'asc' => ['subscribe.id' => SORT_ASC],
                        'desc' => ['subscribe.id' => SORT_DESC],
                    ],
                    'invoice_number' => [
                        'asc' => ['{{invoice}}.[[document_number]] * 1' => SORT_ASC],
                        'desc' => ['{{invoice}}.[[document_number]] * 1' => SORT_DESC],
                    ],
                    'sum' => [
                        'asc' => ['{{payment}}.[[sum]] * 1' => SORT_ASC],
                        'desc' => ['{{payment}}.[[sum]] * 1' => SORT_DESC],
                    ],
                    'payment_date',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'payment.type_id' => $this->type_id,
            'payment.is_confirmed' => $this->is_confirmed,
            'payment.tariff_id' => $this->tariff_id,
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(PaymentType::find()->orderBy('id')->all(), 'id', 'name'));
    }

    /**
     * @return array
     */
    public function getTariffList()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(SubscribeTariff::find()->all(), 'id', 'tariffName'));
    }

    /**
     * @return array
     */
    public function getPaymentTypeList()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(PaymentType::find()->orderBy('id')->all(), 'id', 'name'));
    }

    public function getStatusList()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(SubscribeStatus::find()->all(), 'id', 'name'));
    }
}