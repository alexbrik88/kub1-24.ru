<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 12/5/15
 * Time: 3:08 AM
 * Email: t.kanstantsin@gmail.com
 */

namespace backend\modules\company\models;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\file\File;
use common\models\product\Product;
use common\models\report\Report;
use common\models\report\ReportFile;
use frontend\models\Documents;
use frontend\modules\documents\models\SpecificDocument;
use yii\base\InvalidConfigException;
use yii\base\BaseObject;
use yii\db\Expression;
use yii\helpers\FileHelper;

/**
 * Class CompanyStatistic
 * @package backend\modules\company\models
 */
class CompanyStatistic extends BaseObject
{
    /**
     * @var Company
     */
    public $company;

    /**
     * @var array
     */
    public static $docTypeArray = [
        Documents::DOCUMENT_INVOICE => Documents::FILENAME_INVOICE,
        Documents::DOCUMENT_ACT => Documents::FILENAME_ACT,
        Documents::DOCUMENT_PACKING_LIST => Documents::FILENAME_PACKING_LIST,
        Documents::DOCUMENT_INVOICE_FACTURE => Documents::FILENAME_INVOICE_FACTURE,
    ];

    protected $_company_file_statistic = null;
    protected $_document_file_statistic = null;
    protected $_report_file_statistic = null;
    protected $_specific_file_statistic = null;

    public function init()
    {
        if ($this->company === null) {
            throw new InvalidConfigException('Company not set');
        }
    }

    public function getOutDocumentStat()
    {
        return $this->getDocumentStat(Documents::IO_TYPE_OUT) + [
            Documents::DOCUMENT_PAYMENT_ORDER =>
                (int) PaymentOrder::find()->byCompany($this->company->id)->count(),
        ];
    }

    public function getInDocumentStat()
    {
        return $this->getDocumentStat(Documents::IO_TYPE_IN);
    }

    protected function getDocumentStat($ioType)
    {
        return [
            Documents::DOCUMENT_INVOICE =>
                (int) Invoice::find()->byIOType($ioType)->byDeleted()->byCompany($this->company->id)->count(),
            Documents::DOCUMENT_ACT =>
                (int) Act::find()->byIOType($ioType)->byCompany($this->company->id)->count(),
            Documents::DOCUMENT_PACKING_LIST =>
                (int) PackingList::find()->byIOType($ioType)->byCompany($this->company->id)->count(),
            Documents::DOCUMENT_INVOICE_FACTURE =>
                (int) InvoiceFacture::find()->byIOType($ioType)->byCompany($this->company->id)->count(),
        ];
    }

    public static function directoryFileStatistic($path)
    {
        $result = [
            'count' => 0,
            'size' => 0,
        ];
        if (file_exists($path)) {
            $fileArray = FileHelper::findFiles($path);
            $result['count'] = count($fileArray);

            foreach ($fileArray as $file) {
                $result['size'] += filesize($file);
            }
        }

        return $result;
    }

    public static function scanDir($dir)
    {
        $result = [];
        if (is_dir($dir)) {
            $handle = opendir($dir);
            if ($handle === false) {
                throw new InvalidParamException("Unable to open directory: $dir");
            }
            while (($file = readdir($handle)) !== false) {
                if ($file === '.' || $file === '..') {
                    continue;
                }
                $path = $dir . DIRECTORY_SEPARATOR . $file;
                $result[$file] = $file == 'documents' ? self::scanDir($path) : self::directoryFileStatistic($path);
            }
            closedir($handle);
        }

        return $result;
    }

    public static function totalSize($statistic)
    {
        $result = 0;
        if (is_array($statistic)) {
            foreach ($statistic as $key => $value) {
                $result += isset($value['size']) ? $value['size'] : self::totalSize($value);
            }
        }

        return $result;
    }

    public function companyFileStatistic()
    {
        if ($this->_company_file_statistic === null) {
            $path = Company::fileUploadPath($this->company->id);
            $this->_company_file_statistic = self::scanDir($path);
        }

        return $this->_company_file_statistic;
    }

    public function documentFileStatistic()
    {
        if ($this->_document_file_statistic === null) {
            $this->_document_file_statistic = [];
            $statistic = ArrayHelper::getValue($this->companyFileStatistic(), 'documents', []);
            foreach (self::$docTypeArray as $key => $value) {
                $this->_document_file_statistic[$key] = ArrayHelper::getValue($statistic, $value, [
                    'count' => 0,
                    'size' => 0,
                ]);
            }
        }

        return $this->_document_file_statistic;
    }

    public function reportFileStatistic()
    {
        if ($this->_report_file_statistic === null) {
            $this->_report_file_statistic = ArrayHelper::getValue($this->companyFileStatistic(), 'report', [
                'count' => 0,
                'size' => 0,
            ]);
        }

        return $this->_report_file_statistic;
    }

    public function specificFileStatistic()
    {
        if ($this->_specific_file_statistic === null) {
            $statistic = ArrayHelper::getValue($this->companyFileStatistic(), 'documents', []);
            $this->_specific_file_statistic = ArrayHelper::getValue($statistic, 'specific', [
                'count' => 0,
                'size' => 0,
            ]);
        }

        return $this->_specific_file_statistic;
    }

    public function getFileUploadStat()
    {
        $statistic = $this->documentFileStatistic();
        $result = [];
        foreach ($statistic as $key => $value) {
            $result[$key] = $value['count'];
        }

        return $result;
    }

    public function getFileUploadSize()
    {
        $statistic = $this->documentFileStatistic();
        $result = [];
        foreach ((array) $statistic as $key => $value) {
            $result[$key] = round($value['size'] / 1048576, 3);
        }

        return $result;
    }

    public function getReportFileUploadSize()
    {
        return round($this->reportFileStatistic()['size'] / 1048576, 3);
    }

    public function getMyDocumentsFileUploadSize()
    {
        return round($this->specificFileStatistic()['size'] / 1048576, 3);
    }


    public function getAllFileUploadSize()
    {
        return round(self::totalSize($this->companyFileStatistic()) / 1048576, 3);
    }

    public function getContractorStat()
    {
        $contractorStat = Contractor::find()
            ->select(['type', new Expression('COUNT(*) AS count')])
            ->byCompany($this->company->id)
            ->byDeleted(false)
            ->groupBy('type')
            ->asArray()
            ->all();
        $contractorStat = ArrayHelper::map($contractorStat, 'type', 'count');

        return [
            Contractor::TYPE_SELLER => (int) ArrayHelper::getValue($contractorStat, Contractor::TYPE_SELLER, 0),
            Contractor::TYPE_CUSTOMER => (int) ArrayHelper::getValue($contractorStat, Contractor::TYPE_CUSTOMER, 0),
        ];
    }

    public function getProductStat()
    {
        $productStat = Product::find()
            ->select(['production_type', new Expression('COUNT(*) AS count')])
            ->byCompany($this->company->id)
            ->byDeleted(false)
            ->groupBy('production_type')
            ->asArray()
            ->all();
        $productStat = ArrayHelper::map($productStat, 'production_type', 'count');

        return [
            Product::PRODUCTION_TYPE_GOODS => (int) ArrayHelper::getValue($productStat, Product::PRODUCTION_TYPE_GOODS, 0),
            Product::PRODUCTION_TYPE_SERVICE => (int) ArrayHelper::getValue($productStat, Product::PRODUCTION_TYPE_SERVICE, 0),
        ];
    }

    public function getEmployeeStat()
    {
        $productStat = Employee::find()
            ->select(['employee_role_id', new Expression('COUNT(*) AS count')])
            ->byCompany($this->company->id)
            ->byIsDeleted(false)
            ->groupBy('employee_role_id')
            ->asArray()
            ->all();
        $productStat = ArrayHelper::map($productStat, 'employee_role_id', 'count');

        return ArrayHelper::map(EmployeeRole::find()->actual()->all(), 'id', function (EmployeeRole $role) use ($productStat) {
            return (int) ArrayHelper::getValue($productStat, $role->id, 0);
        });
    }

    public function getEmailMessagesFromInvoice()
    {
        return Invoice::find()
            ->select('SUM(email_messages)')
            ->byCompany($this->company->id)
            ->byDeleted()
            ->andWhere(['>', 'email_messages', 0])
            ->createCommand()
            ->queryScalar();
    }

    public function getEmailMessagesFromCompany()
    {
        $countMessages = 0;
        $messages = Company::find()
            ->select('email_messages_send_visit_card')
            ->where(['id' => $this->company->id])
            ->groupBy('email_messages_send_visit_card')
            ->asArray()
            ->all();
        for ($i = 0; $i < count($messages); $i++) {
            $countMessages += $messages[$i]['email_messages_send_visit_card'];
        }

        return $countMessages;
    }
}
