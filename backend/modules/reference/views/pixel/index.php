<?php

use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\models\employee\Employee;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\reference\models\PixelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пиксель';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="portlet box">
    <div class="filter">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <h3 class="page-title"><?= Html::encode($this->title) . ' (' . $dataProvider->totalCount . ')'; ?></h3>
</div>
<div class="row">
    <div class="col-sm-12" style="margin: 0 0 15px;">
        <?= Html::a('<i class="fa fa-file-excel-o"></i> Выгрузить в Excel', Url::current(['xls' => 1]), [
            'class' => 'btn yellow text-white pull-right',
            'title' => 'Выгрузить в Excel',
        ]); ?>
    </div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">

        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div
                class="dataTables_wrapper dataTables_extended_wrapper">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'style' => 'overflow-x: auto;',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        'created_at:date',
                        'el',
                        'email:email',
                        [
                            'attribute' => 'company_id',
                            'enableSorting' => false,
                            'class' => DropDownSearchDataColumn::className(),
                            'filter' => $searchModel->getCompanyFilter($dataProvider),
                            'value' => function ($model) {
                                return $model->company ? $model->company->getTitle(true) : '';
                            }
                        ],
                        [
                            'attribute' => 'is_client',
                            'enableSorting' => false,
                            'class' => DropDownSearchDataColumn::className(),
                            'format' => 'boolean',
                            'filter' => ['' => 'Все', 1 => 'Да', 0 => 'Нет'],
                        ],
                        [
                            'attribute' => 'is_ours',
                            'enableSorting' => false,
                            'class' => DropDownSearchDataColumn::className(),
                            'filter' => ['' => 'Все', 1 => 'Свой', 0 => 'Чужой'],
                            'value' => function ($model) {
                                $values = [
                                    1 => 'Свой',
                                    0 => 'Чужой',
                                ];
                                return $values[$model->is_ours];
                            }
                        ],
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'template' => '{delete}',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'buttons' => [
                                'delete' => function ($url, $model) {
                                    return \frontend\widgets\ConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                            'class' => '',
                                            'tag' => 'a',
                                        ],
                                        'confirmUrl' => $url,
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите удалить запись?',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>