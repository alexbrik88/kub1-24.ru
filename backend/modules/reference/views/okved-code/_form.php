<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\reference\OkvedCodeCategory;
use yii\helpers\ArrayHelper;
use common\models\reference\CodeOkvedSection;

/* @var $this yii\web\View */
/* @var $model common\models\reference\OkvedCode */
/* @var $form yii\widgets\ActiveForm */
/* @var $codeOkvedSection CodeOkvedSection */
?>
<div class="okved-code-form">
    <?php $form = ActiveForm::begin([
        'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->id],
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
        'validateOnBlur' => true,
    ]); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(OkvedCodeCategory::find()->all(), 'id', 'name')); ?>

    <div class="form-group field-okvedcode-section_id has-success">
        <label class="control-label" for="okvedcode-section_id">Раздел</label>
        <select id="okvedcode-section_id" class="form-control" name="OkvedCode[section_id]" aria-invalid="false">
            <option data-description="" value=""></option>
            <?php foreach (CodeOkvedSection::find()->all() as $codeOkvedSection): ?>
                <option data-description="<?= $codeOkvedSection->description; ?>"
                        value="<?= $codeOkvedSection->id; ?>"
                    <?= $codeOkvedSection->id == $model->section_id ? 'selected' : null; ?>><?= $codeOkvedSection->name; ?></option>
            <?php endforeach; ?>
        </select>

        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'sectionDescription')->textInput(['disabled' => true]); ?>

    <?= $form->field($model, 'class')->textInput(['maxlength' => true]); ?>

    <?= $form->field($model, 'class_name')->textInput(['maxlength' => true]); ?>

    <?= $form->field($model, 'class_description')->textarea(['rows' => 6]); ?>

    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'data-dismiss' => 'modal',
                ]); ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                    'data-dismiss' => 'modal',
                ]); ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJs('
    $(document).ready(function () {
        $(".field-okvedcode-section_id select").change(function () {
            $(this).closest("form").find("#okvedcode-sectiondescription").val($(this).find("option[value=\"" + $(this).val() + "\"]").data("description"));
        });
    });
'); ?>
