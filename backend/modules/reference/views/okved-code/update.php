<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\reference\OkvedCode */

$this->title = 'Update Okved Code: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Okved Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="okved-code-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
