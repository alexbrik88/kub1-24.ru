<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\reference\OkvedCode */

$this->title = 'Create Okved Code';
$this->params['breadcrumbs'][] = ['label' => 'Okved Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="okved-code-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
