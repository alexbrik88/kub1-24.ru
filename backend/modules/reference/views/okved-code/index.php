<?php

use yii\helpers\Html;
use common\components\grid\GridView;
use backend\modules\reference\models\OkvedCodeSearch;
use yii\bootstrap\Modal;
use common\models\reference\OkvedCode;
use common\components\grid\DropDownSearchDataColumn;
use yii\bootstrap\ActiveForm;
use common\models\reference\CodeOkvedSection;

/* @var $this yii\web\View */
/* @var $searchModel OkvedCodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Коды ОКВЭД';
?>
    <div class="portlet box">
        <div class="btn-group pull-right title-buttons">
            <?php Modal::begin([
                'header' => '<h3 style="text-align: center; margin: 0">Добавить код ОКВЭД</h3>',
                'toggleButton' => [
                    'label' => '<i class="fa fa-plus"></i> ДОБАВИТЬ',
                    'class' => 'btn yellow',
                ],
            ]);
            echo $this->render('_form', [
                'model' => new OkvedCode(),
            ]);
            Modal::end(); ?>
        </div>
        <h3 class="page-title"><?= Html::encode($this->title); ?></h3>
    </div>
    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <?php $form = ActiveForm::begin([
                        'action' => ['index'],
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input text-middle" style="width: 83%;">
                            <?= $form->field($searchModel, 'search')->textInput([
                                'placeholder' => 'Поиск по Коду и названию',
                            ]); ?>
                        </div>
                        <div class="wimax_button" style="float: right;">
                            <?= Html::submitButton('НАЙТИ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                                'style' => 'width:100%;',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div class="dataTables_wrapper dataTables_extended_wrapper">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'style' => 'overflow-x: auto;',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'label' => '№',
                                'headerOptions' => [
                                    'width' => '5%',
                                ],
                                'value' => function (OkvedCode $model) {
                                    return $model->id;
                                },
                            ],
                            [
                                'attribute' => 'code',
                                'label' => 'Код',
                                'headerOptions' => [
                                    'width' => '10%',
                                ],
                                'value' => function (OkvedCode $model) {
                                    return $model->code;
                                },
                            ],
                            [
                                'attribute' => 'name',
                                'label' => 'Название',
                                'headerOptions' => [
                                    'width' => '40%',
                                ],
                                'value' => function (OkvedCode $model) {
                                    return $model->name;
                                },
                            ],
                            [
                                'class' => DropDownSearchDataColumn::className(),
                                'attribute' => 'category_id',
                                'label' => 'Категория',
                                'headerOptions' => [
                                    'width' => '40%',
                                    'class' => 'dropdown-filter',
                                ],
                                'filter' => $searchModel->getCategoryFilter(),
                                'value' => function (OkvedCode $model) {
                                    return $model->category->name;
                                },
                            ],
                            [
                                'class' => DropDownSearchDataColumn::className(),
                                'attribute' => 'section_id',
                                'label' => 'Раздел',
                                'headerOptions' => [
                                    'width' => '40%',
                                    'class' => 'dropdown-filter',
                                ],
                                'filter' => $searchModel->getSectionFilter(),
                                'value' => function (OkvedCode $model) {
                                    return $model->section ? $model->section->name : '';
                                },
                            ],
                            [
                                'attribute' => CodeOkvedSection::tableName() . '.description',
                                'label' => 'Описание раздела',
                                'headerOptions' => [
                                    'width' => '40%',
                                ],
                                'format' => 'raw',
                                'value' => function (OkvedCode $model) {
                                    return $model->section ?
                                        ('<span title="' . $model->section->description . '">' . mb_substr($model->section->description, 0, 100) . '...</span>') :
                                        '';
                                },
                            ],
                            [
                                'attribute' => 'class',
                                'label' => 'Класс',
                                'headerOptions' => [
                                    'width' => '40%',
                                ],
                                'value' => function (OkvedCode $model) {
                                    return $model->class ? $model->class : '';
                                },
                            ],
                            [
                                'attribute' => 'class_name',
                                'label' => 'Название класса',
                                'headerOptions' => [
                                    'width' => '40%',
                                ],
                                'value' => function (OkvedCode $model) {
                                    return $model->class_name ? $model->class_name : '';
                                },
                            ],
                            [
                                'attribute' => 'class_description',
                                'label' => 'Описание класса',
                                'headerOptions' => [
                                    'width' => '40%',
                                ],
                                'format' => 'raw',
                                'value' => function (OkvedCode $model) {
                                    return $model->class_description ?
                                        ('<span title="' . $model->class_description . '">' . mb_substr($model->class_description, 0, 100) . '...</span>') :
                                        '';
                                },
                            ],
                            [
                                'class' => \yii\grid\ActionColumn::className(),
                                'template' => '{update} {delete}',
                                'headerOptions' => [
                                    'width' => '5%',
                                ],
                                'buttons' => [
                                    'update' => function ($url, OkvedCode $model) {
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#update-code-okved-' . $model->id, [
                                            'data-toggle' => 'modal',
                                            'title' => Yii::t('yii', 'Обновить'),
                                            'aria-label' => Yii::t('yii', 'Обновить'),
                                        ]);
                                    },
                                    'delete' => function ($url, OkvedCode $model) {
                                        return \frontend\widgets\ConfirmModalWidget::widget([
                                            'toggleButton' => [
                                                'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                                'class' => '',
                                                'tag' => 'a',
                                            ],
                                            'confirmUrl' => $url,
                                            'confirmParams' => [],
                                            'message' => 'Вы уверены, что хотите удалить код ОКВЭД <br>"' . $model->name . '" ?',
                                        ]);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?php foreach ($dataProvider->models as $model) {
    Modal::begin([
        'id' => 'update-code-okved-' . $model->id,
        'header' => '<h3 style="text-align: center; margin: 0">Обновить код ОКВЭД</h3>',
        'toggleButton' => false,
    ]);
    echo $this->render('_form', [
        'model' => $model,
    ]);
    Modal::end();
} ?>