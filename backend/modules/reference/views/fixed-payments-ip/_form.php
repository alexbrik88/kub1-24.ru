<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\reference\FixedPaymentsIp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fixed-payments-ip-form">

    <?php $form = ActiveForm::begin([
        'action' => $model->getIsNewRecord() ? ['create'] : ['update', 'id' => $model->year],
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'year')->textInput() ?>

    <?= $form->field($model, 'form_pfr')->textInput() ?>

    <?= $form->field($model, 'form_oms')->textInput() ?>

    <?= $form->field($model, 'form_limit')->textInput() ?>

    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'data-dismiss' => 'modal',
                ]); ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                    'data-dismiss' => 'modal',
                ]); ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
