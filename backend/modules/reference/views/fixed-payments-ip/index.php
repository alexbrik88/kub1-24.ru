<?php

use common\components\grid\GridView;
use common\models\reference\FixedPaymentsIp;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\reference\models\FixedPaymentsIpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фикс. платежи по ИП';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?php
        Modal::begin([
            'id' => 'create-new-item',
            'header' => '<h1>Добавить Фикс. платежи по ИП</h1>',
            'toggleButton' => [
                'label' => '<i class="fa fa-plus"></i> ДОБАВИТЬ',
                'class' => 'btn yellow',
            ],
        ]);

        echo $this->render('_form', [
            'model' => new FixedPaymentsIp(['year' => FixedPaymentsIp::find()->max('year')+1]),
        ]);

        Modal::end();
        ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'style' => 'overflow-x: auto;',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'columns' => [
                        'year',
                        [
                            'attribute' => 'pfr',
                            'value' => function ($model) {
                                return number_format($model->form_pfr, 2, '.', ' ');
                            },
                        ],
                        [
                            'attribute' => 'oms',
                            'value' => function ($model) {
                                return number_format($model->form_oms, 2, '.', ' ');
                            },
                        ],
                        [
                            'attribute' => 'limit',
                            'value' => function ($model) {
                                return number_format($model->form_limit, 2, '.', ' ');
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#update-item-' . $model->year, [
                                        'data-toggle' => 'modal',
                                        'title' => Yii::t('yii', 'Изменить'),
                                        'aria-label' => Yii::t('yii', 'Изменить'),
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php foreach ($dataProvider->models as $model) {
    Modal::begin([
        'id' => 'update-item-' . $model->year,
        'header' => '<h1>Изменить Фикс. платежи по ИП</h1>',
        'toggleButton' => false,
    ]);

    echo $this->render('_form', [
        'model' => $model,
    ]);

    Modal::end();
} ?>
