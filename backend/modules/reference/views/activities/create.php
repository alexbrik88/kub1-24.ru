<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\company\CompanyType */

$this->title = 'Создать вид деятельности';
?>
<div class="company-type-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
