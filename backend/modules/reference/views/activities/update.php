<?php

use yii\helpers\Html;
use common\models\company\Attendance;

/* @var $this yii\web\View */
/* @var $model Attendance */

$this->title = 'Обновить вид деятельности: ' . $model->id;
?>
<div class="company-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
