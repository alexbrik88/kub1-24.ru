<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.05.2017
 * Time: 17:10
 */

use common\models\company\Activities;
use yii\bootstrap\Html;
use common\components\grid\GridView;
use common\widgets\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\reference\models\ActivitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Виды деятельности';
?>
    <div class="portlet box">
        <div class="btn-group pull-right title-buttons">
            <?php Modal::begin([
                'header' => '<h3 style="text-align: center; margin: 0">Добавить вид деятельности</h3>',
                'toggleButton' => [
                    'label' => '<i class="fa fa-plus"></i> ДОБАВИТЬ',
                    'class' => 'btn yellow',
                ],
            ]);
            echo $this->render('_form', [
                'model' => new Activities(),
            ]);
            Modal::end(); ?>
        </div>
        <h3 class="page-title"><?= Html::encode($this->title); ?></h3>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div
                    class="dataTables_wrapper dataTables_extended_wrapper">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'style' => 'overflow-x: auto;',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'label' => '№',
                                'headerOptions' => [
                                    'width' => '20%',
                                ],
                                'value' => function (Activities $model) {
                                    return $model->id;
                                },
                            ],
                            [
                                'attribute' => 'name',
                                'label' => 'Наименование',
                                'headerOptions' => [
                                    'width' => '75%',
                                ],
                                'value' => function (Activities $model) {
                                    return $model->name;
                                },
                            ],
                            [
                                'class' => \yii\grid\ActionColumn::className(),
                                'template' => '{update} {delete}',
                                'headerOptions' => [
                                    'width' => '5%',
                                ],
                                'buttons' => [
                                    'update' => function ($url, Activities $model) {
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#update-activities-' . $model->id, [
                                            'data-toggle' => 'modal',
                                            'title' => Yii::t('yii', 'Обновить'),
                                            'aria-label' => Yii::t('yii', 'Обновить'),
                                        ]);
                                    },
                                    'delete' => function ($url, Activities $model) {
                                        return \frontend\widgets\ConfirmModalWidget::widget([
                                            'toggleButton' => [
                                                'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                                'class' => '',
                                                'tag' => 'a',
                                            ],
                                            'confirmUrl' => $url,
                                            'confirmParams' => [],
                                            'message' => 'Вы уверены, что хотите удалить вид деятельности <br>"' . $model->name . '" ?',
                                        ]);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?php foreach ($dataProvider->models as $model) {
    Modal::begin([
        'id' => 'update-activities-' . $model->id,
        'header' => '<h3 style="text-align: center; margin: 0">Обновить вид деятельности</h3>',
        'toggleButton' => false,
    ]);
    echo $this->render('_form', [
        'model' => $model,
    ]);
    Modal::end();
} ?>