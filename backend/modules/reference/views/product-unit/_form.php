<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\product\ProductUnit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-unit-form">

    <?php $form = ActiveForm::begin([
        'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->id],
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
        'validateOnBlur' => true,
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code_okei')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'intl_short_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'intl_code')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'goods')->checkbox() ?>

    <?= $form->field($model, 'services')->checkbox() ?>

    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'data-dismiss' => 'modal',
                ]); ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                    'data-dismiss' => 'modal',
                ]); ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
