<?php

use common\components\grid\GridView;
use common\models\product\ProductUnit;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\reference\models\ProductUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Единицы измерения';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?php
        Modal::begin([
            'header' => '<h3 style="text-align: center; margin: 0">Добавить единицу измерения</h3>',
            'toggleButton' => [
                'label' => '<i class="fa fa-plus"></i> ДОБАВИТЬ',
                'class' => 'btn yellow',
            ],
        ]);

        echo $this->render('_form', [
            'model' => new ProductUnit,
        ]);

        Modal::end();
        ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'style' => 'overflow-x: auto;',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'columns' => [
                        // ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'name',
                        'title',
                        'code_okei',
                        'intl_short_name',
                        'intl_code',
                        [
                            'attribute' =>'goods',
                            'format' => 'boolean',
                            'filter' => ['' => 'Все', 1 => 'Да', 0 => 'Нет']
                        ],
                        [
                            'attribute' =>'services',
                            'format' => 'boolean',
                            'filter' => ['' => 'Все', 1 => 'Да', 0 => 'Нет']
                        ],
                        'object_guid',
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'template' => '{update} {delete}',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'buttons' => [
                                'update' => function ($url, $data) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#update-type-' . $data->id, [
                                        'data-toggle' => 'modal',
                                        'title' => Yii::t('yii', 'Обновить'),
                                        'aria-label' => Yii::t('yii', 'Обновить'),
                                    ]);
                                },
                                'delete' => function ($url, $data) {
                                    return \frontend\widgets\ConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                            'class' => '',
                                            'tag' => 'a',
                                        ],
                                        'confirmUrl' => $url,
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите удалить единицу измерения<br>"' . $data->name . '" ?',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php foreach ($dataProvider->models as $model) {
    Modal::begin([
        'id' => 'update-type-' . $model->id,
        'header' => '<h3 style="text-align: center; margin: 0">Обновить единицу измерения</h3>',
        'toggleButton' => false,
    ]);

    echo $this->render('_form', [
        'model' => $model,
    ]);

    Modal::end();
} ?>