<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\reference\models\CrmKubResponsibleForm */

?>

<style type="text/css">
    label.control-label {
        font-weight: bold;
    }
    #crmkubresponsibleform-responsible label {
        display: block;
    }
</style>

<h3 class="mb-5">
    Укажите, кого ставить ответсвенным по Автозагруженным клиентам по ФинДиректору
</h3>

<div class="reference-crm-update">

    <?php $form = ActiveForm::begin([
        'id' => 'reference-crm-update-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'responsible')->checkboxList($model->getResponsibleData()) ?>

    <div class="d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue text-white',
        ]); ?>
        <?= Html::a('Отменить', ['index'], [
            'class' => 'btn darkblue text-white',
        ]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>