<?php

use common\components\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ответсвенные по Автозагруженным клиентам по ФинДиректору';
?>

<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('Изменить', ['update'], [
            'class' => 'btn yellow',
        ]) ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'style' => 'overflow-x: auto;',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                        ],
                        [
                            'label' => 'ФИО',
                            'value' => 'fio'
                        ],
                        [
                            'label' => 'Роль',
                            'value' => 'employeeRole.name'
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>