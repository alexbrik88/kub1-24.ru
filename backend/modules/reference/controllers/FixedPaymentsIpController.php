<?php

namespace backend\modules\reference\controllers;

use Yii;
use common\models\reference\FixedPaymentsIp;
use backend\modules\reference\models\FixedPaymentsIpSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FixedPaymentsIpController implements the CRUD actions for FixedPaymentsIp model.
 */
class FixedPaymentsIpController extends BackendController
{
    /**
     * Lists all FixedPaymentsIp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FixedPaymentsIpSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FixedPaymentsIp model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FixedPaymentsIp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FixedPaymentsIp();

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['index']);
    }

    /**
     * Updates an existing FixedPaymentsIp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['index']);
    }

    /**
     * Finds the FixedPaymentsIp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FixedPaymentsIp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FixedPaymentsIp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
