<?php

namespace backend\modules\reference\controllers;

use backend\components\BackendController;
use backend\modules\reference\models\CrmKubResponsibleForm;
use backend\modules\reference\models\CrmKubResponsibleSearch;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CrmController
 */
class CrmController extends BackendController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'dataProvider' => (new CrmKubResponsibleSearch(Yii::$app->kubCompany))->search(),
        ]);
    }

    /**
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = new CrmKubResponsibleForm(Yii::$app->kubCompany);

        if (Yii::$app->request->post('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
