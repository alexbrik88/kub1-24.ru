<?php

namespace backend\modules\reference\controllers;

use backend\components\BackendController;
use backend\modules\reference\models\PixelSearch;
use common\models\reference\Pixel;
use frontend\components\PageSize;
use frontend\components\StatisticPeriod;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;

/**
 * PixelController implements the CRUD actions for Pixel model.
 */
class PixelController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pixel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();
        $searchModel = new PixelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);

        if (Yii::$app->request->get('xls')) {
            Yii::$app->response->format = Response::FORMAT_RAW;
            $filename = 'Пиксель';
            $dataProvider->pagination->pageSize = -1;

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
            header('Cache-Control: max-age=0');
            $writer = $searchModel->getExcelWriter($dataProvider);
            $writer->save('php://output');
            return;
        }

        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dateRange' => $dateRange,
        ]);
    }

    /**
     * Displays a single Pixel model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing Pixel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pixel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Pixel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pixel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
