<?php

namespace backend\modules\reference\models;

use common\models\Company;
use common\models\EmployeeCompany;
use common\models\company\CompanyType;
use common\models\employee\Employee;
use common\models\reference\Pixel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PixelSearch represents the model behind the search form about `common\models\reference\Pixel`.
 */
class PixelSearch extends Pixel
{
    public $is_client;
    public $is_ours;
    public $isClient;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'email', 'is_client', 'is_ours'], 'safe'],
            [['company_id', 'created_at', 'isClient'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'company_id' => 'От компании',
            'created_at' => 'Дата',
            'isClient' => 'Стал клиентом',
            'is_client' => 'Стал клиентом',
            'is_ours' => 'Получатель',
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param array $dateRange ['from' => 'Y-m-d', 'to' => 'Y-m-d']
     *
     * @return ActiveDataProvider
     */
    public function search($params, $dateRange)
    {
        $from = date_create_from_format('Y-m-d', $dateRange['from'])->setTime(0, 0)->getTimestamp();
        $to = date_create_from_format('Y-m-d', $dateRange['to'])->setTime(23, 59, 59)->getTimestamp();

        $query = self::find()->alias('p')
            ->select([
                'p.*',
                'is_client' => 'IF({{e}}.[[id]] IS NULL, 0, 1)',
                'is_ours' => 'IF({{ec}}.[[employee_id]] IS NOT NULL OR {{c}}.[[email]] = {{p}}.[[email]], 1, 0)',
            ])
            ->joinWith(['company c', 'client e' => function ($q) {
                $q->andOnCondition(['e.is_deleted' => false]);
            }], false)
            ->leftJoin(
                ['ec' => EmployeeCompany::tableName()],
                '{{ec}}.[[company_id]] = {{p}}.[[company_id]] AND {{ec}}.[[employee_id]] = {{e}}.[[id]]'
            )
            ->andWhere(['between', 'p.created_at', $from, $to]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
                'attributes' => [
                    'created_at',
                    'el',
                    'email',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $countQuery = clone $query;
        $countQuery->andFilterWhere([
            'IF({{e}}.[[id]] IS NULL, 0, 1)' => $this->is_client,
            'IF({{ec}}.[[employee_id]] IS NOT NULL OR {{c}}.[[email]] = {{p}}.[[email]], 1, 0)' => $this->is_ours,
        ]);
        $dataProvider->totalCount = (int) $countQuery->limit(-1)->offset(-1)->orderBy([])->count();

        $query->andFilterWhere([
            'p.company_id' => $this->company_id,
        ]);
        $query->andFilterHaving([
            'is_client' => $this->is_client,
            'is_ours' => $this->is_ours,
        ]);


        return $dataProvider;
    }

    public function getCompanyFilter($dataProvider)
    {
        $query = clone $dataProvider->query;

        return ['' => 'Все'] + Company::find()->alias('c')
            ->joinWith('companyType t', false)
            ->where(['c.id' => $query->select([
                'p.company_id',
                'is_client' => 'IF({{e}}.[[id]] IS NULL, 0, 1)',
                'is_ours' => 'IF({{ec}}.[[employee_id]] IS NOT NULL OR {{c}}.[[email]] = {{p}}.[[email]], 1, 0)',
            ])->column()])
            ->select([
                'name' => 'IF({{t}}.[[id]] = ' . CompanyType::TYPE_IP . ',
                    CONCAT({{t}}.[[name_short]], " ", {{c}}.[[name_short]]),
                    CONCAT({{t}}.[[name_short]], " \"", {{c}}.[[name_short]], "\"")
                )',
                'c.id',
            ])
            ->orderBy([
                'IF({{t}}.[[name_short]] IS NULL, 1, 0)' => SORT_ASC,
                't.name_short' => SORT_ASC,
                'c.name_short' => SORT_ASC,
            ])
            ->indexBy('id')
            ->column();
    }

    /**
     * @param $sortParam
     *
     * @return mixed
     */
    public function getExcelWriter(ActiveDataProvider $dataProvider, $type = 'Xlsx')
    {
        $modelArray = $dataProvider->models;
        $count = $dataProvider->count;
        $dataArray = [
            [
                'Дата',
                'El',
                'Email',
                'От компании',
                'Стал клиентом',
                'Получатель',
            ],
        ];
        foreach ($modelArray as $model) {
            $dataArray[] = [
                date('d.m.Y', $model->created_at),
                $model->el,
                $model->email,
                $model->company ? $model->company->getTitle(true) : '',
                $model->is_client ? 'Да' : 'Нет',
                $model->is_ours ? 'Свой' : 'Чужой',
            ];
        }

        $i = $count + 1;
        $objPHPExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(18);
        $objPHPExcel->getActiveSheet()->fromArray($dataArray);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:I{$i}")->applyFromArray([
            'borders' => array(
                'allborders'     => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
            ),
        ]);

        return \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, $type);
    }
}
