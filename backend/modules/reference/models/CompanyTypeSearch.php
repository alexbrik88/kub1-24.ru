<?php

namespace backend\modules\reference\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\company\CompanyType;

/**
 * CompanyTypeSearch represents the model behind the search form about `common\models\company\CompanyType`.
 */
class CompanyTypeSearch extends CompanyType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'in_company', 'in_contractor', 'like_ip'], 'integer'],
            [['name_short', 'name_full'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'in_company' => $this->in_company,
            'in_contractor' => $this->in_contractor,
            'like_ip' => $this->like_ip,
        ]);

        $query->andFilterWhere(['like', 'name_short', $this->name_short])
            ->andFilterWhere(['like', 'name_full', $this->name_full]);

        return $dataProvider;
    }
}
