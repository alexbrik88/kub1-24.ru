<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 08.11.2017
 * Time: 10:16
 */

namespace backend\modules\reference\models;


use common\components\helpers\ArrayHelper;
use common\models\reference\CodeOkvedSection;
use common\models\reference\OkvedCode;
use common\models\reference\OkvedCodeCategory;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class OkvedCodeSearch
 * @package backend\modules\reference\models
 */
class OkvedCodeSearch extends OkvedCode
{
    /**
     * @var
     */
    public $search;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'safe'],
            [['category_id', 'section_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OkvedCode::find()->joinWith('section');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'code',
                    'name',
                    CodeOkvedSection::tableName() . '.description',
                    'class',
                    'class_name',
                    'class_description',
                ],
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere(['category_id' => $this->category_id]);

        $query->andFilterWhere(['section_id' => $this->section_id]);

        $query->andFilterWhere(['OR',
            ['like', self::tableName() . '.code', $this->search],
            ['like', self::tableName() . '.name', $this->search],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getCategoryFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(OkvedCodeCategory::find()->all(), 'id', 'name'));
    }

    /**
     * @return array
     */
    public function getSectionFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(CodeOkvedSection::find()->all(), 'id', 'name'));
    }
}