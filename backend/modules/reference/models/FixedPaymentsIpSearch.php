<?php

namespace backend\modules\reference\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\reference\FixedPaymentsIp;

/**
 * FixedPaymentsIpSearch represents the model behind the search form about `common\models\reference\FixedPaymentsIp`.
 */
class FixedPaymentsIpSearch extends FixedPaymentsIp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'pfr', 'oms'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FixedPaymentsIp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = [
            'year' => SORT_DESC,
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'year' => $this->year,
            'pfr' => $this->pfr,
            'oms' => $this->oms,
        ]);

        return $dataProvider;
    }
}
