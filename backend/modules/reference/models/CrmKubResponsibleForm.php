<?php

namespace backend\modules\reference\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use common\models\Company;
use common\models\CrmKubResponsible;

/**
 * CrmKubResponsibleForm
 */
class CrmKubResponsibleForm extends Model
{
    private $company;
    private $employeeArray;
    private $responsibleArray;

    public $responsible;

    /**
     * Class constructor
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
        $this->employeeArray = $company->getEmployeeCompanies()->alias('employee_company')->joinWith([
            'employee employee',
            'crmKubResponsible',
        ])->andWhere([
            'employee_company.is_working' => true,
            'employee.is_deleted' => false,
        ])->orderBy([
            'employee_company.lastname' => SORT_ASC,
            'employee_company.firstname' => SORT_ASC,
            'employee_company.patronymic' => SORT_ASC,
        ])->indexBy('employee_id')->all();

        foreach ($this->employeeArray as $employee) {
            $isActive = ArrayHelper::getValue($employee, ['crmKubResponsible', 'is_responsible'], 0);
            if ($isActive) {
                $this->responsible[] = $employee->employee_id;
            }
        }

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['responsible', 'validateResponsible', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function validateResponsible($attribute, $params)
    {
        $empty = true;

        if (!empty($this->$attribute) && is_array($this->$attribute)) {
            foreach ($this->$attribute as $key => $value) {
                if (isset($this->employeeArray[$value])) {
                    $empty = false;
                } else {
                    $this->$attribute[$key];
                }
            }
        }

        if ($empty) {
            $this->addError($attribute, 'Необходимо выбрать хотя бы одного сотрудника');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'responsible' => 'Ответственный',
        ];
    }

    /**
     * @return bool
     */
    public function getResponsibleData()
    {
        return ArrayHelper::map($this->employeeArray, 'employee_id', 'fio');
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $rows = [];
        foreach ($this->responsible as $employee_id) {
            $rows[] = [$employee_id, 0, 1];
        }

        return \Yii::$app->db->transaction(function (\yii\db\Connection $db) use ($rows) {
            CrmKubResponsible::updateAll(['is_responsible' => false]);

            $sql = $db->queryBuilder->batchInsert(CrmKubResponsible::tableName(), [
                'employee_id',
                'last_time_at',
                'is_responsible',
            ], $rows) . ' ON DUPLICATE KEY UPDATE [[is_responsible]] = VALUES([[is_responsible]])';

            if ($db->createCommand($sql)->execute()) {
                return true;
            } else {
                $db->transaction->rollBack();

                return false;
            }
        });
    }
}
