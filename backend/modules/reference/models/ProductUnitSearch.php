<?php

namespace backend\modules\reference\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\product\ProductUnit;

/**
 * ProductUnitSearch represents the model behind the search form about `common\models\product\ProductUnit`.
 */
class ProductUnitSearch extends ProductUnit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'goods', 'services'], 'integer'],
            [['name', 'code_okei', 'object_guid'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductUnit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'goods' => $this->goods,
            'services' => $this->services,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code_okei', $this->code_okei])
            ->andFilterWhere(['like', 'object_guid', $this->object_guid]);

        return $dataProvider;
    }
}
