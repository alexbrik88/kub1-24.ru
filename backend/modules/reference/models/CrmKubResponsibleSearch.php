<?php

namespace backend\modules\reference\models;

use Yii;
use common\models\Company;
use common\models\CrmKubResponsible;
use common\models\EmployeeCompany;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * CrmKubResponsibleSearch
 */
class CrmKubResponsibleSearch extends EmployeeCompany
{
    private $company;

    /**
     * Class constructor
     */
    public function __construct(Company $company)
    {
        $this->company = $company;

        parent::__construct();
    }

    /**
     * @return bool
     */
    public function search()
    {
        $query = CrmKubResponsible::getResponsibleQuery()->orderBy([
            'employee_company.lastname' => SORT_ASC,
            'employee_company.firstname' => SORT_ASC,
            'employee_company.patronymic' => SORT_ASC,
        ])->with(['employeeRole']);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
