<?php

namespace backend\modules\employee\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EmployeeSearch represents the model behind the search form about `common\models\employee\Employee`.
 */
class EmployeeSearch extends Employee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'created_at', 'is_email_confirmed', 'author_id', 'sex', 'is_working', 'employee_role_id', 'notify_nearly_report', 'notify_new_features', 'is_active', 'is_deleted', 'is_registration_completed', 'time_zone_id', 'wp_id', 'main_company_id', 'alert_close_count', 'alert_close_time', 'knows_about_sending_invoice', 'need_look_service_video', 'socket_id', 'chat_volume', 'duplicate_notification_to_sms', 'duplicate_notification_to_email', 'push_notification_new_message', 'push_notification_create_closest_document', 'push_notification_overdue_invoice', 'show_scan_popup'], 'integer'],
            [['email', 'email_confirm_key', 'password', 'password_reset_token', 'auth_key', 'lastname', 'firstname', 'firstname_initial', 'patronymic', 'patronymic_initial', 'birthday', 'date_hiring', 'date_dismissal', 'position', 'phone', 'view_notification_date', 'my_companies', 'statistic_range_date_from', 'statistic_range_date_to', 'statistic_range_name', 'access_token', 'chat_photo', 'push_token'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employee::find();

        $query->joinWith('employeeCompany')->andWhere(['employee.is_deleted' => false])->groupBy('employee.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'employee.id' => $this->id,
            'employee_company.company_id' => $this->company_id,
            'employee.sex' => $this->sex,
            'employee.birthday' => $this->birthday,
            'employee.is_active' => $this->is_active,
            'employee.is_deleted' => $this->is_deleted,
            'employee.time_zone_id' => $this->time_zone_id,
        ]);

        $query->andFilterWhere(['like', 'employee.email', $this->email])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'patronymic', $this->patronymic])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
