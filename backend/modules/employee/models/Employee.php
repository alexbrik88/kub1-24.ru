<?php

namespace backend\modules\employee\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property string birthdayInput
 *
 */
class Employee extends \common\models\employee\Employee
{
    const SCENARIO_ADMIN = 'admin';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_ADMIN => [
                'is_active',
                'phone',
                'lastname',
                'firstname',
                'patronymic',
                'phone',
                'sex',
                'birthday',
                'birthdayInput',
                'time_zone_id',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lastname', 'firstname', 'patronymic', 'phone'], 'trim'],
            [['lastname', 'firstname', 'patronymic'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 20],
            [['sex', 'company_id', 'time_zone_id'], 'integer'],
            [['is_active'], 'boolean'],
            [['birthday'], 'date', 'format' => 'php:Y-m-d'],
            [['birthdayInput'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @param string $value
     */
    public function setBirthdayInput($value)
    {
        if (is_string($value)) {
            $this->birthday = implode('-', array_reverse(explode('.', $value)));
        }
    }

    /**
     * @return string
     */
    public function getBirthdayInput()
    {
        if ($this->birthday) {
            return implode('.', array_reverse(explode('-', $this->birthday)));
        }

        return $this->birthday;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'email' => 'Email',
            'password' => 'Пароль',
            'created_at' => 'Дата создания',
            'name' => 'ФИО',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'firstname_initial' => 'Инициал имени',
            'patronymic' => 'Отчество',
            'patronymic_initial' => 'Инициал отчества',
            'sex' => 'Пол',
            'birthday' => 'Дата рождения',
            'date_hiring' => 'Дата приёма на работу',
            'date_dismissal' => 'Дата увольнения',
            'position' => 'Должность',
            'is_active' => 'Вход разрешен',
            'is_deleted' => 'Удалён',
            'is_working' => 'Работает',
            'employee_role_id' => 'Роль',
            'phone' => 'Телефон',
            'notify_nearly_report' => 'Оповещать о приближающихся отчётах',
            'notify_new_features' => 'Оповещать о новых функциях сервиса',
            'time_zone_id' => 'Часовой пояс',
            'view_notification_date' => 'Дата просмотра новостей',
            'last_visit_at' => 'Дата последней активности',
            'wp_id' => 'Wp ID',
            'duplicate_notification_to_sms' => 'Дублировать уведомления по SMS',
            'duplicate_notification_to_email' => 'Дублировать уведомления по электронной почте',
            'push_notification_new_message' => 'Push-уведомление по новому сообщению в чате',
            'push_notification_create_closest_document' => 'Push-уведомление по необходимости создания закрывающего документа',
            'push_notification_overdue_invoice' => 'Push-уведомление по появлению просроченного счета',
            'push_token' => 'Push token',
        ];
    }
}
