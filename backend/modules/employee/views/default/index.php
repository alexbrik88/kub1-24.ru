<?php

use common\components\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\employee\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover dataTable',
            'role' => 'grid',
        ],
        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'columns' => [
            'id',
            'email:email',
            [
                'attribute' => 'company_id',
                'value' => function ($model) {
                    return $model->company ? $model->company->getTitle(true) : '';
                }
            ],
            [
                'attribute' => 'lastname',
                'value' => function ($model) {
                    return $model->lastname ? : '';
                }
            ],
            [
                'attribute' => 'firstname',
                'value' => function ($model) {
                    return $model->firstname ? : '';
                }
            ],
            [
                'attribute' => 'patronymic',
                'value' => function ($model) {
                    return $model->patronymic ? : '';
                }
            ],
            'created_at:datetime',
            'is_active:boolean',
            // 'sex',
            // 'birthday',
            // 'date_hiring',
            // 'date_dismissal',
            // 'position',
            // 'is_working',
            // 'employee_role_id',
            // 'phone',
            // 'notify_nearly_report',
            // 'notify_new_features',
            // 'is_active',
            // 'is_deleted',
            // 'is_registration_completed',
            // 'time_zone_id:datetime',
            // 'view_notification_date',
            // 'last_visit_at',
            // 'wp_id',
            // 'main_company_id',
            // 'my_companies',
            // 'alert_close_count',
            // 'alert_close_time:datetime',
            // 'statistic_range_date_from',
            // 'statistic_range_date_to',
            // 'statistic_range_name',
            // 'knows_about_sending_invoice',
            // 'need_look_service_video',
            // 'access_token',
            // 'socket_id',
            // 'chat_photo',
            // 'chat_volume',
            // 'duplicate_notification_to_sms',
            // 'duplicate_notification_to_email:email',
            // 'push_notification_new_message',
            // 'push_notification_create_closest_document',
            // 'push_notification_overdue_invoice',
            // 'show_scan_popup',
            // 'push_token',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return $this->render('update', ['model' => $model, 'action' => $url]);
                    },
                    'delete' => function ($url, $model) {
                        return \frontend\widgets\ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                'class' => '',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => $url,
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить пользователя <br>"' . $model->email . '" ?',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
