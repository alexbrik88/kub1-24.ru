<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\employee\Employee */
/* @var $action string */

$modalId = 'update-employee-' . $model->id;

Modal::begin([
    'id' => $modalId,
    'header' => Html::tag('h1', 'Редактирование пользователя: ' . $model->id),
    'toggleButton' => [
        'label' => '<span class="glyphicon glyphicon-pencil"></span>',
        'tag' => 'a',
        'href' => '#' . $modalId,
        'title' => Yii::t('yii', 'Обновить'),
        'aria-label' => Yii::t('yii', 'Обновить'),
    ],
]);

echo $this->render('_form', ['model' => $model, 'action' => $action]);

Modal::end();
