<?php

use backend\modules\employee\models\Employee;
use common\components\date\DateHelper;
use common\models\TimeZone;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\employee\Employee */
/* @var $form yii\widgets\ActiveForm */

$companyItems = ArrayHelper::map($model->companies, 'id', function ($model) {
    return $model->getTitle(true);
});
?>

<style type="text/css">
    #employee-sex {
        margin-left: -20px;
    }
    .form-control {
        width: 100%;
    }
</style>

<div class="employee-form">

    <?php $form = ActiveForm::begin([
        'id' => 'employee-form-' . $model->id,
        'action' => $action,
        'enableAjaxValidation' => true,
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?= $form->field($model, 'company_id')->dropDownList($companyItems, ['prompt' => '']) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sex')->radioList(Employee::$sex_message, [
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'value' => $value,
                'label' => $label,
                'labelOptions' => [
                    'class' => 'radio-inline',
                ],
            ]);
        },
    ]) ?>

    <?= $form->field($model, 'birthday')->textInput([
        'class' => 'form-control date-picker',
        'value' => DateHelper::format($model->birthday, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
    ]); ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'time_zone_id')->dropDownList(ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone')) ?>

    <div class="row">
        <div class="col-sm-6">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn darkblue text-white',
                'style' => 'width: 110px;',
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::button('Отмена', [
                'class' => 'btn darkblue text-white pull-right',
                'style' => 'width: 110px;',
                'data' => [
                    'dismiss' => 'modal'
                ]
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
