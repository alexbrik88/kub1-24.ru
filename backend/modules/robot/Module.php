<?php

namespace backend\modules\robot;

/**
 * robot module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\robot\controllers';

    /**
     * @inheritdoc
     */
    public $layout = 'robot';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
