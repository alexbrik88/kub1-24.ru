<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Ifns */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $pjax = Pjax::begin([
    'id' => 'ifns-upload-form-pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
]); ?>

<style type="text/css">
#ifnsuploadform-file-styler {
    height: 70px;
}
.ifns-upload-form .jq-file__name {
    position: absolute;
    top: 40px;
    left: 0;
    padding: 0;
}
.ifns-upload-form .help-block {
    margin: 0;
}
</style>

<div class="ifns-upload-form">
    <?php $form = ActiveForm::begin([
        'id' => 'ifns-upload-form',
        'action' => ['upload'],
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => true,
        ],
    ]); ?>

    <div class="row" style="">
        <div class="col-xs-8">
            <?= $form->field($model, 'file', ['options' => ['class' => '']])->fileInput([
                'class' => 'ifns-file-upload',
                'data-button-text' => 'Выбрать файл',
                'accept' => 'text/csv',
            ])->label(false) ?>
        </div>
        <div class="col-xs-4" style="text-align: right;">
            <div style="position: relative; display: inline-block; color: #fff;">
                <?= Html::submitButton('<i class="fa fa-download"></i> Загрузить', [
                    'id' => 'ifns-upload-submit',
                    'class' => 'btn darkblue',
                ]) ?>
                <div id="ifns-upload-loader" style="display: none;"></div>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
</div>

<?php $pjax->end(); ?>