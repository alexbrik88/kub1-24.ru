<?php

use common\components\grid\GridView;
use common\models\IfnsUploadLog;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\robot\models\IfnsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Справочник ИФНС';
$lastUpload = IfnsUploadLog::find()->orderBy(['created_at' => SORT_DESC])->one();
$lastUploadFile = $lastUpload ? $lastUpload->file_name : null;
$lastUploadTime = $lastUpload ? $lastUpload->created_at : null;
$this->registerJs('
var initUploadButtonStyle = function() {
    $(".ifns-file-upload").styler({"fileBrowse" : "Выбрать файл"});
}
$(document).on("pjax:end", "#ifns-upload-form-pjax", function() {
  initUploadButtonStyle();
});
initUploadButtonStyle();
$(document).on("pjax:send", "#ifns-upload-form-pjax", function() {
    $("#ifns-upload-loader").show();
})
$(document).on("pjax:complete", "#ifns-upload-form-pjax", function() {
    $("#ifns-upload-loader").hide();
})
$("#import-csv").on("hidden.bs.modal", function() {
    $.pjax.reload("#ifns-upload-form-pjax", {"push": false, replace: false, url: $("#ifns-upload-form").attr("action")});
})
');
?>

<div class="ifns-content">
    <div class="row portlet box" style="margin-left: 0;margin-right: 0;">
        <div class="btn-group pull-right title-buttons">
            <?= Html::button('<i class="fa fa-download" style="padding-left: 10px;"></i> Загрузить CSV', [
                'class' => 'btn yellow w100proc no-padding',
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#import-csv',
                ],
                'style' => 'width: 200px;',
            ]); ?>
        </div>
        <div class="modal fade" id="import-csv" tabindex="-1" role="modal"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <?= Html::button('', [
                            'class' => 'close',
                            'data' => [
                                'dismiss' => 'modal',
                            ],
                            'aria-hidden' => true,
                        ]); ?>
                        <h1>Загрузка CSV файла</h1>
                    </div>
                    <div class="modal-body">
                        <?= $this->render('upload_form', ['model' => $form]) ?>
                        <?= Html::a(
                            'Скачать последнюю базу ИФНС',
                            'https://www.nalog.ru/opendata/7707329152-address/',
                            ['target' => '_blank']
                        ) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet box darkblue blk_wth_srch">
        <div class="portlet-title row-fluid">
            <div class="caption list_recip col-sm-12">
                <?= $this->title . ($lastUploadFile ? " ($lastUploadFile)" : '') ?>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container ">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'rowOptions' => function($model, $key, $index, $grid) use ($lastUploadTime) {
                        return ['class' => $lastUploadTime && $model->updated_at >= $lastUploadTime ? 'changed' : ''];
                    },
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
                    'columns' => [
                        'ga',
                        'gb',
                        [
                            'attribute' => 'g1',
                            'format' => 'html',
                            'value' => function($model) {
                                return implode(', ', array_filter(explode(',', $model->g1)));
                            },
                        ],
                        'g2',
                        'g3',
                        'g4',
                        [
                            'attribute' => 'g5',
                            'value' => function($model) {
                                return implode(' ', explode(',', $model->g5));
                            },
                        ],
                        'g6',
                        'g7',
                        'g8',
                        'g9',
                        'g10',
                        'g11',
                        'g12',
                        'g13',
                        [
                            'attribute' => 'g14',
                            'format' => 'html',
                            'value' => function($model) {
                                return implode(', ', array_filter(explode(',', $model->g1)));
                            },
                        ],
                        'g15',
                        'g16',
                        'g18',
                        'g19',
                        'g20',
                        'g21',
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
