<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@backend/views/layouts/main.php');
?>
    <div class="robot-ifns-content container-fluid" style="padding: 0; margin-top: -10px;">
        <?php NavBar::begin([
            'brandLabel' => 'Робот Бухгалтер',
            'brandUrl' => null,
            'options' => [
                'class' => 'navbar-report navbar-default',
            ],
            'brandOptions' => [
                'style' => 'margin-left: 0;'
            ],
            'innerContainerOptions' => [
                'class' => 'container-fluid',
                'style' => 'padding: 0;'
            ],
        ]);
        echo Nav::widget([
            'items' => [
                ['label' => 'Справочник ИФНС', 'url' => ['/robot/ifns/index']],
            ],
            'options' => ['class' => 'navbar-nav navbar-right'],
        ]);
        NavBar::end();
        ?>
        <?= $content; ?>
    </div>
<?php $this->endContent(); ?>