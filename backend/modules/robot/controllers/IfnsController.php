<?php

namespace backend\modules\robot\controllers;

use Yii;
use common\models\Ifns;
use backend\components\BackendController;
use backend\modules\robot\models\IfnsSearch;
use backend\modules\robot\models\IfnsUploadForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IfnsController implements the CRUD actions for Ifns model.
 */
class IfnsController extends BackendController
{
    /**
     * Lists all Ifns models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IfnsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();
        $form = new IfnsUploadForm();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'form' => $form,
        ]);
    }
    /**
     * Lists all Ifns models.
     * @return mixed
     */
    public function actionUpload()
    {
        $form = new IfnsUploadForm();

        if ($form->load() && $form->updateIfns()) {
            return $this->redirect(['index']);
        }

        return $this->render('upload_form', [
            'model' => $form,
        ]);
    }
}
