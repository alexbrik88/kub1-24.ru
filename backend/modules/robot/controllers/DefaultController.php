<?php

namespace backend\modules\robot\controllers;

use backend\components\BackendController;

/**
 * Default controller for the `robot` module
 */
class DefaultController extends BackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
