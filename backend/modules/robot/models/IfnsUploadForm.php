<?php

namespace backend\modules\robot\models;

use common\models\Ifns;
use common\models\IfnsUploadLog;
use Yii;
use yii\base\Model;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * IfnsSearch represents the model behind the search form about `common\models\Ifns`.
 */
class IfnsUploadForm extends Model
{
    public $file;
    public static $attr = [
        0 => 'ga',
        1 => 'gb',
        2 => 'g1',
        3 => 'g2',
        4 => 'g3',
        5 => 'g4',
        6 => 'g5',
        7 => 'g6',
        8 => 'g7',
        9 => 'g8',
        10 => 'g9',
        11 => 'g10',
        12 => 'g11',
        13 => 'g12',
        14 => 'g13',
        15 => 'g14',
        16 => 'g15',
        17 => 'g16',
        18 => 'g17',
        19 => 'g18',
        20 => 'g19',
        21 => 'g20',
        22 => 'g21',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => ['csv'], 'checkExtensionByMimeType' => false],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл CSV',
        ];
    }

    /**
     * @return array
     */
    public function load($data = null, $formName = null)
    {
        if ($this->file = UploadedFile::getInstance($this, 'file')) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function updateIfns()
    {
        if (!$this->validate()) {
            return false;
        }

        $changed = 0;
        $badLines = [];
        $errors = [];
        $catch = false;

        if (($handle = fopen($this->file->tempName, "r")) !== false) {
            $num = 1;
            while (($line = fgetcsv($handle, 0, ";")) !== false) {
                if (count($line) != 23) {
                    $this->addError('file', 'Не соответствует структура файла');

                    return false;
                }

                if ($num === 1) {
                    $uploadLog = new IfnsUploadLog([
                        'file_name' => $this->file->name,
                    ]);
                    $uploadLog->save();
                }

                $bad = true;
                if ($attributes = array_combine(static::$attr, $line)) {
                    if (is_numeric($attributes['ga'])) {
                        if (($model = Ifns::findOne($attributes['ga'])) === null) {
                            $model = new Ifns();
                        }

                        $model->setAttributes($attributes, false);
                        $model->ga *= 1;
                        if ($model->isNewRecord || $model->getDirtyAttributes(static::$attr)) {
                            if ($model->save()) {
                                $changed++;
                                $bad = false;
                            } else {
                                $errors[$num] = $model->errors;
                            }
                        } else {
                            $bad = false;
                        }
                    } elseif ($attributes['ga'] == 'GA') {
                        $bad = false;
                    }
                }
                if ($bad) {
                    $badLines[] = $num;
                }
                $num++;
            }
            fclose($handle);
        }

        Yii::$app->session->setFlash('success', 'Количество обновленных записей: ' . $changed);
        if ($badLines) {
            Yii::$app->session->setFlash('error', 'Не удалось обработать строки: ' . implode(', ', $badLines));
        }

        return true;
    }
}
