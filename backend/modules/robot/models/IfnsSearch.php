<?php

namespace backend\modules\robot\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Ifns;

/**
 * IfnsSearch represents the model behind the search form about `common\models\Ifns`.
 */
class IfnsSearch extends Ifns
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ga', 'created_at', 'updated_at'], 'integer'],
            [['gb', 'g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'g7', 'g8', 'g9', 'g10', 'g11', 'g12', 'g13', 'g14', 'g15', 'g16', 'g18', 'g19', 'g20', 'g21'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ifns::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ga' => $this->ga,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'gb', $this->gb])
            ->andFilterWhere(['like', 'g1', $this->g1])
            ->andFilterWhere(['like', 'g2', $this->g2])
            ->andFilterWhere(['like', 'g3', $this->g3])
            ->andFilterWhere(['like', 'g4', $this->g4])
            ->andFilterWhere(['like', 'g5', $this->g5])
            ->andFilterWhere(['like', 'g6', $this->g6])
            ->andFilterWhere(['like', 'g7', $this->g7])
            ->andFilterWhere(['like', 'g8', $this->g8])
            ->andFilterWhere(['like', 'g9', $this->g9])
            ->andFilterWhere(['like', 'g10', $this->g10])
            ->andFilterWhere(['like', 'g11', $this->g11])
            ->andFilterWhere(['like', 'g12', $this->g12])
            ->andFilterWhere(['like', 'g13', $this->g13])
            ->andFilterWhere(['like', 'g14', $this->g14])
            ->andFilterWhere(['like', 'g15', $this->g15])
            ->andFilterWhere(['like', 'g16', $this->g16])
            ->andFilterWhere(['like', 'g18', $this->g18])
            ->andFilterWhere(['like', 'g19', $this->g19])
            ->andFilterWhere(['like', 'g20', $this->g20])
            ->andFilterWhere(['like', 'g21', $this->g21]);

        return $dataProvider;
    }
}
