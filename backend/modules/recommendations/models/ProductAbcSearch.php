<?php

namespace backend\modules\recommendations\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\product\ProductRecommendationAbc;

/**
 * ProductRecommendationAbcSearch represents the model behind the search form of `common\models\product\ProductRecommendationAbc`.
 */
class ProductAbcSearch extends ProductRecommendationAbc
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'advertising', 'discount', 'selling_price', 'out_from_range', 'created_at', 'updated_at'], 'integer'],
            [['key', 'action', 'purchase_price', 'irreducible_quantity_txt', 'advertising_txt', 'discount_txt', 'action_txt', 'selling_price_txt', 'out_from_range_txt', 'purchase_price_txt'], 'safe'],
            [['irreducible_quantity'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductRecommendationAbc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'irreducible_quantity' => $this->irreducible_quantity,
            'advertising' => $this->advertising,
            'discount' => $this->discount,
            'selling_price' => $this->selling_price,
            'out_from_range' => $this->out_from_range,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'purchase_price', $this->purchase_price])
            ->andFilterWhere(['like', 'irreducible_quantity_txt', $this->irreducible_quantity_txt])
            ->andFilterWhere(['like', 'advertising_txt', $this->advertising_txt])
            ->andFilterWhere(['like', 'discount_txt', $this->discount_txt])
            ->andFilterWhere(['like', 'action_txt', $this->action_txt])
            ->andFilterWhere(['like', 'selling_price_txt', $this->selling_price_txt])
            ->andFilterWhere(['like', 'out_from_range_txt', $this->out_from_range_txt])
            ->andFilterWhere(['like', 'purchase_price_txt', $this->purchase_price_txt]);

        return $dataProvider;
    }
}
