<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recommendations\models\ProductAbcSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-recommendation-abc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'key') ?>

    <?= $form->field($model, 'irreducible_quantity') ?>

    <?= $form->field($model, 'advertising') ?>

    <?= $form->field($model, 'discount') ?>

    <?php // echo $form->field($model, 'action') ?>

    <?php // echo $form->field($model, 'selling_price') ?>

    <?php // echo $form->field($model, 'out_from_range') ?>

    <?php // echo $form->field($model, 'purchase_price') ?>

    <?php // echo $form->field($model, 'irreducible_quantity_txt') ?>

    <?php // echo $form->field($model, 'advertising_txt') ?>

    <?php // echo $form->field($model, 'discount_txt') ?>

    <?php // echo $form->field($model, 'action_txt') ?>

    <?php // echo $form->field($model, 'selling_price_txt') ?>

    <?php // echo $form->field($model, 'out_from_range_txt') ?>

    <?php // echo $form->field($model, 'purchase_price_txt') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
