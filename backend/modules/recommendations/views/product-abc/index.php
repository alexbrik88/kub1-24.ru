<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recommendations\models\ProductAbcSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары ABC';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-recommendation-abc-index">

    <div class="portlet box">
        <?php /*<div class="btn-group pull-right title-buttons">
            <?= Html::a('<i class="fa fa-plus"></i> Добавить', Url::to(['create']), ['class' => 'btn yellow']); ?>
        </div>*/ ?>

        <h3 class="page-title"><?= $this->title; ?></h3>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div class="dataTables_wrapper dataTables_extended_wrapper">
                    <?= \common\components\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'style' => 'overflow-x: auto;',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                        'columns' => [
                            'id',
                            [
                                'attribute' => 'key',
                                'label' => 'Комбинация',
                                'format' => 'raw',
                                'value' => function ($data) {

                                    return Html::a($data['key'], Url::toRoute(['/recommendations/product-abc/view', 'id' => $data['id']]));
                                }
                            ],
                            [
                                'attribute' => 'irreducible_quantity',
                                'label' => 'Неснижаемый остаток',
                                'format' => 'raw',
                                'value' => function ($data) {

                                    return round($data['irreducible_quantity'], 1);
                                },
                            ],
                            'advertising',
                            'discount',
                            'action',
                            'selling_price',
                            'out_from_range',
                            'purchase_price',
                            //'irreducible_quantity_txt:ntext',
                            //'advertising_txt:ntext',
                            //'discount_txt:ntext',
                            //'action_txt:ntext',
                            //'selling_price_txt:ntext',
                            //'out_from_range_txt:ntext',
                            //'purchase_price_txt:ntext',
                            //'created_at',
                            //'updated_at',
                            [
                                'class' => \yii\grid\ActionColumn::className(),
                                'template' => '{update} {delete}',
                                'headerOptions' => [
                                    'width' => '2%',
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>
