<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\product\ProductRecommendationAbc */

$this->title = 'Обновить рекомендацию: ' . $model->key;
?>
<div class="product-recommendation-abc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
