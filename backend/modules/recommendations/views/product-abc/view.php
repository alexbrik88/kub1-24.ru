<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\product\ProductRecommendationAbc */

$this->title = 'Рекомендация ' . $model->key;

\yii\web\YiiAsset::register($this);
?>

<style>
    table.detail-view td p { margin-bottom:0; }
    table.detail-view th { width: 25%; }
    table.detail-view td { width: 75%; }
</style>

<div class="product-recommendation-abc-view">

    <?= Html::a('Назад к списку', Url::toRoute(['/recommendations/product-abc']), ['class' => 'back-to-customers',]); ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <h3>Табличная часть</h3><br/>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'irreducible_quantity',
            'advertising',
            'discount',
            'action',
            'selling_price',
            'out_from_range',
            'purchase_price'
        ]
    ]); ?>

    <h3>Рекоммендации</h3><br/>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'irreducible_quantity_txt',
                'format' => 'raw',
            ],
            [
                'attribute' => 'advertising_txt',
                'format' => 'raw',
            ],
            [
                'attribute' => 'discount_txt',
                'format' => 'raw',
            ],
            [
                'attribute' => 'action_txt',
                'format' => 'raw',
            ],
            [
                'attribute' => 'selling_price_txt',
                'format' => 'raw',
            ],
            [
                'attribute' => 'out_from_range_txt',
                'format' => 'raw',
            ],
            [
                'attribute' => 'purchase_price_txt',
                'format' => 'raw',
            ]
        ]
    ]) ?>

</div>
