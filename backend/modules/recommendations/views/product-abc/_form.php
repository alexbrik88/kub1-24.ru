<?php

use common\widgets\FormButtonsWidget;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\product\ProductRecommendationAbc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-recommendation-abc-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row hidden">
        <div class="col-sm-6">
            <?= $form->field($model, 'key')->textInput(['maxlength' => true, 'readonly' => true]) ?>
        </div>
    </div>


    <br/><h4 style="font-weight: bold">Табличная часть</h4><br/>

    <?= $form->field($model, 'irreducible_quantity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'advertising')->textInput() ?>

    <?= $form->field($model, 'discount')->textInput() ?>

    <?= $form->field($model, 'action')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'selling_price')->textInput() ?>

    <?= $form->field($model, 'out_from_range')->textInput() ?>

    <?= $form->field($model, 'purchase_price')->textInput(['maxlength' => true]) ?>

    <br/><h4 style="font-weight: bold">Рекоммендации</h4><br/>

    <?php $textFields = [
        'irreducible_quantity_txt',
        'advertising_txt',
        'discount_txt',
        'action_txt',
        'selling_price_txt',
        'out_from_range_txt',
        'purchase_price_txt'
    ]; ?>

    <?php foreach ($textFields as $textField): ?>
        <?= $form->field($model, $textField)->widget(TinyMce::className(), [
            'options' => [
                'rows' => 3,
                'tag' => false,
            ],
            'language' => 'ru',
            'clientOptions' => [
                'menubar' => false,
                'statusbar' => false,
                'plugins' => join(',', [
                    'preview', 'paste', 'contextmenu', 'autoresize',
                    'textcolor',
                    'hr', 'table',
                    'charmap',
                    'link',
                ]),
                'external_plugins' => [],

                'contextmenu' => 'cut copy paste | link image inserttable | cell row column deletetable',
                'paste_as_text' => true,
                'resize' => 'both',
                'object_resizing' => 'img',
                'autoresize_max_height' => 300,
                'relative_urls' => false, // for jbimages plugin
                'convert_urls' => true,
                'paste_webkit_styles' => "font-style text-align",
                'toolbar' => "code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                'content_style' => "p {margin-top:0;margin-bottom:0}",
                'forced_root_block' => false,
                'force_br_newlines' => true,
                'force_p_newlines' => false,

            ],
        ]); ?>
    <?php endforeach; ?>

    <?= FormButtonsWidget::widget(); ?>

    <?php ActiveForm::end(); ?>

</div>
