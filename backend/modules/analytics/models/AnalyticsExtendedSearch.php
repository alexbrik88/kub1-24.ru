<?php

namespace backend\modules\analytics\models;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\CompanyQuery;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use frontend\components\StatisticPeriod;
use frontend\rbac\permissions\Service;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\Url;

/**
 * Class AnalyticsExtendedSearch
 * @package backend\modules\analytics\models
 */
class AnalyticsExtendedSearch extends Company
{
    /**
     * @var
     */
    public $bankName;
    /**
     * @var
     */
    public $activeTariff;
    /**
     * @var
     */
    public $registrationPageType;

    /**
     * @var
     */
    public $companyTaxationType;

    /**
     * @var
     */
    public $fio;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bankName', 'activeTariff', 'companyTaxationType'], 'string'],
            [['phone', 'activation_type', 'fio', 'registrationPageType', 'company_type_id'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        /* @var ActiveQuery $query */
        $query = $this->getCompanies()
            ->orderBy(['company.created_at' => SORT_DESC]);

        $this->load($params);

        if ($this->bankName == -1) {
            $query->andWhere(['checking_accountant.bank_name' => null]);
        } else {
            $query->andFilterWhere(['checking_accountant.bank_name' => $this->bankName]);
        }

        $query->andFilterWhere(['activation_type' => $this->activation_type]);

        $query->andFilterWhere(['registration_page_type_id' => $this->registrationPageType]);

        $query->andFilterWhere(['company_type_id' => $this->company_type_id]);

        if (mb_substr($this->activeTariff, 0, 2) == 'pr') {
            $companies = [];
            /* @var $subscribe Subscribe */
            $subscribe = Subscribe::findOne(str_replace('pr', '', $this->activeTariff));
            $duration = SubscribeHelper::getReadableDuration($subscribe);
            $query->andWhere([
                'and',
                [Subscribe::tableName() . '.tariff_id' => null],
                ['not', [Subscribe::tableName() . '.id' => null]],
            ]);
            /* @var $company Company */
            $cloneQuery = clone $query;
            foreach ($cloneQuery->all() as $company) {
                if (SubscribeHelper::getReadableDuration($company->activeSubscribe) == $duration) {
                    $companies[] = $company->id;
                }
            }
            $query->andWhere(['in', Company::tableName() . '.id', $companies]);

        } elseif ($this->activeTariff == 'free') {
            $query->andWhere(['not', [Company::tableName() . '.free_tariff_start_at' => null]]);
        } elseif ($this->activeTariff == 'no') {
            $query->andWhere([
                Company::tableName() . '.free_tariff_start_at' => null,
                Company::tableName() . '.active_subscribe_id' => null,
            ]);
        } else {
            $query->andFilterWhere([Subscribe::tableName() . '.tariff_id' => $this->activeTariff]);
        }

        if ($this->phone) {
            $query->andWhere(['and',
                    ['not', [Company::tableName() . '.phone' => null]],
                    ['!=', 'phone', ''],
                ]
            );
        } elseif ($this->phone === '0') {
            $query->andWhere(['phone' => null]);
        } else {
            $query->andFilterWhere(['phone' => $this->phone]);
        }

        if ($this->fio) {
            $query->andWhere(['or',
                ['not', ['chief_firstname' => null]],
                ['not', ['chief_lastname' => null]],
                ['not', ['chief_patronymic' => null]],
            ]);
        } elseif ($this->fio === '0') {
            $query->andWhere(['and',
                ['chief_firstname' => null],
                ['chief_lastname' => null],
                ['chief_patronymic' => null],
            ]);
        }

        if ($this->companyTaxationType) {
            switch ($this->companyTaxationType) {
                case 'osno':
                    $query->andWhere(['company_taxation_type.osno' => 1]);
                    break;
                case 'usn6':
                    $query->andWhere(['company_taxation_type.usn' => 1])->andWhere(['company_taxation_type.usn_percent' => 6]);
                    break;
                case 'usn15':
                    $query->andWhere(['company_taxation_type.usn' => 1])->andWhere(['company_taxation_type.usn_percent' => 15]);
                    break;
            }
        }

        return new ArrayDataProvider([
            'allModels' => $query->all(),
            'sort' => [
                'attributes' => [
                    'outInvoiceCount',
                    'created_at',
                    'outInvoiceSendEmailCount',
                    'activeSubscribeDaysCount',
                    'customersCount',
                    'sellersCount',
                    'autoInvoicesCount',
                    'outActCount',
                    'paymentOrderCount',
                    'import_xls_product_count',
                    'import_xls_service_count',
                    'goodsCount',
                    'servicesCount',
                    'statementFromBank',
                ],
            ],
        ]);
    }

    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     */
    public function searchModelsForExcel($params)
    {
        /* @var ActiveQuery $query */
        $query = $this->getCompanies()
            ->orderBy(['company.created_at' => SORT_DESC]);

        $this->load($params);

        if ($this->bankName == -1) {
            $query->andWhere(['checking_accountant.bank_name' => null]);
        } else {
            $query->andFilterWhere(['checking_accountant.bank_name' => $this->bankName]);
        }

        $query->andFilterWhere(['activation_type' => $this->activation_type]);

        $query->andFilterWhere(['registration_page_type_id' => $this->registrationPageType]);

        $query->andFilterWhere(['company_type_id' => $this->company_type_id]);

        if (mb_substr($this->activeTariff, 0, 2) == 'pr') {
            $companies = [];
            /* @var $subscribe Subscribe */
            $subscribe = Subscribe::findOne(str_replace('pr', '', $this->activeTariff));
            $duration = SubscribeHelper::getReadableDuration($subscribe);
            $query->andWhere([
                'and',
                [Subscribe::tableName() . '.tariff_id' => null],
                ['not', [Subscribe::tableName() . '.id' => null]],
            ]);
            /* @var $company Company */
            $cloneQuery = clone $query;
            foreach ($cloneQuery->all() as $company) {
                if (SubscribeHelper::getReadableDuration($company->activeSubscribe) == $duration) {
                    $companies[] = $company->id;
                }
            }
            $query->andWhere(['in', Company::tableName() . '.id', $companies]);

        } elseif ($this->activeTariff == 'free') {
            $query->andWhere(['not', [Company::tableName() . '.free_tariff_start_at' => null]]);
        } elseif ($this->activeTariff == 'no') {
            $query->andWhere([
                Company::tableName() . '.free_tariff_start_at' => null,
                Company::tableName() . '.active_subscribe_id' => null,
            ]);
        } else {
            $query->andFilterWhere([Subscribe::tableName() . '.tariff_id' => $this->activeTariff]);
        }

        if ($this->phone) {
            $query->andWhere(['and',
                    ['not', [Company::tableName() . '.phone' => null]],
                    ['!=', 'phone', ''],
                ]
            );
        } elseif ($this->phone === '0') {
            $query->andWhere(['phone' => null]);
        } else {
            $query->andFilterWhere(['phone' => $this->phone]);
        }

        if ($this->fio) {
            $query->andWhere(['or',
                ['not', ['chief_firstname' => null]],
                ['not', ['chief_lastname' => null]],
                ['not', ['chief_patronymic' => null]],
            ]);
        } elseif ($this->fio === '0') {
            $query->andWhere(['and',
                ['chief_firstname' => null],
                ['chief_lastname' => null],
                ['chief_patronymic' => null],
            ]);
        }

        if ($this->companyTaxationType) {
            switch ($this->companyTaxationType) {
                case 'osno':
                    $query->andWhere(['company_taxation_type.osno' => 1]);
                    break;
                case 'usn6':
                    $query->andWhere(['company_taxation_type.usn' => 1])->andWhere(['company_taxation_type.usn_percent' => 6]);
                    break;
                case 'usn15':
                    $query->andWhere(['company_taxation_type.usn' => 1])->andWhere(['company_taxation_type.usn_percent' => 15]);
                    break;
            }
        }

        return $query->all();
    }

    /**
     * @return array
     */
    public function getBankNameArray()
    {
        $statusArray[null] = 'Все';

        $statusArray += ArrayHelper::map($this->getCompanies()
            ->groupBy('checking_accountant.bank_name')
            ->orderBy('checking_accountant.bank_name')->all(), 'mainCheckingAccountant.bank_name', 'mainCheckingAccountant.bank_name');
        $statusArray[-1] = '(Не задано)';

        return $statusArray;
    }

    /**
     * @return array
     */
    public function getCompanyTypesArray()
    {
        $typesArray[null] = 'Все';

        $typesArray += ArrayHelper::map($this->getCompanies()
            ->groupBy('company_type.name_short')
            ->orderBy('company_type.name_short')->all(), 'companyType.id', 'companyType.name_short');

        return $typesArray;
    }

    /**
     * @return array
     */
    public function getCompanyTaxationTypesArray()
    {
        $typesArray[null] = 'Все';
        $typesArray['usn6'] = 'УСН 6%';
        $typesArray['usn15'] = 'УСН 15%';
        $typesArray['osno'] = 'ОСНО';

        return $typesArray;
    }

    /**
     * @return array
     */
    public function getRegistrationPageTypeFilter()
    {
        $registrationPageTypeArray[null] = 'Все';
        $registrationPageTypeArray += $this->getCompanies()
            ->select([
                'registration_page_type.name',
            ])
            ->andWhere(['or',
                ['not', ['company.registration_page_type_id' => null]],
                ['not', ['company.registration_page_type_id' => '']],
            ])
            ->groupBy('company.registration_page_type_id')
            ->orderBy(['registration_page_type.name' => SORT_ASC])
            ->indexBy('registration_page_type.id')
            ->column();

        return $registrationPageTypeArray;
    }

    /**
     * @return mixed
     */
    public function getTariffNameFilter()
    {
        $statusArray = [];
        /* @var $company Company */
        foreach ($this->getCompanies()->all() as $company) {
            if ($company->hasActualSubscription) {
                if ($company->activeSubscribe->tariff) {
                    $statusArray[$company->activeSubscribe->tariff->id] = $company->activeSubscribe->getTariffName();
                } else {
                    $statusArray['pr' . $company->activeSubscribe->id] = 'Промокод (' . SubscribeHelper::getReadableDuration($company->activeSubscribe) . ')';
                }
            } elseif ($company->isFreeTariff) {
                $statusArray['free'] = 'Тариф "Бесплатно"';
            } else {
                $statusArray['no'] = 'Нет';
            }
        }
        $statusArray = array_unique($statusArray);
        natsort($statusArray);

        return [null => 'Все'] + $statusArray;
    }

    /**
     * @return array
     */
    public function getActivationStatusFilter()
    {
        $activationStatusArray = [];
        foreach ($this->getCompanies()->select('activation_type')->groupBy('activation_type')->column() as $activationTypeID) {
            if (isset(Company::$activationType[$activationTypeID])) {
                $activationStatusArray[$activationTypeID] = Company::$activationType[$activationTypeID];
            }
        }

        return [null => 'Все'] + $activationStatusArray;
    }

    /**
     * @return array
     */
    public function getPhoneFilter()
    {
        return [
            null => 'Все',
            1 => 'Есть телефон',
            0 => 'Нет телефона',
        ];
    }

    /**
     * @return array
     */
    public function getChiefFioFilter()
    {
        return [
            null => 'Все',
            1 => 'Есть ФИО',
            0 => 'Нет ФИО',
        ];
    }

    /**
     * @return CompanyQuery
     */
    public function getCompanies()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return Company::find()
            ->isBlocked(self::UNBLOCKED)
            ->isTest(!self::TEST_COMPANY)
            ->andWhere(['between', 'date(from_unixtime(`' . self::tableName() . '`.`created_at`))', $dateRange['from'], $dateRange['to']])
            ->joinWith('mainAccountant')
            ->joinWith('activeSubscribe')
            ->joinWith('registrationPageType')
            ->joinWith('companyType')
            ->joinWith('companyTaxationType')
            ->leftJoin(SubscribeTariff::tableName(), [SubscribeTariff::tableName() . '.id' => Subscribe::tableName() . '.tariff_id']);
    }

    /**
     * @return array
     */
    public function getDownloadExcelUrl()
    {
        $getParams = \Yii::$app->request->get();
        $data[] = 'company-additional-excel';
        if (!empty($getParams) && is_array(current($getParams))) {
            foreach (current($getParams) as $attribute => $getParam) {
                $data['CompanyAdditionalSearch'][$attribute] = $getParam;
            }
        }

        return Url::to($data);
    }
}