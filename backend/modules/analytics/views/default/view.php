<?php
use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\Company;
use common\models\employee\Employee;
use yii\helpers\Url;
use backend\modules\company\models\AffiliateSearch;

/* @var $this yii\web\View */
/* @var Company $model */
/* @var Employee $employeeModel */
/* @var $subscribeSearchModel \backend\modules\company\models\SubscribeSearch */
/* @var $subscribeDataProvider yii\data\ActiveDataProvider */
/* @var $promocodeSearchModel \backend\modules\company\models\PromoCodeSearch */
/* @var $promocodeDataProvider yii\data\ActiveDataProvider */
/* @var $employeeSearchModel common\models\employee\EmployeeSearch */
/* @var $employeeDataProvider yii\data\ActiveDataProvider */
/* @var $affiliateSearchModel AffiliateSearch */
/* @var $affiliateDataProvider yii\data\ActiveDataProvider */
/* @var $backUrl string */

$this->title = 'Профиль компании';
$this->context->layoutWrapperCssClass = 'edit-profile';
?>
<div class="form-horizontal">
    <div class="portlet">
        <div class="portlet-title">
            <?= Html::a('Назад к списку', [$backUrl], [
                'class' => 'back-to-customers',
            ]); ?>

            <p>
                ID <?= $model->id ?>,
                зарегистрирована <?= date(DateHelper::FORMAT_USER_DATE, $model->created_at) ?>
            </p>
            <div class="actions" style="float: left;padding-right: 15px;">
                <?= Html::a(' <i class="icon-pencil"></i>', [
                    '/company/company/update',
                    'id' => $model->id,
                    'return_url' => Url::current(),
                ], [
                    'title' => 'Редактировать',
                    'class' => 'darkblue btn-sm',
                ]); ?>
            </div>
            <div class="caption bold-text">
                <?= $model->getTitle(true); ?>
                <?php if ($model->blocked == Company::BLOCKED): ?>
                    <span
                        class="label label-default">Компания заблокирована</span>
                <?php endif; ?>
            </div>
            <div class="company-type-test" style="float: left;">
                <?= Html::checkbox('Company[test]', $model->test, [
                    'class' => 'form-control',
                    'data' => [
                        'url' => Url::to(['/company/company/change-status', 'id' => $model->id]),
                    ]
                ]); ?>
                <label>Тестовая компания</label>
            </div>
            <div class="col-md-5 navbar-company" style="float: right;">
                <ul id="w3" class="navbar-nav navbar-right nav" style="margin-bottom: -15px;">
                    <li class="active">
                        <?= Html::a('Профиль', Url::to(['view', 'id' => $model->id])); ?>
                    </li>
                    <li>
                        <?= Html::a('Карточка компании', Url::to(['/company/company/view-form-card', 'id' => $model->id])); ?>
                    </li>
                </ul>
            </div>
        </div>

        <?= $this->render('@backend/modules/company/views/company/_viewPartials/_partial_basic_info', [
            'model' => $model,
        ]); ?>

        <?= $this->render('@backend/modules/company/views/company/_viewPartials/_partial_create_invoice', [
            'model' => $model,
        ]); ?>

        <?= $this->render('@backend/modules/company/views/company/_viewPartials/_partial_payment_history', [
            'model' => $model,
            'paymentSearchModel' => $paymentSearchModel,
            'paymentDataProvider' => $paymentDataProvider,
        ]); ?>

        <?= $this->render('@backend/modules/company/views/company/_viewPartials/_partial_promocodes', [
            'model' => $model,
            'promocodeSearchModel' => $promocodeSearchModel,
            'promocodeDataProvider' => $promocodeDataProvider,
        ]); ?>

        <?= $this->render('@backend/modules/company/views/company/_viewPartials/_partial_affiliate', [
            'model' => $model,
            'affiliateSearchModel' => $affiliateSearchModel,
            'affiliateDataProvider' => $affiliateDataProvider,
        ]); ?>

        <?= $this->render('@backend/modules/company/views/company/_viewPartials/_partial_statistic', [
            'model' => $model,
        ]); ?>

        <?= $this->render('@backend/modules/company/views/company/_viewPartials/_partial_profile', [
            'model' => $model,
        ]); ?>

        <?= $this->render('@backend/modules/company/views/company/_viewPartials/_partial_checking_accountant', [
            'model' => $model,
        ]); ?>

        <?= $this->render('@backend/modules/company/views/company/_viewPartials/_partial_employees', [
            'company' => $model,
            'searchModel' => $employeeSearchModel,
            'dataProvider' => $employeeDataProvider,
            'return_url' => Url::current(),
        ]); ?>
    </div>
</div>
