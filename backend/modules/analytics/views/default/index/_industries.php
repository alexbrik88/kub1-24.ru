<?php

use backend\components\helpers\DashBoardHelper;
use frontend\models\Documents;
use common\models\company\CompanyInfoIndustry;
use common\models\employee\EmployeeInfoRole;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$industryArray = CompanyInfoIndustry::find()->orderBy("[[id]] = :other")->addParams([
    ':other' => CompanyInfoIndustry::OTHER,
])->all();
$roleArray = EmployeeInfoRole::find()->all();
$total = [];
$roleTotal = [];
foreach ($industryArray as $industry) {
    $total[$industry->id] = 0;
}
foreach ($roleArray as $role) {
    $roleTotal[$role->id] = 0;
}
?>
<div class="portlet box darkblue" style="width: 100%;">
    <div class="portlet-title">
        <div class="caption">Тип бизнеса</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid">
                <thead>
                <tr class="heading">
                    <th>Тип пользователя</th>
                    <?php foreach ($industryArray as $industry) : ?>
                        <th><?= $industry->name ?></th>
                    <?php endforeach ?>
                    <th>Итого</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($roleArray as $role) : ?>
                        <tr>
                            <th><?= $role->name ?></th>
                            <?php foreach ($industryArray as $industry) : ?>
                                <?php
                                $count = $inData->getEmployeeCountByIndustryRole($industry->id, $role->id);
                                $total[$industry->id] += $count;
                                $roleTotal[$role->id] += $count;
                                ?>
                                <td><?= $count ?></td>
                            <?php endforeach ?>
                            <td><strong><?= $roleTotal[$role->id] ?></strong></td>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <th>Итого</th>
                        <?php foreach ($industryArray as $industry) : ?>
                            <td><strong><?= $total[$industry->id] ?></strong></td>
                        <?php endforeach ?>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>