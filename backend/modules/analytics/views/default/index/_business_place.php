<?php

use backend\components\helpers\DashBoardHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$data = $inData->getBusinessPlaceCount();

?>
<div class="portlet box darkblue" style="max-width: 400px;">
    <div class="portlet-title">
        <div class="caption">Местонахождение бизнеса</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid">
                <thead>
                <tr class="heading">
                    <th>Страна</th>
                    <th>Кол-во</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $row) : ?>
                        <tr>
                            <th><?= $row['name'] ?></th>
                            <td><?= $row['count'] ?></td>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <th>Итого</th>
                        <td><?= array_sum(ArrayHelper::getColumn($data, 'count')) ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>