<?php

use common\models\company\CompanyType;

/* @var $this yii\web\View */
/* @var $inData backend\components\helpers\DashBoardHelper */

$taxTypeParams = [
    'ОСНО' => [true, false, false, false, null],
    'ОСНО + ЕНВД' => [true, false, true, false, null],
    'ОСНО + Патент' => [true, false, false, true, null],
    'ОСНО + ЕНВД + Патент' => [true, false, true, true, null],
    'УСН Доход' => [false, true, false, false, false],
    'УСН Доход + ЕНВД' => [false, true, true, false, false],
    'УСН Доход + Патент' => [false, true, false, true, false],
    'УСН Доход + ЕНВД + Патент' => [false, true, true, true, false],
    'УСН Доход-Расход' => [false, true, false, false, true],
    'УСН Доход-Расход + ЕНВД' => [false, true, true, false, true],
    'УСН Доход-Расход + Патент' => [false, true, false, true, true],
    'УСН Доход-Расход + ЕНВД + Патент' => [false, true, true, true, true],
];

$companyTypeArray = CompanyType::find()
    ->select([
        'name_short',
    ])
    ->andWhere([
        'in_company' => true,
    ])
    ->indexBy('id')
    ->column();

$totalArray = ['total' => 0];
foreach ($companyTypeArray as $id => $name) {
    $totalArray[$id] = 0;
}
?>
<div class="portlet box darkblue" style="max-width: 900px;">
    <div class="portlet-title">
        <div class="caption">Распределение систем налогообложения</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                    <tr class="heading">
                        <th>СНО</th>
                        <?php foreach ($companyTypeArray as $id => $name) : ?>
                            <th style="text-align: center;"><?= $name ?></th>
                        <?php endforeach ?>
                        <th style="text-align: center;">Итого</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($taxTypeParams as $taxType => $taxParams) : ?>
                        <?php $rowTotal = 0; ?>
                        <tr>
                            <td><?= $taxType ?></td>
                            <?php foreach ($companyTypeArray as $id => $name) : ?>
                                <?php
                                $count = $inData->getCompaniesByTypeAndTax($id, $taxParams);
                                $totalArray[$id] += $count;
                                $rowTotal += $count;
                                ?>
                                <td style="text-align: center;"><?= $count ?></td>
                            <?php endforeach ?>
                            <td style="text-align: center; font-weight: bold;"><?= $rowTotal ?></td>
                        </tr>
                        <?php $totalArray['total'] += $rowTotal; ?>
                    <?php endforeach ?>
                    <tr style="font-weight: bold;">
                        <td>Итого</td>
                        <?php foreach ($companyTypeArray as $id => $name) : ?>
                            <td style="text-align: center;"><?= $totalArray[$id] ?></td>
                        <?php endforeach ?>
                        <td style="text-align: center;"><?= $totalArray['total'] ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
