<?php

use backend\components\helpers\DashBoardHelper;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

TooltipAsset::register($this);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'side' => 'bottom',
        'interactive' => true,
    ],
]);
?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">Входные данные</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                <tr class="heading">
                    <th rowspan="2">Кол-во оплаченных на начало</th>
                    <th rowspan="1" colspan="2" style="border-bottom: 1px solid #ddd !important;">Кол-во новых
                        регистраций
                    </th>
                    <th rowspan="2">Кол-во всего в туториал</th>
                    <th rowspan="2">Кол-во ушедших из туториал</th>
                    <th rowspan="2">Кол-во первых оплат</th>
                    <th rowspan="2">Коэф прохождения туториала</th>
                    <th rowspan="2">Кол-во ушедших платников</th>
                    <th rowspan="2">Вернулись</th>
                    <th rowspan="2">Кол-во оплативших повторно</th>
                    <th rowspan="2">Коэф повторных продаж</th>
                    <th rowspan="2">Кол-во оплаченных на конец</th>
                    <th rowspan="2">Сумма оплат</th>
                    <th rowspan="2">Средний чек</th>
                </tr>
                <tr class="heading">
                    <th style="line-height: 12px;">Компаний<br/>&nbsp;</th>
                    <th style="line-height: 12px; border-right: 1px solid #ddd !important;">
                        Пользователей<br><span style="font-size: 9px;">все / Android</span>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= $inData->getPaidInitialCompanies(); ?></td>
                    <td><?= $inData->getNewCompanies() . ' (+' . $inData->getNewTestCompanies() . ')'; ?></td>
                    <td>
                        <?= $inData->getNewMainCompanies() . ' (+' . $inData->getNewTestMainCompanies() . ')'; ?> /
                        <?= $inData->getNewMainCompaniesFromAndroid(); ?>
                    </td>
                    <td><?= $inData->getTutorialCompanies(); ?></td>
                    <td><?= $inData->getNotTutorialCompanies(); ?></td>
                    <td>
                        <div class="tooltip2"
                             data-tooltip-content="#firstPayItems"><?= $inData->getFirstPayCompanies(); ?></div>
                    </td>
                    <td><?= $inData->getTutorialCoefficient(); ?></td>
                    <td>
                        <div class="tooltip2"
                             data-tooltip-content="#leavePaidItems"><?= $inData->getLeavePaidCompanies(); ?></div>
                    </td>
                    <td>
                        <div class="tooltip2"
                             data-tooltip-content="#returningItems"><?= $inData->getReturningCompanies(); ?></div>
                    </td>
                    <td>
                        <div class="tooltip2"
                             data-tooltip-content="#repeatPaidItems"><?= $inData->getRepeatPaidCompanies(); ?></div>
                    </td>
                    <td><?= $inData->getRepeatedSaleCoefficient(); ?></td>
                    <td><?= $inData->getPaidEndPeriodCompanies(); ?></td>
                    <td><?= $inData->getSum(); ?></td>
                    <td><?= $inData->getAverageCheck(); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="tooltip_templates" style="display: none;">
    <span id="firstPayItems" style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
        <?php foreach ($inData->getFirstPayCompaniesArray() as $company) : ?>
            <div>
                <?= Html::a($company['name'], ['/company/company/view', 'id' => $company['id']]) ?>
            </div>
        <?php endforeach ?>
    </span>
    <span id="leavePaidItems" style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
        <?php foreach ($inData->getLeavePaidCompaniesArray() as $company) : ?>
            <div>
                <?= Html::a($company['name'], ['/company/company/view', 'id' => $company['id']]) ?>
            </div>
        <?php endforeach ?>
    </span>
    <span id="returningItems" style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
        <?php foreach ($inData->getReturningCompaniesArray() as $company) : ?>
            <div>
                <?= Html::a($company['name'], ['/company/company/view', 'id' => $company['id']]) ?>
            </div>
        <?php endforeach ?>
    </span>
    <span id="repeatPaidItems" style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
        <?php foreach ($inData->getRepeatPaidCompaniesArray() as $company) : ?>
            <div>
                <?= Html::a($company['name'], ['/company/company/view', 'id' => $company['id']]) ?>
            </div>
        <?php endforeach ?>
    </span>
</div>
