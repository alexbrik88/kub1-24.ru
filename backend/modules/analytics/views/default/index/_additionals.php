<?php

use backend\components\helpers\DashBoardHelper;
use frontend\models\Documents;
use common\models\company\CompanyInfoIndustry;
use common\models\employee\EmployeeInfoRole;
use common\models\service\SubscribeTariffGroup;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$industryArray = CompanyInfoIndustry::find()->all();
$tarifGroupArray = SubscribeTariffGroup::find()->andWhere([
    'id' => [
        SubscribeTariffGroup::STANDART,
        SubscribeTariffGroup::PRICE_LIST,
        //SubscribeTariffGroup::CHECK_CONTRACTOR,
        SubscribeTariffGroup::B2B_PAYMENT,
    ],
])->all();
$totalCol = [];
$totalRow = [];
?>
<div class="portlet box darkblue" style="width: 100%;">
    <div class="portlet-title">
        <div class="caption">Какие дополнительные продукты выбрали</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid">
                <thead>
                <tr class="heading">
                    <th>Отрасль</th>
                    <?php foreach ($tarifGroupArray as $group) : ?>
                        <th><?= $group->name ?></th>
                    <?php endforeach ?>
                    <th>Итого</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($industryArray as $industry) : ?>
                        <tr>
                            <th><?= $industry->name ?></th>
                            <?php foreach ($tarifGroupArray as $group) : ?>
                                <?php
                                $count = $inData->getCompanyTariffSelected($industry->id, $group->id);
                                $totalRow[$industry->id] = ($totalRow[$industry->id] ?? 0) + $count;
                                $totalCol[$group->id] = ($totalCol[$group->id] ?? 0) + $count;
                                ?>
                                <td><?= $count ?></td>
                            <?php endforeach ?>
                            <td><?= $totalRow[$industry->id] ?></td>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <th>Итого</th>
                        <?php foreach ($tarifGroupArray as $group) : ?>
                            <td><strong><?= $totalCol[$group->id] ?></strong></td>
                        <?php endforeach ?>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>