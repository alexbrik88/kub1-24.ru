<?php

use backend\components\helpers\DashBoardHelper;
use frontend\models\Documents;
use common\models\company\CompanyInfoIndustry;
use common\models\employee\EmployeeInfoRole;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$data = $inData->getStoresStatisticsByIndustries();

?>
<div class="portlet box darkblue" style="width: 100%;">
    <div class="portlet-title">
        <div class="caption">Кол-во точек продаж и складов</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid">
                <thead>
                <tr class="heading">
                    <th>Тип пользователя</th>
                    <th>Кол-во</th>
                    <th>Кол-во магазинов</th>
                    <th>Кол-во сайтов</th>
                    <th>Кол-во складов</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $row) : ?>
                        <tr>
                            <th><?= $row['name'] ?></th>
                            <td><?= $row['count'] ?></td>
                            <td><?= $row['shops_count'] ?></td>
                            <td><?= $row['sites_count'] ?></td>
                            <td><?= $row['storage_count'] ?></td>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <th>Итого</th>
                        <td><?= array_sum(ArrayHelper::getColumn($data, 'count')) ?></td>
                        <td><?= array_sum(ArrayHelper::getColumn($data, 'shops_count')) ?></td>
                        <td><?= array_sum(ArrayHelper::getColumn($data, 'sites_count')) ?></td>
                        <td><?= array_sum(ArrayHelper::getColumn($data, 'storage_count')) ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>