<?php

use backend\components\helpers\DashBoardHelper;
use frontend\models\Documents;
use common\models\document\Invoice;
use common\models\document\PackingList;
use common\models\document\Act;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\ofd\components\Ofd;
use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$colCount = count(Banking::$modelClassArray) + count(Ofd::$modelClassArray) + 1;
?>
<div class="portlet box darkblue" style="width: 100%;">
    <div class="portlet-title">
        <div class="caption">Банки</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                <thead>
                <tr class="heading">
                    <th></th>
                    <th>1С</th>
                    <?php foreach (Banking::$modelClassArray as $class) : ?>
                        <th style="line-height:12px"><?= $class::NAME_SHORT ?><br><span style="font-size: 9px;">Руч. / Авто</span></th>
                    <?php endforeach ?>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Кол-во загрузок</th>
                    <td><?= $inData->getImport1C(); ?></td>
                    <?php foreach (Banking::$modelClassArray as $key => $class) : ?>
                        <?php $result = $inData->getStatementFromBank($class); ?>
                        <td>
                            <div class="tooltip2" data-tooltip-content="#statementForBank<?= $key; ?>">
                                <?= $result['manual'] ?> (<?= $result['auto'] ?>)
                            </div>
                            <?php if (count($result['companies'])): ?>
                                <div class="tooltip_templates" style="display: none;">
                                    <span id="statementForBank<?= $key; ?>"
                                          style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
                                        <?php foreach ($result['companies'] as $companyData): ?>
                                            <?php foreach ($companyData as $companyID => $companyName): ?>
                                                <div>
                                                    <?= Html::a($companyName, ['/company/company/view', 'id' => $companyID]) ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </span>
                                </div>
                            <?php endif; ?>
                        </td>
                    <?php endforeach ?>
                </tr>
                <tr>
                    <th>Кол-во интеграций</th>
                    <td></td>
                    <?php foreach (Banking::$modelClassArray as $class) : ?>
                        <td><?= $inData->getBankIntegrationsCount($class::ALIAS); ?></td>
                    <?php endforeach ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>