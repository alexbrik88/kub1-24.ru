<?php

use backend\components\helpers\DashBoardHelper;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$this->title = 'Дашборд';
?>
<div class="portlet box">
    <div class="filter">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<?= $this->render('index/_in_data', [
    'inData' => $inData,
]); ?>

<?= $this->render('index/_bank', [
    'inData' => $inData,
]); ?>

<?= $this->render('index/_downloading', [
    'inData' => $inData,
]); ?>

<?= $this->render('index/_industries', [
    'inData' => $inData,
]); ?>

<?= $this->render('index/_stores', [
    'inData' => $inData,
]); ?>

<?= $this->render('index/_additionals', [
    'inData' => $inData,
]); ?>

<?= $this->render('index/_business_place', [
    'inData' => $inData,
]); ?>

<?= $this->render('index/_tax_spreading', [
    'inData' => $inData,
]); ?>
