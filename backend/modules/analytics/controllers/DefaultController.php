<?php

namespace backend\modules\analytics\controllers;

use backend\components\BackendController;
use backend\components\helpers\DashBoardHelper;
use backend\modules\analytics\models\AnalyticsExtendedSearch;
use backend\modules\company\models\AffiliateSearch;
use backend\modules\company\models\PaymentSearch;
use backend\modules\company\models\PromoCodeSearch;
use common\models\Company;
use common\models\company\RegistrationPageType;
use frontend\components\StatisticPeriod;
use frontend\models\EmployeeCompanySearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `analytics` module
 */
class DefaultController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();
        $inData = new DashBoardHelper($dateRange);
        $inData->registration_page_type_id = RegistrationPageType::PAGE_TYPE_ANALYTICS;

        return $this->render('index', [
            'inData' => $inData,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionExtended()
    {
        $searchModel = new AnalyticsExtendedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('extended', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @param string $backUrl
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id, $backUrl = 'extended')
    {
        /** @var Company $model */
        $model = $this->findModel($id);

        $paymentSearchModel = new PaymentSearch([
            'company_id' => $model->id,
        ]);
        $paymentDataProvider = $paymentSearchModel->search([]);

        $promocodeSearchModel = new PromoCodeSearch([
            'company_id' => $model->id,
        ]);
        $promocodeDataProvider = $promocodeSearchModel->search([]);

        $employeeSearchModel = new EmployeeCompanySearch([
            'company_id' => $model->id,
        ]);
        $employeeDataProvider = $employeeSearchModel->search([], $model->id);

        $affiliateSearchModel = new AffiliateSearch();
        $affiliateDataProvider = $affiliateSearchModel->search($model, Yii::$app->request->get());

        return $this->render('view', [
            'model' => $model,
            'paymentSearchModel' => $paymentSearchModel,
            'paymentDataProvider' => $paymentDataProvider,
            'promocodeSearchModel' => $promocodeSearchModel,
            'promocodeDataProvider' => $promocodeDataProvider,
            'employeeSearchModel' => $employeeSearchModel,
            'employeeDataProvider' => $employeeDataProvider,
            'affiliateSearchModel' => $affiliateSearchModel,
            'affiliateDataProvider' => $affiliateDataProvider,
            'backUrl' => $backUrl,
        ]);
    }

    /**
     * @param $id
     *
     * @return Company
     * @throws NotFoundHttpException
     */
    protected function findModel($id = null)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }

        $model = Company::findOne($id);

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}
