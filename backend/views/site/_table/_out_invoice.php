<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.12.2016
 * Time: 5:57
 */

use backend\components\helpers\DashBoardHelper;
use common\models\document\Invoice;
use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */
?>
<div class="portlet box darkblue" style="width: 35%; display: inline-block;">
    <div class="portlet-title">
        <div class="caption">Модуль выставления счетов</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid">
                <thead>
                <tr class="heading">
                    <th>Ссылок на счет</th>
                    <th>Счетов</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="tooltip2" data-tooltip-content="#outInvoiceCount">
                            <?= $inData->getOutInvoiceCount(); ?>
                        </div>
                    </td>
                    <td><?= $inData->getDocumentsCount(Invoice::className(), 'byOutLink'); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="tooltip_templates" style="display: none;">
    <span id="outInvoiceCount" style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
        <?php foreach ($inData->getOutInvoiceCompanyArray()->groupBy("company_id")->all() as $company) : ?>
            <div>
                <?= Html::a($company['name'], ['/company/company/view', 'id' => $company['id']]) ?>
            </div>
        <?php endforeach ?>
    </span>
</div>