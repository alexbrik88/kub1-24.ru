<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.12.2016
 * Time: 5:57
 */

use backend\components\helpers\DashBoardHelper;
use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */
?>
<div class="portlet box darkblue" style="width: 35%;display: inline-block;">
    <div class="portlet-title">
        <div class="caption">Кабинет покупателя</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid">
                <thead>
                <tr class="heading">
                    <th>Внешних кабинетов</th>
                    <th>Заказов</th>
                    <th>Счетов</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="tooltip2" data-tooltip-content="#storeCabinetCount">
                            <?= $inData->getStoreCabinetCount(); ?>
                        </div>
                    </td>
                    <td><?= $inData->getOrderDocumentCountFromStoreCabinet(); ?></td>
                    <td><?= $inData->getInvoiceCountFromStoreCabinet(); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="tooltip_templates" style="display: none;">
    <span id="storeCabinetCount" style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
        <?php foreach ($inData->getStoreCabinetCompanyArray()->groupBy("company_id")->all() as $company) : ?>
            <div>
                <?= Html::a($company['name'], ['/company/company/view', 'id' => $company['id']]) ?>
            </div>
        <?php endforeach ?>
    </span>
</div>