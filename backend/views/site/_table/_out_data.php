<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.12.2016
 * Time: 5:57
 */

use backend\components\helpers\DashBoardHelper;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */
?>
<div class="portlet box darkblue" style="width: 35%;display: inline-block;">
    <div class="portlet-title">
        <div class="caption">Выгрузки</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                <tr class="heading">
                    <th>В 1С</th>
                    <th>Сканы</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= $inData->getExport1C(); ?></td>
                    <td><?= $inData->getExportFiles(); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
