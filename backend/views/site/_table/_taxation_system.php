<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.12.2016
 * Time: 5:28
 */

use backend\components\helpers\DashBoardHelper;
use common\models\TaxationType;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */
?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">Система налогообложения</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <tbody>
                <tr>
                    <td>ОСНО</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(true, false, false, false, null); ?></td>
                </tr>
                <tr>
                    <td>ОСНО + ЕНВД</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(true, false, true, false, null); ?></td>
                </tr>
                <tr>
                    <td>ОСНО + Патент</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(true, false, false, true, null); ?></td>
                </tr>
                <tr>
                    <td>ОСНО + ЕНВД + Патент</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(true, false, true, true, null); ?></td>
                </tr>
                <tr>
                    <td>УСН Доход</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(false, true, false, false, false); ?></td>
                </tr>
                <tr>
                    <td>УСН Доход + ЕНВД</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(false, true, true, false, false); ?></td>
                </tr>
                <tr>
                    <td>УСН Доход + Патент</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(false, true, false, true, false); ?></td>
                </tr>
                <tr>
                    <td>УСН Доход + ЕНВД + Патент</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(false, true, true, true, false); ?></td>
                </tr>
                <tr>
                    <td>УСН Доход-Расход</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(false, true, false, false, true); ?></td>
                </tr>
                <tr>
                    <td>УСН Доход-Расход + ЕНВД</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(false, true, true, false, true); ?></td>
                </tr>
                <tr>
                    <td>УСН Доход-Расход + Патент</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(false, true, false, true, true); ?></td>
                </tr>
                <tr>
                    <td>УСН Доход-Расход + ЕНВД + Патент</td>
                    <td><?= $inData->getCompaniesByTaxationSystem(false, true, true, true, true); ?></td>
                </tr>
                <tr>
                    <td>ИТОГО</td>
                    <td><?= $inData->getCompanies()->count(); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

