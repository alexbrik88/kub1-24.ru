<?php

use backend\components\helpers\DashBoardHelper;
use frontend\models\Documents;
use common\models\document\Invoice;
use common\models\document\PackingList;
use common\models\document\Act;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\ofd\components\Ofd;
use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$colCount = count(Banking::$modelClassArray) + count(Ofd::$modelClassArray) + 1;
?>
<div class="portlet box darkblue" style="width: 100%;">
    <div class="portlet-title">
        <div class="caption">Загрузки</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid">
                <thead>
                <tr class="heading">
                    <th colspan="5" style="border-bottom: 1px solid #dcdcdc;">Исходящие документы</th>
                    <th colspan="4" style="border-bottom: 1px solid #dcdcdc;">Входящие документы</th>
                    <th colspan="<?= $colCount ?>" style="border-bottom: 1px solid #dcdcdc;">
                        Выписки
                    </th>
                    <th colspan="<?= count(\common\models\ofd\Ofd::active()) ?>" style="border-bottom: 1px solid #dcdcdc;">
                        ОФД
                    </th>
                    <th colspan="1" rowspan="2" style="border-bottom: 1px solid #dcdcdc;">Отчетность</th>
                    <th colspan="1" rowspan="2" style="border-bottom: 1px solid #dcdcdc;">Мои документы</th>
                    <th colspan="2" style="border-bottom: 1px solid #dcdcdc;">Загрузки из Excel</th>
                </tr>
                <tr class="heading">
                    <th>Счета</th>
                    <th>Акты</th>
                    <th>ТН</th>
                    <th>СФ</th>
                    <th>УПД</th>
                    <th>Счета</th>
                    <th>Акты</th>
                    <th>ТН</th>
                    <th>СФ</th>
                    <th>1С</th>
                    <?php foreach (Banking::$modelClassArray as $class) : ?>
                        <th style="line-height:12px"><?= $class::NAME_SHORT ?><br><span style="font-size: 9px;">Руч. / Авто</span></th>
                    <?php endforeach ?>
                    <?php foreach (Ofd::$modelClassArray as $class) : ?>
                        <th style="line-height:12px"><?= $class::NAME_SHORT ?><br><span style="font-size: 9px;">Руч. / Авто</span></th>
                    <?php endforeach ?>
                    <?php foreach (\common\models\ofd\Ofd::active() as $ofd) : ?>
                        <th style="line-height:12px"><?= $ofd->name ?><br><span style="font-size: 9px;">Руч. / Авто</span></th>
                    <?php endforeach ?>
                    <th style="border-right-width: 1px;">Товары</th>
                    <th style="border-right-width: 1px;">Услуги</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= $inData->getFilesCount(Invoice::className(), Documents::IO_TYPE_OUT); ?></td>
                    <td><?= $inData->getFilesCount(Act::className(), Documents::IO_TYPE_OUT); ?></td>
                    <td><?= $inData->getFilesCount(PackingList::className(), Documents::IO_TYPE_OUT); ?></td>
                    <td><?= $inData->getFilesCount(InvoiceFacture::className(), Documents::IO_TYPE_OUT); ?></td>
                    <td><?= $inData->getFilesCount(Upd::className(), Documents::IO_TYPE_OUT); ?></td>
                    <td><?= $inData->getFilesCount(Invoice::className(), Documents::IO_TYPE_IN); ?></td>
                    <td><?= $inData->getFilesCount(Act::className(), Documents::IO_TYPE_IN); ?></td>
                    <td><?= $inData->getFilesCount(PackingList::className(), Documents::IO_TYPE_IN); ?></td>
                    <td><?= $inData->getFilesCount(InvoiceFacture::className(), Documents::IO_TYPE_IN); ?></td>
                    <td><?= $inData->getImport1C(); ?></td>
                    <?php foreach (Banking::$modelClassArray as $key => $class) : ?>
                        <?php $result = $inData->getStatementFromBank($class); ?>
                        <th>
                            <div class="tooltip2" data-tooltip-content="#statementForBank<?= $key; ?>">
                                <?= $result['manual'] ?> (<?= $result['auto'] ?>)
                            </div>
                            <?php if (count($result['companies'])): ?>
                                <div class="tooltip_templates" style="display: none;">
                                    <span id="statementForBank<?= $key; ?>"
                                          style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
                                        <?php foreach ($result['companies'] as $companyData): ?>
                                            <?php foreach ($companyData as $companyID => $companyName): ?>
                                                <div>
                                                    <?= Html::a($companyName, ['/company/company/view', 'id' => $companyID]) ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </span>
                                </div>
                            <?php endif; ?>
                        </th>
                    <?php endforeach ?>
                    <?php foreach (Ofd::$modelClassArray as $key => $class) : ?>
                        <?php $result = $inData->getStatementFromOfd($class); ?>
                        <th>
                            <div class="tooltip2" data-tooltip-content="#statementForOfd<?= $key; ?>">
                                <?= $result['manual'] ?> (<?= $result['auto'] ?>)
                            </div>
                            <?php if (count($result['companies'])): ?>
                                <div class="tooltip_templates" style="display: none;">
                                    <span id="statementForOfd<?= $key; ?>"
                                          style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
                                        <?php foreach ($result['companies'] as $companyID => $companyName): ?>
                                            <div>
                                                <?= Html::a($companyName, ['/company/company/view', 'id' => $companyID]) ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </span>
                                </div>
                            <?php endif; ?>
                        </th>
                    <?php endforeach ?>
                    <?php foreach (\common\models\ofd\Ofd::active() as $ofd) : ?>
                        <?php $result = $inData->getOfdUploads($ofd->id); ?>
                        <th>
                            <div class="tooltip2" data-tooltip-content="#statementForOfd<?= $key; ?>">
                                <?= $result['manual'] ?> (<?= $result['auto'] ?>)
                            </div>
                            <?php if (count($result['companies'])): ?>
                                <div class="tooltip_templates" style="display: none;">
                                    <span id="statementForOfd<?= $key; ?>"
                                          style="display: inline-block; max-height: 240px; line-height: 24px; overflow-y: auto;">
                                        <?php foreach ($result['companies'] as $companyID => $companyName): ?>
                                            <div>
                                                <?= Html::a($companyName, ['/company/company/view', 'id' => $companyID]) ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </span>
                                </div>
                            <?php endif; ?>
                        </th>
                    <?php endforeach ?>
                    <td><?= $inData->getReportFile(); ?></td>
                    <td><?= $inData->getSpecificDocument(); ?></td>
                    <td><?= $inData->getImportXlsProductCount(); ?></td>
                    <td><?= $inData->getImportXlsServiceCount(); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>