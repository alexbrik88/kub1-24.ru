<?php

use backend\components\helpers\DashBoardHelper;
use common\models\Agreement;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use common\models\document\Upd;
use frontend\models\Documents;
use \common\models\CreateType;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$API_PARTNER = 1; // Android app

?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">Активность</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                <tr class="heading">
                    <th rowspan="2" style="line-height: 12px;">Покупатели<br><span style="font-size: 9px;">все / Android</span></th>
                    <th colspan="9"
                        style="border-bottom: 1px solid #dcdcdc;">Исходящие
                        документы
                    </th>
                    <th rowspan="2" style="line-height: 12px;">Поставщики<br><span style="font-size: 9px;">все / Android</span></th>
                    <th colspan="4"
                        style="border-bottom: 1px solid #dcdcdc;">Входящие
                        документы
                    </th>
                    <th colspan="2"
                        style="border-bottom: 1px solid #dcdcdc;">Выслано по
                        e-mail
                    </th>
                </tr>
                <tr class="heading">
                    <th style="line-height: 12px;">Счета<br><span style="font-size: 9px;">все (b2b / Android)</span></th>
                    <th>Счет-договор</th>
                    <th>Договор</th>
                    <th>АвтоСЧ</th>
                    <th style="line-height: 12px;">Акты<br><span style="font-size: 9px;">все / Android</span></th>
                    <th style="line-height: 12px;">ТН<br><span style="font-size: 9px;">все / Android</span></th>
                    <th style="line-height: 12px;">СФ<br><span style="font-size: 9px;">все / Android</span></th>
                    <th style="line-height: 12px;">УПД</th>
                    <th>ПП</th>
                    <th style="line-height: 12px;">Счета<br><span style="font-size: 9px;">все / Android</span></th>
                    <th>Акты</th>
                    <th>ТН</th>
                    <th>СФ</th>
                    <th>Счета</th>
                    <th>Визитка компани</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= $inData->getContractorCount(Contractor::TYPE_CUSTOMER); ?> /
                        <?= $inData->getContractorCountFromAPI(Contractor::TYPE_CUSTOMER, $API_PARTNER); ?>
                    </td>
                    <td>
                        <?= $inData->getDocumentsCount(Invoice::className(), Documents::IO_TYPE_OUT); ?>
                        (<?= $inData->getDocumentsCount(Invoice::className(), 'byOutLink'); ?> /
                        <?= $inData->getDocumentsCountFromAPI(Invoice::className(), Documents::IO_TYPE_OUT, $API_PARTNER); ?>)
                    </td>
                    <td><?= $inData->getDocumentsCount(Invoice::className(), 'invoiceContract'); ?></td>
                    <td><?= $inData->getDocumentsCount(Agreement::className(), Documents::IO_TYPE_OUT); ?></td>
                    <td><?= $inData->getDocumentsCount(InvoiceAuto::className(), Documents::IO_TYPE_OUT); ?></td>
                    <td>
                        <?= $inData->getDocumentsCount(Act::className(), Documents::IO_TYPE_OUT); ?> /
                        <?= $inData->getDocumentsCountFromAPI(Act::className(), Documents::IO_TYPE_OUT, $API_PARTNER); ?>
                    </td>
                    <td>
                        <?= $inData->getDocumentsCount(PackingList::className(), Documents::IO_TYPE_OUT); ?> /
                        <?= $inData->getDocumentsCountFromAPI(PackingList::className(), Documents::IO_TYPE_OUT, $API_PARTNER); ?>
                    </td>
                    <td>
                        <?= $inData->getDocumentsCount(InvoiceFacture::className(), Documents::IO_TYPE_OUT); ?> /
                        <?= $inData->getDocumentsCountFromAPI(InvoiceFacture::className(), Documents::IO_TYPE_OUT, $API_PARTNER); ?>
                    </td>
                    <td>
                        <?= $inData->getDocumentsCount(Upd::className(), Documents::IO_TYPE_OUT); ?> /
                        <?= $inData->getDocumentsCountFromAPI(Upd::className(), Documents::IO_TYPE_OUT, $API_PARTNER); ?>
                    </td>
                    <td><?= $inData->getDocumentsCount(PaymentOrder::className(), Documents::IO_TYPE_OUT); ?></td>
                    <td>
                        <?= $inData->getContractorCount(Contractor::TYPE_SELLER); ?> /
                        <?= $inData->getContractorCountFromAPI(Contractor::TYPE_SELLER, $API_PARTNER); ?>
                    </td>
                    <td><?= $inData->getDocumentsCount(Invoice::className(), Documents::IO_TYPE_IN); ?></td>
                    <td><?= $inData->getDocumentsCount(Act::className(), Documents::IO_TYPE_IN); ?></td>
                    <td><?= $inData->getDocumentsCount(PackingList::className(), Documents::IO_TYPE_IN); ?></td>
                    <td><?= $inData->getDocumentsCount(InvoiceFacture::className(), Documents::IO_TYPE_IN); ?></td>
                    <td>
                        <?= $inData->getSentInvoicesCount(); ?> /
                        <?= $inData->getSentInvoicesCountFromAPI($API_PARTNER); ?>
                    </td>
                    <td><?= $inData->getSentVisitCardCount(); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
