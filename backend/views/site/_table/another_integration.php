<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.11.2018
 * Time: 1:30
 */

use backend\components\helpers\DashBoardHelper;
use frontend\modules\cash\modules\banking\components\Banking;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$data = $inData->getIntegrationsData();
?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">Кол-во других интеграций</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                <tr class="heading">
                    <?php foreach ($data as $item) : ?>
                        <th><?= $item['name'] ?></th>
                    <?php endforeach ?>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php foreach ($data as $item) : ?>
                        <td><?= $item['count'] ?></td>
                    <?php endforeach ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


