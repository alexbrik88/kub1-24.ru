<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.12.2016
 * Time: 5:57
 */

use backend\components\helpers\DashBoardHelper;
use common\models\document\Invoice;
use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */
?>
<div class="portlet box pull-right" style="width: 25%; display: inline-block;"></div>
<div class="portlet box darkblue pull-right" style="width: 35%; display: inline-block;">
    <div class="portlet-title">
        <div class="caption">Прайс-листы</div>
    </div>
    <div class="portlet-body">
        <div id="w111" style="overflow-x: auto;">
            <table
                    class="table table-striped table-bordered table-hover dataTable"
                    role="grid">
                <thead>
                <tr class="heading">
                    <th>Создано</th>
                    <th>Отправлено</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <?= $inData->getPriceListCount() ?: 0; ?>
                    </td>
                    <td>
                        <?= $inData->getPriceListSendsCount() ?: 0; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div style="clear:both"></div>
