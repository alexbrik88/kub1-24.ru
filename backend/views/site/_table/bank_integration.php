<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.11.2018
 * Time: 1:30
 */

use backend\components\helpers\DashBoardHelper;
use frontend\modules\cash\modules\banking\components\Banking;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */
?>
<div class="portlet box darkblue" style="width: 60%;">
    <div class="portlet-title">
        <div class="caption">Кол-во интеграций банков</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                <tr class="heading">
                    <?php foreach (Banking::$modelClassArray as $class) : ?>
                        <th><?= $class::NAME_SHORT ?></th>
                    <?php endforeach ?>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php foreach (Banking::$modelClassArray as $class) : ?>
                        <td><?= $inData->getBankIntegrationsCount($class::ALIAS); ?></td>
                    <?php endforeach ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


