<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.12.2016
 * Time: 5:57
 */

use backend\components\helpers\DashBoardHelper;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */
?>
<div class="portlet box darkblue pull-right" style="width: 60%;display: inline-block;">
    <div class="portlet-title">
        <div class="caption">Ссылка на счет</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                <tr class="heading">
                    <th>Кол-во открывших</th>
                    <th>На Оплатить</th>
                    <th>На Сохранить</th>
                    <th>Скачать</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= $inData->getInvoiceLinksView(); ?></td>
                    <td><?= $inData->getInvoiceLinksPay(); ?></td>
                    <td><?= $inData->getInvoiceLinksSave(); ?></td>
                    <td><?= $inData->getInvoiceLinkDownload(); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>