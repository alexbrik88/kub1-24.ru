<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.11.2018
 * Time: 14:26
 */

use backend\components\helpers\DashBoardHelper;
use common\models\employee\EmployeeClick;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */
?>
<div class="portlet box darkblue pull-right" style="width: 60%;display: inline-block;">
    <div class="portlet-title">
        <div class="caption">Кол-во кликов</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                <tr class="heading">
                    <th>Увелич. продаж (сотрудники)</th>
                    <th>Увелич. продаж (клики)</th>
                    <th>Пред. просмотр кабинета (сотрудники)</th>
                    <th>Пред. просмотр кабинета (клики)</th>
                    <th>Пред. просмотр виджет (сотрудники)</th>
                    <th>Пред. просмотр виджет (клики)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= $inData->getEmployeeClickCount(EmployeeClick::SALE_INCREASE_TYPE); ?></td>
                    <td><?= $inData->getEmployeeClickCount(EmployeeClick::SALE_INCREASE_TYPE, false); ?></td>
                    <td><?= $inData->getEmployeeClickCount(EmployeeClick::PRE_VIEW_STORE_CABINET); ?></td>
                    <td><?= $inData->getEmployeeClickCount(EmployeeClick::PRE_VIEW_STORE_CABINET, false); ?></td>
                    <td><?= $inData->getEmployeeClickCount(EmployeeClick::PRE_VIEW_OUT_INVOICE); ?></td>
                    <td><?= $inData->getEmployeeClickCount(EmployeeClick::PRE_VIEW_OUT_INVOICE, false); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div style="clear:both"></div>