<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.12.2016
 * Time: 5:55
 */
use backend\components\helpers\DashBoardHelper;
use common\models\company\CompanyType;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */
?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">Тип</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <tbody>
                <tr>
                    <td>ООО</td>
                    <td><?= $inData->getCompaniesByType(CompanyType::TYPE_OOO); ?></td>
                </tr>
                <tr>
                    <td>ИП</td>
                    <td><?= $inData->getCompaniesByType(CompanyType::TYPE_IP); ?></td>
                </tr>
                <tr>
                    <td>ОАО</td>
                    <td><?= $inData->getCompaniesByType(CompanyType::TYPE_OAO); ?></td>
                </tr>
                <tr>
                    <td>ИТОГО</td>
                    <td><?= $inData->getCompaniesByType(CompanyType::TYPE_OOO) + $inData->getCompaniesByType(CompanyType::TYPE_IP) + $inData->getCompaniesByType(CompanyType::TYPE_OAO); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>