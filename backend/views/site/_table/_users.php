<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.12.2016
 * Time: 6:32
 */

use backend\components\helpers\DashBoardHelper;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

?>
<div class="portlet box darkblue" style="width: 60%;">
    <div class="portlet-title">
        <div class="caption">Компании</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                <tr class="heading">
                    <th>Итого активность</th>
                    <th>Итого</th>
                    <th>Платники</th>
                    <th>Туториал</th>
                    <th>Бесплатно</th>
                    <th>Коэф-т: Активность на 1 активную компанию</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= $inData->getAllActivity(); ?></td>
                    <td>
                        <?= $allSum = $inData->getPayedCompanies() +
                            $inData->getTrialCompanies() +
                            $inData->getFreeTariffCompanies(); ?>
                    </td>
                    <td><?= $inData->getPayedCompanies(); ?></td>
                    <td><?= $inData->getTrialCompanies(); ?></td>
                    <td><?= $inData->getFreeTariffCompanies(); ?></td>
                    <td><?= round($inData->getAllActivity() / ($allSum ? : 1), 2); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>