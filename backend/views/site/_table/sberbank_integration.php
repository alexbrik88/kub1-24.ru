<?php
use backend\components\helpers\DashBoardHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use \common\models\cash\CashBankStatementUpload;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$companiesIds = $inData->getSberbankCompaniesIds();
$paidCompaniesIds = $inData->getSberbankCompaniesIds(true);
$autoloadModeCount = $inData->getSberbankAutoloadModeCount($companiesIds);
$paidAutoloadModeCount = $inData->getSberbankAutoloadModeCount($paidCompaniesIds);
$autoloadsCount = $inData->getSberbankUploadsCount($companiesIds, CashBankStatementUpload::SOURCE_BANK_AUTO);
$loadsCount = $inData->getSberbankUploadsCount($companiesIds, CashBankStatementUpload::SOURCE_BANK);
$requestsCount = $inData->getSberbankRequestsCount($companiesIds);

$companies = [
    'paid' => count($paidCompaniesIds),
    'free' => count($companiesIds) - count($paidCompaniesIds)
];
$autoloadMode = [
    'paid' => $paidAutoloadModeCount,
    'free' => $autoloadModeCount - $paidAutoloadModeCount
];

?>
<div class="portlet box darkblue" style="width: 60%;">
    <div class="portlet-title">
        <div class="caption">Сбербанк</div>
    </div>
    <div class="portlet-body">
        <div id="w1" style="overflow-x: auto;">
            <table
                class="table table-striped table-bordered table-hover dataTable"
                role="grid">
                <thead>
                <tr class="heading">
                    <th>Кол-во компаний с интеграцией.<br/>Всего (Пл/Бес)</th>
                    <th>Кол-во компаний включивших автозагрузку</th>
                    <th>Кол-во автозагрузок</th>
                    <th>Кол-во ручных загрузок</th>
                    <th>Итого получение выписок</th>
                    <th>Кол-во запросов (дней)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <?= $companies['paid'] + $companies['free'] ?>
                        (<?= $companies['paid'] ?>/<?= $companies['free'] ?>)
                    </td>
                    <td>
                        <?= $autoloadMode['paid'] + $autoloadMode['free'] ?>
                        (<?= $autoloadMode['paid'] ?>/<?= $autoloadMode['free'] ?>)
                    </td>
                    <td>
                        <?= $autoloadsCount ?>
                    </td>
                    <td>
                        <?= $loadsCount ?>
                    </td>
                    <td>
                        <?= $autoloadsCount + $loadsCount ?>
                    </td>
                    <td>
                        <?= $requestsCount ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>