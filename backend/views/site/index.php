<?php

use backend\components\helpers\DashBoardHelper;

/* @var $this yii\web\View */
/* @var $inData DashBoardHelper */

$this->title = 'Дашборд';
?>
<div class="portlet box">
    <div class="filter">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<?= $this->render('_table/_in_data', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/_activiti', [
    'inData' => $inData,
]); ?>

<div>
<?= $this->render('_table/_store_cabinet', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/_employee_clicks', [
    'inData' => $inData,
]); ?>
</div>
<div>
<?= $this->render('_table/_out_invoice', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/_price_list', [
    'inData' => $inData,
]); ?>
</div>
<?= $this->render('_table/_downloading', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/bank_integration', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/another_integration', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/sberbank_integration', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/_out_data', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/_invoice_link', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/_users', [
    'inData' => $inData,
]); ?>

<?= $this->render('_table/_tax_spreading', [
    'inData' => $inData,
]); ?>

