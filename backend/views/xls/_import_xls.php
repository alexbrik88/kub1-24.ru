<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.12.2016
 * Time: 8:09
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $header string
 * @var $text string
 * @var $formData string
 * @var $uploadXlsTemplateUrl string
 * @var $uploadAction string
 * @var $progressAction string
 */

?>
<div class="modal fade" id="import-xls" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= Html::button('', [
                    'class' => 'close',
                    'data' => [
                        'dismiss' => 'modal',
                    ],
                    'aria-hidden' => true,
                ]); ?>
                <h1><?= $header; ?></h1>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 24px">
                    <div class="col-xs-6">
                        <?= Html::a('<i class="fa fa-plus-circle"></i> Добавить файл', 'javascript:;', [
                            'class' => 'btn yellow add-xls-file',
                        ]); ?>
                    </div>
                    <div class="col-xs-6 upload-xls-button"
                         style="text-align: right;">
                        <?= Html::a('<i class="fa fa-download"></i> Загрузить', 'javascript:;', [
                            'class' => 'btn darkblue disabled',
                            'style' => 'color: #ffffff;',
                        ]); ?>
                    </div>
                </div>
                <div class="row xls-title">
                    <div class="gray-alert" style="text-align: center;">
                        <?= $text; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 20px;">
                        <div class="xls-error"></div>
                        <div class="row">
                            <?php ActiveForm::begin([
                                'action' => $uploadAction,
                                'method' => 'post',
                                'id' => 'xls-ajax-form',
                                'options' => [
                                    'enctype' => 'multipart/form-data',
                                    'data-progress' => $progressAction,
                                ],
                            ]); ?>
                            <?= $formData; ?>
                            <?= Html::hiddenInput('createdModels', null, [
                                'class' => 'created-models',
                            ]); ?>
                            <div class="file-list col-sm-12">
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <?= Html::a('Скачать шаблон таблицы', $uploadXlsTemplateUrl, [
                            'class' => 'upload-xls-template-url',
                        ]); ?>
                    </div>
                    <div class="row action-buttons buttons-fixed xls-buttons"
                         style="padding-top: 25px;margin-right: 0px;margin-left: 0px;display: none;">
                        <div class="button-bottom-page-lg col-sm-6 col-xs-6"
                             style="text-align: left;width: 50%">
                            <?= Html::a('Сохранить', 'javascript:;', [
                                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs undelete-models',
                            ]); ?>
                            <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                                'class' => 'btn darkblue widthe-100 hidden-lg undelete-models',
                            ]); ?>
                        </div>
                        <div class="button-bottom-page-lg col-sm-6 col-xs-6"
                             style="text-align: right;width: 50%;">
                            <?= Html::a('Отменить', 'javascript:;', [
                                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs xls-close',
                            ]); ?>
                            <?= Html::a('<i class="fa fa-reply fa-2x"></i>', 'javascript:;', [
                                'class' => 'btn darkblue widthe-100 hidden-lg xls-close',
                            ]); ?>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div id="progressBox"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
