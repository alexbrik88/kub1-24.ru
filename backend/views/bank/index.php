<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.07.2016
 * Time: 18:04
 */

use backend\models\Bank;
use backend\models\BankSearch;
use yii\bootstrap\Html;
use common\components\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\components\ImageHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel BankSearch */
/* @var $model Bank */

$this->title = 'Банки';
?>
    <div class="portlet box">
        <div class="btn-group pull-right title-buttons">
            <?= Html::button('<i class="fa fa-plus"></i> ДОБАВИТЬ', [
                'class' => 'btn yellow',
                'data-toggle' => 'modal',
                'href' => '#add-bank',
            ]); ?>
        </div>
        <h3 class="page-title"><?= $this->title; ?></h3>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
            </div>
            <div class="tools tools_button col-md-9 col-sm-9">
                <div class="form-body">
                    <?php $form = ActiveForm::begin([
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input text-middle"
                             style="width: 83%!important;">
                            <?= $form->field($searchModel, 'search')->textInput([
                                'placeholder' => 'Поиск по названию банка',
                            ]); ?>
                        </div>
                        <div class="wimax_button" style="float: right;">
                            <?= Html::submitButton('ПРИМЕНИТЬ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div class="dataTables_wrapper dataTables_extended_wrapper">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'style' => 'overflow-x: auto;',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'label' => '№',
                                'headerOptions' => [
                                    'width' => '3%',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return $model->id;
                                },
                            ],
                            [
                                'attribute' => 'bank_name',
                                'label' => 'Банк',
                                'headerOptions' => [
                                    'width' => '8%',
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return $model->bank_name;
                                },
                            ],
                            [
                                'attribute' => 'description',
                                'label' => 'Описание',
                                'headerOptions' => [
                                    'width' => '10%',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return (mb_strlen($model->description) > 100) ? mb_substr($model->description, 0, 100, 'UTF-8') . '...' : $model->description;
                                },
                            ],
                            [
                                'attribute' => 'logo_link',
                                'label' => 'Логотип',
                                'headerOptions' => [
                                    'width' => '3%',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    if ($model->logo_link) {
                                        return Html::a(
                                            '<span class="pull-center icon icon-paper-clip"></span>',
                                            Url::to(['upload-logo', 'id' => $model->id]),
                                            ['target' => '_blank',]
                                        );
                                    }

                                    return '';
                                },
                            ],
                            [
                                'attribute' => 'little_logo_link',
                                'label' => 'Логотип 2',
                                'headerOptions' => [
                                    'width' => '3%',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    if ($model->little_logo_link) {
                                        return ImageHelper::getThumb($model->getUploadDirectory() . $model->little_logo_link, [32, 32]);
                                    }

                                    return '';
                                },
                            ],
                            [
                                'attribute' => 'out_bank_name',
                                'label' => 'Выводим название',
                                'headerOptions' => [
                                    'width' => '8%',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return $model->out_bank_name ? $model->out_bank_name : '';
                                },
                            ],
                            [
                                'attribute' => 'url',
                                'label' => 'Ссылка',
                                'headerOptions' => [
                                    'width' => '5%',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return $model->url;
                                },
                            ],
                            [
                                'attribute' => 'email',
                                'label' => 'E-mail',
                                'headerOptions' => [
                                    'width' => '3%',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return $model->email;
                                },
                            ],
                            [
                                'attribute' => 'phone',
                                'label' => 'Телефон',
                                'headerOptions' => [
                                    'width' => '3%',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return $model->phone;
                                },
                            ],
                            [
                                'attribute' => 'contact_person',
                                'label' => 'Контактное лицо',
                                'headerOptions' => [
                                    'width' => '3%',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return $model->contact_person;
                                },
                            ],
                            [
                                'attribute' => 'request_count',
                                'label' => 'Количество заявок',
                                'headerOptions' => [
                                    'width' => '3%',
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return $model->request_count;
                                },
                            ],
                            [
                                'class' => \yii\grid\ActionColumn::className(),
                                'template' => '{block} {update} {delete}',
                                'headerOptions' => [
                                    'width' => '5%',
                                ],
                                'buttons' => [
                                    'update' => function ($url, Bank $data) {
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#update-bank-' . $data->id, [
                                            'data-toggle' => 'modal',
                                            'title' => Yii::t('yii', 'Обновить'),
                                            'aria-label' => Yii::t('yii', 'Обновить'),
                                        ]);
                                    },
                                    'block' => function ($url, Bank $data) {
                                        return \frontend\widgets\ConfirmModalWidget::widget([
                                            'toggleButton' => [
                                                'label' => '<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>',
                                                'class' => '',
                                                'tag' => 'a',
                                            ],
                                            'confirmUrl' => $url,
                                            'confirmParams' => [],
                                            'message' => 'Вы уверены, что хотите заблокировать банк партнера "' . $data->bank_name . '" ?',
                                        ]);
                                    },
                                    'delete' => function ($url, Bank $data) {
                                        return \frontend\widgets\ConfirmModalWidget::widget([
                                            'toggleButton' => [
                                                'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                                'class' => '',
                                                'tag' => 'a',
                                            ],
                                            'confirmUrl' => $url,
                                            'confirmParams' => [],
                                            'message' => 'Вы уверены, что хотите удалить банк партнера "' . $data->bank_name . '" ?',
                                        ]);
                                    },
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    $url = 0;
                                    switch ($action) {
                                        case 'update':
                                            $url = 'update';
                                            break;
                                        case 'delete':
                                            $url = 'delete';
                                            break;
                                        case 'block':
                                            $url = 'block';
                                            break;
                                    }

                                    return Url::to([$url, 'id' => $model->id]);
                                },
                            ],
                            [
                                'label' => 'Модальные окна',
                                'headerOptions' => [
                                    'style' => 'display:none;',
                                ],
                                'contentOptions' => [
                                    'style' => 'display:none;',
                                ],
                                'format' => 'raw',
                                'value' => function (Bank $model) {
                                    return $this->render('_modal_update', [
                                        'model' => Bank::findOne($model->id),
                                    ]);
                                },
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?= $this->render('_modal_create', [
    'model' => $model,
]); ?>