<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.07.2016
 * Time: 18:31
 */

use yii\bootstrap\ActiveForm;
use backend\models\Bank;
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 * @var $this  yii\web\View
 * @var $model Bank
 * @var $form  yii\bootstrap\ActiveForm
 */
$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-4 control-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line-product',
    ],
    'hintOptions' => [
        'tag' => 'small',
        'class' => 'text-muted',
    ],
    'horizontalCssClasses' => [
        'offset' => 'col-md-offset-4',
        'hint' => 'col-md-8',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$form = ActiveForm::begin([
    'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
    'options' => [
        'class' => 'form-horizontal',
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => true,
]);
?>
    <div class="form-body">
        <?= $form->field($model, 'bik', $config); ?>
        <?= $form->field($model, 'bank_name', $config); ?>
        <?= $form->field($model, 'out_bank_name', $config); ?>
        <?= $form->field($model, 'description', $config)->textarea([
            'rows' => 5,
            'style' => 'resize:none;',
        ]); ?>
        <?= $form->field($model, 'url', array_merge($config, [
            'wrapperOptions' => [
                'class' => 'col-md-4 inp_one_line-product',
            ],
        ])); ?>
        <?= $form->field($model, 'email', array_merge($config, [
            'wrapperOptions' => [
                'class' => 'col-md-4 inp_one_line-product',
            ],
        ])); ?>
        <?= $form->field($model, 'phone', array_merge($config, [
            'wrapperOptions' => [
                'class' => 'col-md-4 inp_one_line-product',
            ],
        ]))->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
            'options' => [
                'class' => 'form-control bank-phone',
                'placeholder' => '+7(XXX) XXX-XX-XX',
            ],
        ]);; ?>
        <?= $form->field($model, 'contact_person', array_merge($config, [
            'wrapperOptions' => [
                'class' => 'col-md-4 inp_one_line-product',
            ],
        ])); ?>
        <?= $form->field($model, 'logoImage', ['enableAjaxValidation' => false] + $config)->fileInput([
            'label' => false,
            'class' => 'js_filetype bank-logo',
            'accept' => 'image/gif, image/jpeg, image/png, image/bmp, ',
        ])->label('Логотип'); ?>
        <?= $form->field($model, 'littleLogoImage', ['enableAjaxValidation' => false] + $config)->fileInput([
            'label' => false,
            'class' => 'js_filetype bank-little-logo',
            'accept' => 'image/gif, image/jpeg, image/png, image/bmp, ',
        ])->label('Логотип 2'); ?>
        <?= $form->field($model, 'is_special_offer', $config)->checkbox([], false); ?>
        <?= Html::hiddenInput('Bank[has_logo]', $model->isNewRecord ? 0 : 1, [
            'id' => 'hidden-has-logo',
        ]); ?>
        <div class="form-actions">
            <div class="row action-buttons" id="buttons-fixed">
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    <?= Html::submitButton('Сохранить', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ]); ?>
                    <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                        'title' => 'Сохранить',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    <?= Html::button('Отменить', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                    ]); ?>
                    <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg back',
                        'title' => 'Отменить',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?php $form->end(); ?>