<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.07.2016
 * Time: 18:04
 */

use backend\models\Bank;

/* @var $this yii\web\View */
/* @var $model Bank */
/* @var $title string */
/* @var $id string */

$this->title = $title;
?>
<div class="modal fade <?= $model->isNewRecord ? 'new-bank' : 'exists-bank'; ?>" id="<?= $id; ?>" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h3 style="text-align: center; margin: 0"><?= $this->title; ?></h3>
            </div>
            <div class="modal-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>
