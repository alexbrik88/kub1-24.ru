<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.07.2016
 * Time: 19:09
 */

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows */

$this->title = 'Добавить банк в партнеры';
?>
<div class="bank-create">
    <div class="portlet box">
        <h3 class="page-title"><?= $this->title; ?></h3>
    </div>

    <?= $this->render('_partial/_form', [
        'model' => $model,
    ]) ?>
</div>
