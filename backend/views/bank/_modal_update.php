<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.07.2016
 * Time: 18:04
 */

use backend\models\Bank;

/* @var $this yii\web\View */
/* @var $model Bank */

echo $this->render('_partial/_modal_form', [
    'model' => $model,
    'title' => 'Обновить банк партнер',
    'id' => 'update-bank-' . $model->id,
]);