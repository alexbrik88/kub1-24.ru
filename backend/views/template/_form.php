<?php

use common\widgets\FormButtonsWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\template\Template */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="template-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sequence')->textInput(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= FormButtonsWidget::widget(); ?>

    <?php ActiveForm::end(); ?>

</div>
