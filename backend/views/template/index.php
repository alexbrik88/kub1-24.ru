<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\template\TemplateSearch */
/* @var $templateFile common\models\template\TemplateFile */
/* @var $templates common\models\template\Template[] */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Шаблоны';
$this->params['breadcrumbs'][] = $this->title;
$this->context->layoutWrapperCssClass = 'out-document reports';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <a href="<?= Url::to(['create']) ?>" class="btn yellow add-new-report">
            <i class="fa fa-plus"></i> ДОБАВИТЬ
        </a>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>
<div class="row">
    <div class="col-md-12">
        <?php if (!empty($templates)) { ?>
            <?php foreach ($templates as $template) { ?>
                <div class="portlet customer-info customer-info-cash" id="report_<?= $template->id; ?>">
                    <div class="portlet-title">
                        <div class="col-md-10 caption">
                            <?= $template->title; ?>
                        </div>

                        <div class="actions edit" id="report_edit_<?= $template->id; ?>">
                            <a data-id="<?= $template->id; ?>" class="btn darkblue btn-sm report-item" title="Редактировать">
                                <i class="icon-pencil"></i></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="datatable_ajax" class="table">
                            <tbody>
                            <?php foreach ($template->templateFiles as $file): ?>
                                <tr>
                                    <td>
                                        <span class="pull-left icon icon-paper-clip"></span>
                                        <?= Html::a($file->file_name, ['get-file', 'id' => $file->id, 'filename' => $file->file_name,], [
                                            'class' => 'padding-left-10',
                                            'target' => '_blank',
                                        ]); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <?= $this->render('_form_edit', [
                        'template' => $template,
                        'templateFile' => $templateFile,
                    ]);
                ?>
            <?php } ?>
        <?php } ?>
    </div>
</div>
