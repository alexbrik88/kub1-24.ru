<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $template common\models\template\Template */
/* @var $templateFile common\models\template\TemplateFile */
?>

<?= Html::beginForm('', 'post', [
    'id' => 'report_form_' . $template->id,
    'class' => 'form-horizontal hide',
    'enctype' => 'multipart/form-data',
]); ?>
    <div class="portlet customer-info">
        <div class="portlet-title">
            <?= Html::hiddenInput('id', $template->id); ?>
            <div class="col-md-10 caption">
                <div class="form-group">
                    <?= Html::activeLabel($template, 'sequence', [
                        'class' => 'col-md-4 control-label bold-text',
                    ]); ?>
                    <div class="col-md-12">
                        <?= Html::activeTextInput($template, 'sequence', [
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::activeLabel($template, 'title', [
                        'class' => 'col-md-4 control-label bold-text',
                    ]); ?>
                    <div class="col-md-12">
                        <?= Html::activeTextInput($template, 'title', [
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table id="datatable_ajax" class="table">
                <tbody>
                <?php foreach ($template->templateFiles as $file) { ?>
                    <tr>
                        <td>
                            <?= \frontend\widgets\ConfirmModalWidget::widget([
                                'toggleButton' => [
                                    'label' => '',
                                    'class' => 'delete-report-doc pull-left icon icon-close',
                                    'tag' => 'a',
                                ],
                                'confirmUrl' => Url::to(['delete-file', 'id' => $file->id]),
                                'confirmParams' => [],
                                'message' => 'Вы уверены, что хотите удалить файл? Данное действие необратимо.',
                            ]); ?>

                            <?= Html::a($file->file_name, $file->getFilePath($file->file_name, false), [
                                'class' => 'padding-left-10',
                                'target' => '_blank',
                            ]); ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="portlet action-add clearfix">
                <div class="btn-group pull-left report-padding-file-input">
                    <?= \frontend\widgets\FileUploaderWidget::widget([
                        'name' => Html::getInputName($templateFile, 'file_name'),
                        'wrapperOptions' => ['class' => 'btn-group pull-left report-padding-file-input',],
                        'inputWrapperOptions' => ['class' => 'yellow',],
                    ]); ?>
                </div>
            </div>
            <div class="row action-buttons" style="margin: 0;">
                <div class="repots-bottom-buttons">
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        ]); ?>
                        <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                            'class' => 'btn darkblue widthe-100 hidden-lg',
                        ]); ?>
                    </div>
                    <!--<div class="pull-right">-->
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <button type="button" class="btn darkblue cancel-edit-report widthe-100 hidden-md hidden-sm hidden-xs" data-id="<?= $template->id; ?>">Отменить</button>
                        <button type="button" class="btn darkblue cancel-edit-report widthe-100 hidden-lg" title="Отменить" data-id="<?= $template->id; ?>"><i class="fa fa-reply fa-2x"></i></button>
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <?= \frontend\widgets\ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => 'Удалить',
                                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                            ],
                            'confirmUrl' => Url::toRoute(['delete', 'id' => $template->id]),
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить этот блок? (Вместе с блоком удалятся и все файлы!)',
                        ]); ?>
                        <?= \frontend\widgets\ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                                'title' => 'Удалить',
                                'class' => 'btn darkblue widthe-100 hidden-lg',
                            ],
                            'confirmUrl' => Url::toRoute(['delete', 'id' => $template->id]),
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить этот блок? (Вместе с блоком удалятся и все файлы!)',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= Html::endForm(); ?>