<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Prompt;
use common\models\prompt\PageType;
use common\models\prompt\ForWhomPrompt;

/* @var $this yii\web\View */
/* @var $model backend\models\Prompt */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Prompts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prompt-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => Prompt::$PromptStatus[$model->status],
            ],
            [
                'attribute' => 'page_type_id',
                'format' => 'raw',
                'value' => PageType::findOne($model->page_type_id)->name,
            ],
            [
                'attribute' => 'for_whom_prompt_id',
                'format' => 'raw',
                'value' => ForWhomPrompt::findOne($model->for_whom_prompt_id)->name,
            ],
            [
                'attribute' => 'video_link',
                'format' => 'raw',
                'value' => !empty($model->video_link)
                    ? Html::a('<span class="glyphicon glyphicon-film"></span>', $model->video_link, [
                        'target' => '_blank',
                    ])
                    : 'Нет',
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => $model->title,
            ],
            [
                'attribute' => 'text',
                'format' => 'raw',
                'value' => $model->text,
            ],
        ],
    ]); ?>

</div>
