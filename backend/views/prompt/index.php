<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use backend\models\Prompt;
use common\models\prompt\PageType;
use common\models\prompt\ForWhomPrompt;
use frontend\modules\documents\components\FilterHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\PromptSearch */

$this->title = 'Подсказки';
$this->params['breadcrumbs'][] = $this->title;
$mass = [];
$mass[null] = 'Все';
$mass += Prompt::$PromptStatus;
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', Url::to(['create']), ['class' => 'btn yellow']); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            <?= $this->title; ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div
                class="dataTables_wrapper dataTables_extended_wrapper">
                <?= common\components\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'style' => 'overflow-x: auto;',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => [
                                'width' => '3%',
                            ],
                            'format' => 'raw',
                            'value' => function (Prompt $data) {
                                return Html::a($data->id, Url::toRoute(['view', 'id' => $data->id]));
                            },
                        ],
                        [
                            'attribute' => 'page_type_id',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (Prompt $data) {
                                return PageType::findOne($data->page_type_id)->name;
                            },
                        ],
                        [
                            'attribute' => 'for_whom_prompt_id',
                            'filter' => FilterHelper::getFilterArray(ForWhomPrompt::find()->all(), 'id', 'name', true, false),
                            'headerOptions' => [
                                'width' => '6%',
                            ],
                            'format' => 'raw',
                            'value' => function (Prompt $data) {
                                return ForWhomPrompt::findOne($data->for_whom_prompt_id)->name;
                            },
                        ],
                        [
                            'attribute' => 'title',
                            'headerOptions' => [
                                'width' => '40%',
                            ],
                            'format' => 'raw',
                            'value' => function (Prompt $data) {
                                return $data->title;
                            },
                        ],
                        [
                            'attribute' => 'video_link',
                            'headerOptions' => [
                                'width' => '4%',
                            ],
                            'format' => 'raw',
                            'value' => function (Prompt $data) {
                                if (!empty($data->video_link)) {
                                    return Html::a('<span class="glyphicon glyphicon-film"></span>', $data->video_link, [
                                        'target' => '_blank',
                                    ]);
                                }

                                return 'Нет';
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'filter' => FilterHelper::getPromptStatusArray(),
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'format' => 'raw',
                            'value' => function (Prompt $data) {
                                return Prompt::$PromptStatus[$data->status];
                            },
                        ],
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'template' => '{activate} {block} {update} {delete}',
                            'buttons' => [
                                'activate' => function ($url, Prompt $data) {
                                    if ($data->status == Prompt::BLOCKED) {
                                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                                            'title' => Yii::t('yii', 'Активировать'),
                                            'aria-label' => Yii::t('yii', 'Активировать'),
                                            'data-confirm' => Yii::t('yii', 'Вы действительно хотите активировать эту подсказку?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                        ]);
                                    }
                                },
                                'block' => function ($url, Prompt $data) {
                                    if ($data->status == Prompt::ACTIVE) {
                                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
                                            'title' => Yii::t('yii', 'Заблокировать'),
                                            'aria-label' => Yii::t('yii', 'Заблокировать'),
                                            'data-confirm' => Yii::t('yii', 'Вы действительно хотите заблокировать эту подсказку?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                        ]);
                                    }
                                },
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                $url = 0;
                                if ($action === 'activate') {
                                    $url = '/prompt/activate';
                                } elseif ($action === 'block') {
                                    $url = '/prompt/block';
                                } elseif ($action === 'delete') {
                                    $url = '/prompt/delete';
                                } elseif ($action === 'update') {
                                    $url = '/prompt/update';
                                }

                                return Url::to([$url, 'id' => $model->id]);
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

