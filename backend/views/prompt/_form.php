<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Prompt;
use common\models\prompt\PageType;
use yii\helpers\ArrayHelper;
use dosamigos\tinymce\TinyMce;
use common\widgets\FormButtonsWidget;
use common\models\prompt\ForWhomPrompt;

/* @var $this yii\web\View */
/* @var $model backend\models\Prompt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prompt-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')
        ->dropDownList(Prompt::$PromptStatus); ?>

    <?= $form->field($model, 'page_type_id')
        ->dropDownList(ArrayHelper::map(PageType::find()->all(), 'id', 'name')); ?>

    <?= $form->field($model, 'for_whom_prompt_id')
        ->dropDownList(ArrayHelper::map(ForWhomPrompt::find()->all(), 'id', 'name')); ?>

    <?= $form->field($model, 'video_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(TinyMce::className(), [
        'options' => [
            'rows' => 20,
        ],
        'language' => 'ru',
        'clientOptions' => [
            'menubar' => false,
            'statusbar' => false,
            'toolbar' => [
                'undo redo | hr bullist numlist table | link unlink | charmap',
                'bold italic underline strikethrough | subscript superscript | fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify',
            ],
            'plugins' => join(',', [
                'preview', 'paste', 'contextmenu', 'autoresize',
                'textcolor',
                'hr', 'table',
                'charmap',
                'link',
            ]),
            'external_plugins' => [],

            'contextmenu' => 'cut copy paste | link image inserttable | cell row column deletetable',
            'paste_as_text' => true,
            'resize' => 'both',
            'object_resizing' => 'img',
            'autoresize_max_height' => 300,
            'relative_urls' => false,
            'convert_urls' => true,
        ],
    ]); ?>

    <?= FormButtonsWidget::widget(); ?>

    <?php ActiveForm::end(); ?>

</div>
