<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Prompt */

$this->title = 'Создать подсказку';
$this->params['breadcrumbs'][] = ['label' => 'Prompts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prompt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
