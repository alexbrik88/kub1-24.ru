<?php

use common\components\date\DateHelper;
use common\models\notification\Notification;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\notification\ForWhom;

/* @var $this yii\web\View */
/* @var $searchModel common\models\notification\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Уведомления';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', Url::to(['create']), ['class' => 'btn yellow']); ?>
    </div>
    <div class="btn-group pull-right title-buttons">
        <?= Html::button('<i class="fa fa-download"></i> Загрузить из Excel', [
            'class' => 'btn yellow no-padding pull-right import-xls-button',
            'data' => [
                'toggle' => 'modal',
                'target' => '#import-xls',
            ],
        ]); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            <?= $this->title; ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div
                class="dataTables_wrapper dataTables_extended_wrapper">
                <?= common\components\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'style' => 'overflow-x: auto;',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => [
                                'width' => '3%',
                            ],
                            'format' => 'raw',
                            'value' => function (Notification $data) {
                                return Html::a($data->id, Url::toRoute(['view', 'id' => $data->id]));
                            },
                        ],
                        [
                            'attribute' => 'event_date',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'value' => function (Notification $data) {
                                return DateHelper::format($data->event_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'activation_date',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'value' => function (Notification $data) {
                                return DateHelper::format($data->activation_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'notification_type',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => 'text',
                            'value' => function (Notification $data) {
                                return Notification::$notificationTypeText[$data->notification_type];
                            },
                        ],
                        [
                            'attribute' => 'for_whom',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => 'text',
                            'value' => function (Notification $data) {
                                return ForWhom::$notificationForWhom[$data->for_whom];
                            },
                        ],
                        [
                            'attribute' => 'title',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'format' => 'text',
                        ],
                        [
                            'attribute' => 'text',
                            'headerOptions' => [
                                'width' => '40%',
                            ],
                            'format' => 'raw',
                        ],
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'template' => '{update} {delete}',
                            'headerOptions' => [
                                'width' => '2%',
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?= $this->render('/xls/_import_xls', [
    'header' => 'Загрузка уведомлений из Excel',
    'text' => '<p>Для загрузки списка уведомлений из Excel,
                   <br>
                   заполните шаблон таблицы и загрузите ее тут.
                   </p>',
    'formData' => Html::hiddenInput('className', 'Notification'),
    'uploadXlsTemplateUrl' => Url::to(['/notification/download-template',]),
    'uploadAction' => Url::to(['/notification/upload-xls']),
    'progressAction' => Url::to(['/notification/progress-upload-xls']),
]); ?>