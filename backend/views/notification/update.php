<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\notification\Notification */

$this->title = 'Обновить уведомление: ' . ' ' . $model->id;
?>
<div class="notification-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
