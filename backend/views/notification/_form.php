<?php

use common\components\date\DateHelper;
use common\models\notification\Notification;
use common\widgets\FormButtonsWidget;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use common\models\notification\ForWhom;

/* @var $this yii\web\View */
/* @var $model common\models\notification\Notification */
/* @var $form yii\widgets\ActiveForm */

$model->event_date = DateHelper::format($model->event_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
$model->activation_date = DateHelper::format($model->activation_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
?>

<div class="notification-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'notification_type')
        ->dropDownList(Notification::$notificationTypeText, [
            'class' => 'form-control budget-payment',
        ]); ?>

    <?= $form->field($model, 'for_whom')
        ->dropDownList(ForWhom::$notificationForWhom, [
            'class' => 'form-control budget-payment',
        ]); ?>

    <?= $form->field($model, 'event_date')->textInput([
        'class' => 'form-control date-picker',
        'size' => 16,
        'data-date-viewmode' => 'years',
    ]) ?>

    <?= $form->field($model, 'activation_date')->textInput([
        'class' => 'form-control date-picker',
        'size' => 16,
        'data-date-viewmode' => 'years',
    ]) ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'fine')->textInput() ?>

    <?= $form->field($model, 'text')->widget(TinyMce::className(), [
        'options' => [
            'rows' => 20,
        ],
        'language' => 'ru',
        'clientOptions' => [
            'menubar' => false,
            'statusbar' => false,
            'toolbar' => [
                'undo redo | hr bullist numlist table | link unlink | charmap',
                'bold italic underline strikethrough | subscript superscript | fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify'
            ],
            'plugins' => join(',', [
                'preview', 'paste', 'contextmenu', 'autoresize',
                'textcolor',
                'hr', 'table',
                'charmap',
                'link',
            ]),
            'external_plugins' => [],

            'contextmenu' => 'cut copy paste | link image inserttable | cell row column deletetable',
            'paste_as_text' => true,
            'resize' => 'both',
            'object_resizing' => 'img',
            'autoresize_max_height' => 300,
            'relative_urls' => false, // for jbimages plugin
            'convert_urls' => true,
        ],
    ]); ?>

    <?= FormButtonsWidget::widget(); ?>

    <?php ActiveForm::end(); ?>

</div>
