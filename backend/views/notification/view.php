<?php

use common\components\date\DateHelper;
use common\models\notification\Notification;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\models\notification\ForWhom;

/* @var $this yii\web\View */
/* @var $model common\models\notification\Notification */

$this->title = $model->title;
?>
<div class="notification-view">
    <?= Html::a('Назад к списку', Url::toRoute(['/notification/index']), ['class' => 'back-to-customers',]); ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Тип уведомления',
                'value' => Notification::$notificationTypeText[$model->notification_type],
            ],
            [
                'label' => 'Для кого',
                'value' => ForWhom::$notificationForWhom[$model->for_whom],
            ],
            [
                'attribute' => 'event_date',
                'format' => 'raw',
                'value' => DateHelper::format($model->event_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ],
            [
                'attribute' => 'activation_date',
                'format' => 'raw',
                'value' => DateHelper::format($model->activation_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ],
            [
                'attribute' => 'title',
            ],
            [
                'attribute' => 'fine',
            ],
            [
                'attribute' => 'text',
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
