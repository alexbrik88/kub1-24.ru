<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\news\News */

$this->title = 'Обновить новость: ' . ' ' . $model->id;
?>
<div class="notification-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
