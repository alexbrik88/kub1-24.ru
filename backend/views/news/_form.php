<?php

/* @var $this yii\web\View */
/* @var $model \common\models\news\News */
/* @var $form ActiveForm */

use common\components\date\DateHelper;
use common\models\news\News;
use common\widgets\FormButtonsWidget;
use dosamigos\tinymce\TinyMce;
use yii\widgets\ActiveForm;

if (!empty($model->date)) {
    $model->date = DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
} else {
    $model->date = date(DateHelper::FORMAT_USER_DATE);
}

if ($model->status === null) {
    $model->status = News::STATUS_ACTIVE;
}

if ($model->type === null) {
    $model->type = News::TYPE_NEWS;
}
?>

<div class="news-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList(News::$statusesMap) ?>

    <?= $form->field($model, 'type')->dropDownList(News::$typesMap) ?>

    <?= $form->field($model, 'serial_number')->input('number', [
        'step' => 1,
        'min' => 1,
    ]) ?>

    <?= $form->field($model, 'date')->textInput([
        'class' => 'form-control date-picker',
        'size' => 16,
        'data-date-viewmode' => 'years',
    ]) ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'likes_count')->input('number', [
        'step' => 1,
        'min' => 1,
    ]) ?>

    <?= $form->field($model, 'dislikes_count')->input('number', [
        'step' => 1,
        'min' => 1,
    ]) ?>

    <?= $form->field($model, 'text')->widget(TinyMce::class, [
        'options' => [
            'rows' => 20,
        ],
        'language' => 'ru',
        'clientOptions' => [
            'menubar' => false,
            'statusbar' => false,
            'toolbar' => [
                'undo redo | hr bullist numlist table | link unlink | charmap | image',
                'bold italic underline strikethrough | subscript superscript | fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify'
            ],
            'plugins' => join(',', [
                'preview', 'paste', 'contextmenu', 'autoresize',
                'textcolor',
                'hr', 'table',
                'charmap',
                'link',
                'media', 'image',
            ]),
            'external_plugins' => [],
            'contextmenu' => 'cut copy paste | link image inserttable | cell row column deletetable',
            'paste_as_text' => true,
            'resize' => 'both',
            'object_resizing' => 'img',
            'autoresize_max_height' => 300,
            'relative_urls' => false,
            'convert_urls' => true,
        ],
    ]); ?>

    <?= FormButtonsWidget::widget(); ?>

    <?php ActiveForm::end(); ?>
</div>
