<?php

/* @var $this yii\web\View */
/* @var $model \common\models\news\News */

use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\news\News;
use common\models\news\NewsReaction;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = $model->name;
?>
<div class="notification-view">
    <?= Html::a('Назад к списку', Url::toRoute(['/news/index']), ['class' => 'back-to-customers',]); ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'date',
                'value' => DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ],
            [
                'attribute' => 'status',
                'value' => News::$statusesMap[$model->status],
            ],
            [
                'attribute' => 'type',
                'value' => News::$typesMap[$model->type],
            ],
            [
                'attribute' => 'serial_number',
            ],
            [
                'attribute' => 'text',
                'format' => 'raw',
                'value' => function() use ($model): string {
                    return str_replace(['&lt;', '&gt;'], ['<', '>'], $model->text);
                },
            ],
            [
                'attribute' => 'likes_count',
                'value' => function() use ($model): int {
                    return $model->getLikesCount();
                },
            ],
            [
                'attribute' => 'dislikes_count',
                'value' => function() use ($model): int {
                    return $model->getDislikesCount();
                },
            ],
        ],
    ]) ?>

</div>
