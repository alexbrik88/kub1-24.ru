<?php

/* @var $this yii\web\View */
/* @var $searchModel \backend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\news\News;
use yii\grid\ActionColumn;
use yii\helpers\Url;

$this->title = 'Что нового';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', Url::to(['create']), ['class' => 'btn yellow']); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            <?= $this->title; ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div
                class="dataTables_wrapper dataTables_extended_wrapper">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'style' => 'overflow-x: auto;',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'columns' => [
                        [
                            'attribute' => 'date',
                            'value' => function (News $model): string {
                                return DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function (News $model): string {
                                return News::$statusesMap[$model->status];
                            },
                        ],
                        [
                            'attribute' => 'type',
                            'value' => function (News $model): string {
                                return News::$typesMap[$model->type];
                            },
                        ],
                        [
                            'attribute' => 'name',
                            'value' => function (News $model) {
                                return $model->name;
                            },
                        ],
                        [
                            'attribute' => 'likes_count',
                            'label' => 'Лайк',
                            'value' => function (News $model) {
                                return $model->getLikesCount();
                            },
                        ],
                        [
                            'attribute' => 'dislikes_count',
                            'label' => 'Дизлайк',
                            'value' => function (News $model) {
                                return $model->getDislikesCount();
                            },
                        ],
                        [
                            'class' => ActionColumn::class,
                            'template' => '{view} {update} {delete}',
                            'headerOptions' => [
                                'width' => '2%',
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>