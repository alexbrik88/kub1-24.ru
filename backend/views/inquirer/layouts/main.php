<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.01.2017
 * Time: 4:17
 */

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@backend/views/layouts/main.php');
?>
    <div class="debt-report-content container-fluid"
         style="padding: 0; margin-top: -10px;">
        <?php NavBar::begin([
            'brandLabel' => 'Опросник',
            'brandUrl' => null,
            'options' => [
                'class' => 'navbar-report navbar-default',
                'style' => 'margin-bottom: 20px;',
            ],
            'brandOptions' => [
                'style' => 'margin-left: 0;'
            ],
            'innerContainerOptions' => [
                'class' => 'container-fluid',
                'style' => 'padding: 0;'
            ],
        ]);
        echo Nav::widget([
            'items' => [
                ['label' => 'Опрос 1', 'url' => ['/inquirer/index']],
                ['label' => 'Опрос 2', 'url' => ['/inquirer/index-two']],
            ],
            'options' => ['class' => 'navbar-nav navbar-right'],
        ]);
        NavBar::end();
        ?>
        <?= $content; ?>
    </div>
<?php $this->endContent(); ?>