<?php

use common\components\grid\GridView;
use common\models\Company;
use common\models\Inquirer;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InquirerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Опрос 1';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-9 col-sm-9">
    </div>
    <div class="col-md-3 col-sm-3" style="margin-bottom: 20px;">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <?= GridView::widget([
                    'id' => 'inquirer-grid-view',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'value' => 'company.id',
                        ],

                        [
                            'attribute' => 'company_id',
                            'label' => 'Название',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'content' => function($data) {
                                return Html::a($data->company->shortName, ['/company/company/view', 'id' => $data->company_id]);
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'employee_id',
                            'label' => 'email',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'value' => 'employee.email',
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Дата регистрации',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'value' => 'company.created_at',
                            'format' => 'date',
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'headerOptions' => [
                                'class' => 'dropdown-filter dropdown-pre-wrap',
                                'width' => '10%',
                            ],
                            'filter' => ['' => 'Все'] + Company::$activationType,
                            'value' => 'company.activationStatus',
                        ],
                        [
                            'attribute' => 'rating',
                            'label' => 'Рейтинг',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'filter' => ['' => 'Все', '1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '1-2-3'=> '1-3', '4-5'=>'4-5'],
                        ],
                        [
                            'attribute' => 'difficulty',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'label' => 'С какими трудностями вы столкнулись',
                        ],
                        [
                            'attribute' => 'plan_id',
                            'label' => 'Планируете ли вы использовать КУБ в будущем',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'filter' => ['' => 'Все'] + Inquirer::$planLabels,
                            'value' => 'plan',
                        ],
                        [
                            'attribute' => 'problem',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'label' => 'Сервис помогает  решить вашу проблему? Кстати какую?',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'buttons' => [
                                'delete' => function ($url, $data) {
                                    return \frontend\widgets\ConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                            'class' => '',
                                            'tag' => 'a',
                                        ],
                                        'confirmUrl' => $url,
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите удалить этот опросник?',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
