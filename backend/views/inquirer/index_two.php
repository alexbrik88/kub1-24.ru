<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.10.2017
 * Time: 17:53
 */

use common\components\grid\GridView;
use common\models\Company;
use common\models\ServiceMoreStock;
use yii\helpers\Html;
use common\components\date\DateHelper;
use yii\helpers\Url;
use common\components\grid\DropDownSearchDataColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InquirerTwoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Опрос 2';
?>
<div class="row">
    <div class="col-md-9 col-sm-9">
    </div>
    <div class="col-md-3 col-sm-3" style="margin-bottom: 20px;">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <?= GridView::widget([
                    'id' => 'inquirer-grid-view',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                    'columns' => [
                        [
                            'attribute' => 'company.created_at',
                            'label' => 'Дата регистрации',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'format' => 'raw',
                            'content' => function(ServiceMoreStock $model) {
                                return date(DateHelper::FORMAT_USER_DATE, $model->company->created_at);
                            },
                        ],
                        [
                            'attribute' => 'service_more_stock.created_at',
                            'label' => 'Дата опроса',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'format' => 'raw',
                            'content' => function(ServiceMoreStock $model) {
                                return date(DateHelper::FORMAT_USER_DATE, $model->created_at);
                            },
                        ],
                        [
                            'attribute' => 'company.shortName',
                            'label' => 'Название',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'format' => 'raw',
                            'content' => function(ServiceMoreStock $model) {
                                return Html::a($model->company->getShortName(), Url::to(['/company/company/view', 'id' => $model->company_id]));
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'activities_id',
                            'label' => 'Вид деятельности',
                            'headerOptions' => [
                                'width' => '15%',
                                'class' => 'dropdown-filter',
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getActivitiesFilter(),
                            'content' => function(ServiceMoreStock $model) {
                                return $model->activities ? $model->activities->name : $model->company_type_text;
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'specialization',
                            'label' => 'Специализация',
                            'headerOptions' => [
                                'width' => '15%',
                                'class' => 'dropdown-filter',
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getSpecializationFilter(),
                            'content' => function(ServiceMoreStock $model) {
                                return ServiceMoreStock::$specializationArray[$model->specialization];
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'average_invoice_count',
                            'label' => 'Среднее кол-во счетов в месяц',
                            'headerOptions' => [
                                'width' => '15%',
                                'class' => 'dropdown-filter',
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getAverageInvoiceCountFilter(),
                            'content' => function(ServiceMoreStock $model) {
                                return ServiceMoreStock::$averageInvoiceCountArray[$model->average_invoice_count];
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'employees_count',
                            'label' => 'Кол-во сотрудников',
                            'headerOptions' => [
                                'width' => '10%',
                                'class' => 'dropdown-filter',
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getEmployeesCountFilter(),
                            'content' => function(ServiceMoreStock $model) {
                                return ServiceMoreStock::$employeesCountArray[$model->employees_count];
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'accountant_id',
                            'label' => 'Кто ведет бухгалтерию',
                            'headerOptions' => [
                                'width' => '15%',
                                'class' => 'dropdown-filter',
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getAccountantFilter(),
                            'content' => function(ServiceMoreStock $model) {
                                return ServiceMoreStock::$accountantArray[$model->accountant_id];
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'has_logo',
                            'label' => 'Есть логотип?',
                            'headerOptions' => [
                                'width' => '15%',
                                'class' => 'dropdown-filter',
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getLogoFilter(),
                            'content' => function(ServiceMoreStock $model) {
                                return ServiceMoreStock::$hasLogoArray[$model->has_logo];
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

