<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\company\InnDeny */

$this->title = 'Create Inn Deny';
$this->params['breadcrumbs'][] = ['label' => 'Inn Denies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inn-deny-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
