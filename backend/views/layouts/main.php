<?php
use backend\assets\AppAsset;
use frontend\widgets\Alert;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$session = Yii::$app->session;
$sideBarStatus = !empty($session['sidebar']) ? $session['sidebar']['status'] : 0;
$status = $sideBarStatus ? 'page-sidebar-closed' : '';
$pageCss = !empty($this->context->layoutWrapperCssClass) ? $this->context->layoutWrapperCssClass : '';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css"/>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content home-page  <?= $status; ?>">

<?php echo backend\widgets\MainMenuWidget::widget(); ?>

<div class="page-container">
    <?php echo backend\widgets\SideMenuWidget::widget(); ?>

    <div class="page-content-wrapper">
        <div class="page-content <?= $pageCss; ?>">
            <?= Alert::widget(); ?>

            <?php echo $content ?>
        </div>
    </div>
</div>

<div class="page-footer">
    <div class="page-footer-inner">
        &copy; ООО "КУБ", <?= date('Y'); ?>
    </div>
    <div class="page-footer-inner pull-right">
    </div>
</div>

<?php $this->endBody(); ?>

<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
    });
</script>

</body>
</html>
<?php $this->endPage(); ?>
