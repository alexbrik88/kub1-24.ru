<?php

use frontend\components\PageSize;
use yii\helpers\Html;
use yii\helpers\Url;

if (empty($pageSizeParam)) {
    $pageSizeParam = 'per-page';
}

$perPage = PageSize::get($pageSizeParam);
$items = PageSize::$items;
?>
<ul class="pagination pull-left">
    <li><span class="no-style">Выводить по количеству строк:</span></li>
    <?php foreach ($items as $key => $value) : ?>
        <?= Html::tag('li', Html::a($value, Url::current(['page' => null, $pageSizeParam => $key])), [
            'class' => $perPage == $key ? 'active': null,
        ]) ?>
    <?php endforeach ?>
</ul>
