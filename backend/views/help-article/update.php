<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\helpArticle\HelpArticle */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Help Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="help-article-update">

    <a class="back-to-customers" href="<?= Url::toRoute(['/help-article/index']) ?>">Назад к списку</a>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
