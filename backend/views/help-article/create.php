<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\helpArticle\HelpArticle */

$this->title = 'Создать статью справку';
$this->params['breadcrumbs'][] = ['label' => 'Help Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="help-article-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
