<?php

use common\widgets\FormButtonsWidget;
use dosamigos\tinymce\TinyMce;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\helpArticle\HelpArticle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="help-article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sequence')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'text')->widget(TinyMce::className(), [
        'options' => [
            'rows' => 20,
        ],
        'language' => 'ru',
        'clientOptions' => [
            'menubar' => false,
            'statusbar' => false,
            'toolbar' => [
                'undo redo | hr bullist numlist table | link unlink | charmap',
                'bold italic underline strikethrough | subscript superscript | fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify'
            ],
            'plugins' => join(',', [
                'preview', 'paste', 'contextmenu', 'autoresize',
                'textcolor',
                'hr', 'table',
                'charmap',
                'link',
            ]),
            'external_plugins' => [],

            'contextmenu' => 'cut copy paste | link image inserttable | cell row column deletetable',
            'paste_as_text' => true,
            'resize' => 'both',
            'object_resizing' => 'img',
            'autoresize_max_height' => 300,
            'relative_urls' => false, // for jbimages plugin
            'convert_urls' => true,
        ],
    ]); ?>

    <?= FormButtonsWidget::widget(); ?>

    <?php ActiveForm::end(); ?>

</div>
