<?php

use common\models\helpArticle\HelpArticle;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\helpArticle\HelpArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи справки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portlet box">

    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<button class="btn yellow"><i class="fa fa-plus"></i>Добавить</button>',
            Url::toRoute(['/help-article/create']))?>
    </div>

    <h3 class="page-title"><?= $this->title; ?></h3>

</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            <?= $this->title; ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">

                <?= common\components\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],

                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],

                    'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",

                    'columns' => [
                        [
                            'attribute' => 'sequence',
                            'label' => 'Порядок следования',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'text',
                        ],

                        [
                            'attribute' => 'title',
                            'label' => 'Наименивание',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (HelpArticle $data) {
                                return Html::a($data->title, Url::toRoute(['/help-article/update', 'id' => $data->id]));
                            },
                        ],
                        [
                            'label' => 'Удалить',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a('Удалить', Url::toRoute(['/help-article/delete', 'id' => $data->id]), [
                                    'class' => 'btn darkblue',
                                    'style' => 'color: white',
                                    'data' => [
                                        'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
