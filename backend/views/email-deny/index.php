<?php

use common\models\employee\EmailDeny;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EmailDenySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Черный список Email';
?>
<div class="email-deny-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="form-group text-right">
        <?php Modal::begin([
            'header' => '<h1>Добавить Email в черный список</h1>',
            'toggleButton' => [
                'label' => '<i class="fa fa-plus"></i> Добавить',
                'class' => 'btn yellow',
            ],
        ]) ?>

        <?= $this->render('_form', [
            'model' => new EmailDeny(),
        ]) ?>

        <?php Modal::end() ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'email',
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],
    ]); ?>
</div>
