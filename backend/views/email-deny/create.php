<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\employee\EmailDeny */

$this->title = 'Create Email Deny';
$this->params['breadcrumbs'][] = ['label' => 'Email Denies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-deny-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
