<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\employee\EmailDeny */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-deny-form">

    <?php $form = ActiveForm::begin([
        'action' => ['create'],
    ]); ?>

    <?= $form->field($model, 'email')->textInput([
        'maxlength' => true,
        'style' => 'width: 100%;',
        'enableAjaxValidation' => true,
    ]) ?>

    <div class="row">
        <div class="col-xs-6">
            <?= Html::submitButton('Добавить', [
                'class' => 'btn darkblue text-white',
            ]) ?>
        </div>
        <div class="col-xs-6 text-right">
            <?= Html::button('Отменить', [
                'class' => 'btn darkblue text-white',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
