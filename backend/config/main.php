<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',

    'homeUrl' => '/admin',

    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],

    'modules' => [
        'analytics' => [
            'class' => 'backend\modules\analytics\Module',
        ],
        'employee' => [
            'class' => 'backend\modules\employee\Module',
        ],
        'company' => [
            'class' => 'backend\modules\company\Module',
            'modules' => [
                'import' => [
                    'class' => 'backend\modules\company\modules\import\Module',
                ],
            ],
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ],
        'service' => [
            'class' => 'backend\modules\service\Module',
        ],
        'report' => [
            'class' => 'backend\modules\report\Module',
        ],
        'reference' => [
            'class' => 'backend\modules\reference\Module',
        ],
        'api' => [
            'class' => 'backend\modules\api\Module',
        ],
        'robot' => [
            'class' => 'backend\modules\robot\Module',
        ],
        'unisender' => [
            'class' => 'backend\modules\unisender\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ],
        'recommendations' => [
            'class' => 'backend\modules\recommendations\Module',
        ],
    ],

    'aliases' => [
        // documents
        '@documents' => '@frontend/modules/documents',
        '@documentsUrl' => '/documents',
        // subscribe
        '@subscribe' => '@frontend/modules/subscribe',
        '@subscribeUrl' => '/subscribe',
    ],

    'components' => [
        'request' => [
            'baseUrl' => '/admin',
            'csrfParam' => '_backendCSRF',
            'csrfCookie' => [
                'httpOnly' => true,
                'secure' => YII_ENV_PROD
            ],
            'secureProtocolHeaders' => [
                'x-scheme' => ['https'],
                'X-Forwarded-Proto' => ['https'],
            ],
        ],
        'session' => [
            'name' => 'advanced-backend',
            'cookieParams' => [
                'httpOnly' => true,
                'secure' => YII_ENV_PROD,
            ],
        ],

        'user' => [
            'identityClass' => 'common\models\User',
            'identityCookie' => [
                'name' => '_identity-backend',
                'httpOnly' => true,
                'secure' => YII_ENV_PROD
            ],
            'enableAutoLogin' => true,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG || true ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'validation',
                        'robokassa',
                        'banking',
                    ],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'categories' => ['validation'],
                    'logFile' => '@runtime/logs/validation.log',
                ],
            ],
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
