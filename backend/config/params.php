<?php
return [
    'maxCashSum' => 999 * 1000 * 1000, // without kopeck

    'email' => [
        'logo' => '@frontend/web/img/footer-logo.png',
        'logoWhite' => '@frontend/web/img/Logo_opacity_small.png',
        'link' => [
            'style' => 'font-style: italic; color: #70b0d6; text-decoration: underline;',
            'target' => '_blank',
        ],
    ],
];
