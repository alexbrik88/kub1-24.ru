<?php

namespace backend\widgets;

use backend\modules\company\models\CompanySearch;
use yii\base\Widget;
use Yii;

/**
 * Class MainMenuWidget
 * @package backend\widgets
 */
class MainMenuWidget extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $searchModel = new CompanySearch();
        $searchModel->blockedStatus = CompanySearch::ALL;
        $searchModel->load(Yii::$app->request->get());

        return $this->render('mainMenuWidget', [
            'searchModel' => $searchModel,
        ]);
    }
}
