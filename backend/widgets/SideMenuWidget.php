<?php

namespace backend\widgets;

use yii\base\Widget;
use Yii;

/**
 * Class SideMenuWidget
 * @package backend\widgets
 */
class SideMenuWidget extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $session = Yii::$app->session;
        $sideBarStatus = !empty($session['sidebar']) ? $session['sidebar']['status'] : 0;

        return $this->render('sideMenuWidget', [
            'sideBarStatus' => $sideBarStatus,
        ]);
    }
}
