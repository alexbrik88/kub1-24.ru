<?php
// TODO: set phpdoc.

use yii\bootstrap\Html;
use backend\modules\company\models\CompanySearch;
use common\components\date\DateHelper;

/* @var $searchModel CompanySearch */
?>

<div class="page-header navbar navbar-fixed-top">
    <!-- begin header inner -->
    <div class="page-header-inner">
        <!-- begin logo -->
        <div class="page-logo">
            <a href="/">
            </a>

            <div class="menu-toggler sidebar-toggler admin">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- end logo -->
        <div class="page-header-date">
            <?= date(DateHelper::FORMAT_USER_DATE); ?>
        </div>
        <!-- begin responsive menu toggler -->
        <a data-target=".navbar-collapse" data-toggle="collapse"
           class="menu-toggler responsive-toggler" href="javascript:;"></a>
        <!-- end responsive menu toggler -->
        <!-- begin top navigation menu -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <?php if (!Yii::$app->user->isGuest): ?>
                    <li class="dropdown dropdown-extended dropdown-notification">
                        <div class="tools tools_button col-md-9 col-sm-9" style="margin-top: 6px;">
                            <div class="form-body">
                                <?php $form = \yii\widgets\ActiveForm::begin([
                                    'action' => ['/company/company/index'],
                                    'method' => 'GET',
                                    'fieldConfig' => [
                                        'template' => "{input}\n{error}",
                                        'options' => [
                                            'class' => '',
                                        ],
                                    ],
                                ]); ?>
                                <div class="search_cont">
                                    <div class="wimax_input text-middle"
                                         style="width: 60%!important; float:left;">
                                        <?= $form->field($searchModel, 'search')->textInput([
                                            'placeholder' => 'Поиск по ID, названию, ИНН или E-mail',
                                        ]); ?>
                                        <?= $form->field($searchModel, 'blockedStatus')->hiddenInput(); ?>
                                    </div>
                                    <div class="wimax_button spinner-button" style="float: left;">
                                        <?= Html::submitButton('<span class="ladda-label">ПРИМЕНИТЬ</span><span class="ladda-spinner"></span>', [
                                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze mt-ladda-btn ladda-button',
                                            'data-style' => 'expand-right',
                                        ]) ?>
                                    </div>
                                </div>
                                <?php $form->end(); ?>
                            </div>
                        </div>
                    </li>
                <?php endif; ?>
                <!-- begin quick sidebar toggler -->
                <!-- doc: apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-notification">
                    <?php echo yii\helpers\Html::a('<i class="icon-logout"></i>', Yii::$app->urlManager->createUrl('site/logout'), [
                        'class' => 'dropdown-toggle logout-btn',
                        'title' => 'Logout' . (Yii::$app->user->identity ? ' (' . Yii::$app->user->identity->username . ')' : ''),
                        'data-method' => 'post',
                    ]); ?>
                </li>
                <!-- end quick sidebar toggler -->
            </ul>
        </div>
        <!-- end top navigation menu -->
    </div>
    <!-- end header inner -->
</div>
