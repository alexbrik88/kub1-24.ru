<?php

use common\models\service\SubscribeTariffGroup;
use yii\helpers\Url;

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$module = Yii::$app->controller->module->id;

/** @var integer $sideBarStatus */
/** @var string $module */

$status = $sideBarStatus ? 'page-sidebar-menu-closed' : '';
?>

<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <?php if (!Yii::$app->user->isGuest) {
            echo \common\widgets\SideMenuNav::widget([
                'activateParents' => true,
                'items' => [
                    [
                        'label' => 'Главная страница',
                        'encode' => false,
                        'url' => ['/site/index'],
                        'linkOptions' => [
                            'class' => 'start',
                        ],
                    ],
                    [
                        'label' => 'Блок Аналитика<span class="arrow"></span>',
                        'encode' => false,
                        'active' => $module == 'analytics',
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                        'items' => [
                            [
                                'label' => 'Дашборд',
                                'url' => ['/analytics/default/index'],
                            ],
                            [
                                'label' => 'Расширенный список',
                                'url' => ['/analytics/default/extended'],
                            ],
                        ]
                    ],
                    [
                        'label' => 'Пользователи',
                        'encode' => false,
                        'url' => ['/employee/default/index'],
                        'active' => $module == 'company' && $controller === 'company' && $action === 'index',
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Компании',
                        'encode' => false,
                        'url' => ['/company/company/index'],
                        'active' => $module == 'company' && $controller === 'company' && $action === 'index',
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Расширенный список',
                        'encode' => false,
                        'url' => ['/company/company/index-additional'],
                        'active' => $module == 'company' && $controller === 'company' && ($action === 'index-additional' || $action === 'index-documents'),
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Партнеры',
                        'encode' => false,
                        'url' => ['/company/partner/index'],
                        'active' => $module == 'company' && $controller === 'partner' && $action === 'index',
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Отчеты<span class="arrow"></span>',
                        'encode' => false,
                        'active' => $module == 'report' && $controller === 'report',
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                        'items' => [
                            [
                                'label' => 'Стандартные',
                                'url' => ['/report/report/registration'],
                                'active' => $module == 'report' && $controller === 'report' && ($action != 'robot' && $action != 'robot-download' && $action !== 'urotdel'),
                            ],
                            [
                                'label' => 'Робот-бухгалтер',
                                'url' => ['/report/report/robot'],
                                'active' => $module == 'report' && $controller === 'report' && ($action == 'robot' || $action == 'robot-download'),
                            ],
                            [
                                'label' => 'Коэффициенты',
                                'url' => ['/report/coefficient/by-group', 'group_id' => SubscribeTariffGroup::STANDART],
                                'active' => $module == 'report' && $controller === 'coefficient',
                            ],
                            [
                                'label' => 'ЮрОтдел',
                                'url' => ['/report/report/urotdel'],
                                'active' => $module === 'report' && $controller === 'report' && $action === 'urotdel',
                            ],
                        ]
                    ],
                    [
                        'label' => 'API',
                        'encode' => false,
                        'url' => ['/api/partners/index'],
                        'active' => $module == 'api' && $controller === 'partners' && $action === 'index',
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Продажи<span class="arrow"></span>',
                        'encode' => false,
                        'active' => $module === 'service' && in_array($controller, ['selling-subscribe',
                                'selling-invoice', 'selling-payment']),
                        'items' => [
                            [
                                'label' => 'Подписки',
                                'url' => ['/service/selling/subscribe'],
                                'active' => $module === 'service' && $controller === 'selling' && $action === 'subscribe',
                            ],
                            [
                                'label' => 'Счета',
                                'url' => ['/service/selling/invoice'],
                                'active' => $module === 'service' && $controller === 'selling' && $action === 'invoice',
                            ],
                            [
                                'label' => 'Оплаты',
                                'url' => ['/service/selling/payment'],
                                'active' => $module === 'service' && $controller === 'selling' && $action === 'payment',
                            ],
                            [
                                'label' => 'Свод',
                                'url' => ['/service/selling/summary'],
                                'active' => $module === 'service' && $controller === 'selling' && $action === 'summary',
                            ],
                        ],
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                    ],
                    [
                        'label' => 'Оплата<span class="arrow"></span>',
                        'encode' => false,
                        'active' => $module === 'service' && in_array($controller, ['statistic', 'subscribe', 'payment']),
                        'items' => [
                            [
                                'label' => 'Счета',
                                'url' => ['/service/subscribe/index'],
                                'active' => $module === 'service' && $controller === 'subscribe',
                            ],
                            [
                                'label' => 'Платежи',
                                'url' => ['/service/payment/index'],
                                'active' => $module === 'service' && $controller === 'payment',
                            ],
                            [
                                'label' => 'Статистика',
                                'url' => ['/service/statistic/index'],
                                'active' => $module === 'service' && $controller === 'statistic',
                            ],
                            [
                                'label' => 'Вознаграждение',
                                'url' => ['/service/affiliate/index'],
                                'active' => $module === 'service' && $controller === 'affiliate',
                            ],
                        ],
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                    ],
                    [
                        'label' => 'Промо коды<span class="arrow"></span>',
                        'encode' => false,
                        'linkOptions' => [],
                        'active' => $module === 'service' && $controller === 'promo-code',
                        'items' => [
                            [
                                'label' => 'Все промо коды',
                                'url' => ['/service/promo-code/index'],
                                'active' => $module === 'service' && $controller === 'promo-code' && $action !== 'statistic',
                            ],
                            [
                                'label' => 'Статистика',
                                'url' => ['/service/promo-code/statistic'],
                                'active' => $module === 'service' && $controller === 'promo-code' && $action === 'statistic',
                            ],
                        ],
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                    ],
                    [
                        'label' => 'ООО "КУБ"<span class="arrow"></span>',
                        'encode' => false,
                        'active' => $module === 'service' && in_array($controller, ['contractor', 'company']),
                        'items' => [
                            [
                                'label' => 'Контрагент системы',
                                'url' => ['/service/contractor'],
                                'active' => $module === 'service' && $controller === 'contractor',
                            ],
                            [
                                'label' => 'Компания системы',
                                'url' => ['/service/company'],
                                'active' => $module === 'service' && $controller === 'company',
                            ],
                        ],
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                    ],
                    [
                        'label' => 'Шаблоны',
                        'encode' => false,
                        'url' => Url::to(['/template/index']),
                        'active' => $controller === 'template',
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Справка',
                        'encode' => false,
                        'url' => Url::to(['/help-article/index']),
                        'active' => $controller === 'help-article',
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Уведомления<span class="arrow"></span>',
                        'encode' => false,
                        'active' => in_array($controller, ['notification', 'news']),
                        'items' => [
                            [
                                'label' => 'Уведомления',
                                'url' => Url::to(['/notification/index']),
                                'active' => $controller === 'notification',
                            ],
                            [
                                'label' => 'Что нового',
                                'url' => Url::to(['/news/index']),
                                'active' => $controller === 'news',
                            ],
                        ],
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                    ],
                    [
                        'label' => 'Подсказки',
                        'encode' => false,
                        'url' => Url::to(['/prompt/index']),
                        'active' => $controller === 'prompt',
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Банки',
                        'encode' => false,
                        'url' => Url::to(['/bank/index']),
                        'active' => $controller === 'bank',
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Справочник <span class="arrow"></span>',
                        'encode' => false,
                        'active' => $module === 'reference',
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                        'items' => [
                            [
                                'label' => 'Виды деятельности',
                                'url' => Url::to(['/reference/activities/index']),
                                'active' => $controller === 'activities',
                                'linkOptions' => [],
                            ],
                            [
                                'label' => 'Формы собственности',
                                'url' => Url::to(['/reference/company-type/index']),
                                'active' => $controller === 'company-type',
                                'linkOptions' => [],
                            ],
                            [
                                'label' => 'Фикс. платежи по ИП',
                                'url' => Url::to(['/reference/fixed-payments-ip/index']),
                                'active' => $controller === 'fixed-payments-ip',
                                'linkOptions' => [],
                            ],
                            [
                                'label' => 'Ед. измерений',
                                'url' => Url::to(['/reference/product-unit/index']),
                                'active' => $controller === 'product-unit',
                                'linkOptions' => [],
                            ],
                            [
                                'label' => 'Коды ОКВЭД',
                                'url' => Url::to(['/reference/okved-code/index']),
                                'active' => $controller === 'okved-code',
                                'linkOptions' => [],
                            ],
                            [
                                'label' => 'Пиксель',
                                'url' => Url::to(['/reference/pixel/index']),
                                'active' => $controller === 'pixel',
                                'linkOptions' => [],
                            ],
                            [
                                'label' => 'CRM',
                                'url' => Url::to(['/reference/crm/index']),
                                'active' => $controller === 'pixel',
                                'linkOptions' => [],
                            ],
                        ],
                    ],
                    [
                        'label' => 'Опросник',
                        'encode' => false,
                        'url' => Url::to(['/inquirer/index']),
                        'active' => $controller === 'inquirer',
                        'linkOptions' => [],
                    ],
                    [
                        'label' => 'Рекомендации<span class="arrow"></span>',
                        'encode' => false,
                        'active' => $module == 'analytics',
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                        'items' => [
                            [
                                'label' => 'Товары ABC',
                                'url' => ['/recommendations/product-abc'],
                                'active' => $controller === 'product-abc',
                            ],
                        ]
                    ],
                    [
                        'label' => 'Робот Бухгалтер <span class="arrow"></span>',
                        'encode' => false,
                        'active' => $module === 'robot',
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                        'items' => [
                            [
                                'label' => 'Справочник ИФНС',
                                'url' => Url::to(['/robot/ifns/index']),
                                'active' => $controller === 'ifns',
                                'linkOptions' => [],
                            ],
                        ],
                    ],
                    [
                        'label' => 'Unisender <span class="arrow"></span>',
                        'encode' => false,
                        'active' => $module === 'robot',
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                        'items' => [
                            [
                                'label' => 'Импорт контактов',
                                'url' => ['/unisender/contact/index'],
                            ],
                        ],
                    ],
                    [
                        'label' => 'Черный список <span class="arrow"></span>',
                        'encode' => false,
                        'options' => [
                            'class' => 'nav-item_accordeon',
                        ],
                        'items' => [
                            [
                                'label' => 'ИНН',
                                'url' => ['/inn-deny/index'],
                                'linkOptions' => [],
                            ],
                            [
                                'label' => 'Email',
                                'url' => ['/email-deny/index'],
                                'linkOptions' => [],
                            ],
                        ],
                    ],
                ],
                'options' => [
                    'class' => 'page-sidebar-menu page-sidebar-menu-light metismenu ' . $status,
                    'id' => 'side-menu',
                ],
            ]);
        } ?>
    </div>
</div>