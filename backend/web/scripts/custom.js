$(function () {
    $('.report-item').click(function () {
        var id = $(this).data('id');
        $('#report_' + id).hide();
        $('#report_edit_' + id).hide();
        $('#report_form_' + id).removeClass('hide');
    });

    $('.cancel-edit-report').click(function () {
        var id = $(this).data('id');

        $('#report_' + id).show();
        $('#report_edit_' + id).show();
        $('#report_form_' + id).addClass('hide');
    });
});

$(document).on('apply.daterangepicker', '#reportrange', function (ev, picker) {
    var button = $(this);

    $.post(
        '/admin/site/statistic-range',
        {
            name: picker.chosenLabel,
            date_from: picker.startDate.format('YYYY-MM-DD'),
            date_to: picker.endDate.format('YYYY-MM-DD')
        },
        function (data) {
            if (button.hasClass('ajax')) {
                $('#search-invoice-form').submit();
            } else if (data) {
                location.href = location.href;
            }
        }
    );
});


$('.js_filetype , .js_select').styler({
    'fileBrowse': '<span class="glyphicon glyphicon-plus-sign"></span> Выбрать файл'
});

$('.back').click(function () {
    $('.modal').modal('hide');
});
$('input.bank-logo').change(function () {
    var hasLogo = $(this).closest('.form-group').next(':hidden');
    console.log(123);
    console.log(hasLogo);
    if ($(this).val() == '') {
        hasLogo.val(0);
    } else {
        hasLogo.val(1);
    }
});
$('.modal').on('shown.bs.modal', function () {
    $('.bank-phone').each(function () {
        $(this).inputmask({"mask": "+7(9{3}) 9{3}-9{2}-9{2}"});
    });
});

$('.company-type-test').find('input').click(function () {
    $.post(
        $(this).data('url'),
        {
            status: +$(this).is(':checked')
        },
        function (result) {
            if (!result) {
                $(this).click();
            }
        }
    )
});

var $companyOsno = $('#companytaxationtype-osno');
var $companyUsn = $('#companytaxationtype-usn');
var $companyPsn = $('#companytaxationtype-psn');
var $companyOoo = $('#company-company_type_id [value=2], #company-company_type_id [value=3], #company-company_type_id [value=4]');

changeTaxationType($companyPsn, $companyOoo);
changeTaxationType($companyOsno, $companyUsn);
changeTaxationType($companyUsn, $companyOsno);

function changeTaxationType($changeSelector, $blockSelector) {
    $changeSelector.change(function () {
        if ($(this).is(':checked')) {
            $blockSelector.attr('disabled', true);
            $blockSelector.closest('div').addClass('disabled', true);
        } else {
            $blockSelector.removeAttr('disabled');
            $blockSelector.closest('div').removeClass('disabled');
        }
    });
}

$('#company-company_type_id').change(function () {
    if ($.inArray($(this).find('.checked').children().val(), ['2', '3', '4']) + 1 > 0) {
        $companyPsn.attr('disabled', true);
        $companyPsn.closest('div').addClass('disabled', true);
    } else {
        $companyPsn.removeAttr('disabled');
        $companyPsn.closest('div').removeClass('disabled');
    }
});

$('#reportsearch-periodtype').change(function () {
    $('#search-report').submit();
});

$('.change-period').click(function () {
    var $form = $(this).siblings('#list-date');
    var $input = $form.find('#reportsearch-perioddaylist');
    if ($(this).data('id') == 'plus') {
        $input.val(+$input.val() + 1);
    } else {
        $input.val(+$input.val() - 1);
    }
    $form.submit();
});

$('.activities-drop-down').change(function () {
    $.post($(this).data('url'), {
        activities_id: $(this).val()
    }, function (data) {

    });
});

$(document).on('click', '.copy-affiliate-link', function () {
    var affiliateLink = document.querySelector('.affiliate_invite');
    var range = document.createRange();
    range.selectNode(affiliateLink);
    window.getSelection().addRange(range);
    try {
        document.execCommand('copy');
    } catch (err) {
    }
    window.getSelection().removeAllRanges();
});

$(document).on('click', '.close-modal-button', function () {
    $(this).closest('.modal').modal('hide');
});

$(document).on('click', '.generate-api-key', function () {
    var $this = $(this);
    $.post($(this).data('url'), null, function (data) {
        $this.siblings('#apipartners-api_key').val(data.api_key).show();
        $this.siblings('#apipartners-generatedapikey').val(data.api_key);
        $this.siblings('.icon-refresh').show();
        $this.remove();
    });
});

$(document).on('click', '.field-apipartners-api_key .icon-refresh', function () {
    var $this = $(this);
    $.post($(this).data('url'), null, function (data) {
        $this.siblings('#apipartners-api_key').val(data.api_key);
        $this.siblings('#apipartners-generatedapikey').val(data.api_key);
    });
});

$(document).on('click', '.ladda-button', function (e) {
    e.preventDefault();
    if (!$(this).attr('data-style')) {
        $(this).attr('data-style', 'expand-right');
    }
    var l = Ladda.create(this);
    l.start();
    var $form = $(this).closest('form');
    if ($form.length > 0) {
        $form.submit();
    }
});

$(document).on('change', 'input[name="Company[test]"]', function (e) {
    $.post($(this).data('url'), $(this.form).serialize());
});
