/**
 * Created by Валик on 10.08.2017.
 */
var $xlsError = $('.xls-error');
var $xlsButton = $('.xls-buttons');
var $createdModels = $('.created-models');

function initXlsUpload() {
    vidimusUploader = new ss.SimpleUpload({
        button: $('.add-xls-file'),
        url: $('#xls-ajax-form').attr('action'), // server side handler
        progressUrl: $('#xls-ajax-form').data('progress'), // enables cross-browser progress support (more info below)
        responseType: 'json',
        name: 'uploadfile',
        allowedExtensions: ['xls', 'xlsx'], // for example, if we were uploading pics
        hoverClass: 'ui-state-hover',
        focusClass: 'ui-state-focus',
        disabledClass: 'ui-state-disabled',
        form: $('#xls-ajax-form'),
        onSubmit: function (filename, extension) {
            var progress = document.createElement('div'),
                bar = document.createElement('div'),
                fileSize = document.createElement('div'),
                wrapper = document.createElement('div'),
                progressBox = document.getElementById('progressBox');
            $xlsError.hide();
            progress.className = 'progress progress-striped';
            bar.className = 'progress-bar progress-bar-success';
            fileSize.className = 'size';
            wrapper.className = 'wrapper';
            progress.appendChild(bar);
            wrapper.innerHTML = '<div class="name">' + filename + '</div>';
            wrapper.appendChild(progress);
            progressBox.appendChild(wrapper);
            this.setProgressBar(bar);
            this.setFileSizeBox(fileSize);
            this.setProgressContainer(wrapper);
        },
        onChange: function (filename, extension, uploadBtn, fileSize, file) {
            var $maxFilesCount = 1;
            var $filesQueueKey = vidimusUploader.getQueueSize();
            var $filesCount = $filesQueueKey + 1;
            if (!validateXls(extension, fileSize)) {
                this.removeCurrent();
                return false;
            }
            $xlsButton.hide();
            $('.upload-xls-template-url').hide();
            $('.xls-title').hide();
            $('.upload-xls-button a').removeClass('disabled');
            if ($filesCount == $maxFilesCount) {
                $('.add-xls-file').addClass('disabled');
            }
            var $fileItemHtml = $('<div class="item">'
                + '<div class="file-name">' + filename + '</div>'
                + '<a href="#" data-queue="0" class="delete-file"><i class="fa fa-times-circle"></i></a>'
                + '<div></div>'
                + '</div>');
            $('.file-list').append($fileItemHtml);
        },
        onComplete: function (filename, data) {
            $xlsError.html(data.message);
            $xlsError.show();
            $createdModels.val(data.successProducts);
            $xlsButton.show();
            $('.xls-close, .undelete-models').click(function () {
                $('#import-xls').modal('hide');
            });
        }
    });
}

$('#import-xls').on('hidden', function (e) {
    $('.xls-title').show();
    $('.upload-xls-template-url').show();
    $xlsError.empty().hide();
    $xlsButton.hide();
    $createdModels.val('');
    $('.add-xls-file').removeClass('disabled');
    $('.upload-xls-button a').addClass('disabled');
    $('.file-list').empty();
});

$(document).on("hide.bs.modal", "#import-xls", function(event) {
    if ($xlsButton.is(':visible')) {
        location.href = location.href;
    }
});

function validateXls(fileExtension, fileSize) {
    var maxUploadedFileSize = 3072;
    var $extensions = ['xls', 'xlsx'];
    var $validation = true;

    if (fileSize > maxUploadedFileSize) {
        $xlsError.html('Превышен максимальный размер файла. Допустимый размер файла: 3 Мб');
        $xlsError.show();
        $validation = false;
    }

    if ($.inArray(fileExtension, $extensions) == -1) {
        $xlsError.html('Формат загружаемого файла не поддерживается. Допустимые форматы файла: ' + $extensions.join(', '));
        $xlsError.show();
        $validation = false;
    }

    if ($validation) {
        $xlsError.hide();
    }
    return $validation;
}

$(document).ready(function () {
    $(document).on('click', '.vidimus-cancel', function (e) {
        e.preventDefault();

        $('#vidimus-table').remove();
        $('.close').click();
    });

    $(document).on('click', '.vidimus-type', function (e) {
        e.preventDefault();
        let $items = $(this).siblings('.vidimus-type-item');
        if ($items.length) {
            if ($items.css('display') == 'none') {
                $items.show();
            } else {
                $items.hide();
            }
        }
    });

    $(document).on('click', '.vidimus-type-item div[data-id]', function (e) {
        e.preventDefault();
        $(this).parent().prev().find('.vidimus-type-title').html($(this).html());
        $(this).parent().hide();
        $(this).parent().prev().removeClass('red-vidimus');
        $(this).closest('tr').find('input.vidimus_type').val($(this).data('id'));
    });

    $(document).on('click', '.delete-file', function (e) {
        e.preventDefault();

        $(this).parent().remove();
        vidimusUploader['_queue'].splice($(this).data('queue'), 1);
        $('.add-vidimus-file').removeClass('disabled');

        if (vidimusUploader.getQueueSize() == 0) {
            $('.upload-button a').addClass('disabled');
            $.pjax.reload({
                "container": "#banking-module-pjax",
                "timeout": 5000,
                "url": "/cash/bank/index-upload"
            });
        }
    });

    $(document).on('click', '.upload-button a', function (e) {
        e.preventDefault();
        $('.file-list .item').remove();
        $('.upload-button a').addClass('disabled');
        $('.add-vidimus-file').removeClass('disabled');
        $('.bank-info').html('');
        $('#vidimus-table').html('');
        $('#vidimus-ajax-form').submit();
    });

    $(document).on('click', '.upload-xls-button a', function (e) {
        e.preventDefault();
        $('.file-list .item').remove();
        $('.upload-xls-button a').addClass('disabled');
        $('.add-xls-file').removeClass('disabled');
        $('#xls-ajax-form').submit();
    });

    if ($('.add-vidimus-file').length) {
        initialiseVidimusUploader();
    }
    if ($('.add-xls-file').length) {
        initXlsUpload();
    }

    $('.file-list').on('click', '.delete-file', function (e) {
        e.preventDefault();
        $(this).parent().remove();
        $('.xls-title').show();
        vidimusUploader['_queue'].splice(0, 1);
        $('.upload-xls-template-url').show();
        $('.upload-xls-button a').addClass('disabled');
        $('.add-xls-file').removeClass('disabled');
    });
});
