/**
 * Created by Наталья on 18.11.2014.
 */
/* mootools javascript */
 
// Check for the various File API support.
var isFileAPI = false;
if (window.File && window.FileReader && window.FileList && window.Blob) {
    isFileAPI = true;
}
  
//remove input element
function flushInput(el){
  var parent = $(el).getParent('.file_item');
  $(parent).destroy();
}
 
//copy input element on change event
function copyInput(el){
  var parent = $(el).getParent('.file_item');
  $(el).removeProperty('onchange');
  $(el).setProperty('onchange', 'handleFileSelect(event, this);');
  var copy = $(parent).clone();
  $(copy).getElement('input[type="file"]').setProperty('onchange', 'copyInput(this); handleFileSelect(event, this);');
  $(copy).getElement('input[type="file"]').setProperty('value','');
  $(copy).inject($(parent),'after');
 
  $(parent).getElement('.input_photo_controls').setStyle('display', 'block');
  if ( isFileAPI ) {
      $(parent).getElement('input[type="file"]').setStyle('display', 'none');
  }
}
 
 
//if browser supports File API  let's make image preview
if ( isFileAPI ) {
  function handleFileSelect(evt, el) {
      var files = evt.target.files;
      var f = files[0];
      var reader = new FileReader();
 
      reader.onload = (function(theFile) {
      return function(e) {
          $(el).getParent('.file_item').getElement('.img_preview').set('html', '<img src="'+ e.target.result+'" title="'+theFile.name+'" width="140"/>');
      };
      })(f);
      reader.readAsDataURL(f);
  }
}



