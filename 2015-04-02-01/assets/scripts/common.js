/**
 * Created by Наталья on 18.11.2014.
 */

$(document).ready( function() {
    
    $(function () {
        // $('.knob').knob({});
        $(".knob").parent("div").css("position", "absolute");     
    });

    var Search = function () {

        return {
            //main function to initiate the module
            init: function () {
                if (jQuery().datepicker) {
                    $('.date-picker').datepicker();
                }
                Metronic.initFancybox();
            }
        };
    }();    
    Search.init();

    $.fn.fixedButtons = function(elementObj) {
        $(this).each(function(){
            var container = $(this);
            var contentIn = container.find('.page-content-in');
            var element = $(elementObj);

            function minHeightCheck() {
                var minHeight = $(window).height() - $('.page-header').outerHeight() - $('.page-footer').outerHeight();
                minHeight = minHeight - element.outerHeight();

                contentIn.css('min-height', minHeight);
            }
            setTimeout(minHeightCheck(), 300);

            $(window).resize(function() {
                minHeightCheck();
            });
        });
    };
    $('.page-content').fixedButtons('.action-buttons');

    $.fn.init_checkAll = function() {
        var page = $(this); 
        
        page.find('#check-all').click(function(){
            if($('#check-all').hasClass('active')) {
                $('#check-all').removeClass('active');
                $('.checkall-slide').addClass('collapse');
                $('.checkall-slide').removeClass('expand');
                $('.portlet-body-checkall').css({ display: "block" });
                $('html, body').animate({scrollTop:0},600);
                $('.feeds').removeClass('open');
            } else {
                $('#check-all').addClass('active');
                $('.checkall-slide').removeClass('collapse');
                $('.checkall-slide').addClass('expand');
                $('.portlet-body-checkall').css({ display: "none" });
                $('html, body').delay(100).animate({
                 scrollTop: $('.widget-latest-actions').offset().top
                }, 600, "swing"); 
                $(this).html($(this).html() == 'Посмотреть все' ? 'Посмотреть последние' : 'Посмотреть все');
                $('.feeds').addClass('open');
            }
        });

    };
    $('.home-page').init_checkAll();


     var UIExtendedModals = function () {
        return {
            //main function to initiate the module
            init: function () {

                //dynamic demo:
                $('.dynamic .demo').click(function(){
                  var tmpl = [
                    // tabindex is required for focus
                    '<div class="modal hide fade" tabindex="-1">',
                      '<div class="modal-header">',
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Ã—</button>',
                        '<h4 class="modal-title">Modal header</h4>', 
                      '</div>',
                      '<div class="modal-body">',
                        '<p>Test</p>',
                      '</div>',
                      '<div class="modal-footer">',
                        '<a href="#" data-dismiss="modal" class="btn btn-default">Close</a>',
                        '<a href="#" class="btn btn-primary">Save changes</a>',
                      '</div>',
                    '</div>'
                  ].join('');
                  
                  $(tmpl).modal();
                });

                //ajax demo:
                var $modal = $('#ajax-modal');

                $('#ajax-demo').on('click', function(){
                  // create the backdrop and wait for next modal to be triggered
                  $('body').modalmanager('loading');

                  setTimeout(function(){
                      $modal.load('ui_extended_modals_ajax_sample.html', '', function(){
                      $modal.modal();
                    });
                  }, 1000);
                });

                $modal.on('click', '.update', function(){
                  $modal.modal('loading');
                  setTimeout(function(){
                    $modal
                      .modal('loading')
                      .find('.modal-body')
                        .prepend('<div class="alert alert-info fade in">' +
                          'Updated!<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '</div>');
                  }, 1000);
                });
            }

        };


    }();
    UIExtendedModals.init();
    // Demo.init();

	$('#reportrange').daterangepicker(
		{
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
				'Last 7 Days': [moment().subtract('days', 6), moment()],
				'Last 30 Days': [moment().subtract('days', 29), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
			},
			startDate: moment().subtract('days', 29),
			endDate: moment(),
			language: 'ru'
		},
		function(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}
	);
        var infobutton = $(".info-button");
        $(".edit").click(function() {
            $(".btn-save").removeClass("hide");
            $(".btn-cancel").removeClass("hide");
            $(".editable-field").addClass("hide");
            $(".input-editable-field").removeClass("hide");
            infobutton.addClass("hide");
        });

 /*       $(document).on('click', '.btn-cancel', function () {
            $(".btn-save").addClass("hide");
            $(".btn-cancel").addClass("hide");
            $(".editable-field").removeClass("hide");
            $(".input-editable-field").addClass("hide");
            $(".edit-in-act").removeClass("hide");
            $(".control-panel-edit").addClass("hide");
            $(".in-act__head").addClass("col-md-7");
            $(".in-act__head").removeClass("col-md-12");
            $(".customer-info .caption").css("width", "88%");
            $(".edit-in").removeClass("hide");
            $(".customer-info-cash").removeClass("hide");
            $("#edit-cash-order").addClass("hide");
            $(".remove-table").removeClass("hide");
            infobutton.removeClass("hide");
        });

        $(".btn").click(function(event) {
            event.preventDefault();
        });
        $("#connection-to-account").click(function() {
            $('#accounts-list').modal();
        });

        $(document).on('click', '.edit-in', function () {
            $(".edit-in").addClass("hide");
            $(".control-panel-pre-edit").addClass("hide");
            $(".control-panel-edit").removeClass("hide");
            $(".in-act__head").addClass("col-md-12");
            $(".in-act__head").removeClass("col-md-7");
            $(".remove-table").addClass("hide");
            $(".customer-info .caption").css("width", "60%");
        });*/

        $(".edit-cash-order").click(function() {
            $(".customer-info-cash").addClass("hide");
            $("#edit-cash-order").removeClass("hide"); 
        });

    Layout.init();

	$('#side-menu').metisMenu();
});




